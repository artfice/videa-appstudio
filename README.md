# videa-appstudio serverless project

### First time Setup
```sh
npm install
```

Copy your .env.sample to .env but change the LOGENTRIES_TOKEN

### Commands
#### Start Server Offline
```sh
sls offline
```

#### Build All Typescript
```sh
npm run build:all
```

#### Build appstudio Typescript
```sh
npm run build:appstudio
```

#### Build api Typescript
```sh
npm run build:api
```

#### Run Test
```sh
npm run test
```

#### Run all lints
```sh
npm run lint
```

#### Clean generated js files
```sh
npm run clean
```

#### Run [serverless commands](https://serverless.com/framework/docs/providers/aws/cli-reference/)
```sh
npm run sls -- <cmd>
```
e.g
```sh
npm run sls -- deploy
```
```sh
npm run sls -- invoke --function functionName
```

#### Build & Deploy (env_name could be **deva**, **devb**, **stage**, **preprod**, **prod**)
```sh
npm run env_name
```
