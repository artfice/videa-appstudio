'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {CreateScreenHandler} from '../../appstudio/handler/screen/CreateScreenHandler';

const createScreenHandler = new CreateScreenHandler();

export const handler = Digi.Function.pass(createScreenHandler.handler, [], createScreenHandler);