'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DeleteScreenHandler} from '../../appstudio/handler/screen/DeleteScreenHandler';

const deleteScreenHandler = new DeleteScreenHandler();

export const handler = Digi.Function.pass(deleteScreenHandler.handler, [], deleteScreenHandler);