'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetScreenHandler} from '../../appstudio/handler/screen/GetScreenHandler';

const getScreenHandler = new GetScreenHandler();

export const handler = Digi.Function.pass(getScreenHandler.handler, [], getScreenHandler);