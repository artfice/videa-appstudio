'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {EditScreenHandler} from '../../appstudio/handler/screen/EditScreenHandler';

const editScreenHandler = new EditScreenHandler();

export const handler = Digi.Function.pass(editScreenHandler.handler, [], editScreenHandler);