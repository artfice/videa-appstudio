'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {EditGalleryHandler} from '../../appstudio/handler/gallery/EditGalleryHandler';
const editGalleryHandler = new EditGalleryHandler();

export const handler = Digi.Function.pass(editGalleryHandler.handler, [], editGalleryHandler);