'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetGalleryHandler} from '../../appstudio/handler/gallery/GetGalleryHandler';
const getGalleryHandler = new GetGalleryHandler();

export const handler = Digi.Function.pass(getGalleryHandler.handler, [], getGalleryHandler);