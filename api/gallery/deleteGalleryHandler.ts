'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DeleteGalleryHandler} from '../../appstudio/handler/gallery/DeleteGalleryHandler';
const deleteGalleryHandler = new DeleteGalleryHandler();

export const handler = Digi.Function.pass(deleteGalleryHandler.handler, [], deleteGalleryHandler);