'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {CreateGalleryHandler} from '../../appstudio/handler/gallery/CreateGalleryHandler';
const createGalleryHandler = new CreateGalleryHandler();
export const handler = Digi.Function.pass(createGalleryHandler.handler, [], createGalleryHandler);