'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {LicenseHandler} from '../../appstudio/handler/license/LicenseHandler';
const licenseHandler = new LicenseHandler();
export const handler = Digi.Function.pass(licenseHandler.handler, [], licenseHandler);