'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {ScreenLambda} from '../../appstudio/handler/dynamicEnum/ScreenLambda';

const screenLambda = new ScreenLambda();

export const handler = Digi.Function.pass(screenLambda.handler, [], screenLambda);