'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {AnalyticLambda} from '../../appstudio/handler/dynamicEnum/AnalyticLambda';

const analyticLambda = new AnalyticLambda();

export const handler = Digi.Function.pass(analyticLambda.handler, [], analyticLambda);