'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DeleteBrandHandler} from '../../appstudio/handler/brands/DeleteBrandHandler';

const deleteBrandHandler = new DeleteBrandHandler();

export const handler = Digi.Function.pass(deleteBrandHandler.handler, [], deleteBrandHandler);