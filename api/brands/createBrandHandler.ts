'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {CreateBrandHandler} from '../../appstudio/handler/brands/CreateBrandHandler';

const createBrandHandler = new CreateBrandHandler();

export const handler = Digi.Function.pass(createBrandHandler.handler, [], createBrandHandler)