'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {EditBrandHandler} from '../../appstudio/handler/brands/EditBrandHandler';

const editBrandHandler = new EditBrandHandler();

export const handler = Digi.Function.pass(editBrandHandler.handler, [], editBrandHandler);