'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetAllBrandHandler} from '../../appstudio/handler/brands/GetAllBrandHandler';

const getAllBrandHandler = new GetAllBrandHandler();

export const handler = Digi.Function.pass(getAllBrandHandler.handler, [], getAllBrandHandler);