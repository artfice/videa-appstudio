'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetBrandHandler} from '../../appstudio/handler/brands/GetBrandHandler';

const getBrandHandler = new GetBrandHandler();

export const handler = Digi.Function.pass(getBrandHandler.handler, [], getBrandHandler);