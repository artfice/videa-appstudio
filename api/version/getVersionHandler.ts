'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import { GetVersionHandler } from '../../appstudio/handler/version/GetVersionHandler';

const getVersionHandler = new GetVersionHandler();

export const handler = Digi.Function.pass(getVersionHandler.handler, [], getVersionHandler);