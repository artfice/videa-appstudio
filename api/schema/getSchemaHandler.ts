'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import { GetSchemaHandler } from '../../appstudio/handler/schema/GetSchemaHandler';

const getSchemaHandler = new GetSchemaHandler();

export const handler = Digi.Function.pass(getSchemaHandler.handler, [], getSchemaHandler);