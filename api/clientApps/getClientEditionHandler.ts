'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetClientEditionHandler} from '../../appstudio/handler/clientApps/GetClientEditionHandler';

const getClientEditionHandler = new GetClientEditionHandler();

export const handler = Digi.Function.pass(getClientEditionHandler.handler, [], getClientEditionHandler);