'use strict';
///<reference path="../../typings/index.d.ts"/>

import * as Digi from 'videa-framework/Digi';
import dotenv = require('dotenv');
// load .env
dotenv.config();
import {GetClientConfigHandler} from '../../appstudio/handler/clientApps/GetClientConfigHandler';

const getClientConfigHandler = new GetClientConfigHandler();

export const handler = Digi.Function.pass(getClientConfigHandler.handler, [], getClientConfigHandler);