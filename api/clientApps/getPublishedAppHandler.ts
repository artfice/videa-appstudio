'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetPublishedAppHandler} from '../../appstudio/handler/clientApps/GetPublishedAppHandler';

const getPublishedAppHandler = new GetPublishedAppHandler();

export const handler = Digi.Function.pass(getPublishedAppHandler.handler, [], getPublishedAppHandler);