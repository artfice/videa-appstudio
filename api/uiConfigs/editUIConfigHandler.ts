'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {EditUIConfigHandler} from '../../appstudio/handler/uiConfig/EditUIConfigHandler';

const editUIConfigHandler = new EditUIConfigHandler();

export const handler = Digi.Function.pass(editUIConfigHandler.handler, [], editUIConfigHandler);
