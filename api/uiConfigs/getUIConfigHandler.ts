'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetUIConfigHandler} from '../../appstudio/handler/uiConfig/GetUIConfigHandler';

const getUIConfigHandler = new GetUIConfigHandler();

export const handler = Digi.Function.pass(getUIConfigHandler.handler, [], getUIConfigHandler);
