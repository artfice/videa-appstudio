'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DeleteUIConfigHandler} from '../../appstudio/handler/uiConfig/DeleteUIConfigHandler';

const deleteUIConfigHandler = new DeleteUIConfigHandler();

export const handler = Digi.Function.pass(deleteUIConfigHandler.handler, [], deleteUIConfigHandler);