'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DuplicateUIConfigHandler} from '../../appstudio/handler/uiConfig/DuplicateUIConfigHandler';

const duplicateUIConfigHandler = new DuplicateUIConfigHandler();

export const handler = Digi.Function.pass(duplicateUIConfigHandler.handler, [], duplicateUIConfigHandler);
