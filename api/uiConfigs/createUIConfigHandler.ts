'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {CreateUIConfigHandler} from '../../appstudio/handler/uiConfig/CreateUIConfigHandler';

const createUIConfigHandler = new CreateUIConfigHandler();

export const handler = Digi.Function.pass(createUIConfigHandler.handler, [], createUIConfigHandler);