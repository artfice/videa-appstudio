'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {EditAppHandler} from '../../appstudio/handler/apps/EditAppHandler';

const editAppHandler = new EditAppHandler();

export const handler = Digi.Function.pass(editAppHandler.handler, [], editAppHandler);