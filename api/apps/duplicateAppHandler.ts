'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DuplicateAppHandler} from '../../appstudio/handler/apps/DuplicateAppHandler';

const duplicateAppHandler = new DuplicateAppHandler();

export const handler = Digi.Function.pass(duplicateAppHandler.handler, [], duplicateAppHandler);