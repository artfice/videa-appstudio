'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {CreateAppHandler} from '../../appstudio/handler/apps/CreateAppHandler';

const createAppHandler = new CreateAppHandler();

export const handler = Digi.Function.pass(createAppHandler.handler, [], createAppHandler);