'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetAllAppsHandler} from '../../appstudio/handler/apps/GetAllAppsHandler';

const getAllAppsHandler = new GetAllAppsHandler();

export const handler = Digi.Function.pass(getAllAppsHandler.handler, [], getAllAppsHandler);