'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetAppHandler} from '../../appstudio/handler/apps/GetAppHandler';

const getAppHandler = new GetAppHandler();

export const handler = Digi.Function.pass(getAppHandler.handler, [], getAppHandler);