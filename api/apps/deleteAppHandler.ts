'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DeleteAppHandler} from '../../appstudio/handler/apps/DeleteAppHandler';

const deleteAppHandler = new DeleteAppHandler();

export const handler = Digi.Function.pass(deleteAppHandler.handler, [], deleteAppHandler);