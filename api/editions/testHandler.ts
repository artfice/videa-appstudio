'use strict';
import dotenv = require('dotenv');
dotenv.config();
import AWS = require('aws-sdk');

module.exports = {
    handler: (event, context, callback) => {
    const region = 'us-east-1';
    console.log('1');
    const accountId = process.env.AWS_ACCOUNTID;
    const dispatchFunction = 'configChanged';
    console.log('2');
    const params = {
        Message: JSON.stringify({
            accountId: 'cw',
            editionId: '42b00830-4f66-11e6-bf99-056bc67c515e'
        }),
        TopicArn: 'arn:aws:sns:' + region + ':' + accountId + ':' + dispatchFunction
    };
    console.log('3', params);
    const sns = new AWS.SNS({ apiVersion: '2010-03-31' });
    console.log('4');
    sns.publish(params, (error, data) => {
        if (error) {
            console.log(error);
            console.log('7');
            callback(error);
        }
        console.log('6');
        callback(null, {
            message: 'Message successfully', 
            params: params, 
            data: data
        });
        console.log('8');

    });
}
};