///<reference path="../../typings/index.d.ts"/>
'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {ConfigChangedHandler} from '../../appstudio/handler/edition/ConfigChangedHandler';

const configChangedHandler = new ConfigChangedHandler();

export const handler = Digi.Function.pass(configChangedHandler.handler, [], configChangedHandler);