'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetEditionHandler} from '../../appstudio/handler/edition/GetEditionHandler';

const getEditionHandler = new GetEditionHandler();

export const handler = Digi.Function.pass(getEditionHandler.handler, [], getEditionHandler);