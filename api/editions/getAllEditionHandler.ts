'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetAllEditionStatesHandler} from '../../appstudio/handler/edition/GetAllEditionStatesHandler';

const getAllEditionStatesHandler = new GetAllEditionStatesHandler();

export const handler = Digi.Function.pass(getAllEditionStatesHandler.handler, [], getAllEditionStatesHandler);