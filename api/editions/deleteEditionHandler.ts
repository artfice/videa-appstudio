'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DeleteEditionHandler} from '../../appstudio/handler/edition/DeleteEditionHandler';

const deleteEditionHandler = new DeleteEditionHandler();

export const handler = Digi.Function.pass(deleteEditionHandler.handler, [], deleteEditionHandler);