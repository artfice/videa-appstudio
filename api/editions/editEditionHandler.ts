'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {EditEditionHandler} from '../../appstudio/handler/edition/EditEditionHandler';

const editEditionHandler = new EditEditionHandler();

export const handler = Digi.Function.pass(editEditionHandler.handler, [], editEditionHandler);