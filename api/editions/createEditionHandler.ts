'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {CreateEditionHandler} from '../../appstudio/handler/edition/CreateEditionHandler';

const createEditionHandler = new CreateEditionHandler();

export const handler = Digi.Function.pass(createEditionHandler.handler, [], createEditionHandler);