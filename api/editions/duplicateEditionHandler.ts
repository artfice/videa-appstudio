'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {DuplicateEditionHandler} from '../../appstudio/handler/edition/DuplicateEditionHandler';

const duplicateEditionHandler = new DuplicateEditionHandler();

export const handler = Digi.Function.pass(duplicateEditionHandler.handler, [], duplicateEditionHandler);