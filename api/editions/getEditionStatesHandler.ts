'use strict';
import dotenv = require('dotenv');
dotenv.config();
import * as Digi from 'videa-framework/Digi';
import {GetAllEditionStatesHandler as GetEditionStatesHandler} from '../../appstudio/handler/edition/GetAllEditionStatesHandler';

const getEditionStatesHandler = new GetEditionStatesHandler();

export const handler = Digi.Function.pass(getEditionStatesHandler.handler, [], getEditionStatesHandler);