var gulp = require('gulp');
var shell = require('gulp-shell');

require('videa-build-scripts/serverless/gulp');
var configs = require('videa-build-scripts/serverless/gulp/config');

gulp.task('init', shell.task([
    'npm run build'
]));

gulp.task('test:clean', function() {
    var clean = require('gulp-clean');
    return gulp.src(configs.cleanFixtureJsSrc, { base: '.', read: false })
        .pipe(clean());
});

gulp.task('test:run', ['test:clean'],  function() {
    var mocha = require('gulp-mocha');
    return gulp.src(configs.testSrc, { read: false })
        .pipe(mocha({
            require: [],
            globals: {
                recursive: true,
                timeout: 15000
            },
            reporter: 'spec'
        })).once('end', function () {
            process.exit();
        });
});