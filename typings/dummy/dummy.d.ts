/**
 * Created by bardiakhosravi on 2016-10-14.
 */

declare module 'recursive-iterator' {
    var _q: any;
    export = _q;    
}


declare module 'json-schema-build' {
    var _q: any;
    export = _q;
}

declare module 'streamifier' {
    let _q: any;
    export = _q;
}

declare module 'short-id' {
    let _q: any;
    export = _q;
}