export interface Range {
    beginIndex: number;
    endIndex: number;
}