/**
 * Created by bardiakhosravi on 2015-12-08.
 */

import {Range} from './Range';

export interface Feed {
    sourceUrl: string;
    range: Range;
}