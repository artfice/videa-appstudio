/**
 * Created by bardiakhosravi on 15-12-02.
 */
import {BaseDto} from 'videa-framework/domain/BaseDto';

export interface MetadataOwner extends BaseDto {
    _metadata?: string;
    type?: string;
    displayField?: string;
}