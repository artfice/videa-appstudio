import { MetadataOwner } from '../MetadataOwner';

export interface CustomData extends MetadataOwner {
    content: string;
}