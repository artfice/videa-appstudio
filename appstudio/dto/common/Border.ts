import { MetadataOwner } from '../MetadataOwner';
import { Color } from './Color';
export interface Border extends MetadataOwner {
    radius: number;
    thickness: number;
    color: Color;
}