import { MetadataOwner } from '../MetadataOwner';
import { BaseEvent } from '../common/listener/event/BaseEvent';
import { BaseAction } from '../common/listener/action/BaseAction';

export interface Listener<TEvent extends BaseEvent, TAction extends BaseAction> extends MetadataOwner {
    event: TEvent;
    action: TAction;
    active: string;
}