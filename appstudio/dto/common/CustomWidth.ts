import { MetadataOwner } from '../MetadataOwner';
 
 export interface CustomWidth extends MetadataOwner {
     value: number;
 }