import {GalleryImage} from './GalleryImage';
import {Color} from './Color';

export interface ColorGalleryImage extends GalleryImage{
    color: Color;
}