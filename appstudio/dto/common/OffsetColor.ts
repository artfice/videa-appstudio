import { MetadataOwner } from '../MetadataOwner';

export interface OffsetColor extends MetadataOwner {
    value: string;
    offset: number;
}