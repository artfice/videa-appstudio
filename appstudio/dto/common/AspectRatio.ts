import {MetadataOwner} from '../MetadataOwner';

export interface AspectRatio extends MetadataOwner {
    aspectRatio: any;
    value?: any;
    width?: any;
    height?: any;
}