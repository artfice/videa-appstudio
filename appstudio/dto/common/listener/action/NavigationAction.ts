import {BaseAction} from './BaseAction';

export interface NavigationAction extends BaseAction {
    configId: string;
    screenId: string;
}