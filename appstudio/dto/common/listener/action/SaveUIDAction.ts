import {BaseAction} from './BaseAction';

export interface SaveUIDAction extends BaseAction {
    path: string;
}