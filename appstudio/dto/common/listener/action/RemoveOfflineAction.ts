import {BaseAction} from './BaseAction';

export interface RemoveOfflineAction extends BaseAction {
    path: string;
}