import {BaseAction} from './BaseAction';

export interface PlaybackAction extends BaseAction {
  live: string;
  title: string;
  description: string;
  duration: string;
  source: string;
  format: string;
  closedCaptioningFormat: string;
  screenIdToNavigateOnEnd: string;
  startTime: string;
  player: string;
}