import {BaseAction} from './BaseAction';

export interface SaveOfflineAction extends BaseAction {
    path: string;
}