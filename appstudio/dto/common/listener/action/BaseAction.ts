import { MetadataOwner } from '../../../MetadataOwner';

export interface BaseAction extends MetadataOwner {
    startTime?: string;
    event: any;
}