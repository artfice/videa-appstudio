import {BaseAction} from './BaseAction';

export interface RemoveUIDAction extends BaseAction {
    path: string;
}