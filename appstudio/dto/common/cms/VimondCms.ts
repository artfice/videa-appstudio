import {Cms} from '../../app/configuration/Cms';
export interface VimondCms  extends Cms {
    uid: string;
    delimiter: string;
}