import {Cms} from '../../app/configuration/Cms';

export interface AkamaiVOC extends Cms {

    uid: string;
    delimiter: string;
    apiKey: string;
    serverIp: string;
    networkPreference: string;
    contentRelevance: string;
    prefetchLimit: number;
    individualFileLimit: number;
    minBatteryLevelForPrefetch: number;
    enableBackgroundDownloads: boolean;
    subscribedProviders: string;
    subscribedCategories: string;
}