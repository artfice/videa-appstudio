import {Cms} from '../../app/configuration/Cms';

export interface TrueLogic  extends Cms {
    requestToken: string;
    uid: string;
    delimiter: string;
}