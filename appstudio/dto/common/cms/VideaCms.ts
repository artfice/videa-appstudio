import {Cms} from '../../app/configuration/Cms';
export interface VideaCms  extends Cms {
    PID: string;
    accountID: number;
}