import {Cms} from '../../app/configuration/Cms';

export interface Tvn  extends Cms {
    collectionPath: string;
    uid: string;
    delimiter: string;
}