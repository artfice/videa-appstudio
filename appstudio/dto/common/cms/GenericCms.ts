import {Cms} from '../../app/configuration/Cms';
export interface GenericCms  extends Cms {
    collectionPath?: string;
    uid: string;
    delimiter: string;
}