import {Cms} from '../../app/configuration/Cms';

export interface MPX  extends Cms {
    PID: string;
    accountID: number;
    uid: string;
    delimiter: string;
}