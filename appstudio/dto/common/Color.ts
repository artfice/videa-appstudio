import { MetadataOwner } from '../MetadataOwner';

export interface Color extends MetadataOwner {
    value: string;
}