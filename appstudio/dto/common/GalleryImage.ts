import { MetadataOwner } from '../MetadataOwner';

export interface GalleryImage extends MetadataOwner {
    title?: string;
    active?: boolean;
    galleryImageId?: string;
    url?: string;
}