import {BaseMenu} from './BaseMenu';
import {Container as ContainerDto} from '../../clientapp/component/Container';

export interface DrawerMenu extends BaseMenu {
    section: ContainerDto[];
    secondaryMenu: ContainerDto;
}