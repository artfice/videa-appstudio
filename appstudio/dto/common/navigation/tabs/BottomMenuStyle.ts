import {ColoredComponentStyle} from '../../../app/configuration/uiconfig/screen/component/style/ColoredComponentStyle';
import {Color} from '../../../common/Color';
import {Font} from '../../../common/Font';

export interface BottomMenuStyle extends ColoredComponentStyle {
    selectedColor: Color;
    font: Font;
}