import {Label} from '../../../app/configuration/uiconfig/screen/component/field/label/Label';
import {Image} from '../../../app/configuration/uiconfig/screen/component/field/image/Image';
import {Component} from '../../../app/configuration/uiconfig/screen/component/Component';
import {BaseStyle} from '../../../app/configuration/uiconfig/screen/component/style/BaseStyle';

export interface Tabs extends Component<BaseStyle> {
    label: Label;
    icon: Image;
}