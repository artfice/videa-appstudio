import {BaseMenu} from './BaseMenu';
import {Tabs} from '../../common/navigation/tabs/Tabs';
import {BottomMenuStyle} from '../../common/navigation/tabs/BottomMenuStyle';

export interface BottomMenu extends BaseMenu {
    tab: Tabs[];
    style: BottomMenuStyle;
}