import {MetadataOwner} from '../../../MetadataOwner';

export interface TopMenuContent extends MetadataOwner {
    subComponent: any[];
}