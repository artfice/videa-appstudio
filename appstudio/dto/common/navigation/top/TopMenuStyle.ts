import {SizedComponentStyle} from '../../../../dto/app/configuration/uiconfig/screen/component/style/SizedComponentStyle';
import {Color} from '../../../../dto/common/Color';
import {GradientColor} from '../../../../dto/common/GradientColor';

export interface TopMenuStyle extends SizedComponentStyle {
    backgroundColor: Color | GradientColor;
}