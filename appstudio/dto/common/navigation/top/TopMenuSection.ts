import {Component} from '../../../../dto/app/configuration/uiconfig/screen/component/Component';
import {BaseStyle} from '../../../../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';
import {TopMenuContent} from './TopMenuContent';

export interface TopMenuSection extends Component<BaseStyle> {
    content: TopMenuContent;
}