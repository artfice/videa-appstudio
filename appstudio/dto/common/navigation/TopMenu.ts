import {BaseMenu} from './BaseMenu';
import {TopMenuStyle} from './top/TopMenuStyle';
import {TopMenuSection} from './top/TopMenuSection';

export interface TopMenu extends BaseMenu {
    style: TopMenuStyle;
	leftSection: TopMenuSection;
	middleSection: TopMenuSection;
	rightSection: TopMenuSection;
}