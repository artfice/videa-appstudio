import { MetadataOwner } from '../MetadataOwner';

export interface Position extends MetadataOwner {
    value: string;
}
