import { MetadataOwner } from '../MetadataOwner';

export interface BoxParams extends MetadataOwner {
    top: number;
    right: number;
    bottom: number;
    left: number;
}