import { MetadataOwner } from '../MetadataOwner';
import { Color } from './Color';

export interface Font extends MetadataOwner {
    family: string;
    size: string;
    weight?: string;
    color: Color;
    alignment: string;
}