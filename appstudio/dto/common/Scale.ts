import { MetadataOwner } from '../MetadataOwner';

export interface Scale extends MetadataOwner {
    value: string;
}
