import { MetadataOwner } from '../MetadataOwner';

export interface CustomHeight extends MetadataOwner {
    value: number;
}