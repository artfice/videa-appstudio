import {MetadataOwner} from '../MetadataOwner';
import {Color} from '../common/Color';

export interface ThemeFont extends MetadataOwner {
    color: Color;
    family: string;
    size: string;
    weight: string;
}