import {MetadataOwner} from '../MetadataOwner';

export interface BaseReference extends MetadataOwner {
     refId: string;
     value: string;
}