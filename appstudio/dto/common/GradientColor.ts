import { MetadataOwner } from '../MetadataOwner';
import {OffsetColor} from './OffsetColor';

export interface GradientColor extends MetadataOwner {
    direction: string;
    stops: OffsetColor[]
}