import { MetadataOwner } from '../MetadataOwner';

export interface Alignment extends MetadataOwner {
    value: string;
}
