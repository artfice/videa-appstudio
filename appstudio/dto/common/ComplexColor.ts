import { MetadataOwner } from '../MetadataOwner';
import {Color} from "./Color";
import {GradientColor} from './GradientColor';

export interface ComplexColor extends MetadataOwner {
    value: Color | GradientColor;
}