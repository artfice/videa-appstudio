import { MetadataOwner } from '../MetadataOwner';
import { Color } from './Color';
import { Alignment } from './Alignment';

export interface BaseFont extends MetadataOwner {
    size: string;
    weight?: string;
    color: Color;
    alignment: Alignment;
}
