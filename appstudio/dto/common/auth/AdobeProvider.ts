import {Provider} from './BaseProvider';
export interface AdobeProvider extends Provider {
    accountId: string;
}