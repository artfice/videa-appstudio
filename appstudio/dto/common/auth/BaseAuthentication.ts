import {MetadataOwner} from '../../MetadataOwner';
import {ScreenCredential} from './screen/ScreenCredential';
import UrlConfiguration = require('./url/UrlConfiguration');

export interface BasicAuthentication  extends MetadataOwner {
    provider: string;
    method: string;
    required: boolean;
    url: string;
    loginScreen: ScreenCredential;
    defaultScreen: ScreenCredential;
    login: UrlConfiguration;
    logout: UrlConfiguration;
}