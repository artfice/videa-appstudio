import {MetadataOwner} from '../../../MetadataOwner';
export interface UrlConfiguration  extends MetadataOwner {
    method: string;
     url: string;
 }