import {Provider as BaseProvider} from './BaseProvider';
import {Redirect} from '../auth/akamai/Redirect';
import {Platform} from '../auth/akamai/Platform';
import {CustomData} from '../CustomData';

export interface AkamaiProvider extends BaseProvider {
    akamaiIDP: string;
    platformID: Platform;
    redirect: Redirect;
    resourceAccessCodes: CustomData;
}