import {MetadataOwner} from '../../../MetadataOwner';

export interface ScreenCredential  extends MetadataOwner {
    screenId: string;
    configId: string;
}