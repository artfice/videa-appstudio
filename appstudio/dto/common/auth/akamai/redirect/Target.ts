import {MetadataOwner} from '../../../../MetadataOwner';
export interface Target  extends MetadataOwner {
    name: string;
    target: string;
}