import {Target} from './redirect/Target';
import {MetadataOwner} from '../../../MetadataOwner';

export interface Redirect  extends MetadataOwner {
    iOS: Target;
    android: Target;
    web: Target;
}