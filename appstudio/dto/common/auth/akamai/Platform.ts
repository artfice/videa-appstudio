import {MetadataOwner} from '../../../MetadataOwner';

export interface Platform  extends MetadataOwner {
    iOS: string;
    android: string;
    web: string;
}