/**
 * Created by bardiakhosravi on 2015-12-17.
 */

import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

import _ = require('underscore');


import {Container as ContainerDto} from '../../clientapp/component/Container';
import {Layout as LayoutDto} from '../../clientapp/component/layout/Layout';
import {Component as ComponentDto} from '../../clientapp/Component';
import {Image as ImageDto} from '../../clientapp/component/field/Image';
import {Listener as ListenerDto} from '../../common/Listener';
import {Color as ColorDto} from '../../common/Color';
import {BaseEvent as BaseEvent} from '../../common/listener/event/BaseEvent';
import {BaseAction as BaseAction} from '../../common/listener/action/BaseAction';
import {Style as Style} from '../../clientapp/Style';

import RelativeLayoutSchema = require('../../../schema/clientapp/app/uiconfig/screen/component/layout/RelativeLayout');
import LinearLayoutSchema = require('../../../schema/clientapp/app/uiconfig/screen/component/layout/LinearLayout');


export class Panel {

    private _layout: LayoutDto;

    private _panel: ContainerDto;

    private _toolbarContainer: ContainerDto;

    private _backgroundImage: ImageDto<Style>;

    private _toolbarPosition: string;
    /**
     *
     * @param config
     * backgroundColor: string hex color value
     */
    constructor (config?) {

        var aspectRatio = config && config.aspectRatio,
            padding = config && config.padding,
            toolbarPosition = config && config.toolbarPosition,
            backgroundColor = config && config.backgroundColor,
            backgroundImageValue = config && config.backgroundImageValue,
            backgroundImageAspectRatio = config && config.backgroundImageAspectRatio;

        this._toolbarPosition = toolbarPosition;
        this._layout = this._getLayout(toolbarPosition);

        this._toolbarContainer = <ContainerDto>ClientAppModelFactory.create('Container', {
            style: ClientAppModelFactory.create("Style")
        });

        this._panel = <ContainerDto>ClientAppModelFactory.create('Container', {

            layout: this._layout,

            height: 'wrapContent',
            width: 'matchParent',

            style: ClientAppModelFactory.create('Style'),

            items: [
                this._toolbarContainer
            ],
            listeners: []
        });

        if (backgroundImageValue) {
            this.setBackgroundImage(backgroundImageValue, backgroundImageAspectRatio);
        }

        this.setAspectRatio(aspectRatio);

        this.setContentContainerPadding(padding);

        if (backgroundColor) {
            this.setBackgroundColor(<ColorDto>ClientAppModelFactory.create('Color', {
                value: backgroundColor
            }));
        }
    }

    public setWidth(width: string) {
        this._panel.width = width;
    }

    public setHeight(height: string) {
        this._panel.height = height;
    }

    public setBackgroundColor(color: ColorDto) {
        this._panel.style.backgroundColor = color;
    }

    /**
     *
     * @param value the url of the image
     * @param aspectRatio aspect ratio if the image
     */
    public setBackgroundImage(value: string, aspectRatio?: any) {
        var currentBackgroundImage = this._backgroundImage;

        if (currentBackgroundImage) {
            // if a background image exists remove it from the items
            this._panel.items = _.filter(this._panel.items, (cmp) => {
                return cmp !== currentBackgroundImage
            });
        }

        // then create a new one
        this._backgroundImage = this._getBackgroundImage(value, aspectRatio);


        // and put it as the first item in the itemRenderer
        this._panel.items.unshift(this._backgroundImage);

        this._backgroundImage.value = value;
        if (aspectRatio) {
            this._backgroundImage.aspectRatio = aspectRatio;
        }
    }


    private _getBackgroundImage(backgroundImageValue, backgroundImageAspectRatio?): ImageDto<Style> {
        var imageConfig = {
        };

        if (this._layout._metadata === RelativeLayoutSchema.ID) {
            imageConfig['gravity'] = 'bottom';
            imageConfig['width'] = 'matchParent';
            imageConfig['height'] = 'wrapContent';
        }

        if (this._layout._metadata === LinearLayoutSchema.ID) {
            imageConfig['height'] = 'wrapContent';
            imageConfig['width'] = 'matchParent';
        }

        imageConfig['scale'] = 'aspectFit';

        if (backgroundImageValue) {
            imageConfig['value'] = backgroundImageValue;
        }

        if (backgroundImageAspectRatio) {
            imageConfig['aspectRatio'] = backgroundImageAspectRatio;
        }

        return <ImageDto<Style>> ClientAppModelFactory.create('Image', imageConfig);
    }

    public setBackgroundImageWidth(width: string) {
        if (this._backgroundImage) {
            this._backgroundImage.width = width;
        }
    }

    public getBackgroundImage(): ImageDto<Style> {
        return this._backgroundImage;
    }

    private _getLayout(toolbarPosition): LayoutDto {
        var layoutType,
            layoutConfig = {};

        layoutType = 'LinearLayout';


        switch (toolbarPosition) {
            case 'bottom':
                layoutConfig['orientation'] = 'vertical';
                break;
            case 'right':
                layoutConfig['orientation'] = 'horizontal';
                break;
            case 'overlay':
                layoutConfig['orientation'] = 'vertical';
                break;
            default:
                layoutConfig['orientation'] = 'vertical';
        }

        return <LayoutDto>ClientAppModelFactory.create(layoutType, layoutConfig);
    }

    public getContentContainer(): ContainerDto {
        return this._panel;
    }

    public getToolbarContainer(): ContainerDto {
        return this._toolbarContainer;
    }

    public addItem (cmp: ComponentDto<Style>) {
        this._panel.items.push(cmp);
    }

    public addListener (listener: ListenerDto<BaseEvent, BaseAction>) {
        if (listener){
            this._panel.listeners.push(listener);
        }
    }

    public setVisible (visible: string) {
        this._panel.visible = visible;
    }

    public setAspectRatio (ratio: any) {
        if (!isNaN(parseFloat(ratio))) {
            _.extend(this._panel, {
                aspectRatio: ratio
            });
        }
    }

    public setToolbarBackgroundColor(backgroundColor: ColorDto) {
        this._toolbarContainer.style.backgroundColor = backgroundColor;
    }

    public setToolbarWidth(width: string) {
        this._toolbarContainer.width = width;
    }

    public setToolbarHeight(height: string) {
        this._toolbarContainer.height = height;
    }

    public setToolbarLayout(layout: LayoutDto) {
        this._toolbarContainer.layout = layout;
    }

    public addToolbarItem(cmp: ComponentDto<Style>) {
        if (cmp){
            //listview set labelTileContainer width
            if (this._toolbarPosition == 'right') {
                cmp.width = 'fillParent';
            }
            this._panel.items.push(cmp);
        }
    }

    public setLayoutType (type: string) {
        this._layout.type = type;
    }

    public setColumns (columns: number) {
        this._layout.columns = columns;
    }

    public setRows (rows: number) {
        this._layout.rows = rows;
    }

    public setContentContainerPadding (padding: string) {
        if (padding) {
            _.extend(this._panel.style, {
                padding: padding
            });
        }
    }

    public setToolbarContainer(toolBarContainerDto : ContainerDto ) {
        if (toolBarContainerDto){
            _.extend(this._toolbarContainer, toolBarContainerDto);

            //listview set imageTileContainer width
            if (this._toolbarPosition == 'right') {
                this.setToolbarWidth('wrapContent');
            }
        }
    }

    public setStyle (style) {
        this._panel.style = style;

        if (style && style.width) {
            this.setWidth(style.width);
        }

        if (style && style.height) {
            this.setHeight(style.height);
        }
    }
}