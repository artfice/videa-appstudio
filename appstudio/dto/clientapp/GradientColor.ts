import { MetadataOwner } from '../MetadataOwner';
import { OffsetColor } from '../common/OffsetColor';

export interface GradientColor extends MetadataOwner {
    direction: string;
    stops: OffsetColor[];
}