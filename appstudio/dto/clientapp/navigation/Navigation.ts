/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import {MetadataOwner} from '../../MetadataOwner';
import {Section} from './Section';
import {Container} from '../component/Container';


export interface Navigation extends MetadataOwner {
    mainMenu: any;
	section: Section[];
	secondaryMenu: Container;
	tabs?: any[];
	style?: any;
}