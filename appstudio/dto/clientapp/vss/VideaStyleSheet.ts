/**
 * Created by bardiakhosravi on 2015-12-12.
 */

import {MetadataOwner} from '../../MetadataOwner';
import {Rule} from './Rule';
import {Style} from '../../clientapp/Style';

export interface VideaStyleSheet extends MetadataOwner {
    rules: Rule<Style>[];
}