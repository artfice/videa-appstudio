/**
 * Created by bardiakhosravi on 2015-12-12.
 */

import {MetadataOwner} from '../../MetadataOwner';

import {Style} from '../../clientapp/Style';

export interface Rule<T extends Style> extends MetadataOwner {
    selector: string;
    style: T;
}