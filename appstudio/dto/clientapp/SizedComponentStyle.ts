import {Style} from './Style';

export interface SizedComponentStyle extends Style {
    width: string;
    height: string;
}