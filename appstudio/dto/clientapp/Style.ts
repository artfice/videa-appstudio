import { MetadataOwner } from '../MetadataOwner';
import { Color } from '../common/Color';
import { Border } from '../common/Border';
import { GradientColor } from '../common/GradientColor';

export interface Style extends MetadataOwner {
    padding: string;
    margin: string;
    backgroundColor:  Color | GradientColor;
    border: Border;
    aspectRatio?: string;
    
}