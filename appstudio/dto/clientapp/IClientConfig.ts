/**
 * Created by bardiakhosravi on 2016-01-22.
 */

import {MetadataOwner as MetadataOwner} from '../MetadataOwner';
import {UIConfig as UIConfig} from './app/UIConfig';
import {CustomData as CustomData} from '../common/CustomData';
import {Provider as BaseProvider} from '../common/auth/BaseProvider';
import {Cms as CMS} from '../app/configuration/Cms';
import {Analytics as Analytics} from '../app/configuration/uiconfig/Analytics';
import {Chromecast as Chromecast} from './app/Chromecast';
import {WatchHistory as WatchHistory} from './app/WatchHistory';
import {Favorites} from './app/Favorites';
import {User} from './app/User';

export interface IClientConfig<TProvider extends BaseProvider, TCms extends CMS> extends MetadataOwner {
    authentication: TProvider;
    cms: TCms[];
    uiConfigs: UIConfig<Analytics>[];
    custom: CustomData;
    chromecast: Chromecast;
    watchHistory: WatchHistory;
    favorites: Favorites;
    user: User;
}