import {Container} from '../clientapp/component/Container';
export interface NamedComponent extends Container {
    name: string;
}