import {MetadataOwner} from '../../MetadataOwner';

export interface User extends MetadataOwner {
    provider: string;
    accountId: string;
    pid: string;
}