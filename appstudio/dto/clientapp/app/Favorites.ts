import {MetadataOwner} from '../../MetadataOwner';

export interface Favorites extends MetadataOwner {
    provider: string;
    frequency: number;
    uid: string;
    link: string;
    collectionPath: string;
}