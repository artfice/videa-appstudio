import {MetadataOwner} from '../../MetadataOwner';

export interface WatchHistory extends MetadataOwner {
    provider: string;
    completionThreshold: number;
    frequency: number;
    events: string[];
}