import {Theme} from '../app/chromecast/Theme';
import {Title} from '../app/chromecast/Title';
import {Receiver} from '../app/chromecast/Receiver';
import {MetadataOwner} from '../../MetadataOwner';

export interface Chromecast extends MetadataOwner {
    enabled: boolean;
    appId: string;
    theme: Theme;
    title: Title;
    receiver: Receiver;
};