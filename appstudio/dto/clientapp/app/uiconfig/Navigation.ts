import {MetadataOwner} from '../../../MetadataOwner';
import {Section} from '../../app/uiconfig/navigation/Section';
import {Container} from '../../component/Container';

export interface Navigation extends MetadataOwner {
    mainMenu: any;
	section: Section[];
	secondaryMenu: Container;
	tabs?: any[];
	style?: any[];
}