import {MetadataOwner} from '../../../MetadataOwner';
export interface WelcomeScreen extends MetadataOwner {
    screenId: string;
    persistKey: string;
}