import {Component} from '../../../../app/configuration/uiconfig/screen/component/Component';
import {Image} from '../../../../app/configuration/uiconfig/screen/component/field/image/Image';
import {BaseStyle} from '../../../../app/configuration/uiconfig/screen/component/style/BaseStyle';
import {Label} from '../../../../app/configuration/uiconfig/screen/component/field/label/Label';
import {MetadataOwner} from '../../../../MetadataOwner';

export interface Tabs extends Component<BaseStyle> {
    label: Label;
    icon: Image;
}