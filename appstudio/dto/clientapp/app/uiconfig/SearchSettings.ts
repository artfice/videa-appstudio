import {MetadataOwner} from '../../../MetadataOwner';

export interface SearchSettings extends MetadataOwner {
    enabled: boolean;
    name: string;
    screenId: string;
}