/**
 * Created by bardiakhosravi on 2015-11-30.
 */

import {Theme} from '../component/Theme';
import {MetadataOwner} from '../../MetadataOwner';
import {Screen} from '../component/Screen';
import {Navigation} from '../navigation/Navigation';
import {SearchSettings} from '../../app/configuration/uiconfig/SearchSettings';
import {Analytics} from '../../app/configuration/uiconfig/Analytics';
import {Ads} from '../../app/configuration/uiconfig/Ads';
import {WelcomeScreen} from './uiconfig/WelcomeScreen';

export interface UIConfig<TAnalytics extends Analytics> extends MetadataOwner {
    theme: Theme;
    navigation: Navigation;
    screen: Screen[];
    analytics: TAnalytics[];
    videoAdsProvider: Ads;
    search: SearchSettings;
    defaultScreenId: string;
    editionId: string;
    welcomeScreen: WelcomeScreen;
    tvNavigation?: any;
}