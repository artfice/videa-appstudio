import {Style} from '../clientapp/Style';
import {MetadataOwner} from '../MetadataOwner';
import {Listener} from '../common/Listener';
import {BaseEvent} from '../common/listener/event/BaseEvent';
import {BaseAction} from '../common/listener/action/BaseAction';

export interface Component<TStyle extends Style> extends MetadataOwner {
    listeners?: Listener<BaseEvent, BaseAction>[];
    width: string;
    height: string;
    style: TStyle;
    visible: string;
}