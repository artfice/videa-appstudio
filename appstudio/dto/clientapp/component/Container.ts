import {Layout} from '../component/layout/Layout';
import {Component} from '../Component';
import {ContainerStyle} from './ContainerStyle';

export interface Container extends Component<ContainerStyle> {
    style: ContainerStyle;
    layout: Layout;
    items: any[];
    aspectRatio?: string;
}