import {Collection as CollectionDto} from '../component/collection/Collection';
import {Container as ContainerDto} from '../component/Container';
import {DataView as DataViewDto} from '../component/dataview/DataView';
import {Component as ComponentDto} from '../Component';
import {Style as StyleDto} from '../Style';
import {Layout as LayoutDto} from '../component/layout/Layout';
import {GridLayout as GridLayoutDto} from '../component/layout/GridLayout';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';
import _ = require('lodash');

export class DataView<T extends DataViewDto<LayoutDto, ComponentDto<StyleDto>, ContainerDto, CollectionDto>> {
    protected _type: string;
    protected _width: string;
    protected _height: string;
    protected _collection: CollectionDto;
    protected _itemRenderer: ContainerDto;
    protected _style: StyleDto;
    protected _container: T;
    protected _scroll: string;
    protected _visible: string;
    protected _metadata: string;
    protected _layout: GridLayoutDto;

    constructor (config?) {
        this._type = config && config.type ? config.type : 'DataView';
        this._width = config && config.width ? config.width : 'matchParent';
        this._height = config && config.height ? config.height : 'wrapContent';
        this._visible = 'true';
        this._container = <T>ClientAppModelFactory.create(this._type, {
            width: this._width,
            height: this._height,
            visible: this._visible
        });
    }

    public setWidth (width: string) {
        this._container.width = width;
    }

    public setHeight (height: string) {
        this._container.height = height;
    }

    public setCollection (collection: CollectionDto) {
        if (collection) {
            this._container.collection = collection;
        }
    }

    public setItemRenderer (itemRenderer: ContainerDto) {
        if (itemRenderer) {
            this._container.itemRenderer = itemRenderer;
        }
    }

    public setStyle (style : any) {
        if (style) {
            if (!_.isUndefined(style.width)) {
                this.setWidth(style.width);
                delete style.width;
            }
            if (!_.isUndefined(style.height)) {
                this.setHeight(style.height);
                delete style.height;
            }
            this._container.style = style;
        }
    }

    public setScroll(scroll: string) {
        this._container.scroll = scroll;
    }

    public setVisible (visible: string) {
        this._container.visible = visible;
    }

    public setLayout(layoutDto: GridLayoutDto) {
        this._container.layout = layoutDto;
    }

    public setRows(rows: number) {
        if (!this._container.layout) {
            this._container.layout = ClientAppModelFactory.create('GridLayout');
        }
        this._container.layout.rows = rows;
    }

    public setColumns(columns: number) {
        if (!this._container.layout) {
            this._container.layout = <GridLayoutDto>ClientAppModelFactory.create('GridLayout');
        }
        this._container.layout.columns = columns;
    }

    public getContainer(): T {
        return this._container;
    }
}