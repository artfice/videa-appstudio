import {Container as ContainerDto} from '../../component/Container';
import {Image as ImageDto} from '../../component/field/Image';
import {ImageStyle as ImageStyle} from '../../component/field/style/ImageStyle';
import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';

export class SecondaryMenuSection {
    protected _container: ContainerDto;
    protected _images: ImageDto<ImageStyle>[];

    constructor (config?) {

        this._images = [];

        this._container = <ContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('RelativeLayout'),
			width: 'fillParent',
			height: 'wrapContent',
			visible: 'true',
            listeners: [],
			style: ClientAppModelFactory.create('Style', {
				padding: '0 0 0 0',
				margin: '0 0 0 0',
				backgroundColor: ClientAppModelFactory.create('Color', {
					value: '#00000000'
				})
			}),
			items: [
            ]
        });
    }

    public setVisible (visible: string) {
        if (visible) {
            this._container.visible = visible;
        }
    }

    public setPadding (padding: string) {
        if (padding) {
            this._container.style.padding = padding;
        }
    }

    public setMargin (margin: string) {
        if (margin) {
            this._container.style.margin = margin;
        }
    }

    public setBackgroundColor (backgroundColor) {
        if (backgroundColor) {
            this._container.style.backgroundColor = backgroundColor;
        }
    }

    public setListener (listeners) {
        if (listeners) {
            this._container.listeners = listeners;
        }
    }

    public setImages (image) {
        if (image) {
            this._images = image;
            this._container.items = this._images;
        }
    }

    public getContainer(): ContainerDto {
        return this._container;
    }
}