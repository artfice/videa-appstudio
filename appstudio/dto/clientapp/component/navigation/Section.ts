import {Container as ContainerDto} from '../../component/Container';
import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';

export class Section {
    protected _container: ContainerDto;
    protected _imageContainer: ContainerDto;
    protected _labelContainer: ContainerDto;

    constructor (config?) {

        this._imageContainer = <ContainerDto>ClientAppModelFactory.create('Container', {
            width: 'wrapContent',
            height: 'wrapContent',
            style: ClientAppModelFactory.create('Style', {
                padding: '0 0 0 0',
				margin: '0 0 0 0',
				backgroundColor: ClientAppModelFactory.create('Color', {
					value: '#00000000'
				})
            }),
            layout: ClientAppModelFactory.create('RelativeLayout', {
			}),
            items: []
        });

        this._labelContainer = <ContainerDto>ClientAppModelFactory.create('Container', {
            width: 'fillParent',
            height: 'wrapContent',
            style: ClientAppModelFactory.create('Style', {
                padding: '0 0 0 0',
				margin: '0 0 0 0',
				backgroundColor: ClientAppModelFactory.create('Color', {
					value: '#00000000'
				})
            }),
            layout: ClientAppModelFactory.create('RelativeLayout', {
			}),
            items: []
        });

        this._container = <ContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('LinearLayout', {
				orientation: 'horizontal'
			}),
			width: 'matchParent',
			height: 'wrapContent',
			visible: 'true',
            listeners: [],
			style: ClientAppModelFactory.create('Style', {
				padding: '0 0 0 0',
				margin: '0 0 0 0',
				backgroundColor: ClientAppModelFactory.create('Color', {
					value: '#00000000'
				})
			}),
			items: [
                this._imageContainer,
                this._labelContainer
            ]
        });
    }

    public setVisible (visible: string) {
        if (visible) {
            this._container.visible = visible;
        }
    }

    public setWidth (width: string) {
        if (width) {
            this._container.width = width;
        }
    }

    public setHeight (height: string) {
        if (height) {
            this._container.height = height;
        }
    }

    public setPadding (padding: string) {
        if (padding) {
            this._container.style.padding = padding;
        }
    }

    public setMargin (margin: string) {
        if (margin) {
            this._container.style.margin = margin;
        }
    }

    public setBackgroundColor (backgroundColor) {
        if (backgroundColor) {
            this._container.style.backgroundColor = backgroundColor;
        }
    }

    public setListener (listeners) {
        if (listeners) {
            this._container.listeners = listeners;
        }
    }

    public setLabelContainer (label) {
        if (label) {
            this._labelContainer = label;
            this._container.items[1] = this._labelContainer;
        }
    }

    public setImageContainer (image) {
        if (image) {
            this._imageContainer = image;
            this._container.items[0] = this._imageContainer;
        }
    }

    public getContainer(): ContainerDto {
        return this._container;
    }

    public getLabelContainer (): ContainerDto {
        return this._labelContainer;
    }

    public getImageContainer (): ContainerDto {
        return this._imageContainer;
    }
}