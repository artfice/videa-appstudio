import {Container as ContainerDto} from '../../component/Container';
import {Label as LabelDto} from '../../component/field/Label';
import {LabelStyle as LabelStyle} from '../../component/field/style/LabelStyle';
import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';

export class SecondaryMenu {
    protected _container: ContainerDto;
    protected _sections: ContainerDto[];
    protected _labels: LabelDto<LabelStyle>[];

    constructor (config?) {
        this._sections = [];

        this._labels = [];

        this._container = <ContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('LinearLayout', {
				orientation: 'vertical'
			}),
			width: 'matchParent',
			height: 'wrapContent',
			visible: 'true',
            listeners: [],
			style: ClientAppModelFactory.create('Style', {
				padding: '0 0 0 0',
				margin: '0 0 0 0',
				backgroundColor: ClientAppModelFactory.create('Color', {
					value: '#00000000'
				})
			}),
			items: [
            ]
        });
    }

    public setVisible (visible: string) {
        if (visible) {
            this._container.visible = visible;
        }
    }

    public setWidth (width: string) {
        if (width) {
            this._container.width = width;
        }
    }

    public setHeight (height: string) {
        if (height) {
            this._container.height = height;
        }
    }

    public setPadding (padding: string) {
        if (padding) {
            this._container.style.padding = padding;
        }
    }

    public setMargin (margin: string) {
        if (margin) {
            this._container.style.margin = margin;
        }
    }

    public setBackgroundColor (backgroundColor) {
        if (backgroundColor) {
            this._container.style.backgroundColor = backgroundColor;
        }
    }

    public setListener (listeners) {
        if (listeners) {
            this._container.listeners = listeners;
        }
    }

    public addLabels (label) {
        if (label) {
            this._labels = label;
        }
    }

    public setSections (sections) {
        if (sections) {
            this._sections = sections;
        }
    }

    public getContainer(): ContainerDto {
        var items = [];
        items = items.concat(this._labels);
        items = items.concat(this._sections);
        this._container.items = items;
        return this._container;
    }
}