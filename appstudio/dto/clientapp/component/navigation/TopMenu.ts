import {TopNavContainer} from '../../../../dto/clientapp/component/TopNavContainer';
import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';

export class TopMenu {
    protected _container;
    protected _leftSection;
    protected _middleSection;
    protected _rightSection;

    constructor() {
        this._container = ClientAppModelFactory.create('TopMenu', {
            navigationType: {
                name: 'Top',
                value: 'Top'
            },
            item: this._defaultSectionContainer('Relative')
        });

        this._leftSection = this._defaultLeftSectionContainer();
        this._middleSection = this._defaultMiddleSectionContainer();
        this._rightSection = this._defaultRightSectionContainer();
    }

    public setMenuHeight(height: string | number) {
        if (height) {
            this._container.item.height = height;
        }
    }

    public setMenuWidth(width: string | number) {
        if (width) {
            this._container.item.width = width;
        }
    }

    public setMenuPadding(padding: string | number) {
        if (padding) {
            this._container.item.style.padding = padding;
        }
    }

    public setMenuMargin(margin: string | number) {
        if (margin) {
            this._container.item.style.margin = margin;
        }
    }

    public setMenuBackgroundColor(backgroundColor) {
        if (backgroundColor) {
            this._container.item.style.backgroundColor = backgroundColor;
        }
    }

    public setLeftSectionHeight(height: string | number) {
        if (height) {
            this._leftSection.height = height;
        }
    }

    public setLeftSectionWidth(width: string | number) {
        if (width) {
            this._leftSection.width = width;
        }
    }

    public setLeftSectionVisible(visible: string) {
        if (visible) {
            this._leftSection.visible = visible;
        }
    }

    public setLeftSectionPadding(padding: string | number) {
        if (padding) {
            this._leftSection.style.padding = padding;
        }
    }

    public setLeftSectionMargin(margin: string | number) {
        if (margin) {
            this._leftSection.style.margin = margin;
        }
    }

    public setLeftSectionBackgroundColor(backgroundColor) {
        if (backgroundColor) {
            this._leftSection.style.backgroundColor = backgroundColor;
        }
    }

    public setRightSectionHeight(height: string | number) {
        if (height) {
            this._rightSection.height = height;
        }
    }

    public setRightSectionWidth(width: string | number) {
        if (width) {
            this._rightSection.width = width;
        }
    }

    public setRightSectionPadding(padding: string | number) {
        if (padding) {
            this._rightSection.style.padding = padding;
        }
    }

    public setRightSectionMargin(margin: string | number) {
        if (margin) {
            this._rightSection.style.margin = margin;
        }
    }

    public setRightSectionBackgroundColor(backgroundColor) {
        if (backgroundColor) {
            this._rightSection.style.backgroundColor = backgroundColor;
        }
    }

    public setRightSectionVisible(visible: string) {
        if (visible) {
            this._rightSection.visible = visible;
        }
    }

    public setMiddleSectionHeight(height: string | number) {
        if (height) {
            this._middleSection.height = height;
        }
    }

    public setMiddleSectionWidth(width: string | number) {
        if (width) {
            this._middleSection.width = width;
        }
    }

    public setMiddleSectionPadding(padding: string | number) {
        if (padding) {
            this._middleSection.style.padding = padding;
        }
    }

    public setMiddleSectionMargin(margin: string | number) {
        if (margin) {
            this._middleSection.style.margin = margin;
        }
    }

    public setMiddleSectionBackgroundColor(backgroundColor) {
        if (backgroundColor) {
            this._middleSection.style.backgroundColor = backgroundColor;
        }
    }

    public setMiddleSectionVisible(visible: string) {
        if (visible) {
            this._middleSection.visible = visible;
        }
    }

    protected _defaultLeftSectionContainer () {
        const sectionContainer = this._defaultSectionContainer();
        sectionContainer.gravity = 'top|left';
        sectionContainer.visible = 'true';
        return sectionContainer;
    }

    protected _defaultMiddleSectionContainer () {
        const sectionContainer = this._defaultSectionContainer();
        sectionContainer.gravity = 'top|center';
        sectionContainer.visible = 'true';
        return sectionContainer;
    }

    protected _defaultRightSectionContainer () {
        const sectionContainer = this._defaultSectionContainer();
        sectionContainer.gravity = 'top|right';
        sectionContainer.visible = 'true';
        return sectionContainer;
    }

    protected _defaultSectionContainer (layoutType?: string) {
        let layout = <any>{
            type: 'Linear',
            orientation: 'horizontal'
        };

        if (layoutType == 'Relative') {
            layout = {
                type: 'Relative'
            }
        }

        return <any>ClientAppModelFactory.create('Container', {
            layout: layout,
            gravity: 'top',
            height: 'wrapContent',
            width: 'wrapContent',
            style: ClientAppModelFactory.create('Style', {
                padding: '0 0 0 0',
                margin: '0 0 0 0'
            }),
            items: []
        });
    }

    public addAllLeftSectionComponents (components: any[]) {
        if (components && components.length > 0) {
            this._leftSection.items = components;
            this._container.item.items.push(this._leftSection);
        }
    }

    public addAllMiddleSectionComponents (components: any[]) {
        if (components && components.length > 0) {
            this._middleSection.items = components;
            this._container.item.items.push(this._middleSection);
        }
    }

    public addAllRightSectionComponents (components: any[]) {
        if (components && components.length > 0) {
            this._rightSection.items = components;
            this._container.item.items.push(this._rightSection);
        }
    }

    public getContainer() : TopNavContainer {
        return this._container;
    }

}