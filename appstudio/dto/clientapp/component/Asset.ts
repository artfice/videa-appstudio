/**
 * Created by bardiakhosravi on 15-12-02.
 */
import {Image} from '../component/field/Image';
import {ColoredImage} from '../component/field/ColoredImage';
import {Style} from '../Style';

export interface Asset {
    logo: Image<Style>;
    splashScreen: Image<Style>;
    background: ColoredImage;
    navBackground: ColoredImage;
    topBar: ColoredImage;
    appIcon: Image<Style>;
}