/**
 * Created by bardiakhosravi on 15-12-02.
 */

import {Asset} from './Asset';
import {Colors} from '../../app/configuration/uiconfig/theme/Colors';

export interface Theme {
    asset: Asset;
    colors: Colors;
}