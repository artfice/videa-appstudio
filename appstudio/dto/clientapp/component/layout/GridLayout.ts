import {Layout} from './Layout';

export interface GridLayout extends Layout {
    rows: number;
    columns: number;
}