import {Layout} from './Layout';

export interface LinearLayout extends Layout {
    orientation: string;
}