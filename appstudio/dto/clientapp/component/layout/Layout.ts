import {MetadataOwner} from '../../../MetadataOwner';

export interface Layout extends MetadataOwner {
  columns?: any;
  rows?: any;
}