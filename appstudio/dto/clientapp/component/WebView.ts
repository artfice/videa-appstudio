import {Component} from '../Component';
import {Style} from '../Style';

export interface WebView<TStyle extends Style> extends Component<Style> {
    uri: string;
    name: string;
    verticalScrollEnabled: boolean;
    horizontalScrollEnabled: boolean;
    fallbackMargin: string;
    style: TStyle;
}