import {Style} from '../../Style';

export interface TileStyle extends Style {
    aspectRatio: string;
}