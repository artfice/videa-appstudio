import {Color} from '../../../../common/Color';
import {Position} from '../../../../common/Position';
import {SizedComponentStyle} from '../../../SizedComponentStyle';

export interface CarouselStyle extends SizedComponentStyle {
    tileScale: number;
    paginationDots: boolean;
    paginationSelectedDotColors: Color;
    paginationUnselectedDotColors: Color;
    paginationDotPosition: Position;
    paginationLocation: string;
    paginationMargin: string;
}