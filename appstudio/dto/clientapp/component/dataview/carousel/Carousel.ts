import {BaseCollection as Collection} from '../../../../app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';
import {DataView as DataView} from '../../../component/dataview/DataView';
import {Component as Component} from '../../../Component';
import {Container as Container} from '../../../component/Container';
import {Style as Style} from '../../../Style';
import {Layout as Layout} from '../../../component/layout/Layout';

export interface Carousel<TLayout extends Layout,
                   TComponent extends Component<Style>,
                   TContainer extends Container,
                   TCollection extends Collection> extends DataView<Layout,
                                                                    Component<Style>,
                                                                    Container,
                                                                    Collection> {
}