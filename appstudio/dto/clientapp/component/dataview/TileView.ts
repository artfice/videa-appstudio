import {Component} from '../../Component';
import {Container} from '../../component/Container';
import {TileStyle} from './TileStyle';
import {Layout} from '../../component/layout/Layout';
import {Collection} from '../../component/collection/Collection';
import {DataView} from './DataView';

export interface TileView<TLayout extends Layout,
                   TComponent extends Component<TileStyle>,
                   TContainer extends Container,
                   TCollection extends Collection> extends DataView< Layout, Component<TileStyle>, Container, Collection>  {
    layout: TLayout;
	items: TComponent[];
    //itemRenderer
    //collection
    //scroll
    //width
    //height
    //visible
    //style
    //listeners
}