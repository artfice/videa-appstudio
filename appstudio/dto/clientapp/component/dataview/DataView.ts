import {Component as Component} from '../../Component';
import {Container as Container} from '../../component/Container';
import {Style as Style} from '../../Style';
import {Layout as Layout} from '../../component/layout/Layout';
import {BaseCollection as Collection} from '../../../app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';

export interface DataView<TLayout extends Layout,
                   TComponent extends Component<Style>,
                   TContainer extends Container,
                   TCollection extends Collection> extends Component<Style> {
    type: string;
    itemRenderer: TContainer;
    collection: TCollection;
    scroll?: string;
    layout?: any;
    //width
    //height
    //visible
    //style
    //listeners
}