import { SizedComponentStyle } from '../../../SizedComponentStyle';
import { Color } from '../../../../common/Color';
import { Font } from '../../../../common/Font';

export interface PickerStyle extends SizedComponentStyle {
    backgroundColor: Color;
    font: Font;

}