import {MetadataOwner} from '../../../../MetadataOwner';
import {PickerStyle} from './PickerStyle';
import {Color} from '../../../../common/Color';

export interface PickerRenderer extends MetadataOwner {
    displayName: string;
    width: string;
    height: string;
    style: PickerStyle;
    backgroundColor: Color;
}