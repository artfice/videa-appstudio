import {BaseCollection as Collection} from '../../../../app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';
import {DataView as DataView} from '../../../component/dataview/DataView';
import {Component as Component} from '../../../Component';
import {Container as Container} from '../../../component/Container';
import {Style as Style} from '../../../Style';
import {Layout as Layout} from '../../../component/layout/Layout';
import {PickerRenderer as PickerRenderer} from './PickerRenderer';

export interface CollectionView extends DataView<Layout,
                                Component<Style>,
                                Container,
                                Collection> {
pickerRenderer: PickerRenderer;
}