/**
 * Created by bardiakhosravi on 2015-12-07.
 */

import {Component} from '../Component';
import {Style} from '../Style';
import {NamedComponent} from '../NamedComponent';

export interface Screen extends NamedComponent {
    items: Component<Style>[];
    scroll?: string;
    title: string;
}