import {Component as ComponentDto} from '../Component';
import {Container as ContainerDto} from './Container';
import {Style as StyleDto} from '../Style';

import {Space as SpaceDto} from '../Space';

import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';
import VerticalLayout = require('../../../schema/common/layout/VerticalLayout');
import RelativeLayout = require('../../../schema/common/layout/RelativeLayout');
import HorizontalLayout = require('../../../schema/common/layout/HorizontalLayout')

export class Layer {
    protected _container: ContainerDto | SpaceDto;
    protected _isSpace: boolean;

    constructor(numItems) {
        if (numItems > 0) {
            this._isSpace = false;
            this._container = <ContainerDto>ClientAppModelFactory.create('Container', {
                layout: {
                    type: 'Relative'
                }
            });
        } else {
            this._isSpace = true;
            this._container = <SpaceDto>ClientAppModelFactory.create('Space');
        }
    }

    public setItems(components: any): void {
        if (components && !this._isSpace) {
            this._container.items = components;
        }
    }
    public getContainer(): ContainerDto | SpaceDto {
        return this._container;
    }

    public setLayout(layout): void {
        if (layout && layout._metadata && !this._isSpace) {
            switch (layout._metadata) {
                case VerticalLayout.ID:
                    this._container.layout = {
                        type: 'Linear',
                        orientation: 'Vertical'
                    };
                    break;
                case HorizontalLayout.ID:
                    this._container.layout = {
                        type: 'Linear',
                        orientation: 'Horizontal'
                    };
                    break;
                case RelativeLayout.ID:
                    this._container.layout  = {
                        type: 'Relative'
                    };
                    break;
            
                default:
                    break;
            }
        }
    }
}