/**
 * Created by bardiakhosravi on 2016-02-09.
 */

import {Component} from '../Component';
import {Style} from '../Style';

export interface DropDown extends Component<Style> {
	value: any;
	model: any;
}