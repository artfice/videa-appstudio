import {DataView as DataView} from './DataView';
import {PickerRenderer as PickerRendererDto} from './dataview/collectionview/PickerRenderer';
import {CollectionView as CollectionViewDto} from './dataview/collectionview/CollectionView';

export class CollectionView extends DataView<CollectionViewDto> {
    protected _pickerRenderer: PickerRendererDto;

    constructor (config?) {
        super(config);
    }

    public setPickerRenderer (pickerRenderer: PickerRendererDto) {
        this._container.pickerRenderer = pickerRenderer;
    }

    public _getContainer(): CollectionViewDto {
        return this._container;
    }

}