import {Component} from '../../Component';
import {Style} from '../../Style';

export interface BaseField<TStyle extends Style> extends Component<Style> {
    value: string;
    gravity: string;
    style: TStyle;
}