import {BaseField} from './BaseField';
import {LabelStyle} from '../../component/field/style/LabelStyle';

export interface Label<TStyle extends LabelStyle> extends BaseField<LabelStyle> {
    numberOfLines: number;
    style: TStyle;
}