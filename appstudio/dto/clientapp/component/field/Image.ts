import {BaseField} from './BaseField';
import {Style} from '../../Style';

export interface Image<T extends Style> extends BaseField<Style> {
    aspectRatio?: string;
    scale?: string;
}