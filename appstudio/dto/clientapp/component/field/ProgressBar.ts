import {MetadataOwner} from '../../../MetadataOwner';
import {ProgressBarStyle} from './style/ProgressBarStyle';

export interface ProgressBar extends MetadataOwner {
    value: string;
    gravity: string;
    width: string;
    height: string;
    style: ProgressBarStyle;
}
