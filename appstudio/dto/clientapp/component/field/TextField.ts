import {BaseField} from './BaseField';
import {LabelStyle} from '../../component/field/style/LabelStyle';

export interface TextField<TStyle extends LabelStyle> extends BaseField<LabelStyle> {
    numberOfLines: number;
    hint: string;
    inputType: string;
    style: TStyle;
}