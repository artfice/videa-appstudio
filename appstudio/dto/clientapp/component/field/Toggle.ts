import {BaseField} from './BaseField';
import {ToggleStyle} from '../../component/field/style/ToggleStyle';

export interface Toggle<TStyle extends ToggleStyle> extends BaseField<ToggleStyle> {
    style: TStyle;
}