
/**
 * Created by bardiakhosravi on 15-12-03.
 */

import {Color} from '../../../common/Color';
import {Image} from './Image';
import {Style} from '../../Style';

export interface ColoredImage extends Image<Style> {
    color: Color;
}