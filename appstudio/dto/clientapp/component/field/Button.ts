/**
 * Created by bardiakhosravi on 2016-01-26.
 */

///<reference path="../../../../../typings/index.d.ts"/>

import {Container as ContainerDto} from '../../component/Container';
import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';


export class Button {

	private _button: ContainerDto;
	private _buttonLabel: any;

	constructor (config) {
		this._buttonLabel = ClientAppModelFactory.create('Label', {
			height: 'wrapContent',
			width: 'wrapContent',
			numberOfLines: 1,
			value: config && config.value
		});

		this._button = <ContainerDto> ClientAppModelFactory.create('Container', {
			items: [
				this._buttonLabel
			]
		});
	}

	public getRaw(): ContainerDto {
		return this._button;
	}

}