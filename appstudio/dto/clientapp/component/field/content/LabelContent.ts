import {FieldContent} from './FieldContent';

export interface LabelContent extends FieldContent {
    numberOfLines: number;
}