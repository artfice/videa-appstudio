import {MetadataOwner} from '../../../../MetadataOwner';

export interface FieldContent extends MetadataOwner {
    value: string;
}