import {Style} from '../../../Style';

export interface BaseFieldStyle extends Style {
    width: number;
    height: number;
    gravity: string;
}