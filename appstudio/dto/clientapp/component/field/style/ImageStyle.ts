import {BaseFieldStyle} from './BaseFieldStyle';

export interface ImageStyle extends BaseFieldStyle {
    scale: number;
    aspectRatio: string;
}