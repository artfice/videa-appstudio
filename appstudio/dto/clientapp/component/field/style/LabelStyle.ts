import {Font} from '../../../../common/Font';
import {BaseFieldStyle} from './BaseFieldStyle';

export interface LabelStyle extends BaseFieldStyle {
    font: Font;
}