import { MetadataOwner } from '../../../../MetadataOwner';
import { Color } from '../../../../common/Color';

export interface ProgressBarStyle extends MetadataOwner {
    padding: string;
    margin: string;
    backgroundColor: Color;
    foregroundColor: Color;
}