import {BaseFieldStyle} from './BaseFieldStyle';
import {Color} from '../../../../common/Color';

export interface ToggleStyle extends BaseFieldStyle {
    activatedColor: Color;
    deactivatedColor: Color;
}