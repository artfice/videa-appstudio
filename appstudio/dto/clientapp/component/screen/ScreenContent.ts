import {MetadataOwner} from '../../../MetadataOwner';

export interface ScreenContent extends MetadataOwner {
    title: string;
}