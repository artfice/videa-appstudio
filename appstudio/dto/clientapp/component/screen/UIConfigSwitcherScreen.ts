/**
 * Created by bardiakhosravi on 2016-01-25.
 */

import {Screen as ScreenDto} from '../../component/Screen';
import {Container as ContainerDto} from '../../component/Container';
import {LinearLayout as LinearLayoutDto} from '../../component/layout/LinearLayout';
import {Layout as LayoutDto} from '../../component/layout/Layout';
import {Component as ComponentDto} from '../../Component';
import {Label as LabelDto} from '../../component/field/Label';
import {LabelStyle as LabelStyle} from '../../component/field/style/LabelStyle';
import {Style as Style} from '../../Style';

import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';

export class UIConfigSwitcherScreen {

    private _screen: ScreenDto;
    private _screenLayout: LayoutDto;
    private _items: ComponentDto<Style>[];
    private _welcomeLabel: LabelDto<LabelStyle>;
    private _headerLabel: LabelDto<LabelStyle>;
    private _signInButton: ContainerDto;

    constructor() {

        this._screenLayout = <LinearLayoutDto> ClientAppModelFactory.create('LinearLayout', {
            orientation: 'vertical'
        });

        this._welcomeLabel = <LabelDto<LabelStyle>> ClientAppModelFactory.create('Label', {
            value: 'Welcome'
        });

        this._headerLabel = <LabelDto<LabelStyle>> ClientAppModelFactory.create('Label', {
            height: 'wrapContent',
            width: 'wrapContent',
            numberOfLines: 1,
            value: 'Please sign in or select a brand to see a preview'
        });

        this._signInButton = <ContainerDto> ClientAppModelFactory.create('Label', {
            height: 'wrapContent',
            width: 'wrapContent',
            numberOfLines: 1,
            value: 'SIGN IN'
        });

        this._items = [
            this._welcomeLabel,
            this._headerLabel,
            this._signInButton
        ];

        this._screen = <ScreenDto> {
            layout: this._screenLayout,
            items: this._items
        }

    }

    getRaw(): ScreenDto {
        return this._screen;
    }
}