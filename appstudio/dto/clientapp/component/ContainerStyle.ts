import {Style} from '../Style';
import {Color} from '../../common/Color';
import {GradientColor} from '../../common/GradientColor';

export interface ContainerStyle extends Style {
    backgroundColor: Color  | GradientColor;
}