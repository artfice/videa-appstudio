import {MetadataOwner} from '../../MetadataOwner';
import {Container} from './Container';

export interface TopNavContainer extends MetadataOwner {
    item: Container;
}