import {MetadataOwner} from './MetadataOwner';

export interface App extends MetadataOwner {
    name: string;
    activeEdition: string;
    clientConfig: string;
    brandId: string;
    edition: string[];
}