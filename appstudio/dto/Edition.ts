import {Configuration} from './app/Configuration';

export interface Edition extends Configuration {
    name: string;
    configuration: Configuration;
    activeConfig: string;
    clientConfig: string;
    brandId: string;
    state: string;
    appId: string;
    clientConfigId: string;
}