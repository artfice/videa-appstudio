import { ClientConfigRepository } from '../../repository/ClientConfigRepository';
import { AppRepository } from '../../repository/AppRepository';

export interface PublishedClientConfigApplicationServiceConfig {
    clientConfigRepository: ClientConfigRepository;
    appRepository: AppRepository;
}