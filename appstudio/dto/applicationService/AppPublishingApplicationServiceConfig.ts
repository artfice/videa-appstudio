import { EditionRepository } from '../../repository/EditionRepository';
import { AppRepository } from '../../repository/AppRepository';
import { ClientConfigRepository } from '../../repository/ClientConfigRepository';
import { ITranslationService } from '../../interface/ITranslationService';

export interface IAppPublishingApplicationServiceConfig {
    editionRepository: EditionRepository;
    clientConfigRepository: ClientConfigRepository;
    appRepository: AppRepository;
    translationService: ITranslationService;
}