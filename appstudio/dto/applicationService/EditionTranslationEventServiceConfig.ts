import { UIConfigRepository } from '../../repository/UIConfigRepository';
import { ScreenRepository } from '../../repository/ScreenRepository';
import { EditionRepository } from '../../repository/EditionRepository';
import { AppRepository } from '../../repository/AppRepository';
import { AppPublishingApplicationService } from '../../application/AppPublishingApplicationService';

export interface EditionTranslationEventServiceConfig {
    uiConfigRepository: UIConfigRepository;
    screenRepository: ScreenRepository;
    editionRepository: EditionRepository;
    appRepository: AppRepository;
    appPublishingService: AppPublishingApplicationService;
}