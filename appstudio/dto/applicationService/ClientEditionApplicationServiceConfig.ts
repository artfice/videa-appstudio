import { EditionRepository } from '../../repository/EditionRepository';
import { BrandRepository } from '../../repository/BrandRepository';
import { AppRepository } from '../../repository/AppRepository';
import { EditionStateApplicationService } from '../../application/EditionStateApplicationService';

export interface ClientEditionApplicationServiceConfig {
    editionRepository: EditionRepository;
    appRepository: AppRepository;
    brandRepository: BrandRepository;
    editionStateService: EditionStateApplicationService;
}