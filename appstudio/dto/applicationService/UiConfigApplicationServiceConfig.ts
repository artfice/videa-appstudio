import { EditionRepository } from '../../repository/EditionRepository';
import { UIConfigRepository } from '../../repository/UIConfigRepository';
import { ScreenRepository } from '../../repository/ScreenRepository';
import { IEventRepository } from 'videa-framework/repository/IEventRepository';

export interface IUiConfigApplicationServiceConfig {
    uiConfigRepository: UIConfigRepository;
    screenRepository: ScreenRepository;
    editionRepository: EditionRepository;
    eventRepository: IEventRepository;
}