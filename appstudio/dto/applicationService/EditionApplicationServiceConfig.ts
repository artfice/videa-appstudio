import { UIConfigRepository } from '../../repository/UIConfigRepository';
import { ScreenRepository } from '../../repository/ScreenRepository';
import { EditionRepository } from '../../repository/EditionRepository';
import { ClientConfigRepository } from '../../repository/ClientConfigRepository';
import { AppRepository } from '../../repository/AppRepository';
import { EditionStateApplicationService } from '../../application/EditionStateApplicationService';
import { AppPublishingApplicationService } from '../../application/AppPublishingApplicationService';

export interface EditionApplicationServiceConfig {
    uiConfigRepository: UIConfigRepository;
    screenRepository: ScreenRepository;
    editionRepository: EditionRepository;
    clientConfigRepository: ClientConfigRepository;
    appRepository: AppRepository;
    editionStateApplicationService: EditionStateApplicationService;
    appPublishingApplicationService: AppPublishingApplicationService;
}