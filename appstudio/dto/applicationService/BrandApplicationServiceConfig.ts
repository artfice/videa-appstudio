import { BrandRepository } from '../../repository/BrandRepository';
import { AppRepository } from '../../repository/AppRepository';
import { GalleryImageRepository } from '../../repository/GalleryImageRepository';
import { IImageRepository } from 'videa-framework/repository/IImageRepository';
import { AppApplicationService } from '../../application/AppApplicationService';

export interface BrandApplicationServiceConfig {
  brandRepository: BrandRepository;
  appRepository: AppRepository;
  appService: AppApplicationService;
  galleryImageRepository: GalleryImageRepository;
  imageRepository: IImageRepository;
}