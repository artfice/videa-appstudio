import { UIConfigRepository } from '../../repository/UIConfigRepository';
import { EditionRepository } from '../../repository/EditionRepository';
import { AppRepository } from '../../repository/AppRepository';
import { ClientConfigRepository } from '../../repository/ClientConfigRepository';
import { BrandRepository } from '../../repository/BrandRepository';
import { ScreenRepository } from '../../repository/ScreenRepository';
import {EditionApplicationService} from "../../application/EditionApplicationService";

export interface AppApplicationServiceConfig {
    uiConfigRepository: UIConfigRepository;
    appRepository: AppRepository;
    editionRepository: EditionRepository;
    brandRepository: BrandRepository;
    clientConfigRepository: ClientConfigRepository;
    screenRepository: ScreenRepository;
    editionApplicationService: EditionApplicationService;
}
