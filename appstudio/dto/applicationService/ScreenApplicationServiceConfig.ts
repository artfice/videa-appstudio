import { UIConfigRepository } from '../../repository/UIConfigRepository';
import { ScreenRepository } from '../../repository/ScreenRepository';
import { IEventRepository } from 'videa-framework/repository/IEventRepository';

export interface ScreenApplicationServiceConfig {
    uiConfigRepository: UIConfigRepository;
    screenRepository: ScreenRepository;
    eventRepository: IEventRepository;
}