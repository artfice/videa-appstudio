import { Provider as BaseProvider } from '../../common/auth/BaseProvider';
import { MetadataOwner } from '../../MetadataOwner';

export interface Authentication<TProvider extends BaseProvider> extends MetadataOwner {
    provider: TProvider;
}