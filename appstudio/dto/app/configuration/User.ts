import { MetadataOwner } from '../../MetadataOwner';
import { BaseProvider } from './user/BaseProvider';

export interface User<T extends BaseProvider> extends MetadataOwner {
    provider: T;
}
