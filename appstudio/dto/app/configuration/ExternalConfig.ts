import {MetadataOwner} from '../../MetadataOwner';

export interface ExternalConfig extends MetadataOwner {
    name: string;
    url: string;
};