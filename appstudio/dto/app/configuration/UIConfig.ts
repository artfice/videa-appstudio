import { Theme } from '../../app/configuration/uiconfig/Theme';
import { Navigation } from '../../app/configuration/uiconfig/Navigation';
import { Ads } from '../../app/configuration/uiconfig/Ads';
import { Analytics } from '../../app/configuration/uiconfig/Analytics';
import { SearchSettings } from '../../app/configuration/uiconfig/SearchSettings';
import { UISettings } from '../../app/configuration/uiconfig/UISettings';
import { MetadataOwner } from '../../MetadataOwner';
import { BaseMenu } from '../../common/navigation/BaseMenu';

export interface UIConfig<TMenu extends BaseMenu> extends MetadataOwner {
    name: string;
    theme: Theme;
    screen: any[];
    navigation: Navigation<TMenu>;
    ads: Ads;
    analytics: Analytics[];
    searchSettings: SearchSettings;
    uiSettings: UISettings;
    editionId: string;
}