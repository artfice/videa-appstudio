import {MetadataOwner} from '../../MetadataOwner';
import {BaseProvider} from './favorites/BaseProvider';

export interface Favorites<T extends BaseProvider> extends MetadataOwner {
    provider: T;
}