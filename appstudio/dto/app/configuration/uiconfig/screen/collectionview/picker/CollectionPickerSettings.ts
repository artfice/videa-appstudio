import {PickerContent} from './PickerContent';
import {PickerStyle} from './PickerStyle';
import {MetadataOwner} from '../../../../../../MetadataOwner';
import {BaseCollection} from '../../../../../../common/collection/BaseCollection';

export interface CollectionPickerSettings extends MetadataOwner {
    content: PickerContent<BaseCollection>;
    style: PickerStyle;
}