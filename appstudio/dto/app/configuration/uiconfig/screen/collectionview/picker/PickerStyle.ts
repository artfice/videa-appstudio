import {SizedComponentStyle} from '../../component/style/SizedComponentStyle';
import {Font} from '../../../../../../common/Font';
import {Color} from '../../../../../../common/Color';
import {GradientColor} from '../../../../../../common/GradientColor';

export interface PickerStyle extends SizedComponentStyle {
    backgroundColor: Color | GradientColor;
    font: Font;
}