import {MetadataOwner} from '../../../../../../MetadataOwner';
import {BaseCollection} from '../../../../../../common/collection/BaseCollection';

export interface PickerContent<TCollection extends BaseCollection> extends MetadataOwner {
    collection: TCollection;
    type: string;
    displayName: string;
}