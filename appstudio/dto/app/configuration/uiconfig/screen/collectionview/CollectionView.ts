import {Component} from '../component/Component';
import {ComponentStyle} from '../component/style/ComponentStyle';
import {CollectionPickerSettings} from '../collectionview/picker/CollectionPickerSettings';
import {BaseCollection} from '../../../../../common/collection/BaseCollection';
import {GridView} from '../dataview/gridview/GridView';
import {SizedComponentStyle} from '../component/style/SizedComponentStyle';
import {ComponentControls} from '../component/ComponentControls';
import {BaseEvent} from '../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../common/listener/action/BaseAction';

export interface CollectionView extends Component<ComponentStyle> {
    picker: CollectionPickerSettings;
    gridview: GridView<BaseCollection, SizedComponentStyle, ComponentControls<BaseEvent, BaseAction>>;
}