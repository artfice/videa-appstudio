import {SizedComponentStyle} from '../component/style/SizedComponentStyle';

export interface WebViewStyle extends SizedComponentStyle {
    verticalScrollEnabled: boolean;
    horizontalScrollEnabled: boolean;
    fallbackMargin: string;
}