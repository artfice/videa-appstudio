import {NamedComponent} from '../component/NamedComponent';
import {BaseStyle} from '../component/style/BaseStyle';
import {WebViewContent} from './WebViewContent';
import {WebViewStyle} from './WebViewStyle';

export interface WebView extends NamedComponent<BaseStyle> {
    content: WebViewContent;
    style: WebViewStyle;
}