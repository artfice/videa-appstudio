import {MetadataOwner} from '../../../../../MetadataOwner';

export interface WebViewContent extends MetadataOwner {
    uri: string;
}