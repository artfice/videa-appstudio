import {MetadataOwner} from '../../../../MetadataOwner';

export interface BackgroundImage extends MetadataOwner {
    value: string;
}