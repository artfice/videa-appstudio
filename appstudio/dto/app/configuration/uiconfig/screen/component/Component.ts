import { MetadataOwner } from '../../../../../MetadataOwner';
import { BaseStyle } from './style/BaseStyle';
import { ComponentControls } from './ComponentControls';
import { BaseEvent } from '../../../../../common/listener/event/BaseEvent';
import { BaseAction } from '../../../../../common/listener/action/BaseAction';

export interface Component<TStyle extends BaseStyle> extends MetadataOwner {
    style: TStyle;
    controls: ComponentControls<BaseEvent, BaseAction>;
}