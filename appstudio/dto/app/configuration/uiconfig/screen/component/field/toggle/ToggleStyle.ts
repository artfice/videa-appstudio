import {Color} from '../../../../../../../common/Color';
import {BaseFieldStyle} from '../../../component/field/BaseFieldStyle';

export interface ToggleStyle extends BaseFieldStyle {
    activatedColor: Color;
    deactivatedColor: Color;
}