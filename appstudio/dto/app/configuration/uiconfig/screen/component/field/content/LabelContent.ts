import {FieldContent} from '../../../component/field/FieldContent';

export interface LabelContent extends FieldContent<string> {
    numberOfLines: number;
    maxLines: number;
    link: string;
}