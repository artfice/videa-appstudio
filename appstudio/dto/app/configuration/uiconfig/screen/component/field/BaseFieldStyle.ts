import {SizedComponentStyle} from '../style/SizedComponentStyle';

export interface BaseFieldStyle extends SizedComponentStyle {
    gravity: string;
}