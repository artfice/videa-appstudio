import {BaseField} from '../../../component/field/BaseField';
import {ToggleStyle} from '../../../component/field/toggle/ToggleStyle';

export interface Toggle extends BaseField<ToggleStyle> {
    style: ToggleStyle;
}