import {Scale} from '../../../../../../../common/Scale';
import {BaseFieldStyle} from '../../../component/field/BaseFieldStyle';

export interface ImageStyle extends BaseFieldStyle {
    scale: Scale;
    aspectRatio: string;
}