import { Color } from '../../../../../../../common/Color';
import { BaseFieldStyle } from '../../../../../../../app/configuration/uiconfig/screen/component/field/BaseFieldStyle';

export interface ProgressBarStyle extends BaseFieldStyle {
    backgroundColor: Color;
    foregroundColor: Color;
}