import {BaseField} from '../../../component/field/BaseField';
import {ImageStyle} from '../../../component/field/image/ImageStyle';
import {ImageContent} from '../../../component/field/content/ImageContent';

export interface Image extends BaseField<ImageStyle> {
    content: ImageContent;
    style: ImageStyle;
}