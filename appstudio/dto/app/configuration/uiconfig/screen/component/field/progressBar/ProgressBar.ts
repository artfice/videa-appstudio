import { MetadataOwner } from '../../../../../../../MetadataOwner';
import { ProgressBarStyle } from './ProgressBarStyle';
import { FieldContent } from '../FieldContent';

export interface ProgressBar extends MetadataOwner {
    content: FieldContent<String>;
    style: ProgressBarStyle;
}