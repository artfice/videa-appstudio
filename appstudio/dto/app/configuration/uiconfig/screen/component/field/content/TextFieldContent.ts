import {LabelContent} from './LabelContent';

export interface TextFieldContent extends LabelContent {
    hint: string;
    inputType: string;
    componentId: string;
}