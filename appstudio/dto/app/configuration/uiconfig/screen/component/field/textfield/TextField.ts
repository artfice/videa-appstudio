import {BaseField} from '../../../component/field/BaseField';
import {TextFieldStyle} from '../../../component/field/textfield/TextFieldStyle';
import {TextFieldContent} from '../../../component/field/content/TextFieldContent';

export interface TextField extends BaseField<TextFieldStyle> {
    content: TextFieldContent;
    style: TextFieldStyle;
}