import {Font} from '../../../../../../../common/Font';
import {BaseFieldStyle} from '../../../component/field/BaseFieldStyle';

export interface LabelStyle extends BaseFieldStyle {
    font: Font;
}