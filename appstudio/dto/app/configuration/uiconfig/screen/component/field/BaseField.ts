import {Component} from '../Component';
import {BaseFieldStyle} from './BaseFieldStyle';
import {FieldContent} from './FieldContent';

export interface BaseField<TStyle extends BaseFieldStyle> extends Component<BaseFieldStyle> {
    content: FieldContent<any>;
    style: TStyle;
}