import {BaseField} from '../../../../screen/component/field/BaseField';
import {LabelStyle} from './LabelStyle';
import {LabelContent} from '../../../../screen/component/field/content/LabelContent';

export interface Label extends BaseField<LabelStyle> {
    content: LabelContent;
    style: LabelStyle;
}