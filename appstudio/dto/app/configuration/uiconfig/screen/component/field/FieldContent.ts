import {MetadataOwner} from '../../../../../../MetadataOwner';

export interface FieldContent<T> extends MetadataOwner {
    value: T;
}