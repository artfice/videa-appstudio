import {ComponentControls as ComponentControls} from './ComponentControls';
import {BaseEvent as BaseEvent} from '../../../../../common/listener/event/BaseEvent';
import {BaseAction as BaseAction} from '../../../../../common/listener/action/BaseAction';

import {HorizontalLayout} from '../../../../../common/layout/HorizontalLayout';
import {VerticalLayout} from '../../../../../common/layout/VerticalLayout';
import {RelativeLayout} from '../../../../../common/layout/RelativeLayout';

export interface LayerControls extends ComponentControls<BaseEvent, BaseAction> {
    layout: HorizontalLayout | VerticalLayout | RelativeLayout;
}