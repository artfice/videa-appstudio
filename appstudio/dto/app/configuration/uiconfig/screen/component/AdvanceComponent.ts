import {MetadataOwner} from '../../../../../MetadataOwner';
import {CustomData} from '../../../../../common/CustomData';

export interface AdvanceComponent extends MetadataOwner {
    customData: CustomData;
}