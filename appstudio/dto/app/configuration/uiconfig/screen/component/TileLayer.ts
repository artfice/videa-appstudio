import {Component} from './Component';
import { BaseStyle } from './style/BaseStyle';

export interface TileLayer<T extends BaseStyle> extends Component<BaseStyle> {
    style: T;
    subComponent: Component<BaseStyle>[];
}
