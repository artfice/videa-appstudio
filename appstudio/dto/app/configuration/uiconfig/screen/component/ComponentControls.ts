import { MetadataOwner } from '../../../../../MetadataOwner';
import { Listener } from '../../../../../common/Listener';
import { BaseEvent } from '../../../../../common/listener/event/BaseEvent';
import { BaseAction } from '../../../../../common/listener/action/BaseAction';

export interface ComponentControls<TEvent extends BaseEvent, TAction extends BaseAction> extends MetadataOwner {
    visible: string;
    listeners: Listener<TEvent, TAction>[];
}