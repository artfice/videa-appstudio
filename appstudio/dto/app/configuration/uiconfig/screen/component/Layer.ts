import {Component} from './Component';
import { BaseStyle } from './style/BaseStyle';

export interface Layer<T extends BaseStyle> extends Component<BaseStyle> {
    style: T;
    subComponent: Component<BaseStyle>[];
}