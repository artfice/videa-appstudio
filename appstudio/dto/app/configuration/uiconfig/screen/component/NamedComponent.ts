import { Component } from './Component';
import { BaseStyle } from '../component/style/BaseStyle';

export interface NamedComponent<T extends BaseStyle> extends Component<BaseStyle> {
    name: string;
    style: T;
}