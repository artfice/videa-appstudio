import {ComponentStyle} from './ComponentStyle';

export interface SizedComponentStyle extends ComponentStyle {
    width: any;
    height: any;
}