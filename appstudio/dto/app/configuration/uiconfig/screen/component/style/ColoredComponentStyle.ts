import { ComponentStyle } from './ComponentStyle';
import { Color } from '../../../../../../common/Color';
import { GradientColor } from '../../../../../../common/GradientColor';

export interface ColoredComponentStyle extends ComponentStyle {
    backgroundColor: Color | GradientColor;
}