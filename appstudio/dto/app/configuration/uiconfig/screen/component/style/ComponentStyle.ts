import { BaseStyle } from './BaseStyle';
import { Border } from '../../../../../../common/Border';
import {BoxParams} from '../../../../../../common/BoxParams';

export interface ComponentStyle extends BaseStyle {
    margin: BoxParams;
    padding: BoxParams;
    border: Border;
}