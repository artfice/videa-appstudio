import {MetadataOwner} from '../../../../MetadataOwner';
import {Menu} from '../navigation/Menu';
import {TopBar} from '../navigation/TopBar';

export interface NavigationStyle extends MetadataOwner {
    menu: Menu;
    topBar: TopBar;
}