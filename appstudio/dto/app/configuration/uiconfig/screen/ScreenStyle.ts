import { BackgroundImage } from './BackgroundImage';
import { Color } from '../../../../common/Color';
import { ComponentStyle } from '../screen/component/style/ComponentStyle';
import { NavigationStyle } from './NavigationStyle';
import { GradientColor } from '../../../../common/GradientColor';

export interface ScreenStyle extends ComponentStyle {
    backgroundColor: Color | GradientColor;
    backgroundImage: BackgroundImage;
    navigation: NavigationStyle;
}