import {MetadataOwner} from '../../../../../MetadataOwner';
import {BaseCollection} from '../../../../../common/collection/BaseCollection';

export interface DataViewContent<T extends BaseCollection> extends MetadataOwner {
    collection: T;
}