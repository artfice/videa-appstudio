import {MetadataOwner} from '../../../../../MetadataOwner';
import {DataViewHeadingSettings} from '../dataview/heading/DataViewHeadingSettings';
import {Label} from '../component/field/label/Label';

export interface DataViewHeading extends MetadataOwner {
    settings: DataViewHeadingSettings;
    text: Label[];
}