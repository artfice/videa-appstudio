import {Component} from '../component/Component';
import {SizedComponentStyle} from '../component/style/SizedComponentStyle';
import {BaseCollection} from '../../../../../common/collection/BaseCollection';
import {DataViewTile} from './DataViewTile';
import {DataViewHeading} from './DataViewHeading';
import {ComponentControls} from '../component/ComponentControls';
import {BaseEvent} from '../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../common/listener/action/BaseAction';
import {DataViewContent} from '../dataview/DataViewContent';


export interface DataView<TContent extends BaseCollection, TStyle extends SizedComponentStyle, TControls extends ComponentControls<BaseEvent, BaseAction>> extends Component<SizedComponentStyle> {
    content: DataViewContent<BaseCollection>;
    style: TStyle;
    controls: TControls;
    heading: DataViewHeading;
    tile: DataViewTile;
}