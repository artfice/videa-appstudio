import {MetadataOwner} from '../../../../../../../MetadataOwner';

export interface BaseProvider extends MetadataOwner {
    collectionPath: string;
    customProvider? : string;
}