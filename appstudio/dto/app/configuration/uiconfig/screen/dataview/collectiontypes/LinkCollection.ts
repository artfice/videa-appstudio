import {BaseCollection} from './BaseCollection';

export interface LinkCollection extends BaseCollection {
    link: string;
}