import {LinkCollection} from './LinkCollection';
import {Range} from "../../../../../../Range";

export interface RemoteCollection extends LinkCollection {
    range: Range;
}