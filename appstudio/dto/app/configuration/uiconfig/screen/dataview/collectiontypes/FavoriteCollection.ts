import {Range} from '../../../../../../Range';
import {BaseCollection} from './BaseCollection';

export interface FavoriteCollection extends BaseCollection {
    range: Range;
}