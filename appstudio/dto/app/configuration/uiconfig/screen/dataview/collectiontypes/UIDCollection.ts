import {LinkCollection} from './LinkCollection';

export interface UIDCollection extends LinkCollection {
    path: string;
}