import {Range} from '../../../../../../Range';
import {BaseCollection} from './BaseCollection';

export interface WatchHistoryCollection extends BaseCollection {
    group: string;
    range: Range;
}