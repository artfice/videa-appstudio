import {BaseProvider} from './BaseProvider';

export interface JsonProvider extends BaseProvider {
  collectionPath: string;
}