import {MetadataOwner} from '../../../../../../MetadataOwner';
import {BaseProvider} from './provider/BaseProvider';

export interface BaseCollection extends MetadataOwner {
    provider: BaseProvider;
}