import {MetadataOwner} from '../../../../../../MetadataOwner';
import {DataViewTileStyle} from './DataViewTileStyle';
import {ComponentControls} from '../../component/ComponentControls';
import {BaseEvent} from '../../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../../common/listener/action/BaseAction';

export interface DataViewTileSettings extends MetadataOwner {
    style: DataViewTileStyle;
    controls: ComponentControls<BaseEvent, BaseAction>;
}