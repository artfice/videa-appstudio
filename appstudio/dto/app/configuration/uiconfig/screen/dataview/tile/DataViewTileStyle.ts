import {ComplexColor} from '../../../../../../common/ComplexColor';
import {GradientColor} from '../../../../../../common/GradientColor';
import {SizedComponentStyle} from '../../component/style/SizedComponentStyle';

export interface DataViewTileStyle extends SizedComponentStyle {
    aspectRatio: string;
    backgroundColor: ComplexColor | GradientColor;
}