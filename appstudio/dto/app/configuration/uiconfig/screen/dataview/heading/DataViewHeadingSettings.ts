import {MetadataOwner} from '../../../../../../MetadataOwner';
import {DataViewHeadingStyle} from './DataViewHeadingStyle';
import {ComponentControls} from '../../component/ComponentControls';
import {BaseEvent} from '../../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../../common/listener/action/BaseAction';

export interface DataViewHeadingSettings extends MetadataOwner {
    style: DataViewHeadingStyle;
    controls: ComponentControls<BaseEvent, BaseAction>;
}