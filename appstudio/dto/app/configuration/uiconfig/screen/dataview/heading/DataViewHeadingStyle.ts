import {SizedComponentStyle} from '../../component/style/SizedComponentStyle';
import {Color} from '../../../../../../common/Color';

export interface DataViewHeadingStyle extends SizedComponentStyle {
    backgroundColor: Color;
}