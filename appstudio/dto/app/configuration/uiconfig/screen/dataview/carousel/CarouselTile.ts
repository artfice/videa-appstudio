import {MetadataOwner} from '../../../../../../MetadataOwner';
import {DataViewTileSettings} from '../../dataview/tile/DataViewTileSettings';
import {Label} from '../../component/field/label/Label';
import {Image} from '../../component/field/image/Image';

export interface CarouselTile extends MetadataOwner {
    settings: DataViewTileSettings;
    text: Label[];
    image: Image[];
}