import { SizedComponentStyle } from '../../component/style/SizedComponentStyle';
import { Color } from '../../../../../../common/Color';
import { Position } from '../../../../../../common/Position';

export interface CarouselStyle extends SizedComponentStyle {
    tileScale: number;
    paginationDots: boolean;
    paginationSelectedDotColors: Color;
    paginationUnselectedDotColors: Color;
    paginationDotPosition: Position;
    paginationLocation: string;
    paginationMargin: string;
    autoScrollDuration: number;
}
