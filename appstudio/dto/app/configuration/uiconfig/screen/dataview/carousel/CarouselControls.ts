import {ComponentControls} from '../../component/ComponentControls';
import {BaseEvent} from '../../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../../common/listener/action/BaseAction';

export interface CarouselControls extends ComponentControls<BaseEvent, BaseAction> {
}