import {DataView} from '../../dataview/DataView';
import {CarouselStyle} from './CarouselStyle';
import {CarouselControls} from './CarouselControls';
import {BaseCollection} from '../../../../../../common/collection/BaseCollection';
import {SizedComponentStyle} from '../../component/style/SizedComponentStyle';

export interface Carousel extends DataView<BaseCollection, SizedComponentStyle, CarouselControls> {

    style: CarouselStyle;
    controls: CarouselControls;
}