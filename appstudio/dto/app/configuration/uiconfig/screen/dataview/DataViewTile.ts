import {MetadataOwner} from '../../../../../MetadataOwner';
import {DataViewTileSettings} from '../dataview/tile/DataViewTileSettings';
import {Label} from '../component/field/label/Label';

export interface DataViewTile extends MetadataOwner {
    settings: DataViewTileSettings;
    text: Label[];
    subComponent: any[];
}