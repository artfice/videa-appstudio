import {SizedComponentStyle} from '../../../component/style/SizedComponentStyle';

export interface ListViewStyle extends SizedComponentStyle {
    itemSeparator: boolean;
}