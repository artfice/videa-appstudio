import {DataView} from '../../dataview/DataView';
import {BaseCollection} from '../../../../../../common/collection/BaseCollection';
import {SizedComponentStyle} from '../../component/style/SizedComponentStyle';
import {ComponentControls} from '../../component/ComponentControls';
import {BaseEvent} from '../../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../../common/listener/action/BaseAction';

export interface GridView<TCollection extends BaseCollection,TStyle extends SizedComponentStyle, TControls extends ComponentControls<BaseEvent, BaseAction>> extends DataView<BaseCollection, SizedComponentStyle,  ComponentControls<BaseEvent, BaseAction>> {
    style: TStyle;
    controls: TControls;
}