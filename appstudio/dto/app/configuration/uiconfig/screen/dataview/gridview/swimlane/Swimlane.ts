import {GridView} from '../../gridview/GridView';
import {SizedComponentStyle} from '../../../component/style/SizedComponentStyle';

import {BaseCollection} from '../../../../../../../common/collection/BaseCollection';
import {ComponentControls} from '../../../component/ComponentControls';
import {BaseEvent} from '../../../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../../../common/listener/action/BaseAction';

export interface Swimlane extends GridView<BaseCollection, SizedComponentStyle, ComponentControls<BaseEvent, BaseAction>> {
    style: SizedComponentStyle;
}