import {SizedComponentStyle} from '../../component/style/SizedComponentStyle';

export interface GridViewStyle extends SizedComponentStyle {
    columns: number;
    rows: number;
}