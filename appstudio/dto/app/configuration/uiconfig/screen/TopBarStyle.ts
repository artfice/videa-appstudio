import {MetadataOwner} from '../../../../MetadataOwner';
import {Color} from '../../../../common/Color';
import {GradientColor} from '../../../../common/GradientColor';

export interface TopBarStyle extends MetadataOwner {
    backgroundColor: Color | GradientColor;
}