import { MetadataOwner } from '../../../MetadataOwner';
import { Assets } from '../../../app/configuration/uiconfig/theme/Assets';
import { Colors } from '../../../app/configuration/uiconfig/theme/Colors';
import { Fonts } from '../../../app/configuration/uiconfig/theme/Fonts';

export interface Theme extends MetadataOwner {
    assets: Assets;
    colors: Colors;
    fonts: Fonts;
}