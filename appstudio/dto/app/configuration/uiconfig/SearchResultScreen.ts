import {Screen} from './Screen';
import {Component} from './screen/component/Component';
import {ScreenStyle} from './screen/ScreenStyle';
import {ScreenContent} from './screen/ScreenContent';
import {BaseStyle} from './screen/component/style/BaseStyle';


export interface SearchResultScreen extends Screen<ScreenStyle, Component<BaseStyle>, ScreenContent> {
}