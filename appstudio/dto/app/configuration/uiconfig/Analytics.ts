import {MetadataOwner} from '../../../MetadataOwner';
export interface Analytics extends MetadataOwner {
    name: string;
    events?: any[];
}