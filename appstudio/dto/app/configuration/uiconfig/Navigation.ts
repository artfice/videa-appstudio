import { MetadataOwner } from '../../../MetadataOwner';
import { Section } from '../../../app/configuration/uiconfig/navigation/Section';
import { SecondaryMenu } from '../../../app/configuration/uiconfig/navigation/SecondaryMenu';
import { BaseMenu } from '../../../common/navigation/BaseMenu';

export interface Navigation<TMenu extends BaseMenu> extends MetadataOwner {
    mainMenu: TMenu;
    section: Section[];
    secondaryMenu: SecondaryMenu;
}