import { NamedComponent } from '../../../app/configuration/uiconfig/screen/component/NamedComponent';
import { Component } from '../../../app/configuration/uiconfig/screen/component/Component';
import { ScreenStyle } from '../../../app/configuration/uiconfig/screen/ScreenStyle';
import { ScreenContent } from '../../../app/configuration/uiconfig/screen/ScreenContent';
import { BaseStyle } from '../../../app/configuration/uiconfig/screen/component/style/BaseStyle';

export interface Screen<T extends ScreenStyle, TComponent extends Component<BaseStyle>, TContent extends ScreenContent> extends NamedComponent<BaseStyle> {
    item: TComponent[];
    style: T;
    configId?: string;
    scroll?: string;
    content: TContent;
}