import {MetadataOwner} from '../../../../MetadataOwner';
import {ThemeFont} from '../../../../common/ThemeFont';

export interface Fonts extends MetadataOwner {
    mainFont: ThemeFont;
    titleFont: ThemeFont;
    menuFont: ThemeFont;
    bodyFont: ThemeFont;
}