import {GalleryImage} from '../../../../common/GalleryImage';
import {ColorGalleryImage} from '../../../../common/ColorGalleryImage';

export interface Assets {
    logo: GalleryImage;
    splashScreen: GalleryImage;
    background: ColorGalleryImage;
    navBackground: ColorGalleryImage;
    topBar: ColorGalleryImage;
    appIcon: GalleryImage;
}