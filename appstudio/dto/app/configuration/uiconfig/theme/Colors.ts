import {MetadataOwner} from '../../../../MetadataOwner';
import {Color} from '../../../../common/Color';

export interface Colors extends MetadataOwner {
    primarySpot: Color;
    secondarySpot: Color;
    background: Color;
    foreground: Color;
    accent: Color;
}