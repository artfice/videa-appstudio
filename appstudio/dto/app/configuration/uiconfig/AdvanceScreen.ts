import {CustomData} from '../../../common/CustomData';
import {MetadataOwner} from '../../../MetadataOwner';

export interface AdvanceScreen extends MetadataOwner {
    name: string;
    customData: CustomData;
}