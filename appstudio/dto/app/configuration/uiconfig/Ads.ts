import { MetadataOwner } from '../../../MetadataOwner';
export interface Ads extends MetadataOwner {
    enabled: boolean;
    type: string;
    adTagUrl: string;
}