import {Analytics} from '../../../../../app/configuration/uiconfig/Analytics';
import {Page} from '../../../../../app/configuration/uiconfig/analytics/google/Page';
import {EventField} from '../../../../../app/configuration/uiconfig/analytics/google/EventField';

export interface GemiusAnalytics extends Analytics {
    host: string;
    iOS: string;
    window8: string;
    windowsPhone: string;
    android: string;
    testing: string;
    screens: Page[];
    events: EventField[];
}