import {Analytics} from '../../../../../app/configuration/uiconfig/Analytics';
import {Page} from './Page';
import {EventField} from './EventField';
import {SystemEvents} from './SystemEvents';

export interface GoogleAnalytics extends Analytics {
    social: string;
    exceptions: string;
    trackingId: string;
    screens: Page[];
    events: EventField[];
    systemEvents: SystemEvents;
}