import {MetadataOwner} from '../../../../../MetadataOwner';

export interface EventField extends MetadataOwner {
    videaEventTag?: string;
    category: string;
    action: string;
    labels: string;
    value: string;
    custom?: any; 
}