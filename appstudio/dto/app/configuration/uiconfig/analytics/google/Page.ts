import {MetadataOwner} from '../../../../../MetadataOwner';

export interface Page extends MetadataOwner {
    screenId: string;
    screenName: string;
}