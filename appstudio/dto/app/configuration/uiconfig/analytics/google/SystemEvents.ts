import {MetadataOwner} from '../../../../../MetadataOwner';
import {Event} from './systemEvent/Event';
import {VideoPlaybackEvent} from './systemEvent/VideoPlaybackEvent';

export interface SystemEvents extends MetadataOwner {
    signInSuccess : Event;
    signInFailure : Event;
    search : Event;
    chromecastConnect : Event;
    chromecastDisconnect : Event;
    videoPlayback: VideoPlaybackEvent[];
}