import {MetadataOwner} from '../../../../../../MetadataOwner';
import {Event} from './Event';

export interface VideoPlaybackEvent extends MetadataOwner {
    event: Event;
}