import {CustomData} from '../../../../../../common/CustomData';
import {MetadataOwner} from '../../../../../../MetadataOwner';

export interface Event extends MetadataOwner {
    enabled : boolean;
    category : string;
    action : string;
    labels : string;
    value : string;
    custom: CustomData[];
}