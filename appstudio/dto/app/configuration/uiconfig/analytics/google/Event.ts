import {MetadataOwner} from '../../../../../MetadataOwner';
import {EventField} from './EventField';

export interface Event extends MetadataOwner {
    videaEventTag: string;
    googleAnalyticsEventFields: EventField;
}