import {BaseReference} from '../../../../common/BaseReference';
import {MetadataOwner} from '../../../../MetadataOwner';
export interface WelcomeScreen extends MetadataOwner {
    enabled: boolean;
    screen: BaseReference;
    persistKey: string;
}