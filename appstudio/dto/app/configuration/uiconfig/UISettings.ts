import { MetadataOwner } from '../../../MetadataOwner';
import {WelcomeScreen} from './settings/WelcomeScreen';

export interface UISettings extends MetadataOwner {
    screenId: string;
    welcomeScreen: WelcomeScreen;
}