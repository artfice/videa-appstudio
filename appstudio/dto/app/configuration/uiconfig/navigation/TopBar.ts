import { MetadataOwner as MetadataOwner } from '../../../../MetadataOwner';
import { ColoredComponentStyle as TopBarStyle } from '../screen/component/style/ColoredComponentStyle';
import { TitleStyle } from './topbar/TitleStyle';

export interface TopBar extends MetadataOwner {
  visible: boolean;
  overlay: boolean;
  showAppLogoAsTitle: boolean;
  style: TopBarStyle;
  titleStyle: TitleStyle;
}