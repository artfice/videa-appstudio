import {MetadataOwner} from '../../../../MetadataOwner';
import {SectionSettings} from './sections/SectionSettings';
import {Label} from './sections/Label';
import {Image} from './sections/Image';

export interface Section extends MetadataOwner {
    settings: SectionSettings;
    label: Label;
    image: Image;
}