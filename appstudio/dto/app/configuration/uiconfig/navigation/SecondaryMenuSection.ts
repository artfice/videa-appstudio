import {MetadataOwner} from '../../../../MetadataOwner';
import {SecondaryMenuSectionSettings} from './secondarymenu/SecondaryMenuSectionSettings';
import {Image} from '../../uiconfig/screen/component/field/image/Image';

export interface SecondaryMenuSection extends MetadataOwner {
    settings: SecondaryMenuSectionSettings;
    images: Image[];
}