import {SecondaryMenuSection as SecondaryMenuSection} from '../../../../app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSection';
import {MetadataOwner as MetadataOwner} from '../../../../MetadataOwner';
import {Label as LabelComponent} from '../../../../app/configuration/uiconfig/screen/component/field/label/Label';
import {SecondaryMenuSettings as SecondaryMenuSettings} from '../../../../app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSettings';

export interface SecondaryMenu extends MetadataOwner {
    settings: SecondaryMenuSettings;
    text: LabelComponent[];
    sections: SecondaryMenuSection[];
}