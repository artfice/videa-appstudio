import {MetadataOwner} from '../../../../MetadataOwner';

export interface Menu extends MetadataOwner {
  visible: boolean;
}