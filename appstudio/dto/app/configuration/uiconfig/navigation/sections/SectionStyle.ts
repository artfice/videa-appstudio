import {SizedComponentStyle} from '../../screen/component/style/SizedComponentStyle';
import {Color} from '../../../../../common/Color';
import {GradientColor} from '../../../../../common/GradientColor';

export interface SectionStyle extends SizedComponentStyle {
    backgroundColor: Color | GradientColor;
}