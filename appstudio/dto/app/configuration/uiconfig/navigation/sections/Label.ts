import {MetadataOwner as MetadataOwner} from '../../../../../MetadataOwner';
import {LabelSettings as LabelSettings} from './LabelSettings';
import {Label as LabelComponent} from '../../screen/component/field/label/Label';

export interface Label extends MetadataOwner {
    settings: LabelSettings;
    text: LabelComponent[];
}