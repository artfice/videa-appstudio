import {MetadataOwner} from '../../../../../MetadataOwner';
import {ColoredComponentStyle} from '../../screen/component/style/ColoredComponentStyle';

export interface ImageSettings extends MetadataOwner {
    style: ColoredComponentStyle;
}