import {MetadataOwner as MetadataOwner} from '../../../../../MetadataOwner';
import {ImageSettings as ImageSettings} from './ImageSettings';
import {Image as ImageComponent} from '../../screen/component/field/image/Image';

export interface Image extends MetadataOwner {
    settings: ImageSettings;
    image: ImageComponent[];
}