import {MetadataOwner} from '../../../../../MetadataOwner';
import {ColoredComponentStyle} from '../../screen/component/style/ColoredComponentStyle';

export interface LabelSettings extends MetadataOwner {
    style: ColoredComponentStyle;
}