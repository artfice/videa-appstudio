import {MetadataOwner} from '../../../../../MetadataOwner';
import {SectionStyle} from './SectionStyle';
import {ComponentControls} from '../../screen/component/ComponentControls';
import {BaseEvent} from '../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../common/listener/action/BaseAction';

export interface SectionSettings extends MetadataOwner {
    style: SectionStyle;
    controls: ComponentControls<BaseEvent, BaseAction>;
}