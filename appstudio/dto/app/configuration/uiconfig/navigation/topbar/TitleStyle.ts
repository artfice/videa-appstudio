import { MetadataOwner } from '../../../../../MetadataOwner';
import { Font } from '../../../../../common/Font';

export interface TitleStyle extends MetadataOwner {
    font: Font;
}