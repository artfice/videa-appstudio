import {MetadataOwner} from '../../../../../MetadataOwner';
import {ComponentControls} from '../../../../../app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent} from '../../../../../common/listener/event/BaseEvent';
import {BaseAction} from '../../../../../common/listener/action/BaseAction';
import {ColoredComponentStyle} from '../../../../../app/configuration/uiconfig/screen/component/style/ColoredComponentStyle';

export interface SecondaryMenuSettings extends MetadataOwner {
    style: ColoredComponentStyle;
    controls: ComponentControls<BaseEvent, BaseAction>;
}