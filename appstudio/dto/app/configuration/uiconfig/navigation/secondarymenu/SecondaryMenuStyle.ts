import {SizedComponentStyle} from '../../../../../app/configuration/uiconfig/screen/component/style/SizedComponentStyle';
import {Color} from '../../../../../common/Color';
import {GradientColor} from '../../../../../common/GradientColor';

export interface SecondaryMenuStyle extends SizedComponentStyle {
    backgroundColor: Color | GradientColor;
}