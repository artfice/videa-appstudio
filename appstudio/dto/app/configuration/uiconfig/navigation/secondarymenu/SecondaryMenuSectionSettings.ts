import {MetadataOwner} from '../../../../../MetadataOwner';
import {ColoredComponentStyle} from '../../../uiconfig/screen/component/style/ColoredComponentStyle';

export interface SecondaryMenuSectionSettings extends MetadataOwner {
    style: ColoredComponentStyle;
}