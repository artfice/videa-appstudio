import {MetadataOwner as MetadataOwner} from '../../../../../MetadataOwner';
import {SecondaryMenuSettings as SecondaryMenuSettings} from './SecondaryMenuSettings';
import {Image as ImageComponent} from '../../../uiconfig/screen/component/field/image/Image';

export interface SecondaryMenuSection extends MetadataOwner {
    settings: SecondaryMenuSettings;
    image: ImageComponent[];
}