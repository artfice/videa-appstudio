import { MetadataOwner } from '../../MetadataOwner';

export interface Cms extends MetadataOwner {
    name: string;
    entitlement: string;
}