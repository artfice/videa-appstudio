import { MetadataOwner } from '../../../MetadataOwner';

export interface BaseProvider extends MetadataOwner {
    provider: string;
    completionThreshold: number;
    frequency: number;
    onPauseEvent: boolean;
    onResumeEvent: boolean;
    onSeekEvent: boolean;
    onStopEvent: boolean;
    onPlayEvent: boolean;
}