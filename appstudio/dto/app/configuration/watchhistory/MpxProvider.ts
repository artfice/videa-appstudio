
import {BaseProvider} from './BaseProvider';

export interface MpxProvider extends BaseProvider {
    host : string;
    refreshInterval : number;

}