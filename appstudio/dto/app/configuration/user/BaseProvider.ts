import {MetadataOwner} from '../../../MetadataOwner'

export interface BaseProvider extends MetadataOwner {
    accountId: string;
    pid: string;
}
