import { Font } from '../../../common/Font';
import { MetadataOwner } from '../../../MetadataOwner';

export interface Title extends MetadataOwner {
    font: Font;
}