import { Font } from '../../../common/Font';
import { MetadataOwner } from '../../../MetadataOwner';

export interface Receiver extends MetadataOwner {
    font: Font;
}