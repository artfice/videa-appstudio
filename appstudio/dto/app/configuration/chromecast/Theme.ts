import { Color } from '../../../common/Color';
import { MetadataOwner } from '../../../MetadataOwner';

export interface Theme extends MetadataOwner {
    accentColor: Color;
    secondaryColor: Color;
    backgroundColor: Color;
    foregroundColor: Color;
}