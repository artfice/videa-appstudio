import {MetadataOwner} from '../../MetadataOwner';
export interface Settings extends MetadataOwner {
    minimumVersion: number;
    currentVersion: number;
};