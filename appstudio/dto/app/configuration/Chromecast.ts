import {Theme} from '../../app/configuration/chromecast/Theme';
import {Title} from '../../app/configuration/chromecast/Title';
import {Receiver} from '../../app/configuration/chromecast/Receiver';
import {MetadataOwner} from '../../MetadataOwner';

export interface Chromecast extends MetadataOwner {
    enabled: boolean;
    appId: string;
    theme: Theme;
    title: Title;
    receiver: Receiver;
}