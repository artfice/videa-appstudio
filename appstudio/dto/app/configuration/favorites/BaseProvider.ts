import {MetadataOwner} from '../../../MetadataOwner';

export interface BaseProvider extends MetadataOwner {
    frequency: number;
    uid: string;
    link: string;
    collectionPath: string;
}