import { MetadataOwner } from '../../MetadataOwner';
import { BaseProvider } from '../../app/configuration/watchhistory/BaseProvider';

export interface WatchHistory<TProvider extends BaseProvider> extends MetadataOwner {
    provider: TProvider;
}