import {Authentication} from './configuration/Authentication';
import {Cms} from './configuration/Cms';
import {CustomData} from '../common/CustomData';
import {Provider as BaseProvider} from '../common/auth/BaseProvider';
import {Chromecast} from './configuration/Chromecast';
import {WatchHistory} from './configuration/WatchHistory';
import {BaseProvider as BaseWatchHistoryProvider} from './configuration/watchhistory/BaseProvider';
import {MetadataOwner} from '../MetadataOwner';
import {Favorites} from './configuration/Favorites';
import {BaseProvider as FavoritesBaseProvider} from './configuration/favorites/BaseProvider';
import {User} from './configuration/User';
import {BaseProvider as BaseUserProvider} from './configuration/user/BaseProvider';
import {Settings} from './configuration/Settings';

export interface Configuration extends MetadataOwner {
    authentication: Authentication<BaseProvider>,
    cms: Cms[],
    uiConfig: string[];
    custom: CustomData;
    chromecast: Chromecast;
    watchHistory: WatchHistory<BaseWatchHistoryProvider>;
    favorites: Favorites<FavoritesBaseProvider>;
    user: User<BaseUserProvider>;
    settings: Settings;
}