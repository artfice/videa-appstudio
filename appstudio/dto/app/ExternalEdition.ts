import {MetadataOwner} from '../MetadataOwner';

export interface ExternalEdition extends MetadataOwner {
  uiConfig: string[];
  clientConfig: string;
}