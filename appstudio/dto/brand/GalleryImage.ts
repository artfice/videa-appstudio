import {Image} from 'videa-framework/domain/image/Image';

export interface GalleryImage extends Image {
    title: string;
    active: boolean;
}