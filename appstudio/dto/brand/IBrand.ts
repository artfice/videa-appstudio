import { BaseDto } from 'videa-framework/domain/BaseDto';
import { Image } from 'videa-framework/domain/image/Image';

export interface IBrand extends BaseDto {
    name: string;
    image: Image;
    app: string[]; //appID
    gallery: string[]; //galleryImageID
}