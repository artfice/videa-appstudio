
import { UIConfig as Config } from '../dto/app/configuration/UIConfig';
import { BaseMenu } from '../dto/common/navigation/BaseMenu';

import Promise = require('bluebird');

import { UpdateResult } from 'videa-framework/domain/db/UpdateResult';
import { SearchOperators } from 'videa-framework/domain/db';

import { IUiConfigApplicationServiceConfig } from '../dto/applicationService/UiConfigApplicationServiceConfig';
import { EventService } from 'videa-framework/EventService';
import { ItemNotFoundError } from 'videa-framework/VideaError';

import * as Digi from 'videa-framework/Digi';
import { Logger } from 'videa-framework/Logger';

import { EditionRepository } from '../repository/EditionRepository';
import { UIConfigRepository } from '../repository/UIConfigRepository';
import { ScreenRepository } from '../repository/ScreenRepository';
import { IEventRepository } from 'videa-framework/repository/IEventRepository';

import MobileGoogleAnalytic = require('../schema/appstudio/app/mobile/uiconfig/analytics/MobileGoogleAnalytics');
import TabletGoogleAnalytic = require('../schema/appstudio/app/tablet/uiconfig/analytics/TabletGoogleAnalytics');
import TVGoogleAnalytic = require('../schema/appstudio/app/tv/uiconfig/analytics/TvGoogleAnalytics');

import { AppstudioModelFactory } from '../factory/AppstudioModelFactory';

export class UIConfigApplicationService {
    private _uiConfigRepository: UIConfigRepository;
    private _editionRepository: EditionRepository;
    private _screenRepository: ScreenRepository;
    private _eventRepository: IEventRepository;

    constructor(config: IUiConfigApplicationServiceConfig) {
        this._uiConfigRepository = config.uiConfigRepository;
        this._editionRepository = config.editionRepository;
        this._screenRepository = config.screenRepository;
        this._eventRepository = config.eventRepository;
    }

    public addConfig(accountId: string, editionId: string, newConfig: Config<BaseMenu>): Promise<Config<BaseMenu>> {
        let config;
        let edition;

        return new Promise<Config<BaseMenu>>((resolve, reject) => {
            this._editionRepository.getById(accountId, editionId).then(
                (editionFound) => {
                    edition = editionFound;
                    newConfig.editionId = editionId;
                    return this._uiConfigRepository.add(accountId, newConfig);
                }).then(
                (createdConfig) => {
                    config = createdConfig;
                    if (edition.uiConfig.indexOf(config.id) < 0) {
                        edition.uiConfig.push(config.id);
                    }
                    return this._editionRepository.update(accountId, editionId, edition);
                }).then(
                (updateResult) => {
                    this._eventRepository.publishEvent('configChanged', {
                        accountId: accountId,
                        editionId: editionId
                    }, (err) => {
                        console.log('event triggered', err);
                    });

                    resolve(config);
                }).catch(reject);
        });
    }

    public updateConfig(accountId: string, editionId: string, configId: string,
        config: Config<BaseMenu>): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            this._uiConfigRepository.getById(accountId, configId).then((oldConfig) => {
                config.editionId = oldConfig.editionId;
                return this._uiConfigRepository.replace(accountId, configId, config);
            }).then((config) => {
                this._eventRepository.publishEvent('configChanged', {
                    accountId: accountId,
                    editionId: editionId
                }, (err) => {
                    console.log('event triggered', err);
                });
                resolve(config);
            }).catch(reject);
        });
    }

    public removeConfig(accountId: string, editionId: string,
        configId: string): Promise<void> {
        let edition;
        let uiConfigs = [];
        return new Promise<void>((resolve, reject) => {
            this._editionRepository.getById(accountId, editionId).then(
                (editionFound) => {
                    edition = editionFound;
                    uiConfigs = edition.uiConfig;
                    if (!Digi.List.exist(uiConfigs, configId)) {
                        reject(new ItemNotFoundError('Something wrong. ConfigId ' +
                            configId + ' not found!'));
                    }
                    if (edition.activeConfig === configId) {
                        edition.activeConfig = '';
                    }

                    edition.uiConfig = edition.uiConfig.filter(
                        (uiConfig) => {
                            return uiConfig !== configId;
                        });
                    return this._editionRepository.update(accountId, editionId, edition);
                }).then((updateResult) => {
                    return this._uiConfigRepository.getById(accountId, configId);
                }).then((config) => {
                    return this._uiConfigRepository.remove(accountId, configId);
                }).then(() => {
                    this._eventRepository.publishEvent('configChanged', {
                        accountId: accountId,
                        editionId: editionId
                    }, (err) => {
                        console.log('event triggered', err);
                    });
                    resolve(undefined);
                }).catch(reject);
        });
    }

    public getConfigById(accountId: string, configId: string): Promise<Config<BaseMenu>> {
        return this._uiConfigRepository.getById(accountId, configId);
    }

    public duplicateConfig(accountId: string, editionId: string, configId: string): Promise<Config<BaseMenu>> {
        let edition;
        let uiConfigsScreens = [];
        let rawConfig;
        let promiseList = [];
        let config;
        const self = this;
        let mapping = {};
        let screenList = [];

        return new Promise<Config<BaseMenu>>((resolve, reject) => {
            this._editionRepository.getById(accountId, editionId).then(
                (editionFound) => {
                    edition = editionFound;
                    return this._uiConfigRepository.getById(accountId, configId);
                }).then(
                (configFound) => {
                    rawConfig = configFound;
                    uiConfigsScreens = rawConfig.screen;
                    rawConfig.name = rawConfig.name + ' Copy';
                    delete rawConfig.id;
                    delete rawConfig.createdDate;
                    delete rawConfig.modifiedDate;
                    return this._uiConfigRepository.add(accountId, rawConfig);
                }).then(
                (newConfig) => {
                    mapping[configId] = newConfig.id;
                    config = newConfig;
                    config.screen = [];
                    edition.uiConfig.push(config.id);
                    return this._editionRepository.update(accountId, editionId, edition);
                }).then(
                (updateResult) => {
                    return this._screenRepository.search(accountId, <any>{
                        offset: 0, setSize: 10000, query: {
                            field: 'configId',
                            operator: SearchOperators.Equal,
                            value: configId
                        }
                    });
                }).then((screensFound: any) => {
                    promiseList = screensFound.data.map(
                        (screen) => {
                            const oldScreenId = screen.id;
                            delete screen.id;
                            screen.configId = config.id;
                            return self._screenRepository.add(accountId, screen).then((newScreen: any) => {
                                mapping[oldScreenId] = newScreen.id;
                                screenList.push(newScreen.id);
                                config.screen.push(newScreen.id);
                                return newScreen;
                            });
                        });
                    return Promise.all(promiseList);
                }).then((results) => {
                    const mappingKeys = Object.keys(mapping);
                    for (let i = 0; i < mappingKeys.length; i++) {
                        if (mapping[mappingKeys[i]]) {
                            config = UIConfigApplicationService.searchAndReplace(config,
                                'screenId', mappingKeys[i], mapping[mappingKeys[i]]);
                            config = UIConfigApplicationService.searchAndReplace(config,
                                'configId', mappingKeys[i], mapping[mappingKeys[i]]);
                        }
                    }
                    return this._uiConfigRepository.update(accountId, config.id, config);
                }).then((updateResult) => {
                    const self = this;
                    const mappingKeys = Object.keys(mapping);
                    const promiseList = screenList.map((screenId) => {
                        return new Promise((res, rej) => {
                            return self._screenRepository.getById(accountId, screenId).then(
                                (screen: any) => {
                                    for (let i = 0; i < mappingKeys.length; i++) {
                                        if (mapping[mappingKeys[i]]) {
                                            screen = UIConfigApplicationService.searchAndReplace(screen,
                                                'screenId', mappingKeys[i], mapping[mappingKeys[i]]);
                                            screen = UIConfigApplicationService.searchAndReplace(screen,
                                                'screenIdToNavigateOnEnd', mappingKeys[i], mapping[mappingKeys[i]]);
                                            screen = UIConfigApplicationService.searchAndReplace(screen,
                                                'configId', mappingKeys[i], mapping[mappingKeys[i]]);
                                        }
                                    }
                                    return self._screenRepository.update(accountId, screen.id, screen);
                                }).then(res).catch(rej);
                        });
                    });
                    return Promise.all(promiseList);
                }).then(() => {
                    EventService.emit('retranslate-edition', {
                        accountId: accountId,
                        editionId: editionId
                    }, () => { });
                    resolve(config);
                }).catch(reject);

        });
    }

    public getDynamicEnumAnalytics(accountId: string, configId: string): Promise<any[]> {
        const googleAnalytics = [MobileGoogleAnalytic.ID, TabletGoogleAnalytic.ID, TVGoogleAnalytic.ID];
        return new Promise<any[]>((resolve, reject) => {
            this._uiConfigRepository.getById(accountId, configId).then((configFound) => {
                const analytics = configFound.analytics ? configFound.analytics : [];
                resolve(analytics.filter((analytic) => {
                    return googleAnalytics.indexOf(analytic._metadata) > -1;
                }).map((analytics) => {
                    const events = analytics.events ? analytics.events : [];
                    return events.map((analytic) => {
                        return AppstudioModelFactory.create('BaseReference', {
                            value: analytic.videaEventTag,
                            refId: analytic.videaEventTag
                        });
                    });
                }).reduce(function (a, b) {
                    return a.concat(b);
                }, []));
            }).catch((e) => {
                Logger.error('DynamicEnum Analytic Error accountId: ' + accountId + ' configId ' + configId, e);
                resolve([]);
            });
        });
    }

    public static searchAndReplace(obj: any, target: string, oldValue: any, replaceValue: any) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (key === target && obj[key] === oldValue) {
                    obj[key] = replaceValue;
                } else if (obj[key] !== null && typeof obj[key] === 'object') {
                    obj[key] = UIConfigApplicationService.searchAndReplace(obj[key], target, oldValue, replaceValue);
                }
            }
        }
        return obj;
    }
}