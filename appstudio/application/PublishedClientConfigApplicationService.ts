import Promise = require('bluebird');

import {AppRepository} from '../repository/AppRepository';
import {ClientConfigRepository} from '../repository/ClientConfigRepository';
import {PublishedClientConfigApplicationServiceConfig} from '../dto/applicationService/PublishedClientConfigApplicationServiceConfig';

export class PublishedClientConfigApplicationService {
    private _clientConfigRepository: ClientConfigRepository;
    private _appRepository: AppRepository;

    constructor(config: PublishedClientConfigApplicationServiceConfig) {
        this._clientConfigRepository = config.clientConfigRepository;
        this._appRepository = config.appRepository;
    }

    public getPublishedClientConfig(accountId: string, appId: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this._appRepository.getById(accountId, appId).then((app) => {
                if (app.clientConfig.length > 0) {
                    return this._clientConfigRepository.getById(accountId, app.clientConfig);
                } else {
                    resolve({
                        code: 0,
                        message: 'No config available',
                        data: null
                    });
                }
            }).then(resolve).catch(reject);
        });
    }
}