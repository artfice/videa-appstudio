///<reference path="../../typings/index.d.ts"/>

import _ = require('underscore');
import Promise = require('bluebird');

import {ITranslationService} from '../interface/ITranslationService';
import {IEntityTranslator} from '../interface/IEntityTranslator';

import {MetadataOwner} from '../dto/MetadataOwner';

import {TranslationError} from 'videa-framework/VideaError';

export class TranslationApplicationService implements ITranslationService {

    private _translators: any[];

    constructor() {
        this._translators = [];
    }

    public translate(accountId: string, source: any) {
        let translator: IEntityTranslator<any, any>;

        if (!source || !source._metadata) {
            return Promise.resolve(null);
        }

        translator = this.getTranslator(source);

        if (!translator) {
            return Promise.reject('no translator found for source ' + source['_metadata']);
        }

        return translator.translate(accountId, this, source);
    }

    public getTranslator(source: any) {
        const translatorConfig = this.findTranslator(source);
        return translatorConfig ? new translatorConfig.translatorClass(translatorConfig.config) : null;
    }

    public registerTranslator(translator: any, config?: any) {
        if (!translator) {
            throw new TranslationError('Invalid translator');
        }

        this._translators.push({
            translatorClass: translator,
            config: config
        });
    }

    public registerTranslators(translators: any[]) {
        if (translators.length > 0) {
            this._translators = this._translators.concat(translators);
        }
    }

    private findTranslator(source: any): any {
        return _(this._translators).find((translator) => {
            return TranslationApplicationService._isType(source, translator.translatorClass.SOURCE_TYPE);
        });
    }

    static _isType(source: MetadataOwner, typeString: string): boolean {
        if (source.hasOwnProperty('_metadata')) {
            return source._metadata === typeString;
        }

        return false;
    }

}