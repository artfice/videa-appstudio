export class EditionStateApplicationService {
    protected _states: any;
    
    constructor() {
        this._states = {
            'IN_PROGRESS': ['NEED_REVIEW', 'COMPLETED', 'PUBLISHED'],
            'NEED_REVIEW': ['COMPLETED', 'PUBLISHED', 'IN_PROGRESS'],
            'COMPLETED': ['NEED_REVIEW', 'PUBLISHED', 'IN_PROGRESS'],
            'PUBLISHED': ['COMPLETED']
        };
    }

    public getStates(accountId: string, appId: string): any {
        //TODO: Once we have permissions we can fill this in more
        return this.getAllStates();
    }

    public getStateTypes(): string[] {
        return ['NEED_REVIEW', 'COMPLETED', 'PUBLISHED', 'IN_PROGRESS'];
    }

    protected getAllStates(): any {
        return this._states;
    }
}