import Promise = require('bluebird');

import {Screen as AppstudioScreenDto} from '../dto/app/configuration/uiconfig/Screen';
import {ScreenStyle as AppstudioScreenStyleDto} from '../dto/app/configuration/uiconfig/screen/ScreenStyle';
import {ScreenContent as AppstudioScreenContentDto} from '../dto/app/configuration/uiconfig/screen/ScreenContent';
import {Component as AppstudioComponentDto} from '../dto/app/configuration/uiconfig/screen/component/Component';
import {BaseStyle as AppstudioBaseStyleDto} from '../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';

import {UpdateResult} from 'videa-framework/domain/db/UpdateResult';
import {ScreenApplicationServiceConfig} from '../dto/applicationService/ScreenApplicationServiceConfig';

import {UIConfigRepository} from '../repository/UIConfigRepository';
import {ScreenRepository} from '../repository/ScreenRepository';
import {IEventRepository} from 'videa-framework/repository/IEventRepository';

import {ItemNotFoundError} from 'videa-framework/VideaError';
import MobileSearchResultScreen = require('../schema/appstudio/app/mobile/uiconfig/screen/MobileSearchResultScreen');
import TabletSearchResultScreen = require('../schema/appstudio/app/tablet/uiconfig/screen/TabletSearchResultScreen');
import TVSearchResultScreen = require('../schema/appstudio/app/tv/uiconfig/screen/TVSearchResultScreen');
import {AppstudioModelFactory} from '../factory/AppstudioModelFactory';
import { Logger } from 'videa-framework/Logger';

export class ScreenApplicationService {

    private _uiConfigRepository: UIConfigRepository;
    private _screenRepository: ScreenRepository;
    private _eventRepository: IEventRepository;

    constructor(config: ScreenApplicationServiceConfig) {
        this._uiConfigRepository = config.uiConfigRepository;
        this._screenRepository = config.screenRepository;
        this._eventRepository = config.eventRepository;
    }

    public createScreen(accountId: string, configId: string,
        rawScreen: AppstudioScreenDto<AppstudioScreenStyleDto,
            AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>):
        Promise<AppstudioScreenDto<AppstudioScreenStyleDto, AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>> {
        let config;
        let screen;
        return new Promise<AppstudioScreenDto<AppstudioScreenStyleDto, AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>>(
            (resolve, reject) => {
                this._uiConfigRepository.getById(accountId, configId).then(
                    (configFound) => {
                        config = configFound;
                        rawScreen.configId = configId;
                        return this._screenRepository.add(accountId, rawScreen);
                    }).then((newScreen) => {
                        screen = newScreen;
                        config.screen.push(screen.id);
                        return this._uiConfigRepository.update(accountId, configId, config);
                    }).then((updateResult) => {
                        this._eventRepository.publishEvent('configChanged', {
                            accountId: accountId,
                            configId: configId
                        }, (err) => {
                            console.log('event triggered', err);
                        });
                        resolve(screen);
                    }).catch(reject);
            });
    }

    public getScreens(accountId: string, configId: string): Promise<AppstudioScreenDto<AppstudioScreenStyleDto,
        AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>[]> {
        return this._uiConfigRepository.getById(accountId, configId).then((config) => {
            return this._screenRepository.batchGet(accountId, config.screen);
        });
    }

    public updateScreen(accountId: string, configId: string,
        rawScreen: AppstudioScreenDto<AppstudioScreenStyleDto, AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>):
        Promise<AppstudioScreenDto<AppstudioScreenStyleDto, AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>> {
        let screen;
        return new Promise<AppstudioScreenDto<AppstudioScreenStyleDto,
            AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>>((resolve, reject) => {
                if (!rawScreen || !rawScreen.id) {
                    reject(new ItemNotFoundError('screen not found'));
                }
                this._screenRepository.getById(accountId, rawScreen.id).then(
                    (oldScreen) => {
                        screen = rawScreen;
                        screen.configId = oldScreen.configId;
                        return this._screenRepository.replace(accountId, screen.id, screen);
                    }).then((updateResult) => {
                        this._eventRepository.publishEvent('configChanged', {
                            accountId: accountId,
                            configId: configId
                        }, (err) => {
                            console.log('event triggered', err);
                        });
                        return resolve(screen);
                    }).catch(reject);
            });
    }

    public deleteScreen(accountId: string, configId: string, screenId: string): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            this._uiConfigRepository.getById(accountId, configId).then((config) => {
                if (config.screen.indexOf(screenId) < 0) {
                    reject(new ItemNotFoundError('screen not found in config'));
                }
                config.screen = config.screen.filter((id) => {
                    return id !== screenId;
                });
                return this._uiConfigRepository.update(accountId, config.id, config);
            }).then((newConfig) => {
                return this._screenRepository.remove(accountId, screenId);
            }).then(() => {
                this._eventRepository.publishEvent('configChanged', {
                    accountId: accountId,
                    configId: configId
                }, (err) => {
                    console.log('event triggered', err);
                });
                resolve(undefined);
            }).catch(reject);
        });
    }

    public getDynamicEnumScreen(accountId: string, configId: string, type: string): Promise<AppstudioScreenDto<AppstudioScreenStyleDto,
        AppstudioComponentDto<AppstudioBaseStyleDto>, AppstudioScreenContentDto>[]> {

        const allowedTypes = ['all', 'regular', 'search'];
        const searchScreenTypes = [MobileSearchResultScreen.ID, TabletSearchResultScreen.ID, TVSearchResultScreen.ID];

        if (allowedTypes.indexOf(type) < 0) {
            type = 'all';
        }

        return this._uiConfigRepository.getById(accountId, configId).then((config) => {
            return this._screenRepository.batchGet(accountId, config.screen);
        }).then((screens) => {
            return <any>screens.filter((screen) => {
                const notRegularScreenCondition = (type == 'regular' && searchScreenTypes.indexOf(screen._metadata) > -1);
                const notSearchScreenCondition = (type == 'search' && searchScreenTypes.indexOf(screen._metadata) < 0);

                if (notRegularScreenCondition || notSearchScreenCondition) {
                    return false;
                }

                return true;
            }).map((screen) => {
                return AppstudioModelFactory.create('BaseReference', {
                    refId: screen.id,
                    value: screen.name
                });
            });
        }).catch((e) => {
             Logger.error('DynamicEnum Screens Error accountId: ' + accountId + ' configId ' + configId, e);
            return [];
        });
    }    
}