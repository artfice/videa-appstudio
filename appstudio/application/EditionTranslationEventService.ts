import Promise = require('bluebird');

import {InternalServerError} from 'videa-framework/VideaError';

import {ScreenRepository} from '../repository/ScreenRepository';
import {UIConfigRepository} from '../repository/UIConfigRepository';
import {EditionRepository} from '../repository/EditionRepository';
import {AppRepository} from '../repository/AppRepository';

import {AppPublishingApplicationService} from './AppPublishingApplicationService';

import {EditionTranslationEventServiceConfig} from '../dto/applicationService/EditionTranslationEventServiceConfig';

export class EditionTranslationEventService {

    private _uiConfigRepository: UIConfigRepository;
    private _appRepository: AppRepository;
    private _editionRepository: EditionRepository;
    private _appPublishingService: AppPublishingApplicationService;
    private _screenRepository: ScreenRepository;

    constructor(config: EditionTranslationEventServiceConfig) {
        this._uiConfigRepository = config.uiConfigRepository;
        this._appRepository = config.appRepository;
        this._editionRepository = config.editionRepository;
        this._screenRepository = config.screenRepository;
        this._appPublishingService = config.appPublishingService;
    }

    public retranslate(accountId: string, editionId: string, configId: string, screenId: string): Promise<void> {
        if (configId) {
            return this._uiConfigRepository.getById(accountId, configId).then(
                (config) => {
                    return this._appPublishingService.publishEdition(accountId, config.editionId);
                });
        } else if (editionId) {
            return this._editionRepository.getById(accountId, editionId).then(
            (edition) => {
                if (edition.uiConfig.length > 0) {
                    return this._appPublishingService.publishEdition(accountId, editionId);
                } else {
                    return Promise.resolve(undefined);
                }
            });
        } else if (screenId) {
            return this._screenRepository.getById(accountId, screenId).then(
                (screen: any) => {
                    return this._uiConfigRepository.getById(accountId, screen.configId);
                }).then((config) => {
                    return this._appPublishingService.publishEdition(accountId, config.editionId);
                });
        } else {
            throw new InternalServerError('Edition Could not be Translated since screenId and configId not present');
        }

    }
}