import fs = require('fs');

export class LicenseApplicationService {

    public getLicense(): any {
        const json = JSON.parse(fs.readFileSync('licenses.json', 'utf8'));
        const dependancyKeys = Object.keys(json);
        for (let i = 0; i < dependancyKeys.length; i++) {
            const atIndex = dependancyKeys[i].indexOf('@');
            if(atIndex > -1) {
                json[dependancyKeys[i].substring(0, atIndex)] =  json[dependancyKeys[i]];
                delete json[dependancyKeys[i]];
            }
        }
        return json;
    }
}