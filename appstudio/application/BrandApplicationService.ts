import Promise = require('bluebird');
import streamifier = require('streamifier');
import _ = require('lodash');

import { BrandRepository } from '../repository/BrandRepository';
import { AppRepository } from '../repository/AppRepository';
import { GalleryImageRepository } from '../repository/GalleryImageRepository';
import { IImageRepository } from 'videa-framework/repository/IImageRepository';

import { Image } from 'videa-framework/domain/image/Image';
import { GalleryImage } from '../dto/common/GalleryImage';
import { IBrand } from '../dto/brand/IBrand';

import { ResultSet } from 'videa-framework/domain/db/ResultSet';
import { SearchParams } from 'videa-framework/domain/db/SearchParams';
import { UpdateResult } from 'videa-framework/domain/db/UpdateResult';
import { SearchOperators } from 'videa-framework/domain/db';

import { EventService } from 'videa-framework/EventService';
import { Logger } from 'videa-framework/Logger';
import { ItemNotFoundError } from 'videa-framework/VideaError';


import { BrandApplicationServiceConfig } from '../dto/applicationService/BrandApplicationServiceConfig';

import MediaGallerySchema = require('../schema/appstudio/MediaGallery');

import { AppApplicationService } from './AppApplicationService';

export class BrandApplicationService {

    private _brandRepository: BrandRepository;
    private _appService: AppApplicationService;
    private _appRepository: AppRepository;
    private _imageRepository: IImageRepository;
    private _galleryImageRepository: GalleryImageRepository;
    private _brandLogo: string;

    constructor(config: BrandApplicationServiceConfig) {
        this._brandRepository = config.brandRepository;
        this._imageRepository = config.imageRepository;
        this._appService = config.appService;
        this._appRepository = config.appRepository;
        this._galleryImageRepository = config.galleryImageRepository;
        this._brandLogo = 'f599e673-ff65-4b48-bfcb-35a1b74100f2';
        const self = this;

        EventService.on('gallery-image-removed', (e, next) => {
            if (!e.image || !e.account) {
                Logger.error('Missing image Data Parameters');
                next();
            }
            self._deleteImage(e.account, e.image).then((result) => {
                next();
            }).catch((err) => {
                Logger.log('Remove Gallery Image From Cloudinary Failed', e.account);
                Logger.log('Remove Gallery Image From Cloudinary Failed', e.image);
                Logger.log('Remove Gallery Image From Cloudinary Failed', err);
                next();
            });
        });

    }

    public create(accountId: string, brandName: string, imageFile: any): Promise<IBrand> {
        const image = <Image>{
            imageType: this._brandLogo,
            name: imageFile && imageFile.originalname ? imageFile.originalname : ''
        };
        const brand = <IBrand>{
            name: brandName,
            app: [],
            gallery: []
        };

        if (!imageFile) {
            return this._brandRepository.add(accountId, brand);
        } else {
            return new Promise<IBrand>((resolve, reject) => {
                return this._uploadImage(accountId, image, imageFile).then(
                    (cloudImage) => {
                        brand.image = cloudImage;
                        return this._brandRepository.add(accountId, brand);
                    }).then(resolve).catch(reject);
            });
        }
    }

    public search(accountId: string, searchOptions: SearchParams): Promise<ResultSet> {
        return this._brandRepository.search(accountId, searchOptions);
    }

    public updateBrand(accountId: string, brandId: string, brandName: string, imageFile: any): Promise<UpdateResult> {
        const image = <Image>{
            imageType: this._brandLogo,
            name: imageFile && imageFile.originalname ? imageFile.originalname : ''
        };
        let hasImage = false;
        let oldImage;
        let brand;

        return new Promise<UpdateResult>((resolve, reject) => {
            this._brandRepository.getById(accountId, brandId).then(
                (brandFound) => {
                    brand = brandFound;
                    brand.name = brandName;
                    hasImage = brand && brand.image ? true : false;
                    oldImage = hasImage ? brand.image.id : undefined;

                    if (imageFile) {
                        return this._uploadImage(accountId, image, imageFile);
                    } else {
                        return new Promise((res, rej) => {
                            res(undefined);
                        });
                    }
                }).then(
                (uploadedImage) => {
                    if (uploadedImage) {
                        if (hasImage) {
                            EventService.emit('gallery-image-removed', {
                                image: oldImage,
                                account: accountId
                            }, (err) => {
                                Logger.error('gallery image removal failed', err);
                            });
                        }
                        brand.image = uploadedImage;
                    }
                    return this._brandRepository.update(accountId, brandId, brand);
                }).then(resolve).catch(reject);
        });
    }

    public removeBrand(accountId: string, brandId: string): Promise<any> {
        const promiseList = [];
        return new Promise<any>((resolve, reject) => {
            this._brandRepository.getById(accountId, brandId).then(
                (brand) => {
                    const gallery = _.clone(brand.gallery) || [];
                    promiseList.push(this.bulkRemoveGalleryImage(accountId, brandId, gallery));
                    if (brand.image) {
                        const brandImageId = _.clone(brand.image.id); 
                        this._deleteImage(accountId, brandImageId);
                    }

                    return this._appRepository.search(accountId, <any>{
                        offset: 0, setSize: 10000, query: {
                            field: 'brandId',
                            operator: SearchOperators.Equal,
                            value: brandId
                        }
                    });
                }).then((appsFound) => {
                    if (!appsFound) {
                        return Promise.all(promiseList);
                    }
                    for (let i = 0; i < appsFound.data.length; i++) {
                        promiseList.push(this._appService.remove(accountId, brandId, _.clone(appsFound.data[i].id)));
                    }
                    return Promise.all(promiseList);
                }).then((result) => {
                    return this._brandRepository.remove(accountId, brandId);
                }).then(resolve).catch(reject);
        });
    }

    public getBrandById(accountId: string, brandId: string): Promise<IBrand> {
        return this._brandRepository.getById(accountId, brandId);
    }

    public getGallery(accountId: string, brandId: string): Promise<GalleryImage[]> {
        return this._brandRepository.getById(accountId, brandId).then((brand) => {
            return this._galleryImageRepository.batchGet(accountId, brand.gallery);
        });
    }

    public addGalleryImage(accountId: string, brandId: string, imageTypeId: string,
        imageName: string, imageFile: any): Promise<GalleryImage> {
        const image = <Image>{
            imageType: imageTypeId,
            name: imageName
        };
        let galleryImage;

        return new Promise<GalleryImage>((resolve, reject) => {
            this._uploadImage(accountId, image, imageFile).then(
                (cloudImage) => {
                    return this._addGalleryImage(accountId, cloudImage.name, <any>cloudImage);
                }).then(
                (newGalleryImage: any) => {
                    galleryImage = newGalleryImage;
                    newGalleryImage.brandId = brandId;
                    return this._addGalleryImageIdToBrand(accountId, brandId, <any>newGalleryImage);
                }).then(
                (updateResult) => {
                    return resolve(galleryImage);
                }).catch(reject);
        });
    }

    public replaceGalleryImage(accountId: string, brandId: string, galleryImageId: string,
        imageName: string, imageFile: any): Promise<UpdateResult> {
        let brand;
        let image;
        let galleryImageExist;

        return new Promise<any>((resolve, reject) => {
            this._brandRepository.getById(accountId, brandId).then((brandFound) => {
                brand = brandFound;
                return this._galleryImageRepository.batchGet(accountId, brand.gallery);
            }).then((galleryImages) => {
                galleryImageExist = galleryImages.filter(
                    (oneGallery) => {
                        return oneGallery.id === galleryImageId;
                    });

                if (galleryImageExist.length < 1) {
                    reject(new ItemNotFoundError('Gallery Image Not Found'));
                }

                return this._galleryImageRepository.getById(accountId, galleryImageId);
            }).then((galleryImage) => {
                image = galleryImage;
                image.name = imageName;

                if (imageFile) {
                    return this._uploadImage(accountId, image, imageFile);
                }

                return new Promise((res, rej) => {
                    res(image);
                });
            }).then((updatedGalleryImage: any) => {
                return this._galleryImageRepository.update(accountId, galleryImageId, updatedGalleryImage);
            }).then(resolve).catch(reject);
        });
    }

    public bulkRemoveGalleryImage(accountId: string, brandId: string, galleryImageList: any[]): Promise<any> {
        let cloudinaryOperationPromiseList = [];
        let mongoOperationPromiseList = [];
        return new Promise<any>((resolve, reject) => {
            this._galleryImageRepository.batchGet(accountId, galleryImageList).then(
                (galleryImages: any) => {
                    cloudinaryOperationPromiseList = galleryImages.map((galleryImage) => {
                        return this._deleteImage(accountId, galleryImage.id);
                    });
                    mongoOperationPromiseList = galleryImages.map((galleryImage) => {
                        return this._galleryImageRepository.remove(accountId, galleryImage.id);
                    });
                    return Promise.all(cloudinaryOperationPromiseList.concat(mongoOperationPromiseList));
                }).then(() => {
                    resolve(undefined);
                }).catch(reject);
        });
    }

    public removeGalleryImage(accountId: string, brandId: string, galleryImageId: string): Promise<void> {
        let brand;
        let galleryImageExist;

        return new Promise<void>((resolve, reject) => {
            this._brandRepository.getById(accountId, brandId).then(
                (brandFound) => {
                    brand = brandFound;
                    return this._galleryImageRepository.batchGet(accountId, brand.gallery);
                }).then(
                (galleryImages) => {

                    galleryImageExist = galleryImages.filter(
                        (oneGallery) => {
                            return oneGallery.id === galleryImageId;
                        });

                    if (galleryImageExist.length < 1) {
                        reject(new ItemNotFoundError('Gallery Image Not Found'));
                    }

                    return this._galleryImageRepository.getById(accountId, galleryImageId);
                }).then(
                (galleryImage) => {
                    return this._deleteImage(accountId, galleryImage.id);
                }).then(
                (deletedImage) => {
                    brand.gallery = brand.gallery.filter((oneGallery) => {
                        return oneGallery.id !== galleryImageId;
                    });
                    return this._brandRepository.update(accountId, brandId, brand);
                }).then(
                (deletedBrandResult) => {
                    return this._galleryImageRepository.remove(accountId, galleryImageId);
                }).then(
                (deletedGalleryResult) => {
                    resolve(undefined);
                }).catch(reject);
        });
    }

    protected _addGalleryImage(accountId: string, imageTitle: string, cloudImage: GalleryImage): Promise<GalleryImage> {
        cloudImage.active = false;
        cloudImage.title = imageTitle;
        cloudImage._metadata = MediaGallerySchema.ID;

        return new Promise<GalleryImage>((resolve, reject) => {
            return this._galleryImageRepository.add(accountId, cloudImage).then(resolve).catch(reject);
        });
    }

    protected _addGalleryImageIdToBrand(accountId: string, brandId: string, galleryImage: GalleryImage): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            return this._brandRepository.getById(accountId, brandId).then(
                (brand) => {
                    brand.gallery.push(galleryImage.id);
                    return this._brandRepository.update(accountId, brandId, brand);
                }).then(resolve).catch(reject);
        });
    }

    protected _deleteImage(accountId: string, imageId: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            return this._imageRepository.remove(accountId, imageId).then(resolve).catch(reject);
        });
    }

    protected _uploadImage(accountId: string, imageType: Image, imageFile: any): Promise<Image> {
        return new Promise<Image>((resolve, reject) => {
            this._imageRepository.upload(
                accountId,
                imageType,
                streamifier.createReadStream(imageFile.buffer)
            ).then(resolve).catch(reject);
        });
    }
}