import Promise = require('bluebird');
import {App} from '../dto/App';
import {Edition} from '../dto/Edition';
import {SearchOperators, UpdateResult, ResultSet} from 'videa-framework/domain/db';

import {AppApplicationServiceConfig} from '../dto/applicationService/AppApplicationServiceConfig';

import MobileApp = require('../schema/appstudio/MobileApp');
import TabletApp = require('../schema/appstudio/TabletApp');
import TVApp = require('../schema/appstudio/TVApp');

import {AppstudioModelFactory} from '../factory/AppstudioModelFactory';

import {ScreenRepository} from '../repository/ScreenRepository';
import {BrandRepository} from '../repository/BrandRepository';
import {UIConfigRepository} from '../repository/UIConfigRepository';
import {EditionRepository} from '../repository/EditionRepository';
import {AppRepository} from '../repository/AppRepository';
import {ClientConfigRepository} from '../repository/ClientConfigRepository';

import * as _ from 'lodash';
import {EditionApplicationService} from "./EditionApplicationService";
import {ItemNotFoundError} from 'videa-framework/VideaError';

export class AppApplicationService {

    private _uiConfigRepository: UIConfigRepository;
    private _appRepository: AppRepository;
    private _editionRepository: EditionRepository;
    private _brandRepository: BrandRepository;
    private _clientConfigRepository: ClientConfigRepository;
    private _screenRepository: ScreenRepository;
    private _editionApplicationService: EditionApplicationService;

    constructor(config: AppApplicationServiceConfig) {
        this._uiConfigRepository = config.uiConfigRepository;
        this._appRepository = config.appRepository;
        this._editionRepository = config.editionRepository;
        this._brandRepository = config.brandRepository;
        this._clientConfigRepository = config.clientConfigRepository;
        this._screenRepository = config.screenRepository;
        this._editionApplicationService = config.editionApplicationService;
    }

    public create(accountId: string, brandId: string, app: App): Promise<App> {
        let brand;
        let edition;
        let uiConfigType;
        let uiConfig;
        let editionType;

        return new Promise<App>((resolve, reject) => {
            this._brandRepository.getById(accountId, brandId).then((brandFound) => {
                brand = brandFound;
                app.brandId = brandId;
                return this._appRepository.add(accountId, app);
            }).then((newApp) => {
                app = newApp;
                brand.app.push(app.id);
                return this._brandRepository.update(accountId, brandId, brand);
            }).then((updateResult) => {
                switch (app._metadata) {
                    case MobileApp.ID:
                        editionType = 'MobileAppEdition';
                        uiConfigType = 'MobileUIConfig';
                        break;
                    case TabletApp.ID:
                        editionType = 'TabletAppEdition';
                        uiConfigType = 'TabletUIConfig';
                        break;
                    case TVApp.ID:
                        editionType = 'TVAppEdition';
                        uiConfigType = 'TVUIConfig';
                        break;
                    default:
                        break;
                }

                uiConfig = AppstudioModelFactory.create(uiConfigType, {
                    name: 'Version'
                });
                return this._editionRepository.add(accountId, <Edition>AppstudioModelFactory.create(editionType, {
                    name: 'Edition',
                    appId: app.id,
                    state: 'IN_PROGRESS'
                }));
            }).then((newEdition) => {
                edition = newEdition;
                app.activeEdition = '';
                app.edition = [edition.id];
                return this._appRepository.update(accountId, app.id, app);
            }).then((updateResult) => {
                uiConfig.editionId = edition.id;
                return this._uiConfigRepository.add(accountId, uiConfig);
            }).then((newConfig) => {
                edition.uiConfig = [newConfig.id];
                return this._editionRepository.update(accountId, edition.id, edition);
            }).then((updateResult) => {
                resolve(app);
            }).catch(reject);
        });
    }

    public update(accountId: string, brandId: string, appId: string, app: App): Promise<UpdateResult> {
        return new Promise<UpdateResult>((resolve, reject) => {
            this._appRepository.getById(accountId, appId).then((appFound) => {
                app.brandId = appFound.brandId;
                return this._appRepository.replace(accountId, appId, app);
            }).then(resolve).catch(reject);
        });
    }

    private duplicateEditions(editionIds: string[], fromAccountId: string, toAccountId: string, fromAppId: string, toApp: App) {
        let promise: any = Promise.resolve();

        editionIds.forEach((editionId) => {
            // chain the promises together
            promise = promise.then(() => {
                return this._editionApplicationService.duplicateEdition(fromAccountId, fromAppId, editionId, toAccountId, toApp);
            });
        });

        return promise;
    }

    private createNewAppFromExistingApp(existApp: App, toAccountId: string, toBrand): Promise<App> {
        let newApp: App;
        let newAppObj: App = <App>_.omit(existApp, ['id', 'createdDate', 'modifiedDate', 'edition', 'activeEdition', 'clientConfig']);

        // init the new app
        newAppObj.name = newAppObj.name + ' Copy';
        newAppObj.edition = [];
        newAppObj.activeEdition = '';
        newAppObj.brandId = toBrand.id;

        return this._appRepository.add(toAccountId, newAppObj).then((app) => {
            newApp = app;

            // add the new app to the specified brand
            toBrand.app.push(newApp.id);
            return this._brandRepository.update(toAccountId, toBrand.id, toBrand);
        }).then(() => {
            return newApp;
        });
    }

    public duplicate(fromAccountId: string, fromBrandId, toAccountId: string, toBrandId: string, appId: string): Promise<App> {
        return new Promise<App>((resolve, reject) => {
            let appToBeDuplicate: App;
            let newApp: App;
            let fromBrand;
            let toBrand;

            // if toAccountId and toBrandId is not set, default to duplicate the app under the same account and brand
            toAccountId = toAccountId || fromAccountId;
            toBrandId = toBrandId || fromBrandId;

            this._appRepository.getById(fromAccountId, appId).then((appFound: App) => {
                if (appFound) {
                    appToBeDuplicate = appFound;
                    return appToBeDuplicate;
                } else {
                    return reject(new ItemNotFoundError('The app to be duplicated is not found'));
                }
            }).then((app: App) => {
                // get from brand and to brand
                return Promise.all([
                    this._brandRepository.getById(fromAccountId, app.brandId).then((brand) => {
                        fromBrand = brand;
                    }),
                    this._brandRepository.getById(toAccountId, toBrandId).then((brand) => {
                        toBrand = brand;
                    })
                ]);
            }).then(() => {
                return this.createNewAppFromExistingApp(appToBeDuplicate, toAccountId, toBrand);
            }).then((createdApp) => {
                newApp = createdApp;
                const editions = _.get(appToBeDuplicate, 'edition', []);
                return this.duplicateEditions(editions, fromAccountId, toAccountId, appToBeDuplicate.id, newApp);
            }).then(() => {
                return this._appRepository.update(toAccountId, newApp.id, newApp);
            }).then(() => {
                return this._appRepository.getById(toAccountId, newApp.id);
            }).then(resolve).catch(reject);
        });
    }

    public remove(accountId: string, brandId: string, appId: string): Promise<void> {
        const editions = [];
        const configs = [];
        let app;
        const promiseList = [];
        let brand;
        return new Promise<void>((resolve, reject) => {
            this._brandRepository.getById(accountId, brandId).then(
                (brandFound) => {
                    brand = brandFound;
                    return this._appRepository.getById(accountId, appId);
                }).then((appFound) => {
                    app = appFound;
                    promiseList.push(this._appRepository.remove(accountId, appId));

                    brand.app = brand.app.filter((singleAppId) => {
                        return singleAppId !== appId;
                    });

                    promiseList.push(this._brandRepository.update(accountId, brandId, brand));

                    return this._editionRepository.search(accountId, <any>{
                        offset: 0, setSize: 10000, query: {
                            field: 'appId',
                            operator: SearchOperators.Equal,
                            value: appId
                        }
                    });
                }).then((editionFound) => {
                    for (let i = 0; i < editionFound.data.length; i++) {
                        editions.push(editionFound.data[i].id);
                        promiseList.push(this._editionRepository.remove(accountId, editionFound.data[i].id));
                        if (editionFound.data[i].clientConfig && editionFound.data[i].clientConfig.length > 0) {
                            promiseList.push(this._clientConfigRepository.remove(accountId, editionFound.data[i].clientConfig));
                        }
                    }
                    return this._uiConfigRepository.search(accountId, <any>{
                        offset: 0, setSize: 10000, query: {
                            field: 'editionId',
                            operator: SearchOperators.In,
                            value: editions
                        }
                    });
                }).then((configFound) => {
                    for (let i = 0; i < configFound.data.length; i++) {
                        configs.push(configFound.data[i].id);
                        promiseList.push(this._uiConfigRepository.remove(accountId, configFound.data[i].id));
                    }
                    return this._screenRepository.search(accountId, <any>{
                        offset: 0, setSize: 10000, query: {
                            field: 'configId',
                            operator: SearchOperators.In,
                            value: configs
                        }
                    });
                }).then((screenFound) => {
                    for (let i = 0; i < screenFound.data.length; i++) {
                        promiseList.push(this._screenRepository.remove(accountId, screenFound.data[i].id));
                    }
                    return Promise.all(promiseList);
                }).then(() => {
                    resolve(undefined);
                }).catch(reject);
        });
    }

    public getById(accountId: string, appId: string): Promise<App> {
        return this._appRepository.getById(accountId, appId);
    }

    public getByBrand(accountId: string, brandId: string): Promise<ResultSet> {
        return this._appRepository.search(accountId, <any>{
            offset: 0, setSize: 100000, query: {
                field: 'brandId',
                operator: SearchOperators.Equal,
                value: brandId
            }
        });
    }
}