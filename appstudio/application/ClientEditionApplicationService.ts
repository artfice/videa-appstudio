import Promise = require('bluebird');
import { SearchParams } from 'videa-framework/domain/db/SearchParams';

import MobileApp = require('../schema/appstudio/MobileApp');
import TabletApp = require('../schema/appstudio/TabletApp');
import TVApp = require('../schema/appstudio/TVApp');

import { BrandRepository } from '../repository/BrandRepository';
import { EditionRepository } from '../repository/EditionRepository';
import { AppRepository } from '../repository/AppRepository';
import { EditionStateApplicationService } from './EditionStateApplicationService';

import { ClientEditionApplicationServiceConfig } from '../dto/applicationService/ClientEditionApplicationServiceConfig';

export class ClientEditionApplicationService {

    private _appRepository: AppRepository;
    private _editionRepository: EditionRepository;
    private _brandRepository: BrandRepository;
    private _editionStateService: EditionStateApplicationService;

    constructor(config: ClientEditionApplicationServiceConfig) {
        this._appRepository = config.appRepository;
        this._editionRepository = config.editionRepository;
        this._brandRepository = config.brandRepository;
        this._editionStateService = config.editionStateService;
    }
    
    public getEditions(accountId: string, brandId: string, editionId: string,
        state: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this._editionRepository.search(accountId, <SearchParams>{}).then(
                (editionsFound) => {
                    let editions = editionsFound.data.filter((edition) => {
                        return edition.uiConfig.length > 0;
                    });
                    if (editionId) {
                        editions = editions.filter((edition) => {
                            return edition.id === editionId;
                        });
                    }
                    if (this._editionStateService.getStateTypes().indexOf(state) > -1) {
                        editions = editions.filter((edition) => {
                            return edition.state === state;
                        });
                    }
                    editions = editions.map((edition) => {
                        return this._appRepository.getById(accountId,
                            edition.appId).then((app) => {
                                let type;
                                switch (app._metadata) {
                                    case MobileApp.ID:
                                        type = 'mobile';
                                        break;
                                    case TabletApp.ID:
                                        type = 'tablet';
                                        break;
                                    case TVApp.ID:
                                        type = 'tv';
                                        break;
                                    default:
                                        break;
                                }
                                let clientEdition = {
                                    id: edition.id,
                                    name: edition.name,
                                    clientConfigId: edition.clientConfig,
                                    state: edition.state,
                                    app: app.name,
                                    type: type,
                                    brand: '',
                                    brandId: app.brandId
                                };
                                return this._brandRepository.getById(accountId,
                                    app.brandId).then((brand) => {
                                        clientEdition.brand = brand.name;
                                        return clientEdition;
                                    }).catch((e) => {
                                        return {
                                            id: edition.id,
                                            name: edition.name,
                                            clientConfigId: edition.clientConfig,
                                            state: edition.state,
                                            app: '',
                                            type: '',
                                            brand: '',
                                            brandId: '',
                                            invalid: true
                                        };
                                    });
                            }).catch((e) => {
                                    return {
                                        id: edition.id,
                                        name: edition.name,
                                        clientConfigId: edition.clientConfig,
                                        state: edition.state,
                                        app: '',
                                        type: '',
                                        brand: '',
                                        brandId: '',
                                        invalid: true
                                    };                                    
                                });
                    });
                    return Promise.all(editions);
                }).then((editions) => {
                    resolve(editions.filter((edition) => {
                        if (edition.invalid) {
                            return false;
                        }                        
                        if (brandId && edition.brandId === brandId) {
                            delete edition.brandId;
                            return true;
                        } else if (!brandId) {
                            return true;
                        } else {
                            return false;
                        }
                    }));
                }).catch(reject);
        });
    }
}