import Promise = require('bluebird');
import { Edition } from '../dto/Edition';
import { App } from '../dto/App';

import { UpdateResult } from 'videa-framework/domain/db/UpdateResult';

import { EditionApplicationServiceConfig } from '../dto/applicationService/EditionApplicationServiceConfig';

import { SearchOperators } from 'videa-framework/domain/db';
import { ResultSet } from 'videa-framework/domain/db/ResultSet';
import { EditionStateApplicationService } from './EditionStateApplicationService';
import { AppPublishingApplicationService } from './AppPublishingApplicationService';

import MobileAppEdition = require('../schema/appstudio/app/mobile/MobileAppEdition');
import TabletAppEdition = require('../schema/appstudio/app/tablet/TabletAppEdition');
import TVAppEdition = require('../schema/appstudio/app/tv/TVAppEdition');
import MobileBundleEdition = require('../schema/appstudio/app/mobile/MobileBundleEdition');
import TabletBundleEdition = require('../schema/appstudio/app/tablet/TabletBundleEdition');
import TVBundleEdition = require('../schema/appstudio/app/tv/TVBundleEdition');
import ExternalEdition = require('../schema/appstudio/app/ExternalEdition');
import {AppstudioModelFactory} from '../factory/AppstudioModelFactory';

import { ScreenRepository } from '../repository/ScreenRepository';
import { UIConfigRepository } from '../repository/UIConfigRepository';
import { EditionRepository } from '../repository/EditionRepository';
import { AppRepository } from '../repository/AppRepository';
import { ClientConfigRepository } from '../repository/ClientConfigRepository';
import { EventService } from 'videa-framework/EventService';

export class EditionApplicationService {

    private _configRepository: UIConfigRepository;
    private _editionRepository: EditionRepository;
    private _editionStateService: EditionStateApplicationService;
    private _appRepository: AppRepository;
    private _clientConfigRepository: ClientConfigRepository;
    private _appPublishingService: AppPublishingApplicationService;
    private _screenRepository: ScreenRepository;

    constructor(config: EditionApplicationServiceConfig) {
        this._configRepository = config.uiConfigRepository;
        this._editionRepository = config.editionRepository;
        this._editionStateService = config.editionStateApplicationService;
        this._appRepository = config.appRepository;
        this._clientConfigRepository = config.clientConfigRepository;
        this._screenRepository = config.screenRepository;
        this._appPublishingService = config.appPublishingApplicationService;
    }

    public create(accountId: string, appId: string, rawEdition: Edition): Promise<Edition> {
        let edition;
        let uiConfigType;
        return new Promise<Edition>((resolve, reject) => {
            this._appRepository.getById(accountId, appId).then((appFound) => {
                rawEdition.appId = appFound.id;
                rawEdition.state = 'IN_PROGRESS';
                return this._editionRepository.add(accountId, rawEdition);
            }).then((newEdition) => {
                edition = newEdition;
                switch (edition._metadata) {
                    case MobileAppEdition.ID:
                    case MobileBundleEdition.ID:
                        uiConfigType = 'MobileUIConfig';
                        break;
                    case TabletAppEdition.ID:
                    case TabletBundleEdition.ID:
                        uiConfigType = 'TabletUIConfig';
                        break;
                    case TVAppEdition.ID:
                    case TVBundleEdition.ID:
                        uiConfigType = 'TVUIConfig';
                        break;
                    case ExternalEdition.ID:
                        uiConfigType = 'ExternalUIConfig';
                        break;
                    default:
                        break;
                }
                const uiConfig = AppstudioModelFactory.create(uiConfigType, {
                    editionId: edition.id,
                    name: 'Version'
                });

                return this._configRepository.add(accountId, <any>uiConfig);
            }).then((newConfig: any) => {
                edition.uiConfig = [newConfig.id];
                return this._editionRepository.replace(accountId, edition.id, edition);
            }).then((updateResult) => {
                resolve(edition);
            }).catch(reject);
        });
    }

    public update(accountId: string, appId: string, edition: Edition): Promise<UpdateResult> {
        const editionId = edition.id;
        let result;
        return new Promise<UpdateResult>((resolve, reject) => {
            this._editionRepository.getById(accountId, editionId).then((editionFound) => {
                edition.appId = editionFound.appId;
                return this._editionRepository.replace(accountId, editionId, edition);
            }).then((updateResult) => {
                result = updateResult;
                if (edition.uiConfig.length > 0) {
                    return this._appPublishingService.publishEdition(accountId, edition.id);
                } else {
                    resolve(result);
                }
            }).then(resolve).catch(reject);
        });
    }

    public remove(accountId: string, appId: string, editionId: string): Promise<void> {
        const self = this;

        return new Promise<void>((resolve, reject) => {
            const promiseList = [];
            this._appRepository.getById(accountId, appId).then((app) => {
                app.edition = app.edition.filter((editionRef) => {
                    return editionRef !== editionId;
                });
                promiseList.push(this._appRepository.replace(accountId, appId, app));
                return this._editionRepository.getById(accountId, editionId);
            }).then((edition) => {
                promiseList.push(this._editionRepository.remove(accountId, editionId));
                if (edition.clientConfig && edition.clientConfig.length > 0) {
                    promiseList.push(this._clientConfigRepository.remove(accountId, edition.clientConfig));
                }

                return this._configRepository.search(accountId, <any>{
                    offset: 0, setSize: 10000, query: {
                        field: 'editionId',
                        operator: SearchOperators.Equal,
                        value: editionId
                    }
                });
            }).then((uiConfigList) => {
                const uiConfigs = [];
                for (let i = 0; i < uiConfigList.data.length; i++) {
                    uiConfigs.push(uiConfigList.data[i].id);
                    promiseList.push(self._configRepository.remove(accountId, uiConfigList.data[i].id));
                }
                return this._screenRepository.search(accountId, <any>{
                    offset: 0, setSize: 10000, query: {
                        field: 'configId',
                        operator: SearchOperators.In,
                        value: uiConfigs
                    }
                });
            }).then((screenFound) => {
                for (let i = 0; i < screenFound.data.length; i++) {
                    promiseList.push(this._screenRepository.remove(accountId, screenFound.data[i].id));
                }
                return Promise.all(promiseList);
            }).then((results) => {
                resolve(undefined);
            }).catch(reject);
        });
    }

    public getById(accountId: string, editionId: string): Promise<Edition> {
        return this._editionRepository.getById(accountId, editionId);
    }

    public duplicateEdition(accountId: string, appId: string, editionId: string, toAccountId?: string, toApp?: App): Promise<Edition> {
        let edition;
        let app;
        let editionUiConfigs = [];
        let rawEdition;
        let promiseList = [];
        const newEditionUiConfigs = [];
        let mapping = {};
        let newScreenList = [];

        // if this is not specified, it defaults to be the same as accountId
        toAccountId = toAccountId || accountId;

        return new Promise<Edition>((resolve, reject) => {
            this._appRepository.getById(accountId, appId).then(
                (appFound) => {
                    app = appFound;
                    toApp = toApp || app;
                    return this._editionRepository.getById(accountId, editionId);
                }).then(
                (editionFound) => {
                    rawEdition = editionFound;
                    editionUiConfigs = rawEdition.uiConfig;
                    rawEdition.uiConfig = [];
                    rawEdition.clientConfig = '';
                    rawEdition.state = 'IN_PROGRESS';
                    rawEdition.name = rawEdition.name + ' Copy';
                    if (toApp && toApp.id) {
                        rawEdition.appId = toApp.id;
                    }
                    delete rawEdition.id;
                    delete rawEdition.createdDate;
                    delete rawEdition.modifiedDate;
                    return this._editionRepository.add(toAccountId, rawEdition);
                }).then(
                (newEdition) => {
                    edition = newEdition;
                    toApp.edition = toApp.edition || [];
                    toApp.edition.push(edition.id);
                    return this._appRepository.update(toAccountId, toApp.id, toApp);
                }).then(
                (updateResult) => {
                    return this._configRepository.batchGet(accountId, editionUiConfigs);
                }).then(
                (uiConfigsFound: any) => {
                    //make a copy of the uiConfig
                    promiseList = uiConfigsFound.map(
                        (uiConfig) => {
                            const oldUiConfigId = uiConfig.id;
                            delete uiConfig.id;
                            delete uiConfig.createdDate;
                            delete uiConfig.modifiedDate;

                            for (let i = 0; i < uiConfig.screen; i++) {
                                mapping[uiConfig.screen[i]] = false;
                            }

                            uiConfig.editionId = edition.id;
                            return this._configRepository.add(toAccountId, uiConfig).then(
                                (newUiConfig) => {
                                    mapping[oldUiConfigId] = newUiConfig.id;
                                    newEditionUiConfigs.push(newUiConfig.id);
                                    const uiConfigScreens = uiConfig.screen;
                                    newUiConfig.screen = [];
                                    //copy screens into uiConfig
                                    const p = uiConfigScreens.map((screenId: any) => {
                                        return new Promise((res, rej) => {
                                            this._screenRepository.getById(accountId, screenId).then(
                                                (screen: any) => {
                                                    delete screen.id;
                                                    screen.configId = newUiConfig.id;
                                                    return this._screenRepository.add(toAccountId, screen);
                                                }).then((newScreen: any) => {
                                                newUiConfig.screen.push(newScreen.id);
                                                mapping[screenId] = newScreen.id;
                                                newScreenList.push(newScreen.id);
                                                return res(undefined);
                                            }).catch(rej);
                                        });
                                    });
                                    return Promise.all(p).then((results) => {
                                        const mappingKeys = Object.keys(mapping);
                                        for (let i = 0; i < mappingKeys.length; i++) {
                                            if (mapping[mappingKeys[i]]) {
                                                uiConfig = EditionApplicationService.searchAndReplace(uiConfig,
                                                    'screenId', mappingKeys[i], mapping[mappingKeys[i]]);
                                                uiConfig = EditionApplicationService.searchAndReplace(uiConfig,
                                                    'configId', mappingKeys[i], mapping[mappingKeys[i]]);
                                            }
                                        }
                                        return this._configRepository.update(toAccountId, uiConfig.id, uiConfig);
                                    });
                                });
                        });
                    return Promise.all(promiseList);
                }).then(() => {
                //put uiConfig into edition
                edition.uiConfig = newEditionUiConfigs;
                return this._editionRepository.update(toAccountId, edition.id, edition);
            }).then((updateResult) => {
                const mappingKeys = Object.keys(mapping);
                const promiseList = newScreenList.map((screenId) => {
                    return new Promise((res, rej) => {
                        return this._screenRepository.getById(toAccountId, screenId).then(
                            (screen: any) => {
                                for (let i = 0; i < mappingKeys.length; i++) {
                                    if (mapping[mappingKeys[i]]) {
                                        screen = EditionApplicationService.searchAndReplace(screen,
                                            'screenId', mappingKeys[i], mapping[mappingKeys[i]]);
                                        screen = EditionApplicationService.searchAndReplace(screen,
                                            'screenIdToNavigateOnEnd', mappingKeys[i], mapping[mappingKeys[i]]);
                                        screen = EditionApplicationService.searchAndReplace(screen,
                                            'configId', mappingKeys[i], mapping[mappingKeys[i]]);
                                    }
                                }
                                return this._screenRepository.update(toAccountId, screen.id, screen);
                            }).then(res).catch(rej);
                    });
                });
                return Promise.all(promiseList);
            }).then(() => {
                return this._editionRepository.getById(toAccountId, edition.id);
            }).then(() => {
                EventService.emit('retranslate-edition', {
                    accountId: toAccountId,
                    editionId: edition.id
                });

                return edition;
            }).then(resolve).catch(reject);
        });
    }

    public static searchAndReplace(obj: any, target: string, oldValue: any, replaceValue: any) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (key === target && obj[key] === oldValue) {
                    obj[key] = replaceValue;
                } else if (obj[key] !== null && typeof obj[key] === 'object') {
                    obj[key] = EditionApplicationService.searchAndReplace(obj[key], target, oldValue, replaceValue);
                }
            }
        }
        return obj;
    }
    public getByApp(accountId: string, appId: string): Promise<ResultSet> {
        return this._editionRepository.search(accountId, <any>{
            offset: 0, setSize: 100000, query: {
                field: 'appId',
                operator: SearchOperators.Equal,
                value: appId
            }
        });
    }
}