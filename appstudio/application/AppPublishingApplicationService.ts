import Promise = require('bluebird');
import shortid = require('shortid');
import {EditionRepository} from '../repository/EditionRepository';
import {AppRepository} from '../repository/AppRepository';
import {ClientConfigRepository} from '../repository/ClientConfigRepository';
import {ITranslationService} from '../interface/ITranslationService';
import {IAppPublishingApplicationServiceConfig} from '../dto/applicationService/AppPublishingApplicationServiceConfig';

export class AppPublishingApplicationService {

    private _appRepository: AppRepository;
    private _editionRepository: EditionRepository;
    private _translationService: ITranslationService;
    private _clientConfigRepository: ClientConfigRepository;

    constructor(config: IAppPublishingApplicationServiceConfig) {
        this._appRepository = config.appRepository;
        this._editionRepository = config.editionRepository;
        this._translationService = config.translationService;
        this._clientConfigRepository = config.clientConfigRepository;
    }

    public publishEdition(accountId: string, editionId: string): Promise<any> {
        let clientApp;
        let edition;
        let clientAppId;
        let oldEditionId;
        let editionUpdateResult;
        return new Promise<any>((resolve, reject) => {
            this._editionRepository.getById(accountId, editionId).then(
                (editionFound) => {
                    edition = editionFound;
                    return this._translationService.translate(accountId, edition);
                }).then((translatedEdition) => {
                    clientApp = translatedEdition;
                    clientAppId = edition.clientConfig ? edition.clientConfig : shortid.generate();
                    if (edition.clientConfig && edition.clientConfig.length > 0) {
                        return this._clientConfigRepository.remove(accountId, edition.clientConfig);
                    }
                }).then((removeClientConfig) => {
                    clientApp.id = clientAppId;
                    return this._clientConfigRepository.add(accountId, clientApp);
                }).then((clientConfig) => {
                    edition.clientConfig = clientConfig.id;
                    return this._editionRepository.replace(accountId, editionId, edition);
                }).then((updateResult) => {
                    editionUpdateResult = updateResult;
                    if (edition.state === 'PUBLISHED') {
                        return this._appRepository.getById(accountId, edition.appId);
                    } else {
                        return resolve(editionUpdateResult);
                    }
                }).then((app) => {
                    if (!app || app.success) {
                        return resolve(editionUpdateResult);
                    }
                    //if published edition, update app client config
                    oldEditionId = (app && app.activeEdition) ? app.activeEdition : '';
                    app.activeEdition = edition.id;
                    app.clientConfig = edition.clientConfig;
                    return this._appRepository.replace(accountId, app.id, app);
                }).then((updateResult) => {
                    //if app has a published edition, the last published edition
                    //moves to completed
                    if (oldEditionId && oldEditionId.length > 0 && oldEditionId !== editionId) {
                        return this._editionRepository.getById(accountId, oldEditionId);
                    } else {
                        return resolve(updateResult);
                    }
                }).then((oldPublishedEdition) => {
                    if (oldPublishedEdition) {
                        oldPublishedEdition.state = 'COMPLETED';
                        return this._editionRepository.replace(accountId, oldPublishedEdition.id, oldPublishedEdition);
                    } else {
                        return resolve(oldPublishedEdition);
                    }
                }).then(resolve).catch(reject);
        });
    }
}