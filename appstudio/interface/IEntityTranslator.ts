///<reference path="../../typings/index.d.ts"/>

import Promise = require('bluebird');
import { ITranslationService } from './ITranslationService';
import { MetadataOwner } from '../dto/MetadataOwner';

export interface IEntityTranslator<T extends MetadataOwner, R extends MetadataOwner> {
    translate(accountId: string, service: ITranslationService, source: T): Promise<R>;
}