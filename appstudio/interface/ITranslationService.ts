
export interface ITranslationService {
    registerTranslator(translator: any, config: any);
    translate(accountId: string, source: any): any;
}
