import MobileMpxCmsSchema = require('../../schema/appstudio/app/mobile/cms/MobileMpx');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class MobileMpxCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = MobileMpxCmsSchema.ID;
}