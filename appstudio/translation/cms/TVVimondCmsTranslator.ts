import TvVimondCmsSchema = require('../../schema/appstudio/app/tv/cms/TvVimondCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TVVimondCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TvVimondCmsSchema.ID;
}