import TVGenericCmsSchema = require('../../schema/appstudio/app/tv/cms/TvGenericCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TVGenericCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TVGenericCmsSchema.ID;
}