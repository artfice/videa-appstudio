import MobileCustomCmsSchema = require('../../schema/appstudio/app/mobile/cms/MobileCustomCms');
import {CustomDataTranslator} from '../CustomDataTranslator';

export class MobileCustomCmsTranslator extends CustomDataTranslator {
    public static SOURCE_TYPE = MobileCustomCmsSchema.ID;
}