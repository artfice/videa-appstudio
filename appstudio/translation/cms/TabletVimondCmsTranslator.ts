import TabletVimondCmsSchema = require('../../schema/appstudio/app/tablet/cms/TabletVimondCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TabletVimondCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TabletVimondCmsSchema.ID;
}