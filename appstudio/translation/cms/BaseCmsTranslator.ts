///<reference path="../../../typings/index.d.ts"/>
import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {Cms as CmsDto} from '../../dto/app/configuration/Cms';

import CMSSchema = require('../../schema/appstudio/app/configuration/Cms');

export class BaseCmsTranslator implements IEntityTranslator<CmsDto, CmsDto> {
    public static SOURCE_TYPE = CMSSchema.ID;

    public translate(accountId: string, translationService: ITranslationService,
    source: CmsDto): Promise<any> {
        return Promise.resolve(ClientAppModelFactory.create(source._metadata, source));
    }
}