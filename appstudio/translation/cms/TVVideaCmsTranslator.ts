import TVVideaCmsSchema = require('../../schema/appstudio/app/tv/cms/TvVideaCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TVVideaCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TVVideaCmsSchema.ID;
}