import TabletGenericCmsSchema = require('../../schema/appstudio/app/tablet/cms/TabletGenericCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TabletGenericCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TabletGenericCmsSchema.ID;
}