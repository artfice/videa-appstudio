import TabletVideaCmsSchema = require('../../schema/appstudio/app/tablet/cms/TabletVideaCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TabletVideaCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TabletVideaCmsSchema.ID;
}