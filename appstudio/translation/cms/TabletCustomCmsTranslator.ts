import TabletCustomCmsSchema = require('../../schema/appstudio/app/tablet/cms/TabletCustomCms');
import {CustomDataTranslator} from '../CustomDataTranslator';

export class TabletCustomCmsTranslator extends CustomDataTranslator {
    public static SOURCE_TYPE = TabletCustomCmsSchema.ID;
}