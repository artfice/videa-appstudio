import TVCustomCmsSchema = require('../../schema/appstudio/app/tv/cms/TVCustomCms');
import {CustomDataTranslator} from '../CustomDataTranslator';
export class TVCustomCmsTranslator extends CustomDataTranslator {
    public static SOURCE_TYPE = TVCustomCmsSchema.ID;
}