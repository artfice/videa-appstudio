import TVMpxCmsSchema = require('../../schema/appstudio/app/tv/cms/TvMpx');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TVMpxCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TVMpxCmsSchema.ID;
}