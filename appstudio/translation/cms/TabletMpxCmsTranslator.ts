import TabletMpxCmsSchema = require('../../schema/appstudio/app/tablet/cms/TabletMpx');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class TabletMpxCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = TabletMpxCmsSchema.ID;
}