import MobileVideaCmsSchema = require('../../schema/appstudio/app/mobile/cms/MobileVideaCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class MobileVideaCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = MobileVideaCmsSchema.ID;
}