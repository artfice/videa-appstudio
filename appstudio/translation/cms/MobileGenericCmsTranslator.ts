import MobileGenericCmsSchema = require('../../schema/appstudio/app/mobile/cms/MobileGenericCms');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class MobileGenericCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = MobileGenericCmsSchema.ID;
}