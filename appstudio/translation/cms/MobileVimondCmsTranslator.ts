import MobileVimondCmsSchema = require('../../schema/appstudio/app/mobile/cms/MobileVimond');

import {BaseCmsTranslator} from './BaseCmsTranslator';

export class MobileVimondCmsTranslator extends BaseCmsTranslator {
    public static SOURCE_TYPE = MobileVimondCmsSchema.ID;
}