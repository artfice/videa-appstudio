import _ = require('underscore');
import Promise = require('bluebird');

import { ITranslationService } from '../../interface/ITranslationService';
import { BaseTranslator } from '../BaseTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import AppstudioMpxProviderSchema = require('../../schema/appstudio/app/configuration/user/MpxProvider');
import MpxProviderSchema = require('../../schema/clientapp/app/user/MpxProvider');
import {BaseProvider as MpxProviderDto} from '../../dto/app/configuration/user/BaseProvider';
import {User as UserDto} from '../../dto/clientapp/app/User';

export class MpxProviderTranslator extends BaseTranslator<MpxProviderDto, UserDto> {
    public static SOURCE_TYPE = AppstudioMpxProviderSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: MpxProviderDto): Promise<UserDto> {
        this.addProviderInfoToTranslation(source);
        return <Promise<UserDto>> super.translate(accountId, service, source);
    }

    private addProviderInfoToTranslation(source: MpxProviderDto): void {
        const providerInfo: UserDto = <UserDto> ClientAppModelFactory.create(MpxProviderSchema.ID);

        if (source) {
            providerInfo.accountId = source.accountId;
            providerInfo.pid = source.pid;
        }
        _.extend(this._translation, providerInfo);
    }
}