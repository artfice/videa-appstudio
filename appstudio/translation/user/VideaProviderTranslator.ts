import _ = require('underscore');
import Promise = require('bluebird');

import { ITranslationService } from '../../interface/ITranslationService';
import { BaseTranslator } from '../BaseTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import AppstudioVideaProviderSchema = require('../../schema/appstudio/app/configuration/user/VideaProvider');
import VideaProviderSchema = require('../../schema/clientapp/app/user/VideaProvider');
import { BaseProvider as VideaProviderDto } from '../../dto/app/configuration/user/BaseProvider';
import { User as UserDto } from '../../dto/clientapp/app/User';

export class VideaProviderTranslator extends BaseTranslator<VideaProviderDto, UserDto> {
    public static SOURCE_TYPE = AppstudioVideaProviderSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: VideaProviderDto): Promise<UserDto> {
        this.addProviderInfoToTranslation();
        return <Promise<UserDto>>super.translate(accountId, service, source);
    }

    private addProviderInfoToTranslation(): void {
        const providerInfo: UserDto = <UserDto>ClientAppModelFactory.create(VideaProviderSchema.ID);
        _.extend(this._translation, providerInfo);
    }
}