import _ = require('lodash');
import Promise = require('bluebird');

import { ITranslationService } from '../../interface/ITranslationService';
import { BaseTranslator } from '../BaseTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import { BaseProvider } from '../../dto/app/configuration/user/BaseProvider';
import NoData = require('../../schema/common/NoData');

import AppstudioUserSchema = require('../../schema/appstudio/app/configuration/User');
import UserSchema = require('../../schema/clientapp/app/User');
import { User as AppstudioUserDto } from '../../dto/app/configuration/User';
import { User as UserDto } from '../../dto/clientapp/app/User';

export class UserTranslator extends BaseTranslator<AppstudioUserDto<BaseProvider>, UserDto> {
    public static SOURCE_TYPE = AppstudioUserSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: AppstudioUserDto<BaseProvider>): Promise<UserDto> {

        const provider = source && source.provider;
        const providerMetaData = provider && provider._metadata;

        return new Promise<UserDto>((resolve, reject) => {
            if (!providerMetaData || (providerMetaData == NoData.ID)) {
                return resolve(null);
            }

            this.addUserInfoToTranslation();

            translationService.translate(accountId, provider).then((providerInfo) => {
                _.extend(this._translation, providerInfo);
                return super.translate(accountId, translationService, source);

            }).then(resolve).catch(reject);
        });
    }

    private addUserInfoToTranslation(): void {
        const userInfo = ClientAppModelFactory.create(UserSchema.ID);
        _.extend(this._translation, userInfo);
    }
}