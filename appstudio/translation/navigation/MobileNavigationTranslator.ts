import MobileNavigationSchema = require('../../schema/appstudio/app/mobile/uiconfig/navigation/MobileNavigation');

import {NavigationTranslator} from './NavigationTranslator';

export class MobileNavigationTranslator extends NavigationTranslator  {

	public static SOURCE_TYPE = MobileNavigationSchema.ID;

	constructor() {
		super();
	}
}