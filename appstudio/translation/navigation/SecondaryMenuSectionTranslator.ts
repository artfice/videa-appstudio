
import Promise = require('bluebird');
import _ = require('underscore');

import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ITranslationService} from '../../interface/ITranslationService';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';
import {TranslationUtil} from '../TranslationUtil';

import SecondaryMenuSectionSchema = require('../../schema/appstudio/app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSection');
import {SecondaryMenuSection as SecondaryMenuSectionDto} from '../../dto/app/configuration/uiconfig/navigation/SecondaryMenuSection';

import {Container as ContainerDto} from '../../dto/clientapp/component/Container';

export class SecondaryMenuSectionTranslator implements IEntityTranslator<SecondaryMenuSectionDto, ContainerDto>  {

    public static SOURCE_TYPE = SecondaryMenuSectionSchema.ID;

    public translate(accountId: string, service: ITranslationService, source: SecondaryMenuSectionDto): Promise<ContainerDto> {
        // const backgroundImage = source && source.backgroundImage;
        const subImages = source.images;

        const sectionContainer = <ContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('RelativeLayout'),
            width: 'wrapContent',
            height: 'fillParent',
            listeners: [],
            style: ClientAppModelFactory.create('Style', {
                padding: '0 0 0 0',
                margin: '0 0 0 0',
                backgroundColor: ClientAppModelFactory.create('Color', {
                    value: '#00000000'
                })
            }),
            items: []
        });

        let p = [];

        // p.push(service.translate(accountId, backgroundImage).then(
        //     (imageDto: ClientImageDto<Style>) => {
        //         if (imageDto) {
        //             sectionContainer.items.push(_.extend(imageDto, {
        //                 scale: 'aspectFit',
        //                 height: 'wrapContent',
        //                 width: 'wrapContent',
        //                 style: ClientAppModelFactory.create('Style', {
        //                     padding: '0 0 0 0',
        //                     margin: '0 0 0 0',
        //                     borderThickness: 2
        //                 })
        //             }));
        //         }
        //         return Promise.resolve();
        //     }
        // ));

        const subImagePromises = _.map(subImages, (subImage: any) => {
            return service.translate(accountId, subImage).then((imageDto) => {
                if (imageDto) {
                    _.extend(imageDto, {
                        gravity: subImage.gravity,
                        visible: subImage.visible,
                        scale: TranslationUtil.translateScale(subImage.scale),
                        width: subImage.width,
                        height: subImage.height,
                        padding: subImage.padding,
                        margin: subImage.margin
                    });

                    sectionContainer.items.push(imageDto);
                }
                return Promise.resolve()
            });
        });

        p = p.concat(subImagePromises);

        return Promise.all(p).then(
            () => {
                return Promise.resolve(sectionContainer);
            }
        );

    }
}