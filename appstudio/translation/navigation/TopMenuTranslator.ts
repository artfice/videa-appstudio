import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import {BaseMenu} from '../../dto/common/navigation/BaseMenu';
import TopMenuSchema = require('../../schema/common/navigation/TopMenu');
import {TopMenu} from '../../dto/clientapp/component/navigation/TopMenu';
import {TopMenu as TopMenuDto} from '../../dto/common/navigation/TopMenu';

import {TopNavContainer as ClientTopNavDto} from '../../dto/clientapp/component/TopNavContainer';

import _ = require('lodash');
import Promise = require('bluebird');

export class TopMenuTranslator implements IEntityTranslator<TopMenuDto, ClientTopNavDto>  {

	public static SOURCE_TYPE = TopMenuSchema.ID;

	public translate(accountId: string, service: ITranslationService,
					 source: TopMenuDto): Promise<ClientTopNavDto> {
		let topMenu = new TopMenu();
		const style = _.get(source, 'style', undefined);
		const leftSection = _.get(source, 'leftSection', undefined);
		const middleSection = _.get(source, 'middleSection', undefined);
		const rightSection = _.get(source, 'rightSection', undefined);

		return new Promise<ClientTopNavDto> ((resolve, reject) => {
			return service.translate(accountId, style).then((translatedStyle) => {
				topMenu.setMenuWidth(translatedStyle && translatedStyle.width);
				topMenu.setMenuHeight(translatedStyle && translatedStyle.height);
				topMenu.setMenuPadding(translatedStyle && translatedStyle.padding);
				topMenu.setMenuMargin(translatedStyle && translatedStyle.margin);
				topMenu.setMenuBackgroundColor(translatedStyle && translatedStyle.backgroundColor);
				return service.translate(accountId, leftSection);
			}).then((translatedLeftSection) => {
				topMenu.setLeftSectionWidth(translatedLeftSection &&
				translatedLeftSection.style && translatedLeftSection.style.width);

				topMenu.setLeftSectionHeight(translatedLeftSection &&
				translatedLeftSection.style && translatedLeftSection.style.height);

				topMenu.setLeftSectionPadding(translatedLeftSection &&
				translatedLeftSection.style && translatedLeftSection.style.padding);

				topMenu.setLeftSectionMargin(translatedLeftSection &&
				translatedLeftSection.style && translatedLeftSection.style.margin);

				topMenu.setLeftSectionBackgroundColor(translatedLeftSection &&
				translatedLeftSection.style && translatedLeftSection.style.backgroundColor);

				topMenu.setLeftSectionVisible(translatedLeftSection &&
				translatedLeftSection.controls && translatedLeftSection.controls.visible);

				topMenu.addAllLeftSectionComponents(translatedLeftSection &&
				translatedLeftSection.content && translatedLeftSection.content.subComponent);

				return service.translate(accountId, middleSection);
			}).then((translatedMiddleSection) => {
				topMenu.setMiddleSectionWidth(translatedMiddleSection &&
				translatedMiddleSection.style && translatedMiddleSection.style.width);

				topMenu.setMiddleSectionHeight(translatedMiddleSection &&
				translatedMiddleSection.style && translatedMiddleSection.style.height);

				topMenu.setMiddleSectionPadding(translatedMiddleSection &&
				translatedMiddleSection.style && translatedMiddleSection.style.padding);

				topMenu.setMiddleSectionMargin(translatedMiddleSection &&
				translatedMiddleSection.style && translatedMiddleSection.style.margin);

				topMenu.setMiddleSectionBackgroundColor(translatedMiddleSection &&
				translatedMiddleSection.style && translatedMiddleSection.style.backgroundColor);

				topMenu.setMiddleSectionVisible(translatedMiddleSection &&
				translatedMiddleSection.controls && translatedMiddleSection.controls.visible);

				topMenu.addAllMiddleSectionComponents(translatedMiddleSection &&
				translatedMiddleSection.content && translatedMiddleSection.content.subComponent);

				return service.translate(accountId, rightSection);
			}).then((translatedRightSection) => {
				topMenu.setRightSectionWidth(translatedRightSection &&
				translatedRightSection.style && translatedRightSection.style.width);

				topMenu.setRightSectionHeight(translatedRightSection &&
				translatedRightSection.style && translatedRightSection.style.height);

				topMenu.setRightSectionPadding(translatedRightSection &&
				translatedRightSection.style && translatedRightSection.style.padding);

				topMenu.setRightSectionMargin(translatedRightSection &&
				translatedRightSection.style && translatedRightSection.style.margin);

				topMenu.setRightSectionBackgroundColor(translatedRightSection &&
				translatedRightSection.style && translatedRightSection.style.backgroundColor);

				topMenu.setRightSectionVisible(translatedRightSection &&
				translatedRightSection.controls && translatedRightSection.controls.visible);

				topMenu.addAllRightSectionComponents(translatedRightSection &&
				translatedRightSection.content && translatedRightSection.content.subComponent);

				return topMenu.getContainer();
			}).then(resolve).catch(reject);
		});
	}
}