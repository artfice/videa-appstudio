import _ = require('underscore');
import Promise = require('bluebird');

import { ITranslationService } from '../../interface/ITranslationService';
import { IEntityTranslator } from '../../interface/IEntityTranslator';

import TVHackNavigationSchema = require('../../schema/appstudio/app/tv/uiconfig/navigation/TVHackNavigation');

import { Navigation as ClientNavigationDto } from '../../dto/clientapp/navigation/Navigation';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

export class TVHackNavigationTranslator implements IEntityTranslator<any, any>  {

    public static SOURCE_TYPE = TVHackNavigationSchema.ID;

    constructor() {
    }

    public translate(accountId: string, translationService: ITranslationService,
        tvNavigation: any): Promise<ClientNavigationDto> {
        return new Promise<any>((resolve, reject) => {
            const content = tvNavigation && tvNavigation.customData && tvNavigation.customData.content;
            let navigationDto;

            if (content) {
                try {
                    var contentObj = JSON.parse(content);
                    if (_.isEmpty(contentObj)) {
                        navigationDto = null;
                    } else {
                        navigationDto = ClientAppModelFactory.create('TopMenu', contentObj);
                    }
                    resolve(navigationDto);
                } catch (e) {
                    reject(new Error('invalid json ' + JSON.stringify(e, null, 4)));
                }
            } else {
                navigationDto = ClientAppModelFactory.create('TopMenu');
                resolve(navigationDto);
            }
        });
    }
}