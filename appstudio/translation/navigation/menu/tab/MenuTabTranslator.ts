import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';
import {IEntityTranslator} from '../../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../../interface/ITranslationService';

import TabSchema = require('../../../../schema/appstudio/app/configuration/uiconfig/navigation/Tab');

import {Tabs as TabsDto} from '../../../../dto/common/navigation/tabs/Tabs';

import Promise = require('bluebird');
import _ = require('lodash');

export class MenuTabTranslator implements IEntityTranslator<TabsDto, TabsDto> {

    public static SOURCE_TYPE = TabSchema.ID;

    protected _translation: any;

    constructor() {
        this._translation = ClientAppModelFactory.create('MenuTab', {
            width: 'fillParent',
            height: 'wrapContent'
        });
    }

    public translate(accountId: string, service: ITranslationService, source: TabsDto): Promise<TabsDto> {
        const label = source.label ? source.label : null;
        const icon = source.icon ? source.icon : null;
        const style = source.style ? source.style : null;
        const controls = source.controls ? source.controls : null;
        let translation = this._translation;

        return new Promise<any>((resolve, reject) => {
            service.translate(accountId, label).then(
                (translatedLabel) => {
                    translation.label = translatedLabel;
                    return service.translate(accountId, icon);
                }).then((translatedIcon) => {
                    translation.icon = translatedIcon;
                    return service.translate(accountId, style);
                }).then((translatedStyle) => {
                    translation.style = translatedStyle;
                    return service.translate(accountId, controls);
                }).then((translatedControls) => {
                    translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                    translation.listeners = translatedControls && translatedControls.listeners ? translatedControls.listeners : [];
                    resolve(translation);
                }).catch(reject);
        });
    }
}