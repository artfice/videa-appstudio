import Promise = require('bluebird');

import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

import BottomMenuSchema = require('../../../schema/common/navigation/BottomMenu');
import {BottomMenu as BottomMenuDto} from '../../../dto/common/navigation/BottomMenu';

import {Navigation as ClientNavigationDto} from '../../../dto/clientapp/navigation/Navigation';

export class BottomMenuTranslator implements IEntityTranslator<BottomMenuDto, ClientNavigationDto>  {

    public static SOURCE_TYPE = BottomMenuSchema.ID;

    protected _translation : ClientNavigationDto;

	constructor() {
        this._translation = <ClientNavigationDto>ClientAppModelFactory.create('ClientNavigation', {
            mainMenu: <BottomMenuDto>ClientAppModelFactory.create('BottomMenu')
        });
	}

    public translate(accountId: string, service: ITranslationService, source: BottomMenuDto): Promise<ClientNavigationDto> {

        let tabs = source && source.tab ? source.tab : [];
        const style = source.style ? source.style : null;

        tabs = tabs.map((tab) => {
            return service.translate(accountId, tab);
        });

        return new Promise<ClientNavigationDto> ((resolve, reject) => {
            Promise.all(tabs).then((translatedTabs) => {
                this._translation.tabs = translatedTabs ? translatedTabs : [];
                return service.translate(accountId, style);
            }).then((translatedStyle) => {
                this._translation.style = translatedStyle ? translatedStyle : undefined;
                return resolve(this._translation);
            }).catch(reject);
        });
    }
}