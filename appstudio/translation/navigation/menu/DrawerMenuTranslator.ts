import Promise = require('bluebird');
import _ = require('underscore');

import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

import DrawerMenuSchema = require('../../../schema/common/navigation/DrawerMenu');
import {DrawerMenu as DrawerMenuDto} from '../../../dto/common/navigation/DrawerMenu';

import {Navigation as ClientNavigationDto} from '../../../dto/clientapp/navigation/Navigation';

export class DrawerMenuTranslator implements IEntityTranslator<DrawerMenuDto, ClientNavigationDto>  {

    static SOURCE_TYPE = DrawerMenuSchema.ID;

    protected _translation : ClientNavigationDto;

	constructor() {
        this._translation = <ClientNavigationDto>ClientAppModelFactory.create('ClientNavigation', {
            mainMenu: <DrawerMenuDto>ClientAppModelFactory.create('DrawerMenu')
        });
	}

    public translate(accountId: string, service: ITranslationService, source: DrawerMenuDto): Promise<ClientNavigationDto> {

        let sections = source && source.section ? source.section : [];
        const secondaryMenu = source && source.secondaryMenu;

        sections = _.map(sections, function(section){
            return service.translate(accountId, section);
        });

        return new Promise<ClientNavigationDto> ((resolve, reject) => {
            Promise.all(sections).then(
                (translatedSections) =>{
                    this._translation.section = translatedSections ? translatedSections : [];
                    return service.translate(accountId, secondaryMenu);
            }).then(
                (translatedSecondaryMenu) =>{
                    this._translation.secondaryMenu = translatedSecondaryMenu;
                    resolve(this._translation);
            }).catch(reject);
        });
    }
}