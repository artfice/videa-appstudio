import _ = require('underscore');
import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';

import {SecondaryMenu as SecondaryMenuDto} from '../../dto/app/configuration/uiconfig/navigation/SecondaryMenu';
import {Container as ContainerDto} from '../../dto/clientapp/component/Container';

import SecondaryMenuSchema = require('../../schema/appstudio/app/configuration/uiconfig/navigation/SecondaryMenu')

import {SecondaryMenu} from '../../dto/clientapp/component/navigation/SecondaryMenu';

export class SecondaryMenuTranslator implements IEntityTranslator<SecondaryMenuDto, ContainerDto>    {

	public static SOURCE_TYPE = SecondaryMenuSchema.ID;

	protected _secondaryMenu : SecondaryMenu;

	constructor() {
		this._secondaryMenu = new SecondaryMenu({});
	}

	public translate(accountId: string, service: ITranslationService,
					 secondaryMenu: SecondaryMenuDto): Promise<ContainerDto> {
		const settings = secondaryMenu && secondaryMenu.settings;
		let texts = secondaryMenu && secondaryMenu.text ? secondaryMenu.text : [];
		let sections = secondaryMenu && secondaryMenu.sections ? secondaryMenu.sections : [];
		const controls = settings && settings.controls;
		const visible = controls && controls.visible ? controls.visible : 'true';
		const styles = settings && settings.style;

		texts = _.map(texts, function(label){
            return service.translate(accountId, label);
        });

		sections = _.map(sections, function(section){
            return service.translate(accountId, section);
        });

		return new Promise<ContainerDto> ((resolve, reject) => {
			service.translate(accountId, styles).then((translatedStyle) => {
                this._secondaryMenu.setWidth(translatedStyle && translatedStyle.width);
                this._secondaryMenu.setHeight(translatedStyle && translatedStyle.height);
                this._secondaryMenu.setPadding(translatedStyle && translatedStyle.padding);
                this._secondaryMenu.setMargin(translatedStyle && translatedStyle.margin);
                this._secondaryMenu.setBackgroundColor(translatedStyle && translatedStyle.backgroundColor);
                return Promise.all(texts);
			}).then((translatedText) => {
                this._secondaryMenu.addLabels(translatedText);
                return service.translate(accountId, controls);
			}).then((translatedControls) => {
                this._secondaryMenu.setVisible(visible);
                this._secondaryMenu.setListener(translatedControls && translatedControls.listeners);
                return Promise.all(sections);
			}).then((translatedSections) => {
                this._secondaryMenu.setSections(translatedSections);
                resolve(this._secondaryMenu.getContainer());
			}).catch(reject);
		});
	}
}