import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';

import TabletNavigationSchema = require('../../schema/appstudio/app/tablet/uiconfig/navigation/TabletNavigation');

import {Navigation as AppStudioNavigationDto} from '../../dto/app/configuration/uiconfig/Navigation';
import {BaseMenu} from '../../dto/common/navigation/BaseMenu';

import {NavigationTranslator} from './NavigationTranslator';

import {Navigation as ClientNavigationDto} from '../../dto/clientapp/navigation/Navigation';

export class TabletNavigationTranslator extends NavigationTranslator  {

	public static SOURCE_TYPE = TabletNavigationSchema.ID;

	constructor() {
		super();
	}

	public translate(accountId: string, translationService: ITranslationService,
					 tabletNavigation: AppStudioNavigationDto<BaseMenu>): Promise<ClientNavigationDto> {

		return super.translate(accountId, translationService, <AppStudioNavigationDto<BaseMenu>>tabletNavigation);
	}
}