import Promise = require('bluebird');
import _ = require('lodash');

import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import NavigationSchema = require('../../schema/appstudio/app/configuration/uiconfig/Navigation');
import {Navigation as AppStudioNavigationDto} from '../../dto/app/configuration/uiconfig/Navigation';
import {BaseMenu} from '../../dto/common/navigation/BaseMenu';
import BlankMenuSchema = require('../../schema/common/navigation/BlankMenu');
import TVBlankMenuSchema = require('../../schema/common/navigation/TVBlankMenu');

import {Navigation as ClientNavigationDto} from '../../dto/clientapp/navigation/Navigation';

export class NavigationTranslator implements IEntityTranslator<AppStudioNavigationDto<BaseMenu>, ClientNavigationDto>  {

    public static SOURCE_TYPE = NavigationSchema.ID;

    protected _clientNavigation: ClientNavigationDto;

    constructor() {
        this._clientNavigation = <ClientNavigationDto>ClientAppModelFactory.create('ClientNavigation');
    }

    public translate(accountId: string, service: ITranslationService, source: AppStudioNavigationDto<BaseMenu>): Promise<ClientNavigationDto> {

        const mainMenu = source && source.mainMenu;

        return new Promise<ClientNavigationDto>((resolve, reject) => {
            if (this.isBlankMenu(mainMenu)) {
                return resolve(undefined);
            }

            service.translate(accountId, mainMenu).then(
                (translatedMenu) => {
                    if (translatedMenu && translatedMenu.mainMenu) {
                         _.merge(this._clientNavigation, translatedMenu);
                    } else {
                        this._clientNavigation = translatedMenu;
                    }
                    resolve(this._clientNavigation);
                }).catch(reject);
        });
    }

    protected isBlankMenu (mainMenu: BaseMenu) : boolean {
        const blankMenus = [TVBlankMenuSchema.ID, BlankMenuSchema.ID];
        return mainMenu && (blankMenus.indexOf(mainMenu._metadata) > -1);
    }
}