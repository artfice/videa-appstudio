import Promise = require('bluebird');
import _ = require('underscore');

import {SectionStyle as AppstudioSectionStyleDto} from '../../../dto/app/configuration/uiconfig/navigation/sections/SectionStyle';
import SectionStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/section/style/SectionStyle');

import {SizedComponentStyleTranslator} from '../../component/SizedComponentStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

export class SectionStyleTranslator<T extends AppstudioSectionStyleDto, R extends ClientBaseStyleDto> extends SizedComponentStyleTranslator<AppstudioSectionStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = SectionStyleSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioSectionStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;
        let backgroundColor = source && source.backgroundColor;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {

            service.translate(accountId, backgroundColor).then((translatedBackgroundColor) => {
                _.extend(translation, {
                    backgroundColor: translatedBackgroundColor
                });

                return translation;
            }).then(() => {
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}