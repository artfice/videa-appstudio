import {TopMenuStyle as AppstudioTopMenuStyleDto} from '../../../dto/common/navigation/top/TopMenuStyle';
import TopMenuStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/style/TopMenuStyle');

import {ITranslationService} from '../../../interface/ITranslationService';
import {SizedComponentStyleTranslator} from '../../component/SizedComponentStyleTranslator';

import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

import Promise = require('bluebird');
import _ = require('lodash');

export class TopMenuStyleTranslator<T extends AppstudioTopMenuStyleDto, R extends ClientBaseStyleDto> extends SizedComponentStyleTranslator<AppstudioTopMenuStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = TopMenuStyleSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioTopMenuStyleDto): Promise<ClientBaseStyleDto> {
        let backgroundColor = source && source.backgroundColor;
        let translation = this._translation;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            return service.translate(accountId, backgroundColor).then((translatedBackgroundColor) => {
                _.extend(translation, {
                    backgroundColor: translatedBackgroundColor
                });
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}
