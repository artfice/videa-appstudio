///<reference path="../../../../typings/index.d.ts"/>
import Promise = require('bluebird');
import _ = require('underscore');

import {SecondaryMenuStyle as AppstudioSecondaryMenuStyleDto} from '../../../dto/app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuStyle';
import SecondaryMenuStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/secondarymenu/style/SecondaryMenuStyle');

import {SizedComponentStyleTranslator} from '../../component/SizedComponentStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

export class SecondaryMenuStyleTranslator<T extends AppstudioSecondaryMenuStyleDto, R extends ClientBaseStyleDto> extends SizedComponentStyleTranslator<AppstudioSecondaryMenuStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = SecondaryMenuStyleSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioSecondaryMenuStyleDto): Promise<ClientBaseStyleDto> {
        let backgroundColor = source && source.backgroundColor;
        let translation = this._translation;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            service.translate(accountId, backgroundColor).then((translatedBackgroundColor) => {
                _.extend(translation, {
                    backgroundColor: translatedBackgroundColor
                });
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}