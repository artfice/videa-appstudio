import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';
import {BottomMenuStyle as AppstudioBottomMenuStyleDto} from '../../../dto/common/navigation/tabs/BottomMenuStyle';
import {ColoredComponentStyleTranslator} from '../../component/ColoredComponentStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import Promise = require('bluebird');
import _ = require('underscore');

import BottomMenuStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/style/BottomMenuStyle');

export class BottomMenuStyleTranslator<T extends AppstudioBottomMenuStyleDto, 
R extends ClientBaseStyleDto> extends ColoredComponentStyleTranslator<AppstudioBottomMenuStyleDto, 
ClientBaseStyleDto>  {

    public static SOURCE_TYPE = BottomMenuStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioBottomMenuStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;
        let selectedColor = source && source.selectedColor;
        let font = source && source.font;

        translation.selectedColor = selectedColor;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            service.translate(accountId, font).then((translatedFont) => {
                translation.font = translatedFont;
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}