import Promise = require('bluebird');
import _ = require('underscore');

import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import {Section as SectionDto} from '../../dto/app/configuration/uiconfig/navigation/Section';
import SectionSchema = require('../../schema/appstudio/app/configuration/uiconfig/navigation/Section');

import {Container as ContainerDto} from '../../dto/clientapp/component/Container';
import {Section} from '../../dto/clientapp/component/navigation/Section';

export class SectionTranslator implements IEntityTranslator<SectionDto, ContainerDto>  {

    public static SOURCE_TYPE = SectionSchema.ID;

    protected _section: Section;

    constructor(config) {
        this._section = new Section();
    }

    public translate(accountId: string, service: ITranslationService, source: SectionDto): Promise<ContainerDto> {
        const settings = source && source.settings;
        const style = settings && settings.style;
        const controls = settings && settings.controls;
        const visible = controls && controls.visible ? controls.visible : 'true';
        let listeners = controls && controls.listeners ? controls.listeners : [];
        const label = source && source.label;
        const image = source && source.image;

        listeners = _.map(listeners, function (listener) {
            return service.translate(accountId, listener);
        });

        return new Promise<ContainerDto>((resolve, reject) => {
            Promise.all(listeners).then((translatedListeners) => {
                this._section.setVisible(visible);
                this._section.setListener(translatedListeners);
                return service.translate(accountId, style);
            }).then((translatedStyle) => {
                this._section.setWidth(translatedStyle && translatedStyle.width);
                this._section.setHeight(translatedStyle && translatedStyle.height);
                this._section.setPadding(translatedStyle && translatedStyle.padding);
                this._section.setMargin(translatedStyle && translatedStyle.margin);
                this._section.setBackgroundColor(translatedStyle && translatedStyle.backgroundColor);
                return service.translate(accountId, label);
            }).then((translatedLabel) => {
                this._section.setLabelContainer(translatedLabel);
                return service.translate(accountId, image);
            }).then((translatedImage) => {
                this._section.setImageContainer(translatedImage);
                resolve(this._section.getContainer());
            }).catch(reject);
        });
    }
}