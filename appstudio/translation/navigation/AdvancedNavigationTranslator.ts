import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import AdvancedNavigationSchema = require('../../schema/appstudio/app/tv/uiconfig/navigation/AdvancedNavigation');

import {Navigation as ClientNavigationDto} from '../../dto/clientapp/navigation/Navigation';

import _ = require('lodash');
import Promise = require('bluebird');

export class AdvancedNavigationTranslator implements IEntityTranslator<any, any>  {

	public static SOURCE_TYPE = AdvancedNavigationSchema.ID;

	public translate(accountId: string, translationService: ITranslationService,
					 tvNavigation): Promise<ClientNavigationDto> {
            let navigation = null;
            const content = tvNavigation && tvNavigation.customData && tvNavigation.customData.content;
            return new Promise<any>((resolve, reject) => {
                if (content) {
                    try {
                        var contentObj = JSON.parse(content);

                        if (_.isEmpty(contentObj)) {
                            navigation = null;
                        } else {
                            navigation = contentObj;
                        }
                        return resolve(navigation);
                    } catch(e) {
                        return reject(new Error('invalid json ' + JSON.stringify(e, null, 4)));
                    }
                } else {
                    return resolve(navigation);
                }
            });
	}
}