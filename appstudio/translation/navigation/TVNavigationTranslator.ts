import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';

import TVNavigationSchema = require('../../schema/appstudio/app/tv/uiconfig/navigation/TVNavigation');
import {Navigation as AppStudioNavigationDto} from '../../dto/app/configuration/uiconfig/Navigation';
import {BaseMenu} from '../../dto/common/navigation/BaseMenu';

import {NavigationTranslator} from './NavigationTranslator';

import {Navigation as ClientNavigationDto} from '../../dto/clientapp/navigation/Navigation';

export class TVNavigationTranslator extends NavigationTranslator  {

	public static SOURCE_TYPE = TVNavigationSchema.ID;

	constructor() {
		super();
	}

	public translate(accountId: string, translationService: ITranslationService,
					 tvNavigation: AppStudioNavigationDto<BaseMenu>): Promise<ClientNavigationDto> {

		return super.translate(accountId, translationService, <AppStudioNavigationDto<BaseMenu>>tvNavigation);
	}
}