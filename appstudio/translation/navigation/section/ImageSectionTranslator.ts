import Promise = require('bluebird');
import _ = require('underscore');

import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {Image as ImageDto} from '../../../dto/app/configuration/uiconfig/navigation/sections/Image';
import ImageSectionSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/section/Image');

import {Container as ContainerDto} from '../../../dto/clientapp/component/Container';
import {Section as Section} from '../../../dto/clientapp/component/navigation/Section';

export class ImageSectionTranslator implements IEntityTranslator<ImageDto, ContainerDto>  {

	public static SOURCE_TYPE = ImageSectionSchema.ID;

	protected _imageContainer : ContainerDto;

	constructor(config) {
        const section = new Section();
		this._imageContainer = section.getImageContainer();
	}

	public translate(accountId: string, service: ITranslationService, source: ImageDto): Promise<ContainerDto> {
		const settings = source && source.settings;
        const style = settings && settings.style;
		let images = source && source.image ? source.image : [];

        images = _.map(images, function(image){
            return service.translate(accountId, image);
        });

        return new Promise<ContainerDto> ((resolve, reject) => {
			Promise.all(images).then((translatedImage) => {
				this._imageContainer.items = translatedImage ? translatedImage : [];
				return service.translate(accountId, style);
			}).then((translatedStyle) => {
				this._imageContainer.style.padding = translatedStyle && translatedStyle.padding;
				this._imageContainer.style.margin = translatedStyle && translatedStyle.margin;
				this._imageContainer.style.backgroundColor = translatedStyle && translatedStyle.backgroundColor;
				this._imageContainer.style.border = translatedStyle && translatedStyle.border;
				resolve(this._imageContainer);
			}).catch(reject);
		});
	}
}