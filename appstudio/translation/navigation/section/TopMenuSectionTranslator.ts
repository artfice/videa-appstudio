import TopMenuSectionSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/TopMenuSection');
import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {TopMenuSection as AppstudioTopMenuSectionDto} from '../../../dto/common/navigation/top/TopMenuSection';

import _ = require('lodash');
import Promise = require('bluebird');

export class TopMenuSectionTranslator implements IEntityTranslator<AppstudioTopMenuSectionDto, AppstudioTopMenuSectionDto> {
    public static SOURCE_TYPE = TopMenuSectionSchema.ID;

    public translate(accountId: string, service: ITranslationService,
        source: AppstudioTopMenuSectionDto):
        Promise<AppstudioTopMenuSectionDto> {
        const content = _.get(source, 'content', undefined);
        const subComponents = _.get(content, 'subComponent', []);
        const style = _.get(source, 'style', undefined);
        const control = _.get(source, 'controls', undefined);
        const numSubComponents = subComponents.length;
        let translation = <AppstudioTopMenuSectionDto>{};

        const promiseList = subComponents.map((component) => {
            return service.translate(accountId, component);
        });

        return Promise.all(promiseList).then((componentList) => {
            _.merge(translation, {
                content: {
                    subComponent: componentList
                }
            });
            return service.translate(accountId, control);
        }).then((translatedControls) => {
            _.merge(translation, {
                controls: translatedControls
            });
            return service.translate(accountId, style);
        }).then((translatedStyle) => {
            _.merge(translation, {
                style: translatedStyle
            });
            return Promise.resolve(translation);
        });
    }
}