import Promise = require('bluebird');
import _ = require('underscore');

import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {Label as LabelDto} from '../../../dto/app/configuration/uiconfig/navigation/sections/Label';
import LabelSectionSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/section/Label');

import {Container as ContainerDto} from '../../../dto/clientapp/component/Container';
import {Section} from '../../../dto/clientapp/component/navigation/Section';

export class LabelSectionTranslator implements IEntityTranslator<LabelDto, ContainerDto>  {

	public static SOURCE_TYPE = LabelSectionSchema.ID;

	protected _labelContainer : ContainerDto;

	constructor(config) {
        const section = new Section();
		this._labelContainer = section.getLabelContainer();
	}

	public translate(accountId: string, service: ITranslationService, source: LabelDto): Promise<ContainerDto> {
		const settings = source && source.settings;
        const style = settings && settings.style;
		let texts = source && source.text ? source.text : [];

        texts = _.map(texts, function(text){
            return service.translate(accountId, text);
        });

        return new Promise<ContainerDto> ((resolve, reject) => {
			Promise.all(texts).then( (translatedText) => {
				this._labelContainer.items = translatedText ? translatedText : [];
				return service.translate(accountId, style);
			}).then((translatedStyle) => {
				this._labelContainer.style.padding = translatedStyle && translatedStyle.padding;
				this._labelContainer.style.margin = translatedStyle && translatedStyle.margin;
				this._labelContainer.style.backgroundColor = translatedStyle && translatedStyle.backgroundColor;
				this._labelContainer.style.border = translatedStyle && translatedStyle.border;
				resolve(this._labelContainer);
			}).catch(reject);
		});
	}
}