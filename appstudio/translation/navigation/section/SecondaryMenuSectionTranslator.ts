import Promise = require('bluebird');
import _ = require('underscore');

import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import SecondaryMenuSectionSchema = require('../../../schema/appstudio/app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSection');
import {SecondaryMenuSection as SecondaryMenuSectionDto} from '../../../dto/app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSection';

import {Container as ContainerDto} from '../../../dto/clientapp/component/Container';

import {SecondaryMenuSection} from '../../../dto/clientapp/component/navigation/SecondaryMenuSection';

export class SecondaryMenuSectionTranslator implements IEntityTranslator<SecondaryMenuSectionDto, ContainerDto>  {

	public static SOURCE_TYPE = SecondaryMenuSectionSchema.ID;

	protected _section : SecondaryMenuSection;

	constructor(config) {
		this._section = new SecondaryMenuSection();
	}

	public translate(accountId: string, service: ITranslationService, source: SecondaryMenuSectionDto): Promise<ContainerDto> {
		const settings = source && source.settings;
		const style = settings && settings.style;
		const controls = settings && settings.controls;
		const visible = controls && controls.visible ? controls.visible : 'true';
		let images = source && source.image;

		images = _.map(images, function(image){
            return service.translate(accountId, image);
        });

		return new Promise<ContainerDto> ((resolve, reject) => {
			service.translate(accountId, style).then((translatedStyle) => {
                this._section.setPadding(translatedStyle && translatedStyle.padding);
                this._section.setMargin(translatedStyle && translatedStyle.margin);
                this._section.setBackgroundColor(translatedStyle && translatedStyle.backgroundColor);
                return Promise.all(images);
			}).then((translatedImage) =>{
                this._section.setImages(translatedImage);
                return service.translate(accountId, controls);
			}).then((translatedControls) => {
                this._section.setVisible(visible);
                this._section.setListener(translatedControls && translatedControls.listeners);
                resolve(this._section.getContainer());
            }).catch(reject);
		});
	}
}