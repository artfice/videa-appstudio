import {TvGoogleAnalyticTranslator} from './uiConfig/analytic/TvGoogleAnalyticTranslator';
import {TabletGoogleAnalyticTranslator} from './uiConfig/analytic/TabletGoogleAnalyticTranslator';
import {MobileGoogleAnalyticTranslator} from './uiConfig/analytic/MobileGoogleAnalyticTranslator';
import {CustomAnalyticTranslator} from './uiConfig/analytic/CustomAnalyticTranslator';
import {CustomDataTranslator} from './CustomDataTranslator';
import {GoogleAnalyticTranslator} from './uiConfig/analytic/GoogleAnalyticTranslator';
import {AdvanceComponentTranslator} from './component/AdvanceComponentTranslator';
import {LocalProviderTranslator} from './watchhistory/LocalProviderTranslator';
import {MpxProviderTranslator} from './watchhistory/MpxProviderTranslator';
import {WatchHistoryTranslator} from './watchhistory/WatchHistoryTranslator';
import {BottomMenuTranslator} from './navigation/menu/BottomMenuTranslator';
import {MenuTabTranslator} from './navigation/menu/tab/MenuTabTranslator';
import {BottomMenuStyleTranslator} from './navigation/style/BottomMenuStyleTranslator';
import {DrawerMenuTranslator} from './navigation/menu/DrawerMenuTranslator';
import {SecondaryMenuStyleTranslator} from './navigation/style/SecondaryMenuStyleTranslator';
import {SectionStyleTranslator} from './navigation/style/SectionStyleTranslator';
import {ImageSectionTranslator} from './navigation/section/ImageSectionTranslator';
import {LabelSectionTranslator} from './navigation/section/LabelSectionTranslator';
import {ColoredComponentStyleTranslator} from './component/ColoredComponentStyleTranslator';
import {TextFieldTranslator} from './field/TextFieldTranslator';
import {TextFieldContentTranslator} from './field/content/TextFieldContentTranslator';
import {TextFieldStyleTranslator} from './field/style/TextFieldStyleTranslator';
import {ToggleTranslator} from './field/ToggleTranslator';
import {ToggleStyleTranslator} from './field/style/ToggleStyleTranslator';
import {ChromecastTranslator} from './chromecast/ChromecastTranslator';
import {ChromecastThemeTranslator} from './chromecast/ChromecastThemeTranslator';
import {ChromecastTitleTranslator} from './chromecast/ChromecastTitleTranslator';
import {ChromecastReceiverTranslator} from './chromecast/ChromecastReceiverTranslator';
import {CarouselControlsTranslator} from './dataview/CarouselControlsTranslator';
import {ImageContentTranslator} from './field/content/ImageContentTranslator';
import {ScreenStyleTranslator} from './screen/ScreenStyleTranslator';
import {MobileAuthenticationTranslator} from './authentication/MobileAuthenticationTranslator';
import {TabletAuthenticationTranslator} from './authentication/TabletAuthenticationTranslator';
import {TVAuthenticationTranslator} from './authentication/TVAuthenticationTranslator';
import {BaseFieldStyleTranslator} from './field/style/BaseFieldStyleTranslator';
import {DataViewTileTranslator} from './dataview/tile/DataViewTileTranslator';
import {CarouselTileTranslator} from './dataview/tile/CarouselTileTranslator';
import {CollectionPickerContentTranslator} from './dataview/picker/CollectionPickerContentTranslator';
import {DataViewHeadingTranslator} from './dataview/heading/DataViewHeadingTranslator';
import {UIDCollectionTranslator} from './dataview/collection/UIDCollectionTranslator';
import {RemoteCollectionTranslator} from './dataview/collection/RemoteCollectionTranslator';
import {OfflineCollectionTranslator} from './dataview/collection/OfflineCollectionTranslator';
import {LinkCollectionTranslator} from './dataview/collection/LinkCollectionTranslator';
import {SizedComponentStyleTranslator} from './component/SizedComponentStyleTranslator';
import {NamedComponentTranslator} from './component/NamedComponentTranslator';
import {ComponentTranslator} from './component/ComponentTranslator';
import {CollectionPickerStyleTranslator} from './dataview/style/CollectionPickerStyleTranslator';
import {TileViewStyleTranslator} from './dataview/style/TileViewStyleTranslator';
import {DataViewHeadingStyleTranslator} from './dataview/style/DataViewHeadingStyleTranslator';
import {SectionTranslator} from './navigation/SectionTranslator';
import {SecondaryMenuSectionTranslator} from './navigation/SecondaryMenuSectionTranslator';
import {SecondaryMenuTranslator} from './navigation/SecondaryMenuTranslator';
import {NavigationTranslator} from './navigation/NavigationTranslator';
import {TVNavigationTranslator} from './navigation/TVNavigationTranslator';
import {TabletNavigationTranslator} from './navigation/TabletNavigationTranslator';
import {MobileNavigationTranslator} from './navigation/MobileNavigationTranslator';
import {ListenerTranslator} from './ListenerTranslator';
import {AuthenticationTranslator} from './authentication/AuthenticationTranslator';
import {ComponentStyleTranslator} from './component/ComponentStyleTranslator';
import {LabelStyleTranslator} from './field/style/LabelStyleTranslator';
import {ImageStyleTranslator} from './field/style/ImageStyleTranslator';
import {LabelContentTranslator} from './field/content/LabelContentTranslator';
import {FieldContentTranslator} from './field/content/FieldContentTranslator';
import {ScreenTranslator} from './screen/ScreenTranslator';
import {AdvanceScreenTranslator} from './screen/AdvanceScreenTranslator';
import {SearchResultScreenTranslator} from './screen/SearchResultScreenTranslator';
import {CarouselTranslator} from './dataview/CarouselTranslator';
import {SwimlaneTranslator} from './dataview/tileview/SwimlaneTranslator';
import {ListViewTranslator} from './dataview/tileview/ListViewTranslator';
import {GridViewTranslator} from './dataview/tileview/GridViewTranslator';
import {ImageTranslator} from './field/ImageTranslator';
import {LabelTranslator} from './field/LabelTranslator';
import {CollectionsViewTranslator} from './dataview/CollectionsViewTranslator';
import {MobileScreenTranslator} from './screen/MobileScreenTranslator';
import {TabletScreenTranslator} from './screen/TabletScreenTranslator';
import {TvScreenTranslator} from './screen/TvScreenTranslator';
import {MobileSearchResultScreenTranslator} from './screen/MobileSearchResultScreenTranslator';
import {TabletSearchResultScreenTranslator} from './screen/TabletSearchResultScreenTranslator';
import {TVSearchResultScreenTranslator} from './screen/TVSearchResultScreenTranslator';
import {TVHackNavigationTranslator} from './navigation/TVHackNavigationTranslator';
import {CarouselStyleTranslator} from './dataview/style/CarouselStyleTranslator';
import {GridViewStyleTranslator} from './dataview/style/GridViewStyleTranslator';
import {ComponentControlsTranslator} from './component/ComponentControlsTranslator';
import {ExternalConfigTranslator} from './uiConfig/ExternalConfigTranslator';
import {GemiusAnalyticsTranslator} from './uiConfig/analytic/GemiusAnalyticsTranslator';
import {MobileGemiusAnalyticsTranslator} from './uiConfig/analytic/MobileGemiusAnalyticsTranslator';
import {TabletGemiusAnalyticsTranslator} from './uiConfig/analytic/TabletGemiusAnalyticsTranslator';
import {TvGemiusAnalyticsTranslator} from './uiConfig/analytic/TvGemiusAnalyticsTranslator';
import {MobileCustomCmsTranslator} from './cms/MobileCustomCmsTranslator';
import {MobileGenericCmsTranslator} from './cms/MobileGenericCmsTranslator';
import {MobileMpxCmsTranslator} from './cms/MobileMpxCmsTranslator';
import {MobileVideaCmsTranslator} from './cms/MobileVideaCmsTranslator';
import {MobileVimondCmsTranslator} from './cms/MobileVimondCmsTranslator';
import {TabletCustomCmsTranslator} from './cms/TabletCustomCmsTranslator';
import {TabletGenericCmsTranslator} from './cms/TabletGenericCmsTranslator';
import {TabletMpxCmsTranslator} from './cms/TabletMpxCmsTranslator';
import {TabletVideaCmsTranslator} from './cms/TabletVideaCmsTranslator';
import {TabletVimondCmsTranslator} from './cms/TabletVimondCmsTranslator';
import {TVCustomCmsTranslator} from './cms/TVCustomCmsTranslator';
import {TVGenericCmsTranslator} from './cms/TVGenericCmsTranslator';
import {TVMpxCmsTranslator} from './cms/TVMpxCmsTranslator';
import {TVVideaCmsTranslator} from './cms/TVVideaCmsTranslator';
import {TVVimondCmsTranslator} from './cms/TVVimondCmsTranslator';
import {ExternalEditionTranslator} from './edition/ExternalEditionTranslator';
import {BaseEditionTranslator} from './edition/BaseEditionTranslator';
import {MobileAppEditionTranslator} from './edition/MobileAppEditionTranslator';
import {TabletAppEditionTranslator} from './edition/TabletAppEditionTranslator';
import {TVAppEditionTranslator} from './edition/TVAppEditionTranslator';
import {MobileBundleEditionTranslator} from './edition/MobileBundleEditionTranslator';
import {TabletBundleEditionTranslator} from './edition/TabletBundleEditionTranslator';
import {TVBundleEditionTranslator} from './edition/TVBundleEditionTranslator';

import {GalleryImageTranslator} from './uiConfig/theme/GalleryImageTranslator';
import {UIConfigTranslator} from './uiConfig/UIConfigTranslator';
import {TabletUiConfigTranslator} from './uiConfig/TabletUiConfigTranslator';
import {MobileUiConfigTranslator} from './uiConfig/MobileUiConfigTranslator';
import {TvUiConfigTranslator} from './uiConfig/TvUiConfigTranslator';
import {GalleryImageRepository} from '../repository/GalleryImageRepository';
import {UIConfigRepository} from '../repository/UIConfigRepository';
import {ScreenApplicationService} from '../application/ScreenApplicationService';

import {BaseFontTranslator} from './font/BaseFontTranslator';
import {OpenSansTranslator} from './font/OpenSansTranslator';
import {RobotoTranslator} from './font/RobotoTranslator';
import {SanFranciscoTranslator} from './font/SanFranciscoTranslator';
import {CustomFontTranslator} from './font/CustomFontTranslator';

import {StandardAspectRatioTranslator} from './aspectRatio/StandardAspectRatioTranslator';
import {CustomAspectRatioTranslator} from './aspectRatio/CustomAspectRatioTranslator';

import {ProgressBarTranslator} from './field/ProgressBarTranslator';
import {ProgressBarStyleTranslator} from './field/style/ProgressBarStyleTranslator';

import {FavoritesTranslator} from './favorites/FavoritesTranslator';
import {LocalProviderTranslator as FavoritesLocalProviderTranslator} from './favorites/LocalProviderTranslator';

import {MpxProviderTranslator as UserMpxProviderTranslator} from './user/MpxProviderTranslator';
import {VideaProviderTranslator as UserVideaProviderTranslator} from './user/VideaProviderTranslator';
import {UserTranslator as UserTranslator} from './user/UserTranslator';

import {ColorTranslator} from './color/ColorTranslator';
import {OffsetColorTranslator} from './color/OffsetColorTranslator';
import {GradientColorTranslator} from './color/GradientColorTranslator';
import {ComplexColorTranslator} from './color/ComplexColorTranslator';

import {SearchSettingsTranslator} from './uiConfig/search/SearchSettingsTranslator';
import {FavoriteCollectionTranslator} from './dataview/collection/FavoriteCollectionTranslator';
import {WatchHistoryCollectionTranslator} from './dataview/collection/WatchHistoryCollectionTranslator';

import {WebViewTranslator} from './webview/WebViewTranslator';
import {WebViewStyleTranslator} from './webview/WebViewStyleTranslator';
import {WebViewContentTranslator} from './webview/WebViewContentTranslator';
import {WelcomeScreenTranslator} from './WelcomeScreenTranslator';
import {SystemEventTranslator} from './uiConfig/analytic/systemEvents/SystemEventTranslator';
import {LayerTranslator} from './component/LayerTranslator';
import {TileLayerTranslator} from './component/TileLayerTranslator';
import {LayerControlsTranslator} from './component/LayerControlsTranslator';
import {TopMenuLayerTranslator}  from './component/TopMenuLayerTranslator';
import {AdvancedNavigationTranslator} from './navigation/AdvancedNavigationTranslator';
import {TopMenuSectionTranslator}  from './navigation/section/TopMenuSectionTranslator';
import {TopMenuStyleTranslator}  from './navigation/style/TopMenuStyleTranslator';
import {TopMenuTranslator}  from './navigation/TopMenuTranslator';


export class TranslatorList {
    private _galleryImageRepository: GalleryImageRepository;
    private _uiConfigRepository: UIConfigRepository;
    private _screenApplicationService: ScreenApplicationService;

    constructor(galleryImageRepository: GalleryImageRepository,
        screenApplicationService: ScreenApplicationService,
        uiConfigRepository: UIConfigRepository) {
        this._galleryImageRepository = galleryImageRepository;
        this._screenApplicationService = screenApplicationService;
        this._uiConfigRepository = uiConfigRepository;
    }

    public getList(): any[] {
        return [{
            translatorClass: TvGoogleAnalyticTranslator
        }, {
            translatorClass: TabletGoogleAnalyticTranslator
        }, {
            translatorClass: MobileGoogleAnalyticTranslator
        }, {
            translatorClass: CustomAnalyticTranslator
        }, {
            translatorClass: CustomDataTranslator
        }, {
            translatorClass: GoogleAnalyticTranslator
        }, {
            translatorClass: AdvanceComponentTranslator
        }, {
            translatorClass: LocalProviderTranslator
        }, {
            translatorClass: MpxProviderTranslator
        }, {
            translatorClass: WatchHistoryTranslator
        }, {
            translatorClass: BottomMenuTranslator
        }, {
            translatorClass: BottomMenuStyleTranslator
        }, {
            translatorClass: MenuTabTranslator
        }, {
            translatorClass: DrawerMenuTranslator
        }, {
            translatorClass: SecondaryMenuStyleTranslator
        }, {
            translatorClass: SectionStyleTranslator
        }, {
            translatorClass: ImageSectionTranslator
        }, {
            translatorClass: LabelSectionTranslator
        }, {
            translatorClass: ColoredComponentStyleTranslator
        }, {
            translatorClass: TextFieldTranslator
        }, {
            translatorClass: TextFieldContentTranslator
        }, {
            translatorClass: TextFieldStyleTranslator
        }, {
            translatorClass: ToggleTranslator
        }, {
            translatorClass: ToggleStyleTranslator
        }, {
            translatorClass: ChromecastTranslator
        }, {
            translatorClass: ChromecastThemeTranslator
        }, {
            translatorClass: ChromecastTitleTranslator
        }, {
            translatorClass: ChromecastReceiverTranslator
        }, {
            translatorClass: CarouselControlsTranslator
        }, {
            translatorClass: ImageContentTranslator
        }, {
            translatorClass: ScreenStyleTranslator
        }, {
            translatorClass: MobileAuthenticationTranslator
        }, {
            translatorClass: TabletAuthenticationTranslator
        }, {
            translatorClass: TVAuthenticationTranslator
        }, {
            translatorClass: BaseFieldStyleTranslator
        }, {
            translatorClass: DataViewTileTranslator
        }, {
            translatorClass: CarouselTileTranslator
        }, {
            translatorClass: CollectionPickerContentTranslator
        }, {
            translatorClass: DataViewHeadingTranslator
        }, {
            translatorClass: UIDCollectionTranslator
        }, {
            translatorClass: RemoteCollectionTranslator
        }, {
            translatorClass: OfflineCollectionTranslator
        }, {
            translatorClass: LinkCollectionTranslator
        }, {
            translatorClass: SizedComponentStyleTranslator
        }, {
            translatorClass: NamedComponentTranslator
        }, {
            translatorClass: ComponentTranslator
        }, {
            translatorClass: CollectionPickerStyleTranslator
        }, {
            translatorClass: TileViewStyleTranslator
        }, {
            translatorClass: DataViewHeadingStyleTranslator
        }, {
            translatorClass: SectionTranslator
        }, {
            translatorClass: SecondaryMenuSectionTranslator
        }, {
            translatorClass: SecondaryMenuTranslator
        }, {
            translatorClass: NavigationTranslator
        }, {
            translatorClass: TVNavigationTranslator
        }, {
            translatorClass: TabletNavigationTranslator
        }, {
            translatorClass: MobileNavigationTranslator
        }, {
            translatorClass: ListenerTranslator
        }, {
            translatorClass: AuthenticationTranslator
        }, {
            translatorClass: ComponentStyleTranslator
        }, {
            translatorClass: LabelStyleTranslator
        }, {
            translatorClass: ImageStyleTranslator
        }, {
            translatorClass: LabelContentTranslator
        }, {
            translatorClass: FieldContentTranslator
        }, {
            translatorClass: GalleryImageTranslator,
            config: {
                galleryService: this._galleryImageRepository
            }
        }, {
            translatorClass: UIConfigTranslator,
            config: {
                galleryService: this._galleryImageRepository,
                screenService: this._screenApplicationService
            }
        }, {
            translatorClass: TabletUiConfigTranslator,
            config: {
                galleryService: this._galleryImageRepository,
                screenService: this._screenApplicationService
            }
        }, {
            translatorClass: MobileUiConfigTranslator,
            config: {
                galleryService: this._galleryImageRepository,
                screenService: this._screenApplicationService
            }
        }, {
            translatorClass: TvUiConfigTranslator,
            config: {
                galleryService: this._galleryImageRepository,
                screenService: this._screenApplicationService
            }
        }, {
            translatorClass: ScreenTranslator
        }, {
            translatorClass: AdvanceScreenTranslator
        }, {
            translatorClass: SearchResultScreenTranslator
        }, {
            translatorClass: CarouselTranslator
        }, {
            translatorClass: SwimlaneTranslator
        }, {
            translatorClass: ListViewTranslator
        }, {
            translatorClass: GridViewTranslator
        }, {
            translatorClass: ImageTranslator
        }, {
            translatorClass: LabelTranslator
        }, {
            translatorClass: CollectionsViewTranslator
        }, {
            translatorClass: MobileScreenTranslator
        }, {
            translatorClass: TabletScreenTranslator
        }, {
            translatorClass: TvScreenTranslator
        }, {
            translatorClass: MobileSearchResultScreenTranslator
        }, {
            translatorClass: TabletSearchResultScreenTranslator
        }, {
            translatorClass: TVSearchResultScreenTranslator
        }, {
            translatorClass: TVHackNavigationTranslator
        }, {
            translatorClass: CarouselStyleTranslator
        }, {
            translatorClass: GridViewStyleTranslator
        }, {
            translatorClass: ComponentControlsTranslator
        }, {
            translatorClass: ExternalConfigTranslator
        }, {
            translatorClass: GemiusAnalyticsTranslator
        }, {
            translatorClass: MobileGemiusAnalyticsTranslator
        }, {
            translatorClass: TabletGemiusAnalyticsTranslator
        }, {
            translatorClass: TvGemiusAnalyticsTranslator
        }, {
            translatorClass: ExternalEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: BaseEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: MobileAppEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: TabletAppEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: TVAppEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: MobileBundleEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: TabletBundleEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: TVBundleEditionTranslator,
            config: {
                uiConfigRepository: this._uiConfigRepository
            }
        }, {
            translatorClass: MobileCustomCmsTranslator
        }, {
            translatorClass: MobileGenericCmsTranslator
        }, {
            translatorClass: MobileMpxCmsTranslator
        }, {
            translatorClass: MobileVideaCmsTranslator
        }, {
            translatorClass: MobileVimondCmsTranslator
        }, {
            translatorClass: TabletCustomCmsTranslator
        }, {
            translatorClass: TabletGenericCmsTranslator
        }, {
            translatorClass: TabletMpxCmsTranslator
        }, {
            translatorClass: TabletVideaCmsTranslator
        }, {
            translatorClass: TabletVimondCmsTranslator
        }, {
            translatorClass: TVCustomCmsTranslator
        }, {
            translatorClass: TVGenericCmsTranslator
        }, {
            translatorClass: TVMpxCmsTranslator
        }, {
            translatorClass: TVVideaCmsTranslator
        }, {
            translatorClass: TVVimondCmsTranslator
        }, {
            translatorClass: BaseFontTranslator
        }, {
            translatorClass: OpenSansTranslator
        }, {
            translatorClass: RobotoTranslator
        }, {
            translatorClass: SanFranciscoTranslator
        }, {
            translatorClass: CustomFontTranslator
        }, {
            translatorClass: StandardAspectRatioTranslator
        }, {
            translatorClass: CustomAspectRatioTranslator
        }, {
            translatorClass: ProgressBarTranslator
        }, {
            translatorClass: ProgressBarStyleTranslator
        }, {
            translatorClass: FavoritesTranslator
        }, {
            translatorClass: FavoritesLocalProviderTranslator
        }, {
            translatorClass: UserMpxProviderTranslator
        }, {
            translatorClass: UserVideaProviderTranslator
        }, {
            translatorClass: UserTranslator
        }, {
            translatorClass: ColorTranslator
        }, {
            translatorClass: OffsetColorTranslator
        }, {
            translatorClass: GradientColorTranslator
        }, {
            translatorClass: ComplexColorTranslator
        }, {
            translatorClass: SearchSettingsTranslator
        }, {
            translatorClass: FavoriteCollectionTranslator
        }, {
            translatorClass: WatchHistoryCollectionTranslator
        }, {
            translatorClass: WebViewTranslator
        }, {
            translatorClass: WebViewStyleTranslator
        }, {
            translatorClass: WebViewContentTranslator
        }, {
            translatorClass: WelcomeScreenTranslator
        }, {
            translatorClass: SystemEventTranslator
        }, {
            translatorClass: LayerTranslator
        }, {
            translatorClass: TileLayerTranslator
        }, {
            translatorClass: LayerControlsTranslator
        }, {
            translatorClass: AdvancedNavigationTranslator
        }, {
            translatorClass: TopMenuSectionTranslator
        }, {
            translatorClass: TopMenuTranslator
        }, {
            translatorClass: TopMenuStyleTranslator
        }, {
            translatorClass: TopMenuLayerTranslator
        }
        ];
    }
}