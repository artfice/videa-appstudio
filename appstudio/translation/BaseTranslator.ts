///<reference path="../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('underscore');

import {MetadataOwner} from '../dto/MetadataOwner';

import {IEntityTranslator} from '../interface/IEntityTranslator';
import {ITranslationService} from '../interface/ITranslationService';

export class BaseTranslator<T extends MetadataOwner, R extends MetadataOwner>
    implements IEntityTranslator<MetadataOwner, MetadataOwner>  {
    protected _translation;

    constructor(dto?: R) {
        this._translation = dto ? dto : {};
    }

    public translate(accountId: string, service: ITranslationService, source: T): Promise<R> {
        this._translation = _.omit(this._translation, _.isUndefined);
        this._translation = _.omit(this._translation, _.isNull);

        return new Promise<R>(
            (resolve, reject) => {
                resolve(this._translation);
            });
    }
}