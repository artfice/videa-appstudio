///<reference path="../../../../typings/index.d.ts"/>
import _ = require('lodash');
import Promise = require('bluebird');

import { ITranslationService } from '../../../interface/ITranslationService';
import { IEntityTranslator } from '../../../interface/IEntityTranslator';

import { ClientAppModelFactory } from '../../../factory/ClientAppModelFactory';

import AppstudioSearchSettingsSchema = require('../../../schema/appstudio/app/configuration/uiconfig/SearchSettings');
import { SearchSettings as AppstudioSearchSettingsDto } from '../../../dto/app/configuration/uiconfig/SearchSettings';
import SearchSettingsSchema = require('../../../schema/clientapp/app/uiconfig/Search');
import { SearchSettings as SearchSettingsDto } from '../../../dto/clientapp/app/uiconfig/SearchSettings';

export class SearchSettingsTranslator implements IEntityTranslator<AppstudioSearchSettingsDto, SearchSettingsDto> {
    public static SOURCE_TYPE = AppstudioSearchSettingsSchema.ID;

    public translate(accountId: string,
        translationService: ITranslationService,
        source: AppstudioSearchSettingsDto): Promise<SearchSettingsDto> {
        const isEnabled: boolean = _.get(source, 'enabled', false);
        const hasScreenId: boolean = !_.isEmpty(_.get(source, 'screenId', false));

        if (isEnabled && hasScreenId) {
            return Promise.resolve(this.buildSearchSettings(source));
        }
        return Promise.resolve(null);
    }

    private buildSearchSettings(source: AppstudioSearchSettingsDto): SearchSettingsDto {
        const searchSettings: SearchSettingsDto = <SearchSettingsDto>ClientAppModelFactory.create(
            SearchSettingsSchema.ID, {
                name: source.name,
                screenId: source.screenId
            });

        return searchSettings;
    }
}