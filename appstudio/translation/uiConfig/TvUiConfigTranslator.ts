import TvUiConfigSchema = require('../../schema/appstudio/app/tv/uiconfig/TVUIConfig');

import {UIConfigTranslator} from './UIConfigTranslator';

import {UIConfig as AppstudioUIConfigDto} from '../../dto/app/configuration/UIConfig';
import {UIConfig as ClientUIConfigDto} from '../../dto/clientapp/app/UIConfig';
import {ITranslationService} from '../../interface/ITranslationService';
import {Analytics} from '../../dto/app/configuration/uiconfig/Analytics';
import _ = require('lodash');

export class TvUiConfigTranslator extends UIConfigTranslator {

    public static SOURCE_TYPE = TvUiConfigSchema.ID;

    constructor(config) {
        super(config);
    }

	public translate(accountId: string, translationService: ITranslationService,
					 tvUiConfig: AppstudioUIConfigDto<Analytics>): Promise<ClientUIConfigDto<Analytics>> {
		return super.translate(accountId, translationService, 
		<AppstudioUIConfigDto<Analytics>>tvUiConfig).then(
			(uiConfig) => {
				if (uiConfig.navigation) {
					uiConfig.tvNavigation = _.clone(uiConfig.navigation);
					delete uiConfig.navigation;
				}
				return uiConfig;
			}
		);
	}
}