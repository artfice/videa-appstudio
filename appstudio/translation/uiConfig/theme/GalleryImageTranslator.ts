///<reference path="../../../../typings/index.d.ts"/>

import _ = require('underscore');
import Promise = require('bluebird');

import {ITranslationService} from '../../../interface/ITranslationService';
import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

import {GalleryImageRepository} from '../../../repository/GalleryImageRepository';

import {ItemNotFoundError} from 'videa-framework/VideaError';

import GalleryImageSchema = require('../../../schema/appstudio/common/GalleryImage');
import {GalleryImage as GalleryImageDto} from '../../../dto/common/GalleryImage';
import {Style} from '../../../dto/clientapp/Style';
import {Image as ClientImage} from '../../../dto/clientapp/component/field/Image';

export class GalleryImageTranslator implements IEntityTranslator<GalleryImageDto, ClientImage<Style>>  {

    static SOURCE_TYPE = GalleryImageSchema.ID;

    private _galleryRepository: GalleryImageRepository;

    constructor(config) {
        this._galleryRepository = config.galleryRepository;
    }

    public translate(accountId: string, translationService: ITranslationService,
        galleryImageDto: GalleryImageDto): Promise<ClientImage<Style>> {

        const galleryImageId = galleryImageDto && galleryImageDto.galleryImageId;
        const clientImage = <ClientImage<Style>>ClientAppModelFactory.create('Image');

        return new Promise<ClientImage<Style>>((resolve, reject) => {

            if (galleryImageId && galleryImageId.length > 0) {

                this._galleryRepository.getById(accountId, galleryImageId).then(
                    (imageDto) => {
                        if (!imageDto) {
                            return resolve(<any>clientImage);
                        }

                        _.extend(clientImage, {
                            value: imageDto.url
                        });

                        resolve(<any>clientImage);
                    }
                ).catch((err) => {
                    if (err instanceof ItemNotFoundError) {
                        return resolve(<any>clientImage);
                    }
                    reject(err);
                });
            } else {
                clientImage.value = null;
                resolve(<any>clientImage);
            }

        });
    }


}