import TabletUiConfigSchema = require('../../schema/appstudio/app/tablet/uiconfig/TabletUIConfig');

import {UIConfigTranslator} from './UIConfigTranslator';

export class TabletUiConfigTranslator extends UIConfigTranslator {

    public static SOURCE_TYPE = TabletUiConfigSchema.ID;

    constructor(config) {
        super(config);
    }
}