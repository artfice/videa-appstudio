import MobileUiConfigSchema = require('../../schema/appstudio/app/mobile/uiconfig/MobileUIConfig');
import {UIConfigTranslator} from './UIConfigTranslator';

export class MobileUiConfigTranslator extends UIConfigTranslator {

    public static SOURCE_TYPE = MobileUiConfigSchema.ID;

    constructor(config) {
        super(config);
    }

}