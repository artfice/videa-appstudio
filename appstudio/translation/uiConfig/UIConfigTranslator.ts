///<reference path="../../../typings/index.d.ts"/>

import _ = require('lodash');
import Promise = require('bluebird');

import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import {Image as ImageDto} from 'videa-framework/domain/image/Image';

import AppStudioUIConfigSchema = require('../../schema/appstudio/app/configuration/UIConfig');
import ContainerStyleSchema = require('../../schema/clientapp/app/uiconfig/screen/component/container/ContainerStyle');
import ContainerSchema = require('../../schema/clientapp/app/uiconfig/screen/component/container/Container');
import RuleSchema = require('../../schema/clientapp/style/Rule');
import TopNavSchema = require('../../schema/common/navigation/TopMenu');

import {Ads as Ads} from '../../dto/app/configuration/uiconfig/Ads';
import {UIConfig as AppstudioUIConfigDto} from '../../dto/app/configuration/UIConfig';
import {Theme as AppStudioTheme} from '../../dto/app/configuration/uiconfig/Theme';
import {Assets as AppstudioAsset} from '../../dto/app/configuration/uiconfig/theme/Assets';
import {Analytics as Analytics} from '../../dto/app/configuration/uiconfig/Analytics';

import {GalleryImage as GalleryImageReference} from '../../dto/common/GalleryImage';
import {ColorGalleryImage as GalleryColorImageReference} from '../../dto/common/ColorGalleryImage';
import {BaseMenu as BaseMenu} from '../../dto/common/navigation/BaseMenu';

import {Screen as ClientScreen} from '../../dto/clientapp/component/Screen';
import {Theme as ClientTheme} from '../../dto/clientapp/component/Theme';
import {Asset as ClientAsset} from '../../dto/clientapp/component/Asset';
import {UIConfig as ClientUIConfigDto} from '../../dto/clientapp/app/UIConfig';
import {Image as ClientImage} from '../../dto/clientapp/component/field/Image';
import {Style as Style} from '../../dto/clientapp/Style';
import {ColoredImage as ColoredImage} from '../../dto/clientapp/component/field/ColoredImage';
import {Screen as AppStudioScreen} from '../../dto/clientapp/component/Screen';
import {ContainerStyle as ContainerStyleDto} from '../../dto/clientapp/component/ContainerStyle';
import {VideaStyleSheet as VssDto} from '../../dto/clientapp/vss/VideaStyleSheet';
import {Rule as RuleDto} from '../../dto/clientapp/vss/Rule';
import {Colors as Colors} from '../../dto/app/configuration/uiconfig/theme/Colors';

import {ItemNotFoundError} from 'videa-framework/VideaError';
import {Logger} from 'videa-framework/Logger';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {TranslationUtil} from '../TranslationUtil';
import GoogleAdTypeSchema = require('../../schema/appstudio/app/configuration/uiconfig/ad/GoogleAdType');
import CustomAdTypeSchema = require('../../schema/appstudio/app/configuration/uiconfig/ad/CustomAdType');
import DrawerMenuSchema = require('../../schema/common/navigation/DrawerMenu');
import BottomMenuSchema = require('../../schema/common/navigation/BottomMenu');

export class UIConfigTranslator implements IEntityTranslator<AppstudioUIConfigDto<BaseMenu>, ClientUIConfigDto<Analytics>> {

    public static SOURCE_TYPE = AppStudioUIConfigSchema.ID;

    private _galleryService;
    private _screenService;

    private _vss: VssDto;

    protected _clientUIConfig: ClientUIConfigDto<Analytics>;

    constructor(config: any) {
        this._galleryService = config.galleryService;
        this._screenService = config.screenService;

        this._vss = <VssDto>ClientAppModelFactory.createVss();

        this._clientUIConfig = <ClientUIConfigDto<Analytics>>{
            screen: []
        };
    }

    public translate(accountId: string, service: ITranslationService,
        appstudioUIConfigDto: AppstudioUIConfigDto<BaseMenu>): Promise<ClientUIConfigDto<Analytics>> {
        const ads = _.get(appstudioUIConfigDto, 'ads', null);
        const uiSettings = _.get(appstudioUIConfigDto, 'uiSettings', null);
        const searchSettings = _.get(appstudioUIConfigDto, 'searchSettings', null);
        let defaultScreenId = _.get(appstudioUIConfigDto, 'uiSettings.screenId', null);
        const welcomeScreen = _.get(appstudioUIConfigDto, 'uiSettings.welcomeScreen', null);

        const analytics = _.get(appstudioUIConfigDto, 'analytics', []);
        const analyticTranslations = Promise.all(analytics.map((analytic) => {
            return service.translate(accountId, analytic);
        }).filter((analytic) => {
             return analytic != null;
        }));

        return new Promise<ClientUIConfigDto<Analytics>>((resolve, reject) => {
            const translationPromises:Promise<any>[] = [
                this._getThemeTranslation(accountId, appstudioUIConfigDto.theme),
                this._getScreenTranslations(accountId, service, appstudioUIConfigDto.id),
                analyticTranslations,
                service.translate(accountId, searchSettings),
                service.translate(accountId, welcomeScreen)
            ];
            let clientTheme = <ClientTheme>{};

            Promise.all(translationPromises).then((translations: any[]) => {
                const screens: ClientScreen[] = <ClientScreen[]>translations[1];
                let styledScreens = [];
                const translatedAnalytics = translations[2];
                clientTheme = <ClientTheme>translations[0];
                const translatedSearchSettings = translations[3];
                const translatedWelcomeScreen = translations[4];

                if (clientTheme && clientTheme.asset && clientTheme.asset.background) {
                    this._translateBackground(this._vss, clientTheme.asset.background);
                }

                if (screens && screens.length > 0) {
                    styledScreens = screens.map((screen) => {
                        return TranslationUtil.applyVss(screen, this._vss);
                    });

                    Array.prototype.push.apply(this._clientUIConfig.screen, styledScreens);

                    if (!defaultScreenId) {
                        defaultScreenId = screens[0].id;
                    }

                }

                _.extend(this._clientUIConfig, {
                    id: appstudioUIConfigDto.id,
                    _metadata: '',
                    theme: clientTheme,
                    videoAdsProvider: this._translateAds(ads),
                    analytics: translatedAnalytics,
                    search: translatedSearchSettings,
                    defaultScreenId: defaultScreenId,
                    welcomeScreen: translatedWelcomeScreen
                });

                return service.translate(accountId, appstudioUIConfigDto.navigation);
            }).then((clientNavigation) => {
                this._clientUIConfig.navigation = clientNavigation;
                //HACK FOR NAB REMOVE AFTER
                if (this._requireTopNavBar(clientNavigation)) {
                    this._addTopBarHack(this._clientUIConfig, clientTheme, appstudioUIConfigDto.theme);
                }
                resolve(<any>this._clientUIConfig);
            }).catch(reject);
        });
    }

    protected _requireTopNavBar (clientNavigation) : boolean {
        const menus = [BottomMenuSchema.ID, DrawerMenuSchema.ID];
        return clientNavigation  && (menus.indexOf(clientNavigation._metadata) > -1);
    }

    protected _addTopBarHack(clientUIConfig: ClientUIConfigDto<Analytics>,
        clientTheme: ClientTheme, appStudioTheme: AppStudioTheme) {
        const colors = appStudioTheme && appStudioTheme.colors;
        const accentColor = colors && colors.accent;
        const primarySpotColor = colors && colors.primarySpot;
        const secondarySpotColor = colors && colors.secondarySpot;
        const clientThemeAsset = clientTheme && clientTheme.asset;
        const logo = clientThemeAsset && clientThemeAsset.logo;
        const topBar = clientThemeAsset && clientThemeAsset.topBar;

        _.merge(clientUIConfig.navigation, {
            topBar: {
                menuIcon: {
                    value: 'http://digiflare.s3.amazonaws.com/videa/videa-new/images/btn_nav.png',
                    color: {
                        value: (accentColor && accentColor.value) || '#272A2C'
                    }
                },

                logo: {
                    url: logo && logo.value
                },
                backgroundColor: {
                    value: (primarySpotColor && primarySpotColor.value) || '#272A2C'
                }
            },

            menu: {
                backgroundColor: {
                    value: (secondarySpotColor && secondarySpotColor.value) || '#272A2C'
                },
                logo: {
                    url: topBar && topBar.value
                }
            }
        });
    }

    protected _getScreenTranslations(accountId: string, service: ITranslationService,
        uiConfigId: string): Promise<AppStudioScreen[]> {
        return this._screenService.getScreens(accountId, uiConfigId).then(
            (screens) => {
                return Promise.all(screens.map((screen) => {
                    return service.translate(accountId, screen);
                }));
            }
        );
    }

    protected _getThemeTranslation(accountId: string,
        appStudioTheme: AppStudioTheme): Promise<ClientTheme> {
        if (!appStudioTheme) {
            return Promise.resolve(null);
        }

        return new Promise<ClientTheme>((resolve, reject) => {
            this._getAssetTranslation(accountId, appStudioTheme.assets).then(
                (clientAsset) => {
                    resolve(<ClientTheme>{
                        asset: clientAsset,
                        colors: this._translateThemeColors(appStudioTheme.colors)
                    });
                }
            ).catch((err) => {
                Logger.error('Theme Translation Error: ', err);
                reject(err);
            });
        });
    }

    protected _translateBackground(vss: any, backgroundImage: ColoredImage): void {
        const backgroundColor = backgroundImage && backgroundImage.color &&
            backgroundImage.color.value;

        if (backgroundColor) {

            vss.rules.push(<RuleDto<ContainerStyleDto>>ClientAppModelFactory.create(RuleSchema.ID, {
                selector: ContainerSchema.ID,
                style: <ContainerStyleDto>ClientAppModelFactory.create(ContainerStyleSchema.ID, {
                    backgroundColor: backgroundImage.color
                })
            }));
        }
    }

    protected _getAssetTranslation(accountId: string, asset: AppstudioAsset): Promise<ClientAsset> {
        if (!asset) {
            return Promise.resolve(<ClientAsset>{
                logo: null,
                splashScreen: null,
                appIcon: null,
                background: null,
                navBackground: null,
                topBar: null
            });
        }
        return new Promise<ClientAsset>((resolve, reject) => {
            const galleryTranslationPromise = [
                this._getGalleryImageReferenceTranslation(accountId, asset.logo),
                this._getGalleryColorImageReferenceTranslation(accountId, asset.topBar)
            ];

            Promise.all(galleryTranslationPromise).then(
                (translation) => {
                    resolve(<ClientAsset>{
                        logo: translation[0],
                        splashScreen: null,
                        appIcon: null,
                        background: null,
                        navBackground: null,
                        topBar: translation[1]
                    });
                }).catch(reject);
        });
    }

    protected _getGalleryImageReferenceTranslation(accountId: string,
        galleryImageReference: GalleryImageReference): Promise<ClientImage<Style>> {
        const clientImage = <ClientImage<Style>>ClientAppModelFactory.create('Image');
        return new Promise<ClientImage<Style>>((resolve, reject) => {
            this._getGalleryImage(accountId, galleryImageReference && galleryImageReference.galleryImageId).then(
                (imageDto) => {
                    if (imageDto) {
                        _.extend(clientImage, {
                            value: imageDto.rawUrl
                        });
                    }
                    resolve(clientImage);
                }).catch(reject);

        });
    }

    protected _getGalleryImage(accountId: string, galleryImageId: string): Promise<ImageDto> {
        return new Promise<ImageDto>((resolve, reject) => {
            if (galleryImageId && galleryImageId.length > 0) {
                this._galleryService.getById(accountId, galleryImageId).then(resolve).catch(
                    (err) => {
                        if (err instanceof ItemNotFoundError) {
                            return resolve(null);
                        }
                        return reject(err);
                    });
            } else {
                resolve(null);
            }
        });
    }

    protected _getGalleryColorImageReferenceTranslation(accountId: string,
        galleryColorImageReference: GalleryColorImageReference):
        Promise<ColoredImage> {
        const coloredImage = <any>{
            _metadata: '',
            type: 'ColoredImage',
            color: {
                _metadata: 'schema.Color',
                value: ''
            }
        };
        const galleryImageId = galleryColorImageReference && galleryColorImageReference.galleryImageId;
        return new Promise<ColoredImage>((resolve, reject) => {
            coloredImage.color = galleryColorImageReference && galleryColorImageReference.color;
            this._getGalleryImage(accountId, galleryImageId).then(
                (imageDto: ImageDto) => {
                    if (imageDto) {
                        coloredImage.value = imageDto.rawUrl;
                    }
                    resolve(<any>coloredImage);
                }).catch(reject);
        });
    }

    protected _translateAds(ads: Ads): Ads {
        let newAds = null;
        const adEnabled = ads && ads.enabled;
        const adType = ads && ads.type;
        const adTypeMeta = ads && ads.type && ads.type._metadata;
        const adTagUrl = ads && ads.adTagUrl && ads.adTagUrl.length > 0;
        if (adEnabled && adTagUrl) {
            newAds = ads;
            switch (adTypeMeta) {
                case GoogleAdTypeSchema.ID:
                    newAds.type = 'Google IMA';
                    break;
                case CustomAdTypeSchema.ID:
                    newAds.type = adType.custom ? adType.custom : '';
                    break;
                default:
                    break;
            }
        }
        return newAds;
    }

    private _translateThemeColors(colors: Colors): Colors {
        if (!colors) {
            return <Colors> {
                primarySpot: null,
                secondarySpot: null,
                background: null,
                foreground: null,
                accent: null
            };
        }

        return <Colors> {
            primarySpot: colors.primarySpot,
            secondarySpot: colors.secondarySpot,
            background: null,
            foreground: null,
            accent: colors.accent
        };
    }    
}
