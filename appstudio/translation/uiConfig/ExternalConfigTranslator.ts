///<reference path="../../../typings/index.d.ts"/>

import Promise = require('bluebird');
import request = require('request');

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';

import ExternalConfigSchema = require('../../schema/appstudio/app/configuration/ExternalConfig');
import {ExternalConfig as ExternalConfigDto} from '../../dto/app/configuration/ExternalConfig';

import {Logger} from 'videa-framework/Logger';

import {JSONParseError,ExternalResourceError} from 'videa-framework/VideaError';

export class ExternalConfigTranslator implements IEntityTranslator<ExternalConfigDto, any>  {
    public static SOURCE_TYPE = ExternalConfigSchema.ID;

    constructor() {
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: ExternalConfigDto): Promise<any> {
        const url = source && source.url;

        return new Promise<any>((resolve, reject) => {
            if (!url) {
                return resolve({});
            }

            request(url, (error, response, body) => {
                if (!error && response.statusCode == 200) {
                    try {
                        return resolve(JSON.parse(body));
                    } catch (e) {
                        Logger.critical('External Config Translation Error: ', e);
                        return reject(new JSONParseError('ExternalConfig is not a valid JSON ' + url));
                    }
                } else {
                    Logger.critical('External Config Url Not Found Error', source);
                    return reject(new ExternalResourceError('ExternalConfig Not Found ' + url));
                }
            })
        });
    }
}