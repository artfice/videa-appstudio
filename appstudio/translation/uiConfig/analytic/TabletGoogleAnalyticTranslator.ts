import TabletGoogleAnalyticsSchema = require('../../../schema/appstudio/app/tablet/uiconfig/analytics/TabletGoogleAnalytics');
import {GoogleAnalyticTranslator} from './GoogleAnalyticTranslator';

export class TabletGoogleAnalyticTranslator extends GoogleAnalyticTranslator {
  public static SOURCE_TYPE = TabletGoogleAnalyticsSchema.ID;
}