import {CustomDataTranslator} from '../../CustomDataTranslator';
import CustomAnalyticsSchema = require('../../../schema/appstudio/app/configuration/uiconfig/analytics/custom/CustomAnalytics');

export class CustomAnalyticTranslator extends CustomDataTranslator {
    public static SOURCE_TYPE = CustomAnalyticsSchema.ID;
}