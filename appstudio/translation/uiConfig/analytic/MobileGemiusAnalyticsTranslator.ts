import MobileGemiusAnalyticsSchema = require('../../../schema/appstudio/app/mobile/uiconfig/analytics/MobileGemiusAnalytics');
import {GemiusAnalyticsTranslator} from './GemiusAnalyticsTranslator';

export class MobileGemiusAnalyticsTranslator extends GemiusAnalyticsTranslator {
  public static SOURCE_TYPE = MobileGemiusAnalyticsSchema.ID;
}