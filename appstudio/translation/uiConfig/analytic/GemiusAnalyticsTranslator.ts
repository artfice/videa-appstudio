///<reference path="../../../../typings/index.d.ts"/>
import Promise = require('bluebird');

import {ITranslationService} from '../../../interface/ITranslationService';
import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';
import {GemiusAnalytics as GemiusAnalyticsDto} from '../../../dto/app/configuration/uiconfig/analytics/gemius/GemiusAnalytics';
import GemiusAnalyticschema = require('../../../schema/appstudio/app/configuration/uiconfig/analytics/gemius/GemiusAnalytics');

export class GemiusAnalyticsTranslator implements IEntityTranslator<GemiusAnalyticsDto, GemiusAnalyticsDto>  {
    public static SOURCE_TYPE = GemiusAnalyticschema.ID;

    public translate(accountId: string, translationService: ITranslationService, rawAnalytic: GemiusAnalyticsDto): Promise<GemiusAnalyticsDto> {
        const analytic = rawAnalytic || <GemiusAnalyticsDto>{};
        let host = analytic.host;
        let name = analytic.name;
        let iOS = analytic.iOS;
        let window8 = analytic.window8;
        let windowsPhone = analytic.windowsPhone;
        let android = analytic.android;
        let testing = analytic.testing;
        let screens = analytic.screens ? analytic.screens : [];
        let events = analytic.events ? analytic.events : [];
        let translation = <GemiusAnalyticsDto>ClientAppModelFactory.create('GemiusAnalytics', {
            host: host,
            name: name,
            screens: screens,
            events: events,
            accountID: {
                iOS: iOS,
                window8: window8,
                windowsPhone: windowsPhone,
                android: android,
                testing: testing
            }
        });

        return Promise.resolve(translation);
    }
}