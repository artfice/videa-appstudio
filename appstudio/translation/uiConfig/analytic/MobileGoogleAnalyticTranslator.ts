import MobileGoogleAnalyticsSchema = require('../../../schema/appstudio/app/mobile/uiconfig/analytics/MobileGoogleAnalytics');
import {GoogleAnalyticTranslator} from './GoogleAnalyticTranslator';

export class MobileGoogleAnalyticTranslator extends GoogleAnalyticTranslator {
  public static SOURCE_TYPE = MobileGoogleAnalyticsSchema.ID;
}