import TvGemiusAnalyticsSchema = require('../../../schema/appstudio/app/tv/uiconfig/analytics/TvGemiusAnalytics');
import {GemiusAnalyticsTranslator} from './GemiusAnalyticsTranslator';

export class TvGemiusAnalyticsTranslator extends GemiusAnalyticsTranslator {
  public static SOURCE_TYPE = TvGemiusAnalyticsSchema.ID;
}