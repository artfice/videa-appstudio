import TvGoogleAnalyticsSchema = require('../../../schema/appstudio/app/tv/uiconfig/analytics/TvGoogleAnalytics');
import {GoogleAnalyticTranslator} from './GoogleAnalyticTranslator';

export class TvGoogleAnalyticTranslator extends GoogleAnalyticTranslator {
  public static SOURCE_TYPE = TvGoogleAnalyticsSchema.ID;
}