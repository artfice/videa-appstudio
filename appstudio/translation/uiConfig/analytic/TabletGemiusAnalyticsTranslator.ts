import TabletGemiusAnalyticsSchema = require('../../../schema/appstudio/app/tablet/uiconfig/analytics/TabletGemiusAnalytics');
import {GemiusAnalyticsTranslator} from './GemiusAnalyticsTranslator';

export class TabletGemiusAnalyticsTranslator extends GemiusAnalyticsTranslator {
  public static SOURCE_TYPE = TabletGemiusAnalyticsSchema.ID;
}