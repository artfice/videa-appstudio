///<reference path="../../../../typings/index.d.ts"/>
import Promise = require('bluebird');
import _ = require('underscore');

import {ITranslationService} from '../../../interface/ITranslationService';
import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';
import {GoogleAnalytics as GoogleAnalyticsDto} from '../../../dto/app/configuration/uiconfig/analytics/google/GoogleAnalytics';
import GoogleAnalyticSchema = require('../../../schema/appstudio/app/configuration/uiconfig/analytics/google/GoogleAnalytics');

export class GoogleAnalyticTranslator implements IEntityTranslator<GoogleAnalyticsDto, GoogleAnalyticsDto>  {
    public static SOURCE_TYPE = GoogleAnalyticSchema.ID;

    public translate(accountId: string, translationService: ITranslationService, analytic: GoogleAnalyticsDto): Promise<GoogleAnalyticsDto> {
        let translation = <GoogleAnalyticsDto>ClientAppModelFactory.create(analytic._metadata, analytic);
        let events = analytic.events && analytic.events.length > 0 ? analytic.events : [];
        let systemEvents = analytic.systemEvents;

        translation.events = events.map((event: any) => {
            let custom = (event && event.custom &&
                event.custom.content.length > 0) ?
                event.custom.content :
                '[]';

            try {
                custom = JSON.parse(custom);
                if (Object.keys(custom).length < 1) {
                    custom = undefined;
                }
            } catch (e) {
                custom = undefined;
            }

            event.custom = custom;
            event.id = _.clone(event.videaEventTag);
            delete event.videaEventTag;
            return event;
        });
        return translationService.translate(accountId, systemEvents).then(
            (translatedEvent) => {
                translation.systemEvents = translatedEvent;
                return translation;
            });
    }
}