import {IEntityTranslator} from '../../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../../interface/ITranslationService';

import {ClientAppModelFactory} from '../../../../factory/ClientAppModelFactory';
import {SystemEvents as SystemEventsDto} from '../../../../dto/app/configuration/uiconfig/analytics/google/SystemEvents';
import {Event as EventDto} from '../../../../dto/app/configuration/uiconfig/analytics/google/systemEvent/Event';
import SystemEventSchema = require('../../../../schema/appstudio/app/configuration/uiconfig/analytics/google/SystemEvents');
import Promise = require('bluebird');
import _ = require('lodash');
import uuid = require('node-uuid');

export class SystemEventTranslator implements IEntityTranslator<SystemEventsDto, SystemEventsDto>  {
    public static SOURCE_TYPE = SystemEventSchema.ID;

    public translate(accountId: string, translationService: ITranslationService, rawSystemEvent: SystemEventsDto): Promise<SystemEventsDto> {
        const analytic = rawSystemEvent;
        let translation = <SystemEventsDto>{};
        const signInSuccess = analytic && analytic.signInSuccess && analytic.signInSuccess.enabled ? analytic.signInSuccess : undefined;
        const signInFailure = analytic && analytic.signInFailure && analytic.signInFailure.enabled ? analytic.signInFailure : undefined;
        const search = analytic && analytic.search && analytic.search.enabled ? analytic.search : undefined;
        const chromecastConnect = analytic && analytic.chromecastConnect && analytic.chromecastConnect.enabled ? analytic.chromecastConnect : undefined;
        const chromecastDisconnect = analytic && analytic.chromecastDisconnect && analytic.chromecastDisconnect.enabled ? analytic.chromecastDisconnect : undefined;
        let videoPlayback = analytic && analytic.videoPlayback.length > 0 ? analytic.videoPlayback : [];

        if (signInSuccess) {
            signInSuccess.id = uuid.v1();
            delete signInSuccess.enabled;
            signInSuccess.custom = signInSuccess.custom.map((custom) => {
                if (custom.content.length > 0) {
                    try {
                        return JSON.parse(custom.content);
                    } catch (e) {
                        return undefined;
                    }
                }
                return custom;
            }).filter((custom) => {
                return custom != undefined;
            });
        }
        if (signInFailure) {
            signInFailure.id = uuid.v1();
            delete signInFailure.enabled;
            signInFailure.custom = signInFailure.custom.map((custom) => {
                if (custom.content.length > 0) {
                    try {
                        return JSON.parse(custom.content);
                    } catch (e) {
                        return undefined;
                    }
                }
                return custom;
            }).filter((custom) => {
                return custom != undefined;
            });
        }

        if (search) {
            search.id = uuid.v1();
            delete search.enabled;
            search.custom = search.custom.map((custom) => {
                if (custom.content.length > 0) {
                    try {
                        return JSON.parse(custom.content);
                    } catch (e) {
                        return undefined;
                    }
                }
                return custom;
            }).filter((custom) => {
                return custom != undefined;
            });
        }

        if (chromecastConnect) {
            chromecastConnect.id = uuid.v1();
            delete chromecastConnect.enabled;
            chromecastConnect.custom = chromecastConnect.custom.map((custom) => {
                if (custom.content.length > 0) {
                    try {
                        return JSON.parse(custom.content);
                    } catch (e) {
                        return undefined;
                    }
                }
                return custom;
            }).filter((custom) => {
                return custom != undefined;
            });
        }

        if (chromecastDisconnect) {
            chromecastDisconnect.id = uuid.v1();
            delete chromecastDisconnect.enabled;
            chromecastDisconnect.custom = chromecastDisconnect.custom.map((custom) => {
                if (custom.content.length > 0) {
                    try {
                        return JSON.parse(custom.content);
                    } catch (e) {
                        return undefined;
                    }
                }
                return custom;
            }).filter((custom) => {
                return custom != undefined;
            });
        }

        if (videoPlayback.length > 0) {
            videoPlayback = videoPlayback.map((videoPlaybackEvent) => {
                if (videoPlaybackEvent && videoPlaybackEvent.event) {
                    let event = <EventDto>ClientAppModelFactory.create(videoPlaybackEvent.event._metadata, videoPlaybackEvent.event);
                    event.custom = event.custom.map((custom) => {
                        if (custom.content.length > 0) {
                            try {
                                return JSON.parse(custom.content);
                            } catch (e) {
                                return undefined;
                            }
                        }
                        return custom;
                    }).filter((custom) => {
                        return custom != undefined;
                    });
                    return event;
                }
                return undefined;
            });
        }

        _.extend(translation, {
            signInSuccess: signInSuccess,
            signInFailure: signInFailure,
            search: search,
            chromecastConnect: chromecastConnect,
            chromecastDisconnect: chromecastDisconnect,
            videoPlayback: videoPlayback
        });

        return Promise.resolve(translation);
    }
}