import RobotoSchema = require('../../schema/common/font/Roboto');

import {BaseFontTranslator} from './BaseFontTranslator';

export class RobotoTranslator extends BaseFontTranslator  {

	static SOURCE_TYPE = RobotoSchema.ID;
	protected _fontFamily = 'Roboto-';	
}