import OpenSansSchema = require('../../schema/common/font/OpenSans');

import {BaseFontTranslator} from './BaseFontTranslator';

export class OpenSansTranslator extends BaseFontTranslator  {

	static SOURCE_TYPE = OpenSansSchema.ID;
	protected _fontFamily = 'OpenSans-';	
}