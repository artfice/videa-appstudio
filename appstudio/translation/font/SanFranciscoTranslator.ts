import SanFranciscoSchema = require('../../schema/common/font/SanFrancisco');

import {BaseFontTranslator} from './BaseFontTranslator';

export class SanFranciscoTranslator extends BaseFontTranslator  {

	static SOURCE_TYPE = SanFranciscoSchema.ID;
	protected _fontFamily = '.SFUIText-';	
}