import { IEntityTranslator } from '../../interface/IEntityTranslator';
import { ITranslationService } from '../../interface/ITranslationService';

import BaseFontSchema = require('../../schema/common/font/BaseFont');
import AlignmentSchema = require('../../schema/common/Alignment');

import { BaseFont as BaseFontDto } from '../../dto/common/BaseFont';
import { Font as ClientFont } from '../../dto/common/Font';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import * as Promise from 'bluebird';
import * as _ from 'lodash';

export class BaseFontTranslator implements IEntityTranslator<BaseFontDto, ClientFont> {

    public static SOURCE_TYPE = BaseFontSchema.ID;

    protected _fontFamily = 'BaseFont-';

    public translate(accountId: string, translationService: ITranslationService,
                     source: BaseFontDto): Promise<ClientFont> {
        const weight = source.weight ? source.weight : 'Regular';
        const size = source && source.size;
        const color = source && source.color;

        let translation = <any>ClientAppModelFactory.create(source._metadata);

        _.extend(translation, {
            size: size || 12,
            color: color,
            alignment: this._getAlignment(source)
        });

        translation.family = this._fontFamily + weight;

        return Promise.resolve(translation);
    }

    private _getAlignment(source: BaseFontDto): string {
        return <string> _.get(source, 'alignment.value', AlignmentSchema.getDefaultValue());
    }
}