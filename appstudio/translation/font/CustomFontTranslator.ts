import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import CustomFontSchema = require('../../schema/common/font/CustomFont');

import {BaseFont as BaseFontDto} from '../../dto/common/BaseFont';

import Promise = require('bluebird');

export class CustomFontTranslator implements IEntityTranslator<BaseFontDto, BaseFontDto>  {

	static SOURCE_TYPE = CustomFontSchema.ID;

	public translate(accountId: string, translationService: ITranslationService,
		source: BaseFontDto): Promise<BaseFontDto> {
			source.type = 'Font';
			return Promise.resolve(source);
	}
}