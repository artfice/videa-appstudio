import {ComponentControlsTranslator} from './ComponentControlsTranslator';

import LayerControlsSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/LayerControls');

import {LayerControls as LayerControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/LayerControls';

import Promise = require('bluebird');
import _ = require('lodash');

import {ITranslationService} from '../../interface/ITranslationService';

export class LayerControlsTranslator extends ComponentControlsTranslator {

    public static SOURCE_TYPE = LayerControlsSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService,
        source: LayerControlsDto): Promise<LayerControlsDto> {

        let translation = this._translation;
        const layout = _.get(source, 'layout', undefined);

        translation.layout = layout;

        return super.translate(accountId, service, source);
    }
}
