import TileLayerSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/TileLayer');
import {LayerTranslator} from './LayerTranslator';

export class TileLayerTranslator extends LayerTranslator {
    public static SOURCE_TYPE = TileLayerSchema.ID;
}