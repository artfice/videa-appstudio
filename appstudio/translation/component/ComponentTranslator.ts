///<reference path="../../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('underscore');

import ComponentSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/Component');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import {Component as AppStudioComponentDto} from '../../dto/app/configuration/uiconfig/screen/component/Component';
import {ComponentStyle as AppStudioComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';
import {ComponentControls as AppstudioComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../dto/common/listener/action/BaseAction';

import {Component as ClientComponentDto} from '../../dto/clientapp/Component';
import {Style as ClientStyleDto} from '../../dto/clientapp/Style';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

export class ComponentTranslator<R extends AppStudioComponentDto<AppStudioComponentStyleDto>,
    T extends ClientComponentDto<ClientStyleDto>> extends
    BaseTranslator<AppStudioComponentDto<AppStudioComponentStyleDto>, ClientComponentDto<ClientStyleDto>>  {

    public static SOURCE_TYPE = ComponentSchema.ID;

    protected _style: ClientStyleDto;

    constructor() {
        super();
        this._style = <ClientStyleDto>ClientAppModelFactory.create('Style');
    }

    public translate(accountId: string, service: ITranslationService, source: R): Promise<T> {
        const translation = this._translation;
        const style = source && source.style;
        const controls = source && source.controls;

        _.extend(translation, {
            id: source && source.id
        });

        return new Promise<T>((resolve, reject) => {
            service.translate(accountId, style).then(
                (translatedStyle: ClientStyleDto) => {
                    translatedStyle = _.omit(translatedStyle, _.isUndefined);
                    translatedStyle = _.omit(translatedStyle, _.isNull);
                    _.extend(this._style, translatedStyle);
                    _.extend(translation, {
                        style: this._style
                    });
                    return service.translate(accountId, controls);
                }).then(
                (translatedControls: AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>) => {
                    translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                    return super.translate(accountId, service, source);
                }).then(resolve).catch(reject);
        });
    }
}