import TopMenuLayerSchema = require('../../schema/appstudio/app/configuration/uiconfig/navigation/content/TopMenuLayer');
import {LayerTranslator} from './LayerTranslator';

export class TopMenuLayerTranslator extends LayerTranslator {
    public static SOURCE_TYPE = TopMenuLayerSchema.ID;

    constructor() {
        super();
    }
}