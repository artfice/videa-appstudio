///<reference path="../../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('lodash');

import {ComponentStyleTranslator} from './ComponentStyleTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import ColoredComponentStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/style/ColoredComponentStyle');

import {ColoredComponentStyle as AppstudioColoredComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/ColoredComponentStyle';
import {Style as ClientBaseStyleDto} from '../../dto/clientapp/Style';

export class ColoredComponentStyleTranslator<T extends AppstudioColoredComponentStyleDto, R extends ClientBaseStyleDto> extends
            ComponentStyleTranslator<AppstudioColoredComponentStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = ColoredComponentStyleSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioColoredComponentStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            const backgroundColor = source && source.backgroundColor;

            return service.translate(accountId, backgroundColor)
                .then(function(translatedBackgroundColor) {
                    _.extend(translation, {
                        backgroundColor: translatedBackgroundColor
                    });

                    return translation;
                }).then(() => {
                    return super.translate(accountId, service, source);
                }).then(resolve).catch(reject);
        });
    }
}