import LayerSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/Layer');
import {ITranslationService} from '../../interface/ITranslationService';
import {ComponentTranslator} from './ComponentTranslator';

import {Layer as AppstudioLayerDto} from '../../dto/app/configuration/uiconfig/screen/component/Layer';
import {ComponentStyle as AppStudioComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';
import {Component as ClientComponentDto} from '../../dto/clientapp/Component';
import {Style as ClientStyleDto} from '../../dto/clientapp/Style';

import {Layer} from '../../dto/clientapp/component/Layer';
import _ = require('lodash');
import Promise = require('bluebird');

export class LayerTranslator extends ComponentTranslator<AppstudioLayerDto<AppStudioComponentStyleDto>,
    ClientComponentDto<ClientStyleDto>> {
    public static SOURCE_TYPE = LayerSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService,
        source: AppstudioLayerDto<AppStudioComponentStyleDto>):
        Promise<ClientComponentDto<ClientStyleDto>> {
        const subComponents = _.get(source, 'subComponent', []);
        const numSubComponents = subComponents.length;
        const style = _.get(source, 'style');
        const control = _.get(source, 'controls');

        let layer = new Layer(numSubComponents);

        const promiseList = subComponents.map((component) => {
            return service.translate(accountId, component);
        });

        return Promise.all(promiseList).then((componentList) => {
            layer.setItems(componentList);
            return service.translate(accountId, control);
        }).then((translatedControls) => {
            const layout = _.get(translatedControls, 'layout', undefined);
            layer.setLayout(layout);
            this._translation = layer.getContainer(); 
            return service.translate(accountId, style);  
        }).then((translatedStyle) => {
            _.extend(this._translation, {
                width: translatedStyle && translatedStyle.width,
                height: translatedStyle && translatedStyle.height
            });
                
            source.style = _.omit(style, ['width', 'height']);
            return super.translate(accountId, service, source);
        });
    }

}