import Promise = require('bluebird');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import {TranslationUtil} from '../TranslationUtil';

import ComponentStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/style/ComponentStyle');

import {ComponentStyle as AppstudioComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';
import {Style as ClientComponentStyleDto} from '../../dto/clientapp/Style';

export class ComponentStyleTranslator<T extends AppstudioComponentStyleDto,
    R extends ClientComponentStyleDto> extends BaseTranslator<AppstudioComponentStyleDto,
    ClientComponentStyleDto>  {

    public static SOURCE_TYPE = ComponentStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService,
        source: AppstudioComponentStyleDto): Promise<ClientComponentStyleDto> {

        const padding = source && source.padding;
        const margin = source && source.margin;
        const border = source && source.border;

        this._translation = TranslationUtil.applyIf(this._translation, {
            padding: padding ? [padding.top, padding.right, padding.bottom, padding.left].join(' ') : undefined,
            margin: margin? [margin.top, margin.right, margin.bottom, margin.left].join(' ') : undefined,
            border: border || null
        });

        return super.translate(accountId, service, source);
    }
}