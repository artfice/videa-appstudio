import Promise = require('bluebird');

import NamedComponentSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/NamedComponent');
import {ITranslationService} from '../../interface/ITranslationService';
import {ComponentTranslator} from './ComponentTranslator';

import {NamedComponent as AppstudioNamedComponentDto} from '../../dto/app/configuration/uiconfig/screen/component/NamedComponent';
import {ComponentStyle as AppStudioComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';

import {NamedComponent as ClientNamedComponentDto} from '../../dto/clientapp/NamedComponent';

export class NamedComponentTranslator<T extends AppstudioNamedComponentDto<AppStudioComponentStyleDto>> extends ComponentTranslator<AppstudioNamedComponentDto<AppStudioComponentStyleDto>, ClientNamedComponentDto> {
    public static SOURCE_TYPE = NamedComponentSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: T): Promise<ClientNamedComponentDto> {
        const name = source && source.name;

        this._translation.name = name || '';
        return super.translate(accountId, service, source);
    }

}