///<reference path="../../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('underscore');

import ComponentControlsSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/ComponentControls');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../dto/common/listener/action/BaseAction';

export class ComponentControlsTranslator extends BaseTranslator<ComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>, ComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>>  {

    public static SOURCE_TYPE = ComponentControlsSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService,
        source: ComponentControlsDto<AppstudioBaseEventDto,
            AppstudioBaseActionDto>): Promise<ComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>> {

        const translation = this._translation;
        const visible = source && source.visible && source.visible.length > 0 ? source.visible : 'true';
        let listeners = source && source.listeners ? source.listeners : [];

        listeners = _.map(listeners, function (listener) {
            return service.translate(accountId, listener);
        });

        return new Promise<ComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>>((resolve, reject) => {
            Promise.all(listeners).then((translatedListeners) => {
                translation.visible = visible;
                translation.listeners = translatedListeners;
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}