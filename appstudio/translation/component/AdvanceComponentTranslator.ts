import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';

import AdvanceComponentSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/AdvanceComponent');

import {AdvanceComponent as AdvanceComponentDto} from '../../dto/app/configuration/uiconfig/screen/component/AdvanceComponent';
import {Screen as ClientScreenDto} from '../../dto/clientapp/component/Screen';
import * as Digi from 'videa-framework/Digi';

export class AdvanceComponentTranslator implements IEntityTranslator<AdvanceComponentDto,
ClientScreenDto>  {

    public static SOURCE_TYPE = AdvanceComponentSchema.ID;

    constructor() {

    }

    public translate(accountId: string, translationService: ITranslationService,
        advanceComponent: AdvanceComponentDto): Promise<ClientScreenDto> {
        return new Promise<ClientScreenDto>((resolve, reject) => {
            const content = advanceComponent && advanceComponent.customData &&
            advanceComponent.customData.content;

            if (content) {
                try {
                    let contentObj = JSON.parse(content);
                    contentObj = Digi.Obj.deepOmit(contentObj, '_metadata');
                    contentObj._metadata = AdvanceComponentSchema.ID;
                    resolve(contentObj);
                } catch (e) {
                    reject(new Error('invalid json ' + JSON.stringify(e, null, 4)));
                }
            } else {
                reject(new Error('invalid advance component ' +
                JSON.stringify(advanceComponent, null, 4)));
            }

        });
    }
}