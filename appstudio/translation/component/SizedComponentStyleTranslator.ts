import Promise = require('bluebird');

import { ComponentStyleTranslator } from './ComponentStyleTranslator';
import { ITranslationService } from '../../interface/ITranslationService';

import SizedComponentStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/style/SizedComponentStyle');
import { SizedComponentStyle as AppstudioSizedComponentStyleDto } from '../../dto/app/configuration/uiconfig/screen/component/style/SizedComponentStyle';
import { Style as ClientBaseStyleDto } from '../../dto/clientapp/Style';

import WrapContent = require('../../schema/common/WrapContent');
import MatchParent = require('../../schema/common/MatchParent');
import CustomHeight = require('../../schema/common/CustomHeight');
import CustomWidth = require('../../schema/common/CustomWidth');

export class SizedComponentStyleTranslator<T extends AppstudioSizedComponentStyleDto, R extends ClientBaseStyleDto> extends ComponentStyleTranslator<AppstudioSizedComponentStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = SizedComponentStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioSizedComponentStyleDto): Promise<ClientBaseStyleDto> {
        const width = source && source.width;
        const height = source && source.height;

        if (width && width.value) {
            switch (width.value._metadata) {
                case WrapContent.ID:
                    this._translation.width = 'wrapContent';
                    break;
                case MatchParent.ID:
                    this._translation.width = 'matchParent';
                    break;
                case CustomWidth.ID:
                    this._translation.width = width.value.value;
                    break;

                default:
                    break;
            }
        }

        if (height && height.value) {
            switch (height.value._metadata) {
                case WrapContent.ID:
                    this._translation.height = 'wrapContent';
                    break;
                case MatchParent.ID:
                    this._translation.height = 'matchParent';
                    break;
                case CustomHeight.ID:
                    this._translation.height = height.value.value;
                    break;
                default:
                    break;
            }
        }
        
        return super.translate(accountId, service, source);
    }
}