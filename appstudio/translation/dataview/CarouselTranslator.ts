import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';

import CarouselSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/Carousel');

import {Carousel as AppstudioCarouselDto} from '../../dto/app/configuration/uiconfig/screen/dataview/carousel/Carousel';
import {Carousel as ClientCarouselDto} from '../../dto/clientapp/component/dataview/carousel/Carousel';

import {Component as ClientComponentDto} from '../../dto/clientapp/Component';
import {Container as ClientContainerDto} from '../../dto/clientapp/component/Container';
import {Style as ClientStyleDto} from '../../dto/clientapp/Style';
import {Layout as ClientLayoutDto} from '../../dto/clientapp/component/layout/Layout';
import {Collection as ClientCollectionDto} from '../../dto/clientapp/component/collection/Collection';
import {TileStyle as ClientTileStyleDto} from '../../dto/clientapp/component/dataview/TileStyle';
import {DataView as ClientDataViewDto} from '../../dto/clientapp/component/dataview/DataView';
import _ = require('lodash');
import {ComponentTranslator} from '../component/ComponentTranslator';

import {DataView} from '../../dto/clientapp/component/DataView';

export class CarouselTranslator extends ComponentTranslator<AppstudioCarouselDto,
    ClientCarouselDto<ClientLayoutDto,
    ClientComponentDto<ClientStyleDto>,
    ClientContainerDto,
    ClientCollectionDto>>{

    static SOURCE_TYPE = CarouselSchema.ID;

    protected _tileContainer: ClientContainerDto;
    protected _dataview: DataView<ClientDataViewDto<ClientLayoutDto, ClientComponentDto<ClientStyleDto>, ClientContainerDto, ClientCollectionDto>>;

    constructor() {
        super();

        this._dataview = new DataView({
            type: 'Carousel'
        });
        this._translation = this._dataview.getContainer();
    }

    public translate(
        accountId: string,
        service: ITranslationService,
        source: AppstudioCarouselDto): Promise<ClientCarouselDto<ClientLayoutDto,
        ClientComponentDto<ClientTileStyleDto>,
        ClientContainerDto,
        ClientCollectionDto>> {
        let tile = source && source.tile;
        let content = source && source.content && source.content.collection;
        let controls = source && source.controls;
        let translation = this._translation;
        let dataview = this._dataview;
        let style = source && source.style;
        let result = new Promise<ClientCarouselDto<ClientLayoutDto,
            ClientComponentDto<ClientTileStyleDto>,
            ClientContainerDto,
            ClientCollectionDto>>((resolve, reject) => {
                service.translate(accountId, tile).then((translatedTile) => {
                    dataview.setItemRenderer(translatedTile);
                    return service.translate(accountId, content);
                }).then((translatedContent) => {
                    dataview.setCollection(translatedContent);
                    return service.translate(accountId, controls);
                }).then((translatedControls) => {
                    translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                    translation.listeners = translatedControls && translatedControls.listeners ? translatedControls.listeners : [];
                    return service.translate(accountId, style);
                }).then((translatedStyle) => {
                    dataview.setStyle(translatedStyle);
                    translation = dataview.getContainer();
                    source.style =_.omit(style, ['width', 'height']);
                    return super.translate(accountId, service, source);
                }).then(resolve).catch(reject);
            });

        return result;
    }

}