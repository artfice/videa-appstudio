///<reference path="../../../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('underscore');

import CollectionPickerContentSchema =
require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/collectionview/picker/PickerContent');

import {BaseTranslator} from '../../BaseTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {PickerRenderer as ClientPickerRendererDto} from '../../../dto/clientapp/component/dataview/collectionview/PickerRenderer';
import {PickerContent as AppStudioCollectionPickerContentDto} from '../../../dto/app/configuration/uiconfig/screen/collectionview/picker/PickerContent';
import {BaseCollection as BaseCollectionDto} from '../../../dto/common/collection/BaseCollection';


import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

export class CollectionPickerContentTranslator extends
BaseTranslator<AppStudioCollectionPickerContentDto<BaseCollectionDto>, ClientPickerRendererDto>  {

    public static SOURCE_TYPE = CollectionPickerContentSchema.ID;

    constructor() {
        super();

        this._translation = <ClientPickerRendererDto>ClientAppModelFactory.create('PickerRenderer', {
            width: 'wrapContent',
            height: '50',
            style: ClientAppModelFactory.create('ContainerStyle')
        });
    }

    public translate(accountId: string, service: ITranslationService,
        source: AppStudioCollectionPickerContentDto<BaseCollectionDto>): Promise<ClientPickerRendererDto> {
        const translation = this._translation;
        const collection = source && source.collection;
        const collectionType = source && source.type;
        const displayName = source && source.displayName;

        return service.translate(accountId, collection).then(
            (translatedCollection) => {
                translation.collection = translatedCollection;
                _.extend(translation, {
                    type: collectionType,
                    displayName: displayName
                });
                return super.translate(accountId, service, source);
            });

    }
}