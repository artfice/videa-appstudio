///<reference path="../../../../typings/index.d.ts"/>

import _ = require('underscore');
import Promise = require('bluebird');

import {SizedComponentStyleTranslator} from '../../component/SizedComponentStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import CollectionPickerStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/collectionview/picker/PickerStyle');
import {PickerStyle as AppstudioCollectionPickerStyleDto} from '../../../dto/app/configuration/uiconfig/screen/collectionview/picker/PickerStyle';

import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

export class CollectionPickerStyleTranslator extends SizedComponentStyleTranslator<AppstudioCollectionPickerStyleDto, ClientBaseStyleDto>  {
    public static SOURCE_TYPE = CollectionPickerStyleSchema.ID;

    constructor() {
        super();
        this._translation = ClientAppModelFactory.create('Style');
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioCollectionPickerStyleDto): Promise<ClientBaseStyleDto> {

        const font = source && source.font;
		const backgroundColor = source && source.backgroundColor;
        const padding = source && source.padding;
        const margin = source && source.margin;

        let translation = this._translation;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            return service.translate(accountId, backgroundColor)
                .then(function(translatedBackgroundColor) {
                    _.extend(translation, {
                        backgroundColor: translatedBackgroundColor,
                        padding: padding ? [padding.top, padding.right, padding.bottom, padding.left].join(' ') : '0 0 0 0',
                        margin: margin? [margin.top, margin.right, margin.bottom, margin.left].join(' ') : '0 0 0 0'
                    });

                    return translation;
                }).then(() => {
                    if (!font) {
                        translation.font = null;
                        return super.translate(accountId, service, source);
                    } else {
                        return service.translate(accountId, font).then((translatedFont) => {
                            translation.font = translatedFont;
                            return super.translate(accountId, service, source);
                        }).catch((err) => {
                            return err;
                        });
                    }
                }).then(resolve).catch(reject);
        });
	}
}