///<reference path="../../../../typings/index.d.ts"/>

import * as _ from 'lodash';
import * as Promise from 'bluebird';

import { SizedComponentStyleTranslator } from '../../component/SizedComponentStyleTranslator';
import { ITranslationService } from '../../../interface/ITranslationService';

import CarouselStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/CarouselStyle');
import { CarouselStyle as AppstudioCarouselStyleDto } from '../../../dto/app/configuration/uiconfig/screen/dataview/carousel/CarouselStyle';
import { Style as ClientBaseStyleDto } from '../../../dto/clientapp/Style';
import { ClientAppModelFactory } from '../../../factory/ClientAppModelFactory';

import PositionSchema = require('../../../schema/common/Position');

export class CarouselStyleTranslator extends SizedComponentStyleTranslator<AppstudioCarouselStyleDto, ClientBaseStyleDto> {
    public static SOURCE_TYPE = CarouselStyleSchema.ID;

    constructor() {
        super();
        this._translation = ClientAppModelFactory.create('Style');
    }

    public translate(accountId: string,
                     service: ITranslationService,
                     source: AppstudioCarouselStyleDto): Promise<ClientBaseStyleDto> {

        let paginationDots = source && source.paginationDots;
        let paginationSelectedDotColors = source && source.paginationSelectedDotColors;
        let paginationUnselectedDotColors = source && source.paginationUnselectedDotColors;
        let paginationLocation = source && source.paginationLocation;
        let paginationMargin = source && source.paginationMargin;
        let autoScrollDuration = source && source.autoScrollDuration;

        _.extend(this._translation, {
            itemScale: _.get(source, 'tileScale', null),
            pagination: {
                visible: paginationDots,
                selectedColor: paginationSelectedDotColors,
                unselectedColor: paginationUnselectedDotColors,
                alignment: this._getPosition(source),
                location: paginationLocation,
                margin: paginationMargin
            },
            autoScrollDuration: autoScrollDuration
        });

        return super.translate(accountId, service, source);
    }

    private _getPosition(source: AppstudioCarouselStyleDto): string {
        return <string> _.get(source, 'paginationDotPosition.value', PositionSchema.getDefaultValue());
    }
}