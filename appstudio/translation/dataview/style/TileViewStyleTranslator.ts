import Promise = require('bluebird');

import { SizedComponentStyleTranslator } from '../../component/SizedComponentStyleTranslator';
import { ITranslationService } from '../../../interface/ITranslationService';

import DataViewTileStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/tile/DataViewTileStyle');
import { DataViewTileStyle as AppstudioTileViewStyleDto } from '../../../dto/app/configuration/uiconfig/screen/dataview/tile/DataViewTileStyle';
import { Style as ClientBaseStyleDto } from '../../../dto/clientapp/Style';

import { ClientAppModelFactory } from '../../../factory/ClientAppModelFactory';

export class TileViewStyleTranslator extends SizedComponentStyleTranslator<AppstudioTileViewStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = DataViewTileStyleSchema.ID;

    constructor() {
        super();
        this._translation = ClientAppModelFactory.create('ContainerStyle');
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioTileViewStyleDto): Promise<ClientBaseStyleDto> {
        let self = this;
        const aspectRatio = source && source.aspectRatio;
        const backgroundColor = source && source.backgroundColor;

        return new Promise<ClientBaseStyleDto> ((resolve, reject) => {
            return service.translate(accountId, backgroundColor).then((translatedBackgroundColor) => {
                self._translation.backgroundColor = translatedBackgroundColor;

                return service.translate(accountId, aspectRatio).then(
                    (translatedAspectRatio) => {
                        this._translation.aspectRatio = translatedAspectRatio;
                        return super.translate(accountId, service, source);
                    });
            }).then(resolve).catch(reject);

        });
    }
}