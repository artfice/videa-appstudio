///<reference path="../../../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('underscore');

import {SizedComponentStyleTranslator} from '../../component/SizedComponentStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import DataViewHeadingStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/heading/DataViewHeadingStyle');
import {DataViewHeadingStyle as AppstudioDataViewHeadingStyleDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/heading/DataViewHeadingStyle';
import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';


export class DataViewHeadingStyleTranslator extends SizedComponentStyleTranslator<AppstudioDataViewHeadingStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = DataViewHeadingStyleSchema.ID;

    constructor() {
        super();
        this._translation = ClientAppModelFactory.create('ContainerStyle');
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioDataViewHeadingStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            const backgroundColor = source && source.backgroundColor;

            return service.translate(accountId, backgroundColor).then((translatedBackgroundColor) => {
                _.extend(translation, {
                    backgroundColor: translatedBackgroundColor
                });
            }).then(() => {
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}