import Promise = require('bluebird');

import {SizedComponentStyleTranslator} from '../../component/SizedComponentStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import GridViewStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/GridViewStyle');
import {GridViewStyle as AppstudioGridViewStyleDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/gridview/GridViewStyle';
import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';
import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';


export class GridViewStyleTranslator extends SizedComponentStyleTranslator<AppstudioGridViewStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = GridViewStyleSchema.ID;

    constructor() {
        super();
        this._translation = ClientAppModelFactory.create('ContainerStyle');
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioGridViewStyleDto): Promise<ClientBaseStyleDto> {
        return super.translate(accountId, service, source);
    }
}