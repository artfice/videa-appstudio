import {ComponentControlsTranslator} from '../component/ComponentControlsTranslator';

import CarouselControlsSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/CarouselControls');

export class CarouselControlsTranslator extends ComponentControlsTranslator {

    static SOURCE_TYPE = CarouselControlsSchema.ID;

    constructor() {
        super();
    }
}