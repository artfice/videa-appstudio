import {GridViewTranslator} from './GridViewTranslator';

import SwimlaneSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/swimlane/Swimlane');

export class SwimlaneTranslator extends  GridViewTranslator {

    public static SOURCE_TYPE = SwimlaneSchema.ID;

    constructor (config) {
        super(config, 'overlay');
    }

    protected _getColumns(): number {
        return undefined;
    }

    protected _getRows(): number {
        return 1;
    }

    protected _getScroll(): string {
        return 'horizontal';
    }
}