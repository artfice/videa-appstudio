import {GridViewTranslator} from './GridViewTranslator';

import ListViewSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/listview/ListView');

export class ListViewTranslator extends GridViewTranslator {

    public static SOURCE_TYPE = ListViewSchema.ID;

    constructor (config) {
        super(config, 'right');
    }

    protected _getColumns(): number {
        return 1;
    }

    protected _getRows(): number {
        return undefined;
    }
}