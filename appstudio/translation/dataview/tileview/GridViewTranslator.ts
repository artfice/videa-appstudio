import Promise = require('bluebird');

import {ITranslationService} from '../../../interface/ITranslationService';
import {TileViewTranslator} from '../TileViewTranslator';

import {BaseCollection as AppstudioBaseCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';
import {ComponentControls as AppstudioComponentControlsDto} from '../../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../../dto/common/listener/action/BaseAction';
import {GridView as AppstudioGridViewDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/gridview/GridView';
import {GridViewStyle as AppstudioGridViewStyleDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/gridview/GridViewStyle';

import {TileView as ClientTileViewDto} from '../../../dto/clientapp/component/dataview/TileView';
import {Component as ClientComponentDto} from '../../../dto/clientapp/Component';
import {Container as ClientContainerDto} from '../../../dto/clientapp/component/Container';
import {Layout as ClientLayoutDto} from '../../../dto/clientapp/component/layout/Layout';
import {Collection as ClientCollectionDto} from '../../../dto/clientapp/component/collection/Collection';
import {TileStyle as ClientTileStyleDto} from '../../../dto/clientapp/component/dataview/TileStyle';

import GridViewSchema = require ('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/GridView');

import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

export class GridViewTranslator extends  TileViewTranslator {

    public static SOURCE_TYPE = GridViewSchema.ID;

    constructor (config, toolbarPosition?: string) {
        super(config, toolbarPosition);

        this._tileContainer.setToolbarLayout(ClientAppModelFactory.create('LinearLayout'));
        this._tileContainer.setToolbarWidth('matchParent');
        this._tileContainer.setToolbarHeight('wrapContent');
    }

    public translate(
        accountId:string,
        service: ITranslationService,
        source: AppstudioGridViewDto <AppstudioBaseCollectionDto,
                                      AppstudioGridViewStyleDto,
                                      AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>> ): Promise<ClientTileViewDto<ClientLayoutDto,
                                               ClientComponentDto<ClientTileStyleDto>,
                                               ClientContainerDto,
                                               ClientCollectionDto>> {
        const style = source && source.style;
        const columns = style && style.columns;
        const rows = style && style.rows;

        if (columns) {
            this._setColumns(columns);
        }

        if (rows) {
            this._setRows(rows);
        }

        return super.translate(accountId, service, source);
    }

}