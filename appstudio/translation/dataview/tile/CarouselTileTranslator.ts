import Promise = require('bluebird');
import _ = require('underscore');

import CarouselTileSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/CarouselTile');

import {BaseTranslator} from '../../BaseTranslator';

import {ITranslationService} from '../../../interface/ITranslationService';

import {DataViewTile as AppStudioDataViewTileDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/DataViewTile';
import {ComponentControls as AppstudioComponentControlsDto} from '../../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../../dto/common/listener/action/BaseAction';

import {Container as ClientContainerDto} from '../../../dto/clientapp/component/Container';
import {Style as ClientStyleDto} from '../../../dto/clientapp/Style';

import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

export class CarouselTileTranslator extends BaseTranslator<AppStudioDataViewTileDto, ClientContainerDto>  {

    public static SOURCE_TYPE = CarouselTileSchema.ID;

    protected _tileLabelContainer: ClientContainerDto;

    constructor () {
        super();

        this._tileLabelContainer = <ClientContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('LinearLayout'),
            gravity: 'bottom'
        });

        this._translation = <ClientContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('RelativeLayout'),
            items: []
        });
    }

    public translate (accountId: string, service: ITranslationService, source: AppStudioDataViewTileDto): Promise<ClientContainerDto> {
        const translation = this._translation;
        const settings = source && source.settings;
        const style = settings && settings.style;
		const controls = settings && settings.controls;
        let texts = source && source.text ? source.text : [];
        let subComponents = source && source.subComponent ? source.subComponent : [];
        let tileLabelContainer = this._tileLabelContainer;

        texts = _.map(texts, function(text){
            return service.translate(accountId, text);
        });

        subComponents = _.map(subComponents, function(image){
            return service.translate(accountId, image);
        });

        return new Promise<ClientContainerDto> ((resolve, reject) => {
            Promise.all(subComponents).then((translatedSubComponents) => {
                    translation.items = translatedSubComponents ? translatedSubComponents : [];
                    return Promise.all(texts);
            }).then((translatedText) => {
                    tileLabelContainer.items = translatedText;
                    translation.items.push(tileLabelContainer);
                    return service.translate(accountId, style);
            }).then((translatedStyle: ClientStyleDto) => {
                    translation.style = translatedStyle;
                    translation.aspectRatio = translatedStyle && translatedStyle.aspectRatio;
                    return service.translate(accountId, controls);
            }).then(
                (translatedControls: AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>) => {
                    translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                    translation.listeners = translatedControls && translatedControls.listeners ? translatedControls.listeners : [];
                    return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}