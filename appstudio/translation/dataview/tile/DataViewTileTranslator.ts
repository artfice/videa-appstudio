import Promise = require('bluebird');
import _ = require('underscore');

import DataviewTileSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/DataViewTile');

import {BaseTranslator} from '../../BaseTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {DataViewTile as AppStudioDataViewTileDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/DataViewTile';
import {ComponentControls as AppstudioComponentControlsDto} from '../../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../../dto/common/listener/action/BaseAction';

import {Container as ClientContainerDto} from '../../../dto/clientapp/component/Container';
import {Style as ClientStyleDto} from '../../../dto/clientapp/Style';
import {TileStyle as ClientTileStyleDto} from '../../../dto/clientapp/component/dataview/TileStyle';


import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

export class DataViewTileTranslator extends BaseTranslator<AppStudioDataViewTileDto, ClientContainerDto>  {

    public static SOURCE_TYPE = DataviewTileSchema.ID;

    protected _tileImageContainer: ClientContainerDto;
    protected _tileLabelContainer: ClientContainerDto;

    constructor () {
        super();


        this._tileImageContainer = <ClientContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('RelativeLayout')
        });

        this._tileLabelContainer = <ClientContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('LinearLayout')
        });

        this._translation = <ClientContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('LinearLayout'),
            style: <ClientStyleDto>ClientAppModelFactory.create('ContainerStyle'),
            items: [
                this._tileImageContainer,
                this._tileLabelContainer
            ]
        });
    }

    public translate (accountId: string, service: ITranslationService, source: AppStudioDataViewTileDto): Promise<ClientContainerDto> {
        const translation = this._translation;
        const settings = source && source.settings;
        const style = settings && settings.style;
		const controls = settings && settings.controls;
        let texts = source && source.text ? source.text : [];
        let subComponents = source && source.subComponent ? source.subComponent : [];
        const tileImageContainer = this._tileImageContainer;
        let tileLabelContainer = this._tileLabelContainer;

        texts = _.map(texts, function(text){
            return service.translate(accountId, text);
        });

        subComponents = _.map(subComponents, function(subComponent){
            return service.translate(accountId, subComponent);
        });

        return new Promise<ClientContainerDto> ((resolve, reject) => {
            Promise.all(subComponents).then((translatedsubComponents) => {
                    tileImageContainer.items = translatedsubComponents ? translatedsubComponents : [];
                    return Promise.all(texts);
            }).then((translatedText) => {
                    tileLabelContainer.items = translatedText ? translatedText : [];
                    return service.translate(accountId, style);
            }).then((translatedStyle: ClientTileStyleDto) => {
                    translatedStyle = _.omit(translatedStyle, _.isUndefined);
                    translatedStyle =_.omit(translatedStyle, _.isNull);
                    translation.style = translatedStyle;
                    if (translatedStyle && translatedStyle.aspectRatio) {
                        tileImageContainer.aspectRatio = translatedStyle.aspectRatio
                    }

                    let backgroundColor = translatedStyle && translatedStyle.backgroundColor;
                    return service.translate(accountId, backgroundColor);
            }).then((translatedBackgroundColor) => {
                translation.style.backgroundColor = translatedBackgroundColor;
                return service.translate(accountId, controls);
            }).then(
                (translatedControls: AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>) => {
                    translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                    translation.listeners = translatedControls && translatedControls.listeners ? translatedControls.listeners : [];
                    return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}