import Promise = require('bluebird');

import CollectionViewSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/collectionview/CollectionsView');

import {CollectionView as AppstudioCollectionDto} from '../../dto/app/configuration/uiconfig/screen/collectionview/CollectionView';

import {ITranslationService} from '../../interface/ITranslationService';
import {ComponentTranslator} from '../component/ComponentTranslator';

import {CollectionView as ClientCollectionViewDto} from '../../dto/clientapp/component/dataview/collectionview/CollectionView';

import {PickerRenderer as ClientPickerRendererDto} from '../../dto/clientapp/component/dataview/collectionview/PickerRenderer';
import {Container as ClientContainerDto} from '../../dto/clientapp/component/Container';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {Panel} from '../../dto/clientapp/panel/Panel';
import {CollectionView as CollectionsView} from '../../dto/clientapp/component/CollectionView';

export class CollectionsViewTranslator extends ComponentTranslator<AppstudioCollectionDto, ClientCollectionViewDto> {

    static SOURCE_TYPE = CollectionViewSchema.ID;

    protected _pickerRender: ClientPickerRendererDto;

    protected _gridview: ClientContainerDto;

    protected _collectionView: CollectionsView;

    constructor() {
        super();

        const panel = new Panel({
            toolbarPosition: 'bottom'
        });

        this._collectionView = new CollectionsView({
            type: 'CollectionsView'
        });

        this._collectionView.setHeight('wrapContent');
        this._collectionView.setWidth('matchParent');

        panel.setToolbarLayout(ClientAppModelFactory.create('LinearLayout'));
        panel.setToolbarWidth('matchParent');
        panel.setToolbarHeight('wrapContent');
        panel.setLayoutType('Grid');
        panel.setColumns(1);

        this._pickerRender = <ClientPickerRendererDto>ClientAppModelFactory.create('PickerRenderer', {
            width: 'wrapContent',
            height: '50'
        });

        this._gridview = panel.getContentContainer();

        this._collectionView.setItemRenderer(this._gridview);

        this._collectionView.setPickerRenderer(this._pickerRender);

        this._translation = this._collectionView.getContainer();
    }

    protected setItemRenderer(clientContainer: ClientContainerDto) {
        if (clientContainer) {
            this._collectionView.setItemRenderer(clientContainer);
        }
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioCollectionDto): Promise<ClientCollectionViewDto> {
        const translation = this._translation;
        const pickerRenderer = this._pickerRender;
        const picker = source && source.picker;
        const appstudioGridView = source && source.gridview;
        const style = picker && picker.style;
        const content = picker && picker.content;
        const controls = source && source.controls;

        return new Promise<ClientCollectionViewDto>((resolve, reject) => {
            service.translate(accountId, content).then((translatedContent) => {
                translation.collection = translatedContent && translatedContent.collection;
                pickerRenderer.displayName = translatedContent && translatedContent.displayName;
                pickerRenderer.type = translatedContent && translatedContent.type;
                return service.translate(accountId, style);
            }).then((translatedStyle) => {
                pickerRenderer.style = translatedStyle;
                return service.translate(accountId, controls);
            }).then((translatedControls) => {
                translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                translation.listeners = translatedControls && translatedControls.listeners ? translatedControls.listeners : [];
                return service.translate(accountId, appstudioGridView);
            }).then((translatedGridView) => {
                this.setItemRenderer(translatedGridView);
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}