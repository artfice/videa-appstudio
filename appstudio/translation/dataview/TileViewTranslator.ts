import _ = require('underscore');
import Promise = require('bluebird');

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {ITranslationService} from '../../interface/ITranslationService';

import {SizedComponentStyle as AppstudioSizedComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/SizedComponentStyle';
import {BaseCollection as AppstudioBaseCollectionDto} from '../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';
import {ComponentControls as AppstudioComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../dto/common/listener/action/BaseAction';

import {DataView as AppstudioDataViewDto} from '../../dto/app/configuration/uiconfig/screen/dataview/DataView';
import {DataView as ClientDataViewDto} from '../../dto/clientapp/component/dataview/DataView';
import {TileView as ClientTileViewDto} from '../../dto/clientapp/component/dataview/TileView';
import {ContainerStyle as ContainerStyleDto} from '../../dto/clientapp/component/ContainerStyle';

import {Component as ClientComponentDto} from '../../dto/clientapp/Component';
import {Container as ClientContainerDto} from '../../dto/clientapp/component/Container';
import {Style as ClientStyleDto} from '../../dto/clientapp/Style';
import {Layout as ClientLayoutDto} from '../../dto/clientapp/component/layout/Layout';
import {Collection as ClientCollectionDto} from '../../dto/clientapp/component/collection/Collection';
import {TileStyle as ClientTileStyleDto} from '../../dto/clientapp/component/dataview/TileStyle';
import {GridLayout as ClientGridLayoutDto} from '../../dto/clientapp/component/layout/GridLayout';

import {ComponentTranslator} from '../component/ComponentTranslator';

import {Panel} from '../../dto/clientapp/panel/Panel';
import {DataView} from '../../dto/clientapp/component/DataView';

export class TileViewTranslator extends ComponentTranslator<AppstudioDataViewDto<AppstudioBaseCollectionDto,
    AppstudioSizedComponentStyleDto,
    AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>>,
    ClientTileViewDto<ClientLayoutDto,
    ClientComponentDto<ClientTileStyleDto>,
    ClientContainerDto,
    ClientCollectionDto>> {

    protected _layout: ClientGridLayoutDto;
    protected _dataView: DataView<ClientDataViewDto<ClientLayoutDto, ClientComponentDto<ClientStyleDto>, ClientContainerDto, ClientCollectionDto>>;
    protected _tileContainer: Panel;
    protected _headerContainer: ClientContainerDto;

    constructor(config, toolbarPosition?: string) {
        super();

        this._style = <ContainerStyleDto>ClientAppModelFactory.create('ContainerStyle');

        this._headerContainer = <ClientContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('RelativeLayout'),
            style: ClientAppModelFactory.create('ContainerStyle')
        });

        this._dataView = new DataView();

        this._tileContainer = new Panel({
            toolbarPosition: toolbarPosition || 'bottom'
        });

        this._layout = <ClientGridLayoutDto>ClientAppModelFactory.create('GridLayout');

        if (this._getColumns()) {
            this._layout.columns = this._getColumns();
        }

        if (this._getRows()) {
            this._layout.rows = this._getRows();
        }

        var scroll = this._getScroll();

        if (scroll) {
            this._dataView.setScroll(scroll);
        }
        this._dataView.setLayout(this._layout);

        this._dataView.setItemRenderer(this._tileContainer.getContentContainer());

        this._translation = ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('LinearLayout'),
            style: <ClientStyleDto>ClientAppModelFactory.create('ContainerStyle'),
            items: [
                this._headerContainer,
                this._dataView.getContainer()
            ]
        });
    }

    protected _getColumns(): number {
        return 1;
    }

    protected _getRows(): number {
        return undefined;
    }

    protected _getScroll(): string {
        return undefined;
    }

    public _setHeaderContainer(headerContainerDto: ClientContainerDto) {
        this._headerContainer = headerContainerDto;
    }

    public _setLayout(layoutDto: ClientGridLayoutDto) {
        this._layout = layoutDto;
    }

    protected _setRows(rows: number) {
        this._dataView.setRows(rows);
    }

    protected _setColumns(columns: number) {
        this._dataView.setColumns(columns);
    }

    public getHeaderContainer(): ClientContainerDto {
        return this._headerContainer;
    }

    public getPanelContainer(): ClientContainerDto {
        return this._tileContainer.getContentContainer();
    }

    public translate(
        accountId: string,
        service: ITranslationService,
        source: AppstudioDataViewDto<AppstudioBaseCollectionDto,
            AppstudioSizedComponentStyleDto,
            AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>>): Promise<ClientTileViewDto<ClientLayoutDto,
            ClientComponentDto<ClientTileStyleDto>,
            ClientContainerDto,
            ClientCollectionDto>> {
        const heading = source && source.heading;
        const headerContainer = this._headerContainer;
        const tile = source && source.tile;
        const tileContainer = this._tileContainer;
        const translation = this._translation;
        let translatedTileImageContainer;
        let translatedTileLabelContainer;
        const content = source && source.content && source.content.collection;
        const controls = source && source.controls;
        const dataview = this._dataView;

        return new Promise<ClientTileViewDto<ClientLayoutDto,
            ClientComponentDto<ClientTileStyleDto>,
            ClientContainerDto,
            ClientCollectionDto>>((resolve, reject) => {
                service.translate(accountId, heading).then((translatedHeading) => {
                    _.extend(headerContainer, translatedHeading);
                    return service.translate(accountId, tile);
                }).then((translatedTile) => {
                    translatedTileImageContainer = translatedTile && translatedTile.items[0];
                    translatedTileLabelContainer = translatedTile && translatedTile.items[1];

                    tileContainer.setToolbarContainer(translatedTileImageContainer);
                    tileContainer.setStyle(translatedTile && translatedTile.style);
                    tileContainer.setAspectRatio(translatedTileImageContainer && translatedTileImageContainer.aspectRatio);

                    tileContainer.addToolbarItem(translatedTileLabelContainer);

                    tileContainer.setVisible(translatedTile && translatedTile.visible);

                    if (translatedTile) {
                        translatedTile.listeners.forEach(translatedListener => {
                            tileContainer.addListener(translatedListener);
                        });
                    }
                    return service.translate(accountId, controls);
                }).then((translatedControls) => {
                    translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                    translation.listeners = translatedControls && translatedControls.listeners ? translatedControls.listeners : [];
                    return service.translate(accountId, content);
                }).then((translatedContent) => {
                    dataview.setCollection(translatedContent);
                    return super.translate(accountId, service, source);
                }).then(resolve).catch(reject);
            });

    }

}