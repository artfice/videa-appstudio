///<reference path="../../../../typings/index.d.ts"/>

import Promise = require('bluebird');

import UIDCollectionSchema = require('../../../schema/common/collection/UIDCollection');

import {BaseCollectionTranslator} from './BaseCollectionTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {UIDCollection as AppstudioUIDCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/UIDCollection';

export class UIDCollectionTranslator extends BaseCollectionTranslator<AppstudioUIDCollectionDto,AppstudioUIDCollectionDto> {

    public static SOURCE_TYPE = UIDCollectionSchema.ID;

    constructor () {
        super(<AppstudioUIDCollectionDto>{});
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioUIDCollectionDto): Promise<AppstudioUIDCollectionDto> {
        return super.translate(accountId, service,source);
    }
}