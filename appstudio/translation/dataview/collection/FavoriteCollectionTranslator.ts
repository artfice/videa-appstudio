import FavoriteCollectionSchema = require('../../../schema/common/collection/FavoriteCollection');

import {BaseCollectionTranslator} from './BaseCollectionTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {FavoriteCollection as AppstudioFavoriteCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/FavoriteCollection';
import {AppstudioModelFactory} from '../../../factory/AppstudioModelFactory';

import Promise = require('bluebird');

export class FavoriteCollectionTranslator extends BaseCollectionTranslator<AppstudioFavoriteCollectionDto,AppstudioFavoriteCollectionDto> {

    public static SOURCE_TYPE = FavoriteCollectionSchema.ID;

    constructor () {
        super(<AppstudioFavoriteCollectionDto>{});
    }

    public translate (accountId: string, service: ITranslationService, 
    source: AppstudioFavoriteCollectionDto): Promise<AppstudioFavoriteCollectionDto> {
        if (source && !source.range) {
            source.range = AppstudioModelFactory.create('Range', {
                beginIndex: 0,
                endIndex: 9
            });
        }
        return super.translate(accountId, service, source);
    }
}
