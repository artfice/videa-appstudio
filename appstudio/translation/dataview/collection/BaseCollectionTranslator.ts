///<reference path="../../../../typings/index.d.ts"/>

import Promise = require('bluebird');

import CollectionSchema = require('../../../schema/common/collection/BaseCollection');

import {IEntityTranslator} from '../../../interface/IEntityTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {BaseCollection as AppstudioCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';

import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

import JsonProvider = require('../../../schema/common/collection/provider/JsonProvider');
import MpxProvider = require('../../../schema/common/collection/provider/MpxProvider');
import VideaProvider = require('../../../schema/common/collection/provider/VideaProvider');
import CustomProvider = require('../../../schema/common/collection/provider/CustomProvider');
import VimondProvider = require('../../../schema/common/collection/provider/VimondProvider');
import AppProvider = require('../../../schema/common/collection/provider/AppProvider');

export class BaseCollectionTranslator<T extends AppstudioCollectionDto,
    R extends AppstudioCollectionDto> implements IEntityTranslator<AppstudioCollectionDto,
    AppstudioCollectionDto>  {

    public static SOURCE_TYPE = CollectionSchema.ID;

    protected _translation;

    constructor(dto: R) {
        this._translation = dto;
    }

    public translate(accountId: string, service: ITranslationService, source: T): Promise<R> {
        const provider = source && source.provider;
        const providerType = provider && provider._metadata;
        const collectionPath = provider && provider.collectionPath;

        this._translation = ClientAppModelFactory.create(source._metadata, source);
        switch (providerType) {
            case MpxProvider.ID:
                this._translation.provider = 'MPX';
                break;
            case VideaProvider.ID:
                this._translation.provider = 'Videa';
                break;
            case JsonProvider.ID:
                this._translation.provider = 'JSON';
                break;
            case AppProvider.ID:
                delete this._translation.provider;
                break;
            case CustomProvider.ID:
                if (provider && provider.customProvider) {
                   this._translation.provider = provider.customProvider;
                } else {
                   delete this._translation.provider; 
                }
                break;
            case VimondProvider.ID:
                this._translation.provider = 'Vimond';
                break;
            default:
                break;
        }

        if (collectionPath && collectionPath.length > 0) {
            this._translation.collectionPath = collectionPath;
        }

        return Promise.resolve(this._translation);
    }
}