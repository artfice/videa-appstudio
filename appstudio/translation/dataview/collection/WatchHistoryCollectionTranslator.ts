import WatchHistoryCollectionSchema = require('../../../schema/common/collection/WatchHistoryCollection');

import {BaseCollectionTranslator} from './BaseCollectionTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {WatchHistoryCollection as AppstudioWatchHistoryCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/WatchHistoryCollection';
import {AppstudioModelFactory} from '../../../factory/AppstudioModelFactory';

import Promise = require('bluebird');

export class WatchHistoryCollectionTranslator extends BaseCollectionTranslator<AppstudioWatchHistoryCollectionDto, AppstudioWatchHistoryCollectionDto> {

    public static SOURCE_TYPE = WatchHistoryCollectionSchema.ID;

    constructor() {
        super(<AppstudioWatchHistoryCollectionDto>{});
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioWatchHistoryCollectionDto): Promise<AppstudioWatchHistoryCollectionDto> {
        if (source && !source.range) {
            source.range = AppstudioModelFactory.create('Range', {
                beginIndex: 0,
                endIndex: 9
            });
        }

        if (source && (!source.group || source.group == '""' || source.group == "''")) {
            delete source.group;
        }

        return super.translate(accountId, service, source);
    }
}