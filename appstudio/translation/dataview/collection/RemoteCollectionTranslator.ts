///<reference path="../../../../typings/index.d.ts"/>

import Promise = require('bluebird');

import RemoteCollectionSchema = require('../../../schema/common/collection/RemoteCollection');

import {BaseCollectionTranslator} from './BaseCollectionTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {RemoteCollection as AppstudioRemoteCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/RemoteCollection';
import {Range} from '../../../dto/Range';

export class RemoteCollectionTranslator extends BaseCollectionTranslator<AppstudioRemoteCollectionDto, AppstudioRemoteCollectionDto> {

    public static SOURCE_TYPE = RemoteCollectionSchema.ID;

    constructor() {
        super(<AppstudioRemoteCollectionDto>{});
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioRemoteCollectionDto): Promise<AppstudioRemoteCollectionDto> {
        if (source && !source.range) {
            source.range = <Range>{
                beginIndex: 0,
                endIndex: 9
            };
        }

        return super.translate(accountId, service, source);
    }
}