import Promise = require('bluebird');

import LinkCollectionSchema = require('../../../schema/common/collection/LinkCollection');

import {BaseCollectionTranslator} from './BaseCollectionTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {LinkCollection as AppstudioLinkCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/LinkCollection';


export class LinkCollectionTranslator extends BaseCollectionTranslator<AppstudioLinkCollectionDto, AppstudioLinkCollectionDto> {

    public static SOURCE_TYPE = LinkCollectionSchema.ID;

    constructor() {
        super(<AppstudioLinkCollectionDto>{});
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioLinkCollectionDto): Promise<AppstudioLinkCollectionDto> {
        return super.translate(accountId, service, source);
    }
}