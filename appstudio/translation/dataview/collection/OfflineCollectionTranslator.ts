import Promise = require('bluebird');

import OfflineCollectionSchema = require('../../../schema/common/collection/OfflineCollection');

import {BaseCollectionTranslator} from './BaseCollectionTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {OfflineCollection as AppstudioOfflineCollectionDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/OfflineCollection';


export class OfflineCollectionTranslator extends BaseCollectionTranslator<AppstudioOfflineCollectionDto,AppstudioOfflineCollectionDto> {

    public static SOURCE_TYPE = OfflineCollectionSchema.ID;

    constructor () {
        super(<AppstudioOfflineCollectionDto>{});
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioOfflineCollectionDto): Promise<AppstudioOfflineCollectionDto> {
        return super.translate(accountId, service,source);
    }
}