import Promise = require('bluebird');
import _ = require('underscore');

import DataViewHeadingSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/DataViewHeading');

import {BaseTranslator} from '../../BaseTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {DataViewHeading as AppStudioDataViewHeadingDto} from '../../../dto/app/configuration/uiconfig/screen/dataview/DataViewHeading';

import {Container as ClientContainerDto} from '../../../dto/clientapp/component/Container';

import {ComponentControls as AppstudioComponentControlsDto} from '../../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../../dto/common/listener/action/BaseAction';


import {ClientAppModelFactory} from '../../../factory/ClientAppModelFactory';

export class DataViewHeadingTranslator extends BaseTranslator<AppStudioDataViewHeadingDto, ClientContainerDto>  {

    public static SOURCE_TYPE = DataViewHeadingSchema.ID;

    constructor() {
        super();

        this._translation = <ClientContainerDto>ClientAppModelFactory.create('Container', {
            layout: ClientAppModelFactory.create('RelativeLayout'),
            style: ClientAppModelFactory.create('ContainerStyle')
        });
    }

    public translate(accountId: string, service: ITranslationService, source: AppStudioDataViewHeadingDto): Promise<ClientContainerDto> {
        const translation = this._translation;
        const settings = source && source.settings;
        const style = settings && settings.style;
        const controls = settings && settings.controls;
        let texts = source && source.text ? source.text : [];

        texts = _.map(texts, function (text) {
            return service.translate(accountId, text);
        });

        return new Promise<ClientContainerDto>((resolve, reject) => {
            Promise.all(texts).then((translatedText) => {
                translation.items = translatedText ? translatedText : [];
                return service.translate(accountId, style);
            }).then((translatedStyle) => {
                translation.width = translatedStyle && translatedStyle.width;
                translation.height = translatedStyle && translatedStyle.height;
                translation.style = translatedStyle && _.omit(translatedStyle, ['width', 'height']);
                return service.translate(accountId, controls);
            }).then((translatedControls: AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>) => {
                translation.visible = translatedControls && translatedControls.visible ? translatedControls.visible : 'true';
                translation.listeners = translatedControls && translatedControls.listeners ? translatedControls.listeners : [];
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}