import MobileBundleEditionSchema = require('../../schema/appstudio/app/mobile/MobileBundleEdition');
import {BaseEditionTranslator} from './BaseEditionTranslator';

export class MobileBundleEditionTranslator extends BaseEditionTranslator {
    public static SOURCE_TYPE = MobileBundleEditionSchema.ID;
    constructor(config) {
        super(config);
    }
}