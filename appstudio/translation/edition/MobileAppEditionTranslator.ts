import MobileAppEditionSchema = require('../../schema/appstudio/app/mobile/MobileAppEdition');
import {BaseEditionTranslator} from './BaseEditionTranslator';

export class MobileAppEditionTranslator extends BaseEditionTranslator {
    public static SOURCE_TYPE = MobileAppEditionSchema.ID;
    constructor(config) {
        super(config);
    }
}