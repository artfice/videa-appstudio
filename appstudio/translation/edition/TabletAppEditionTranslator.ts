import TabletAppEditionSchema = require('../../schema/appstudio/app/tablet/TabletAppEdition');
import {BaseEditionTranslator} from './BaseEditionTranslator';

export class TabletAppEditionTranslator extends BaseEditionTranslator {
    public static SOURCE_TYPE = TabletAppEditionSchema.ID;
    constructor(config) {
        super(config);
    }
}