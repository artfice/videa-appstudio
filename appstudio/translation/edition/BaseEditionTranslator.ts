import Promise = require('bluebird');
import _ = require('lodash');

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';
import {UIConfigRepository} from '../../repository/UIConfigRepository';

import ConfigurationSchema = require('../../schema/appstudio/app/Configuration');

import {Configuration as ConfigurationDto} from '../../dto/app/Configuration';
import {Provider as BaseProvider} from '../../dto/common/auth/BaseProvider';
import {Cms as CMS} from '../../dto/app/configuration/Cms';
import {IClientConfig as ClientConfigDto} from '../../dto/clientapp/IClientConfig';

import {ItemNotFoundError} from 'videa-framework/VideaError';
import * as Digi from 'videa-framework/Digi';

export class BaseEditionTranslator implements IEntityTranslator<ConfigurationDto,
ClientConfigDto<BaseProvider, CMS>>  {

    public static SOURCE_TYPE = ConfigurationSchema.ID;
    private _configRepository: UIConfigRepository;

    constructor(config: any) {
        this._configRepository = config.uiConfigRepository;
    }

    public translate(accountId: string, translationService: ITranslationService,
        configuration: ConfigurationDto): Promise<ClientConfigDto<BaseProvider, CMS>> {

        configuration = Digi.Obj.deepOmit(configuration, 'displayField');
        const clientAppDto = <ClientConfigDto<BaseProvider, CMS>>ClientAppModelFactory.create('ClientApp');
        const authentication = configuration && configuration.authentication;
        let cmsList = configuration && configuration.cms;
        const custom = configuration && configuration.custom;
        let chromecast = configuration && configuration.chromecast;
        const watchHistory = configuration && configuration.watchHistory;
        let content = custom && custom.content && JSON.parse(custom.content);
        const favorites = configuration && configuration.favorites;
        const user = configuration && configuration.user;
        const settings = configuration && configuration.settings;

        cmsList = <any>cmsList.map((CMS) => {
            return translationService.translate(accountId, CMS);
        });

        if (chromecast && !chromecast.enabled) {
            chromecast = null;
        }

        if (content && _.isEmpty(content)) {
            content = null;
        }

        _.extend(clientAppDto, {
            custom: content,
            version: settings
        });

        return new Promise<ClientConfigDto<BaseProvider, CMS>>((resolve, reject) => {
            this.translateConfigs(accountId, translationService, configuration.uiConfig).then(
                (translatedUIConfig) => {
                    clientAppDto.uiConfigs = [];
                    if (Array.isArray(translatedUIConfig)) {
                        Array.prototype.push.apply(clientAppDto.uiConfigs, translatedUIConfig);
                    } else {
                        clientAppDto.uiConfigs[0] = translatedUIConfig;
                    }
                    return translationService.translate(accountId, watchHistory);
                }).then((translatedWatchHistory) => {
                    clientAppDto.watchHistory = translatedWatchHistory;
                    return translationService.translate(accountId, authentication);
                }).then((translatedAuthentication) => {
                    if (translatedAuthentication && _.isEmpty(translatedAuthentication)) {
                        translatedAuthentication = null;
                    }
                    clientAppDto.authentication = translatedAuthentication;
                    return Promise.all(cmsList);
                }).then((translatedCms) => {
                    clientAppDto.cms = translatedCms;
                    return translationService.translate(accountId, chromecast);
                }).then((translatedChromeCast) => {
                    clientAppDto.chromecast = translatedChromeCast;
                    return translationService.translate(accountId, favorites);
                }).then((translatedFavorites) => {
                    clientAppDto.favorites = translatedFavorites;        
                    return translationService.translate(accountId, user);
                }).then((translatedUser) => {
                    clientAppDto.user = translatedUser;
                    resolve(clientAppDto);
                }).catch(reject);
        });
    }

    protected translateConfigs(accountId: string, translationService: ITranslationService, uiConfigs: string[]): Promise<any> {
        if (uiConfigs.length < 1) {
            throw new ItemNotFoundError('No Config Found');
        }
        return new Promise<any>((resolve, reject) => {
            const uiConfigPromises = uiConfigs.map((uiConfigRef) => {
                return this._configRepository.getById(accountId, uiConfigRef);
            });

            return Promise.all(uiConfigPromises).then((uiConfigs) => {
                const translatedUIConfigPromises = uiConfigs.map((uiConfig) => {
                    return translationService.translate(accountId, uiConfig);
                });
                
                return Promise.all(translatedUIConfigPromises).then(resolve).catch(reject);
            });
        });
    }
}