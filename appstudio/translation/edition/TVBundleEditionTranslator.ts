import TVBundleEditionSchema = require('../../schema/appstudio/app/tv/TVBundleEdition');
import {BaseEditionTranslator} from './BaseEditionTranslator';

export class TVBundleEditionTranslator extends BaseEditionTranslator {
    public static SOURCE_TYPE = TVBundleEditionSchema.ID;
    constructor(config) {
        super(config);
    }
}