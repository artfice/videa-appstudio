import Promise = require('bluebird');

import ExternalEditionSchema = require('../../schema/appstudio/app/ExternalEdition');

import {UIConfigRepository} from '../../repository/UIConfigRepository';

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';

import {ExternalEdition as ExternalEditionDto} from '../../dto/app/ExternalEdition';

import {ExternalResourceError} from 'videa-framework/VideaError';

export class ExternalEditionTranslator implements IEntityTranslator<ExternalEditionDto, any>  {
    public static SOURCE_TYPE = ExternalEditionSchema.ID;

    private _configRepository: UIConfigRepository;

    constructor(config) {
        this._configRepository = config.configRepository;
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: ExternalEditionDto): Promise<any> {
        const uiConfigId = source && source.uiConfig && source.uiConfig[0];
        if (!uiConfigId) {
            throw new ExternalResourceError('No External Config Found');
        }
        return this._configRepository.getById(accountId, uiConfigId).then(
            (uiConfig) => {
            return translationService.translate(accountId, uiConfig);
        }).catch((e) => {
            return e;
        });
    }
}