import TVAppEditionSchema = require('../../schema/appstudio/app/tv/TVAppEdition');
import {BaseEditionTranslator} from './BaseEditionTranslator';

export class TVAppEditionTranslator extends BaseEditionTranslator {
    public static SOURCE_TYPE = TVAppEditionSchema.ID;
    constructor(config) {
        super(config);
    }
}