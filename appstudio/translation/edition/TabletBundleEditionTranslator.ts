import TabletBundleEditionSchema = require('../../schema/appstudio/app/tablet/TabletBundleEdition');
import {BaseEditionTranslator} from './BaseEditionTranslator';

export class TabletBundleEditionTranslator extends BaseEditionTranslator {
    public static SOURCE_TYPE = TabletBundleEditionSchema.ID;
    constructor(config) {
        super(config);
    }
}