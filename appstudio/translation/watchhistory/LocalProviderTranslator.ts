///<reference path="../../../typings/index.d.ts"/>
import _ = require('underscore');
import Promise = require('bluebird');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import LocalProviderSchema = require('../../schema/appstudio/app/configuration/uiconfig/watchhistory/LocalProvider');
import {LocalProvider as AppstudioLocalProviderDto} from '../../dto/app/configuration/watchhistory/LocalProvider';
import {WatchHistory as WatchHistoryDto} from '../../dto/clientapp/app/WatchHistory';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';


export class LocalProviderTranslator extends BaseTranslator<AppstudioLocalProviderDto, WatchHistoryDto>  {
    public static SOURCE_TYPE = LocalProviderSchema.ID;

    constructor() {
        super();
        this._translation = ClientAppModelFactory.create('LocalWatchHistoryProvider');
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioLocalProviderDto): Promise<WatchHistoryDto> {

        const completionThreshold = source && source.completionThreshold;
        const frequency = source && source.frequency;
        const onPauseEvent = source && source.onPauseEvent;
        const onResumeEvent = source && source.onResumeEvent;
        const onSeekEvent = source && source.onSeekEvent;
        const onStopEvent = source && source.onStopEvent;
        const onPlayEvent = source && source.onPlayEvent;
        let events = [];

        if (onPauseEvent) {
            events.push('onPause');
        }
        if (onResumeEvent) {
            events.push('onResume');
        }
        if (onSeekEvent) {
            events.push('onSeek');
        }
        if (onStopEvent) {
            events.push('onStop');
        }
        if (onPlayEvent) {
            events.push('onPlay');
        }

        _.extend(this._translation, {
            completionThreshold: completionThreshold,
            frequency: frequency,
            events: events
        });

        return super.translate(accountId, service, source);
    }
}