///<reference path="../../../typings/index.d.ts"/>
import _ = require('underscore');
import Promise = require('bluebird');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import MpxProviderSchema = require('../../schema/appstudio/app/configuration/uiconfig/watchhistory/MpxProvider');
import {MpxProvider as AppstudioMpxProviderDto} from '../../dto/app/configuration/watchhistory/MpxProvider';
import {WatchHistory as WatchHistoryDto} from '../../dto/clientapp/app/WatchHistory';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

export class MpxProviderTranslator extends BaseTranslator<AppstudioMpxProviderDto, WatchHistoryDto>  {
    public static SOURCE_TYPE = MpxProviderSchema.ID;

    constructor() {
        super();
        this._translation = ClientAppModelFactory.create('MpxWatchHistoryProvider');
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioMpxProviderDto): Promise<WatchHistoryDto> {

        const completionThreshold = source && source.completionThreshold;
        const frequency = source && source.frequency;
        const host = source && source.host;
        const refreshInterval = source && source.refreshInterval;
        const onPauseEvent = source && source.onPauseEvent;
        const onResumeEvent = source && source.onResumeEvent;
        const onSeekEvent = source && source.onSeekEvent;
        const onStopEvent = source && source.onStopEvent;
        const onPlayEvent = source && source.onPlayEvent;
        let events = [];

        if (onPauseEvent) {
            events.push('onPause');
        }
        if (onResumeEvent) {
            events.push('onResume');
        }
        if (onSeekEvent) {
            events.push('onSeek');
        }
        if (onStopEvent) {
            events.push('onStop');
        }
        if (onPlayEvent) {
            events.push('onPlay');
        }

        _.extend(this._translation, {
            completionThreshold: completionThreshold,
            frequency: frequency,
            host: host,
            refreshInterval: refreshInterval,
            events: events
        });

        return super.translate(accountId, service, source);
    }
}