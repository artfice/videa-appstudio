///<reference path="../../../typings/index.d.ts"/>

import _ = require('underscore');
import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';

import {BaseTranslator} from '../BaseTranslator';

import WatchHistorySchema = require('../../schema/appstudio/app/configuration/WatchHistory');
import NoneProvider = require('../../schema/appstudio/app/configuration/uiconfig/watchhistory/NoneProvider');

import {WatchHistory as WatchHistoryDto} from '../../dto/clientapp/app/WatchHistory';
import {WatchHistory as AppstudioWatchHistoryDto} from '../../dto/app/configuration/WatchHistory';
import {BaseProvider as BaseProvider} from '../../dto/app/configuration/watchhistory/BaseProvider';

export class WatchHistoryTranslator extends BaseTranslator<AppstudioWatchHistoryDto<BaseProvider>, WatchHistoryDto> {
    public static SOURCE_TYPE = WatchHistorySchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: AppstudioWatchHistoryDto<BaseProvider>): Promise<WatchHistoryDto> {

        const translation = this._translation;
        const provider = source && source.provider;
        const providerMetaData = provider && provider._metadata;

        return new Promise<WatchHistoryDto>((resolve, reject) => {
            if (!provider || (providerMetaData && providerMetaData == NoneProvider.ID)) {
                resolve(null);
            }

            translationService.translate(accountId, provider).then(
                (translatedProvider) => {
                    _.extend(translation, translatedProvider);
                    return super.translate(accountId, translationService, source);
                }).then(resolve).catch(reject);
        });
    }
}