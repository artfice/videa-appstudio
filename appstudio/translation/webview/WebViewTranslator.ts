import {ITranslationService} from '../../interface/ITranslationService';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import WebViewSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebView');
import {WebView as AppstudioWebViewDto} from '../../dto/app/configuration/uiconfig/screen/webview/WebView';
import {WebView as ClientWebViewDto} from '../../dto/clientapp/component/WebView';
import {Style} from '../../dto/clientapp/Style';
import _ = require('lodash');
import Promise = require('bluebird');

import {ComponentTranslator} from '../component/ComponentTranslator';

export class WebViewTranslator  extends  ComponentTranslator <AppstudioWebViewDto, ClientWebViewDto<Style>> {
	public static SOURCE_TYPE = WebViewSchema.ID;
		
	constructor() {
		super();
		this._translation = <ClientWebViewDto<Style>>ClientAppModelFactory.create('WebView');
	}

	public translate(accountId: string, service: ITranslationService, 
					 source: AppstudioWebViewDto): Promise<ClientWebViewDto<Style>> {
		
		const translation = this._translation;
        const content = source && source.content;
        const style = source && source.style;
        const controls = source && source.controls;
        const name = source && source.name;

        translation.name = name;
        
        return new Promise<ClientWebViewDto<Style>>((resolve, reject) => {
            service.translate(accountId, content).then(
                (translatedContent) => {
                    translation.uri = translatedContent && translatedContent.uri;
                    return service.translate(accountId, style);  
            }).then((translatedStyle) => {
                _.extend(translation, {
                    width: translatedStyle && translatedStyle.width,
                    height: translatedStyle && translatedStyle.height,
                    verticalScrollEnabled: translatedStyle && translatedStyle.verticalScrollEnabled,
                    horizontalScrollEnabled: translatedStyle && translatedStyle.horizontalScrollEnabled,
                    fallbackMargin: translatedStyle && translatedStyle.fallbackMargin
                });
                
                source.style = _.omit(style, ['width', 'height', 'gravity', 'verticalScrollEnabled',
                'horizontalScrollEnabled', 'fallbackMargin']);
                return service.translate(accountId, controls);
            }).then((translatedControls) => {
                    _.extend(translation, {
                        visible: translatedControls && translatedControls.visible,
                        listeners: translatedControls && translatedControls.listeners ? translatedControls.listeners : []
                    });
                    return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
		});
	}
}