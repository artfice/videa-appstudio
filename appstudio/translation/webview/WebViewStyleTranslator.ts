import {SizedComponentStyleTranslator} from '../component/SizedComponentStyleTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import WebViewStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebViewStyle');
import {WebViewStyle as AppstudioWebViewStyleDto} from '../../dto/app/configuration/uiconfig/screen/webview/WebViewStyle';
import {Style as ClientBaseStyleDto} from '../../dto/clientapp/Style';

import Promise = require('bluebird');
import _ = require('lodash');

export class WebViewStyleTranslator<T extends AppstudioWebViewStyleDto, R extends ClientBaseStyleDto> extends SizedComponentStyleTranslator<AppstudioWebViewStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = WebViewStyleSchema.ID;
    
    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioWebViewStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;
        const fallbackMargin = source && source.fallbackMargin;
        const verticalScrollEnabled = source && source.verticalScrollEnabled;
        const horizontalScrollEnabled = source && source.horizontalScrollEnabled;
        _.extend(translation, {
            fallbackMargin: fallbackMargin,
            verticalScrollEnabled: verticalScrollEnabled,
            horizontalScrollEnabled: horizontalScrollEnabled
        });
        return super.translate(accountId, service, source);
    }
}