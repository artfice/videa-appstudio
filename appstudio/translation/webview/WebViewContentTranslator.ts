import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';

import WebViewContentSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebViewContent');
import {WebViewContent as WebViewContentDto} from '../../dto/app/configuration/uiconfig/screen/webview/WebViewContent';

import Promise = require('bluebird');

export class WebViewContentTranslator implements IEntityTranslator<WebViewContentDto, WebViewContentDto>  {

	public static SOURCE_TYPE = WebViewContentSchema.ID;

	public translate (accountId: string, service: ITranslationService, source: WebViewContentDto): Promise<WebViewContentDto> {
		var uri = source && source.uri;
        return Promise.resolve(<WebViewContentDto>{
			uri: uri || ''
		});
	}
}