import Promise = require('bluebird');

import { ITranslationService } from '../interface/ITranslationService';

import { BaseTranslator } from './BaseTranslator';

import { ClientAppModelFactory } from '../factory/ClientAppModelFactory';

import ListenerSchema = require('../schema/common/Listener');
import PlaybackActionSchema = require('../schema/common/listener/action/PlaybackAction');
import AnalyticActionSchema = require('../schema/common/listener/action/AnalyticAction');
import NavigationActionSchema = require('../schema/common/listener/action/NavigationAction');

import { Listener as ListenerDto } from '../dto/common/Listener';
import { BaseEvent as BaseEventDto } from '../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../dto/common/listener/action/BaseAction';

export class ListenerTranslator extends BaseTranslator<ListenerDto<BaseEventDto,
    BaseActionDto>, ListenerDto<BaseEventDto, BaseActionDto>> {
    static SOURCE_TYPE = ListenerSchema.ID;

    constructor() {
        super();
        this._translation = <ListenerDto<BaseEventDto,
            BaseActionDto>>ClientAppModelFactory.create('Listener');
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: ListenerDto<BaseEventDto, BaseActionDto>): Promise<ListenerDto<BaseEventDto,
        BaseActionDto>> {

        const event = source.event && ClientAppModelFactory.create(source.event._metadata, source.event);
        const action: any = source.action && ClientAppModelFactory.create(source.action._metadata, source.action);
        const active = source.active;
        
        if (action) {
            switch (action._metadata) {
                case PlaybackActionSchema.ID:
                    if (action.startTime !== undefined && action.startTime.length < 1) {
                        delete action.startTime;
                    }
                    break;
                case AnalyticActionSchema.ID:
                    action.event = (action.event && action.event.value) ? action.event.value : '';
                    delete action.displayField;
                    break;
				case NavigationActionSchema.ID:
					if (action.collection) {
						return translationService.translate(accountId, action.collection).then(
							(translatedCollection) => {
								if (active) {
									this._translation.active = active;
								}

								this._translation.event = event;
								this._translation.action = action;
								this._translation.action.collection = translatedCollection;
								return Promise.resolve(this._translation);
							});
					}
					break;                    
                default:
                    break;
            }
        }

		if (active) {
			this._translation.active = active;
		}

        this._translation.event = event;
        this._translation.action = action;

        return super.translate(accountId, translationService, source);
    }
}