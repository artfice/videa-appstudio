import _ = require('lodash');
import Promise = require('bluebird');

import LabelContentSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/content/LabelContent');

import {FieldContentTranslator} from './FieldContentTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {LabelContent as AppstudioLabelContentDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/content/LabelContent';
import {LabelContent as ClientLabelContentDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/content/LabelContent';


export class LabelContentTranslator<T extends AppstudioLabelContentDto, R extends AppstudioLabelContentDto> extends FieldContentTranslator<AppstudioLabelContentDto, ClientLabelContentDto>  {
    public static SOURCE_TYPE = LabelContentSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioLabelContentDto): Promise<ClientLabelContentDto> {
        const numberOfLines = source && source.numberOfLines;
        const maxLines = source && source.maxLines;
        const link = source && source.link && source.link.length > 0 ? source.link : undefined;

        _.extend(this._translation, {
            numberOfLines: numberOfLines,
            link: link,
            maxLines: maxLines
        });

        return super.translate(accountId, service, source);
    }
}