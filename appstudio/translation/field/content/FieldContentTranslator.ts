///<reference path="../../../../typings/index.d.ts"/>

import _ = require('underscore');
import Promise = require('bluebird');

import {BaseTranslator} from '../../BaseTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import FieldContentSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/FieldContent');
import {FieldContent as AppstudioFieldContentDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/FieldContent';
import {FieldContent as ClientFieldContentDto} from '../../../dto/clientapp/component/field/content/FieldContent';

export class FieldContentTranslator<T extends AppstudioFieldContentDto<string>, R extends ClientFieldContentDto> extends BaseTranslator<AppstudioFieldContentDto<string>, R>  {
	public static SOURCE_TYPE = FieldContentSchema.ID;

	constructor() {
		super();
	}

	public translate (accountId: string, service: ITranslationService, source: AppstudioFieldContentDto<string>): Promise<R> {
		const value = source && source.value;

		_.extend(this._translation, {
            value: value || null
        });

		return super.translate(accountId, service, source);
	}
}