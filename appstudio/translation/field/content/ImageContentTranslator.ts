import ImageContentSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/content/ImageContent');
import {FieldContentTranslator} from './FieldContentTranslator';
import {FieldContent as AppstudioFieldContentDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/FieldContent';
import {FieldContent as ClientFieldContentDto} from '../../../dto/clientapp/component/field/content/FieldContent';

export class ImageContentTranslator extends FieldContentTranslator<AppstudioFieldContentDto<string>, ClientFieldContentDto>  {
    public static SOURCE_TYPE = ImageContentSchema.ID;

    constructor() {
        super();
    }
}