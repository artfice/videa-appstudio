///<reference path="../../../../typings/index.d.ts"/>
import _ = require('underscore');
import Promise = require('bluebird');

import TextFieldContentSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/content/TextFieldContent');

import {FieldContentTranslator} from './FieldContentTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import {TextFieldContent as AppstudioTextFieldContentDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/content/TextFieldContent';
import {TextFieldContent as ClientTextFieldContentDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/content/TextFieldContent';

export class TextFieldContentTranslator<T extends AppstudioTextFieldContentDto, R extends AppstudioTextFieldContentDto> extends FieldContentTranslator<AppstudioTextFieldContentDto, ClientTextFieldContentDto>  {
    public static SOURCE_TYPE = TextFieldContentSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioTextFieldContentDto): Promise<ClientTextFieldContentDto> {
        const inputType = source && source.inputType;
        const hint = source && source.hint;
        const componentId = source && source.componentId;

        _.extend(this._translation, {
            numberOfLines: 1,
            inputType: inputType,
            hint: hint,
            componentId: componentId
        });

        return super.translate(accountId, service, source);
    }
}