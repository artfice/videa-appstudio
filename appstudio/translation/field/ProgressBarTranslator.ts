import ProgressBarSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/field/progressBar/ProgressBar');

import {ITranslationService} from '../../interface/ITranslationService';
import {BaseTranslator} from '../BaseTranslator';

import {ProgressBar as ClientProgressBarDto} from '../../dto/clientapp/component/field/ProgressBar';
import {ProgressBar as ProgressBarDto} from '../../dto/app/configuration/uiconfig/screen/component/field/progressBar/ProgressBar';

import Promise = require('bluebird');
import _ = require('underscore');

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

export class ProgressBarTranslator extends BaseTranslator<ProgressBarDto, ClientProgressBarDto>  {

    static SOURCE_TYPE = ProgressBarSchema.ID;

    constructor () {
        const progressBar = <ClientProgressBarDto>ClientAppModelFactory.create('ProgressBar');
        super(progressBar);
    }

    public translate (accountId: string, service: ITranslationService,
    source: ProgressBarDto): Promise<ClientProgressBarDto> {
        const style = source && source.style;
		const content = source && source.content;

        return new Promise<ClientProgressBarDto> ((resolve, reject) => {
            service.translate(accountId, style).then((translatedStyle) => {
                translatedStyle = _.omit(translatedStyle, _.isUndefined);
                translatedStyle =_.omit(translatedStyle, _.isNull);
                _.extend(this._translation, {
                    width: translatedStyle && translatedStyle.width,
                    height: translatedStyle && translatedStyle.height,
                    gravity: translatedStyle && translatedStyle.gravity
                });
                source.style = _.omit(translatedStyle, ['width', 'height', 'gravity', 'border']);
                this._translation.style = source.style;
                return service.translate(accountId, content);
            }).then((translatedContent) => {
                this._translation.value = translatedContent.value;
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}