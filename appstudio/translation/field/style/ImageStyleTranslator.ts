///<reference path="../../../../typings/index.d.ts"/>
import * as Promise from 'bluebird';
import * as _ from 'lodash';

import { BaseFieldStyleTranslator } from './BaseFieldStyleTranslator';
import { ITranslationService } from '../../../interface/ITranslationService';

import ImageStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/image/ImageStyle');

import { ImageStyle as AppstudioImageStyleDto } from '../../../dto/app/configuration/uiconfig/screen/component/field/image/ImageStyle';
import { Style as ClientBaseStyleDto } from '../../../dto/clientapp/Style';

import { TranslationUtil } from '../../TranslationUtil';

export class ImageStyleTranslator extends BaseFieldStyleTranslator<AppstudioImageStyleDto, ClientBaseStyleDto> {

    public static SOURCE_TYPE = ImageStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioImageStyleDto): Promise<ClientBaseStyleDto> {
        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            this.translateScale(source);

            const aspectRatio = source && source.aspectRatio;

            return service.translate(accountId, aspectRatio).then((translatedAspectRatio) => {
                this._translation.aspectRatio = translatedAspectRatio;
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }

    private translateScale(source: AppstudioImageStyleDto): void {
        const scale = _.get(source, 'scale.value', null);
        this._translation.scale = TranslationUtil.translateScale(scale);
    }
}
