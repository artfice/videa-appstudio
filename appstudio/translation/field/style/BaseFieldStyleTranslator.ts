import Promise = require('bluebird');
import _ = require('underscore');

import {SizedComponentStyleTranslator} from '../../component/SizedComponentStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import BaseFieldStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/BaseFieldStyle');
import {BaseFieldStyle as AppstudioBaseFieldStyleDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/BaseFieldStyle';
import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

export class BaseFieldStyleTranslator<T extends AppstudioBaseFieldStyleDto, R extends ClientBaseStyleDto> extends SizedComponentStyleTranslator<AppstudioBaseFieldStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = BaseFieldStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioBaseFieldStyleDto): Promise<ClientBaseStyleDto> {
        const gravity = source && source.gravity;

        _.extend(this._translation, {
            gravity: gravity
        });

        return super.translate(accountId, service, source);
    }
}