import Promise = require('bluebird');

import {BaseFieldStyleTranslator} from './BaseFieldStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import LabelStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/label/LabelStyle');

import {LabelStyle as AppstudioLabelStyleDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/label/LabelStyle';
import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

export class LabelStyleTranslator extends BaseFieldStyleTranslator<AppstudioLabelStyleDto, ClientBaseStyleDto>  {
    public static SOURCE_TYPE = LabelStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioLabelStyleDto): Promise<ClientBaseStyleDto> {

        const font = source && source.font;

        return new Promise<ClientBaseStyleDto> ((resolve, reject) => {
            if (!font) {
                this._translation.font = null;
                super.translate(accountId, service, source).then(resolve).catch(reject);
            } else {
                service.translate(accountId, font).then((translatedFont) => {
                    this._translation.font = translatedFont;
                    return super.translate(accountId, service, source);
                }).then(resolve).catch(reject);
            }
        });
	}
}