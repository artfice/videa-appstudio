import { BaseFieldStyleTranslator } from './BaseFieldStyleTranslator';
import { ITranslationService } from '../../../interface/ITranslationService';

import ProgressBarStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/progressBar/ProgressBarStyle');

import { ProgressBarStyle as AppstudioProgressBarStyleDto } from '../../../dto/app/configuration/uiconfig/screen/component/field/progressBar/ProgressBarStyle';
import { Style as ClientBaseStyleDto } from '../../../dto/clientapp/Style';

import Promise = require('bluebird');
import _ = require('underscore');


export class ProgressBarStyleTranslator extends BaseFieldStyleTranslator<AppstudioProgressBarStyleDto, ClientBaseStyleDto>  {

    static SOURCE_TYPE = ProgressBarStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioProgressBarStyleDto): Promise<ClientBaseStyleDto> {
        var backgroundColor = source && source.backgroundColor,
            foregroundColor = source && source.foregroundColor;

        _.extend(this._translation, {
            backgroundColor: backgroundColor,
            foregroundColor: foregroundColor
        });

        return super.translate(accountId, service, source);
    }
}
