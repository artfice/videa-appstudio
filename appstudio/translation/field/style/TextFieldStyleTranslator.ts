///<reference path="../../../../typings/index.d.ts"/>
import Promise = require('bluebird');

import {BaseFieldStyleTranslator} from './BaseFieldStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import TextFieldStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/textfield/TextFieldStyle');

import {TextFieldStyle as AppstudioTextFieldStyleDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/textfield/TextFieldStyle';
import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

export class TextFieldStyleTranslator extends BaseFieldStyleTranslator<AppstudioTextFieldStyleDto, ClientBaseStyleDto>  {
    public static SOURCE_TYPE = TextFieldStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioTextFieldStyleDto): Promise<ClientBaseStyleDto> {
        const font = source && source.font;

        if (!font) {
            this._translation.font = null;
            return super.translate(accountId, service, source);
        } else {
            return service.translate(accountId, font).then((translatedFont) => {
                this._translation.font = translatedFont;
                return super.translate(accountId, service, source);
            }).catch((err) => {
                return err;
            });
        }
	}
}