///<reference path="../../../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('underscore');

import {BaseFieldStyleTranslator} from './BaseFieldStyleTranslator';
import {ITranslationService} from '../../../interface/ITranslationService';

import ToggleStyleSchema = require('../../../schema/appstudio/app/configuration/uiconfig/screen/component/field/toggle/ToggleStyle');
import {ToggleStyle as AppstudioToggleStyleDto} from '../../../dto/app/configuration/uiconfig/screen/component/field/toggle/ToggleStyle';
import {Style as ClientBaseStyleDto} from '../../../dto/clientapp/Style';

export class ToggleStyleTranslator extends BaseFieldStyleTranslator<AppstudioToggleStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = ToggleStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioToggleStyleDto): Promise<ClientBaseStyleDto> {
        const activatedColor = source && source.activatedColor;
        const deactivatedColor = source && source.deactivatedColor;

        _.extend(this._translation, {
            activatedColor: activatedColor,
            deactivatedColor: deactivatedColor
        });

        return super.translate(accountId, service, source);
    }
}