import _ = require('underscore');
import Promise = require('bluebird');

import AppStudioTextFieldSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/field/textfield/TextField');
import {ITranslationService} from '../../interface/ITranslationService';
import {BaseFieldTranslator} from './BaseFieldTranslator';

import {Label as AppstudioLabelDto} from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';
import {Label as ClientLabelDto} from '../../dto/clientapp/component/field/Label';
import {LabelStyle as ClientLabelStyleDto} from '../../dto/clientapp/component/field/style/LabelStyle';

import {ClientAppModelFactory as ClientappFactory} from '../../factory/ClientAppModelFactory';

export class TextFieldTranslator extends BaseFieldTranslator<AppstudioLabelDto, ClientLabelDto<ClientLabelStyleDto>> {

    public static SOURCE_TYPE = AppStudioTextFieldSchema.ID;

    constructor() {
        super();

        this._translation = ClientappFactory.create('TextField', {
            numberOfLines: 1,
            inputType: 'Text'
        });
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioLabelDto): Promise<ClientLabelDto<ClientLabelStyleDto>> {
        const translation = this._translation;
        const style = source && source.style;
        const content = source && source.content;

        return new Promise<ClientLabelDto<ClientLabelStyleDto>> ((resolve, reject) => {
            service.translate(accountId, content).then((translatedContent) => {
                    translation.inputType = translatedContent && translatedContent.inputType;
                    translation.hint = translatedContent && translatedContent.hint;
                    source.id = translatedContent && translatedContent.componentId ? translatedContent.componentId : source.id;
                    return service.translate(accountId, style);
            }).then((translatedStyle: ClientLabelStyleDto) => {
                    _.extend(this._style, translatedStyle);
                    return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}