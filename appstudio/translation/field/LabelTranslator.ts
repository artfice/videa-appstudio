///<reference path="../../../typings/index.d.ts"/>
import _ = require('underscore');
import Promise = require('bluebird');

import AppStudioLabelSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/field/label/Label');
import {ITranslationService} from '../../interface/ITranslationService';
import {BaseFieldTranslator} from './BaseFieldTranslator';

import {Label as AppstudioLabelDto} from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';
import {Label as ClientLabelDto} from '../../dto/clientapp/component/field/Label';
import {LabelStyle as ClientLabelStyleDto} from '../../dto/clientapp/component/field/style/LabelStyle';
import {LabelContent as ClientLabelContentDto} from '../../dto/app/configuration/uiconfig/screen/component/field/content/LabelContent';

import {ClientAppModelFactory as ClientappFactory} from '../../factory/ClientAppModelFactory';

export class LabelTranslator extends BaseFieldTranslator<AppstudioLabelDto, ClientLabelDto<ClientLabelStyleDto>> {

    public static SOURCE_TYPE = AppStudioLabelSchema.ID;

    constructor() {
        super();

        this._translation = ClientappFactory.create('Label');
    }

    public translate(accountId: string, service: ITranslationService,
        source: AppstudioLabelDto): Promise<ClientLabelDto<ClientLabelStyleDto>> {
        const translation = this._translation;
        const style = source && source.style;
        const content = source && source.content;

        return new Promise<ClientLabelDto<ClientLabelStyleDto>>((resolve, reject) => {
            service.translate(accountId, content).then(
                (translatedContent: ClientLabelContentDto) => {
                    translation.numberOfLines = translatedContent && translatedContent.numberOfLines;
                    translation.link = translatedContent && translatedContent.link;
                    translation.maxLines = translatedContent && translatedContent.maxLines;
                    return service.translate(accountId, style);
                }).then((translatedStyle: ClientLabelStyleDto) => {
                    if (translatedStyle) {
                        delete translatedStyle.gravity;
                        delete translatedStyle.width;
                        delete translatedStyle.height;
                    }
                    _.extend(this._style, translatedStyle);
                    return super.translate(accountId, service, source);
                }).then(resolve).catch(reject);
        });
    }
}