import _ = require('underscore');
import Promise = require('bluebird');

import AppStudioToggleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/field/toggle/Toggle');
import {ITranslationService} from '../../interface/ITranslationService';
import {BaseFieldTranslator} from './BaseFieldTranslator';

import {Toggle as AppstudioToggleDto} from '../../dto/app/configuration/uiconfig/screen/component/field/toggle/Toggle';
import {Toggle as ClientToggleDto} from '../../dto/clientapp/component/field/Toggle';
import {ToggleStyle as ClientToggleStyleDto} from '../../dto/clientapp/component/field/style/ToggleStyle';

import {ClientAppModelFactory as ClientappFactory} from '../../factory/ClientAppModelFactory';

export class ToggleTranslator extends BaseFieldTranslator<AppstudioToggleDto, ClientToggleDto<ClientToggleStyleDto>> {

    public static SOURCE_TYPE = AppStudioToggleSchema.ID;

    constructor() {
        super();

        this._translation = ClientappFactory.create('Toggle');
    }

    public translate(accountId: string, service: ITranslationService, source: AppstudioToggleDto): Promise<ClientToggleDto<ClientToggleStyleDto>> {
        const style = source && source.style;

        return new Promise<ClientToggleDto<ClientToggleStyleDto>>((resolve, reject) => {
            service.translate(accountId, style).then((translatedStyle) => {
                _.extend(this._style, translatedStyle);
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}