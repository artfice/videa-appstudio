///<reference path="../../../typings/index.d.ts"/>
import _ = require('underscore');
import Promise = require('bluebird');

import ImageSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/field/image/Image');
import {ITranslationService} from '../../interface/ITranslationService';
import {BaseFieldTranslator} from './BaseFieldTranslator';

import {Image as AppstudioImageDto} from '../../dto/app/configuration/uiconfig/screen/component/field/image/Image';
import {ImageStyle as ClientImageStyleDto} from '../../dto/clientapp/component/field/style/ImageStyle';
import {Image as ClientImageDto} from '../../dto/clientapp/component/field/Image';
import {Style as ClientStyle} from '../../dto/clientapp/Style';


import {ClientAppModelFactory as ClientappFactory} from '../../factory/ClientAppModelFactory';

export class ImageTranslator extends BaseFieldTranslator<AppstudioImageDto, ClientImageDto<ClientStyle>> {

    public static SOURCE_TYPE = ImageSchema.ID;

    constructor() {
        super();

        this._translation = ClientappFactory.create('Image');
    }

    public translate (accountId: string, service: ITranslationService, source: AppstudioImageDto): Promise<ClientImageDto<ClientStyle>> {
        var translation = this._translation,
          style = source && source.style;

        return new Promise<ClientImageDto<ClientStyle>> ((resolve, reject) => {
            service.translate(accountId, style).then(
                (translatedStyle: ClientImageStyleDto) => {
                    _.extend(translation, {
                        scale: translatedStyle && translatedStyle.scale || 'aspectFit',
                        aspectRatio: translatedStyle && translatedStyle.aspectRatio
                    });
                    source.style = _.omit(style, ['scale', 'aspectRatio']);
                    return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}