///<reference path="../../../typings/index.d.ts"/>

import _ = require('underscore');
import Promise = require('bluebird');

import BaseFieldSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/field/BaseField');
import {ITranslationService} from '../../interface/ITranslationService';
import {ComponentTranslator} from '../component/ComponentTranslator';

import {BaseField as AppstudioBaseFieldDto} from '../../dto/app/configuration/uiconfig/screen/component/field/BaseField';
import {BaseFieldStyle as AppstudioBaseFieldStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/field/BaseFieldStyle';
import {ComponentControls as AppstudioComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto} from '../../dto/common/listener/action/BaseAction';
import {BaseField as ClientBaseFieldDto} from '../../dto/clientapp/component/field/BaseField';
import {Style as ClientStyle} from '../../dto/clientapp/Style';
import {FieldContent as ClientFieldContentDto} from '../../dto/clientapp/component/field/content/FieldContent';
import {BaseFieldStyle as ClientBaseFieldStyleDto} from '../../dto/clientapp/component/field/style/BaseFieldStyle';


export class BaseFieldTranslator<T extends AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>, R extends ClientBaseFieldDto<ClientStyle>> extends ComponentTranslator<AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>, ClientBaseFieldDto<ClientStyle>> {

	public static SOURCE_TYPE = BaseFieldSchema.ID;

	constructor () {
		super();
    }

	public translate (accountId: string, service: ITranslationService, source: AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>): Promise<ClientBaseFieldDto<ClientStyle>> {
		const translation = this._translation;
        const style = source && source.style;
        const controls = source && source.controls;
        const content = source && source.content;

		return new Promise<ClientBaseFieldDto<ClientStyle>> ((resolve, reject) => {
            service.translate(accountId, content).then(
                (translatedContent: ClientFieldContentDto) => {
                    translation.value = translatedContent && translatedContent.value;
                    return service.translate(accountId, style);
            }).then((translatedStyle : ClientBaseFieldStyleDto) => {
                _.extend(translation, {
                    width: translatedStyle && translatedStyle.width,
                    height: translatedStyle && translatedStyle.height,
                    gravity: translatedStyle && translatedStyle.gravity
                });

                source.style = _.omit(style, ['width', 'height', 'gravity']);
                return service.translate(accountId, controls);
            }).then(
                (translatedControls: AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>) => {
                    _.extend(translation, {
                        visible: translatedControls && translatedControls.visible,
                        listeners: translatedControls && translatedControls.listeners ? translatedControls.listeners : []
                    });
                    return super.translate(accountId, service, <AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>>source);
            }).then(resolve).catch(reject);
        });
	}
}