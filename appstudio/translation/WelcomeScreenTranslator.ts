import { ITranslationService } from '../interface/ITranslationService';
import { IEntityTranslator } from '../interface/IEntityTranslator';

import {WelcomeScreen as AppstudioWelcomeScreenDto} from '../dto/app/configuration/uiconfig/settings/WelcomeScreen';
import {WelcomeScreen as ClientWelcomeScreenDto} from '../dto/clientapp/app/uiconfig/WelcomeScreen';
import WelcomeScreenSchema = require('../schema/appstudio/app/configuration/uiconfig/settings/WelcomeScreen');

import Promise = require('bluebird');

export class WelcomeScreenTranslator implements IEntityTranslator<AppstudioWelcomeScreenDto, ClientWelcomeScreenDto>  {
    public static SOURCE_TYPE = WelcomeScreenSchema.ID;

    public translate(accountId: string, translationService: ITranslationService, source: AppstudioWelcomeScreenDto):
        Promise<ClientWelcomeScreenDto> {
        const enabled = source && source.enabled;
        if (!enabled || !source.screen || !source.screen.refId) {
            return Promise.resolve(undefined);
        } else {
            return Promise.resolve(<ClientWelcomeScreenDto>{
                screenId: source.screen.refId,
                persistKey: source.persistKey
            });
        }
    }
}