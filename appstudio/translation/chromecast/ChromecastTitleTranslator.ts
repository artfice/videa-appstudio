import Promise = require('bluebird');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import ChromecastTitleSchema = require('../../schema/appstudio/app/configuration/chromecast/Title');

import {Title as AppstudioChromecastTitleDto} from '../../dto/app/configuration/chromecast/Title';

export class ChromecastTitleTranslator extends BaseTranslator<AppstudioChromecastTitleDto,
    AppstudioChromecastTitleDto>  {
    static SOURCE_TYPE = ChromecastTitleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService,
    source: AppstudioChromecastTitleDto): Promise<AppstudioChromecastTitleDto> {
        const font = source && source.font;

        if (!font) {
            this._translation.font = null;
            return super.translate(accountId, service, source);
        } else {
            return service.translate(accountId, font).then((translatedFont) => {
                this._translation.font = translatedFont;
                return super.translate(accountId, service, source);
            }).catch((err) => {
                return err;
            });
        }
    }
}