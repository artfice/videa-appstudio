import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';
import {BaseTranslator} from '../BaseTranslator';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import ChromecastSchema = require('../../schema/appstudio/app/configuration/Chromecast');
import {Chromecast as ChromecastDto} from '../../dto/clientapp/app/Chromecast';

export class ChromecastTranslator extends BaseTranslator<ChromecastDto, ChromecastDto> {
    static SOURCE_TYPE = ChromecastSchema.ID;

    constructor() {
        super();
        this._translation = <ChromecastDto>ClientAppModelFactory.create('Chromecast');
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: ChromecastDto): Promise<ChromecastDto> {

        let translation = this._translation;
        const appId = source && source.appId;
        const theme = source && source.theme;
        const title = source && source.title;
        const receiver = source && source.receiver;

        translation.appId = appId;

        return new Promise<ChromecastDto>((resolve, reject) => {
            return translationService.translate(accountId, theme).then(
                (translatedTheme) => {
                    translation.theme = translatedTheme;
                    return translationService.translate(accountId, title);
                }).then(
                (translatedTitle) => {
                    translation.title = translatedTitle;
                    return translationService.translate(accountId, receiver);
                }).then(
                (translatedReceiver) => {
                    translation.receiver = translatedReceiver;
                    return super.translate(accountId, translationService, source);
                }).then(resolve).catch(reject);
        });
    }
}