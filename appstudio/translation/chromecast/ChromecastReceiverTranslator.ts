import Promise = require('bluebird');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import ChromecastReceiverSchema = require('../../schema/appstudio/app/configuration/chromecast/Receiver');

import {Receiver as AppstudioChromecastReceiverDto} from '../../dto/app/configuration/chromecast/Receiver';



export class ChromecastReceiverTranslator extends BaseTranslator<AppstudioChromecastReceiverDto,
    AppstudioChromecastReceiverDto>  {
    static SOURCE_TYPE = ChromecastReceiverSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService,
        source: AppstudioChromecastReceiverDto): Promise<AppstudioChromecastReceiverDto> {
        const font = source && source.font;

        if (!font) {
            this._translation.font = null;
            return super.translate(accountId, service, source);
        } else {
            return service.translate(accountId, font).then((translatedFont) => {
                this._translation.font = translatedFont;
                return super.translate(accountId, service, source);
            }).catch((err) => {
                return err;
            });
        }
    }
}