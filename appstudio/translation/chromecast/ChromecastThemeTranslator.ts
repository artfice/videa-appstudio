///<reference path="../../../typings/index.d.ts"/>

import _ = require('underscore');
import Promise = require('bluebird');

import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import ChromecastThemeSchema = require('../../schema/appstudio/app/configuration/chromecast/Theme');

import {Theme as AppstudioChromecastThemeDto} from '../../dto/app/configuration/chromecast/Theme';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

export class ChromecastThemeTranslator extends BaseTranslator<AppstudioChromecastThemeDto,
    AppstudioChromecastThemeDto>  {

    static SOURCE_TYPE = ChromecastThemeSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService,
        source: AppstudioChromecastThemeDto): Promise<AppstudioChromecastThemeDto> {

        const accentColor = source && source.accentColor && source.accentColor.value;
        const secondaryColor = source && source.secondaryColor && source.secondaryColor.value;
        const backgroundColor = source && source.backgroundColor && source.backgroundColor.value;
        const foregroundColor = source && source.foregroundColor && source.foregroundColor.value;

        _.extend(this._translation, {
            accentColor: ClientAppModelFactory.create('Color', {
                value: accentColor
            }),
            secondaryColor: ClientAppModelFactory.create('Color', {
                value: secondaryColor
            }),
            backgroundColor: ClientAppModelFactory.create('Color', {
                value: backgroundColor
            }),
            foregroundColor: ClientAppModelFactory.create('Color', {
                value: foregroundColor
            })
        });

        return super.translate(accountId, service, source);
    }
}