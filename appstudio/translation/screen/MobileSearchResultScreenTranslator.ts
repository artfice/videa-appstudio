import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';

import MobileSearchResultScreenSchema = require('../../schema/appstudio/app/mobile/uiconfig/screen/MobileSearchResultScreen');
import {Screen as AppStudioScreen} from '../../dto/app/configuration/uiconfig/Screen';

import {SearchResultScreenTranslator} from './SearchResultScreenTranslator';

import {Screen as ClientScreenDto} from '../../dto/clientapp/component/Screen';
import {Component as AppstudioComponent} from '../../dto/app/configuration/uiconfig/screen/component/Component';
import {ScreenStyle} from '../../dto/app/configuration/uiconfig/screen/ScreenStyle';
import {ScreenContent} from '../../dto/app/configuration/uiconfig/screen/ScreenContent';
import {BaseStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';

export class MobileSearchResultScreenTranslator extends SearchResultScreenTranslator {

    public static SOURCE_TYPE = MobileSearchResultScreenSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, translationService: ITranslationService,
        mobileScreen: AppStudioScreen<ScreenStyle, AppstudioComponent<BaseStyle>, ScreenContent>): Promise<ClientScreenDto> {
        return super.translate(accountId, translationService, mobileScreen);
    }
}