import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import NavigationStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/NavigationStyle');
import {Style as ClientBaseStyleDto} from '../../dto/clientapp/Style';
import {NavigationStyle as NavigationStyleDto} from '../../dto/app/configuration/uiconfig/screen/NavigationStyle';
import Promise = require('bluebird');
import _ = require('lodash');

export class NavigationStyleTranslator extends BaseTranslator<NavigationStyleDto, ClientBaseStyleDto>  {

    static SOURCE_TYPE = NavigationStyleSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: NavigationStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;
        const menu = source && source.menu;
        const topBar = source && source.topBar;
        const topBarStyle = topBar && topBar.style;
        const topBarTitleStyle = topBar && topBar.titleStyle;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {

            service.translate(accountId, topBarStyle).then((translatedTopBarStyle) => {
                _.extend(topBar, {
                    style: translatedTopBarStyle
                });

                _.extend(translation, {
                    menu: menu,
                    topBar: topBar
                });

                return service.translate(accountId, topBarTitleStyle);
            }).then((translatedTopBarTitleStyle) => {
                _.extend(topBar, {
                    titleStyle: translatedTopBarTitleStyle
                });
            }).then(() => {
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });

    }
}
