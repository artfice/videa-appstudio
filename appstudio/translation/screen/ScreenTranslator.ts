import _ = require('underscore');
import Promise = require('bluebird');
import {Logger} from 'videa-framework/Logger';

import {Screen as AppStudioScreen} from '../../dto/app/configuration/uiconfig/Screen';
import {Component as AppStudioComponent} from '../../dto/app/configuration/uiconfig/screen/component/Component';
import {ScreenStyle} from '../../dto/app/configuration/uiconfig/screen/ScreenStyle';
import {ScreenContent} from '../../dto/app/configuration/uiconfig/screen/ScreenContent';
import {BaseStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';

import AppStudioScreenSchema = require('../../schema/appstudio/app/configuration/uiconfig/Screen');

import {ITranslationService} from '../../interface/ITranslationService';

import {NamedComponentTranslator} from '../component/NamedComponentTranslator';

import {Screen as ClientScreen} from '../../dto/clientapp/component/Screen';
import {Component as ClientComponent} from '../../dto/clientapp/Component';
import {Style} from '../../dto/clientapp/Style';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

export class ScreenTranslator extends NamedComponentTranslator<AppStudioScreen<ScreenStyle,
    AppStudioComponent<BaseStyle>, ScreenContent>> {

    public static SOURCE_TYPE = AppStudioScreenSchema.ID;

    constructor() {
        super();
    }

    protected _getClientScreen(): ClientScreen {
        return <ClientScreen>ClientAppModelFactory.create('Screen');
    }

    public translate(accountId: string, service: ITranslationService,
        source: AppStudioScreen<ScreenStyle, AppStudioComponent<BaseStyle>, ScreenContent>): Promise<ClientScreen> {
        let f;
        let clientScreen: ClientScreen;
        const scroll = source && source.scroll || 'vertical';
        const controls = source.controls;
        const content = source && source.content;

        clientScreen = this._getClientScreen();
        clientScreen.scroll = scroll;

        f = _.map(source.item, (appStudioComponent) => {
            return service.translate(accountId, appStudioComponent);
        });

        return service.translate(accountId, controls).then((translatedControls) => {
            _.extend(clientScreen, {
                visible: translatedControls && translatedControls.visible ? translatedControls.visible : 'true',
                listeners: translatedControls && translatedControls.listeners ? translatedControls.listeners : []
            });

            return Promise.all(f);
        }).then((components: ClientComponent<Style>[]) => {

            clientScreen.items = components;

            _.extend(this._translation, clientScreen);

            // get title from content, if not set, user screen name as the title
            const title = content && content.title;
            const screenName = source && source.name;
            _.extend(this._translation, {
                title: title || screenName
            });

            return super.translate(accountId, service, source);
        }).catch((e) => {
            Logger.error('SCREEN ERROR: ', e);
            return e;
        });
    }

}