import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';


import TopBarStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/navigation/topbar/TopBarStyle');
import {Style as ClientBaseStyleDto} from '../../dto/clientapp/Style';
import {TopBarStyle as TopBarStyleDto} from '../../dto/app/configuration/uiconfig/screen/TopBarStyle';
import Promise = require('bluebird');
import _ = require('lodash');

class TopBarStyleTranslator extends BaseTranslator<TopBarStyleDto, ClientBaseStyleDto>  {

    static SOURCE_TYPE = TopBarStyleSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: TopBarStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;
        const backgroundColor = source && source.backgroundColor;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {

            service.translate(accountId, _.get(backgroundColor, 'value'))
                .then((translatedBackgroundColor) => {
                    _.extend(translation, {
                        backgroundColor: translatedBackgroundColor
                    });

                    return translation;
                }).then(() => {
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}

export = TopBarStyleTranslator;