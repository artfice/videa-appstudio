import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';

import MobileScreenSchema = require('../../schema/appstudio/app/mobile/uiconfig/screen/MobileScreen');
import {Screen as AppStudioScreen} from '../../dto/app/configuration/uiconfig/Screen';

import {ScreenTranslator} from './ScreenTranslator';

import {Screen as ClientScreenDto} from '../../dto/clientapp/component/Screen';
import {Component as AppstudioComponent} from '../../dto/app/configuration/uiconfig/screen/component/Component';
import {ScreenStyle} from '../../dto/app/configuration/uiconfig/screen/ScreenStyle';
import {ScreenContent} from '../../dto/app/configuration/uiconfig/screen/ScreenContent';
import {BaseStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';

export class MobileScreenTranslator extends ScreenTranslator {

    public static SOURCE_TYPE = MobileScreenSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, translationService: ITranslationService,
        mobileScreen: AppStudioScreen<ScreenStyle, AppstudioComponent<BaseStyle>, ScreenContent>): Promise<ClientScreenDto> {
        return super.translate(accountId, translationService, mobileScreen);
    }
}