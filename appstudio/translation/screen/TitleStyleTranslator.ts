import {BaseTranslator} from '../BaseTranslator';
import {ITranslationService} from '../../interface/ITranslationService';

import TopBarTitleStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/navigation/topbar/TitleStyle');
import {Style as ClientBaseStyleDto} from '../../dto/clientapp/Style';
import {TitleStyle as TopBarTitleStyleDto} from '../../dto/app/configuration/uiconfig/navigation/topbar/TitleStyle';
import Promise = require('bluebird');
import _ = require('underscore');

export class TitleStyleTranslator extends BaseTranslator<TopBarTitleStyleDto, ClientBaseStyleDto>  {

    static SOURCE_TYPE = TopBarTitleStyleSchema.ID;

    constructor () {
        super();
    }

    public translate (accountId: string, service: ITranslationService, source: TopBarTitleStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;
        const font = source && source.font;

        return new Promise<ClientBaseStyleDto>((resolve, reject) => {

            service.translate(accountId, font)
                .then((translatedFont) => {
                    _.extend(translation, {
                        _metadata: TopBarTitleStyleSchema.ID,
                        font: translatedFont
                    });

                    return translation;
                }).then(() => {
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}
