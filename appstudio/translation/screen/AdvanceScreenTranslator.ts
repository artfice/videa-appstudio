import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';


import AdvanceScreenSchema = require('../../schema/appstudio/app/configuration/uiconfig/AdvanceScreen');
import {AdvanceScreen as AdvanceScreenDto} from '../../dto/app/configuration/uiconfig/AdvanceScreen';
import {Screen as ClientScreenDto} from '../../dto/clientapp/component/Screen';
import * as Digi from 'videa-framework/Digi';

export class AdvanceScreenTranslator implements IEntityTranslator<AdvanceScreenDto, ClientScreenDto>  {

    static SOURCE_TYPE = AdvanceScreenSchema.ID;

    constructor() {

    }

    public translate(accountId: string, translationService: ITranslationService,
        advanceScreen: AdvanceScreenDto): Promise<ClientScreenDto> {
        return new Promise<ClientScreenDto>((resolve, reject) => {
            const content = advanceScreen && advanceScreen.customData && advanceScreen.customData.content;
            const name = advanceScreen && advanceScreen.name;

            if (content) {
                try {
                    let contentObj = <ClientScreenDto>JSON.parse(content);
                    contentObj.id = advanceScreen.id;
                    contentObj.name = name;
                    contentObj = Digi.Obj.deepOmit(contentObj, '_metadata');
                    contentObj._metadata = advanceScreen._metadata;
                    resolve(contentObj);
                } catch (e) {
                    reject(new Error('invalid json ' + JSON.stringify(e, null, 4)));
                }
            } else {
                reject(new Error('invalid advance screen ' + JSON.stringify(advanceScreen, null, 4)));
            }

        });
    }
}