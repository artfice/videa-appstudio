import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';
import SearchResultScreenSchema = require('../../schema/appstudio/app/configuration/uiconfig/SearchResultScreen');

import {Screen as ClientScreenDto} from '../../dto/clientapp/component/Screen';

import {ScreenTranslator} from './ScreenTranslator';

import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {Screen as AppStudioScreen} from '../../dto/app/configuration/uiconfig/Screen';
import {ScreenStyle } from '../../dto/app/configuration/uiconfig/screen/ScreenStyle';
import {ScreenContent } from '../../dto/app/configuration/uiconfig/screen/ScreenContent';
import {BaseStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';
import {Component as AppStudioComponent} from '../../dto/app/configuration/uiconfig/screen/component/Component';

export class SearchResultScreenTranslator extends ScreenTranslator {

    public static SOURCE_TYPE = SearchResultScreenSchema.ID;

    constructor() {
        super();
    }

    protected _getClientScreen(): ClientScreenDto {
        return <ClientScreenDto>ClientAppModelFactory.create('SearchResultsScreen');
    }

    public translate(accountId: string, translationService: ITranslationService,
        mobileScreen: AppStudioScreen<ScreenStyle,
            AppStudioComponent<BaseStyle>, ScreenContent>): Promise<ClientScreenDto> {
        return super.translate(accountId, translationService,
            <AppStudioScreen<ScreenStyle, AppStudioComponent<BaseStyle>, ScreenContent>>mobileScreen);
    }
}