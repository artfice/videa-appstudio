///<reference path="../../../typings/index.d.ts"/>
import Promise = require('bluebird');
import _ = require('underscore');

import { ComponentStyleTranslator } from '../component/ComponentStyleTranslator';
import { ITranslationService } from '../../interface/ITranslationService';

import ScreenStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/ScreenStyle');
import { Style as ClientBaseStyleDto } from '../../dto/clientapp/Style';
import { ScreenStyle as ScreenStyleDto } from '../../dto/app/configuration/uiconfig/screen/ScreenStyle';

export class ScreenStyleTranslator extends ComponentStyleTranslator<ScreenStyleDto, ClientBaseStyleDto>  {

    public static SOURCE_TYPE = ScreenStyleSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, service: ITranslationService, source: ScreenStyleDto): Promise<ClientBaseStyleDto> {
        let translation = this._translation;
        const backgroundColor = source && source.backgroundColor;
        return new Promise<ClientBaseStyleDto>((resolve, reject) => {
            service.translate(accountId, backgroundColor)
                .then((translatedBackgroundColor) => {
                    const backgroundImage = source && source.backgroundImage && source.backgroundImage;

                    _.extend(translation, {
                        backgroundColor: translatedBackgroundColor,
                        backgroundImage: backgroundImage
                    });
                    return translation;
                }).then(() => {
                const navigation = source && source.navigation;
                return service.translate(accountId, navigation);
            }).then((translatedNavigationStyle) => {
                _.extend(translation, {
                    navigation: translatedNavigationStyle
                });
                return translation;
            }).then(() => {
                return super.translate(accountId, service, source);
            }).then(resolve).catch(reject);
        });
    }
}