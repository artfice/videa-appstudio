///<reference path="../../typings/index.d.ts"/>

import Promise = require('bluebird');
import _ = require('underscore');

import { ITranslationService } from '../interface/ITranslationService';
import { IEntityTranslator } from '../interface/IEntityTranslator';

import { CustomData as CustomDataDto } from '../dto/common/CustomData';
import CustomDataSchema = require('../schema/common/CustomData');
import * as Digi from 'videa-framework/Digi';

export class CustomDataTranslator implements IEntityTranslator<CustomDataDto, CustomDataDto>  {
    public static SOURCE_TYPE = CustomDataSchema.ID;

    public translate(accountId: string, translationService: ITranslationService,
        customData: CustomDataDto):
        Promise<CustomDataDto> {
        const content: any = customData && customData.content;
        let clientProvider: any = {};

        try {
            clientProvider = JSON.parse(content);
            if (_.isEmpty(clientProvider)) {
                clientProvider = null;
            }

            clientProvider = Digi.Obj.deepOmit(clientProvider, '_metadata');
        } catch (e) {
            clientProvider = null;
        }

        return Promise.resolve(clientProvider);
    }
}