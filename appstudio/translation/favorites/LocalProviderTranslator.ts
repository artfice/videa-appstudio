import _ = require('lodash');
import Promise = require('bluebird');

import { ITranslationService } from '../../interface/ITranslationService';
import { BaseTranslator } from '../BaseTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import AppstudioLocalProviderSchema = require('../../schema/appstudio/app/configuration/favorites/LocalProvider');
import LocalProviderSchema = require('../../schema/clientapp/app/favorites/LocalProvider');

import { BaseProvider as LocalProviderDto } from '../../dto/app/configuration/favorites/BaseProvider';
import { Favorites as FavoritesDto } from '../../dto/clientapp/app/Favorites';

export class LocalProviderTranslator extends BaseTranslator<LocalProviderDto, FavoritesDto> {
    public static SOURCE_TYPE = AppstudioLocalProviderSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: LocalProviderDto): Promise<FavoritesDto> {

        this.addProviderInfoToTranslation(source);
        return <Promise<FavoritesDto>>super.translate(accountId, translationService, source);
    }

    private addProviderInfoToTranslation(source: LocalProviderDto): void {
        const providerInfo: FavoritesDto = <FavoritesDto>ClientAppModelFactory.create(LocalProviderSchema.ID);

        if (source) {
            providerInfo.frequency = source.frequency;
            providerInfo.uid = source.uid;
            providerInfo.link = source.link;
            providerInfo.collectionPath = source.collectionPath;
        }
        _.extend(this._translation, providerInfo);
    }
}