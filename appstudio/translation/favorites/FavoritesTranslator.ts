import _ = require('lodash');
import Promise = require('bluebird');


import { ITranslationService } from '../../interface/ITranslationService';
import { BaseTranslator } from '../BaseTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import AppstudioFavoritesSchema = require('../../schema/appstudio/app/configuration/Favorites');
import FavoritesSchema = require('../../schema/clientapp/app/Favorites');
import NoDataSchema = require('../../schema/common/NoData');

import { Favorites as FavoritesDto } from '../../dto/clientapp/app/Favorites';

import { Favorites as AppstudioFavoritesDto } from '../../dto/app/configuration/Favorites';
import { BaseProvider } from '../../dto/app/configuration/favorites/BaseProvider';

export class FavoritesTranslator extends BaseTranslator<AppstudioFavoritesDto<BaseProvider>, FavoritesDto> {
    public static SOURCE_TYPE = AppstudioFavoritesSchema.ID;

    constructor() {
        super();
    }

    public translate(accountId: string, translationService: ITranslationService,
        source: AppstudioFavoritesDto<BaseProvider>): Promise<FavoritesDto> {

        const providerDto: BaseProvider = _.get(source, 'provider', undefined);
        const providerMetadata: string = _.get(source, 'provider._metadata', NoDataSchema.ID);

        return new Promise<FavoritesDto>((resolve, reject) => {
            if (_.isEqual(providerMetadata, NoDataSchema.ID)) {
                return resolve(null);
            }

            this.addFavoritesInfoToTranslation();

            translationService.translate(accountId, providerDto).then((providerInfo: FavoritesDto) => {
                _.extend(this._translation, providerInfo);
                return super.translate(accountId, translationService, source);

            }).then(resolve).catch(reject);
        });
    }

    private addFavoritesInfoToTranslation(): void {
        const favoritesInfo: FavoritesDto = <FavoritesDto>ClientAppModelFactory.create(FavoritesSchema.ID);
        _.extend(this._translation, favoritesInfo);
    }
}