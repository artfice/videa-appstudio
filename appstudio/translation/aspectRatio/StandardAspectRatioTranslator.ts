///<reference path="../../../typings/index.d.ts"/>
import Promise = require('bluebird');

import { ITranslationService } from '../../interface/ITranslationService';
import { IEntityTranslator } from '../../interface/IEntityTranslator';
import {TranslationUtil} from '../TranslationUtil';

import {AspectRatio as AspectRatioDto} from '../../dto/common/AspectRatio';

import StandardAspectRatioSchema = require('../../schema/common/aspectRatio/StandardAspectRatio');

export class StandardAspectRatioTranslator implements IEntityTranslator<AspectRatioDto, any>  {
    public static SOURCE_TYPE = StandardAspectRatioSchema.ID;

    public translate(accountId: string, translationService: ITranslationService, 
    source: AspectRatioDto): Promise<number> {

        const value: any = source && source.value;

        return new Promise<number>((resolve, reject) => {
            resolve(TranslationUtil.translateAspectRatio(value));
        });
    }
}