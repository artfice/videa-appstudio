import Promise = require('bluebird');

import { ITranslationService } from '../../interface/ITranslationService';
import { IEntityTranslator } from '../../interface/IEntityTranslator';

import { TranslationUtil } from '../TranslationUtil';

import { AspectRatio as AspectRatioDto } from '../../dto/common/AspectRatio';

import CustomAspectRatioSchema = require('../../schema/common/aspectRatio/CustomAspectRatio');

export class CustomAspectRatioTranslator implements IEntityTranslator<AspectRatioDto, any>  {
    public static SOURCE_TYPE = CustomAspectRatioSchema.ID;

    public translate(accountId: string, translationService: ITranslationService,
        source: AspectRatioDto): Promise<number> {
        const width = source.width ? source.width : '';
        const height = source.height ? source.height : '';
        const aspectRatioString = width + ':' + height;

        return new Promise<number>((resolve, reject) => {
            resolve(TranslationUtil.translateAspectRatio(aspectRatioString));
        });
    }
}