import MobileAuthenticationSchema = require('../../schema/appstudio/app/mobile/authentication/MobileAuthentication');

import {AuthenticationTranslator} from './AuthenticationTranslator';

export class MobileAuthenticationTranslator extends AuthenticationTranslator {
    static SOURCE_TYPE = MobileAuthenticationSchema.ID;

    constructor() {
        super();
    }
}