///<reference path="../../../typings/index.d.ts"/>
import Promise = require('bluebird');

import {ITranslationService} from '../../interface/ITranslationService';
import {IEntityTranslator} from '../../interface/IEntityTranslator';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import AuthenticationSchema = require('../../schema/appstudio/app/configuration/Authentication');

import {Provider as BaseProviderDto} from '../../dto/common/auth/BaseProvider';
import {Authentication as AuthenticationDto} from '../../dto/app/configuration/Authentication';
import AkamaiProvider = require('../../schema/common/auth/AkamaiProvider');
import NoneProvider = require('../../schema/common/auth/NoneProvider');
import BasicAuthentication = require('../../schema/common/auth/BasicAuthentication');
import CustomData = require('../../schema/common/CustomData');

export class AuthenticationTranslator implements IEntityTranslator<AuthenticationDto<BaseProviderDto>,
    AuthenticationDto<BaseProviderDto>>  {
    public static SOURCE_TYPE = AuthenticationSchema.ID;

    public translate(accountId: string, translationService: ITranslationService,
    authentication: AuthenticationDto<BaseProviderDto>):
        Promise<AuthenticationDto<BaseProviderDto>> {

        const provider: any = authentication && authentication.provider;
        let clientProvider: any = {};

        if (provider) {
            clientProvider = ClientAppModelFactory.create(provider._metadata, provider);

            switch (provider._metadata) {
                case AkamaiProvider.ID:
                    let content = clientProvider.resourceAccessCodes &&
                        clientProvider.resourceAccessCodes.content ?
                        clientProvider.resourceAccessCodes.content : '{}';
                    if (content) {
                        clientProvider.resourceAccessCodes = JSON.parse(content);
                    }
                    break;
                case NoneProvider.ID:
                    clientProvider = null;
                    break;
                case BasicAuthentication.ID:
                    clientProvider.login = {
                        method: clientProvider && clientProvider.loginMethod,
                        url: clientProvider && clientProvider.loginUrl
                    };

                    clientProvider.logout = {
                        method: clientProvider && clientProvider.logoutMethod,
                        url: clientProvider && clientProvider.logoutUrl
                    };                
                    clientProvider.loginScreen = {
                        configId: clientProvider && clientProvider.loginConfigId,
                        screenId: clientProvider && clientProvider.loginScreenId
                    };

                    clientProvider.defaultScreen = {
                        configId: clientProvider && clientProvider.defaultConfigId,
                        screenId: clientProvider && clientProvider.defaultScreenId
                    };
                    delete clientProvider.loginMethod;
                    delete clientProvider.loginUrl;
                    delete clientProvider.logoutMethod;
                    delete clientProvider.logoutUrl;
                    delete clientProvider.loginScreenId;
                    delete clientProvider.loginConfigId;
                    delete clientProvider.defaultScreenId;
                    delete clientProvider.defaultConfigId;
                    break;
                case CustomData.ID:
                    content = clientProvider.content;
                    try {
                        clientProvider = JSON.parse(content);
                    } catch (e) {
                        clientProvider = {};
                    }
                    break;
                default:
                    break;
            }
        }

        return new Promise<AuthenticationDto<BaseProviderDto>>((resolve, reject) => {
            resolve(clientProvider);
        });
    }
}