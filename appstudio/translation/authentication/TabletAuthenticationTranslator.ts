import TabletAuthenticationSchema = require('../../schema/appstudio/app/tablet/authentication/TabletAuthentication');

import {AuthenticationTranslator} from './AuthenticationTranslator';

export class TabletAuthenticationTranslator extends AuthenticationTranslator  {
	static SOURCE_TYPE = TabletAuthenticationSchema.ID;

	constructor() {
        super();
	}
}