import TVAuthenticationSchema = require('../../schema/appstudio/app/tv/authentication/TVAuthentication');

import {AuthenticationTranslator} from './AuthenticationTranslator';

export class TVAuthenticationTranslator extends AuthenticationTranslator  {
	static SOURCE_TYPE = TVAuthenticationSchema.ID;

	constructor() {
        super();
	}
}