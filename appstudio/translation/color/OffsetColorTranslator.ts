import { ITranslationService } from '../../interface/ITranslationService';
import { IEntityTranslator } from '../../interface/IEntityTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import OffsetColorSchema = require('../../schema/common/OffsetColor');
import { OffsetColor as OffsetColorDto } from '../../dto/common/OffsetColor';
import { OffsetColor as ClientOffsetColorDto } from '../../dto/common/OffsetColor';

import Promise = require('bluebird');
import _ = require('lodash');

export class OffsetColorTranslator implements IEntityTranslator<OffsetColorDto, ClientOffsetColorDto>  {

    static SOURCE_TYPE = OffsetColorSchema.ID;

    public translate(accountId: string, translationService: ITranslationService, source: OffsetColorDto): Promise<ClientOffsetColorDto> {

        const color = source && source.value;
        const offset = source && source.offset;

        let translation = <any>ClientAppModelFactory.create(source._metadata);

        _.extend(translation, {
            _metadata: OffsetColorSchema.ID,
            value: color,
            offset: offset
        });

        return Promise.resolve(translation);
    }
}