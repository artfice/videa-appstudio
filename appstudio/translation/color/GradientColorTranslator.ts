import { ITranslationService } from '../../interface/ITranslationService';
import { IEntityTranslator } from '../../interface/IEntityTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';
 
 import GradientColorSchema = require('../../schema/appstudio/common/GradientColor');
 import ClientGradientColorSchema = require('../../schema/clientapp/GradientColor');
 import {GradientColor as GradientColorDto} from '../../dto/common/GradientColor';
 import {GradientColor as ClientGradientColorDto} from '../../dto/clientapp/GradientColor';

 import Directions = require('../../schema/common/enum/components/Directions');
 
 import Promise = require('bluebird');
 import _ = require('lodash');
 
 export class GradientColorTranslator implements IEntityTranslator<GradientColorDto, ClientGradientColorDto>  {
 
     static SOURCE_TYPE = GradientColorSchema.ID;
 
     public translate(accountId: string, translationService: ITranslationService, source: GradientColorDto): Promise<ClientGradientColorDto> {
 
         let translation = <any>ClientAppModelFactory.create(ClientGradientColorSchema.ID);
 
         return new Promise<ClientGradientColorDto>((resolve, reject) => {
             const stops = source && source.stops;
             const direction = source && source.direction;

             if (direction === Directions[0]) {
                // vertical
                 _.extend(translation, {
                     start: [0.5, 0.0],
                     end: [0.5,1.0]
                 });
             } else if (direction === Directions[1]) {
                 // horizontal
                 _.extend(translation, {
                     start: [0.0, 0.5],
                     end: [1.0, 0.5]
                 });
             } else {
                 reject('Unsupported direction: '  + direction);
             }
 
             let promises = [];
             let offsetStops = [];
 
             if (stops && stops.length > 0) {
                 stops.forEach((stop) => {
 
                     // offset color translation
                    promises.push(translationService.translate(accountId, stop).then((translatedStop) => {
                        offsetStops.push(translatedStop);
                    }));
                 });
             }
 
             Promise.all(promises).then(() => {
                 _.extend(translation, {
                     // TODO: remove the hardcoded fallback image once it is finalized
                     fallbackImage: 'www.videa.com/gradient.png',
                     stops: offsetStops
                 });
                 return translation;
             }).then(resolve).catch(reject);
         });
     }
 }