import { ITranslationService } from '../../interface/ITranslationService';
import { IEntityTranslator } from '../../interface/IEntityTranslator';

import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');
import ColorSchema = require('../../schema/common/Color');
import ClientGradientColorSchema = require('../../schema/clientapp/GradientColor');
import GradientColorSchema = require('../../schema/appstudio/common/GradientColor');
import {ComplexColor as ComplexColorDto} from '../../dto/common/ComplexColor';
import {Color as ClientColorDto} from '../../dto/common/Color';
import {GradientColor as ClientGradientColorDto} from '../../dto/clientapp/GradientColor';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import Promise = require('bluebird');

export class ComplexColorTranslator implements IEntityTranslator<ComplexColorDto, ClientColorDto | ClientGradientColorDto>  {

    static SOURCE_TYPE = ComplexColorSchema.ID;

    public translate(accountId: string, translationService: ITranslationService, source: ComplexColorDto): Promise<ClientColorDto | ClientGradientColorDto> {

        const complexColor = source && source.value;
        let translation;

        if (complexColor) {
            switch (complexColor._metadata) {
                case ColorSchema.ID:
                    translation = <any>ClientAppModelFactory.create(ColorSchema.ID);
                    break;
                case GradientColorSchema.ID:
                    translation = <any>ClientAppModelFactory.create(ClientGradientColorSchema.ID);
                    break;
                default:
                    console.warn('Unknown complex color: ', complexColor);
                    break;
            }

            return translationService.translate(accountId, complexColor)
                .then((translatedColor) => {
                    return Promise.resolve(translatedColor);
                });
        }

        return Promise.resolve(translation);
    }
}