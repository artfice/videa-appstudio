import { ITranslationService } from '../../interface/ITranslationService';
import { IEntityTranslator } from '../../interface/IEntityTranslator';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import ColorSchema = require('../../schema/common/Color');
import { Color as ColorDto } from '../../dto/common/Color';
import { Color as ClientColorDto } from '../../dto/common/Color';

import Promise = require('bluebird');
import _ = require('lodash');

export class ColorTranslator implements IEntityTranslator<ColorDto, ClientColorDto>  {

    static SOURCE_TYPE = ColorSchema.ID;

    public translate(accountId: string, translationService: ITranslationService, source: ColorDto): Promise<ClientColorDto> {

        const color = source && source.value;

        let translation = <any>ClientAppModelFactory.create(source._metadata);

        _.extend(translation, {
            _metadata: ColorSchema.ID,
            value: color
        });

        return Promise.resolve(translation);
    }
}