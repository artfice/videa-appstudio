import _L = require('lodash');
import _ = require('underscore');
import ObjectIterator = require('recursive-iterator');

import {Logger} from 'videa-framework/Logger';

import {BaseSchema} from 'videa-framework/schema/BaseSchema';
import ClientComponentSchema = require('../schema/clientapp/app/uiconfig/screen/Component');

import DrawerMenuSchema = require('../schema/common/navigation/DrawerMenu');
import VerticalMenuSchema = require('../schema/common/navigation/VerticalMenu');
import TopMenuSchema = require('../schema/common/navigation/TopMenu');
import AdvanceScreenSchema = require('../schema/appstudio/app/configuration/uiconfig/AdvanceScreen');
import CustomDataSchema = require('../schema/common/CustomData');
import AdvanceComponentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/AdvanceComponent');
import TVHackNavigationSchema = require('../schema/appstudio/app/tv/uiconfig/navigation/TVHackNavigation');

import AkamaiProvider = require('../schema/common/auth/AkamaiProvider');
import AdobeProvider = require('../schema/common/auth/AdobeProvider');

import {VideaStyleSheet as VssDto} from '../dto/clientapp/vss/VideaStyleSheet';
import {Rule as RuleDto} from '../dto/clientapp/vss/Rule';
import {Style as StyleDto} from '../dto/clientapp/Style';
import {Component as ClientComponentDto} from '../dto/clientapp/Component';
import {Style as ClientStyleDto} from '../dto/clientapp/Style';

import {MetadataOwner} from '../dto/MetadataOwner';

import {BaseMenu as Menu} from '../dto/common/navigation/BaseMenu';

import {ClientAppModelFactory} from '../factory/ClientAppModelFactory';

export class TranslationUtil {

    public static FONT_SIZE_MAPPING = {
        'Extra Small': 10,
        'Small': 13,
        'Medium': 15,
        'Large': 18,
        'Extra Large': 22
    };

    public static SCALE_MAPPING = {
        'Aspect Fit': 'aspectFit',
        'Aspect Fill': 'aspectFill',
        'Fill Parent': 'fillParent'
    };

    /**
     * @param  {string} scale
     */
    public static translateScale(scale: string) {
        return TranslationUtil.SCALE_MAPPING[scale];
    }

    /**
     * @param  {string} aspectRatio
     * @returns number
     */
    public static translateAspectRatio(aspectRatio: string): number {
        let ratio;

        try {
            if (!aspectRatio || aspectRatio.indexOf(':') < 0) {
                return undefined;
            }

            ratio = aspectRatio.split(':');

            return parseInt(ratio[0]) / parseInt(ratio[1]);

        } catch (e) {
            Logger.critical('error converting aspect ratio ', e);
            return undefined;
        }
    }

    /**
     * @param  {} schemaId
     * @returns string
     */
    public static generatePathFromId(schemaId): string {
        const parts = schemaId.split('.').join('/');
        return __dirname + '/../' + parts;
    }

    /**
     * @param  {Menu} menu
     */
    public static translateMainMenu(menu: Menu) {

        switch (menu._metadata) {
            case DrawerMenuSchema.ID:
                return ClientAppModelFactory.create('DrawerMenu');
            case VerticalMenuSchema.ID:
                return ClientAppModelFactory.create('VerticalMenu');
            case TopMenuSchema.ID:
                return ClientAppModelFactory.create('TopMenu');
        }

        Logger.warn('Warning: translateMainMenu: menu._metadata ' + menu._metadata + ' not available');
        return null;
    }

    public static translateFontSize(size: any) {
        const transformedSize = TranslationUtil.FONT_SIZE_MAPPING[size];
        if (!transformedSize) {
            return size;
        }
        return transformedSize;
    }

    public static getClassHierarchy(obj: MetadataOwner) {
        if (!obj) {
            Logger.critical('warning null passed to getClassHierarchy');
            return null;
        }

        const filePath = TranslationUtil.generatePathFromId(obj._metadata);
        const classHierarchy = [];

        try {

            const Schema = require(filePath);
            const schemaObject = new Schema();

            TranslationUtil._getProtoHierarchy(schemaObject, classHierarchy);
        } catch (e) {
            Logger.critical('Error finding filePath getClassHierarchy ' +
                filePath + ' ' + JSON.stringify(e, null, 4));
            return null;
        }

        return classHierarchy;
    }

    public static classExtends(obj: MetadataOwner, parentId: string) {
        const classHierarchy = TranslationUtil.getClassHierarchy(obj);
        if (classHierarchy === null) {
            return false;
        }

        return _.indexOf(classHierarchy, parentId) > -1;
    }

    private static _getProtoHierarchy(obj: BaseSchema, h: any[]) {
        if (obj['__proto__'] && obj['__proto__'].constructor.ID) {
            h.push(obj['__proto__'].constructor.ID);
            TranslationUtil._getProtoHierarchy(obj['__proto__'], h);
        }
    }

    public static isType(source: MetadataOwner, typeString: string): boolean {
        if (!source) {
            return false;
        }

        if (source.hasOwnProperty('_metadata')) {
            return source._metadata.indexOf(typeString) > -1;
        }

        return false;
    }

    public static applyVss(clientComponent: ClientComponentDto<ClientStyleDto>, vss: VssDto) {
        if (clientComponent && (clientComponent._metadata == AdvanceScreenSchema.ID ||
            clientComponent._metadata == AdvanceComponentSchema.ID ||
            clientComponent._metadata == TVHackNavigationSchema.ID ||
            clientComponent._metadata == CustomDataSchema.ID )) {
            return clientComponent;
        }

        const iterator = new ObjectIterator(clientComponent, 0, true);

        for (let item = iterator.next(); !item.done; item = iterator.next()) {

            const obj = item.value.parent;
            const key = item.value.key;

            let vssStyle = {};

            if (key === '_metadata' && TranslationUtil.classExtends(obj, ClientComponentSchema.ID)) {

                if (!obj.style) {
                    obj.style = <StyleDto>{};
                }

                const classHierarchy = TranslationUtil.getClassHierarchy(obj);

                for (let i = classHierarchy.length - 1; i >= 0; i--) {
                    const className = classHierarchy[i];

                    const rules = _.where(vss.rules, { selector: className });

                    if (!rules || rules.length <= 0) {
                        Logger.warn('no rules for ' + className);
                    } else {
                        _.each(rules, (rule: RuleDto<StyleDto>) => {

                            _L.merge(vssStyle, _.omit(rule.style, ['_metadata', 'type']));
                        });
                    }
                }

                obj.style = _L.merge(vssStyle, obj.style);

            }
        }
        return clientComponent;

    }

    public static translateAuthentication(authentication): any {
        const metadata = authentication && authentication.provider && authentication.provider._metadata;

        if (metadata) {
            switch (authentication.provider._metadata) {
                case AkamaiProvider.ID:
                    authentication.provider.type = 'akamai';
                    break;
                case AdobeProvider.ID:
                    authentication.provider.type = 'adobe';
                    break;
                default:
                    break;
            }
        }

        return authentication;
    }

    public static applyIf(object: any, config: any) {
        let property;

        if (object) {
            for (property in config) {
                if (config.hasOwnProperty(property) &&
                    (object[property] === null || object[property] === undefined)) {
                    object[property] = config[property];
                }
            }
        }

        return object;
    }
}