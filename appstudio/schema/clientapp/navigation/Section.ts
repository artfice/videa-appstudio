
/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import Container = require('../app/uiconfig/screen/component/container/Container');

class Section extends Container {

    public static ID = 'schema.clientapp.navigation.Section';

    constructor() {
        super();
    }
}

export = Section;
