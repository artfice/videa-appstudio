/**
 * Created by bardiakhosravi on 2016-03-09.
 */

import Container = require('../app/uiconfig/screen/component/container/Container');
import RelativeLayout = require('../app/uiconfig/screen/component/layout/RelativeLayout');
import Image = require('../app/uiconfig/screen/field/Image');


class SecondaryMenuSectionContainer extends Container {

	public static ID = 'schema.clientapp.navigation.SecondaryMenuSectionContainer';

	constructor() {
		super();
	}

	protected _setLayout() {
		this._addRefProperty('layout', 'Layout', RelativeLayout.ID);
	}

	protected _getComponents() {
		// TODO; we need image to have extra properties based on what layout it is being put under. Need to figure out a solution for this
		return [Image.ID];
	}

}

export = SecondaryMenuSectionContainer;
