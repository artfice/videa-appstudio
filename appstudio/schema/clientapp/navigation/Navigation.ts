/**
 * Created by bardiakhosravi on 15-11-17.
 */

///<reference path="../../../../typings/index.d.ts"/>

import DrawerMenu = require('../../common/navigation/DrawerMenu');
import Section = require('./Section');
import Menu = require('../../common/navigation/Menu');
import SecondaryMenuContainer = require('./SecondaryMenuContainer');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import BottomMenu = require('../../../schema/common/navigation/BottomMenu');

class Navigation extends BaseSchema {

    public static ID = 'schema.clientapp.navigation.Navigation';

    protected _menuTypes: string[];
	protected _menuTypeTitles: string[];
	
    constructor() {
        super();

		this._menuTypes = [];
		this._menuTypeTitles = [];
		
        this._addMenuType(DrawerMenu.ID, 'Drawer Menu');
        this._addMenuType(BottomMenu.ID, 'Bottom Menu');

		this._addOneOfRefProperty('mainMenu', 'Navigation Type', this._menuTypes, this._menuTypeTitles, Menu.ID);
        this._addArrayProperty('section', 'Sections', Section.ID);
		this._addRefProperty('secondaryMenu', 'Secondary Menu', SecondaryMenuContainer.ID);
    }

	protected _addMenuType(menuType: string, menuTypeTitle: string) {
		this._menuTypes.push(menuType);
		this._menuTypeTitles.push(menuTypeTitle);
	}
}

export = Navigation;

