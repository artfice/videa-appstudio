/**
 * Created by bardiakhosravi on 2016-03-09.
 */
///<reference path="../../../../typings/index.d.ts"/>

import Container = require('../app/uiconfig/screen/component/container/Container');
import LinearHorizontalLayout = require('../app/uiconfig/screen/component/layout/LinearHorizontalLayout');
import Label = require('../app/uiconfig/screen/field/Label');
import SecondaryMenuSectionContainer = require('./SecondaryMenuSectionContainer');

class SecondaryMenuContainer extends Container {

	public static ID = 'schema.clientapp.navigation.SecondaryMenuContainer';

	constructor() {
		super();
	}

	protected _setLayout() {
		this._addRefProperty('layout', 'Layout', LinearHorizontalLayout.ID);
	}

	protected _getComponents() {
		return [Label.ID, SecondaryMenuSectionContainer.ID];
	}

}

export = SecondaryMenuContainer;
