import AppstudioTVHackNavigation = require('../../../appstudio/app/tv/uiconfig/navigation/TVHackNavigation');

class TVHackNavigation extends AppstudioTVHackNavigation {
    public static ID = 'schema.clientapp.app.uiconfig.TVHackNavigation';

    constructor() {
        super();
    }

}

export = TVHackNavigation;