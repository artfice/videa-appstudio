import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Analytics extends BaseSchema {
    public static ID = 'schema.clientapp.app.uiconfig.Analytics';

    constructor() {
		super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
    }
}

export = Analytics;
