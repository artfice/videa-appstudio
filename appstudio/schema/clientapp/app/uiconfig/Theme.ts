import { BaseSchema } from 'videa-framework/schema/BaseSchema';

import Assets = require("./theme/Colors");
import Colors = require("./theme/Colors");

class Theme extends BaseSchema {
    public static ID = 'schema.clientapp.app.uiconfig.Theme';

    constructor() {
		super();
        this._addRefProperty('assets','Asset', Assets.ID);
        this._addRefProperty('colors','Color', Colors.ID);
    }
}

export = Theme;