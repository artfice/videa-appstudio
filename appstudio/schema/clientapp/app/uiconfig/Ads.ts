import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Ads extends BaseSchema {
    public static ID = 'schema.clientapp.app.uiconfig.Ads';

    constructor() {
        super();
        this._addPrimitiveProperty('enabled', DataTypes.BOOLEAN, 'Enabled', false);
        this._addPrimitiveProperty('type', DataTypes.STRING, 'Type', '');
        this._addPrimitiveProperty('adTagUrl', DataTypes.STRING, 'Ad Tag Url', '');
    }

}

export = Ads;