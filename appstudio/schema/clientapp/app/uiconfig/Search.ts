import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Search extends BaseSchema {
    public static ID = 'schema.clientapp.app.uiconfig.Search';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name');
        this._addPrimitiveProperty('screenId', DataTypes.STRING, 'Search Screen');
    }

}

export = Search;