import Style = require('./component/Style');
import Listener = require('../../../../common/Listener');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Component extends BaseSchema {

    public static ID = 'schema.clientapp.app.uiconfig.screen.Component';

    constructor() {
        super();

        this._addPrimitiveProperty('width', DataTypes.STRING, 'Width', '');
        this._addPrimitiveProperty('height', DataTypes.STRING, 'Height', '');
        this._addRefProperty('style', 'Style', this._getStyle());
        this._addArrayProperty('listeners', 'Listeners', Listener.ID);
        this._addPrimitiveProperty('visible', DataTypes.STRING, 'Height', '');
    }

    protected _getStyle () {
        return Style.ID;
    }

}

export = Component;
