import AppstudioScreenStyle = require('../../../../appstudio/app/configuration/uiconfig/screen/ScreenStyle');

// TODO: we cant extend appstudio schemas here. https://digiflare.atlassian.net/browse/VAS-1170
class ScreenStyle extends AppstudioScreenStyle {

    public static ID = 'schema.clientapp.app.uiconfig.screen.ScreenStyle';

    constructor() {
        super();
    }
}

export = ScreenStyle;