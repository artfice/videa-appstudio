import BaseField = require('./BaseField');
import ToggleStyle = require("./style/ToggleStyle");

class Toggle extends BaseField {

    public static ID = 'schema.clientapp.app.uiconfig.screen.field.Toggle';

    constructor() {
        super();
    }

    protected _getStyle() {
        return ToggleStyle.ID;
    }

}

export = Toggle;
