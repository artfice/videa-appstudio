import Component = require('../Component');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BaseField extends Component {
    
    public static ID = 'schema.clientapp.app.uiconfig.screen.field.BaseField';

    constructor() {
        super();
        this._addPrimitiveProperty('value', DataTypes.STRING, 'Value', '');
        this._addPrimitiveProperty('gravity', DataTypes.STRING, 'Gravity');
    }


}

export = BaseField;