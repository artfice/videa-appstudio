
import BaseField = require('./BaseField');
import { DataTypes } from 'videa-framework/schema/DataTypes';
import LabelStyle = require("./style/LabelStyle");

class TextField extends BaseField {

    public static ID = 'schema.clientapp.app.uiconfig.screen.field.TextField';

    constructor() {
        super();

        this._addPrimitiveProperty('inputType', DataTypes.STRING, 'Input Type', 'Text');
        this._addPrimitiveProperty('hint', DataTypes.STRING, 'Hints');
        this._addPrimitiveProperty('numberOfLines', DataTypes.NUMBER, 'Number of lines', 1);
    }
    
    protected _getStyle() {
        return LabelStyle.ID;
    }
}

export = TextField;
