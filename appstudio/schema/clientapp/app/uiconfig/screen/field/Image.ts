
import BaseField = require('./BaseField');
import SCALES = require('../../../../../common/enum/components/Scales');
import ASPECT_RATIOS = require('../../../../../common/enum/components/AspectRatio');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Image extends BaseField {
    
    public static ID = 'schema.clientapp.app.uiconfig.screen.field.Image';

    constructor() {
        super();

        this._addEnumProperty('aspectRatio', DataTypes.STRING, 'Aspect Ratio', ASPECT_RATIOS);
        this._addEnumProperty('scale', DataTypes.STRING, 'Scale', SCALES);
    }


}

export = Image;

