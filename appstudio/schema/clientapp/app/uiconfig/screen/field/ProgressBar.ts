import ProgressBarStyle = require('./style/ProgressBarStyle');
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Component = require('../Component');

class ProgressBar extends Component {
    public static ID = 'schema.clientapp.app.uiconfig.screen.field.ProgressBar';

    constructor() {
        super();
        this._addPrimitiveProperty('value', DataTypes.STRING, 'Value', '');
        this._addPrimitiveProperty('width', DataTypes.STRING, 'Width', '');
        this._addPrimitiveProperty('height', DataTypes.STRING, 'Height', '');
        this._addPrimitiveProperty('gravity', DataTypes.STRING, 'gravity', '');
        this._addRefProperty('style', 'Style', ProgressBarStyle.ID);
    }
}

export = ProgressBar;