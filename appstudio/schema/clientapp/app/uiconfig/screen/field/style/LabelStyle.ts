///<reference path="../../../../../../../../typings/index.d.ts"/>

import Font = require('../../../../../../common/Font');
import Style = require('../../component/Style');

class LabelStyle extends Style {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.field.style.LabelStyle';

    constructor() {
        super();
        this._addRefProperty("font", "Font", Font.ID);
    }

}

export = LabelStyle;


