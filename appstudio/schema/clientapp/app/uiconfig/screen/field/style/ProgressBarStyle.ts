import Color = require('../../../../../../common/Color');
import Style = require('../../component/Style');

class ProgressBarStyle extends Style {

    public static ID = 'schema.clientapp.app.uiconfig.screen.field.style.ProgressBarStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', Color.ID);
        this._addRefProperty('foregroundColor', 'Foreground Color', Color.ID);
    }
}

export = ProgressBarStyle;