import Style = require('../../component/Style');
import Color = require('../../../../../../common/Color');

class ToggleStyle extends Style {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.field.style.ToggleStyle';

    constructor() {
        super();
        this._addRefProperty('activatedColor', 'Activated Color', Color.ID);
        this._addRefProperty('deactivatedColor', 'Deactivated Color', Color.ID);
    }

}

export = ToggleStyle;


