
import BaseField = require('./BaseField');
import { DataTypes } from 'videa-framework/schema/DataTypes';
import LabelStyle = require("./style/LabelStyle");

class Label extends BaseField {

    public static ID = 'schema.clientapp.app.uiconfig.screen.field.Label';

    constructor() {
        super();

        this._addPrimitiveProperty('numberOfLines', DataTypes.NUMBER, 'Number of lines', 1);
    }

    protected _getStyle() {
        return LabelStyle.ID;
    }

}

export = Label;
