

/**
 * Created by bardiakhosravi on 15-11-05.
 */

///<reference path="../../../../../../../typings/index.d.ts"/>

import Component = require("../Component");
import { DataTypes } from 'videa-framework/schema/DataTypes';

class NamedComponent extends Component {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.NamedComponent';
    
    constructor() {
        super();

        this._addPrimitiveProperty('name', DataTypes.STRING, "Name", '');
    }
}

export = NamedComponent;
