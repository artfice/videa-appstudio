///<reference path="../../../../../../../typings/index.d.ts"/>

import Component = require('../../screen/Component');

import Container = require('../../screen/component/container/Container');
import Collection = require('../../screen/component/dataview/Collection');
import PickerRenderer = require('../../screen/component/picker/PickerRenderer');


class CollectionView extends Component {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.CollectionView';

    constructor() {
        super();
        
        this._addRefProperty('itemRenderer', 'Item Render', this._getItemRenderer());
        this._addRefProperty('collection', 'Collection', this._getCollection());
        this._addRefProperty('pickerRenderer', 'Picker Renderer', PickerRenderer.ID);
    }
    
    protected _getItemRenderer () : string {
        return Container.ID;
    }

    protected _getCollection () : string {
        return Collection.ID;
    }
}

export = CollectionView;
