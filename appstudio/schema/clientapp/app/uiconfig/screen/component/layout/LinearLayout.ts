import Layout = require('./Layout');

class LinearLayout extends Layout {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.layout.LinearLayout';

    constructor() {
        super();
		this._addEnumProperty('orientation', 'string', 'Direction', this._getOrientations());
    }

	protected _getOrientations() {
		return ['vertical', 'horizontal'];
	}
}

export = LinearLayout;
