import Layout = require('./Layout');

class LinearHorizontalLayout extends Layout {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.layout.LinearHorizontalLayout';

    constructor() {
        super();
    }

	protected _getOrientations() {
		return ['horizontal'];
	}
}

export = LinearHorizontalLayout;
