import Layout = require('./Layout');
import { DataTypes } from 'videa-framework/schema/DataTypes';


class GridLayout extends Layout {

    public static ID = "schema.clientapp.app.uiconfig.screen.component.layout.GridLayout";

    constructor() {
        super();

        this._addPrimitiveProperty("columns", DataTypes.NUMBER, "Number of columns", 1);
        this._addPrimitiveProperty("rows", DataTypes.NUMBER, "Number of rows", 1);
    }
}

export = GridLayout;
