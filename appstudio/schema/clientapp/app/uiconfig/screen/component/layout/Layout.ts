import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Layout extends BaseSchema {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.layout.Layout';

    constructor() {
        super();
    }
}

export = Layout;
