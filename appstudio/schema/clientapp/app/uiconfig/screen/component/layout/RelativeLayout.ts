import Layout = require('./Layout');

class RelativeLayout extends Layout {

    public static ID = "schema.clientapp.app.uiconfig.screen.component.layout.RelativeLayout";

    constructor() {
        super();
    }
}

export = RelativeLayout;