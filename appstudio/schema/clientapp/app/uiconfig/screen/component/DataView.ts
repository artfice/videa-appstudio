///<reference path="../../../../../../../typings/index.d.ts"/>

import Component = require('../../screen/Component');
import Container = require('../../screen/component/container/Container');
import Layout = require('../../screen/component/layout/Layout');
import Collection = require('../../screen/component/dataview/Collection');

class DataView extends Component {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.DataView';

    constructor() {
        super();
        
        this._addRefProperty('layout', 'Layout', this._getLayout());
        this._addRefProperty('itemRenderer', 'Item Render', this._getItemRenderer());
        this._addRefProperty('collection', 'Collection', this._getCollection());
        this._addArrayProperty('items', 'Items', Component.ID);
    }

    protected _getLayout () : string {
        return Layout.ID;
    }

    protected _getItemRenderer () : string {
        return Container.ID;
    }

    protected _getCollection () : string {
        return Collection.ID;
    }
}

export = DataView;
