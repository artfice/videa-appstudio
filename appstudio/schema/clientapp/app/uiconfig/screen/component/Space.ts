import Component = require('../Component');

class Space extends Component {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.Space';

    constructor() {
        super();
    }
}

export = Space;