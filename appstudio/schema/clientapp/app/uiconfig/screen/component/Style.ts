import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Border = require("../../../../../common/Border");

class Style extends BaseSchema {

    public static ID = "schema.clientapp.app.uiconfig.screen.component.Style";

    constructor() {
        super();

        this._addPrimitiveProperty('padding', 'string', 'Padding', '0 0 0 0');
        this._addPrimitiveProperty('margin', 'string', 'Margin', '0 0 0 0');
        this._addRefProperty('border', 'Border', Border.ID);
    }
}

export = Style;