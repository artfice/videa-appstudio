import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import OfflineCollection = require('../../../../../../common/collection/OfflineCollection');
import RemoteCollection = require('../../../../../../common/collection/RemoteCollection');
import UIDCollection = require('../../../../../../common/collection/UIDCollection');
import CustomData = require('../../../../../../common/CustomData');

class PickerRenderer extends BaseSchema {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.picker.PickerRenderer';

    constructor() {
        super();

        this._addOneOfRefProperty('collection', 'Collection Type', [OfflineCollection.ID, RemoteCollection.ID, UIDCollection.ID, CustomData.ID]);
        this._addPrimitiveProperty('type', DataTypes.STRING, 'Type', 'Dropdown');
        this._addPrimitiveProperty('displayName', DataTypes.STRING, 'Display Name', '');
    }
}

export = PickerRenderer;