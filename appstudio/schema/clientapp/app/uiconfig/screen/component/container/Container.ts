import Component = require('../../Component');
import ContainerStyle = require('./ContainerStyle');
import LinearLayout = require('../layout/LinearLayout');
import RelativeLayout = require('../layout/RelativeLayout');
import { DataTypes } from 'videa-framework/schema/DataTypes';


class Container extends Component {
    public static ID = 'schema.clientapp.app.uiconfig.screen.component.container.Container';

    constructor() {
        super();

        this._setLayout();
		this._addPrimitiveProperty('scroll', DataTypes.BOOLEAN, 'Scroll', false);
        this._addArrayAnyOfProperty('items',
                                    'Components',
                                    'Components',
                                    Component.ID,
                                    this._getComponents(),
                                    ['Component']);
    }

	protected _setLayout() {
		this._addOneOfRefProperty('layout', 'Layout', [LinearLayout.ID, RelativeLayout.ID], undefined, LinearLayout.ID);
	}

	protected _getComponents() {
		return [Component.ID];
	}

    protected _getStyle () {
        return ContainerStyle.ID;
    }

}

export = Container;
