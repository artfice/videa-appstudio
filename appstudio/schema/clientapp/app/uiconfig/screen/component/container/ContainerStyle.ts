import Color = require('../../../../../../common/Color');
import Style = require('../Style');

class ContainerStyle extends Style {

    public static ID = "schema.clientapp.app.uiconfig.screen.component.container.ContainerStyle";

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', Color.ID);
    }
}

export = ContainerStyle;