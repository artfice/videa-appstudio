import Component = require('../Component');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class WebView extends Component {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.WebView';

    constructor() {
        super();
        this._addPrimitiveProperty('uri', DataTypes.STRING, 'URI', '');
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addPrimitiveProperty('verticalScrollEnabled', DataTypes.BOOLEAN, 'Vertical Scroll', false);
        this._addPrimitiveProperty('horizontalScrollEnabled', DataTypes.BOOLEAN, 'Horizontal Scroll', false);
        this._addPrimitiveProperty('fallbackMargin', DataTypes.STRING, 'Fallback Margin', '10 15 10 15');
    }
}

export = WebView;