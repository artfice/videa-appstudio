///<reference path="../../../../../../../typings/index.d.ts"/>

import Component = require('../Component');
import Container = require('./container/Container');
import Collection = require('./dataview/Collection');

class Carousel extends Component {

    public static ID = 'schema.clientapp.app.uiconfig.screen.component.Carousel';

    constructor() {
        super();
        
        this._addRefProperty('itemRenderer', 'Item Render', this._getItemRenderer());
        this._addRefProperty('collection', 'Collection', this._getCollection());
    }
    
    protected _getItemRenderer () : string {
        return Container.ID;
    }

    protected _getCollection () : string {
        return Collection.ID;
    }
}

export = Carousel;
