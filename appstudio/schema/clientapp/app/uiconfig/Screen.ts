import Listener = require('../../../common/Listener');
import ScreenStyle = require('./screen/ScreenStyle');
import LinearLayout = require('./screen/component/layout/LinearLayout');
import Label = require('./screen/field/Label');
import Image = require('./screen/field/Image');
import Toggle = require('./screen/field/Toggle');
import TextField = require('./screen/field/TextField');
import Component = require('./screen/Component');
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Container = require('./screen/component/container/Container');
import WebView = require('./screen/component/WebView');

class Screen extends Container {
    public static ID = 'schema.clientapp.app.uiconfig.Screen';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addPrimitiveProperty('visible', DataTypes.STRING, 'Visible', 'true');
        this._addPrimitiveProperty('width', DataTypes.STRING, 'Width', 'matchParent');
        this._addPrimitiveProperty('height', DataTypes.STRING, 'Height', 'matchParent');
        this._addPrimitiveProperty('scroll', DataTypes.STRING, 'scroll', 'vertical');
        this._addRefProperty('layout','Layout', this.getLayout());
        this._addRefProperty('style','Style', this.getStyle());
		this._addArrayProperty('listeners', 'Listeners', Listener.ID);
		this._addArrayProperty('items', 'Items', Listener.ID);
        this._addArrayAnyOfProperty('item', 'Components', 'Items',Component.ID,
            [Container.ID, Label.ID, Image.ID, Toggle.ID, TextField.ID, WebView.ID],
            ['Container', 'Label', 'Image', 'Toggle', 'TextField', 'WebView']);
    }
    
    protected getStyle() {
        return ScreenStyle.ID;
    }    
    protected getLayout() {
        return LinearLayout.ID;
    }    
}

export = Screen;
