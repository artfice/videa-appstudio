import Color = require('../../../../common/Color');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Colors extends BaseSchema {
    public static ID = 'schema.clientapp.app.uiconfig.theme.Colors';

    constructor() {
        super();

        this._addRefProperty('primarySpot', 'Primary Spot Color', Color.ID);
        this._addRefProperty('secondarySpot', 'Secondary Spot Color', Color.ID);
        this._addRefProperty('accent', 'Accent Color', Color.ID);
    }

}

export = Colors;