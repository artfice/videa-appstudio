import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Image extends BaseSchema {
    public static ID = 'schema.clientapp.app.uiconfig.theme.image.Image';

    constructor() {
		super();
        this._addPrimitiveProperty('value', DataTypes.STRING, 'Value');
    }
}

export = Image;