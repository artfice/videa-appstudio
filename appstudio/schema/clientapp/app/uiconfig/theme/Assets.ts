import Image = require('./image/Image');
import ColorGalleryImage = require('../../../../appstudio/common/ColorGalleryImage');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Assets extends BaseSchema {
    public static ID = 'schema.clientapp.app.uiconfig.theme.Assets';

    constructor() {
        super();
        this._addRefProperty('logo', 'Logo', Image.ID);
        this._addRefProperty('topBar', 'Top Bar', ColorGalleryImage.ID);
    }

}

export = Assets;


