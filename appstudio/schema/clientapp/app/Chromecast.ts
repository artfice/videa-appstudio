
import ChromecastTheme = require('./chromecast/Theme');
import ChromecastTitle = require('./chromecast/Title');
import ChromecastReceiver = require('./chromecast/Receiver');

import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Chromecast extends BaseSchema {
    public static ID = 'schema.clientapp.app.Chromecast';

    constructor() {
		super();
        this._addPrimitiveProperty('enabled', DataTypes.BOOLEAN, 'Enabled');
        this._addPrimitiveProperty('appId', DataTypes.STRING, 'App ID');
        this._addRefProperty('theme','Theme', ChromecastTheme.ID);
        this._addRefProperty('title','Title', ChromecastTitle.ID);
        this._addRefProperty('reciever','Receiver', ChromecastReceiver.ID);
    }
}

export = Chromecast;