import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class User extends BaseSchema {
    public static ID = 'schema.clientapp.app.User';

    constructor() {
		super();
    }
}

export = User;