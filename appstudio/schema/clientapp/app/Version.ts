import ChromecastTheme = require('./chromecast/Theme');
import ChromecastTitle = require('./chromecast/Title');
import ChromecastReceiver = require('./chromecast/Receiver');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Version extends BaseSchema {
    public static ID = 'schema.clientapp.app.Version';

    constructor() {
		super();
        this._addPrimitiveProperty('minimumVersion', DataTypes.NUMBER, 'Minimum Version');
        this._addPrimitiveProperty('currentVersion', DataTypes.NUMBER, 'Current Version');
    }
}

export = Version;