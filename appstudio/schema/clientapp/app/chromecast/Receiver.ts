import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Roboto = require('../../../common/font/Roboto');
import OpenSans = require('../../../common/font/OpenSans');
import SanFrancisco = require('../../../common/font/SanFrancisco');
import CustomFont = require('../../../common/font/CustomFont');

class Receiver extends BaseSchema {
    public static ID = 'schema.clientapp.app.chromecast.Receiver';


    constructor() {
        super();
        this._addOneOfRefProperty('font', 'Font',
        [OpenSans.ID, CustomFont.ID , Roboto.ID, SanFrancisco.ID],
        ['Open Sans', 'Custom', 'Roboto',  'San Francisco'], OpenSans.ID);
    }

}

export = Receiver;