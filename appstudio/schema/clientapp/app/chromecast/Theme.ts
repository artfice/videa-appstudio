
import Color = require('../../../common/Color');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Theme extends BaseSchema {
    public static ID = 'schema.clientapp.app.chromecast.Theme';

    constructor() {
        super();
        this._addRefProperty('accentColor', 'Accent Color', Color.ID);
        this._addRefProperty('secondaryColor', 'Secondary Color', Color.ID);
        this._addRefProperty('backgroundColor', 'Background Color', Color.ID);
        this._addRefProperty('foregroundColor', 'Foreground Color', Color.ID);
    }

}

export = Theme;