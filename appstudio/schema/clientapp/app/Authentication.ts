

import AkamaiProvider = require('../../common/auth/AkamaiProvider');
import AdobeProvider = require('../..//common/auth/AdobeProvider');
import NoneProvider = require('../..//common/auth/NoneProvider');
import BasicAuthentication = require('../../common/auth/BasicAuthentication');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Authentication extends BaseSchema {
    public static ID = 'schema.clientapp.app.Authentication';

    constructor() {
		super();
		this._addOneOfRefProperty('provider', 'Provider', 
    [NoneProvider.ID, AkamaiProvider.ID, AdobeProvider.ID, BasicAuthentication.ID], 
    ['None', 'Akamai', 'Adobe', 'Basic Authentication']);
    }
}

export = Authentication;
