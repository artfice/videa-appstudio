import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class MpxProvider extends BaseSchema {
    public static ID = 'schema.clientapp.app.user.MpxProvider';

    constructor() {
        super();
        
        this._addPrimitiveProperty('provider', DataTypes.STRING, 'Provider', 'MPX');
        this._addPrimitiveProperty('accountId', DataTypes.STRING, 'MPX Account Identifier');
        this._addPrimitiveProperty('pid', DataTypes.STRING, 'MPX Pid');
    }
}

export = MpxProvider;