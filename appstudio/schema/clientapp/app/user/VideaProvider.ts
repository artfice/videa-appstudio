import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class VideaProvider extends BaseSchema {
    public static ID = 'schema.clientapp.app.user.VideaProvider';

    constructor() {
        super();
        
        this._addPrimitiveProperty('provider', DataTypes.STRING, 'Provider', 'Videa');
    }
}

export = VideaProvider;