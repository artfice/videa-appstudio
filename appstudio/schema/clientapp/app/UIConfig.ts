import Navigation = require('../navigation/Navigation');
import Theme = require('./uiconfig/Theme');
import Screen = require('./uiconfig/Screen');

import Analytic = require('./uiconfig/Analytics');

import GoogleAnalytic = require('../../appstudio/app/configuration/uiconfig/analytics/google/GoogleAnalytics');

import Ads = require('./uiconfig/Ads');
import Search = require('./uiconfig/Search');
import TVHackNavigation = require('./uiconfig/TVHackNavigation');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class UIConfig extends BaseSchema {
    public static ID = 'schema.clientapp.app.UIConfig';

    constructor() {
        super();
        this._addPrimitiveProperty('defaultScreenId', DataTypes.STRING, 'Default Screen Id', '');
        this._addRefProperty('theme', 'Theme', this.getTheme());
        this.addReferenceArrayOneOfProperty('screen', 'Screens', this.getScreen());
        this._addRefProperty('navigation', 'Navigation', this.getNavigation());
        this._addRefProperty('tvNavigation', 'TV Navigation', TVHackNavigation.ID);
        
        this._addRefProperty('videoAdsProvider', 'Ads', this._getAds());
        
		this._addArrayAnyOfProperty('analytics', 'Analytics', 'Provider', Analytic.ID,
			this._getAnalyticSchemas(), 
			this._getAnalyticTitles());
            
        this._addRefProperty('search', 'Search', Search.ID);

    }

    protected getNavigation() {
        return Navigation.ID;
    }
    
    protected getTheme() {
        return Theme.ID;
    }  
	
	//TODO: rename to getScreens
    protected getScreen() {
        return [Screen.ID];
    }   
    
    protected _getAnalyticSchemas() {
        return [GoogleAnalytic.ID];
    }
    
    protected _getAnalyticTitles() {
        return ['Google Analytics'];
    }         
    
    protected _getAds() {
        return Ads.ID;
    }    
}

export = UIConfig;
