import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class WatchHistory extends BaseSchema {
    public static ID = 'schema.clientapp.app.WatchHistory';

    constructor() {
		super();
        this._addEnumProperty("provider", DataTypes.STRING, "Provider",
            ['null', 'VideaMPX'], 'null');
        this._addPrimitiveProperty('completionThreshold', DataTypes.NUMBER, 'Completion Threshold', 0.95);
        this._addPrimitiveProperty('frequency', DataTypes.NUMBER, 'Frequency', 10000);
        this.addReferenceArrayOneOfProperty('events', 'Events', ['']);            

    }
}

export = WatchHistory;