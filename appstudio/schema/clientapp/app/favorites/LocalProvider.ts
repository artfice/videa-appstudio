import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class LocalProvider extends BaseSchema {
    public static ID = 'schema.clientapp.app.favorites.LocalProvider';

    constructor() {
        super();

        this._addPrimitiveProperty('type', DataTypes.STRING, 'Type', 'Local');
        this._addPrimitiveProperty('provider', DataTypes.STRING, 'Provider', 'MPX');

        this._addPrimitiveProperty('frequency', DataTypes.NUMBER, 'Frequency (ms)');
        this._addPrimitiveProperty('uid', DataTypes.STRING, 'Unique Model Identifier');
        this._addPrimitiveProperty('link', DataTypes.STRING, 'Service Endpoint');
        this._addPrimitiveProperty('collectionPath', DataTypes.STRING, 'Collection Path');
    }
}

export = LocalProvider;