import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Favorites extends BaseSchema {
    public static ID = 'schema.clientapp.app.Favorites';

    constructor() {
        super();
        
        //TODO: It must contain the properties to describe/validate the clientApp DTO
    }
}

export = Favorites;