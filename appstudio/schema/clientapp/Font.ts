/**
 * Created by bardiakhosravi on 2015-12-10.
 */
import ALIGNMENTS = require('../common/enum/fonts/Alignments');
import FONT_FAMILIES = require('../common/enum/fonts/FontFamilies');
import FONT_SIZES = require('./FontSizes');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Color = require('../common/Color');

class Font extends BaseSchema {

    public static ID = "schema.clientapp.Font";

    constructor() {
        super();
		
        this._addEnumProperty('family', DataTypes.STRING, 'Font Family', FONT_FAMILIES);
        this._addEnumProperty('size', DataTypes.STRING, 'Font Size', FONT_SIZES);
        this._addRefProperty('color', 'Font Color', Color.ID);
		this._addEnumProperty('alignment', DataTypes.STRING, 'Alignment', ALIGNMENTS, 'Left');
    }

}

export = Font;
