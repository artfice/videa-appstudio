
// TODO: https://digiflare.atlassian.net/browse/VAS-1172
import Page = require('../../appstudio/app/configuration/uiconfig/analytics/google/Page');
import Event = require('../../appstudio/app/configuration/uiconfig/analytics/google/Event');
import Analytics = require('../../appstudio/app/configuration/uiconfig/Analytics');
import GemiusAccount = require('./account/GemiusAccount');
import { DataTypes } from 'videa-framework/schema/DataTypes';


class GemiusAnalytics extends Analytics {
    public static ID = 'schema.clientapp.analytics.GemiusAnalytics';

    constructor() {
        super();
        this._addPrimitiveProperty("host", DataTypes.STRING, "Host", '');
        this._addPrimitiveProperty("name", DataTypes.STRING, "Name", '');
        this._addArrayAnyOfProperty('screens', 'Screens', 'Page Events', Page.ID, [Page.ID], ['Page']);
        this._addArrayAnyOfProperty('events', 'Analytic Events', 'Action Events', Event.ID, [Event.ID], ['Events']);
        this._addRefProperty('accountID','Account ID', GemiusAccount.ID);
    }

}

export = GemiusAnalytics;
