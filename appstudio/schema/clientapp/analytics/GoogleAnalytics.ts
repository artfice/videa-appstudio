import Page = require('../../appstudio/app/configuration/uiconfig/analytics/google/Page');
import Event = require('../../appstudio/app/configuration/uiconfig/analytics/google/Event');
import Analytics = require('../../appstudio/app/configuration/uiconfig/Analytics');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class GoogleAnalytics extends Analytics {
    public static ID = 'schema.clientapp.analytics.GoogleAnalytics';

    constructor() {
        super();
        this._addPrimitiveProperty("social", DataTypes.BOOLEAN, "Social", false);
        this._addPrimitiveProperty("exceptions", DataTypes.BOOLEAN, "Exceptions", false);
        this._addPrimitiveProperty("trackingId", DataTypes.STRING, "Tracking ID", '');
        this._addArrayAnyOfProperty('screens', 'Screens', 'Page Events', Page.ID, [Page.ID], ['Page']);
        this._addArrayAnyOfProperty('events', 'Analytic Events', 'Action Events', Event.ID, [Event.ID], ['Events']);
    }

}

export = GoogleAnalytics;
