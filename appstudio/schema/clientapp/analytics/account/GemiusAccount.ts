import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class GemiusAccount extends BaseSchema {
    public static ID = 'schema.clientapp.analytics.GemiusAccount';
    constructor() {
        super();
        this._addPrimitiveProperty("iOS", DataTypes.STRING, "iOS", '');
        this._addPrimitiveProperty("window8", DataTypes.STRING, "window8", '');
        this._addPrimitiveProperty("windowsPhone", DataTypes.STRING, "windowsPhone", '');
        this._addPrimitiveProperty("android", DataTypes.STRING, "android", '');
        this._addPrimitiveProperty("testing", DataTypes.STRING, "testing", '');
    }
}

export = GemiusAccount;
