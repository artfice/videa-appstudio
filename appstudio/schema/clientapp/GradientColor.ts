import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import OffsetColor = require('../common/OffsetColor');

class GradientColor extends BaseSchema {
    public static ID = 'schema.clientapp.GradientColor';

    constructor() {
        super();
        
        this._addArrayProperty('start', 'Start', DataTypes.ARRAY);
        this._addArrayProperty('end', 'End', DataTypes.ARRAY);
        this._addArrayProperty('stops', 'Stops', OffsetColor.ID);
    }
}

export = GradientColor;