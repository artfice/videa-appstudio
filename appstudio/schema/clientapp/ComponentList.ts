/**
 * Created by bardiakhosravi on 2015-12-12.
 */

import Container = require('./app/uiconfig/screen/component/container/Container');
import Carousel = require('./app/uiconfig/screen/component/Carousel');
import Field = require('./app/uiconfig/screen/field/Label');
import BaseField = require('./app/uiconfig/screen/field/BaseField');
import Screen = require('./app/uiconfig/Screen');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Component = require('./app/uiconfig/screen/Component');
import Image = require('./app/uiconfig/screen/field/Image');
import CollectionView = require('./app/uiconfig/screen/component/CollectionView');
import TextField = require('./app/uiconfig/screen/field/TextField');
import Toggle = require('./app/uiconfig/screen/field/Toggle');
import Dataview = require('./app/uiconfig/screen/component/DataView');

var ComponentList = [
    Container.ID, Carousel.ID, Field.ID, BaseField.ID, Screen.ID, BaseSchema.ID,
    Component.ID, Image.ID, CollectionView.ID, TextField.ID, Toggle.ID, Dataview.ID
];

export = ComponentList;