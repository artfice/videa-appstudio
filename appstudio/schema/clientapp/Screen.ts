import Container = require('./app/uiconfig/screen/component/container/Container');

class Screen extends Container {
    public static ID = 'schema.clientapp.Screen';

    constructor() {
        super();
        this._addPrimitiveProperty('name', 'string', 'Name', '');
        this._addPrimitiveProperty('title', 'string', 'Title', '');
    }
}

export = Screen;
