/**
 * Created by bardiakhosravi on 2015-12-07.
 */


import Range = require('../common/Range');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Collection extends BaseSchema {
    public static ID = 'schema.clientapp.Collection';

    constructor() {
        super();

        this._addPrimitiveProperty('link', DataTypes.STRING, 'Link', '');
		this._addRefProperty('range', 'Range', Range.ID);
		this._addPrimitiveProperty('provider', DataTypes.STRING, 'Provider', 'MPX');
		this._addPrimitiveProperty('displayField', DataTypes.STRING, 'displayField', '');
    }
}

export = Collection;
