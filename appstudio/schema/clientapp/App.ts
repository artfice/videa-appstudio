/**
 * Created by bardiakhosravi on 2016-02-01.
 */
import Cms = require('../appstudio/app/configuration/Cms');
import Mpx = require('../common/cms/Mpx');
import VideaCms = require('../common/cms/VideaCms');
import GenericJSONCms = require('../common/cms/GenericCms');

import Chromecast = require('./app/Chromecast');
import Authentication = require('./app/Authentication');
import CustomData = require('../common/CustomData');
import UIConfig = require('../appstudio/app/configuration/UIConfig');
import TrueLogic = require('../common/cms/TrueLogic');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Version = require('./app/Version');

class App extends BaseSchema {

	public static ID = 'schema.clientapp.App';

	constructor() {
		super();
		this._addArrayAnyOfProperty('cms', 'CMS', 'Providers', Cms.ID,
			this._getCmsSchemas(),
			this._getCmsTitles());
		this._addRefProperty('chromecast', 'Chromecast', Chromecast.ID);
		this._addRefProperty('custom', 'Custom', CustomData.ID);
		this._addRefProperty('authentication', 'Authentication', this._getAuthentication());
		this.addReferenceArrayOneOfProperty('uiConfig', 'Version', this._getUIConfig());
		this._addRefProperty('version', 'Version', Version.ID);
	}

    protected _getCmsSchemas() {
        return [Mpx.ID, VideaCms.ID, GenericJSONCms.ID, TrueLogic.ID];
    }

    protected _getCmsTitles() {
        return ['Mpx', 'Videa', 'Generic JSON CMS', 'TrueLogic'];
    }

	protected _getAuthentication() {
		return Authentication.ID;
	}

	protected _getUIConfig() {
		return [UIConfig.ID];
	}
}

export = App;
