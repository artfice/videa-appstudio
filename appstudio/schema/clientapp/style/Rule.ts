/**
 * Created by bardiakhosravi on 2015-12-12.
 */


import ComponentList = require('../ComponentList');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Rule extends BaseSchema {

    public static ID = 'schema.clientapp.style.Rule';

    constructor() {
        super();
        this._addEnumProperty('selector', DataTypes.STRING, 'Selector', ComponentList);
    }
}

export = Rule;
