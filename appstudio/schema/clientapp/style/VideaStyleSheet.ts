/**
 * Created by bardiakhosravi on 2015-12-12.
 */

import Rule = require('./Rule');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class VideaStyleSheet extends BaseSchema {
    
    public static ID = 'schema.clientapp.style.VideaStyleSheet';

    constructor() {
        super();
        this._addArrayProperty('rule', 'Rules', Rule.ID);
    }
}

export = VideaStyleSheet;
