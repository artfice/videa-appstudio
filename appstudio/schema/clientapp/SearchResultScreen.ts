import Container = require('./app/uiconfig/screen/component/container/Container');

class SearchResultScreen extends Container {
    public static ID = 'schema.clientapp.SearchResultScreen';

    constructor() {
        super();
        this._addPrimitiveProperty('name', 'string', 'Name', '');
    }
}

export = SearchResultScreen;
