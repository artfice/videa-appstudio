import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class MediaGallery extends BaseSchema {
    public static ID = 'schema.appstudio.MediaGallery';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addPrimitiveProperty('fullName', DataTypes.STRING, 'Full Name', '');
        this._addPrimitiveProperty('imageType', DataTypes.STRING, 'Image Type', '');
        this._addPrimitiveProperty('width', DataTypes.NUMBER, 'Width', '');
        this._addPrimitiveProperty('height', DataTypes.NUMBER, 'Height', '');
        this._addPrimitiveProperty('url', DataTypes.STRING, 'url', '');
        this._addPrimitiveProperty('bytes', DataTypes.NUMBER, 'bytes', '');
        this._addPrimitiveProperty('active', DataTypes.BOOLEAN, 'Active', '');
        this._addPrimitiveProperty('title', DataTypes.STRING, 'Title', '');
    }

    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = MediaGallery;
