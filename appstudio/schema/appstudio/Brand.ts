import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import MediaGallery = require('./MediaGallery');
import App = require('./App');
import MobileApp = require('./MobileApp');
import TabletApp = require('./TabletApp');
import TVApp = require('./TVApp');


class Brand extends BaseSchema {
    public static ID = 'schema.appstudio.Brand';

    constructor() {
        super();

        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addRefProperty('image', 'Logo', MediaGallery.ID);
        this.addReferenceArrayOneOfProperty('gallery', 'Gallery', [MediaGallery.ID]);
        this._addArrayAnyOfProperty('app', 'Apps', 'Apps', App.ID, [MobileApp.ID, TabletApp.ID, TVApp.ID], [
            'Mobile App', 'Tablet App', 'TV App']);
    }

    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = Brand;
