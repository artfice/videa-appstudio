import App = require('./App');
import TVAppEdition = require("./app/tv/TVAppEdition");
import TVBundleEdition = require("./app/tv/TVBundleEdition");
import ExternalEdition = require("./app/ExternalEdition");


class TVApp extends App {
    public static ID = 'schema.appstudio.TVApp';

    constructor() {
        super();
    }
    protected _getEditions () : string[]{
        return [TVAppEdition.ID, TVBundleEdition.ID, ExternalEdition.ID];
    }        
}

export = TVApp;
