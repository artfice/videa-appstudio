import App = require('./App');
import MobileAppEdition = require("./app/mobile/MobileAppEdition");
import MobileBundleEdition = require("./app/mobile/MobileBundleEdition");
import ExternalEdition = require("./app/ExternalEdition");


class MobileApp extends App {
    public static ID = 'schema.appstudio.MobileApp';

    constructor() {
        super();
    }
    protected _getEditions () : string[]{
        return [MobileAppEdition.ID, MobileBundleEdition.ID, ExternalEdition.ID];
    }    
}

export = MobileApp;
