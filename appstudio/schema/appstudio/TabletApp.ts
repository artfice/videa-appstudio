import App = require('./App');
import TabletAppEdition = require("./app/tablet/TabletAppEdition");
import TabletBundleEdition = require("./app/tablet/TabletBundleEdition");
import ExternalEdition = require("./app/ExternalEdition");


class TabletApp extends App {
    public static ID = 'schema.appstudio.TabletApp';

    constructor() {
        super();
    }
    protected _getEditions () : string[]{
        return [TabletAppEdition.ID, TabletBundleEdition.ID, ExternalEdition.ID];
    }            
}

export = TabletApp;
