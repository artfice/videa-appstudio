import Color = require('../../common/Color');
import GalleryImage = require('./GalleryImage');


class ColorGalleryImage extends GalleryImage {
    public static ID = 'schema.appstudio.common.ColorGalleryImage';

    constructor() {
        super();
        
        this._addRefProperty('color', 'Color', Color.ID);
    }

}

export = ColorGalleryImage;