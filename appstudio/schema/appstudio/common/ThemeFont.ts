/**
 * Created by bardiakhosravi on 2016-01-04.
 */

import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

import Color = require('../../common/Color');

import FONT_FAMILIES = require('../../common/enum/fonts/FontFamilies');

class ThemeFont extends BaseSchema {

    public static ID = 'schema.appstudio.common.ThemeFont';

    public static FONT_SIZES = ['Extra Small', 'Small', 'Medium', 'Large', 'Default'];
    public static FONT_WEIGHTS = ['Regular', 'Bold', 'Italic', 'Light', 'Thick'];

    constructor() {
        super();

        this._addRefProperty('color', 'Color', Color.ID);

        this._addEnumProperty('family', DataTypes.STRING, 'Font Family', FONT_FAMILIES, FONT_FAMILIES[0]);
        this._addEnumProperty('size', DataTypes.STRING, 'Font Size', ThemeFont.FONT_SIZES, ThemeFont.FONT_SIZES[0]);
        this._addEnumProperty('weight', DataTypes.STRING, 'Font Weight', ThemeFont.FONT_WEIGHTS, ThemeFont.FONT_WEIGHTS[0]);
    }

}

export = ThemeFont;