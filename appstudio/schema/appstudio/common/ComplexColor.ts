import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Color = require('../../common/Color');
import GradientColor = require('./GradientColor');

class ComplexColor extends BaseSchema {
    public static ID = 'schema.appstudio.common.ComplexColor';

    constructor() {

        super();

        this._addOneOfRefProperty('value', 'Complex Color', [Color.ID, GradientColor.ID], ['Solid', 'Gradient'], Color.ID);
    }
}

export = ComplexColor;