import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BaseControls extends BaseSchema {
	public static ID = 'schema.appstudio.common.BaseControls';
	
	constructor() {
		super();
		
		this._addPrimitiveProperty('visible', DataTypes.STRING, 'Visiblity', 'true');
	}
}

export = BaseControls;
