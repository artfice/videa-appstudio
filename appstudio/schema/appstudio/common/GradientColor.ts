import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Directions = require('../../common/enum/components/Directions');
import OffsetColor = require('../../common/OffsetColor');


class GradientColor extends BaseSchema {
    public static ID = 'schema.appstudio.common.GradientColor';

    constructor() {

        super();
        
        const directions = [];
        
        for (let item in Directions) {
            directions.push(item);
        }
        
        this._addEnumProperty('direction', DataTypes.STRING, 'Direction', directions, Directions[0]);
        this._addArrayProperty('stops', 'Stops', OffsetColor.ID);
    }
}

export = GradientColor;