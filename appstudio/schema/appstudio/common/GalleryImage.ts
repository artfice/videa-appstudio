import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class GalleryImage extends BaseSchema {
    public static ID = 'schema.appstudio.common.GalleryImage';

    constructor() {
        super();
        this._addPrimitiveProperty("galleryImageId", DataTypes.STRING, "Gallery Image");
    }

}

export = GalleryImage;