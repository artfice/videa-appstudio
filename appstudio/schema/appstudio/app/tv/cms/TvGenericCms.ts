import GenericCms = require("../../../../common/cms/GenericCms");

class TvGenericCms extends GenericCms {
    public static ID = 'schema.appstudio.app.tv.cms.TvGenericCms';

    constructor() {
        super();
    }

}

export = TvGenericCms;
