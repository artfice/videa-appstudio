import VideaCms = require("../../../../common/cms/VideaCms");

class TvVideaCms extends VideaCms {
    
    public static ID = 'schema.appstudio.app.tv.cms.TvVideaCms';

    constructor() {
        super();
    }

}

export = TvVideaCms;
