import Mpx = require("../../../../common/cms/Mpx");

class TvMpx extends Mpx {
    public static ID = 'schema.appstudio.app.tv.cms.TvMpx';

    constructor() {
        super();
    }

}

export = TvMpx;
