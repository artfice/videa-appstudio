import CustomCms = require("../../../../common/cms/CustomCms");

class TVCustomCms extends CustomCms {
    public static ID = 'schema.appstudio.app.tv.cms.TVCustomCms';

    constructor() {
        super();
    }
}

export = TVCustomCms;
