import VideaCms = require("../../../../common/cms/VideaCms");

class TvVimondCms extends VideaCms {
    
    public static ID = 'schema.appstudio.app.tv.cms.TvVimondCms';

    constructor() {
        super();
    }

}

export = TvVimondCms;
