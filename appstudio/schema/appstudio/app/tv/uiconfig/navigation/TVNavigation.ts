import Navigation = require('../../../configuration/uiconfig/Navigation');
import DrawerMenu = require('../../../../../common/navigation/DrawerMenu');
import TopMenu = require('../../../../../common/navigation/TopMenu');
import TVBlankMenu = require('../../../../../common/navigation/TVBlankMenu');
import AdvancedNavigation = require('./AdvancedNavigation');

class TVNavigation extends Navigation {

    public static ID = 'schema.appstudio.app.tv.uiconfig.navigation.TVNavigation';

    constructor() {
        super();
    }

    protected _getMenuType() {
        return [TVBlankMenu.ID, AdvancedNavigation.ID, TopMenu.ID];
    }

    protected _getMenuTypeTitle() {
        return ['None', 'Advanced', 'Top'];
    }

    protected _getDefaultMenu() {
        return TVBlankMenu.ID;
    }
}

export = TVNavigation;
