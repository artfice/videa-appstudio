import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import CustomData = require('../../../../../common/CustomData');

class TVHackNavigation extends BaseSchema {

    public static ID = 'schema.appstudio.app.tv.uiconfig.navigation.TVHackNavigation';

    constructor() {
        super();
        this._addRefProperty('customData', 'CustomData', CustomData.ID);
    }
}

export = TVHackNavigation;
