import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import CustomData = require('../../../../../common/CustomData');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class AdvancedNavigation extends BaseSchema {

    public static ID = 'schema.appstudio.app.tv.uiconfig.navigation.AdvancedNavigation';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', 'Advanced Navigation');
        this._addRefProperty('customData', 'Content', CustomData.ID);
    }
}

export = AdvancedNavigation;