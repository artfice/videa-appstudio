import SearchResultScreen = require('../../../configuration/uiconfig/SearchResultScreen');

class TVSearchResultScreen extends SearchResultScreen {
    public static ID = 'schema.appstudio.app.tv.uiconfig.screen.TVSearchResultScreen';

    constructor() {
        super();
    }
    
    
}

export = TVSearchResultScreen;
