import TVUIConfig = require('./TVUIConfig');

class BundleTVUIConfig extends TVUIConfig {
    public static ID = 'schema.appstudio.app.tv.uiconfig.BundleTVUIConfig';

    constructor() {
        super();
        this.addReferenceArrayProperty('uiConfigRefs', 'UI Config List', TVUIConfig.ID);
    }
}

export = BundleTVUIConfig;
