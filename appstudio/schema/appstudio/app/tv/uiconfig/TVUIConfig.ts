import UIConfig = require('../../configuration/UIConfig');
import TVNavigation = require('./navigation/TVNavigation');
import TvTheme = require('./theme/TvTheme');
import TvScreen = require('./screen/TvScreen');
import AdvanceScreen = require('../../configuration/uiconfig/AdvanceScreen');
import TVSearchResultScreen = require('./screen/TVSearchResultScreen');
import TVAds = require('./ads/TVAds');
import TvGoogleAnalytics = require('./analytics/TvGoogleAnalytics');
import CustomAnalytics = require('../../configuration/uiconfig/analytics/custom/CustomAnalytics');
import TvGemiusAnalytics = require('./analytics/TvGemiusAnalytics');

class TVUIConfig extends UIConfig {
    public static ID = 'schema.appstudio.app.tv.uiconfig.TVUIConfig';

    constructor() {
        super();
    }

    protected getNavigation() {
        return TVNavigation.ID;
    }

    protected getTheme() {
        return TvTheme.ID;
    }

    protected getScreen() {
        return [TvScreen.ID, AdvanceScreen.ID, TVSearchResultScreen.ID];
    }

    protected _getAds() {
        return TVAds.ID;
    }

    protected _getAnalyticSchemas() {
        return [TvGoogleAnalytics.ID, CustomAnalytics.ID, TvGemiusAnalytics.ID];
    }

    protected _getAnalyticTitles() {
        return ['Google Analytics', 'Custom', 'Gemius'];
    }
}

export = TVUIConfig;
