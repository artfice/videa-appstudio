import GoogleAnalytics = require('../../../configuration/uiconfig/analytics/google/GoogleAnalytics');

class TvGoogleAnalytics extends GoogleAnalytics {
    public static ID = 'schema.appstudio.app.tv.uiconfig.analytics.TvGoogleAnalytics';

    constructor() {
        super();
    }

}

export = TvGoogleAnalytics;
