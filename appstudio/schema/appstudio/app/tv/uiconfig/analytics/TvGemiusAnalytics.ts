import GemiusAnalytics = require('../../../configuration/uiconfig/analytics/gemius/GemiusAnalytics');

class TvGemiusAnalytics extends GemiusAnalytics {
    public static ID = 'schema.appstudio.app.tv.uiconfig.analytics.TvGemiusAnalytics';

    constructor() {
        super();
    }

}

export = TvGemiusAnalytics;
