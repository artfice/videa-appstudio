import Ads = require('../../../configuration/uiconfig/Ads');

class TVAds extends Ads {
    public static ID = 'schema.appstudio.app.tv.uiconfig.ads.TVAds';

    constructor() {
        super();
    }

}

export = TVAds;