import Configuration = require('../Configuration');
import TVUIConfig = require('./uiconfig/TVUIConfig');
import ExternalConfig = require('../configuration/ExternalConfig');
import TVAuthentication = require('./authentication/TVAuthentication');
import TVCustomCms = require('./cms/TVCustomCms');
import TvGenericCms = require('./cms/TvGenericCms');
import TvVideaCms = require('./cms/TvVideaCms');
import TvVimondCms = require('./cms/TvVimondCms');
import TvMpx = require('./cms/TvMpx');

class TVBundleEdition extends Configuration {
    public static ID = 'schema.appstudio.app.tv.TVBundleEdition';

    constructor() {
        super();
    }

    protected _getUIConfig() {
        return [TVUIConfig.ID, ExternalConfig.ID];
    }

    protected _getAuthentication() {
        return TVAuthentication.ID;
    }

    protected _getCmsSchemas() {
        return [TvMpx.ID, TvVideaCms.ID, TvGenericCms.ID, TVCustomCms.ID, TvVimondCms.ID];
    }

    protected _getCmsTitles() {
        return ['Mpx', 'Videa', 'Generic JSON CMS', 'Custom', 'Vimond'];
    }
}

export = TVBundleEdition;
