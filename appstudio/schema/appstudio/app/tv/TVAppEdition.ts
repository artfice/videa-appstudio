import Configuration = require('../Configuration');
import TVAuthentication = require('./authentication/TVAuthentication');
import TvMpx = require('./cms/TvMpx');
import TvVideaCms = require('./cms/TvVideaCms');
import TvGenericCms = require('./cms/TvGenericCms');
import TVCustomCms = require('./cms/TVCustomCms');
import TvVimondCms = require('./cms/TvVimondCms');
import TVUIConfig = require('./uiconfig/TVUIConfig');

class TVAppEdition extends Configuration {
    public static ID = 'schema.appstudio.app.tv.TVAppEdition';

    constructor() {
        super();
    }

    protected _getUIConfig() {
        return [TVUIConfig.ID];
    }

    protected _getAuthentication() {
        return TVAuthentication.ID;
    }

    protected _getCmsSchemas() {
        return [TvMpx.ID, TvVideaCms.ID, TvGenericCms.ID, TVCustomCms.ID, TvVimondCms.ID];
    }

    protected _getCmsTitles() {
        return ['Mpx', 'Videa', 'Generic JSON CMS', 'Custom', 'Vimond'];
    }
}

export = TVAppEdition;
