import Authentication = require("../../configuration/Authentication");


class TVAuthentication extends Authentication {
    public static ID = 'schema.appstudio.app.tv.authentication.TVAuthentication';

    constructor() {
        super();
    }

}

export = TVAuthentication;
