import VideaCms = require('../../../../common/cms/VideaCms');

class MobileVideaCms extends VideaCms {
    
    public static ID = 'schema.appstudio.app.mobile.cms.MobileVideaCms';

    constructor() {
        super();
    }

}

export = MobileVideaCms;
