import Mpx = require('../../../../common/cms/Mpx');

class MobileMpx extends Mpx {
    public static ID = 'schema.appstudio.app.mobile.cms.MobileMpx';

    constructor() {
        super();
    }

}

export = MobileMpx;
