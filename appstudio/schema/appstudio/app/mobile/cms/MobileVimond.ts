import VimondCms = require('../../../../common/cms/VimondCms');

class MobileVimond extends VimondCms {
    
    public static ID = 'schema.appstudio.app.mobile.cms.MobileVimond';

    constructor() {
        super();
    }

}

export = MobileVimond;
