import GenericCms = require('../../../../common/cms/GenericCms');

class MobileGenericCms extends GenericCms {

    public static ID = 'schema.appstudio.app.mobile.cms.MobileGenericCms';

    constructor() {
        super();
    }

}

export = MobileGenericCms;
