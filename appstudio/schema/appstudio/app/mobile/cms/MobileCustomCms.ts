import CustomCms = require('../../../../common/cms/CustomCms');

class MobileCustomCms extends CustomCms {
    
    public static ID = 'schema.appstudio.app.mobile.cms.MobileCustomCms';

    constructor() {
        super();
    }
}

export = MobileCustomCms;
