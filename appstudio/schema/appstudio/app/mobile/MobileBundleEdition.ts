import MobileUIConfig = require('../../app/mobile/uiconfig/MobileUIConfig');
import MobileMpx = require('../../app/mobile/cms/MobileMpx');
import MobileVideaCms = require('../../app/mobile/cms/MobileVideaCms');
import MobileGenericCms = require('../../app/mobile/cms/MobileGenericCms');
import ExternalConfig = require('../../app/configuration/ExternalConfig');
import MobileAuthentication = require('../../app/mobile/authentication/MobileAuthentication');
import MobileCustom = require('../../app/mobile/cms/MobileCustomCms');
import MobileVimond = require('../../app/mobile/cms/MobileVimond');
import Configuration = require('../Configuration');


class MobileBundleEdition extends Configuration {
    public static ID = 'schema.appstudio.app.mobile.MobileBundleEdition';

    constructor() {
        super();
    }

    protected _getUIConfig() {
        return [MobileUIConfig.ID, ExternalConfig.ID];
    }

    protected _getAuthentication() {
        return MobileAuthentication.ID;
    }

    protected _getCmsSchemas() {
        return [MobileMpx.ID, MobileVideaCms.ID, MobileGenericCms.ID, MobileCustom.ID, MobileVimond.ID];
    }

    protected _getCmsTitles() {
        return ['Mpx', 'Videa', 'Generic JSON CMS', 'Custom', 'Vimond'];
    }
}

export = MobileBundleEdition;
