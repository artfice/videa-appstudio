import Authentication = require('../../configuration/Authentication');

class MobileAuthentication extends Authentication {
    public static ID = 'schema.appstudio.app.mobile.authentication.MobileAuthentication';

    constructor() {
        super();
    }

}

export = MobileAuthentication;
