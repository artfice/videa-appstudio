import MobileUIConfig = require('./uiconfig/MobileUIConfig');
import MobileMpx = require('./cms/MobileMpx');
import MobileVideaCms = require('./cms/MobileVideaCms');
import MobileGenericCms = require('./cms/MobileGenericCms');
import MobileAuthentication = require('../mobile/authentication/MobileAuthentication');
import MobileCustom = require('../mobile/cms/MobileCustomCms');
import MobileVimond = require('../mobile/cms/MobileVimond');
import Configuration = require('../Configuration');

class MobileAppEdition extends Configuration {
	public static ID = 'schema.appstudio.app.mobile.MobileAppEdition';

	constructor() {
		super();
	}

	protected _getUIConfig() {
		return  [MobileUIConfig.ID];
	}
    
    protected _getAuthentication() {
		return MobileAuthentication.ID;
	}

    protected _getCmsSchemas() {
        return [MobileMpx.ID, MobileVideaCms.ID, MobileGenericCms.ID, MobileCustom.ID, MobileVimond.ID];
    }

    protected _getCmsTitles() {
        return ['Mpx', 'Videa', 'Generic JSON CMS', ' Custom', 'Vimond'];
    }
}

export = MobileAppEdition;
