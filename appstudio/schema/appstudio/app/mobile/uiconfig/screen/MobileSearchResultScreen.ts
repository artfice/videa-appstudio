import SearchResultScreen = require('../../../configuration/uiconfig/SearchResultScreen');

class MobileSearchResultScreen extends SearchResultScreen {
    public static ID = 'schema.appstudio.app.mobile.uiconfig.screen.MobileSearchResultScreen';

    constructor() {
        super();
    }
    
    
}

export = MobileSearchResultScreen;
