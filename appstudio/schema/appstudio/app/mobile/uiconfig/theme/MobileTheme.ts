import Theme = require('../../../configuration/uiconfig/Theme');

class MobileTheme extends Theme {
    public static ID = 'schema.appstudio.app.mobile.uiconfig.theme.MobileTheme';

    constructor() {
        super();
    }
    
    
}

export = MobileTheme;
