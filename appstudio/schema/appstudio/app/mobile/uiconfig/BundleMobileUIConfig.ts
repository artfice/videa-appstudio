import MobileUIConfig = require('./MobileUIConfig');

class BundleMobileUIConfig extends MobileUIConfig {
	public static ID = 'schema.appstudio.app.mobile.uiconfig.BundleMobileUIConfig';

	constructor() {
        super();
		this.addReferenceArrayProperty('uiConfigRefs', 'UI Config List', MobileUIConfig.ID);
	}
}

export = BundleMobileUIConfig;
