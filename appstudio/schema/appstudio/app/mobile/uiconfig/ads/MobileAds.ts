import Ads = require('../../../configuration/uiconfig/Ads');

class MobileAds extends Ads {
    public static ID = 'schema.appstudio.app.mobile.uiconfig.ads.MobileAds';

    constructor() {
        super();
    }

}

export = MobileAds;