import GoogleAnalytics = require('../../../configuration/uiconfig/analytics/google/GoogleAnalytics');

class MobileGoogleAnalytics extends GoogleAnalytics {
    public static ID = 'schema.appstudio.app.mobile.uiconfig.analytics.MobileGoogleAnalytics';

    constructor() {
        super();
    }

}

export = MobileGoogleAnalytics;
