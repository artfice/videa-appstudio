import GemiusAnalytics = require('../../../configuration/uiconfig/analytics/gemius/GemiusAnalytics');

class MobileGemiusAnalytics extends GemiusAnalytics {
    public static ID = 'schema.appstudio.app.mobile.uiconfig.analytics.MobileGemiusAnalytics';

    constructor() {
        super();
    }

}

export = MobileGemiusAnalytics;
