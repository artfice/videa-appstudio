/**
 * Created by bardiakhosravi on 2016-01-05.
 */

import UIConfig = require('../../configuration/UIConfig');

import MobileNavigation = require('../../mobile/uiconfig/navigation/MobileNavigation');

import MobileTheme = require('../../mobile/uiconfig/theme/MobileTheme');
import MobileScreen = require('../../mobile/uiconfig/screen/MobileScreen');

import AdvanceScreen = require('../../configuration/uiconfig/AdvanceScreen');
import MobileGoogleAnalytics = require('../../mobile/uiconfig/analytics/MobileGoogleAnalytics');
import MobileGemiusAnalytics = require('../../mobile/uiconfig/analytics/MobileGemiusAnalytics');
import MobileAds = require('../../mobile/uiconfig/ads/MobileAds');
import CustomAnalytics = require('../../configuration/uiconfig/analytics/custom/CustomAnalytics');
import MobileSearchResultScreen = require("./screen/MobileSearchResultScreen");

class MobileUIConfig extends UIConfig {
    public static ID = 'schema.appstudio.app.mobile.uiconfig.MobileUIConfig';

    constructor() {
        super();
    }

    protected getNavigation() {
        return MobileNavigation.ID;
    }

    protected getTheme() {
        return MobileTheme.ID;
    }

    protected getScreen() {
        return [MobileScreen.ID, AdvanceScreen.ID, MobileSearchResultScreen.ID];
    }

    protected _getAds() {
        return MobileAds.ID;
    }

    protected _getAnalyticSchemas() {
        return [MobileGoogleAnalytics.ID, CustomAnalytics.ID, MobileGemiusAnalytics.ID];
    }

    protected _getAnalyticTitles() {
        return ['Google Analytics', 'Custom', 'Gemius'];
    }
}

export = MobileUIConfig;
