import BaseEdition = require('./BaseEdition');
import Authentication = require('./configuration/Authentication');
import Chromecast = require('./configuration/Chromecast');

import UIConfig = require('./configuration/UIConfig');

import Cms = require('./configuration/Cms');
import Mpx = require('../../common/cms/Mpx');
import VideaCms = require('../../common/cms/VideaCms');
import GenericJSONCms = require('../../common/cms/GenericCms');
import BundleUIConfig = require('./configuration/BundleUIConfig');
import CustomData = require('../../common/CustomData');
import TrueLogic = require('../../common/cms/TrueLogic');
import AkamaiVOC = require('../../common/cms/AkamaiVOC');
import WatchHistory = require('./configuration/WatchHistory');
import ExternalConfig = require('./configuration/ExternalConfig');
import CustomCms = require('../../common/cms/CustomCms');
import VimondCms = require('../../common/cms/VimondCms');
import Favorites = require('./configuration/Favorites');
import User = require('./configuration/User');
import Settings = require('./configuration/Settings');

class Configuration extends BaseEdition {
    public static ID = 'schema.appstudio.app.Configuration';

    constructor() {
        super();
        this._addRefProperty('authentication', 'Authentication', this._getAuthentication());

        this._addArrayAnyOfProperty('cms', 'CMS', 'Providers', Cms.ID,
            this._getCmsSchemas(),
            this._getCmsTitles());
        this._addRefProperty('custom', 'Custom', CustomData.ID);
        this._addRefProperty('chromecast', 'Chromecast', Chromecast.ID);
        this._addRefProperty('watchHistory', 'Watch History', WatchHistory.ID);
        this._addRefProperty('favorites', 'Favorites', Favorites.ID);
        this._addRefProperty('user', 'User', User.ID);
        this._addRefProperty('settings', 'Settings', Settings.ID);
    }

    protected _getUIConfig() {
        return [UIConfig.ID, BundleUIConfig.ID, ExternalConfig.ID];
    }

    protected _getAuthentication() {
        return Authentication.ID;
    }

    protected _getCmsSchemas() {
        return [Mpx.ID, VideaCms.ID, GenericJSONCms.ID, TrueLogic.ID, AkamaiVOC.ID, CustomCms.ID, VimondCms.ID];
    }

    protected _getCmsTitles() {
        return ['Mpx', 'Videa', 'Generic JSON CMS', 'True Logic', 'AkamaiVOC', 'Custom', 'Vimond'];
    }
}

export = Configuration;
