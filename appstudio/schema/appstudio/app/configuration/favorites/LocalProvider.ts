import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class LocalProvider extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.favorites.LocalProvider';

    constructor() {
        super();

        this._addPrimitiveProperty('frequency', DataTypes.NUMBER, 'Frequency (ms)', 60000, 
            'How often the video player will update the watch history');
        
        this._addPrimitiveProperty('uid', DataTypes.STRING, 'Unique Model Identifier', '');
        
        this._addPrimitiveProperty('link', DataTypes.STRING, 'Service Endpoint', '', 
            'Remote HTTP Request to fetch content models for favorites');
        
        this._addPrimitiveProperty('collectionPath', DataTypes.STRING, 'Collection Path', '', 
            'Path within the collection where list of content models reside');
    }
}

export = LocalProvider;