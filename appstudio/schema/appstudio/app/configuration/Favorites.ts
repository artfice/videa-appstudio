import { BaseSchema } from 'videa-framework/schema/BaseSchema';

import LocalProvider = require('./favorites/LocalProvider');
import NoData = require('../../../../schema/common/NoData');
import CustomData = require('../../../../schema/common/CustomData');

class Favorites extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.Favorites';

    constructor() {
        super();

        this._addOneOfRefProperty('provider', 'Provider',
            [NoData.ID, LocalProvider.ID, CustomData.ID],
            ['None', 'Local', 'Custom']);
    }
}

export = Favorites;