import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import ChromecastTheme = require('./chromecast/Theme');
import ChromecastTitle = require('./chromecast/Title');
import ChromecastReceiver = require('./chromecast/Receiver');


class Chromecast extends BaseSchema {
    
    public static ID = 'schema.appstudio.app.configuration.Chromecast';

    constructor() {
		super();
        this._addPrimitiveProperty('enabled', DataTypes.BOOLEAN, 'Enabled');
        this._addPrimitiveProperty('appId', DataTypes.STRING, 'App ID');
        this._addRefProperty('theme','Theme', ChromecastTheme.ID);
        this._addRefProperty('title','Title', ChromecastTitle.ID);
        this._addRefProperty('receiver','Receiver', ChromecastReceiver.ID);
    }
}

export = Chromecast;