/**
 * Created by bardiakhosravi on 2015-12-01.
 */
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Theme = require('./uiconfig/Theme');
import Screen = require('./uiconfig/Screen');
import AdvanceScreen = require('./uiconfig/AdvanceScreen');
import Analytic = require('./uiconfig/Analytics');
import GoogleAnalytic = require('./uiconfig/analytics/google/GoogleAnalytics');
import Ads = require('./uiconfig/Ads');
import UISettings = require('./uiconfig/UISettings');
import SearchSettings = require('./uiconfig/SearchSettings');
import SearchResultScreen = require('./uiconfig/SearchResultScreen');
import CustomData = require('../../../common/CustomData');
import Navigation = require('./uiconfig/Navigation');


class UIConfig extends BaseSchema {
    
    public static ID = 'schema.appstudio.app.configuration.UIConfig';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this.addReferenceArrayOneOfProperty('screen', 'Screens', this.getScreen());
        this._addRefProperty('theme', 'Theme', this.getTheme());
        this._addRefProperty('navigation', 'Navigation', this.getNavigation());
        
        this._addRefProperty('ads', 'Ads', this._getAds());
        
		this._addArrayAnyOfProperty('analytics', 'Analytics', 'Provider', Analytic.ID,
			this._getAnalyticSchemas(), 
			this._getAnalyticTitles());
            
        this._addRefProperty('searchSettings', 'Search', SearchSettings.ID);
        this._addRefProperty('uiSettings', 'Settings', UISettings.ID);

    }

    protected getNavigation() {
        return Navigation.ID;
    }
    
    protected getTheme() {
        return Theme.ID;
    }  
	
	//TODO: rename to getScreens
    protected getScreen() {
        return [Screen.ID, AdvanceScreen.ID, SearchResultScreen.ID];
    }   
    
    protected _getAnalyticSchemas() {
        return [GoogleAnalytic.ID, CustomData.ID];
    }
    
    protected _getAnalyticTitles() {
        return ['Google Analytics', 'Custom'];
    }         
    
    protected _getAds() {
        return Ads.ID;
    }    

    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = UIConfig;
