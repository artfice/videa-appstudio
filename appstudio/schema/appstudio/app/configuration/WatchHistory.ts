import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import NoneProvider = require('./uiconfig/watchhistory/NoneProvider');
import LocalProvider = require('./uiconfig/watchhistory/LocalProvider');
import MPXProvider = require('./uiconfig/watchhistory/MpxProvider');


class WatchHistory extends BaseSchema {
    
    public static ID = 'schema.appstudio.app.configuration.WatchHistory';

    constructor() {
		super();
		this._addOneOfRefProperty('provider', 'Provider', 
        [NoneProvider.ID, LocalProvider.ID, MPXProvider.ID], 
        ['None', 'Local', 'MPX']);
    }
}

export = WatchHistory;