import { BaseSchema } from 'videa-framework/schema/BaseSchema';

import MpxProvider = require('./user/MpxProvider');
import VideaProvider = require('./user/VideaProvider');
import CustomData = require('../../../common/CustomData');
import NoData = require('../../../common/NoData');

class User extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.User';

    constructor() {
        super();

        this._addOneOfRefProperty('provider', 'Provider',
            [NoData.ID, MpxProvider.ID, VideaProvider.ID, CustomData.ID],
            ['None', 'MPX', 'Videa', 'Custom']);
    }
}

export = User;