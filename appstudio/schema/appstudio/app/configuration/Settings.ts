import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Settings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.Settings';

    constructor() {
		super();
        this._addPrimitiveProperty('minimumVersion', DataTypes.NUMBER, 'Minimum Version');
        this._addPrimitiveProperty('currentVersion', DataTypes.NUMBER, 'Current Version');
    }
}

export = Settings;