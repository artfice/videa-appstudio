import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Font = require('../../../../common/Font');

class Title extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.chromecast.Title';

    constructor() {
        super();
        this._addRefProperty('font', 'Font', Font.ID);
    }

}

export = Title;