import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Font = require('../../../../common/Font');

class Receiver extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.chromecast.Receiver';


    constructor() {
        super();
        this._addRefProperty('font', 'Font', Font.ID);
    }

}

export = Receiver;