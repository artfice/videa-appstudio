import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import BaseReference = require('../../../../../common/BaseReference');

class WelcomeScreen extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.settings.WelcomeScreen';

    constructor() {
        super();
        this._addPrimitiveProperty('enabled', DataTypes.BOOLEAN, 'Welcome Screen Enabled');
        this._addDynamicRefProperty('screen', 'Welcome Screen', BaseReference.ID, 
        'api/v1/dynamicEnum/accounts/{accountId}/uiConfigs/{configId}/screens?type=regular');
        this._addPrimitiveProperty('persistKey', DataTypes.STRING, 'persist key', new Date().getTime());
    }

}

export = WelcomeScreen;