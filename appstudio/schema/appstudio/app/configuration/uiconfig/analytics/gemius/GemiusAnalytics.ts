import Page = require('../google/Page');
import Event = require('../google/Event');
import Analytics = require('../../Analytics');
import { DataTypes } from 'videa-framework/schema/DataTypes';


class GemiusAnalytics extends Analytics {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.gemius.GemiusAnalytics';

    constructor() {
        super();
        this._addPrimitiveProperty('host', DataTypes.STRING, 'Host', '');
        this._addPrimitiveProperty('iOS', DataTypes.STRING, 'IOS Account ID', '');
        this._addPrimitiveProperty('window8', DataTypes.STRING, 'Window 8 Account ID', '');
        this._addPrimitiveProperty('windowsPhone', DataTypes.STRING, 'Windows Phone Account ID', '');
        this._addPrimitiveProperty('android', DataTypes.STRING, 'Android Account ID', '');
        this._addPrimitiveProperty('testing', DataTypes.STRING, 'Testing Account ID', '');

        this._addArrayAnyOfProperty('screens', 'Screens', 'Page Events', Page.ID, [Page.ID], ['Page']);
        this._addArrayAnyOfProperty('events', 'Analytic Events', 'Action Events', Event.ID, [Event.ID], ['Events']);
    }

}

export = GemiusAnalytics;
