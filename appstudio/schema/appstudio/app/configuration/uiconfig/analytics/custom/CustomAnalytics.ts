import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomAnalytics extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.custom.CustomAnalytics';

    constructor() {
		super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addPrimitiveProperty('content', DataTypes.STRING, '', '{}');
    }
    
    protected _getDefaultDisplayField(): string {
        return 'name';
    }         
}

export = CustomAnalytics;
