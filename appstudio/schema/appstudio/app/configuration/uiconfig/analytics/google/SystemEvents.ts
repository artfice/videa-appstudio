import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Event = require('./systemEvents/Event'); 
import VideoPlaybackEvent = require('./systemEvents/VideoPlaybackEvent'); 

class SystemEvents extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.SystemEvents';

    constructor() {
        super();
        this._addRefProperty('signInSuccess', 'Sign In Success', Event.ID);   
        this._addRefProperty('signInFailure', 'Sign In Failure', Event.ID);   
        this._addRefProperty('search', 'Search', Event.ID);   
        this._addRefProperty('chromecastConnect', 'Chromecast Connect', Event.ID);   
        this._addRefProperty('chromecastDisconnect', 'Chromecast Disconnect', Event.ID);   
        this._addArrayAnyOfProperty('videoPlayback', 'Video Playback Event', 'Playback Event', 
        VideoPlaybackEvent.ID, [VideoPlaybackEvent.ID], ['Playback Event']);
    }
}

export = SystemEvents;