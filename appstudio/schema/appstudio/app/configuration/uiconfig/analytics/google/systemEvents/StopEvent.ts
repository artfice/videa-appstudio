import EnabledEvent = require('./EnabledEvent');

class StopEvent extends EnabledEvent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.systemEvents.StopEvent';

    constructor() {
        super();
    }
}

export = StopEvent;