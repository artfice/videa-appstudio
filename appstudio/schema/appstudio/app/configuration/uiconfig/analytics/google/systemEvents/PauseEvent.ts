import EnabledEvent = require('./EnabledEvent');

class PauseEvent extends EnabledEvent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.systemEvents.PauseEvent';

    constructor() {
        super();
    }
}

export = PauseEvent;