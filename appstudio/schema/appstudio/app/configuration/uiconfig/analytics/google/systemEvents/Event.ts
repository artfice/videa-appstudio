import CustomData = require('../../../../../../../common/CustomData');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Event extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.systemEvents.Event';

    constructor() {
        super();
        this._addPrimitiveProperty('enabled', DataTypes.BOOLEAN, 'Enabled', false);
        this._addPrimitiveProperty('category', DataTypes.STRING, 'Category', '');
        this._addPrimitiveProperty('action', DataTypes.STRING, 'Action', '');
        this._addPrimitiveProperty('labels', DataTypes.STRING, 'Labels', '');
        this._addPrimitiveProperty('value', DataTypes.STRING, 'Value', '');     
        this._addArrayAnyOfProperty('custom', 'Custom', 'Custom', CustomData.ID, [CustomData.ID], ['Custom']);
    }
}

export = Event;