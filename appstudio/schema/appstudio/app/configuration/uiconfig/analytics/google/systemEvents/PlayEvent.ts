import EnabledEvent = require('./EnabledEvent');

class PlayEvent extends EnabledEvent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.systemEvents.PlayEvent';

    constructor() {
        super();
    }
}

export = PlayEvent;