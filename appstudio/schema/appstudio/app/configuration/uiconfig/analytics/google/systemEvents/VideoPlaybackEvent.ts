import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import CustomData = require('../../../../../../../common/CustomData');
import EnabledEvent = require('./EnabledEvent');
import PlayEvent = require('./PlayEvent');
import PauseEvent = require('./PauseEvent');
import ResumeEvent = require('./ResumeEvent');
import StopEvent = require('./StopEvent');
import PercentageEvent = require('./PercentageEvent');

class VideoPlaybackEvent extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.systemEvents.VideoPlaybackEvent';

    constructor() {
        super();
        this._addOneOfRefProperty('event', 'Event Type', this._getEvents(), 
        this._getEventTitles(), EnabledEvent.ID, undefined);
    }

    protected _getEvents () : string[] {
        return [PercentageEvent.ID, PlayEvent.ID, PauseEvent.ID, ResumeEvent.ID, StopEvent.ID];
    }

    protected _getEventTitles () : string[] {
        return ['Percentage', 'Play', 'Pause', 'Resume', 'Stop'];
    }
}

export = VideoPlaybackEvent;