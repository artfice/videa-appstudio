import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import CustomData = require('../../../../../../common/CustomData');

class Event extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.Event';

    constructor() {

        super();
        this._addPrimitiveProperty('videaEventTag', DataTypes.STRING, 'Tag Name', '');
        this._addPrimitiveProperty('category', DataTypes.STRING, 'Category', '');
        this._addPrimitiveProperty('action', DataTypes.STRING, 'Action', '');
        this._addPrimitiveProperty('labels', DataTypes.STRING, 'Labels', '{model.series_name} - {model.title} | EP {model.episode}');
        this._addPrimitiveProperty('value', DataTypes.STRING, 'Value', '');     
        this._addRefProperty('custom', 'Custom', CustomData.ID);   
    }
}

export = Event;