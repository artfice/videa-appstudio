import EnabledEvent = require('./EnabledEvent');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class PercentageEvent extends EnabledEvent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.systemEvents.PercentageEvent';

    constructor() {
        super();
        this._addPrimitiveProperty('percentage', DataTypes.NUMBER, 'Percentage');
    }
}

export = PercentageEvent;