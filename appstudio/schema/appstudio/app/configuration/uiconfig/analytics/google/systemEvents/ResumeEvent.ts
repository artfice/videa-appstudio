import EnabledEvent = require('./EnabledEvent');

class ResumeEvent extends EnabledEvent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.systemEvents.ResumeEvent';

    constructor() {
        super();
    }
}

export = ResumeEvent;