import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Page extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.Page';

    constructor() {

        super();
        this._addPrimitiveProperty('screenId', DataTypes.STRING, 'Screen', '');
        this._addPrimitiveProperty('screenName', DataTypes.STRING, 'Screen Name', '');
    }
}

export = Page;
