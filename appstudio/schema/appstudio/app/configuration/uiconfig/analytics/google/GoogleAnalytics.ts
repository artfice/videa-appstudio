import Page = require('./Page');
import Event = require('./Event');
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Analytics = require('../../Analytics');
import SystemEvents = require('./SystemEvents');

class GoogleAnalytics extends Analytics {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.analytics.google.GoogleAnalytics';

    constructor() {
        super();
        this._addPrimitiveProperty('trackingId', DataTypes.STRING, 'Tracking ID', '');
        this._addArrayAnyOfProperty('screens', 'Screens', 'Screen View Events', Page.ID, [Page.ID], ['Page']);
        this._addArrayAnyOfProperty('events', 'Analytic Events', 'Action Events', Event.ID, [Event.ID], ['Events']);
        this._addRefProperty('systemEvents', 'System Events', SystemEvents.ID);
    }

}

export = GoogleAnalytics;
