import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import ColoredComponentStyle = require("../../../screen/component/style/ColoredComponentStyle");

class ImageSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.section.image.ImageSettings';

    constructor() {
        super();
        this._addRefProperty('style', 'Style', ColoredComponentStyle.ID);
    }
}

export = ImageSettings;