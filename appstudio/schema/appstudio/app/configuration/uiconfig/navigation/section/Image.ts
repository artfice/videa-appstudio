import ImageComponent = require('../../screen/component/field/image/Image');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import ImageSettings = require('./image/ImageSettings');

class Image extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.section.Image';

    constructor() {
        super();
         this._addRefProperty('settings', 'Settings', ImageSettings.ID);
         this._addArrayAnyOfProperty('image', 'Image', 'Image', ImageComponent.ID, [ImageComponent.ID], ['Image']);
    }
}

export = Image;