import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import SectionStyle = require('./style/SectionStyle');
import ComponentControls = require('../../screen/component/ComponentControls');

class SectionSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.section.SectionSettings';

    constructor() {
        super();
        this._addRefProperty('style', 'Style', SectionStyle.ID);
        this._addRefProperty('controls', 'Controls', ComponentControls.ID);
    }
}

export = SectionSettings;