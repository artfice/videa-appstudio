import ComplexColor = require('../../../../../../../appstudio/common/ComplexColor');
import SizedComponentStyle = require('../../../screen/component/style/SizedComponentStyle');

class SectionStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.section.style.SectionStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
    }
}

export = SectionStyle;