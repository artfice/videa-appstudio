import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import LabelSettings = require('./label/LabelSettings');
import LabelComponent = require('../../screen/component/field/label/Label');

// TODO: https://digiflare.atlassian.net/browse/VAS-1166
class Label extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.section.Label';

    constructor() {
        super();
         this._addRefProperty('settings', 'Settings', LabelSettings.ID);
        this._addArrayAnyOfProperty('text', 'Text', 'Text', LabelComponent.ID, [Label.ID], ['Label']);
    }
}

export = Label;