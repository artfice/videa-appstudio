/**
 * Created by bardiakhosravi on 2016-03-02.
 */

import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import SecondaryMenuSettings = require('./secondarymenu/SecondaryMenuSettings');
import SecondaryMenuSection = require('./secondarymenu/SecondaryMenuSection');
import LabelComponent = require('../screen/component/field/label/Label');


class SecondaryMenu extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.SecondaryMenu';

    constructor() {
        super();
        this._addRefProperty('settings', 'Settings', SecondaryMenuSettings.ID);
        this._addArrayAnyOfProperty('text', 'Heading', 'Text', LabelComponent.ID, [LabelComponent.ID], ['Label']);
        this._addArrayProperty('sections', 'Sections', SecondaryMenuSection.ID);
    }
}

export = SecondaryMenu;
