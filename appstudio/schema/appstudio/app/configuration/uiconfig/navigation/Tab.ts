import Label = require('../screen/component/field/label/Label');
import Image = require('../screen/component/field/image/Image');
import Component = require('../screen/component/Component');

class Tab extends Component {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.Tab';

    constructor() {
        super();
        this._addRefProperty('label', 'Label', Label.ID);
        this._addRefProperty('icon', 'Icon', Image.ID);
    }
}

export = Tab;