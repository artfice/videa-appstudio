import Component = require('../../screen/component/Component');
import NamedComponent = require('../../screen/component/NamedComponent');

import Image = require('../../screen/component/field/image/Image');
import Label = require('../../screen/component/field/label/Label');
import SizedComponentStyle = require('../../screen/component/style/SizedComponentStyle');
import LayerControls = require('../../screen/component/LayerControls');

class TopMenuLayer extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.content.TopMenuLayer';

    constructor() {
        super();
        this._addArrayAnyOfProperty('subComponent', 'Components', 'SubComponents', Component.ID,
            [Image.ID, Label.ID, TopMenuLayer.ID],
            [ 'Image', 'Label', 'Layer']);
    }

    protected _getStyle () {
        return SizedComponentStyle.ID;
    }

    protected _getControls() {
	    return LayerControls.ID;
    }
}

export = TopMenuLayer;