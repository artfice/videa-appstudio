import Component = require('../../screen/component/Component');

import Image = require('../../screen/component/field/image/Image');
import Label = require('../../screen/component/field/label/Label');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import TopMenuLayer = require('./TopMenuLayer');

class TopMenuContent extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.content.TopMenuContent';

    constructor() {
        super();
        this._addArrayAnyOfProperty('subComponent', 'Components', 'SubComponents', Component.ID,
            [Image.ID, Label.ID, TopMenuLayer.ID],
            ['Image', 'Label', 'Layer']);
    }
}

export = TopMenuContent;