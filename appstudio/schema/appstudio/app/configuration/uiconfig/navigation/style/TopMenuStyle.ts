import SizedComponentStyle = require('../../screen/component/style/SizedComponentStyle');
import ComplexColor = require('../../../../../common/ComplexColor');

class TopMenuStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.style.TopMenuStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
    }
}

export = TopMenuStyle;