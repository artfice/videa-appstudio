import ColoredComponentStyle = require('../../screen/component/style/ColoredComponentStyle');
import Color = require('../../../../../../common/Color');
import Roboto = require('../../../../../../common/font/Roboto');
import OpenSans = require('../../../../../../common/font/OpenSans');
import SanFrancisco = require('../../../../../../common/font/SanFrancisco');
import CustomFont = require('../../../../../../common/font/CustomFont');

class BottomMenuStyle extends ColoredComponentStyle {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.style.BottomMenuStyle';

    constructor() {

        super();
        this._addRefProperty('selectedColor', 'Selected Color', Color.ID);
        this._addOneOfRefProperty('font', 'Font',
        [OpenSans.ID, CustomFont.ID , Roboto.ID, SanFrancisco.ID],
        ['Open Sans', 'Custom', 'Roboto',  'San Francisco'], OpenSans.ID);
    }
}

export = BottomMenuStyle;