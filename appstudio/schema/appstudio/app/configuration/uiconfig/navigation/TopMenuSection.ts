import TopMenuContent = require('./content/TopMenuContent');

import Component = require('../screen/component/Component');
import TopMenuStyle = require('./style/TopMenuStyle');

class TopMenuSection extends Component {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.TopMenuSection';

    constructor() {
        super();
        this._addRefProperty('content', 'Content', TopMenuContent.ID);
    }

    protected _getStyle () {
        return TopMenuStyle.ID;
    }
}

export = TopMenuSection;