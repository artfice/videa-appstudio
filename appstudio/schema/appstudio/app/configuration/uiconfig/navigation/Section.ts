/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import SectionSettings = require('./section/SectionSettings');

import Label = require('./section/Label');
import Image = require('./section/Image');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Section extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.Section';

    constructor() {
        super();
        this._addRefProperty('settings', 'Settings', SectionSettings.ID);
        this._addRefProperty('label', 'Label', Label.ID);
        this._addRefProperty('image', 'Image', Image.ID);
    }
}

export = Section;
