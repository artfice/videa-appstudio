import ImageComponent = require('../../screen/component/field/image/Image');

import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import SecondaryMenuSectionSettings = require('./section/SecondaryMenuSectionSettings');

// TODO: https://digiflare.atlassian.net/browse/VAS-1167
class SecondaryMenuSection extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.secondarymenu.SecondaryMenuSection';

    constructor() {
        super();
        this._addRefProperty('settings', 'Section Settings', SecondaryMenuSectionSettings.ID);
        this._addArrayAnyOfProperty('image', 'Images', 'image', ImageComponent.ID, [ImageComponent.ID], ['Image']);
    }
}

export = SecondaryMenuSection;
