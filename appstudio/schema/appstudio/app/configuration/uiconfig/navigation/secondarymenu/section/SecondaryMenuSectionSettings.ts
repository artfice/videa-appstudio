import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import ColoredComponentStyle = require('../../../screen/component/style/ColoredComponentStyle');

class SecondaryMenuSectionSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.secondarymenu.section.SecondaryMenuSectionSettings';

    constructor() {
        super();
        this._addRefProperty('style', 'Style', ColoredComponentStyle.ID);
    }
}

export = SecondaryMenuSectionSettings;