import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import ComponentControls = require('../../screen/component/ComponentControls');
import SecondaryMenuStyle = require('./style/SecondaryMenuStyle');

class SecondaryMenuSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.secondarymenu.SecondaryMenuSettings';
    
    constructor() {
        super();
        this._addRefProperty('style', 'Style', SecondaryMenuStyle.ID);
        this._addRefProperty('controls', 'Controls', ComponentControls.ID);
    }
}

export = SecondaryMenuSettings;