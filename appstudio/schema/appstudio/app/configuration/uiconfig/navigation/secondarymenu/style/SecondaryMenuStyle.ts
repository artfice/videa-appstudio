import SizedComponentStyle = require('../../../screen/component/style/SizedComponentStyle');
import ComplexColor = require('../../../../../../../appstudio/common/ComplexColor');

class SecondaryMenuStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.navigation.secondarymenu.style.SecondaryMenuStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
    }
}

export = SecondaryMenuStyle;