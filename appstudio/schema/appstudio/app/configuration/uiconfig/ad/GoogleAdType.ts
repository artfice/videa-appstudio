import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class GoogleAdType extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.ad.GoogleAdType';
}

export = GoogleAdType;