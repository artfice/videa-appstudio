import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomAdType extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.ad.CustomAdType';

    constructor() {
        super();
        this._addPrimitiveProperty('custom', DataTypes.STRING, 'Custom Value', '');
    }
}

export = CustomAdType; 