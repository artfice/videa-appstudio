import Screen = require('./Screen');

class SearchResultScreen extends Screen {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.SearchResultScreen';

    constructor() {
        super();
    }
}

export = SearchResultScreen;