import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import WelcomeScreen = require('./settings/WelcomeScreen');

class UISettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.UISettings';

    constructor() {
        super();
        this._addPrimitiveProperty('screenId', DataTypes.STRING, 'Default Screen');
        this._addRefProperty('welcomeScreen', 'Welcome Screen', WelcomeScreen.ID);
    }

}

export = UISettings;