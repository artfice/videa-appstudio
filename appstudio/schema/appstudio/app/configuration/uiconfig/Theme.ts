import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Assets = require('./theme/Assets');
import Colors = require('../../../../clientapp/app/uiconfig/theme/Colors');
// import Fonts = require('./theme/Fonts');

class Theme extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.Theme';
    
    constructor() {
        super();

        this._addRefProperty('assets', 'assets', Assets.ID);
        this._addRefProperty('colors', 'colors', Colors.ID);
        // this._addRefProperty('fonts', 'fonts', Fonts.ID);
    }

}

export = Theme;