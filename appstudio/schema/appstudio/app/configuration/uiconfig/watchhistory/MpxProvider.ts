import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class MpxProvider extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.watchhistory.MpxProvider';

    constructor() {
        super();
        this._addPrimitiveProperty('completionThreshold', DataTypes.NUMBER, 'Completion Threshold', 0.95);
        this._addPrimitiveProperty('frequency', DataTypes.NUMBER, 'Frequency', 10000);
        this._addPrimitiveProperty('host', DataTypes.STRING, 'Host');
        this._addPrimitiveProperty('refreshInterval', DataTypes.NUMBER, 'Refresh Interval', 300000);
        this._addPrimitiveProperty('onPauseEvent', DataTypes.BOOLEAN, 'On Pause Event', false);
        this._addPrimitiveProperty('onResumeEvent', DataTypes.BOOLEAN, 'On Resume Event', false);
        this._addPrimitiveProperty('onSeekEvent', DataTypes.BOOLEAN, 'On Seek Event', false);
        this._addPrimitiveProperty('onStopEvent', DataTypes.BOOLEAN, 'On Stop Event', false);
        this._addPrimitiveProperty('onPlayEvent', DataTypes.BOOLEAN, 'On Play Event', false);        
    }

}

export = MpxProvider;