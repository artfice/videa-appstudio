import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class NoneProvider extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.watchhistory.NoneProvider';

    constructor() {
        super();
    }

}

export = NoneProvider;