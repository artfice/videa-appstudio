import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import CustomData = require('../../../../common/CustomData');

class AdvanceScreen extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.AdvanceScreen';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addRefProperty("customData", "CustomData", CustomData.ID);
    }

    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = AdvanceScreen;