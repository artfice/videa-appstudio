import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Menu = require('../../../../common/navigation/Menu');
import DrawerMenu = require('../../../../common/navigation/DrawerMenu');
import BottomMenu = require('../../../../common/navigation/BottomMenu');

class Navigation extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.Navigation';

    protected _menuTypes: string[];
	protected _menuTypeTitles: string[];

    constructor() {
        super();

        this._addOneOfRefProperty('mainMenu', 'Navigation Type', this._getMenuType(), this._getMenuTypeTitle(), Menu.ID, this._getDefaultMenu());
    }

    protected _getMenuType() {
        return [DrawerMenu.ID, BottomMenu.ID];
    }

    protected _getMenuTypeTitle() {
        return ['Drawer Menu', 'Tab Menu'];
    }

    protected _getDefaultMenu() {
        return undefined;
    }
}

export = Navigation;

