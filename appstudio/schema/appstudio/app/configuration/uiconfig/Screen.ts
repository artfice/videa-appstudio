import NamedComponent = require('./screen/component/NamedComponent');
import Component = require('./screen/component/Component');
import ListView = require('./screen/component/dataview/gridview/listview/ListView');
import Carousel = require('./screen/component/dataview/carousel/Carousel');
import GridView = require('./screen/component/dataview/gridview/GridView');
import Swimlane = require('./screen/component/dataview/gridview/swimlane/Swimlane');
import Image = require('./screen/component/field/image/Image');
import Label = require('./screen/component/field/label/Label');
import CollectionView = require('./screen/component/collectionview/CollectionsView');
import Toggle = require('./screen/component/field/toggle/Toggle');
import TextField = require('./screen/component/field/textfield/TextField');
import AdvanceComponent = require('./screen/component/AdvanceComponent');
import ScreenStyle = require('./screen/ScreenStyle');
import ScreenContent = require('./screen/ScreenContent');
import Layer = require('./screen/component/Layer');
import WebView = require('./screen/component/webview/WebView');

class Screen extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.Screen';

    constructor() {
        super();
        
        this._addArrayAnyOfProperty('item', 'Components', 'Screens', Component.ID,
            [ListView.ID, Carousel.ID, GridView.ID, Swimlane.ID, Image.ID, Label.ID, CollectionView.ID, Toggle.ID, TextField.ID, AdvanceComponent.ID, WebView.ID, Layer.ID],
            ['List View', 'Carousel', 'Grid View', 'Swim Lane', 'Image', 'Label', 'Collection View', 'Toggle', 'Text Field', 'Advanced Component', 'Web View', 'Layer']);

        this._addRefProperty('content', 'Content', ScreenContent.ID);
    }
    
    protected _getStyle () {
        return ScreenStyle.ID;
    }
        
    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = Screen;