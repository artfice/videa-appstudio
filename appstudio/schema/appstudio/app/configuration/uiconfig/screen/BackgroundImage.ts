import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BackgroundImage extends BaseSchema {
    
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.BackgroundImage';

    constructor() {

        super();
        this._addPrimitiveProperty('value', DataTypes.STRING, 'Value', '{model.thumbnails.Default-2208x1242.url}');
    }
}

export = BackgroundImage;
