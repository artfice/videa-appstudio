import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Menu = require('./navigation/Menu');
import TopBar = require('./navigation/TopBar');

class NavigationStyle extends BaseSchema {
    
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.NavigationStyle';

    constructor() {

        super();
        this._addRefProperty('menu', 'Menu', Menu.ID);
        this._addRefProperty('topBar', 'Top Bar', TopBar.ID);
    }
}

export = NavigationStyle;
