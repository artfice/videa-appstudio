import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Menu extends BaseSchema {
    
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.navigation.Menu';

    constructor() {

        super();
        this._addPrimitiveProperty('visible', DataTypes.STRING, 'Visible', 'true');
    }
}

export = Menu;
