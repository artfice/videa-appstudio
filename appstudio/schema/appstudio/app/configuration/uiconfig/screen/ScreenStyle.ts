import NavigationStyle = require('./NavigationStyle');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Color = require('../../../../../common/Color');
import BackgroundImage = require('./BackgroundImage');
import ComplexColor = require('../../../../common/ComplexColor')

class ScreenStyle extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.ScreenStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background color', ComplexColor.ID);
        this._addRefProperty('backgroundColor', 'Background color', Color.ID);
        this._addRefProperty('backgroundImage', 'Background Image', BackgroundImage.ID);
        this._addRefProperty('navigation', 'Navigation Style', NavigationStyle.ID);
    }
}

export = ScreenStyle;