import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Roboto = require('../../../../../../../common/font/Roboto');
import OpenSans = require('../../../../../../../common/font/OpenSans');
import SanFrancisco = require('../../../../../../../common/font/SanFrancisco');
import CustomFont = require('../../../../../../../common/font/CustomFont');

class TitleStyle extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.navigation.topbar.TitleStyle';

    constructor() {

        super();
        this._addOneOfRefProperty('font', 'Font',
            [OpenSans.ID, CustomFont.ID , Roboto.ID, SanFrancisco.ID],
            ['Open Sans', 'Custom', 'Roboto',  'San Francisco'], OpenSans.ID);
    }
}

export = TitleStyle;
