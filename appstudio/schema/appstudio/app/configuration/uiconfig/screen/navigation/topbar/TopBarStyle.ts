import ComplexColor = require('../../../../../../../appstudio/common/ComplexColor');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class TopBarStyle extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.navigation.topbar.TopBarStyle';

    constructor() {

        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
    }
}

export = TopBarStyle;
