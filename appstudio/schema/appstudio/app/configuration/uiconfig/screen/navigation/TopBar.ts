import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import TopBarStyle = require('./topbar/TopBarStyle');
import TitleStyle = require('./topbar/TitleStyle');

class TopBar extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.navigation.TopBar';

    constructor() {

        super();
        this._addPrimitiveProperty('visible', DataTypes.BOOLEAN, 'Visible', true);
        this._addPrimitiveProperty('overlay', DataTypes.BOOLEAN, 'Overlay', false);
        this._addPrimitiveProperty('showAppLogoAsTitle', DataTypes.BOOLEAN, 'Show App Logo instead of Title', true);
        this._addRefProperty('style', 'Style', TopBarStyle.ID);
        this._addRefProperty('titleStyle', 'Title Style', TitleStyle.ID);
    }
}

export = TopBar;
