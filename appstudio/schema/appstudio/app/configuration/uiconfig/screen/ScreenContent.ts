import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class ScreenContent extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.ScreenContent';

    constructor() {
        super();
        this._addPrimitiveProperty('title', DataTypes.STRING, 'Title');
    }
}

export = ScreenContent;