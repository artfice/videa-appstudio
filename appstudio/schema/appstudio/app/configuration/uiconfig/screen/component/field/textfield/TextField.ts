import BaseField = require('../BaseField');
import TextFieldContent = require('../content/TextFieldContent');
import TextFieldStyle = require('./TextFieldStyle');

class TextField extends BaseField {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.textfield.TextField';

    constructor () {
        super();
    }
    
    protected _getContent(): string {
        return TextFieldContent.ID;
    }    
    
    protected _getStyle(): string {
        return TextFieldStyle.ID;
    }
}

export = TextField;