import BaseFieldStyle = require('../BaseFieldStyle');
import Color = require('../../../../../../../../common/Color');

class ProgressBarStyle extends BaseFieldStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.progressBar.ProgressBarStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', Color.ID);
        this._addRefProperty('foregroundColor', 'Foreground Color', Color.ID);
    }

    protected _getDefaultHeight() : string {
        return 'matchParent';
    }

    protected _getDefaultWidth() : string {
        return 'matchParent';
    }

    protected _defaultGravity () : string {
        return 'bottom';
    }
}

export = ProgressBarStyle;