import SizedComponentStyle = require('../../style/SizedComponentStyle');
import ComplexColor = require('../../../../../../../../appstudio/common/ComplexColor');

class DataViewHeadingStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.heading.DataViewHeadingStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
    }
}

export = DataViewHeadingStyle;