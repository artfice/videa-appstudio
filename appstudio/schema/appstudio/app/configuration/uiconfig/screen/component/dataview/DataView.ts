import NamedComponent = require('../NamedComponent');
import DataViewHeading = require('./DataViewHeading');
import DataViewContent = require('./DataViewContent');
import SizedComponentStyle = require('../style/SizedComponentStyle');
import DataViewTile = require('./DataViewTile');

class DataView extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.DataView';

    constructor() {
        super();

        this._addRefProperty('content', 'Collection', this._getCollection());
        this._addRefProperty('heading', 'Heading', DataViewHeading.ID);
        this._addRefProperty('tile', 'Tile', this._getTileSettings());
    }


    protected _getCollection(): string {
        return DataViewContent.ID;
    }

    protected _getStyle(): string {
        return SizedComponentStyle.ID;
    }
    
    protected _getTileSettings () : string {
        return DataViewTile.ID;
    }
}

export = DataView;
