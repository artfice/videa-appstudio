import Color = require('../../../../../../../../common/Color');
import BaseFieldStyle = require('../BaseFieldStyle');

class ToggleStyle extends BaseFieldStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.toggle.ToggleStyle';

    constructor() {
        super();
        this._addRefProperty('activatedColor', 'Activated Color', Color.ID);
        this._addRefProperty('deactivatedColor', 'Deactivated Color', Color.ID);
    }

}

export = ToggleStyle;