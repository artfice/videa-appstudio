import PickerContent = require('./PickerContent');
import PickerStyle = require('./PickerStyle');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class CollectionPickerSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.collectionview.picker.CollectionPickerSettings';

    constructor() {
        super();
        this._addRefProperty("content", "Content", PickerContent.ID);
        this._addRefProperty("style", "Style", PickerStyle.ID);
    }
}

export = CollectionPickerSettings;