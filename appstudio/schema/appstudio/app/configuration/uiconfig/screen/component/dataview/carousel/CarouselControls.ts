import ComponentControls = require('../../ComponentControls');

class CarouselControls extends ComponentControls {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.carousel.CarouselControls';

    constructor() {
        super();
    }
}

export = CarouselControls;
