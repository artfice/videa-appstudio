import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import CustomData = require("../../../../../../common/CustomData");

class AdvanceComponent extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.AdvanceComponent';

    constructor() {
        super();
        this._addRefProperty('customData', 'Advance Editor', CustomData.ID);
    }
}

export = AdvanceComponent;