import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class FieldContent extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.FieldContent';

    constructor() {
        super();
        this._addPrimitiveProperty(this._getValueName(), this._getValueType(), this._getValueTitle(), this._getValueDefaultValue());
    }
    
   protected _getValueDefaultValue(): string {
        return '';
    }
    
    protected _getValueName(): string {
        return 'value';
    }

    protected _getValueType(): string {
        return DataTypes.STRING;
    }

    protected _getValueTitle(): string {
        return 'Value';
    }
}

export = FieldContent;