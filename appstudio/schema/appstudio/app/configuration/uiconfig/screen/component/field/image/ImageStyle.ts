import BaseFieldStyle = require('../BaseFieldStyle');

import Scale = require('../../../../../../../../../schema/common/Scale');

import StandardAspectRatio = require('../../../../../../../../common/aspectRatio/StandardAspectRatio');
import CustomAspectRatio = require('../../../../../../../../common/aspectRatio/CustomAspectRatio');

class ImageStyle extends BaseFieldStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.ImageStyle';

    constructor() {
        super();

        this._addRefProperty('scale', 'Scale', Scale.ID);

        this._addOneOfRefProperty('aspectRatio', 'Aspect Ratio', [StandardAspectRatio.ID, CustomAspectRatio.ID],
            ['Standard Aspect Ratio', 'Custom Aspect Ratio'], StandardAspectRatio.ID);
    }
}

export = ImageStyle;