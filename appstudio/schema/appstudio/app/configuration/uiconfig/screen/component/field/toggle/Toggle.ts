import ToggleStyle = require('./ToggleStyle');
import BaseField = require('../BaseField');

class Toggle extends BaseField {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.toggle.Toggle';

    constructor () {
        super();
    }
    
    protected _getStyle(): string {
        return ToggleStyle.ID;
    }
}

export = Toggle;