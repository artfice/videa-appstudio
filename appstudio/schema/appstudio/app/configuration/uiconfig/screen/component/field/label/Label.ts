import BaseField = require('../BaseField');
import LabelContent = require('../content/LabelContent');
import LabelStyle = require('./LabelStyle');

class Label extends BaseField {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.label.Label';

    constructor () {
        super();
    }
    
    protected _getContent(): string {
        return LabelContent.ID;
    }    
    
    protected _getStyle(): string {
        return LabelStyle.ID;
    }
}

export = Label;