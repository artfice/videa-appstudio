import ComplexColor = require('../../../../../../../appstudio/common/ComplexColor');
import ComponentStyle = require('./ComponentStyle');

class ColoredComponentStyle extends ComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.style.ColoredComponentStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
    }
}

export = ColoredComponentStyle;