import BaseFieldStyle = require('../BaseFieldStyle');
import Roboto = require('../../../../../../../../common/font/Roboto');
import OpenSans = require('../../../../../../../../common/font/OpenSans');
import SanFrancisco = require('../../../../../../../../common/font/SanFrancisco');
import CustomFont = require('../../../../../../../../common/font/CustomFont');

class LabelStyle extends BaseFieldStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.label.LabelStyle';

    constructor() {
        super();
        this._addOneOfRefProperty('font', 'Font',
        [OpenSans.ID, CustomFont.ID , Roboto.ID, SanFrancisco.ID],
        ['Open Sans', 'Custom', 'Roboto',  'San Francisco'], OpenSans.ID);

    }
}

export = LabelStyle;