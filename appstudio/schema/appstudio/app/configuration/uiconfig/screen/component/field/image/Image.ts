/**
 * Created by bardiakhosravi on 2015-12-21.
 */
import BaseField = require('../BaseField');
import ImageStyle = require('./ImageStyle');
import ImageContent = require('../content/ImageContent');

class Image extends BaseField {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.Image';

    constructor () {
        super();
    }
    
    protected _getContent(): string {
        return ImageContent.ID;
    }    
    
    protected _getStyle(): string {
        return ImageStyle.ID;
    }
}

export = Image;