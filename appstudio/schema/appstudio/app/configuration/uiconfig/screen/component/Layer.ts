import { DataTypes } from 'videa-framework/schema/DataTypes';
import Component = require('./Component');
import NamedComponent = require('./NamedComponent');
import Carousel = require('./dataview/carousel/Carousel');
import ListView = require('./dataview/gridview/listview/ListView');
import GridView = require('./dataview/gridview/GridView');
import Swimlane = require('./dataview/gridview/swimlane/Swimlane');
import Image = require('./field/image/Image');
import Label = require('./field/label/Label');
import CollectionView = require('./collectionview/CollectionsView');
import Toggle = require('./field/toggle/Toggle');
import TextField = require('./field/textfield/TextField');
import AdvanceComponent = require('./AdvanceComponent');
import WebView = require('./webview/WebView');
import SizedComponentStyle = require('./style/SizedComponentStyle');
import LayerControls = require('./LayerControls');

class Layer extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.Layer';

    constructor() {
        super();
        this._addArrayAnyOfProperty('subComponent', 'Components', 'SubComponents', Component.ID,
            [ListView.ID, GridView.ID, Carousel.ID, Swimlane.ID, Image.ID, Label.ID,
            CollectionView.ID, Toggle.ID, TextField.ID, AdvanceComponent.ID, WebView.ID,
            Layer.ID],
            ['List View', 'Grid View', 'Carousel', 'Swim Lane', 'Image', 'Label', 'Collection View',
                'Toggle', 'Text Field', 'Advanced Component', 'Web View', 'Layer']);

    }

    protected _getStyle () {
        return SizedComponentStyle.ID;
    }

    protected _getControls() {
        return LayerControls.ID;
    }
}

export = Layer;