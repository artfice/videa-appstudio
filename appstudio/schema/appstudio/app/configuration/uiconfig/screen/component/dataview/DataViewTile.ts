import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import DataViewTileSettings = require('./tile/DataViewTileSettings');
import Label = require('../field/label/Label');
import Image = require('../field/image/Image');
import ProgressBar = require('../field/progressBar/ProgressBar');
import NamedComponent = require('../../component/NamedComponent');
import TileLayer = require('../../component/TileLayer');

class DataViewTile extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.DataViewTile';

    constructor() {
        super();
        this._addRefProperty('settings', 'Settings', DataViewTileSettings.ID);
        this._addArrayAnyOfProperty('text', 'Textfields', 'Text', Label.ID, [Label.ID], ['Text']);
        this._addArrayAnyOfProperty('subComponent', 'Sub Components', 'Sub Component', NamedComponent.ID, [
            Image.ID, Label.ID, ProgressBar.ID, TileLayer.ID], ['Image', 'Label', 'ProgressBar', 'Layer']);
    }
}

export = DataViewTile;