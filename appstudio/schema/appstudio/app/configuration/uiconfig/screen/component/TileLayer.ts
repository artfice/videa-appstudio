import { DataTypes } from 'videa-framework/schema/DataTypes';
import Component = require('./Component');
import NamedComponent = require('./NamedComponent');
import Image = require('./field/image/Image');
import Label = require('./field/label/Label');
import Toggle = require('./field/toggle/Toggle');
import TextField = require('./field/textfield/TextField');
import SizedComponentStyle = require('./style/SizedComponentStyle');
import LayerControls = require('./LayerControls');

class TileLayer extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.TileLayer';


    constructor() {
        super();
        this._addArrayAnyOfProperty('subComponent', 'Components', 'SubComponents', Component.ID,
            [Image.ID, Label.ID, Toggle.ID, TextField.ID, TileLayer.ID],
            [ 'Image', 'Label', 'Toggle', 'Text Field', 'TileLayer']);
    }

    protected _getStyle () {
        return SizedComponentStyle.ID;
    }

    protected _getControls() {
        return LayerControls.ID;
    }
}

export = TileLayer;