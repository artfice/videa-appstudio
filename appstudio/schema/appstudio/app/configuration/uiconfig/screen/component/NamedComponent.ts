import Component = require('./Component');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class NamedComponent extends Component {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.NamedComponent';
	
    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
    }
}

export = NamedComponent;

