import SizedComponentStyle = require('../style/SizedComponentStyle');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class WebViewStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.webview.WebViewStyle';

    constructor() {
        super();
        this._addPrimitiveProperty('verticalScrollEnabled', DataTypes.BOOLEAN, 'Vertical Scroll', false);
        this._addPrimitiveProperty('horizontalScrollEnabled', DataTypes.BOOLEAN, 'Horizontal Scroll', false);
        this._addPrimitiveProperty('fallbackMargin', DataTypes.STRING, 'Fallback Margin', '10 15 10 15');
    }
}

export = WebViewStyle;