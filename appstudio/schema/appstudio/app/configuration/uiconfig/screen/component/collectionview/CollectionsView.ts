import NamedComponent = require('../NamedComponent');
import CollectionPickerSettings = require('./picker/CollectionPickerSettings');
import GridView = require('../dataview/gridview/GridView');
import SizedComponentStyle = require('../style/SizedComponentStyle');

class CollectionView extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.collectionview.CollectionsView';

    constructor() {
        super();
        this._addRefProperty('picker', 'Picker', CollectionPickerSettings.ID);
        this._addRefProperty('gridview', 'Grid', GridView.ID);
    }

    protected _getStyle(): string {
        return SizedComponentStyle.ID;
    }
}

export = CollectionView;
