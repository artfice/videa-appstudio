import DataView = require('../DataView');
import GridViewStyle = require('./GridViewStyle');

class GridView extends DataView {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.gridview.GridView';

    constructor() {
        super();
    }

    protected _getStyle(): string {
        return GridViewStyle.ID;
    }
}

export = GridView;
