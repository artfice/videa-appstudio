import FieldContent = require('../FieldContent');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class TextFieldContent extends FieldContent {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.TextFieldContent';

    constructor() {
        super();
        this._addPrimitiveProperty('componentId', DataTypes.STRING, 'Component Id', '');
        this._addPrimitiveProperty('inputType', DataTypes.STRING, 'Input Type', 'Text');
        this._addPrimitiveProperty('hint', DataTypes.STRING, 'Hints');
    }
    
    protected _getValueTitle(): string {
        return 'Default Value';
    }
}

export = TextFieldContent;