import ComponentStyle = require('./ComponentStyle');
import Width = require('../../../../../../../common/Width');
import Height = require('../../../../../../../common/Height');

class SizedComponentStyle extends ComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.style.SizedComponentStyle';

    constructor() {
        super();
        this._addRefProperty('width', 'Width', Width.ID);
        this._addRefProperty('height', 'Height', Height.ID);
    }
    
}

export = SizedComponentStyle;