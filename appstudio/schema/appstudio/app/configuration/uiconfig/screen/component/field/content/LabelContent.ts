import FieldContent = require('../FieldContent');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class LabelContent extends FieldContent {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.LabelContent';

    constructor() {
        super();
        this._addPrimitiveProperty('numberOfLines', DataTypes.NUMBER, 'Number Of Lines', 1);
        this._addPrimitiveProperty('maxLines', DataTypes.NUMBER, 'Maximum Number Of Lines', 1);
        this._addPrimitiveProperty('link', DataTypes.STRING, 'Link');
    }
}

export = LabelContent;