import ComponentControls = require('./ComponentControls');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import ComponentStyle = require('./style/ComponentStyle');

class Component extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.Component';
	
	
    constructor() {
        super();
		
		this._addRefProperty('controls', 'Controls', this._getControls());
		this._addRefProperty('style', 'Style', this._getStyle());
    }
	
	protected _getControls() {
		return ComponentControls.ID;
	}
    
    protected _getStyle () {
        return ComponentStyle.ID;
    }
}

export = Component;

