import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import DataViewHeadingSettings = require('./heading/DataViewHeadingSettings');
import Label = require('../field/label/Label');

class DataViewHeading extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.DataViewHeading';

    constructor() {
        super();
        this._addRefProperty('settings', 'Settings', DataViewHeadingSettings.ID);
        this._addArrayAnyOfProperty('text', 'Textfields', 'Text', Label.ID, [Label.ID], ['Text']);
    }
}

export = DataViewHeading;