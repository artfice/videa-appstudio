import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import ProgressBarStyle = require('./ProgressBarStyle');
import FieldContent = require('../FieldContent');

class ProgressBar extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.progressBar.ProgressBar';

    constructor() {
        super();
        this._addRefProperty('content', 'Content', FieldContent.ID);
        this._addRefProperty('style', 'Style', ProgressBarStyle.ID);
    }
}

export = ProgressBar;