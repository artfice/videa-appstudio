import NamedComponent = require('../../NamedComponent');
import DataViewContent = require('../DataViewContent');
import CarouselStyle = require('./CarouselStyle');
import CarouselControls = require('./CarouselControls');
import CarouselTile = require('./CarouselTile');

class Carousel extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.carousel.Carousel';

    constructor() {
        super();
        this._addRefProperty('content', 'Collection', this._getCollection());
        this._addRefProperty('tile', 'Tile', this._getTileSettings());
    }

   protected _getCollection(): string {
        return DataViewContent.ID;
    }

    protected _getStyle(): string {
        return CarouselStyle.ID;
    }

    protected _getControls(): string {
        return CarouselControls.ID;
    }
    
    protected _getTileSettings() : string {
        return CarouselTile.ID;
    }
}

export = Carousel;
