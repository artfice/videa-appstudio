import FieldContent = require('../FieldContent');

class ImageContent extends FieldContent {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.ImageContent';

    constructor() {
        super();
    }
    
    protected _getValueTitle(): string {
        return 'Source Url';
    }
    
   protected _getValueDefaultValue(): string {
        return '{model.defaultThumbnailUrl}';
    }    
    
}

export = ImageContent;