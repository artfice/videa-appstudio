import DataViewTile = require('../DataViewTile');

class CarouselTile extends DataViewTile {
    
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.carousel.CarouselTile';

    constructor() {
        super();
    }
}

export = CarouselTile;