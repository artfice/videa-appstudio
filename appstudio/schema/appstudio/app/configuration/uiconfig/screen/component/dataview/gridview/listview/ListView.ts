import GridView = require('../GridView');
import SizedComponentStyle = require('../../../style/SizedComponentStyle');

class ListView extends GridView {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.gridview.listview.ListView';

    constructor() {
        super();
    }
    
    protected _getStyle(): string {
        return SizedComponentStyle.ID;
    }    
}

export = ListView;
