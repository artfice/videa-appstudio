import LabelStyle = require('../label/LabelStyle');

class TextFieldStyle extends LabelStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.textfield.TextFieldStyle';

    constructor() {
        super();
    }
}

export = TextFieldStyle;