import Listener = require('../../../../../../common/Listener');
import BaseControls = require('../../../../../common/BaseControls');

class ComponentControls extends BaseControls {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls';

    private _listeners: string[];
    private _listenerTitles: string[];

    constructor() {
        super();
        this._listeners = [];
        this._listenerTitles = [];
        this._addArrayProperty('listeners', 'Listeners', Listener.ID);
    }

    // not used yet
    protected _addListener(listener: string, listenerTitle: string) {
        this._listeners.push(listener);
        this._listenerTitles.push(listenerTitle);
    }
}

export = ComponentControls;
