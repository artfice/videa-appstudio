import * as PAGINATION_LOCATIONS from '../../../../../../../../common/enum/components/PaginationLocation';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import SizedComponentStyle = require("../../style/SizedComponentStyle");
import Color = require('../../../../../../../../common/Color');
import Position = require('../../../../../../../../common/Position');

class CarouselStyle extends SizedComponentStyle {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.carousel.CarouselStyle';

    constructor() {
        super();
        this._addPrimitiveProperty('tileScale', DataTypes.NUMBER, 'Tile Scale', 1);

        this._addPrimitiveProperty('paginationDots', DataTypes.BOOLEAN, 'Pagination Dots', true);

        this._addRefProperty('paginationSelectedDotColors', 'Pagination Selected Dot Colors', Color.ID);

        this._addRefProperty('paginationUnselectedDotColors', 'Pagination UnSelected Dot Colors', Color.ID);

        this._addRefProperty('paginationDotPosition', 'Pagination Dot Position', Position.ID);

        this._addEnumProperty('paginationLocation', DataTypes.STRING, 'Pagination Location', PAGINATION_LOCATIONS, PAGINATION_LOCATIONS[0]);

        this._addPrimitiveProperty('paginationMargin', DataTypes.STRING, 'Pagination Margin', '0 0 0 0');

        this._addPrimitiveProperty('autoScrollDuration', DataTypes.NUMBER, 'Autoscroll (ms)', 0);
    }
}

export = CarouselStyle;