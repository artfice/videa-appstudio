import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import OfflineCollection = require('../../../../../../../common/collection/OfflineCollection');
import RemoteCollection = require('../../../../../../../common/collection/RemoteCollection');
import UIDCollection = require('../../../../../../../common/collection/UIDCollection');
import FavoriteCollection = require('../../../../../../../common/collection/FavoriteCollection');
import WatchHistoryCollection = require('../../../../../../../common/collection/WatchHistoryCollection');
import CustomData = require('../../../../../../../common/CustomData');

class DataViewContent extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.DataViewContent';

    constructor() {
        super();
        this._addOneOfRefProperty('collection', 'Collection', [
            OfflineCollection.ID, 
            RemoteCollection.ID, 
            UIDCollection.ID,
            FavoriteCollection.ID,
            WatchHistoryCollection.ID,
            CustomData.ID], ['Offline', 'Remote', 'UID', 'Favorite', 'Watch History', 'Custom']);
    }
}

export = DataViewContent;