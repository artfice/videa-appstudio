import SizedComponentStyle = require('../../style/SizedComponentStyle');
import ComplexColor = require('../../../../../../../../appstudio/common/ComplexColor');
import StandardAspectRatio = require('../../../../../../../../common/aspectRatio/StandardAspectRatio');
import CustomAspectRatio = require('../../../../../../../../common/aspectRatio/CustomAspectRatio');

class DataViewTileStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.tile.DataViewTileStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
        this._addOneOfRefProperty('aspectRatio', 'Aspect Ratio', [
            StandardAspectRatio.ID, CustomAspectRatio.ID], [
                'Standard Aspect Ratio', 'Custom Aspect Ratio'
            ], StandardAspectRatio.ID);
    }
}

export = DataViewTileStyle;