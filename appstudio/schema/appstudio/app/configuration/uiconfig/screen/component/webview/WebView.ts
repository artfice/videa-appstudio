import WebViewStyle = require('./WebViewStyle');
import WebViewContent = require('./WebViewContent');
import NamedComponent = require('../NamedComponent');

class WebView extends NamedComponent {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.webview.WebView';

    constructor() {
        super();
        this._addRefProperty('content', 'Content', this._getContent());
    }

    protected _getContent(): string {
        return WebViewContent.ID;
    }

    protected _getStyle () {
        return WebViewStyle.ID;
    }
}

export = WebView;