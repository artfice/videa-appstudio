import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import DataViewTileStyle = require('./DataViewTileStyle');
import ComponentControls = require('../../ComponentControls');

class DataViewTileSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.tile.DataViewTileSettings';

    constructor() {
        super();
        this._addRefProperty('style', 'Style', DataViewTileStyle.ID);
        this._addRefProperty('controls', 'Controls', ComponentControls.ID);
    }
}

export = DataViewTileSettings;