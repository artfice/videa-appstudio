/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import Border = require('../../../../../../../common/Border');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import BoxParams = require('../../../../../../../common/BoxParams');

class ComponentStyle extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.style.ComponentStyle';

    constructor() {
        super();
        this._addRefProperty('margin', 'Margin', BoxParams.ID);
        this._addRefProperty('padding', 'Padding', BoxParams.ID);
        this._addRefProperty('border', 'Border', Border.ID);
    }
}

export = ComponentStyle;