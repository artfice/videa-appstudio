import SizedComponentStyle = require('../../style/SizedComponentStyle');
import ComplexColor = require('../../../../../../../../appstudio/common/ComplexColor');
import Roboto = require('../../../../../../../../common/font/Roboto');
import OpenSans = require('../../../../../../../../common/font/OpenSans');
import SanFrancisco = require('../../../../../../../../common/font/SanFrancisco');
import CustomFont = require('../../../../../../../../common/font/CustomFont');

class PickerStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.collectionview.picker.PickerStyle';

    constructor() {
        super();
        this._addRefProperty('backgroundColor', 'Background Color', ComplexColor.ID);
        this._addOneOfRefProperty('font', 'Font',
        [OpenSans.ID, CustomFont.ID , Roboto.ID, SanFrancisco.ID],
        ['Open Sans', 'Custom', 'Roboto',  'San Francisco'], OpenSans.ID);
    }
}

export = PickerStyle;