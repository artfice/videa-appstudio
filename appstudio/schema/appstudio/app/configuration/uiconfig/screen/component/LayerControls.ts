import { DataTypes } from 'videa-framework/schema/DataTypes';
import ComponentControls = require('../component/ComponentControls');
import VerticalLayout = require('../../../../../../common/layout/VerticalLayout');
import RelativeLayout = require('../../../../../../common/layout/RelativeLayout');
import HorizontalLayout = require('../../../../../../common/layout/HorizontalLayout');

class LayerControls extends ComponentControls {
  public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.LayerControls';

  constructor() {
    super();

    this._addOneOfRefProperty('layout', 'Layout', [HorizontalLayout.ID, VerticalLayout.ID,
    RelativeLayout.ID], 
    ['Horizontal', 'Vertical', 'Relative'], undefined, RelativeLayout.ID);
  }
}

export = LayerControls;