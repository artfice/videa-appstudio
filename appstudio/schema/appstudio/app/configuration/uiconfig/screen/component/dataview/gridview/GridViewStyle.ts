import SizedComponentStyle = require('../../style/SizedComponentStyle');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class GridViewStyle extends SizedComponentStyle {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.gridview.GridViewStyle';

    constructor() {

        super();

        this._addPrimitiveProperty('columns', DataTypes.NUMBER, 'Columns', this._getdefaultColumns());
        this._addPrimitiveProperty('rows', DataTypes.NUMBER, 'Rows', this._getDefaultRows());
    }        
    
    protected _getdefaultColumns(): number {
        return 2;
    }
    
    protected _getDefaultRows(): number {
        return 1;
    }    
}

export = GridViewStyle;