/**
 * Created by bardiakhosravi on 2015-12-22.
 */
import SizedComponentStyle = require('../style/SizedComponentStyle');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BaseFieldStyle extends SizedComponentStyle {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.BaseFieldStyle';

    constructor() {
        super();

        this._addPrimitiveProperty('gravity', DataTypes.STRING, 'Gravity', this._defaultGravity());
    }
    
    protected _defaultGravity(): string {
        return 'center';
    }
}

export = BaseFieldStyle;