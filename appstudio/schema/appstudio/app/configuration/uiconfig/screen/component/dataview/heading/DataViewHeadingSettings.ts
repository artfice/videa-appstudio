import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import DataViewHeadingStyle = require('./DataViewHeadingStyle');
import ComponentControls = require('../../ComponentControls');

class DataViewHeadingSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.heading.DataViewHeadingSettings';

    constructor() {
        super();
        this._addRefProperty('style', 'Style', DataViewHeadingStyle.ID);
        this._addRefProperty('controls', 'Controls', ComponentControls.ID);
    }
}

export = DataViewHeadingSettings;