import BaseFieldStyle = require('./BaseFieldStyle');

import Content = require('./FieldContent');
import NamedComponent = require('../NamedComponent');

class BaseField extends NamedComponent {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.field.BaseField';

    constructor () {
        super();
        this._addRefProperty('content', 'Content', this._getContent());
    }

    protected _getContent(): string {
        return Content.ID;
    }
    
    protected _getStyle () {
        return BaseFieldStyle.ID;
    }    
}

export = BaseField;
