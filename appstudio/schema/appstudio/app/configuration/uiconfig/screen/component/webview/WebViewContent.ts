import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class WebViewContent extends BaseSchema {

    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.webview.WebViewContent';

    constructor() {
        super();
        this._addPrimitiveProperty('uri', DataTypes.STRING, 'URI', '');
    }
}

export = WebViewContent;