import GridView = require('../GridView');
import SizedComponentStyle = require('../../../style/SizedComponentStyle');

class Swimlane extends GridView {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.dataview.gridview.swimlane.Swimlane';

    constructor() {
        super();
    }

    protected _getStyle(): string {
        return SizedComponentStyle.ID;
    }
}

export = Swimlane;
