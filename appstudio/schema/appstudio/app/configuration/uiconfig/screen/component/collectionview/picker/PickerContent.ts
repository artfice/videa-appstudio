import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import OfflineCollection = require('../../../../../../../../common/collection/OfflineCollection');
import RemoteCollection = require('../../../../../../../../common/collection/RemoteCollection');
import UIDCollection = require('../../../../../../../../common/collection/UIDCollection');
import CustomData = require('../../../../../../../../common/CustomData');

class PickerContent extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.screen.component.collectionview.picker.PickerContent';

    constructor() {
        super();
        this._addOneOfRefProperty('collection', 'Collection Type', [
            OfflineCollection.ID, 
            RemoteCollection.ID, 
            UIDCollection.ID,
            CustomData.ID], ['Offline', 'Remote', 'UID', 'Custom']);
        this._addPrimitiveProperty('type', DataTypes.STRING, 'Type', 'Dropdown');
        this._addPrimitiveProperty('displayName', DataTypes.STRING, 'Display Name', '');
    }
}

export = PickerContent;