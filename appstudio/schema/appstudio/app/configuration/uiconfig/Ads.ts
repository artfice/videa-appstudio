import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import GoogleAdType = require('./ad/GoogleAdType');
import CustomAdType = require('./ad/CustomAdType');

class Ads extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.Ads';

    constructor() {
        super();
        this._addPrimitiveProperty('enabled', DataTypes.BOOLEAN, 'Enabled', false);
        this._addOneOfRefProperty('type', 'Type',
         [GoogleAdType.ID, CustomAdType.ID],
         ['Google IMA', 'Custom'], undefined, GoogleAdType.ID);
        this._addPrimitiveProperty('adTagUrl', DataTypes.STRING, 'Ad Tag Url', '');
    }

}

export = Ads;