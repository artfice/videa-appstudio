import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import ThemeFont = require('../../../../common/ThemeFont');

class Fonts extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.theme.Fonts';

    constructor() {
        super();

        this._addRefProperty('mainFont', 'Main Font', ThemeFont.ID, 'N/A');
        this._addRefProperty('titleFont', 'Title Font', ThemeFont.ID, 'N/A');
        this._addRefProperty('menuFont', 'Menu Font', ThemeFont.ID, 'N/A');
        this._addRefProperty('bodyFont', 'Body Font', ThemeFont.ID, 'N/A');
    }

}

export = Fonts;