import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import Color = require('../../../../../common/Color');

class Colors extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.theme.Colors';

    constructor() {
        super();

        this._addRefProperty('primarySpot', 'Primary Spot Color', Color.ID, '(Required) Top Bar Background Colour');
        this._addRefProperty('secondarySpot', 'Secondary Spot Color', Color.ID, '(Required) Menu Background Colour');
        this._addRefProperty('accent', 'Accent Color', Color.ID, 'N/A');
    }

}

export = Colors;