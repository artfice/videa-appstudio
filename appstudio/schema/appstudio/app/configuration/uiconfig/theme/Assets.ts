import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import GalleryImage = require('../../../../common/GalleryImage');
import ColorGalleryImage = require('../../../../common/ColorGalleryImage');

class Assets extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.theme.Assets';

    constructor() {
        super();
        this._addRefProperty('logo', 'Logo', GalleryImage.ID, 
            'The app logo/image that is displayed at the top of your menu navigation');
        this._addRefProperty('topBar', 'Top Bar', ColorGalleryImage.ID,
            '(Required) The top bar of your application where the screen name usually appears');
    }

}

export = Assets;


