import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class SearchSettings extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.SearchSettings';

    constructor() {
        super();
        this._addPrimitiveProperty('enabled', DataTypes.BOOLEAN, 'Enabled');
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name');
        this._addPrimitiveProperty('screenId', DataTypes.STRING, 'Search Screen');
    }

}

export = SearchSettings;