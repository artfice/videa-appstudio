import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Analytics extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.uiconfig.Analytics';

    constructor() {
		super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
    }
    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = Analytics;
