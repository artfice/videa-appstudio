/**
 * Created by bardiakhosravi on 2016-01-25.
 */
import UIConfig = require('./UIConfig');


class BundleUIConfig extends UIConfig {
	
	public static ID = 'schema.appstudio.app.configuration.BundleUIConfig';

	constructor() {
        super();
		this.addReferenceArrayProperty('uiConfigRefs', 'UI Config List', UIConfig.ID);
	}
}

export = BundleUIConfig;
