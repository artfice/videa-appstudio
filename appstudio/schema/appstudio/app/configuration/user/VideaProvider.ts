import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class VideaProvider extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.user.VideaProvider';

    constructor() {
        super();
    }

}

export = VideaProvider;