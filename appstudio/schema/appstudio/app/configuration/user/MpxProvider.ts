import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class MpxProvider extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.user.MpxProvider';

    constructor() {
        super();

        this._addPrimitiveProperty('accountId', DataTypes.STRING, 'MPX Account Identifier', 'http://mps.theplatform.com/data/Account/2666549181');
        this._addPrimitiveProperty('pid', DataTypes.STRING, 'MPX Pid', '_8Ho73IN2EVerZxw');
    }

}

export = MpxProvider;