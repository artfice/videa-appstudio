import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Cms extends BaseSchema {
	
	public static ID = 'schema.appstudio.app.configuration.Cms';

	constructor() {
		super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addPrimitiveProperty('entitlement', DataTypes.STRING, 'Entitlement');
	}

    protected _getDefaultDisplayField(): string {
        return 'name';
    }	

}

export = Cms;
