import { BaseSchema } from 'videa-framework/schema/BaseSchema';

import AkamaiProvider = require('../../../common/auth/AkamaiProvider');
import AdobeProvider = require('../../../common/auth/AdobeProvider');
import NoneProvider = require('../../../common/auth/NoneProvider');
import BasicAuthentication = require('../../../common/auth/BasicAuthentication');
import CustomData = require('../../../common/CustomData');

class Authentication extends BaseSchema {
    public static ID = 'schema.appstudio.app.configuration.Authentication';

    constructor() {
        super();
        this._addOneOfRefProperty('provider', 'Provider',
            [NoneProvider.ID, AkamaiProvider.ID, AdobeProvider.ID,
                BasicAuthentication.ID, CustomData.ID],
            ['None', 'Akamai', 'Adobe', 'Basic Authentication', 'Custom'], NoneProvider.ID);
    }
}

export = Authentication;
