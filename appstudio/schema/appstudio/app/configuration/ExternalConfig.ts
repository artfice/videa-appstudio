import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class ExternalConfig extends BaseSchema {
    
    public static ID = 'schema.appstudio.app.configuration.ExternalConfig';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addPrimitiveProperty('url', DataTypes.STRING, 'External Source URL', '');
    }

    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = ExternalConfig;
