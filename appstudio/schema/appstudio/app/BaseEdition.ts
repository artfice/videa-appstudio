import ExternalConfig = require('./configuration/ExternalConfig');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BaseEdition extends BaseSchema {
    public static ID = 'schema.appstudio.app.BaseEdition';

    constructor() {
        super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this.addReferenceArrayOneOfProperty(this._getUiConfigFieldName(), 'Version', this._getUIConfig());
        this._addPrimitiveProperty('clientConfig', DataTypes.STRING, 'Client Config ID', '');
        this._addEnumProperty('state', DataTypes.STRING, 'State', [
            'IN_PROGRESS',
            'NEED_REVIEW',
            'COMPLETED',
            'PUBLISHED'
        ], 'IN_PROGRESS');
    }

    protected _getUiConfigFieldName() {
        return 'uiConfig';
    }

    protected _getUIConfig() {
        return [ExternalConfig.ID];
    }

    protected _getDefaultDisplayField(): string {
        return 'name';
    }    

}

export = BaseEdition;
