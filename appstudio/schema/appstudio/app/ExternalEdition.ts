import BaseEdition = require('./BaseEdition');


class ExternalEdition extends BaseEdition {
    public static ID = 'schema.appstudio.app.ExternalEdition';

    constructor() {
        super();    
    }
}

export = ExternalEdition;
