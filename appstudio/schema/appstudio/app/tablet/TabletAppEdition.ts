
import Configuration = require('../Configuration');
import TabletUIConfig = require('./uiconfig/TabletUIConfig');
import TabletAuthentication = require('./authentication/TabletAuthentication');
import TabletMpx = require('./cms/TabletMpx');
import TabletVideaCms = require('./cms/TabletVideaCms');
import TabletGenericCms = require('./cms/TabletGenericCms');
import TabletCustomCms = require('./cms/TabletCustomCms');
import TabletVimondCms = require('./cms/TabletVimondCms');

class TabletAppEdition extends Configuration {
    public static ID = 'schema.appstudio.app.tablet.TabletAppEdition';

    constructor() {
        super();
    }

    protected _getUIConfig() {
        return [TabletUIConfig.ID];
    }

    protected _getAuthentication() {
        return TabletAuthentication.ID;
    }

    protected _getCmsSchemas() {
        return [TabletMpx.ID, TabletVideaCms.ID, TabletGenericCms.ID, TabletCustomCms.ID, TabletVimondCms.ID];
    }

    protected _getCmsTitles() {
        return ['Mpx', 'Videa', 'Generic JSON CMS', 'Custom', 'Vimond'];
    }
}

export = TabletAppEdition;
