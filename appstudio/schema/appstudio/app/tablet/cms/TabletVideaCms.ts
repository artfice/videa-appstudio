import VideaCms = require('../../../../common/cms/VideaCms');

class TabletVideaCms extends VideaCms {
    public static ID = 'schema.appstudio.app.tablet.cms.TabletVideaCms';

    constructor() {
        super();
    }

}

export = TabletVideaCms;
