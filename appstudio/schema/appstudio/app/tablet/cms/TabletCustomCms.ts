import CustomCms = require('../../../../common/cms/CustomCms');

class TabletCustomCms extends CustomCms {
    public static ID = 'schema.appstudio.app.tablet.cms.TabletCustomCms';

    constructor() {
        super();
    }
}

export = TabletCustomCms;
