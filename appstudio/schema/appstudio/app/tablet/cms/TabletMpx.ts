import Mpx = require('../../../../common/cms/Mpx');

class TabletMpx extends Mpx {
    public static ID = 'schema.appstudio.app.tablet.cms.TabletMpx';

    constructor() {
        super();
    }

}

export = TabletMpx;
