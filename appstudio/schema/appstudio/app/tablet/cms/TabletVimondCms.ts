import VideaCms = require('../../../../common/cms/VideaCms');

class TabletVimondCms extends VideaCms {
    public static ID = 'schema.appstudio.app.tablet.cms.TabletVimondCms';

    constructor() {
        super();
    }

}

export = TabletVimondCms;
