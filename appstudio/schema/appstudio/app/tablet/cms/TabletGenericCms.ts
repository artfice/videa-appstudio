import GenericCms = require('../../../../common/cms/GenericCms');

class TabletGenericCms extends GenericCms {
    public static ID = 'schema.appstudio.app.tablet.cms.TabletGenericCms';

    constructor() {
        super();
    }

}

export = TabletGenericCms;
