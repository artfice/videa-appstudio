
import Authentication = require('../../configuration/Authentication');


class TabletAuthentication extends Authentication {
    public static ID = 'schema.appstudio.app.tablet.authentication.TabletAuthentication';

    constructor() {
        super();
    }

}

export = TabletAuthentication;
