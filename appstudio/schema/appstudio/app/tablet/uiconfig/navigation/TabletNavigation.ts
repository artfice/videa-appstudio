import Navigation = require('../../../configuration/uiconfig/Navigation');
import BlankMenu = require('../../../../../common/navigation/BlankMenu');
import DrawerMenu = require('../../../../../common/navigation/DrawerMenu');
import BottomMenu = require('../../../../../common/navigation/BottomMenu');

class TabletNavigation extends Navigation {

    public static ID = 'schema.appstudio.app.tablet.uiconfig.navigation.TabletNavigation';

    constructor() {
        super();
    }

    protected _getMenuType() {
        return [
            BlankMenu.ID, 
            DrawerMenu.ID, 
            BottomMenu.ID
            ];
    }

    protected _getMenuTypeTitle() {
        return ['No Menu', 
                'Drawer Menu', 
                'Tabs Menu'
                ];
    }

    protected _getDefaultMenu() {
        return BlankMenu.ID;
    } 
}

export = TabletNavigation;
