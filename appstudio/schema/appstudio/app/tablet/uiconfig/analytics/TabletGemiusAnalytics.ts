import GemiusAnalytics = require('../../../configuration/uiconfig/analytics/gemius/GemiusAnalytics');

class TabletGemiusAnalytics extends GemiusAnalytics {
    public static ID = 'schema.appstudio.app.tablet.uiconfig.analytics.TabletGemiusAnalytics';

    constructor() {
        super();
    }

}

export = TabletGemiusAnalytics;
