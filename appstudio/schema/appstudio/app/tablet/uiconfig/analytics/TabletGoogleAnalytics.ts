import GoogleAnalytics = require('../../../configuration/uiconfig/analytics/google/GoogleAnalytics');

class TabletGoogleAnalytics extends GoogleAnalytics {
    public static ID = 'schema.appstudio.app.tablet.uiconfig.analytics.TabletGoogleAnalytics';

    constructor() {
        super();
    }

}

export = TabletGoogleAnalytics;
