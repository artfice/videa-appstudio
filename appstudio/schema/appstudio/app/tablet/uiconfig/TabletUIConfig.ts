import UIConfig = require('../../configuration/UIConfig');
import TabletNavigation = require('./navigation/TabletNavigation');
import TabletTheme = require('./theme/TabletTheme');
import TabletScreen = require('./screen/TabletScreen');
import AdvanceScreen = require('../../configuration/uiconfig/AdvanceScreen');
import TabletSearchResultScreen = require('./screen/TabletSearchResultScreen');
import TabletAds = require('./ads/TabletAds');
import TabletGoogleAnalytics = require('./analytics/TabletGoogleAnalytics');
import CustomAnalytics = require('../../configuration/uiconfig/analytics/custom/CustomAnalytics');
import TabletGemiusAnalytics = require('./analytics/TabletGemiusAnalytics');


class TabletUIConfig extends UIConfig {
    public static ID = 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig';

    constructor() {
        super();
    }

    protected getNavigation() {
        return TabletNavigation.ID;
    }

    protected getTheme() {
        return TabletTheme.ID;
    }

    protected getScreen() {
        return [TabletScreen.ID, AdvanceScreen.ID, TabletSearchResultScreen.ID];
    }

    protected _getAds() {
        return TabletAds.ID;
    }

    protected _getAnalyticSchemas() {
        return [TabletGoogleAnalytics.ID, CustomAnalytics.ID, TabletGemiusAnalytics.ID];
    }

    protected _getAnalyticTitles() {
        return ['Google Analytics', 'Custom', 'Gemius'];
    }
}

export = TabletUIConfig;
