import Ads = require('../../../configuration/uiconfig/Ads');

class TabletAds extends Ads {
    public static ID = 'schema.appstudio.app.tablet.uiconfig.ads.TabletAds';

    constructor() {
        super();
    }

}

export = TabletAds;