import SearchResultScreen = require('../../../configuration/uiconfig/SearchResultScreen');

class TabletSearchResultScreen extends SearchResultScreen {
    public static ID = 'schema.appstudio.app.tablet.uiconfig.screen.TabletSearchResultScreen';

    constructor() {
        super();
    }
    
    
}

export = TabletSearchResultScreen;
