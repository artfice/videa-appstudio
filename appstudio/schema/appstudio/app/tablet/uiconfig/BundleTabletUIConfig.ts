import TabletUIConfig = require('./TabletUIConfig');


class BundleTabletUIConfig extends TabletUIConfig {
	public static ID = 'schema.appstudio.app.tablet.uiconfig.BundleTabletUIConfig';

	constructor() {
        super();
		this.addReferenceArrayProperty('uiConfigRefs', 'UI Config List', TabletUIConfig.ID);
	}
}

export = BundleTabletUIConfig;
