import Theme = require('../../../configuration/uiconfig/Theme');

class TabletTheme extends Theme {
    public static ID = 'schema.appstudio.app.tablet.uiconfig.theme.TabletTheme';

    constructor() {
        super();
    }
    
    
}

export = TabletTheme;
