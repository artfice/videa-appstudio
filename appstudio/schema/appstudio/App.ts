import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import ExternalEdition = require('./app/ExternalEdition');

class App extends BaseSchema {
    public static ID = 'schema.appstudio.App';

    constructor() {
        super();

        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this.addReferenceArrayOneOfProperty('edition', 'Editions', this._getEditions());
        this._addPrimitiveProperty('activeEdition', DataTypes.STRING, 'Active Edition', '');
        this._addPrimitiveProperty('clientConfig', DataTypes.STRING, 'Client App Config ID', '');
    }

    protected _getEditions () : string[]{
        return [ExternalEdition.ID];
    }

    protected _getDefaultDisplayField(): string {
        return 'name';
    }    
}

export = App;
