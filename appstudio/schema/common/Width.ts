import { BaseSchema } from 'videa-framework/schema/BaseSchema';

import WrapContent = require('./WrapContent');
import CustomWidth = require('./CustomWidth');
import MatchParent = require('./MatchParent');

class Width extends BaseSchema {

    public static ID = 'schema.appstudio.common.Width';

    constructor() {
        super();
        this._addOneOfRefProperty('value', 'Value', [
            WrapContent.ID, MatchParent.ID, CustomWidth.ID], [
                'Wrap Content', 'Match Parent', 'Custom Width'
            ], this._getDefaultWidth());
    }
    
    protected _getDefaultWidth() : string {
        return WrapContent.ID;      
    }    
}

export = Width;