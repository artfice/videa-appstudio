/**
 * Created by bardiakhosravi on 15-11-19.
 */
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Range extends BaseSchema {
    public static ID = 'schema.common.Range';

    constructor() {
        super();

        this._addPrimitiveProperty('beginIndex', DataTypes.NUMBER, 'Begin Index', 0);
        this._addPrimitiveProperty('endIndex', DataTypes.NUMBER, 'End Index', 9);
    }
}

export = Range;
