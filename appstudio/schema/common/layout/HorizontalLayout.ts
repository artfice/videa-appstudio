import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class HorizontalLayout extends BaseSchema {
    public static ID = 'schema.common.layout.HorizontalLayout';

    constructor() {
        super();
    }
}

export = HorizontalLayout;