import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class VerticalLayout extends BaseSchema {
    public static ID = 'schema.common.layout.VerticalLayout';

    constructor() {
        super();
    }
}

export = VerticalLayout;