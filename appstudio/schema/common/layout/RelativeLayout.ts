import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class RelativeLayout extends BaseSchema {
    public static ID = 'schema.common.layout.RelativeLayout';

    constructor() {
        super();
    }
}

export = RelativeLayout;