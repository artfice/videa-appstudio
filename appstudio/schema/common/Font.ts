/**
 * Created by bardiakhosravi on 2015-12-10.
 */
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import FontColor = require('../common/FontColor');

import FONT_WEIGHTS = require('../common/enum/fonts/FontWeight');
import FONT_FAMILIES = require('../common/enum/fonts/FontFamilies');
import FONT_ALIGNMENTS = require('../common/enum/fonts/Alignments');


class Font extends BaseSchema {

    public static ID = 'schema.common.Font';

    constructor() {
        super();

        // TODO: needs base set of fonts and specific fonts for each form factor or device

        this._addEnumProperty("family", DataTypes.STRING, "Font Family",
            FONT_FAMILIES, FONT_FAMILIES[0]);


        // this._addEnumProperty("size", DataTypes.NUMBER, "Font Size", Font.FONT_SIZES, 'Medium');
        this._addPrimitiveProperty("size", DataTypes.NUMBER, "Font Size", 12);


        this._addEnumProperty('weight', DataTypes.STRING, 'Font Weight',
            FONT_WEIGHTS, FONT_WEIGHTS[0]);

        this._addRefProperty('color', 'Color', FontColor.ID);
        
        this._addEnumProperty('alignment', DataTypes.STRING, 'Alignment', FONT_ALIGNMENTS, FONT_ALIGNMENTS[0]);

    }

}

export = Font;
