import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class FontColor extends BaseSchema {
    public static ID = 'schema.common.FontColor';

    constructor() {

        super();
        this._addPrimitiveProperty("value", DataTypes.STRING, "Value", '#FF000000');
    }
}

export = FontColor;
