/**
 * Created by bardiakhosravi on 2015-12-08.
 */
///<reference path="../../../typings/index.d.ts"/>

import SelectEvent = require('./listener/event/SelectEvent');
import LongSelectEvent = require('./listener/event/LongSelectEvent');
import NavigationAction = require('./listener/action/NavigationAction');
import SigninAction = require('./listener/action/SigninAction');
import SignoutAction = require('./listener/action/SignoutAction');
import PlayBackAction = require('./listener/action/PlaybackAction');
import SaveOfflineAction = require('./listener/action/SaveOfflineAction');
import RemoveOfflineAction = require('./listener/action/RemoveOfflineAction');
import SaveUIDAction = require('./listener/action/SaveUIDAction');
import RemoveUIDAction = require('./listener/action/RemoveUIDAction');
import AnalyticAction = require('./listener/action/AnalyticAction');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import LinkAction = require('./listener/action/LinkAction');

class Listener extends BaseSchema {

    public static ID = 'schema.common.Listener';
	
    constructor() {
        super();
        this._addPrimitiveProperty('active', DataTypes.STRING, 'Active State');
        this._addOneOfRefProperty('event', 'Event', [SelectEvent.ID, LongSelectEvent.ID], ['Select', 'Long Select']);
        this._addOneOfRefProperty('action', 'Action', [
			NavigationAction.ID, SigninAction.ID, SignoutAction.ID, PlayBackAction.ID, 
			SaveOfflineAction.ID, RemoveOfflineAction.ID, SaveUIDAction.ID, RemoveUIDAction.ID, AnalyticAction.ID, LinkAction.ID
		], ['Navigate', 'Signin', 'Signout','Playback', 'Save Offline', 'Remove Offline', 'Save UID', 'Remove UID', 'Analytics', 'Link']);
    }

}

export = Listener;

