import ASPECT_RATIOS = require('../enum/components/AspectRatio');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class StandardAspectRatio extends BaseSchema {
    public static ID = 'schema.appstudio.common.aspectRatio.StandardAspectRatio';

    constructor() {
        super();
        this._addEnumProperty('value', DataTypes.STRING, '', ASPECT_RATIOS, ASPECT_RATIOS[0]);
    }
}

export = StandardAspectRatio;