import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomAspectRatio extends BaseSchema {
    public static ID = 'schema.appstudio.common.aspectRatio.CustomAspectRatio';

    constructor() {
        super();
        this._addPrimitiveProperty('width', DataTypes.NUMBER, 'Width');
        this._addPrimitiveProperty('height', DataTypes.NUMBER, 'Height');
    }
}

export = CustomAspectRatio;