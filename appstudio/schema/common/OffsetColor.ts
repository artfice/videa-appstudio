import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class OffsetColor extends BaseSchema {
    public static ID = 'schema.common.OffsetColor';

    constructor() {

        super();
        this._addPrimitiveProperty('offset', DataTypes.NUMBER, 'Offset', '0.0');
        this._addPrimitiveProperty('value', DataTypes.STRING, 'Value', '#00000000');
    }
}

export = OffsetColor;