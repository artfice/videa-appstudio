import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

import SCALES = require('./enum/components/Scales');

class Scale extends BaseSchema {
    public static ID = 'schema.common.Scale';

    constructor() {
        super();

        this._addEnumProperty('value', DataTypes.STRING, 'Value', SCALES, Scale.getDefaultValue());
    }
    
    public static getDefaultValue(): string {
        return SCALES[0];
    }
}

export = Scale;
