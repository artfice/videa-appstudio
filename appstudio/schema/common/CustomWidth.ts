import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomWidth extends BaseSchema {
    public static ID = 'schema.appstudio.common.CustomWidth';

    constructor() {
        super();
        this._addPrimitiveProperty('value', DataTypes.NUMBER, 'Value');
    }

}

export = CustomWidth;