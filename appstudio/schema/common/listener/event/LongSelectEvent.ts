/**
 * Created by bardiakhosravi on 2015-12-08.
 */

///<reference path="../../../../../typings/index.d.ts"/>

import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class LongSelectEvent extends BaseSchema {

    public static ID = 'schema.common.listener.event.LongSelectEvent';

    constructor() {
        super();
    }

}

export = LongSelectEvent;

