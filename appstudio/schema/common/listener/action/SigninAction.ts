/**
 * Created by bardiakhosravi on 2015-12-08.
 */
import SigninActionData = require("./data/SigninActionData");
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class SigninAction extends BaseSchema {

    public static ID = 'schema.common.listener.action.SigninAction';

    constructor() {
        super();
		this._addRefProperty('data', 'Data', SigninActionData.ID);
    }

}

export = SigninAction;

