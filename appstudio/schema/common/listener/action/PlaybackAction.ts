import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class PlaybackAction extends BaseSchema {

    public static ID = 'schema.common.listener.action.PlaybackAction';

    constructor() {
        super();

        this._addPrimitiveProperty('live', DataTypes.STRING, 'Live Stream', '');
        this._addPrimitiveProperty('title', DataTypes.STRING, 'Video Player Title', '');
        this._addPrimitiveProperty('description', DataTypes.STRING, 'Videa Player Description', '');
        this._addPrimitiveProperty('duration', DataTypes.STRING, 'Content Duration (milliseconds)', '');
        this._addPrimitiveProperty('source', DataTypes.STRING, 'Video Source URL', '');
        this._addEnumProperty('format', DataTypes.STRING, 'Content Format', ['SMIL', 'HLS', 'ISM', 'MPEG-DASH', 'MP4']);
        this._addPrimitiveProperty('closedCaptioningFormat', DataTypes.STRING, 'Closed Captioning Format', '');
        this._addPrimitiveProperty('screenIdToNavigateOnEnd', DataTypes.STRING, 'Screen To Navigate To When Complete', '');
        this._addPrimitiveProperty('startTime', DataTypes.STRING, 'Start Time', '{app.watchHistory.resumePoint}');
        this._addPrimitiveProperty('player', DataTypes.STRING, 'Player', '');
    }

}

export = PlaybackAction;

