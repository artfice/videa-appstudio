import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class LinkAction extends BaseSchema {

    public static ID = 'schema.common.listener.action.LinkAction';

    constructor() {
        super();

        this._addPrimitiveProperty('link', DataTypes.STRING, 'Link');
    }

}

export = LinkAction;

