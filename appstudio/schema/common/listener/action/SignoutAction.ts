/**
 * Created by bardiakhosravi on 2015-12-08.
 */

import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class SignoutAction extends BaseSchema {

    public static ID = 'schema.common.listener.action.SignoutAction';

    constructor() {
        super();
    }

}

export = SignoutAction;

