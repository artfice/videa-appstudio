import OfflineCollection = require('../../collection/OfflineCollection');
import RemoteCollection = require('../../collection/RemoteCollection');
import UIDCollection = require('../../collection/UIDCollection');
import CustomData = require('../../../common/CustomData');

import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class NavigationAction extends BaseSchema {

    public static ID = 'schema.common.listener.action.NavigationAction';

    constructor() {
        super();
        this._addPrimitiveProperty('screenId', DataTypes.STRING, 'Screen ID');
		this._addPrimitiveProperty('configId', DataTypes.STRING, 'Configuration ID');
        this._addOneOfRefProperty('collection', 'Collection Type', [
            OfflineCollection.ID, 
            RemoteCollection.ID, 
            UIDCollection.ID,
            CustomData.ID], ['Offline', 'Remote', 'UID', 'Custom']);
    }

}

export = NavigationAction;

