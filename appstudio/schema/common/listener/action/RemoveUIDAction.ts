/**
 * Created by bardiakhosravi on 2015-12-08.
 */

import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class RemoveUIDAction extends BaseSchema {

    public static ID = 'schema.common.listener.action.RemoveUIDAction';

    constructor() {
        super();
		this._addPrimitiveProperty('path', DataTypes.STRING, 'UID Table Name', '');
    }

}

export = RemoveUIDAction;

