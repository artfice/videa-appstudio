import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class SigninActionData extends BaseSchema {

	public static ID = 'schema.common.listener.action.data.SigninActionData';

	constructor() {
		super();
		this._addPrimitiveProperty('username', DataTypes.STRING, 'Username', null, 'username');
		this._addPrimitiveProperty('password', DataTypes.STRING, 'Password', null, 'password');
	}

}

export = SigninActionData;

