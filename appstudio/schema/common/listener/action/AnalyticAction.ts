import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import BaseReference = require('../../BaseReference');

class AnalyticAction extends BaseSchema {

  public static ID = 'schema.common.listener.action.AnalyticAction';

  constructor() {
    super();

    this._addDynamicRefProperty('event', 'Event', BaseReference.ID, 
    'v1/dynamicEnum/accounts/{accountId}/uiConfigs/{configId}/analytics');
  }

}

export = AnalyticAction;