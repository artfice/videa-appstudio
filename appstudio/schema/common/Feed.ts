import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Range = require('./Range');


class Feed extends BaseSchema {
    public static ID = 'schema.appstudio.common.Feed';

    constructor() {
        super();

        this._addPrimitiveProperty('sourceUrl', DataTypes.STRING, 'Source Url', '');
        this._addRefProperty('range', 'Range', Range.ID);

        //TODO: provider has been moved from DataViewData to here because it makes more sense. 
        // BaseDataViewTranslator should be updated to reflect this
        this._addPrimitiveProperty('provider', DataTypes.STRING, 'Provider', '');

    }
}

export = Feed;
