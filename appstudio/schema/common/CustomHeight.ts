import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomHeight extends BaseSchema {
    public static ID = 'schema.appstudio.common.CustomHeight';

    constructor() {
        super();
        this._addPrimitiveProperty('value', DataTypes.NUMBER, 'Value');
    }

}

export = CustomHeight;