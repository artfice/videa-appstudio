import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

import * as POSITIONS from './enum/components/Positions';

class Position extends BaseSchema {
    public static ID = 'schema.common.Position';

    constructor() {
        super();

        this._addEnumProperty('value', DataTypes.STRING, 'Value', POSITIONS, Position.getDefaultValue());
    }

    public static getDefaultValue(): string {
        return POSITIONS[1];
    }
}

export = Position;
