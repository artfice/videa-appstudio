/**
 * Created by bardiakhosravi on 2015-11-19.
 */

import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Color extends BaseSchema {
    public static ID = 'schema.common.Color';

    constructor() {

        super();
        this._addPrimitiveProperty("value", DataTypes.STRING, "Value", '#00000000');
    }
}

export = Color;
