import BaseFont = require('./BaseFont');

class SanFrancisco extends BaseFont {

    public static ID = 'schema.common.font.SanFrancisco';

    constructor() {
        super();
    }

    protected _getWeights () : string[] {
        return ['Regular', 'Bold', 'BoldItalic', 'Heavy', 'HeavyItalic', 'Light', 
        'LightItalic', 'Medium', 'MediumItalic', 'RegularItalic', 
        'Semibold', 'SemiboldItalic'];
    }
}

export = SanFrancisco;