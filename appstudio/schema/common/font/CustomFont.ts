import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

import FontColor = require('../FontColor');
import Alignment = require('../Alignment');

class CustomFont extends BaseSchema {

    public static ID = 'schema.common.font.CustomFont';

    constructor() {
        super();

        this._addPrimitiveProperty('family', DataTypes.STRING, 'Font Family', '');

        this._addPrimitiveProperty('size', DataTypes.NUMBER, 'Font Size', 12);

        this._addRefProperty('color', 'Color', FontColor.ID);

        this._addRefProperty('alignment', 'Alignment', Alignment.ID);
    }
}

export = CustomFont;
