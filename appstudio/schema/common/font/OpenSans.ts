import BaseFont = require('./BaseFont');

class OpenSans extends BaseFont {

    public static ID = 'schema.common.font.OpenSans';

    constructor() {
        super();
    }

    protected _getWeights () : string[] {
        return ['Regular', 'Bold', 'BoldItalic', 'ExtraBold', 'ExtraBoldItalic', 'Italic', 
        'Light', 'LightItalic', 'Semibold','SemiboldItalic'];
    }
}

export = OpenSans;