import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import FontColor = require('../FontColor');
import Alignment = require('../Alignment');

class BaseFont extends BaseSchema {

    public static ID = 'schema.common.BaseFont';

    constructor() {
        super();

        this._addPrimitiveProperty('size', DataTypes.NUMBER, 'Font Size', 12);

        this._addEnumProperty('weight', DataTypes.STRING, 'Font Weight', this._getWeights(), this._getWeights()[0]);

        this._addRefProperty('color', 'Color', FontColor.ID);

        this._addRefProperty('alignment', 'Alignment', Alignment.ID);
    }

    protected _getWeights(): string[] {
        return ['Regular'];
    }
}

export = BaseFont;