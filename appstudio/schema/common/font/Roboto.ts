import BaseFont = require('./BaseFont');

class Roboto extends BaseFont {

    public static ID = 'schema.common.font.Roboto';

    constructor() {
        super();
    }

    protected _getWeights () : string[] {
        return ['Regular', 'Black', 'BlackItalic', 'Bold', 'BoldItalic', 'Italic', 'Light', 
        'LightItalic', 'Medium', 'MediumItalic', 'Thin', 'ThinItalic'];
    }
}

export = Roboto;