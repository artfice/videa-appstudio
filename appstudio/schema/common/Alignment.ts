import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

import * as FONT_ALIGNMENTS from './enum/fonts/Alignments';

class Alignment extends BaseSchema {
    public static ID = 'schema.common.Alignment';

    constructor() {
        super();

        this._addEnumProperty('value', DataTypes.STRING, 'Value', FONT_ALIGNMENTS, Alignment.getDefaultValue());
    }
    
    public static getDefaultValue(): string {
        return FONT_ALIGNMENTS[0];
    }
}

export = Alignment;
