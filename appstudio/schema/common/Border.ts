import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import Color = require('../common/Color');


class Border extends BaseSchema {

    public static ID = 'schema.common.Border';

    constructor() {
        super();
        this._addPrimitiveProperty('radius', DataTypes.NUMBER, 'Radius', 0);
        this._addPrimitiveProperty('thickness', DataTypes.NUMBER, 'Thickness', 0);
        this._addRefProperty('color', 'Color', Color.ID);
    }    
}

export = Border;