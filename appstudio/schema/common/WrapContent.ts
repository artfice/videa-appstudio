import { BaseSchema } from 'videa-framework/schema/BaseSchema';
 
 class WrapContent extends BaseSchema {
     public static ID = 'schema.appstudio.common.WrapContent';
 
     constructor() {
         super();
     }
 
 }
 
 export = WrapContent; 