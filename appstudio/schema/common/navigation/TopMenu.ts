/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import Menu = require('./Menu');
import TopMenuSection = require('../../appstudio/app/configuration/uiconfig/navigation/TopMenuSection');
import TopMenuStyle = require('../../appstudio/app/configuration/uiconfig/navigation/style/TopMenuStyle');

class TopMenu extends Menu {

    public static ID = 'schema.common.navigation.TopMenu';

    constructor() {
        super();
		this._addRefProperty('style', 'Style', TopMenuStyle.ID);
		this._addRefProperty('leftSection', 'Left Section', TopMenuSection.ID);
		this._addRefProperty('middleSection', 'Center Section', TopMenuSection.ID);
		this._addRefProperty('rightSection', 'Right Section', TopMenuSection.ID);
    }
}

export = TopMenu;
