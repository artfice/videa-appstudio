///<reference path="../../../../typings/index.d.ts"/>

import Menu = require('./Menu');

class BlankMenu extends Menu {

    public static ID = 'schema.common.navigation.BlankMenu';

    constructor() {
        super();
    }
}

export = BlankMenu;
