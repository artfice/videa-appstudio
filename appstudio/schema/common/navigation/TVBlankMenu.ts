import Menu = require('./Menu');

class TVBlankMenu extends Menu {

    public static ID = 'schema.common.navigation.TVBlankMenu';

    constructor() {
        super();
    }
}

export = TVBlankMenu;