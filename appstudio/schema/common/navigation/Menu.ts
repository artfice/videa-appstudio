/**
 * Created by bardiakhosravi on 2016-02-26.
 */


///<reference path="../../../../typings/index.d.ts"/>

import { BaseSchema } from 'videa-framework/schema/BaseSchema';


class Menu extends BaseSchema {

    public static ID = 'schema.common.navigation.Menu';

    constructor() {
        super();
    }
}

export = Menu;
