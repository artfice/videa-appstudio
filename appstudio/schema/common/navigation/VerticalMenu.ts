/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import Menu = require('./Menu');

class VerticalMenu extends Menu {

    public static ID = 'schema.common.navigation.VerticalMenu';

    constructor() {
        super();
    }
}

export = VerticalMenu;
