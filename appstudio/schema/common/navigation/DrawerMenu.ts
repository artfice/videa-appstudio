/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import Menu = require('./Menu');

import Section = require("../../appstudio/app/configuration/uiconfig/navigation/Section");
import SecondaryMenu = require("../../appstudio/app/configuration/uiconfig/navigation/SecondaryMenu");

class DrawerMenu extends Menu {

    public static ID = 'schema.common.navigation.DrawerMenu';

    constructor() {
        super();
        this._addArrayProperty('section', 'Sections', Section.ID);
		this._addRefProperty('secondaryMenu', 'Secondary Menu', SecondaryMenu.ID);        
    }
}

export = DrawerMenu;
