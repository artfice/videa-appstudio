/**
 * Created by bardiakhosravi on 2015-12-09.
 */

import Menu = require('./Menu');

import Tab = require("../../appstudio/app/configuration/uiconfig/navigation/Tab");
import BottomMenuStyle = require("../../appstudio/app/configuration/uiconfig/navigation/style/BottomMenuStyle");

class BottomMenu extends Menu {

    public static ID = 'schema.common.navigation.BottomMenu';

    constructor() {
        super();
        this._addArrayProperty('tab', 'Tabs', Tab.ID);
        this._addRefProperty('style', 'Style', BottomMenuStyle.ID);
    }
}

export = BottomMenu;
