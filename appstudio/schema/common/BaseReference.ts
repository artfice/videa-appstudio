import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BaseReference extends BaseSchema {

    public static ID = 'schema.common.BaseReference';

    constructor() {
        super();
        this._addPrimitiveProperty('value', DataTypes.STRING, 'value', '');
        this._addPrimitiveProperty('refId', DataTypes.STRING, 'Reference', '');
    }    

    protected _getDefaultDisplayField(): string {
        return 'value';
    }    
}

export = BaseReference;