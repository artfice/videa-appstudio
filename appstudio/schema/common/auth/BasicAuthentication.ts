import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import PROVIDERS = require('../../common/enum/components/Providers');
import METHODS = require('../../common/enum/components/Methods');


class BasicAuthentication extends BaseSchema {
    public static ID = 'schema.common.auth.BasicAuthentication';

    constructor() {
        super();
        this._addEnumProperty('provider', DataTypes.STRING, 'Provider', PROVIDERS, PROVIDERS[0]);
        this._addPrimitiveProperty('required', DataTypes.BOOLEAN, 'Required', true);
		this._addEnumProperty('loginMethod', DataTypes.STRING, 'Login Url Method', METHODS, METHODS[1]);
		this._addPrimitiveProperty('loginUrl', DataTypes.STRING, 'Login Url');
		this._addEnumProperty('logoutMethod', DataTypes.STRING, 'Logout Url Method', METHODS, METHODS[1]);
		this._addPrimitiveProperty('logoutUrl', DataTypes.STRING, 'Logout Url');        
        this._addPrimitiveProperty('loginScreenId', DataTypes.STRING, 'Login Screen');
        this._addPrimitiveProperty('loginConfigId', DataTypes.STRING, 'Login Config');
        this._addPrimitiveProperty('defaultScreenId', DataTypes.STRING, 'Default Screen');
        this._addPrimitiveProperty('defaultConfigId', DataTypes.STRING, 'Default Config');
    }

}

export = BasicAuthentication;