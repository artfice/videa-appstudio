import RedirectTarget = require('../../../common/auth/akamai/redirect/Target');
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class Redirect extends BaseSchema {
	public static ID = 'schema.common.auth.akamai.Redirect';

	constructor() {
		super();
        this._addRefProperty('web', 'Web', RedirectTarget.ID);
        this._addRefProperty('iOS', 'iOS', RedirectTarget.ID);
        this._addRefProperty('android', 'Android', RedirectTarget.ID);
	}

}

export = Redirect;