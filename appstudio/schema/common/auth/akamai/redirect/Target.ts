import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Target extends BaseSchema {
	public static ID = 'schema.common.auth.akamai.redirect.Target';

	constructor() {
		super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'name', 'prod');
        this._addPrimitiveProperty('target', DataTypes.STRING, 'target', '');
	}

}

export = Target;
