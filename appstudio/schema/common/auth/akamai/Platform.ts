import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Platform extends BaseSchema {
    public static ID = 'schema.common.auth.akamai.Platform';

    constructor() {
        super();
        this._addPrimitiveProperty('iOS', DataTypes.STRING, 'iOS', '');
        this._addPrimitiveProperty('android', DataTypes.STRING, 'Android', '');
        this._addPrimitiveProperty('web', DataTypes.STRING, 'Web', '');
    }

}

export = Platform;