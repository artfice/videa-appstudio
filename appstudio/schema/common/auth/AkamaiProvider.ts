/**
 * Created by bardiakhosravi on 2016-01-15.
 */
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';
import AkamaiPlatform = require('./akamai/Platform');
import AkamaiRedirect = require('./akamai/Redirect');
import CustomData = require('../CustomData');


class AkamaiProvider extends BaseSchema {
    public static ID = 'schema.common.auth.AkamaiProvider';

    constructor() {
        super();
        this._addPrimitiveProperty('akamaiIDP', DataTypes.STRING, 'Akamai IDP', '');
        this._addRefProperty('platformID', 'Platform ID', AkamaiPlatform.ID);
        this._addRefProperty('redirect', 'Redirects', AkamaiRedirect.ID);
        this._addRefProperty('resourceAccessCodes', 'Resource Access Codes', CustomData.ID);
    }

}

export = AkamaiProvider;
