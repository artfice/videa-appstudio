/**
 * Created by bardiakhosravi on 2016-01-15.
 */
import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class NoneProvider extends BaseSchema {
	public static ID = 'schema.common.auth.NoneProvider';

	constructor() {
		super();
	}
	
	protected _getTitle() {
		return 'None';
	}
}

export = NoneProvider;
