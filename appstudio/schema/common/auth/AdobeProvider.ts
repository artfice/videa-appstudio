/**
 * Created by bardiakhosravi on 2016-01-15.
 */
import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class AdobeProvider extends BaseSchema {
	public static ID = 'schema.common.auth.AdobeProvider';

	constructor() {
		super();
        this._addPrimitiveProperty('accountId', DataTypes.STRING, 'Account ID', '');
	}

	protected _getTitle() {
		return 'Adobe Provider';
	}
}

export = AdobeProvider;
