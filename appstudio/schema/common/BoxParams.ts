import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BoxParams extends BaseSchema {

    public static ID = 'schema.common.BoxParams';

    constructor() {
        super();
        this._addPrimitiveProperty('top', DataTypes.STRING, 'Top', '0');
        this._addPrimitiveProperty('right', DataTypes.STRING, 'Right', '0');
        this._addPrimitiveProperty('bottom', DataTypes.STRING, 'Bottom', '0');
        this._addPrimitiveProperty('left', DataTypes.STRING, 'Left', '0');
    }
}

export = BoxParams;