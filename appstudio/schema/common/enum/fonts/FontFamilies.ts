/**
 * Created by bardiakhosravi on 2016-02-24.
 */

var FONT_FAMILY: string[] = ['GothamHTF-Book', 'Comic Sans', 'Roboto-Regular', 'Verdana', 'GothamHTF-Bold'];

export = FONT_FAMILY;
