/**
 * Created by bardiakhosravi on 2016-02-24.
 */
var ALIGNMENTS: string[] = ['left', 'center', 'right'];

export = ALIGNMENTS;
