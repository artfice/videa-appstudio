/**
 * Created by bardiakhosravi on 2016-02-24.
 */

var GRAVITIES: string[] = ['top', 'bottom', 'left', 'right', 'center'];

export = GRAVITIES;
