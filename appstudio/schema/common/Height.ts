import { BaseSchema } from 'videa-framework/schema/BaseSchema';

import WrapContent = require('./WrapContent');
import CustomHeight = require('./CustomHeight');
import MatchParent = require('./MatchParent');

class Height extends BaseSchema {

    public static ID = 'schema.appstudio.common.Height';

    constructor() {
        super();
        this._addOneOfRefProperty('value', 'Value', [
            WrapContent.ID, MatchParent.ID, CustomHeight.ID], [
                'Wrap Content', 'Match Parent', 'Custom Height'
            ], this._getDefaultHeight());
    }
    
    protected _getDefaultHeight() : string {
        return WrapContent.ID;      
    }    
}

export = Height;