import { BaseSchema } from 'videa-framework/schema/BaseSchema';
 
 class MatchParent extends BaseSchema {
     public static ID = 'schema.appstudio.common.MatchParent';
 
     constructor() {
         super();
     }
 
 }
 
 export = MatchParent; 