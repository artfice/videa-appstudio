import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomData extends BaseSchema {
    public static ID = 'schema.common.CustomData';

    constructor() {
        super();
        this._addPrimitiveProperty('content', DataTypes.STRING, 'Data', '{}');
    }

}

export = CustomData;