import Cms = require('../../appstudio/app/configuration/Cms');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class GenericCms extends Cms {
	public static ID = 'schema.common.cms.GenericCms';

	constructor() {
		super();
        this._addPrimitiveProperty('uid', DataTypes.STRING, 'UID', '');
        this._addPrimitiveProperty('delimiter', DataTypes.STRING, 'Delimiter', '');
	}

}

export = GenericCms;
       