import Cms = require('../../appstudio/app/configuration/Cms');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class Mpx extends Cms {
	public static ID = 'schema.common.cms.Mpx';

	constructor() {
		super();
        
        this._addPrimitiveProperty('PID', DataTypes.STRING, 'PID', '');
        this._addPrimitiveProperty('accountID', DataTypes.NUMBER, 'Account ID', 0);
        this._addPrimitiveProperty('uid', DataTypes.STRING, 'UID', '{model.guid}');
        this._addPrimitiveProperty('delimiter', DataTypes.STRING, 'Delimiter', '|');
	}

}

export = Mpx;
