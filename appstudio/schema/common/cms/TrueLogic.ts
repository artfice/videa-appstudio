import Cms = require('../../appstudio/app/configuration/Cms');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class TrueLogic extends Cms {
	public static ID = 'schema.common.cms.TrueLogic';

	constructor() {
		super();
        this._addPrimitiveProperty('uid', DataTypes.STRING, 'UID');
        this._addPrimitiveProperty('delimiter', DataTypes.STRING, 'Delimiter');
        this._addPrimitiveProperty('requestToken', DataTypes.STRING, 'Request Token');
	}
}

export = TrueLogic;
