import Cms = require('../../appstudio/app/configuration/Cms');
import { DataTypes } from 'videa-framework/schema/DataTypes';


// TODO: Move cms schema to common https://digiflare.atlassian.net/browse/VAS-1162
class AkamaiVOC extends Cms {
    public static ID = 'schema.common.cms.AkamaiVOC';

    constructor() {
        super();
        
        this._addPrimitiveProperty('uid', DataTypes.STRING, 'UID', '{model.guid}');
        this._addPrimitiveProperty('delimiter', DataTypes.STRING, 'Delimiter', '|');
        this._addPrimitiveProperty('apiKey', DataTypes.STRING, 'Api Key');
        this._addPrimitiveProperty('serverIp', DataTypes.STRING, 'Server IP');
        this._addPrimitiveProperty('networkPreference', DataTypes.STRING, 'Network Preference');
        this._addPrimitiveProperty('contentRelevance', DataTypes.STRING, 'Content Relevance');
        this._addPrimitiveProperty('prefetchLimit', DataTypes.NUMBER, 'Prefetch Limit');
        this._addPrimitiveProperty('individualFileLimit', DataTypes.NUMBER, 'Individual File Limit');
        this._addPrimitiveProperty('minBatteryLevelForPrefetch', DataTypes.NUMBER, 'Minumum Battery Level for Prefetch');
        this._addPrimitiveProperty('enableBackgroundDownloads', DataTypes.BOOLEAN, 'Enable Background Downloads');
        this._addPrimitiveProperty('subscribedProviders', DataTypes.STRING, 'Subscribed Providers');
        this._addPrimitiveProperty('subscribedCategories', DataTypes.STRING, 'Subscribed Categories');
    }
}

export = AkamaiVOC;
