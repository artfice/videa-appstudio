import Cms = require('../../appstudio/app/configuration/Cms');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class VimondCms extends Cms {
	public static ID = 'schema.common.cms.VimondCms';

	constructor() {
		super();
		this._addPrimitiveProperty('uid', DataTypes.STRING, 'UID', '{model.guid}');
		this._addPrimitiveProperty('delimiter', DataTypes.STRING, 'Delimiter', '|');
	}

}

export = VimondCms;
