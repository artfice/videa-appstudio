import Cms = require('../../appstudio/app/configuration/Cms');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class VideaCms extends Cms {
	public static ID = 'schema.common.cms.VideaCms';

	constructor() {
		super();
        this._addPrimitiveProperty('PID', DataTypes.STRING, 'PID', '');
        this._addPrimitiveProperty('accountID', DataTypes.NUMBER, 'Account ID', 0);
	}

}

export = VideaCms;
