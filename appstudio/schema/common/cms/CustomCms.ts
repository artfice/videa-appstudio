import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomCms extends BaseSchema {
    public static ID = 'schema.common.cms.CustomCms';

    constructor() {
		super();
        this._addPrimitiveProperty('name', DataTypes.STRING, 'Name', '');
        this._addPrimitiveProperty('content', DataTypes.STRING, '', '{}');
    }
}

export = CustomCms;
