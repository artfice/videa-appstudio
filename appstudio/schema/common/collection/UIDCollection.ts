import { DataTypes } from 'videa-framework/schema/DataTypes';
import LinkCollection = require('./LinkCollection');


class UIDCollection extends LinkCollection {
    public static ID = 'schema.common.collection.UIDCollection';
     
    constructor() {
        super();
        this._addPrimitiveProperty('path', DataTypes.STRING, 'Path', '');
    }
}

export = UIDCollection;
