import Range = require('../Range');
import LinkCollection = require('./LinkCollection');


class RemoteCollection extends LinkCollection {
    public static ID = 'schema.common.collection.RemoteCollection';
     
    constructor() {
        super();
        this._addRefProperty('range', 'Range', Range.ID);
    }
}

export = RemoteCollection;
