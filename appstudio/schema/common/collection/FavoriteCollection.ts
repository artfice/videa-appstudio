import AppProvider = require('./provider/AppProvider');
import BaseCollection = require('./BaseCollection');
import Range = require('../Range');

class FavoriteCollection extends BaseCollection {
    public static ID = 'schema.common.collection.FavoriteCollection';
     
    constructor() {
        super();
        this._addRefProperty('range', 'Range', Range.ID);
    }

    protected _getProvider() {
        return [AppProvider.ID];
    }

    protected _getProviderTitle() {
        return ['Default'];
    }

    protected _getDefaultProvider() {
        return AppProvider.ID;
    }    
}

export = FavoriteCollection;
