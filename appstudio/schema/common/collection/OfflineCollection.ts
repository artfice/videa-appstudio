import BaseCollection = require('./BaseCollection');

class OfflineCollection extends BaseCollection {
    public static ID = 'schema.common.collection.OfflineCollection';
     
    constructor() {
        super();
    }
}

export = OfflineCollection;
