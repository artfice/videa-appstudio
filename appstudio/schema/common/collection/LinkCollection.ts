import BaseCollection = require('./BaseCollection');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class LinkCollection extends BaseCollection {
    public static ID = 'schema.common.collection.LinkCollection';
     
    constructor() {
        super();
        this._addPrimitiveProperty('link', DataTypes.STRING, 'Source Url', '');
    }
}

export = LinkCollection;
