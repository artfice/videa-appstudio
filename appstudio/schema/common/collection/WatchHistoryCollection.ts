import { DataTypes } from 'videa-framework/schema/DataTypes';
import BaseCollection = require('./BaseCollection');
import Range = require('../Range');
import AppProvider = require('./provider/AppProvider');

class WatchHistoryCollection extends BaseCollection {
    public static ID = 'schema.common.collection.WatchHistoryCollection';

    constructor() {
        super();
        this._addPrimitiveProperty('group', DataTypes.STRING, 'group', '');
        this._addRefProperty('range', 'Range', Range.ID);
    }

    protected _getProvider() {
        return [AppProvider.ID];
    }

    protected _getProviderTitle() {
        return ['Default'];
    }

    protected _getDefaultProvider() {
        return AppProvider.ID;
    }
}

export = WatchHistoryCollection;