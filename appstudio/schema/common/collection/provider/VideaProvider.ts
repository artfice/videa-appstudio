import BaseProvider = require('./BaseProvider');

class VideaProvider extends BaseProvider {
    public static ID = 'schema.common.collection.provider.VideaProvider';
     
    constructor() {
        super();
    }
    
    protected _getCollectionPath() : string {
        return 'data';
    }    
}

export = VideaProvider;
