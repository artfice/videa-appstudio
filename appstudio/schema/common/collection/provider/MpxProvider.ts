import BaseProvider = require('./BaseProvider');

class MpxProvider extends BaseProvider {
    public static ID = 'schema.common.collection.provider.MpxProvider';

    constructor() {
        super();
    }

    protected _getCollectionPath(): string {
        return 'entries';
    }
}

export = MpxProvider;
