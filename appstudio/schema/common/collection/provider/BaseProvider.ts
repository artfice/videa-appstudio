import { BaseSchema } from 'videa-framework/schema/BaseSchema';
import { DataTypes } from 'videa-framework/schema/DataTypes';

class BaseProvider extends BaseSchema {
    public static ID = 'schema.common.collection.provider.BaseProvider';

    constructor() {
        super();
        this._addPrimitiveProperty('collectionPath', DataTypes.STRING, 'Collection Path', this._getCollectionPath());
    }

    protected _getCollectionPath(): string {
        return '';
    }
}

export = BaseProvider;
