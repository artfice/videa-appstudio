import BaseProvider = require('./BaseProvider');

class VimondProvider extends BaseProvider {
    public static ID = 'schema.common.collection.provider.VimondProvider';
     
    constructor() {
        super();
    }
    
    protected _getCollectionPath() : string {
        return '';
    }    
}

export = VimondProvider;
