import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class AppProvider extends BaseSchema {
    public static ID = 'schema.common.collection.provider.AppProvider';

    constructor() {
        super();
    }
}

export = AppProvider;
