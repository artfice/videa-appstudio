import BaseProvider = require('./BaseProvider');

class JsonProvider extends BaseProvider {
    public static ID = 'schema.common.collection.provider.JsonProvider';

    constructor() {
        super();
    }

    protected _getCollectionPath(): string {
        return '';
    }
}

export = JsonProvider;
