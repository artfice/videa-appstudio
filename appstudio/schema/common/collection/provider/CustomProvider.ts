import BaseProvider = require('./BaseProvider');
import { DataTypes } from 'videa-framework/schema/DataTypes';

class CustomProvider extends BaseProvider {
    public static ID = 'schema.common.collection.provider.CustomProvider';

    constructor() {
        super();
        this._addPrimitiveProperty('customProvider', DataTypes.STRING, 'Custom Provider', '');
    }

    protected _getCollectionPath(): string {
        return '';
    }
}

export = CustomProvider;