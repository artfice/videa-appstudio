import { BaseSchema } from 'videa-framework/schema/BaseSchema';

import MpxProvider = require('./provider/MpxProvider');
import VideaProvider = require('./provider/VideaProvider');
import JsonProvider = require('./provider/JsonProvider');
import BaseProvider = require('./provider/BaseProvider');
import CustomProvider = require('./provider/CustomProvider');
import VimondProvider = require('./provider/VimondProvider');

class BaseCollection extends BaseSchema {
    public static ID = 'schema.common.collection.BaseCollection';

    constructor() {
        super();
        this._addOneOfRefProperty('provider',
            'Provider',
            this._getProvider(),
            this._getProviderTitle(), BaseProvider.ID, this._getDefaultProvider());
    }

    protected _getProvider() {
        return [MpxProvider.ID, VideaProvider.ID, JsonProvider.ID,
        CustomProvider.ID, VimondProvider.ID];
    }

    protected _getProviderTitle() {
        return ['MPX', 'Videa', 'Json', 'Custom', 'Vimond'];
    }

    protected _getDefaultProvider () {
        return undefined;
    }
}

export = BaseCollection;
