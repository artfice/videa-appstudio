import { BaseSchema } from 'videa-framework/schema/BaseSchema';

class NoData extends BaseSchema {
    public static ID = 'schema.common.NoData';

    constructor() {
        super();
    }
}

export = NoData;