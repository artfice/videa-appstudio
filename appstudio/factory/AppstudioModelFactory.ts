import { MetadataOwner as MetadataOwnerDto } from '../dto/MetadataOwner';

import uuid = require('node-uuid');

import MpxSchema = require('../schema/common/cms/Mpx');
import VideaCmsSchema = require('../schema/common/cms/VideaCms');

import MobileMPXSchema = require('../schema/appstudio/app/mobile/cms/MobileMpx');
import TabletMPXSchema = require('../schema/appstudio/app/tablet/cms/TabletMpx');
import TVMPXSchema = require('../schema/appstudio/app/tv/cms/TvMpx');

import MobileVideaCmsSchema = require('../schema/appstudio/app/mobile/cms/MobileVideaCms');
import TabletVideaCmsSchema = require('../schema/appstudio/app/tablet/cms/TabletVideaCms');
import TvVideaCmsSchema = require('../schema/appstudio/app/tv/cms/TvVideaCms');

import CarouselControlsSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/CarouselControls');
import ComponentControlsSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/ComponentControls');
import ComponentStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/style/ComponentStyle');
import ComponentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/Component');

import SizedComponentStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/style/SizedComponentStyle');

import FontSchema = require('../schema/common/Font');
import ColorSchema = require('../schema/common/Color');

import ImageContentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/content/ImageContent');

import FieldContentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/FieldContent');

import LabelContentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/content/LabelContent');

import BaseFieldStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/BaseFieldStyle');

import LabelStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/label/LabelStyle');

import ImageStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/image/ImageStyle');

import BaseFieldSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/BaseField');

import LabelSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/label/Label');

import ImageSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/image/Image');

import NamedComponentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/NamedComponent');
import BaseCollectionSchema = require('../schema/common/collection/BaseCollection');

import CarouselStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/CarouselStyle');

import DataViewTileStyleSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/tile/DataViewTileStyle');

import CollectionPickerStyleSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/collectionview/picker/PickerStyle');
import { DataViewTileSettings as DataViewTileSettingsDto } from '../dto/app/configuration/uiconfig/screen/dataview/tile/DataViewTileSettings';

import DataViewHeadingSettingsSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/heading/DataViewHeadingSettings');
import DataViewHeadingStyleSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/heading/DataViewHeadingStyle');
import DataViewTileSettingsSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/tile/DataViewTileSettings');
import DataViewHeadingSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/DataViewHeading');
import DataViewTileSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/DataViewTile');

import GridViewSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/GridView');
import GridViewStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/GridViewStyle');

import ListViewSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/listview/ListView');

import SwimlaneSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/gridview/swimlane/Swimlane');

import CarouselSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/Carousel');
import CarouselTileSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/carousel/CarouselTile');

import CollectionViewSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/collectionview/CollectionsView');
import CollectionViewPickerContentSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/collectionview/picker/PickerContent');
import CollectionPickerSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/collectionview/picker/CollectionPickerSettings');

import DataViewContentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/dataview/DataViewContent');

import WatchHistorySchema = require('../schema/appstudio/app/configuration/WatchHistory');

import NoWatchHistoryProviderSchema = require('../schema/appstudio/app/configuration/uiconfig/watchhistory/NoneProvider');
import LocalProviderSchema = require('../schema/appstudio/app/configuration/uiconfig/watchhistory/LocalProvider');
import MpxProviderSchema = require('../schema/appstudio/app/configuration/uiconfig/watchhistory/MpxProvider');

import ChromecastSchema = require('../schema/appstudio/app/configuration/Chromecast');

import ChromecastTitleSchema = require('../schema/appstudio/app/configuration/chromecast/Title');

import ChromecastThemeSchema = require('../schema/appstudio/app/configuration/chromecast/Theme');

import ChromecastReceiverSchema = require('../schema/appstudio/app/configuration/chromecast/Receiver');

import ToggleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/toggle/Toggle');

import ToggleStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/toggle/ToggleStyle');

import ColoredComponentStyleSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/style/ColoredComponentStyle');

import TextFieldSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/textfield/TextField');

import TextFieldStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/textfield/TextFieldStyle');

import TextFieldContentSchema =
require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/content/TextFieldContent');

import AlignmentSchema = require('../schema/common/Alignment');

import PositionSchema = require('../schema/common/Position');

import ScaleSchema = require('../schema/common/Scale');

import UIConfigSchema = require('../schema/appstudio/app/configuration/UIConfig');

import AppSchema = require('../schema/appstudio/App');
import MobileAppSchema = require('../schema/appstudio/MobileApp');
import TabletAppSchema = require('../schema/appstudio/TabletApp');
import TVAppSchema = require('../schema/appstudio/TVApp');

import BrandSchema = require('../schema/appstudio/Brand');

import ScreenSchema = require('../schema/appstudio/app/configuration/uiconfig/Screen');

import ImageSectionSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/section/Image');
import ImageSettingsSectionSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/section/image/ImageSettings');

import LabelSectionSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/section/Label');
import LabelSettingsSectionSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/section/label/LabelSettings');

import SectionSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/Section');

import SectionSettingsSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/section/SectionSettings');

import SectionStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/section/style/SectionStyle');

import SecondaryMenuSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/SecondaryMenu');

import SecondaryMenuSectionSettingsSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/secondarymenu/section/SecondaryMenuSectionSettings');

import SecondaryMenuSectionSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSection');

import SecondaryMenuSettingsSchema =
require('../schema/appstudio/app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSettings');

import SecondaryMenuStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/secondarymenu/style/SecondaryMenuStyle');

import TopMenuSchema = require('../schema/common/navigation/TopMenu');
import DrawerMenuSchema = require('../schema/common/navigation/DrawerMenu');
import VerticalMenuSchema = require('../schema/common/navigation/VerticalMenu');
import TabletNavigationSchema = require('../schema/appstudio/app/tablet/uiconfig/navigation/TabletNavigation');
import MobileNavigationSchema = require('../schema/appstudio/app/mobile/uiconfig/navigation/MobileNavigation');
import TVNavigationSchema = require('../schema/appstudio/app/tv/uiconfig/navigation/TVNavigation');
import AppStudioNavigationSchema = require('../schema/appstudio/app/configuration/uiconfig/Navigation');

import AdvanceComponentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/AdvanceComponent');

import JsonCollectionProviderSchema = require('../schema/common/collection/provider/JsonProvider');
import MpxCollectionProviderSchema = require('../schema/common/collection/provider/MpxProvider');
import VideaCollectionProviderSchema = require('../schema/common/collection/provider/VideaProvider');

import AuthenticationSchema = require('../schema/appstudio/app/configuration/Authentication');
import CustomDataSchema = require('../schema/common/CustomData');

import GemiusAnalyticSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/gemius/GemiusAnalytics');

import MobileAppEdition = require('../schema/appstudio/app/mobile/MobileAppEdition');
import TabletAppEdition = require('../schema/appstudio/app/tablet/TabletAppEdition');
import TVAppEdition = require('../schema/appstudio/app/tv/TVAppEdition');
import ExternalEdition = require('../schema/appstudio/app/ExternalEdition');
import MobileUIConfig = require('../schema/appstudio/app/mobile/uiconfig/MobileUIConfig');
import TabletUIConfig = require('../schema/appstudio/app/tablet/uiconfig/TabletUIConfig');
import TVUIConfig = require('../schema/appstudio/app/tv/uiconfig/TVUIConfig');
import ExternalUIConfig = require('../schema/appstudio/app/configuration/ExternalConfig');
import MobileScreenSchema = require('../schema/appstudio/app/mobile/uiconfig/screen/MobileScreen');
import TabletScreenSchema = require('../schema/appstudio/app/tablet/uiconfig/screen/TabletScreen');
import TVScreenSchema = require('../schema/appstudio/app/tv/uiconfig/screen/TvScreen');

import CustomCollectionProviderSchema = require('../schema/common/collection/provider/CustomProvider');
import StandardAspectRatioSchema = require('../schema/common/aspectRatio/StandardAspectRatio');
import CustomAspectRatioSchema = require('../schema/common/aspectRatio/CustomAspectRatio');

import ProgressBarSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/progressBar/ProgressBar');
import ProgressBarStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/field/progressBar/ProgressBarStyle');

import NoDataSchema = require('../schema/common/NoData');
import FavoritesSchema = require('../schema/appstudio/app/configuration/Favorites');
import FavoritesLocalProviderSchema = require('../schema/appstudio/app/configuration/favorites/LocalProvider');

import UserSchema = require('../schema/appstudio/app/configuration/User');
import UserMpxProviderSchema = require('../schema/appstudio/app/configuration/user/MpxProvider');
import UserVideaProviderSchema = require('../schema/appstudio/app/configuration/user/VideaProvider');

import BaseReferenceSchema = require('../schema/common/BaseReference');

import WrapContentSchema = require('../schema/common/WrapContent');
import WidthSchema = require('../schema/common/Width');
import HeightSchema = require('../schema/common/Height');
import MatchParentSchema = require('../schema/common/MatchParent');
import CustomHeightSchema = require('../schema/common/CustomHeight');
import CustomWidthSchema = require('../schema/common/CustomWidth');

import GradientColorSchema = require('../schema/common/GradientColor');
import OffsetColorSchema = require('../schema/common/OffsetColor');

import SearchSettingsSchema = require('../schema/appstudio/app/configuration/uiconfig/SearchSettings');

import TabSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/Tab');

import RangeSchema = require('../schema/common/Range');

import WebViewSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebView');
import WebViewContentSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebViewContent');
import WebViewStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebViewStyle');

import WelcomeScreenSchema = require('../schema/appstudio/app/configuration/uiconfig/settings/WelcomeScreen');
import GoogleAnalyticSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/GoogleAnalytics');
import MobileGoogleAnalytics = require('../schema/appstudio/app/mobile/uiconfig/analytics/MobileGoogleAnalytics');
import TabletGoogleAnalytics = require('../schema/appstudio/app/tablet/uiconfig/analytics/TabletGoogleAnalytics');
import TVGoogleAnalytics = require('../schema/appstudio/app/tv/uiconfig/analytics/TvGoogleAnalytics');
import GooglePageSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/Page');
import GoogleEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/Event');
import GoogleSystemEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/Event');
import SystemEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/SystemEvents');
import VideoPlaybackEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/VideoPlaybackEvent');
import LayerSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/Layer');
import BoxParams = require('../schema/common/BoxParams');
import LayerControlsSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/LayerControls');
import TopMenuStyleSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/style/TopMenuStyle');
import TopMenuSectionSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/TopMenuSection');

export class AppstudioModelFactory {
    static create(type, data?, config?): MetadataOwnerDto {
        let instance;
        let schema;

        switch (type) {
            case 'MetadataOwner':
                instance = <MetadataOwnerDto>{};
                break;
            case 'MPX':
            case MobileMPXSchema.ID:
            case TabletMPXSchema.ID:
            case TVMPXSchema.ID:
                instance = {
                    _metadata: MpxSchema.ID,
                    type: 'MPX'
                };
                break;
            case 'Videa':
            case MobileVideaCmsSchema.ID:
            case TabletVideaCmsSchema.ID:
            case TvVideaCmsSchema.ID:
                instance = {
                    _metadata: VideaCmsSchema.ID,
                    type: 'Videa'
                };
                break;
            case 'ComponentControls':
                instance = {
                    _metadata: ComponentControlsSchema.ID,
                    listeners: []
                };
                break;
            case 'CarouselControls':
                instance = {
                    _metadata: CarouselControlsSchema.ID,
                    listeners: []
                };
                break;
            case 'ComponentStyle':
                instance = {
                    _metadata: ComponentStyleSchema.ID
                };
                break;
            case 'Component':
                instance = {
                    _metadata: ComponentSchema.ID
                };
                break;
            case 'Font':
                instance = {
                    _metadata: FontSchema.ID,
                    type: 'Font'
                };
                break;
            case 'Color':
                instance = {
                    _metadata: ColorSchema.ID,
                    type: 'Color'
                };
                break;
            case 'FieldContent':
                instance = {
                    _metadata: FieldContentSchema.ID
                };
                break;
            case 'LabelContent':
                instance = {
                    _metadata: LabelContentSchema.ID
                };
                break;
            case 'ImageContent':
                instance = {
                    _metadata: ImageContentSchema.ID
                };
                break;
            case 'BaseFieldStyle':
                instance = {
                    _metadata: BaseFieldStyleSchema.ID
                };
                break;
            case 'LabelStyle':
                instance = {
                    _metadata: LabelStyleSchema.ID
                };
                break;

            case 'ImageStyle':
            case ImageStyleSchema.ID:
                instance = <any>{
                    _metadata: ImageStyleSchema.ID
                };
                break;

            case 'BaseField':
                instance = {
                    _metadata: BaseFieldSchema.ID
                };
                break;
            case 'Label':
                instance = {
                    _metadata: LabelSchema.ID
                };
                break;
            case 'Image':
                instance = {
                    _metadata: ImageSchema.ID
                };
                break;
            case 'NamedComponent':
                instance = {
                    _metadata: NamedComponentSchema.ID
                };
                break;

            case 'BaseCollection':
                instance = {
                    _metadata: BaseCollectionSchema.ID
                };
                break;

            case 'SizedComponentStyle':
                instance = {
                    _metadata: SizedComponentStyleSchema.ID,
                    type: 'Style'
                };
                break;
            case 'SectionStyle':
                instance = {
                    _metadata: SectionStyleSchema.ID,
                    type: 'Style'
                };
                break;

            case 'CarouselStyle':
            case CarouselStyleSchema.ID:
                instance = {
                    _metadata: CarouselStyleSchema.ID,
                    type: 'Style'
                };
                break;
            case 'TileStyle':
                instance = {
                    _metadata: DataViewTileStyleSchema.ID,
                    type: 'Style'
                };
                break;
            case 'CollectionPickerStyle':
                instance = {
                    _metadata: CollectionPickerStyleSchema.ID,
                    type: 'Style'
                };
                break;
            case 'DataViewHeadingStyle':
                instance = {
                    _metadata: DataViewHeadingStyleSchema.ID,
                    type: 'Style'
                };
                break;
            case 'DataViewHeadingSettings':
                instance = {
                    _metadata: DataViewHeadingSettingsSchema.ID
                };
                break;
            case 'DataViewTileSettings':
                instance = {
                    _metadata: DataViewTileSettingsSchema.ID
                };
                break;
            case 'DataViewHeading':
                instance = {
                    _metadata: DataViewHeadingSchema.ID,
                    text: []
                };
                break;
            case 'DataViewContent':
                instance = {
                    _metadata: DataViewContentSchema.ID,
                    text: []
                };
                break;
            case 'DataViewTile':
                instance = {
                    _metadata: DataViewTileSchema.ID,
                    settings: <DataViewTileSettingsDto>AppstudioModelFactory.create('DataViewTileSettings'),
                    text: [],
                    subComponent: []
                };
                break;
            case 'DataView':
                instance = {
                    type: 'DataView'
                };
                break;

            case 'GridViewStyle':
                instance = {
                    _metadata: GridViewStyleSchema.ID
                };
                break;
            case 'GridView':
                instance = {
                    _metadata: GridViewSchema.ID,
                    type: 'GridView'
                };
                break;
            case 'ListView':
                instance = {
                    _metadata: ListViewSchema.ID,
                    type: 'ListView'
                };
                break;
            case 'Swimlane':
                instance = {
                    _metadata: SwimlaneSchema.ID,
                    type: 'Swimlane'
                };
                break;
            case 'CarouselTile':
                instance = {
                    _metadata: CarouselTileSchema.ID,
                    settings: <DataViewTileSettingsDto>AppstudioModelFactory.create('DataViewTileSettings'),
                    text: [],
                    subComponent: []
                };
                break;
            case 'Carousel':
                instance = {
                    _metadata: CarouselSchema.ID,
                    type: 'Carousel'
                };
                break;

            case 'CollectionPickerContent':
                instance = {
                    _metadata: CollectionViewPickerContentSchema.ID
                };
                break;

            case 'CollectionPicker':
                instance = {
                    _metadata: CollectionPickerSchema.ID
                };
                break;

            case 'CollectionView':
                instance = {
                    _metadata: CollectionViewSchema.ID
                };
                break;

            case 'Chromecast':
            case ChromecastSchema.ID:
                instance = {
                    _metadata: ChromecastSchema.ID
                };
                break;
            case 'ChromecastTheme':
            case ChromecastThemeSchema.ID:
                instance = {
                    _metadata: ChromecastThemeSchema.ID
                };
                break;
            case 'ChromecastTitle':
            case ChromecastTitleSchema.ID:
                instance = {
                    _metadata: ChromecastTitleSchema.ID
                };
                break;
            case 'ChromecastReceiver':
            case ChromecastReceiverSchema.ID:
                instance = {
                    _metadata: ChromecastReceiverSchema.ID
                };
                break;
            case 'Toggle':
            case ToggleSchema.ID:
                instance = {
                    _metadata: ToggleSchema.ID
                };
                break;
            case 'ToggleStyle':
            case ToggleStyleSchema.ID:
                instance = {
                    _metadata: ToggleStyleSchema.ID
                };
                break;
            case 'ColoredComponentStyle':
            case ColoredComponentStyleSchema.ID:
                instance = {
                    _metadata: ColoredComponentStyleSchema.ID
                };
                break;
            case 'TextField':
            case TextFieldSchema.ID:
                instance = {
                    _metadata: TextFieldSchema.ID
                };
                break;

            case 'TextFieldStyle':
            case TextFieldStyleSchema.ID:
                instance = {
                    _metadata: TextFieldStyleSchema.ID
                };
                break;

            case 'TextFieldContent':
            case TextFieldContentSchema.ID:
                instance = {
                    _metadata: TextFieldContentSchema.ID
                };
                break;

            case 'Alignment':
            case AlignmentSchema.ID:
                instance = <any> {
                    _metadata: AlignmentSchema.ID
                };
                break;

            case 'Position':
            case PositionSchema.ID:
                instance = <any> {
                    _metadata: PositionSchema.ID
                };
                break;

            case 'Scale':
            case ScaleSchema.ID:
                instance = <any> {
                    _metadata: ScaleSchema.ID
                };
                break;

            case 'Brand':
            case BrandSchema.ID:
                instance = {
                };
                break;

            case 'App':
            case AppSchema.ID:
                instance = {
                    _metadata: AppSchema.ID
                };
                break;
            case 'MobileApp':
            case MobileAppSchema.ID:
                instance = {
                    _metadata: MobileAppSchema.ID
                };
                break;
            case 'TabletApp':
            case TabletAppSchema.ID:
                instance = {
                    _metadata: TabletAppSchema.ID
                };
                break;
            case 'TVApp':
            case TVAppSchema.ID:
                instance = {
                    _metadata: TVAppSchema.ID
                };
                break;

            case 'UIConfig':
            case UIConfigSchema.ID:
                instance = {
                    _metadata: UIConfigSchema.ID
                };
                break;

            case 'Screen':
            case ScreenSchema.ID:
                instance = {
                    _metadata: ScreenSchema.ID
                };
                break;
            case 'MobileScreen':
            case MobileScreenSchema.ID:
                instance = {
                    _metadata: MobileScreenSchema.ID
                };
                break;
            case 'TabletScreen':
            case TabletScreenSchema.ID:
                instance = {
                    _metadata: TabletScreenSchema.ID
                };
                break;
            case 'TVScreen':
            case TVScreenSchema.ID:
                instance = {
                    _metadata: TVScreenSchema.ID
                };
                break;
            case 'ImageSection':
            case ImageSectionSchema.ID:
                instance = {
                    _metadata: ImageSectionSchema.ID
                };
                break;
            case 'ImageSettingsSection':
            case ImageSettingsSectionSchema.ID:
                instance = {
                    _metadata: ImageSettingsSectionSchema.ID
                };
                break;
            case 'LabelSettingsSection':
            case LabelSettingsSectionSchema.ID:
                instance = {
                    _metadata: LabelSettingsSectionSchema.ID
                };
                break;
            case 'LabelSection':
            case LabelSectionSchema.ID:
                instance = {
                    _metadata: LabelSectionSchema.ID
                };
                break;

            case 'Section':
            case SectionSchema.ID:
                instance = {
                    _metadata: SectionSchema.ID
                };
                break;
            case 'SecondaryMenu':
            case SecondaryMenuSchema.ID:
                instance = {
                    _metadata: SecondaryMenuSchema.ID
                };
                break;
            case 'SecondaryMenuSectionSettings':
            case SecondaryMenuSectionSettingsSchema.ID:
                instance = {
                    _metadata: SecondaryMenuSectionSettingsSchema.ID
                };
                break;
            case 'SecondaryMenuSection':
            case SecondaryMenuSectionSchema.ID:
                instance = {
                    _metadata: SecondaryMenuSectionSchema.ID
                };
                break;
            case 'SecondaryMenuSettings':
            case SecondaryMenuSettingsSchema.ID:
                instance = {
                    _metadata: SecondaryMenuSettingsSchema.ID
                };
                break;
            case 'SecondaryMenuStyle':
            case SecondaryMenuStyleSchema.ID:
                instance = {
                    _metadata: SecondaryMenuStyleSchema.ID
                };
                break;
            case 'SectionSettings':
            case SectionSettingsSchema.ID:
                instance = {
                    _metadata: SectionSettingsSchema.ID
                };
                break;
            case 'Navigation':
            case TVNavigationSchema.ID:
            case MobileNavigationSchema.ID:
            case TabletNavigationSchema.ID:
            case AppStudioNavigationSchema.ID:
                instance = {
                    _metadata: AppStudioNavigationSchema.ID,
                    type: 'Navigation'
                };
                break;

            case 'DrawerMenu':
            case DrawerMenuSchema.ID:
                instance = {
                    _metadata: DrawerMenuSchema.ID,
                    type: 'Drawer',
                    section: [],
                    secondaryMenu: []
                };

                break;

            case 'VerticalMenu':
            case VerticalMenuSchema.ID:
                instance = {
                    _metadata: VerticalMenuSchema.ID,
                    type: 'Vertical'
                };
                break;


            case 'TopMenu':
            case TopMenuSchema.ID:
                instance = {
                    _metadata: TopMenuSchema.ID,
                    type: 'Top'
                };
                break;

            case 'WatchHistory':
            case WatchHistorySchema.ID:
                instance = {
                    _metadata: WatchHistorySchema.ID
                };
                break;

            case 'NoWatchHistoryProvider':
            case WatchHistorySchema.ID:
                instance = {
                    _metadata: NoWatchHistoryProviderSchema.ID
                };
                break;

            case 'LocalWatchHistoryProvider':
            case LocalProviderSchema.ID:
                instance = {
                    _metadata: LocalProviderSchema.ID
                };
                break;

            case 'MpxWatchHistoryProvider':
            case MpxProviderSchema.ID:
                instance = {
                    _metadata: MpxProviderSchema.ID
                };
                break;

            case 'AdvanceComponent':
            case AdvanceComponentSchema.ID:
                instance = {
                    _metadata: AdvanceComponentSchema.ID
                };
                break;

            case JsonCollectionProviderSchema.ID:
                instance = {
                    _metadata: JsonCollectionProviderSchema.ID
                };
                break;
            case MpxCollectionProviderSchema.ID:
                instance = {
                    _metadata: MpxCollectionProviderSchema.ID
                };
                break;
            case VideaCollectionProviderSchema.ID:
                instance = {
                    _metadata: VideaCollectionProviderSchema.ID
                };
                break;
            case 'Authentication':
            case AuthenticationSchema.ID:
                instance = {
                    _metadata: AuthenticationSchema.ID
                };
                break;
            case 'CustomData':
            case CustomDataSchema.ID:
                instance = {
                    _metadata: CustomDataSchema.ID,
                    content: '{}'
                };
                break;
            case 'GemiusAnalytics':
            case GemiusAnalyticSchema.ID:
                instance = {
                    _metadata: GemiusAnalyticSchema.ID
                };
                break;
            case 'MobileAppEdition':
            case MobileAppEdition.ID:
                schema = new MobileAppEdition();
                instance = schema.getInstance({
                    _metadata: MobileAppEdition.ID
                });
                break;
            case 'TabletAppEdition':
            case TabletAppEdition.ID:
                schema = new TabletAppEdition();
                instance = schema.getInstance({
                    _metadata: TabletAppEdition.ID
                });
                break;
            case 'TVAppEdition':
            case TVAppEdition.ID:
                schema = new TVAppEdition();
                instance = schema.getInstance({
                    _metadata: TVAppEdition.ID
                });
                break;
            case 'ExternalEdition':
            case ExternalEdition.ID:
                schema = new ExternalEdition();
                instance = schema.getInstance({
                    _metadata: ExternalEdition.ID
                });
                break;
            case 'MobileUIConfig':
            case MobileUIConfig.ID:
                schema = new MobileUIConfig();
                instance = schema.getInstance({
                    _metadata: MobileUIConfig.ID
                });
                break;
            case 'TabletUIConfig':
            case TabletUIConfig.ID:
                schema = new TabletUIConfig();
                instance = schema.getInstance({
                    _metadata: TabletUIConfig.ID
                });
                break;
            case 'TVUIConfig':
            case TVUIConfig.ID:
                schema = new TVUIConfig();
                instance = schema.getInstance({
                    _metadata: TVUIConfig.ID
                });
                break;
            case 'ExternalUIConfig':
            case ExternalUIConfig.ID:
                schema = new ExternalUIConfig();
                instance = schema.getInstance({
                    _metadata: ExternalUIConfig.ID
                });
                break;
            case CustomCollectionProviderSchema.ID:
                instance = {
                    _metadata: CustomCollectionProviderSchema.ID
                };
                break;
            case 'StandardAspectRatio':
            case StandardAspectRatioSchema.ID:
                instance = {
                    _metadata: StandardAspectRatioSchema.ID
                };
                break;
            case 'CustomAspectRatio':
            case CustomAspectRatioSchema.ID:
                instance = {
                    _metadata: CustomAspectRatioSchema.ID
                };
                break;
            case 'ProgressBar':
            case ProgressBarSchema.ID:
                instance = {
                    _metadata: ProgressBarSchema.ID
                };
                break;
            case 'ProgressBarStyle':
            case ProgressBarStyleSchema.ID:
                instance = {
                    _metadata: ProgressBarStyleSchema.ID
                };
                break;
            case 'NoData':
            case NoDataSchema.ID:
                instance = {
                    _metadata: NoDataSchema.ID
                };
                break;

            case 'Favorites':
            case FavoritesSchema.ID:
                instance = <any>{
                    _metadata: FavoritesSchema.ID
                };
                break;

            case 'FavoritesLocalProvider':
            case FavoritesLocalProviderSchema.ID:
                instance = <any>{
                    _metadata: FavoritesLocalProviderSchema.ID
                };
                break;
            case 'User':
            case UserSchema.ID:
                instance = <any> {
                    _metadata: UserSchema.ID
                };
                break;

            case 'UserMpxProvider':
            case UserMpxProviderSchema.ID:
                instance = <any> {
                    _metadata: UserMpxProviderSchema.ID
                };
                break;

            case 'UserVideaProvider':
            case UserVideaProviderSchema.ID:
                instance = <any> {
                    _metadata: UserVideaProviderSchema.ID
                };
                break;
            case 'BaseReference':
            case BaseReferenceSchema.ID:
                instance = <any> {
                    _metadata: BaseReferenceSchema.ID,
                    displayField: 'value'
                };
                break;     
            case 'CustomWidth':
            case CustomWidthSchema.ID:
                instance = <any> {
                    _metadata: CustomWidthSchema.ID
                };
                break;

            case 'WrapContent':
            case WrapContentSchema.ID:
                instance = <any> {
                    _metadata: WrapContentSchema.ID
                };
                break;

            case 'MatchParent':
            case MatchParentSchema.ID:
                instance = <any> {
                    _metadata: MatchParentSchema.ID
                };
                break;

            case 'CustomHeight':
            case CustomHeightSchema.ID:
                instance = <any> {
                    _metadata: CustomHeightSchema.ID
                };
                break;
            case 'Width':
            case WidthSchema.ID:
                instance = <any> {
                    _metadata: WidthSchema.ID
                };
                break;
            case 'Height':
            case HeightSchema.ID:
                instance = <any> {
                    _metadata: HeightSchema.ID
                };
                break;                             
            case 'OffsetColor':
                instance = <any>{
                    _metadata: OffsetColorSchema.ID,
                    type: 'OffsetColor'
                };
                break;
            case 'GradientColor':
                instance = <any>{
                    _metadata: GradientColorSchema.ID,
                    type: 'GradientColor'
                };
                break;   
            case 'SearchSettings':
            case SearchSettingsSchema.ID:
                instance = <any> {
                    _metadata: SearchSettingsSchema.ID
                };
                break;       
            case 'MenuTab':
            case TabSchema.ID:
                instance = <any> {
                    _metadata: TabSchema.ID
                };
                break;
            case 'Range':
            case RangeSchema.ID:
                instance = <any> {
                    _metadata: RangeSchema.ID
                };
                break;
            case 'WebView':
            case WebViewSchema.ID:
                instance = <any> {
                    _metadata: WebViewSchema.ID
                };
                break;
            case 'WebViewStyle':
            case WebViewStyleSchema.ID:
                instance = <any> {
                    _metadata: WebViewStyleSchema.ID
                };
                break;
            case 'WebViewContent':
            case WebViewContentSchema.ID:
                instance = <any> {
                    _metadata: WebViewContentSchema.ID
                };
                break;
            case 'WelcomeScreen':
            case WelcomeScreenSchema.ID:
                instance = <any> {
                    _metadata: WelcomeScreenSchema.ID
                };
                break;
            case 'GoogleAnalytics':
            case GoogleAnalyticSchema.ID:
            case TVGoogleAnalytics.ID:
            case TabletGoogleAnalytics.ID:
            case MobileGoogleAnalytics.ID:
                instance = <any>{
                    _metadata: GoogleAnalyticSchema.ID
                };
                break;
            case 'GooglePage':
            case GooglePageSchema.ID:
                instance = <any> {
                    _metadata: GooglePageSchema.ID
                };
                break;
            case 'GoogleEvent':
            case GoogleEventSchema.ID:
                instance = <any> {
                    _metadata: GoogleEventSchema.ID
                };
                break;
            case 'GoogleSystemEvent':
            case GoogleSystemEventSchema.ID:
                instance = <any> {
                    _metadata: GoogleSystemEventSchema.ID
                };
                break;
            case 'SystemEvent':
            case SystemEventSchema.ID:
                instance = <any> {
                    _metadata: SystemEventSchema.ID
                };
                break;
            case 'VideoPlaybackEvent':
            case VideoPlaybackEventSchema.ID:
                instance = <any> {
                    _metadata: VideoPlaybackEventSchema.ID
                };
                break;
            case 'BoxParams':
            case BoxParams.ID:
                instance = <any> {
                    _metadata: BoxParams.ID
                };
                break;                
            case 'Layer':
            case LayerSchema.ID:
                instance = <any> {
                    _metadata: LayerSchema.ID
                };
                break;                
            case 'LayerControls':
            case LayerControlsSchema.ID:
                instance = <any> {
                    _metadata: LayerControlsSchema.ID
                };
                break;
            case 'TopMenuStyle':
            case TopMenuStyleSchema.ID:
                instance = <any> {
                    _metadata: TopMenuStyleSchema.ID
                };
                break;
            case 'TopMenuSection':
            case TopMenuSectionSchema.ID:
                instance = <any> {
                    _metadata: TopMenuSectionSchema.ID
                };
                break;
        }

        if (!instance) {
            console.log('type cannot be found in AppstudioModelFactory ', type);
        }

        instance.id = uuid.v1();

        return AppstudioModelFactory.applyIf(instance, data);
    }

    static applyIf(object: any, config: any) {
        let property;

        if (object) {
            for (property in config) {
                if (config.hasOwnProperty(property) &&
                    (object[property] === null || object[property] === undefined || object[property] === '')) {
                    object[property] = config[property];
                }
            }
        }

        return object;
    }
}