/**
 * Created by bardiakhosravi on 2016-10-19.
 */
import uuid = require('node-uuid');
import {MetadataOwner as MetadataOwner} from '../dto/MetadataOwner';

import AppSchema = require('../schema/clientapp/App');

import StyleSchema = require('../schema/clientapp/app/uiconfig/screen/component/Style');
import {Style as StyleDto} from '../dto/clientapp/Style';

import ComponentSchema = require('../schema/clientapp/app/uiconfig/screen/Component');
import ListenerSchema = require('../schema/common/Listener');
import ContainerSchema = require('../schema/clientapp/app/uiconfig/screen/component/container/Container');
import ContainerStyleSchema = require('../schema/clientapp/app/uiconfig/screen/component/container/ContainerStyle');
import LayoutSchema = require('../schema/clientapp/app/uiconfig/screen/component/layout/Layout');
import LinearLayoutSchema = require('../schema/clientapp/app/uiconfig/screen/component/layout/LinearLayout');
import RelativeLayoutSchema = require('../schema/clientapp/app/uiconfig/screen/component/layout/RelativeLayout');
import DataViewSchema = require('../schema/clientapp/app/uiconfig/screen/component/DataView');
import ClientCollectionSchema = require('../schema/common/collection/BaseCollection');
import RemoteCollectionSchema = require('../schema/common/collection/RemoteCollection');
import LinkCollectionSchema = require('../schema/common/collection/LinkCollection');
import OfflineCollectionSchema = require('../schema/common/collection/OfflineCollection');
import UIDCollectionSchema = require('../schema/common/collection/UIDCollection');
import CarouselSchema = require('../schema/clientapp/app/uiconfig/screen/component/Carousel');
import PickerRendererSchema = require('../schema/clientapp/app/uiconfig/screen/component/picker/PickerRenderer');
import ImageSchema = require('../schema/clientapp/app/uiconfig/screen/field/Image');
import GridLayoutSchema = require('../schema/clientapp/app/uiconfig/screen/component/layout/GridLayout');
import LabelSchema = require('../schema/clientapp/app/uiconfig/screen/field/Label');
import LabelStyleSchema = require('../schema/clientapp/app/uiconfig/screen/field/style/LabelStyle');
import {LabelStyle as LabelStyleDto} from '../dto/clientapp/component/field/style/LabelStyle';
import NavigationActionSchema = require('../schema/common/listener/action/NavigationAction');
import SigninActionSchema = require('../schema/common/listener/action/SigninAction');
import SignoutActionSchema = require('../schema/common/listener/action/SignoutAction');
import PlayBackActionSchema = require('../schema/common/listener/action/PlaybackAction');
import SaveOfflineActionSchema = require('../schema/common/listener/action/SaveOfflineAction');
import RemoveOfflineActionSchema = require('../schema/common/listener/action/RemoveOfflineAction');
import SaveUIDActionSchema = require('../schema/common/listener/action/SaveUIDAction');
import RemoveUIDActionSchema = require('../schema/common/listener/action/RemoveUIDAction');

import AkamaiProvider = require('../schema/common/auth/AkamaiProvider');
import AdobeProvider = require('../schema/common/auth/AdobeProvider');
import NoneProvider = require('../schema/common/auth/NoneProvider');

import ScreenSchema = require('../schema/clientapp/Screen');
import SearchScreenSchema = require('../schema/appstudio/app/configuration/uiconfig/SearchResultScreen');

import {VideaStyleSheet as VssDto} from '../dto/clientapp/vss/VideaStyleSheet';
import VssSchema = require('../schema/clientapp/style/VideaStyleSheet');

import RuleSchema = require('../schema/clientapp/style/Rule');

import ColorSchema = require('../schema/common/Color');

import MobileGoogleAnalytics = require('../schema/appstudio/app/mobile/uiconfig/analytics/MobileGoogleAnalytics');
import TabletGoogleAnalytics = require('../schema/appstudio/app/tablet/uiconfig/analytics/TabletGoogleAnalytics');
import TVGoogleAnalytics = require('../schema/appstudio/app/tv/uiconfig/analytics/TvGoogleAnalytics');
import GoogleAnalyticsSchema = require('../schema/clientapp/analytics/GoogleAnalytics');

import TabletNavigationSchema = require('../schema/appstudio/app/tablet/uiconfig/navigation/TabletNavigation');
import MobileNavigationSchema = require('../schema/appstudio/app/mobile/uiconfig/navigation/MobileNavigation');
import TVNavigationSchema = require('../schema/appstudio/app/tv/uiconfig/navigation/TVNavigation');

import TopMenuSchema = require('../schema/common/navigation/TopMenu');
import DrawerMenuSchema = require('../schema/common/navigation/DrawerMenu');
import BottomMenuSchema = require('../schema/common/navigation/BottomMenu');
import VerticalMenuSchema = require('../schema/common/navigation/VerticalMenu');
import NavigationSchema = require('../schema/clientapp/navigation/Navigation');
import AppStudioNavigationSchema = require('../schema/appstudio/app/configuration/uiconfig/Navigation');
import BlankMenuSchema = require('../schema/common/navigation/BlankMenu');

import FontSchema = require('../schema/clientapp/Font');

import MobileMPX = require('../schema/appstudio/app/mobile/cms/MobileMpx');
import TabletMPX = require('../schema/appstudio/app/tablet/cms/TabletMpx');
import TVMPX = require('../schema/appstudio/app/tv/cms/TvMpx');
import MPX = require('../schema/common/cms/Mpx');

import MobileVideaCms = require('../schema/appstudio/app/mobile/cms/MobileVideaCms');
import TabletVideaCms = require('../schema/appstudio/app/tablet/cms/TabletVideaCms');
import TvVideaCms = require('../schema/appstudio/app/tv/cms/TvVideaCms');
import VideaCMS = require('../schema/common/cms/VideaCms');

import TabletVimondCms = require('../schema/appstudio/app/tablet/cms/TabletVimondCms');
import TvVimondCms = require('../schema/appstudio/app/tv/cms/TvVimondCms');
import VimondCms = require('../schema/common/cms/VimondCms');

import MobileGenericCms = require('../schema/appstudio/app/mobile/cms/MobileGenericCms');
import TabletGenericCms = require('../schema/appstudio/app/tablet/cms/TabletGenericCms');
import TvGenericCms = require('../schema/appstudio/app/tv/cms/TvGenericCms');
import GenericCMS = require('../schema/common/cms/GenericCms');


import SelectEventSchema = require('../schema/common/listener/event/SelectEvent');
import LongSelectEventSchema = require('../schema/common/listener/event/LongSelectEvent');
import CollectionViewSchema = require('../schema/clientapp/app/uiconfig/screen/component/CollectionView');

import ChromecastSchema = require('../schema/clientapp/app/Chromecast');

import BaseAuthenticationSchema = require('../schema/common/auth/BasicAuthentication');

import ToggleSchema = require('../schema/clientapp/app/uiconfig/screen/field/Toggle');

import TextFieldSchema = require('../schema/clientapp/app/uiconfig/screen/field/TextField');

import LocalProviderSchema = require('../schema/appstudio/app/configuration/uiconfig/watchhistory/LocalProvider');

import MpxProviderSchema = require('../schema/appstudio/app/configuration/uiconfig/watchhistory/MpxProvider');

import MediaGallerySchema = require('../schema/appstudio/MediaGallery');
import CustomDataSchema = require('../schema/common/CustomData');

import GemiusAnalyticsSchema = require('../schema/clientapp/analytics/GemiusAnalytics');

import {BaseSchema} from 'videa-framework/schema/BaseSchema';
import BaseField = require('../schema/clientapp/app/uiconfig/screen/field/BaseField');

import BaseFontSchema = require('../schema/common/font/BaseFont');
import OpenSansSchema = require('../schema/common/font/OpenSans');
import RobotoSchema = require('../schema/common/font/Roboto');
import SanFranciscoSchema = require('../schema/common/font/SanFrancisco');

import ProgressBarSchema = require('../schema/clientapp/app/uiconfig/screen/field/ProgressBar');

import FavoritesSchema = require('../schema/clientapp/app/Favorites');
import FavoritesLocalProviderSchema = require('../schema/clientapp/app/favorites/LocalProvider');

import UserSchema = require('../schema/clientapp/app/User');
import UserMpxProviderSchema = require('../schema/clientapp/app/user/MpxProvider');
import UserVideaProviderSchema = require('../schema/clientapp/app/user/VideaProvider');
import {User as UserDto} from '../dto/clientapp/app/User';

import AnalyticActionSchema = require('../schema/common/listener/action/AnalyticAction');
import SearchSettingsSchema = require('../schema/clientapp/app/uiconfig/Search');

import OffsetColorSchema = require('../schema/common/OffsetColor');
import GradientColorSchema = require('../schema/clientapp/GradientColor');
import MobileVimond = require('../schema/appstudio/app/mobile/cms/MobileVimond');

import MenuTabSchema = require('../schema/appstudio/app/configuration/uiconfig/navigation/Tab');
import FavoriteCollectionSchema = require('../schema/common/collection/FavoriteCollection');
import WatchHistoryCollectionSchema = require('../schema/common/collection/WatchHistoryCollection');

import WebViewSchema = require('../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebView');
import LinkActionSchema = require('../schema/common/listener/action/LinkAction');
import AppstudioGoogleAnalyticSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/GoogleAnalytics');
import PlayEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/PlayEvent');
import PauseEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/PauseEvent');
import ResumeEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/ResumeEvent');
import StopEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/StopEvent');
import PercentageEventSchema = require('../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/PercentageEvent');
import BasicAuthenticationSchema = require('../schema/common/auth/BasicAuthentication');
import SpaceSchema = require('../schema/clientapp/app/uiconfig/screen/component/Space');

import VerticalLayoutSchema = require('../schema/common/layout/VerticalLayout');
import RelativeLayoutSchema = require('../schema/common/layout/RelativeLayout');
import HorizontalLayoutSchema = require('../schema/common/layout/HorizontalLayout');

export class ClientAppModelFactory {

    static create(type, data?, config?): MetadataOwner {
        let instance;

        switch (type) {
            case 'Dataview':
            case DataViewSchema.ID:
                instance = {
                    _metadata: DataViewSchema.ID
                };
                break;
            case 'BaseField':
            case BaseField.ID:
                instance = {
                    _metadata: BaseField.ID
                };
                break;
            case 'BaseSchema':
            case BaseSchema.ID:
                instance = {
                    _metadata: BaseSchema.ID
                };
                break;
            case 'ClientApp':
            case AppSchema.ID:
                instance = {
                    _metadata: AppSchema.ID
                };
                break;
            case 'Style':
            case StyleSchema.ID:
                instance = <any>{
                    _metadata: StyleSchema.ID,
                    type: 'Style'
                };
                break;
            case 'Component':
            case ComponentSchema.ID:
                instance = {};
                instance.listeners = [];
                break;
            case 'Container':
            case ContainerSchema.ID:
                instance = {
                    _metadata: ContainerSchema.ID,
                    type: 'Container'
                };

                if (!(data && data.width)) {
                    instance.width = 'matchParent';
                }

                if (!(data && data.height)) {
                    instance.height = 'wrapContent';
                }

                if (!(data && data.items)) {
                    instance.items = [];
                }
                break;

            case 'ContainerStyle':
            case ContainerStyleSchema.ID:
                instance = {
                    _metadata: ContainerStyleSchema.ID,
                    type: 'ContainerStyle'
                };
                break;
            case 'Layout':
            case LayoutSchema.ID:
                instance = {
                    _metadata: LayoutSchema.ID,
                    type: 'Layout'
                };
                break;
            case 'LinearLayout':
            case LinearLayoutSchema.ID:
                instance = {
                    _metadata: LinearLayoutSchema.ID,
                    type: 'Linear'
                };

                if (!(data && data.orientation)) {
                    instance.orientation = 'vertical';
                }

                break;
            case 'GridLayout':
            case GridLayoutSchema.ID:
                instance = {
                    _metadata: GridLayoutSchema.ID,
                    type: 'Grid'
                };

                if (!(data && data.orientation)) {
                    instance.orientation = 'vertical';
                }

                break;
            case 'RelativeLayout':
            case RelativeLayoutSchema.ID:
                instance = {
                    _metadata: RelativeLayoutSchema.ID,
                    type: 'Relative'
                };
                break;
            case 'DataView':
            case DataViewSchema.ID:
                instance = {
                    _metadata: DataViewSchema.ID,
                    type: 'DataView'
                };
                break;
            case 'CollectionView':
            case 'CollectionsView':
                instance = {
                    _metadata: CollectionViewSchema.ID,
                    type: 'CollectionsView'
                };
                break;
            case 'Collection':
            case ClientCollectionSchema.ID:
                instance = {
                    _metadata: ClientCollectionSchema.ID,
                    type: 'Collection'
                };
                break;
            case 'Link':
            case LinkCollectionSchema.ID:
                instance = {
                    _metadata: LinkCollectionSchema.ID,
                    type: 'Link'
                };
                break;
            case 'Remote':
            case RemoteCollectionSchema.ID:
                instance = {
                    _metadata: RemoteCollectionSchema.ID,
                    type: 'Remote'
                };
                break;
            case 'Offline':
            case OfflineCollectionSchema.ID:
                instance = {
                    _metadata: OfflineCollectionSchema.ID,
                    type: 'Offline'
                };
                break;
            case 'UID':
            case UIDCollectionSchema.ID:
                instance = {
                    _metadata: UIDCollectionSchema.ID,
                    type: 'UID'
                };
                break;
            case 'Carousel':
            case CarouselSchema.ID:
                instance = {
                    _metadata: CarouselSchema.ID,
                    type: 'Carousel',
                    style: ClientAppModelFactory.create('ContainerStyle')
                };

                if (!(data && data.width)) {
                    instance.width = 'matchParent';
                }

                if (!(data && data.height)) {
                    instance.height = 'wrapContent';
                }
                break;
            case 'PickerRenderer':
                instance = <any>{
                    _metadata: PickerRendererSchema.ID,
                    type: 'Dropdown'
                };
                break;
            case 'Image':
            case ImageSchema.ID:
                instance = {
                    _metadata: ImageSchema.ID,
                    type: 'Image',
                    value: null
                };
                break;
            case 'Label':
            case LabelSchema.ID:
                instance = {
                    _metadata: LabelSchema.ID,
                    type: 'Label'
                };
                break;
            case 'Signin':
            case SigninActionSchema.ID:
                instance = {
                    _metadata: SigninActionSchema.ID,
                    type: 'Signin',
                };
                break;
            case 'Signout':
            case SignoutActionSchema.ID:
                instance = {
                    _metadata: SignoutActionSchema.ID,
                    type: 'Signout',
                };
                break;
            case 'NavigationAction':
            case NavigationActionSchema.ID:
                instance = {
                    _metadata: NavigationActionSchema.ID,
                    type: 'Navigation',
                };
                break;
            case 'Playback':
            case PlayBackActionSchema.ID:
                instance = {
                    _metadata: PlayBackActionSchema.ID,
                    type: 'Playback',
                };
                break;
            case 'SaveOffline':
            case SaveOfflineActionSchema.ID:
                instance = {
                    _metadata: SaveOfflineActionSchema.ID,
                    type: 'SaveOffline',
                };
                break;
            case 'RemoveOffline':
            case RemoveOfflineActionSchema.ID:
                instance = {
                    _metadata: RemoveOfflineActionSchema.ID,
                    type: 'RemoveOffline',
                };
                break;
            case 'SaveUID':
            case SaveUIDActionSchema.ID:
                instance = {
                    _metadata: SaveUIDActionSchema.ID,
                    type: 'SaveUID',
                };
                break;
            case 'RemoveUID':
            case RemoveUIDActionSchema.ID:
                instance = {
                    _metadata: RemoveUIDActionSchema.ID,
                    type: 'RemoveUID',
                };
                break;
            case AdobeProvider.ID:
                instance = {
                    _metadata: AdobeProvider.ID,
                    type: 'Adobe'
                };
                break;
            case AkamaiProvider.ID:
                instance = {
                    _metadata: AkamaiProvider.ID,
                    type: 'Akamai'
                };
                break;
            case NoneProvider.ID:
                instance = {
                    _metadata: NoneProvider.ID,
                    type: 'None'
                };
                break;
            case 'Screen':
            case ScreenSchema.ID:
                instance = {
                    _metadata: ScreenSchema.ID,
                    type: 'Screen'
                };

                if (!(data && data.width)) {
                    instance.width = 'matchParent';
                }

                if (!(data && data.height)) {
                    instance.height = 'matchParent';
                }

                if (!(data && data.layout)) {
                    instance.layout = ClientAppModelFactory.create(LinearLayoutSchema.ID, {
                        orientation: 'vertical'
                    });
                }
                break;
            case 'SearchResultsScreen':
            case SearchScreenSchema.ID:
                instance = {
                    _metadata: SearchScreenSchema.ID,
                    type: 'SearchResultsScreen'
                };

                if (!(data && data.width)) {
                    instance.width = 'matchParent';
                }

                if (!(data && data.height)) {
                    instance.height = 'matchParent';
                }

                if (!(data && data.layout)) {
                    instance.layout = ClientAppModelFactory.create(LinearLayoutSchema.ID, {
                        orientation: 'vertical'
                    });
                }
                break;
            case 'GoogleAnalytics':
            case TVGoogleAnalytics.ID:
            case TabletGoogleAnalytics.ID:
            case MobileGoogleAnalytics.ID:
            case AppstudioGoogleAnalyticSchema.ID:
                instance = <any>{
                    _metadata: GoogleAnalyticsSchema.ID,
                    type: 'GoogleAnalytics'
                };
                break;
            case 'ClientNavigation':
            case NavigationSchema.ID:
            case TVNavigationSchema.ID:
            case MobileNavigationSchema.ID:
            case TabletNavigationSchema.ID:
            case AppStudioNavigationSchema.ID:
                instance = {
                    _metadata: NavigationSchema.ID,
                    type: 'Navigation'
                };
                break;
            case 'DrawerMenu':
            case DrawerMenuSchema.ID:
                instance = {
                    _metadata: DrawerMenuSchema.ID,
                    type: 'Drawer'
                };
                break;
            case 'BottomMenu':
            case BottomMenuSchema.ID:
                instance = {
                    _metadata: BottomMenuSchema.ID,
                    type: 'Bottom'
                };
                break;
            case 'BlankMenu':
            case BlankMenuSchema.ID:
                instance = {
                    _metadata: BlankMenuSchema.ID,
                    type: 'None'
                };
                break;
            case 'VerticalMenu':
            case VerticalMenuSchema.ID:
                instance = {
                    _metadata: VerticalMenuSchema.ID,
                    type: 'Vertical'
                };
                break;
            case 'TopMenu':
            case TopMenuSchema.ID:
                instance = {
                    _metadata: TopMenuSchema.ID,
                    type: 'Top'
                };
                break;
            case 'Font':
            case FontSchema.ID:
                instance = {
                    _metadata: FontSchema.ID,
                };
                break;
            case 'Color':
            case ColorSchema.ID:
                instance = {
                    _metadata: ColorSchema.ID,
                    type: 'Color'
                };
                break;
            case MobileMPX.ID:
            case TabletMPX.ID:
            case TVMPX.ID:
                instance = {
                    _metadata: MPX.ID,
                    type: 'MPX'
                };
                break;
            case MobileVideaCms.ID:
            case TabletVideaCms.ID:
            case TvVideaCms.ID:
                instance = {
                    _metadata: VideaCMS.ID,
                    type: 'Videa'
                };
                break;
            case MobileVimond.ID:
            case TabletVimondCms.ID:
            case TvVimondCms.ID:
                instance = {
                    _metadata: VimondCms.ID,
                    type: 'Vimond'
                };
                break;
            case MobileGenericCms.ID:
            case TabletGenericCms.ID:
            case TvGenericCms.ID:
                instance = {
                    _metadata: GenericCMS.ID,
                    type: 'JSON'
                };
                break;
            case 'Rule':
            case RuleSchema.ID:
                instance = {
                    _metadata: RuleSchema.ID
                };
                break;
            case 'LabelStyle':
            case LabelStyleSchema.ID:
                instance = {
                    _metadata: LabelStyleSchema.ID
                };
                break;
            case 'Listener':
            case ListenerSchema.ID:
                instance = {
                    _metadata: ListenerSchema.ID
                };
                break;
            case 'SelectEvent':
            case SelectEventSchema.ID:
                instance = {
                    _metadata: SelectEventSchema.ID,
                    type: 'Select'
                };
                break;
            case 'LongSelectEvent':
            case LongSelectEventSchema.ID:
                instance = {
                    _metadata: LongSelectEventSchema.ID,
                    type: 'LongSelect'
                };
                break;
            case 'Chromecast':
            case ChromecastSchema.ID:
                instance = {
                    _metadata: ChromecastSchema.ID,
                    type: 'Chromecast'
                };
                break;
            case 'BasicAuthentication':
            case BasicAuthenticationSchema.ID:
                instance = {
                    _metadata: BasicAuthenticationSchema.ID,
                    type: 'BasicAuthentication'
                };
                break;
            case 'Toggle':
            case ToggleSchema.ID:
                instance = {
                    _metadata: ToggleSchema.ID,
                    type: 'Toggle'
                };
                break;
            case 'TextField':
            case TextFieldSchema.ID:
                instance = {
                    _metadata: TextFieldSchema.ID,
                    type: 'TextField'
                };
                break;
            case 'LocalWatchHistoryProvider':
            case LocalProviderSchema.ID:
                instance = {
                    provider: null
                };
                break;
            case 'MpxWatchHistoryProvider':
            case MpxProviderSchema.ID:
                instance = {
                    provider: 'VideaMPX'
                };
                break;
            case 'MediaGallery':
            case MediaGallerySchema.ID:
                instance = {
                    _metadata: MediaGallerySchema.ID
                };
                break;
            case 'CustomData':
            case CustomDataSchema.ID:
                instance = {
                    _metadata: CustomDataSchema.ID,
                    content: '{}'
                };

                if (data && data.content) {
                    instance.content = data.content;
                }
                break;
            case 'GemiusAnalytics':
            case GemiusAnalyticsSchema.ID:
                instance = {
                    _metadata: GemiusAnalyticsSchema.ID,
                    type: 'Gemius'
                };
                break;
            case 'BaseFont':
            case BaseFontSchema.ID:
                instance = <any> {
                    _metadata: BaseFontSchema.ID,
                    type: 'Font'
                };
                break;
            case 'OpenSans':
            case OpenSansSchema.ID:
                instance = <any> {
                    _metadata: OpenSansSchema.ID,
                    type: 'Font'
                };
                break;
            case 'Roboto':
            case RobotoSchema.ID:
                instance = <any> {
                    _metadata: RobotoSchema.ID,
                    type: 'Font'
                };
                break;
            case 'SanFrancisco':
            case SanFranciscoSchema.ID:
                instance = <any> {
                    _metadata: SanFranciscoSchema.ID,
                    type: 'Font'
                };
                break;              
            case 'ProgressBar':
            case ProgressBarSchema.ID:
                instance = <any> {
                _metadata: ProgressBarSchema.ID,
                type: 'ProgressBar'
                };
                break;      
            case 'Favorites':
            case FavoritesSchema.ID:
                instance = <any> {
                _metadata: FavoritesSchema.ID
                };
                break;
            case 'FavoritesLocalProvider':
            case FavoritesLocalProviderSchema.ID:
                instance = <any> {
                type: 'Local', 
                provider: 'MPX'
                };
                break;     
            case 'User':
            case UserSchema.ID:
                instance = <any> {
                _metadata: UserSchema.ID
                };
                break;

            case 'UserMpxProvider':
            case UserMpxProviderSchema.ID:
                instance = <UserDto> {
                provider: 'MPX'
                };
                break;

            case 'UserVideaProvider':
            case UserVideaProviderSchema.ID:
                instance = <UserDto> {
                provider: 'Videa'
                };
                break;  
            case 'SearchSettings':
                case SearchSettingsSchema.ID:
                    instance = <any> {
                    _metadata: SearchSettingsSchema.ID
                    };
                    break;                                         
            case 'AnalyticAction':
            case AnalyticActionSchema.ID:
                instance = <any> {
                _metadata: AnalyticActionSchema.ID,
                type: 'Analytics'
                };
                break;                          
            case 'OffsetColor':
            case OffsetColorSchema.ID:
            instance = <any> {
                _metadata: OffsetColorSchema.ID,
            };
            break;
            case 'GradientColor':
            case GradientColorSchema.ID:
            instance = <any> {
                _metadata: GradientColorSchema.ID,
                type: 'LinearGradientColor'
            };
            break;     
            case 'MenuTab':
            case MenuTabSchema.ID:
              instance = <any> {
                _metadata: MenuTabSchema.ID,
                type: 'Tab'
              };
              break;      
            case 'FavoriteCollection':
            case FavoriteCollectionSchema.ID:
            instance = <any> {
                _metadata: FavoriteCollectionSchema.ID,
                type: 'Favorites'
            };
            break;                    
            case 'WatchHistoryCollection':
            case WatchHistoryCollectionSchema.ID:
            instance = <any> {
                _metadata: WatchHistoryCollectionSchema.ID,
                type: 'WatchHistory'
            };
            break;
            case 'WebView':
            case WebViewSchema.ID:
            instance = <any> {
                _metadata: WebViewSchema.ID,
                type: 'WebView'
            };
            break;
            case 'LinkAction':
            case LinkActionSchema.ID:
            instance = <any> {
                _metadata: LinkActionSchema.ID,
                type: 'Link'
            };
            break;
            case 'PlayEvent':
            case PlayEventSchema.ID:
            instance = <any> {
                _metadata: PlayEventSchema.ID,
                type: 'Play'
            };
            break;       
            case 'PauseEvent':
            case PauseEventSchema.ID:
            instance = <any> {
                _metadata: PauseEventSchema.ID,
                type: 'Pause'
            };
            break;        
            case 'ResumeEvent':
            case ResumeEventSchema.ID:
            instance = <any> {
                _metadata: ResumeEventSchema.ID,
                type: 'Resume'
            };
            break;       
            case 'StopEvent':
            case StopEventSchema.ID:
            instance = <any> {
                _metadata: StopEventSchema.ID,
                type: 'Stop'
            };
            break;        
            case 'PercentageEvent':
            case PercentageEventSchema.ID:
            instance = <any> {
                _metadata: PercentageEventSchema.ID,
                type: 'Percentage'
            };
            break;
            case 'Space':
            case SpaceSchema.ID:
              instance = <any> {
                _metadata: SpaceSchema.ID,
                type: 'Space'
              };
              break;
            case 'VerticalLayout':
            case VerticalLayoutSchema.ID:
            instance = <any> {
                _metadata: VerticalLayoutSchema.ID,
                type: 'Vertical'
            };
            break;
            case 'HorizontalLayout':
            case HorizontalLayoutSchema.ID:
            instance = <any> {
                _metadata: HorizontalLayoutSchema.ID,
                type: 'HorizontalLayout'
            };
            break;
            case 'RelativeLayout':
            case RelativeLayoutSchema.ID:
            instance = <any> {
                _metadata: RelativeLayoutSchema.ID,
                type: 'RelativeLayout'
            };
            break;
        }

        if (!instance) {
            console.log('type cannot be found in ClientAppModelFactory ', type);
        }

        instance.id = uuid.v1();

        return ClientAppModelFactory.applyIf(instance, data);
    }

    public static createVss(): VssDto {
        return <any>{
            _metadata: VssSchema.ID,
            // TODO: this might be more optimized if it was a map
            rules: [
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: ComponentSchema.ID,
                    style: ClientAppModelFactory.create(StyleSchema.ID, {
                        padding: '0 0 0 0',
                        margin: '0 0 0 0'
                    })
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: ContainerSchema.ID,

                    style: ClientAppModelFactory.create(StyleSchema.ID, {

                        backgroundColor: ClientAppModelFactory.create(ColorSchema.ID, {
                            value: '#FF000000'
                        }),
                        padding: '0 0 0 0',
                        margin: '0 0 0 0'
                    })
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: CarouselSchema.ID,

                    style: <StyleDto>ClientAppModelFactory.create(StyleSchema.ID, {
                        margin: '0 0 0 0',
                        padding: '0 0 0 0'
                    })
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: ImageSchema.ID,
                    gravity: 'center',
                    scale: 'aspectFit',
                    style: <StyleDto>ClientAppModelFactory.create(StyleSchema.ID, {
                        padding: '0 0 0 0',
                        margin: '0 0 0 0'
                    })
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: LabelSchema.ID,
                    gravity: 'center',
                    numberOfLines: 1,
                    style: <LabelStyleDto>ClientAppModelFactory.create(LabelStyleSchema.ID, {
                        font: {
                            family: 'OpenSans-Regular',
                            size: 12,
                            color: {
                                value: '#FF000000'
                            },
                            alignment: 'left',
                            weight: 'Regular'
                        },
                        padding: '0 0 0 0',
                        margin: '0 0 0 0'
                    })
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: CollectionViewSchema.ID,

                    style: <StyleDto>ClientAppModelFactory.create(StyleSchema.ID, {
                        margin: '0 0 0 0',
                        padding: '0 0 0 0'
                    }),
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: TextFieldSchema.ID,
                    gravity: 'left',
                    numberOfLines: 1,
                    inputType: 'Text',
                    style: ClientAppModelFactory.create(StyleSchema.ID, {
                        font: {
                            family: 'OpenSans-Regular',
                            size: 20,
                            alignment: 'left',
                            weight: 'Regular'
                        },
                        padding: '0 0 0 0',
                        margin: '0 0 0 0'
                    })
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: ToggleSchema.ID,
                    gravity: 'left',
                    style: ClientAppModelFactory.create(StyleSchema.ID, {
                        padding: '0 0 0 0',
                        margin: '0 0 0 0'
                    })
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: BaseSchema.ID
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: ScreenSchema.ID
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: BaseField.ID
                }),
                ClientAppModelFactory.create(RuleSchema.ID, {
                    selector: DataViewSchema.ID
                })
            ]

        };
    }

    /**
     * Copies all the properties of config to object if they don't already exist.
     * // TODO: move to a util class. perhaps Digi.ts
     * // we should make the Digi.ts library an npm module
     */
    static applyIf(object: any, config: any) {
        let property;

        if (object) {
            for (property in config) {
                if (config.hasOwnProperty(property) &&
                    (object[property] === null || object[property] === undefined)) {
                    object[property] = config[property];
                }
            }
        }

        return object;
    }
}