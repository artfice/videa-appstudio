import { BaseAppHandler } from './BaseAppHandler';
import { Logger } from 'videa-framework/Logger';

export class DeleteAppHandler extends BaseAppHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._appApplicationService.remove(event.path.accountId, event.path.brandId, event.path.appId)
            .then(
            (result) => {
                callback(null, result);
            })
            .catch((err) => {
                Logger.info('Failed to delete an app', err);
                callback(err);
            });
    };
}