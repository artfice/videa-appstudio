import { BaseAppHandler } from './BaseAppHandler';
import { Logger } from 'videa-framework/Logger';

export class CreateAppHandler extends BaseAppHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const brandId = event.path.brandId;

        this._appApplicationService.create(accountId, brandId, event.body)
            .then((app) => {
                callback(null, app);
            })
            .catch((err) => {
                Logger.info('Failed to create an app', err);
                callback(err);
            });
    };
}