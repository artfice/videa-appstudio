import { BaseAppHandler } from './BaseAppHandler';
import { Logger } from 'videa-framework/Logger';

export class DuplicateAppHandler extends BaseAppHandler {

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._appApplicationService.duplicate(event.path.accountId, event.path.brandId, null, null, event.path.appId)
            .then((app) => {
                callback(null, app);
            })
            .catch((err) => {
                Logger.info('Failed to duplicate an app', err);
                callback(err);
            });
    };
}