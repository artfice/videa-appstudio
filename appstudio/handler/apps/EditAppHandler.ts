import { BaseAppHandler } from './BaseAppHandler';
import { Logger } from 'videa-framework/Logger';

export class EditAppHandler extends BaseAppHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._appApplicationService.update(event.path.accountId, event.path.brandId, event.path.appId, event.body)
            .then(
            (result) => {
                callback(null, result);
            })
            .catch((err) => {
                Logger.info('Failed to edit an app', err);
                callback(err);
            });
    };
}