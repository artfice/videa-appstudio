import { BaseAppHandler } from './BaseAppHandler';
import { Logger } from 'videa-framework/Logger';

export class GetAppHandler extends BaseAppHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._appApplicationService.getById(event.path.accountId, event.path.appId)
            .then((app) => {
                callback(null, app);
            })
            .catch((err) => {
                Logger.info('Failed to get an app', err);
                callback(err);
            });
    };
}