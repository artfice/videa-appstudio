import { BaseAppHandler } from './BaseAppHandler';
import { Logger } from 'videa-framework/Logger';

export class GetAllAppsHandler extends BaseAppHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._appApplicationService.getByBrand(event.path.accountId, event.path.brandId)
            .then((app) => {
                callback(null, app);
            })
            .catch((err) => {
                Logger.info('Failed to get all apps referenced in a brand', err);
                callback(err);
            });
    };
}