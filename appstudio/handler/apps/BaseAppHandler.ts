import { ScreenRepository } from '../../repository/ScreenRepository';
import { UIConfigRepository } from '../../repository/UIConfigRepository';
import { EditionRepository } from '../../repository/EditionRepository';
import { AppRepository } from '../../repository/AppRepository';
import { BrandRepository } from '../../repository/BrandRepository';
import { ClientConfigRepository } from '../../repository/ClientConfigRepository';
import { AppApplicationService } from '../../application/AppApplicationService';
import { AccountBasedHandler } from 'videa-framework/handler';
import {EditionApplicationService} from "../../application/EditionApplicationService";
import {EditionStateApplicationService} from "../../application/EditionStateApplicationService";
import {AppPublishingApplicationService} from "../../application/AppPublishingApplicationService";
import {TranslatorList} from "../../translation/TranslatorList";
import {TranslationApplicationService} from "../../application/TranslationApplicationService";
import {GalleryImageRepository} from "../../repository/GalleryImageRepository";
import {ScreenApplicationService} from "../../application/ScreenApplicationService";
import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';
import {App} from '../../dto/App';

export class BaseAppHandler extends AccountBasedHandler<App> {
    protected _appApplicationService: AppApplicationService;

    constructor() {
        super();

        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'App',
            collectionName: 'Apps',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const screenRepository = new ScreenRepository(mongoAdapterConfig);
        const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
        const appRepository = new AppRepository(mongoAdapterConfig);
        const clientConfigRepository = new ClientConfigRepository(mongoAdapterConfig);
        const editionRepository = new EditionRepository(mongoAdapterConfig);
        const brandRepository = new BrandRepository(mongoAdapterConfig);
        const galleryImageRepository = new GalleryImageRepository(mongoAdapterConfig);

        const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);
        const eventRepository = new EventRepository(awsSnsAdapter);

        const screenApplicationService = new ScreenApplicationService({
            screenRepository: screenRepository,
            uiConfigRepository: uiConfigRepository,
            eventRepository: eventRepository
        });

        const translatorList = new TranslatorList(galleryImageRepository,
            screenApplicationService, uiConfigRepository);

        const translationApplicationService = new TranslationApplicationService();
        translationApplicationService.registerTranslators(translatorList.getList());

        const editionApplicationService = new EditionApplicationService({
            uiConfigRepository: new UIConfigRepository(mongoAdapterConfig),
            screenRepository: screenRepository,
            editionRepository: editionRepository,
            clientConfigRepository: clientConfigRepository,
            appRepository: appRepository,
            editionStateApplicationService: new EditionStateApplicationService(),
            appPublishingApplicationService: new AppPublishingApplicationService({
                editionRepository: editionRepository,
                clientConfigRepository: clientConfigRepository,
                appRepository: appRepository,
                translationService: translationApplicationService
            }),
        });

        this._appApplicationService = new AppApplicationService({
            uiConfigRepository: uiConfigRepository,
            editionRepository: editionRepository,
            appRepository: appRepository,
            brandRepository: brandRepository,
            clientConfigRepository: clientConfigRepository,
            screenRepository: screenRepository,
            editionApplicationService: editionApplicationService
        });
    }

}