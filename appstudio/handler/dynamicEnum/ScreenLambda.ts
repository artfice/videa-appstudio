import {BaseScreenHandler} from '../screen/BaseScreenHandler';

export class ScreenLambda extends BaseScreenHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        this._screenApplicationService.getDynamicEnumScreen(event.path.accountId, event.path.configId, event.query.type).then((screen) => {
            callback(null, screen);
        }).catch((err) => {
            console.log('Fail to find screen enum', err);
            callback(err);
        });
    };
}