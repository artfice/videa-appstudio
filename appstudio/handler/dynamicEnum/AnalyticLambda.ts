import { BaseUIConfigHandler } from '../uiConfig/BaseUIConfigHandler';

export class AnalyticLambda extends BaseUIConfigHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const configId = event.path.configId;
        this._uiConfigApplicationService.getDynamicEnumAnalytics(accountId, configId).then((analytics) => {
            callback(null, analytics);
        }).catch((err) => {
            console.log('Fail to find analytic enum', err);
            callback(err);
        });
    };
}