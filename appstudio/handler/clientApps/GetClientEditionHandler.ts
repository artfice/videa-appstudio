import {HttpHandler} from 'videa-framework/handler';
import {BrandRepository} from '../../repository/BrandRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {AppRepository} from '../../repository/AppRepository';
import {ClientEditionApplicationService} from '../../application/ClientEditionApplicationService';
import {EditionStateApplicationService} from '../../application/EditionStateApplicationService';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db/mongodb';

export class GetClientEditionHandler extends HttpHandler {
    protected _clientEditionApplicationService: ClientEditionApplicationService;

    constructor() {
        super();

        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'Edition',
            collectionName: 'Editions',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const appRepository = new AppRepository(mongoAdapterConfig);
        const editionRepository = new EditionRepository(mongoAdapterConfig);
        const brandRepository = new BrandRepository(mongoAdapterConfig);
        const editionStateApplicationService = new EditionStateApplicationService();

        this._clientEditionApplicationService = new ClientEditionApplicationService({
            brandRepository: brandRepository,
            editionRepository: editionRepository,
            editionStateService: editionStateApplicationService,
            appRepository: appRepository
        });
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        const accountId = event.path.accountId;
        const brandId = event.query.brandId ? event.query.brandId : '';
        const editionId = event.query.editionId ? event.query.editionId : '';
        const state = event.query.state ? event.query.state : '';

        this._clientEditionApplicationService.getEditions(accountId, brandId, editionId,
        state).then((clientEditions) => {
            callback(null, clientEditions);
        }).catch((err) => {
            console.log('Fail to find Client Editions', err);
            callback(err);
        });
    }

}