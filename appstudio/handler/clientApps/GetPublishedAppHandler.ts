import {HttpHandler} from 'videa-framework/handler';

import {AppRepository} from '../../repository/AppRepository';
import {ClientConfigRepository} from '../../repository/ClientConfigRepository';
import {PublishedClientConfigApplicationService} from '../../application/PublishedClientConfigApplicationService';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db/mongodb';

export class GetPublishedAppHandler extends HttpHandler {
    protected _publishedClientConfigApplicationService: PublishedClientConfigApplicationService;

    constructor() {
        super();

        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'App',
            collectionName: 'Apps',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const appRepository = new AppRepository(mongoAdapterConfig);
        const clientConfigRepository = new ClientConfigRepository(mongoAdapterConfig);

        this._publishedClientConfigApplicationService = new PublishedClientConfigApplicationService({
            clientConfigRepository: clientConfigRepository,
            appRepository: appRepository
        });
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        const accountId = event.path.accountId;
        const appId = event.path.appId;

        this._publishedClientConfigApplicationService.getPublishedClientConfig(
          accountId, appId).then((clientApp) => {
            callback(null, clientApp);
        }).catch((err) => {
            console.log('Fail to find Published ClientConfig', err);
            callback(err);
        });
    }

}