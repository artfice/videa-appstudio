import { ClientConfigRepository } from '../../repository/ClientConfigRepository';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';
import { HttpHandler } from 'videa-framework/handler/HttpHandler';

export class GetClientConfigHandler extends HttpHandler {

    private _clientConfigRepository: ClientConfigRepository;

    public constructor() {
        super();

        this._clientConfigRepository = new ClientConfigRepository(<DtoMongoDbAdapterConfig>{
            dtoName: 'ClientConfig',
            collectionName: 'ClientConfigs',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        });
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        const configId = event.path.configId;

        this._clientConfigRepository.getById(this._path.accountId, configId).then((clientApp) => {
            callback(null, clientApp);
        }).catch((err) => {
            console.log('Fail to find ClientConfig', err);
            callback(err);
        });
    }
}