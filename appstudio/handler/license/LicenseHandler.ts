import { BaseHandler } from 'videa-framework/handler/BaseHandler';
import { LicenseApplicationService } from '../../application/LicenseApplicationService';

export class LicenseHandler extends BaseHandler {

    private _licenseApplicationService: LicenseApplicationService;

    public constructor() {
        super();
        this._licenseApplicationService = new LicenseApplicationService();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        this._licenseApplicationService.getLicense().then((license) => {
            callback(null, license);
        }).catch((err) => {
            callback(err);
        });
    }
}