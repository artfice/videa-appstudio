import {BaseHandler} from 'videa-framework/handler/BaseHandler';

import {ScreenRepository} from '../../repository/ScreenRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {AppRepository} from '../../repository/AppRepository';
import {ClientConfigRepository} from '../../repository/ClientConfigRepository';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import {GalleryImageRepository} from '../../repository/GalleryImageRepository';

import {AppPublishingApplicationService} from '../../application/AppPublishingApplicationService';
import {ScreenApplicationService} from '../../application/ScreenApplicationService';
import {EditionTranslationEventService} from '../../application/EditionTranslationEventService';

import {TranslationApplicationService} from '../../application/TranslationApplicationService';
import {TranslatorList} from '../../translation/TranslatorList';

import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {Logger} from 'videa-framework/Logger';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db';

export class ConfigChangedHandler extends BaseHandler {

    protected _editionTranslationEventService: EditionTranslationEventService;
    protected _eventRepository: EventRepository;

    constructor() {
        super();

        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'Config',
            collectionName: 'Configs',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);

        const screenRepository = new ScreenRepository(mongoAdapterConfig);
        const editionRepository = new EditionRepository(mongoAdapterConfig);
        const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
        const appRepository = new AppRepository(mongoAdapterConfig);
        const clientConfigRepository = new ClientConfigRepository(mongoAdapterConfig);
        const galleryImageRepository = new GalleryImageRepository(mongoAdapterConfig);
        this._eventRepository = new EventRepository(awsSnsAdapter);

        const screenApplicationService = new ScreenApplicationService({
            screenRepository: screenRepository,
            uiConfigRepository: uiConfigRepository,
            eventRepository: this._eventRepository
        });

        const translatorList = new TranslatorList(galleryImageRepository,
            screenApplicationService, uiConfigRepository);

        const translationApplicationService = new TranslationApplicationService();
        translationApplicationService.registerTranslators(translatorList.getList());

        const appPublishingApplicationService = new AppPublishingApplicationService({
            appRepository: appRepository,
            editionRepository: editionRepository,
            clientConfigRepository: clientConfigRepository,
            translationService: translationApplicationService
        });

        this._editionTranslationEventService = new EditionTranslationEventService({
            uiConfigRepository: uiConfigRepository,
            appRepository: appRepository,
            editionRepository: editionRepository,
            screenRepository: screenRepository,
            appPublishingService: appPublishingApplicationService
        });
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        console.log('events', JSON.stringify(event, null, 4));
        Logger.info('Translate Edition Event', JSON.stringify(event, null, 4));
        const records = event.Records;
        let promiseList = records.map((event) => {
            const eventSource = event.EventSource ? event.EventSource : false;
            if (this._eventRepository.isValidEvent(eventSource)) {
                const payload = this._eventRepository.retrievePayload(event);
                const accountId = payload.accountId ? payload.accountId : undefined;
                const editionId = payload.editionId ? payload.editionId : undefined;
                const configId = payload.configId ? payload.configId : undefined;
                const screenId = payload.screenId ? payload.screenId : undefined;
                return this._editionTranslationEventService.retranslate(accountId,
                    editionId, configId, screenId);
            } else {
                return Promise.resolve(undefined);
            }
        });

        Promise.all(promiseList).then((result) => {
            Logger.info('Translate Edition Event Success', JSON.stringify(event, null, 4));
            callback(null, result);
        }).catch((error) => {
            Logger.critical('Translate Edition Event Error: ', error);
            callback(error);
        });
    };
}