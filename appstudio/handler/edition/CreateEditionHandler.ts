import {BaseEditionHandler} from './BaseEditionHandler';

export class CreateEditionHandler extends BaseEditionHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const appId = event.path.appId;
        //TODO: force body to be required

        this._editionApplicationService.create(accountId, appId, event.body).then(
          (edition) => {
            callback(null, edition);
        }).catch((err) => {
            console.log('Fail to create edition', err);
            callback(err);
        });
    };
}