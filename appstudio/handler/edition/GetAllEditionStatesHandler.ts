import {HttpHandler} from 'videa-framework/handler';
import {EditionStateApplicationService} from '../../application/EditionStateApplicationService';

export class GetAllEditionStatesHandler extends HttpHandler {

    protected _editionStateApplicationService: EditionStateApplicationService;

    constructor() {
        super();
        this._editionStateApplicationService = new EditionStateApplicationService();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const appId = event.path.appId;

        callback(null, this._editionStateApplicationService.getStates(accountId, appId));
    };
}