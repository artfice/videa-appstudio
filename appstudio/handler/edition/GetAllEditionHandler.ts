import {BaseEditionHandler} from './BaseEditionHandler';

export class GetAllEditionHandler extends BaseEditionHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const appId = event.path.appId;

        this._editionApplicationService.getByApp(accountId, appId).then(
          (editions) => {
            callback(null, editions);
        }).catch((err) => {
            console.log('Fail to find All edition', err);
            callback(err);
        });
    };
}