import {BaseEditionHandler} from './BaseEditionHandler';

export class DuplicateEditionHandler extends BaseEditionHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const appId = event.path.appId;
        const editionId = event.path.editionId;

        this._editionApplicationService.duplicateEdition(accountId, appId, editionId).then(
          (edition) => {
            callback(null, edition);
        }).catch((err) => {
            console.log('Fail to duplicate edition', err);
            callback(err);
        });
    };
}