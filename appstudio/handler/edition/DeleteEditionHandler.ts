import {BaseEditionHandler} from './BaseEditionHandler';

export class DeleteEditionHandler extends BaseEditionHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const appId = event.path.appId;
        const editionId = event.path.editionId;

        this._editionApplicationService.remove(accountId, appId, editionId).then(
          (deleteResult) => {
            callback(null, deleteResult);
        }).catch((err) => {
            console.log('Fail to find edition', err);
            callback(err);
        });
    };
}