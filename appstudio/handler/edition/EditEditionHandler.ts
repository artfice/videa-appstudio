import {BaseEditionHandler} from './BaseEditionHandler';

export class EditEditionHandler extends BaseEditionHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const appId = event.path.appId;
        //TODO: force body to be required

        this._editionApplicationService.update(accountId, appId, event.body).then(
          (updateResult) => {
            callback(null, updateResult);
        }).catch((err) => {
            console.log('Fail to edit edition', err);
            callback(err);
        });
    };
}