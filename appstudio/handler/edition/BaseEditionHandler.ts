import {AccountBasedHandler} from 'videa-framework/handler';

import {ScreenRepository} from '../../repository/ScreenRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {AppRepository} from '../../repository/AppRepository';
import {GalleryImageRepository} from '../../repository/GalleryImageRepository';
import {ClientConfigRepository} from '../../repository/ClientConfigRepository';
import {EventRepository} from 'videa-framework/repository/EventRepository';

import {EditionApplicationService} from '../../application/EditionApplicationService';
import {ScreenApplicationService} from '../../application/ScreenApplicationService';
import {EditionStateApplicationService} from '../../application/EditionStateApplicationService';
import {AppPublishingApplicationService} from '../../application/AppPublishingApplicationService';

import {TranslationApplicationService} from '../../application/TranslationApplicationService';
import {TranslatorList} from '../../translation/TranslatorList';
import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db/mongodb';
import {Edition} from '../../dto/Edition';

export class BaseEditionHandler extends AccountBasedHandler<Edition> {
    protected _editionApplicationService: EditionApplicationService;

    constructor() {
        super();

        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'Edition',
            collectionName: 'Editions',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);

        const screenRepository = new ScreenRepository(mongoAdapterConfig);
        const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
        const appRepository = new AppRepository(mongoAdapterConfig);
        const clientConfigRepository = new ClientConfigRepository(mongoAdapterConfig);
        const editionRepository = new EditionRepository(mongoAdapterConfig);
        const galleryImageRepository = new GalleryImageRepository(mongoAdapterConfig);

        const eventRepository = new EventRepository(awsSnsAdapter);

        const editionStateApplicationService = new EditionStateApplicationService();

        const screenApplicationService = new ScreenApplicationService({
            screenRepository: screenRepository,
            uiConfigRepository: uiConfigRepository,
            eventRepository: eventRepository
        });

        const translatorList = new TranslatorList(galleryImageRepository,
        screenApplicationService, uiConfigRepository);

        const translationApplicationService = new TranslationApplicationService();
        translationApplicationService.registerTranslators(translatorList.getList());

        const appPublishingApplicationService = new AppPublishingApplicationService({
            appRepository: appRepository,
            editionRepository: editionRepository,
            clientConfigRepository: clientConfigRepository,
            translationService: translationApplicationService
        });

        this._editionApplicationService = new EditionApplicationService({
          uiConfigRepository: uiConfigRepository,
          editionRepository: editionRepository,
          editionStateApplicationService: editionStateApplicationService,
          appRepository: appRepository,
          clientConfigRepository: clientConfigRepository,
          screenRepository: screenRepository,
          appPublishingApplicationService:appPublishingApplicationService
        });
    }

}