import {BaseEditionHandler} from './BaseEditionHandler';

export class GetEditionHandler extends BaseEditionHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const editionId = event.path.editionId;

        this._editionApplicationService.getById(accountId, editionId).then(
          (edition) => {
            callback(null, edition);
        }).catch((err) => {
            console.log('Fail to find edition', err);
            callback(err);
        });
    };
}