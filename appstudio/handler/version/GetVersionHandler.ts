import { BaseHandler } from 'videa-framework/handler/BaseHandler';
import { VersionApplicationService } from 'videa-framework/application/VersionApplicationService';

export class GetVersionHandler extends BaseHandler {

    private _versionApplicationService: VersionApplicationService;

    public constructor() {
        super();
        this._versionApplicationService = new VersionApplicationService();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        callback(null, this._versionApplicationService.getPackageVersion());
    }
}