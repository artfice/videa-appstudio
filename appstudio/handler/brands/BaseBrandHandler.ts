import {BrandRepository} from '../../repository/BrandRepository';
import {AppRepository} from '../../repository/AppRepository';
import {ClientConfigRepository} from '../../repository/ClientConfigRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {GalleryImageRepository} from '../../repository/GalleryImageRepository';
import {CloudinaryImageRepository as ImageRepository} from 'videa-framework/repository/cloudinary';
import {ScreenRepository} from '../../repository/ScreenRepository';
import {BrandApplicationService} from '../../application/BrandApplicationService';
import {AppApplicationService} from '../../application/AppApplicationService';
import {CloudinaryAdapterConfig} from 'videa-framework/adapter/image/cloudinary';
import {AccountBasedHandler} from 'videa-framework/handler';
import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import {ScreenApplicationService} from "../../application/ScreenApplicationService";
import {TranslatorList} from "../../translation/TranslatorList";
import {TranslationApplicationService} from "../../application/TranslationApplicationService";
import {EditionApplicationService} from "../../application/EditionApplicationService";
import {EditionStateApplicationService} from "../../application/EditionStateApplicationService";
import {AppPublishingApplicationService} from "../../application/AppPublishingApplicationService";
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db/mongodb';
import {IBrand} from '../../dto/brand/IBrand';

export class BaseBrandHandler extends AccountBasedHandler<IBrand> {
    protected _brandApplicationService : BrandApplicationService;

    constructor() {
        super();
        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'Brand',
            collectionName: 'Brands',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const cloudinaryAdapterConfig = <CloudinaryAdapterConfig>{
            cloudName: process.env.CLOUDINARY_CLOUD_NAME,
            apiKey: process.env.CLOUDINARY_API_KEY,
            apiSecret: process.env.CLOUDINARY_API_SECRET
        };

        const brandRepository = new BrandRepository(mongoAdapterConfig);
        const appRepository = new AppRepository(mongoAdapterConfig);
        const galleryImageRepository = new GalleryImageRepository(mongoAdapterConfig);
        const screenRepository = new ScreenRepository(mongoAdapterConfig);
        const editionRepository = new EditionRepository(mongoAdapterConfig);
        const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
        const clientConfigRepository = new ClientConfigRepository(mongoAdapterConfig);
        const imageRepository = new ImageRepository(cloudinaryAdapterConfig);

        const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);
        const eventRepository = new EventRepository(awsSnsAdapter);

        const screenApplicationService = new ScreenApplicationService({
            screenRepository: screenRepository,
            uiConfigRepository: uiConfigRepository,
            eventRepository: eventRepository
        });

        const translatorList = new TranslatorList(galleryImageRepository,
            screenApplicationService, uiConfigRepository);

        const translationApplicationService = new TranslationApplicationService();
        translationApplicationService.registerTranslators(translatorList.getList());

        const editionApplicationService = new EditionApplicationService({
            uiConfigRepository: new UIConfigRepository(mongoAdapterConfig),
            screenRepository: screenRepository,
            editionRepository: editionRepository,
            clientConfigRepository: clientConfigRepository,
            appRepository: appRepository,
            editionStateApplicationService: new EditionStateApplicationService(),
            appPublishingApplicationService: new AppPublishingApplicationService({
                editionRepository: editionRepository,
                clientConfigRepository: clientConfigRepository,
                appRepository: appRepository,
                translationService: translationApplicationService
            }),
        });

        const appApplicationService = new AppApplicationService({
            uiConfigRepository: uiConfigRepository,
            editionRepository: editionRepository,
            appRepository: appRepository,
            brandRepository: brandRepository,
            clientConfigRepository: clientConfigRepository,
            screenRepository: screenRepository,
            editionApplicationService: editionApplicationService
        });

        this._brandApplicationService = new BrandApplicationService({
            brandRepository: brandRepository,
            appRepository: appRepository,
            galleryImageRepository: galleryImageRepository,
            imageRepository: imageRepository,
            appService: appApplicationService
        });
    }
}