import { BaseBrandHandler } from './BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class GetBrandHandler extends BaseBrandHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._brandApplicationService.getBrandById(event.path.accountId, event.path.brandId)
            .then((brand) => {
                context.done(null, brand);
            }).catch((err) => {
                Logger.info('Failed to get brand.', err);
                context.done(err);
            });
    };
}