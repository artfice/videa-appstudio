import { BaseBrandHandler } from './BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class CreateBrandHandler extends BaseBrandHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this.getRequest(event)
            .then((request) => {
                let imageFile = <any>{
                    buffer: request.body.image
                };

                return this._brandApplicationService.create(event.path.accountId, request.body.name, imageFile);
            })
            .then((brand) => {
                Logger.debug('Created brand.', brand);
                context.done(null, brand);
            })
            .catch((err) => {
                Logger.info('Failed to create a brand.', err);
                context.done(err);
            });
    };
}