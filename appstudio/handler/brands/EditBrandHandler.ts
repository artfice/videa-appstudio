import { BaseBrandHandler } from './BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class EditBrandHandler extends BaseBrandHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this.getRequest(event)
            .then((request) => {
                let imageFile = <any>{
                    buffer: request.body.image
                };

                return this._brandApplicationService.updateBrand(event.path.accountId, event.path.brandId, request.body.name, imageFile);
            })
            .then((brand) => {
                Logger.debug('Updated brand.', brand);
                context.done(null, brand);
            })
            .catch((err) => {
                Logger.info('Failed to update a brand.', err);
                context.done(err);
            });
    };
}