import { BaseBrandHandler } from './BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class DeleteBrandHandler extends BaseBrandHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._brandApplicationService.removeBrand(event.path.accountId, event.path.brandId)
            .then((brand) => {
                Logger.debug('Removed brand', brand);
                context.done(null, brand);
            })
            .catch((err) => {
                Logger.info('Failed to find all brands', err);
                context.done(err);
            });
    };
}