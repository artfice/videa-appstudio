import { BaseBrandHandler } from './BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class GetAllBrandHandler extends BaseBrandHandler {
    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const searchOptions = {};

        this._brandApplicationService.search(event.path.accountId, <any>searchOptions)
            .then((brands) => {
                Logger.debug('Found brands', brands);
                context.done(null, brands);
            })
            .catch((err) => {
                Logger.info('Failed to find all brands', err);
                context.done(err);
            });
    };
}