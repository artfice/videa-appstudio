import {AccountBasedHandler} from 'videa-framework/handler';
import {ScreenRepository} from '../../repository/ScreenRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {UIConfigApplicationService} from './../../application/UIConfigApplicationService';
import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db';
import {UIConfig} from '../../dto/app/configuration/UIConfig';
import {BaseMenu} from '../../dto/common/navigation/BaseMenu';

export class BaseUIConfigHandler extends AccountBasedHandler<UIConfig<BaseMenu>> {

    protected _uiConfigApplicationService: UIConfigApplicationService;

    constructor() {
        super();
        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'Config',
            collectionName: 'Configs',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);

        const editionRepository = new EditionRepository(mongoAdapterConfig);
        const screenRepository = new ScreenRepository(mongoAdapterConfig);
        const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
        const eventRepository = new EventRepository(awsSnsAdapter);

        this._uiConfigApplicationService = new UIConfigApplicationService({
            screenRepository: screenRepository,
            uiConfigRepository: uiConfigRepository,
            editionRepository: editionRepository,
            eventRepository: eventRepository
        });
    }
}