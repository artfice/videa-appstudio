import {BaseUIConfigHandler} from './BaseUIConfigHandler';

export class CreateUIConfigHandler extends BaseUIConfigHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const editionId = event.path.editionId;
        const body = event.body;
        this._uiConfigApplicationService.addConfig(accountId, editionId, body).then((uiConfig) => {
            callback(null, uiConfig);
        }).catch((err) => {
            console.log('Fail to create uiConfig', err);
            callback(err);
        });
    };
}