import {BaseUIConfigHandler} from './BaseUIConfigHandler';

export class DeleteUIConfigHandler extends BaseUIConfigHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const configId = event.path.configId;
        const editionId = event.path.editionId;
        this._uiConfigApplicationService.removeConfig(accountId, editionId,
            configId).then((deleteResult) => {
                callback(null, deleteResult);
            }).catch((err) => {
                console.log('Fail to delete uiConfig', err);
                callback(err);
            });
    };
}