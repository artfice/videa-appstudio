import {BaseUIConfigHandler} from './BaseUIConfigHandler';

export class DuplicateUIConfigHandler extends BaseUIConfigHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const configId = event.path.configId;
        const editionId = event.path.editionId;
        this._uiConfigApplicationService.duplicateConfig(accountId, editionId,
            configId).then((uiConfig) => {
                callback(null, uiConfig);
            }).catch((err) => {
                console.log('Fail to duplicate uiConfig', err);
                callback(err);
            });
    };
}