import {BaseUIConfigHandler} from './BaseUIConfigHandler';

export class EditUIConfigHandler extends BaseUIConfigHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const configId = event.path.configId;
        const editionId = event.path.editionId;
        const body = event.body;
        this._uiConfigApplicationService.updateConfig(accountId, editionId,
            configId, body).then((updatedResult) => {
                callback(null, updatedResult);
            }).catch((err) => {
                console.log('Fail to edit uiConfig', err);
                callback(err);
            });
    };
}