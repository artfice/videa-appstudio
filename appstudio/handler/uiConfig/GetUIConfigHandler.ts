import {BaseUIConfigHandler} from './BaseUIConfigHandler';

export class GetUIConfigHandler extends BaseUIConfigHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);
        const accountId = event.path.accountId;
        const configId = event.path.configId;
        this._uiConfigApplicationService.getConfigById(accountId, configId).then((uiConfig) => {
            callback(null, uiConfig);
        }).catch((err) => {
            console.log('Fail to find uiConfig', err);
            callback(err);
        });
    };
}