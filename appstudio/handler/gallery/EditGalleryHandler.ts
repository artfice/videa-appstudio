import { BaseBrandHandler } from '../brands/BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class EditGalleryHandler extends BaseBrandHandler {
    constructor() {
        super();
    }
    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this.getRequest(event)
            .then((request) => {
                let imageFile = <any>{
                    buffer: request.body.image
                };

                return this._brandApplicationService.replaceGalleryImage(event.path.accountId,
                    event.path.brandId, event.path.galleryImageId, request.body.name, imageFile);
            })
            .then((brand) => {
                Logger.debug('Edited gallery image', brand);
                context.done(null, brand);
            })
            .catch((err) => {
                Logger.info('Failed to edit a gallery image', err);
                context.done(err);
            });
    };
}