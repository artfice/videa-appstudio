import { BaseBrandHandler } from '../brands/BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class GetGalleryHandler extends BaseBrandHandler {
    constructor() {
        super();
    }
    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._brandApplicationService.getGallery(event.path.accountId, event.path.brandId)
            .then((brand) => {
                context.done(null, brand);
            })
            .catch((err) => {
                Logger.info('Failed to find gallery', err);
                context.done(err);
            });
    };
}