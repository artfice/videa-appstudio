import { BaseBrandHandler } from '../brands/BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class CreateGalleryHandler extends BaseBrandHandler {

    public static IMAGE_TYPE_ID = 'f599e673-ff65-4b48-bfcb-35a1b74100f2';

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this.getRequest(event)
            .then((request) => {
                let imageFile = <any>{
                    buffer: request.body.image
                };

                return this._brandApplicationService.addGalleryImage(event.path.accountId,
                    event.path.brandId, CreateGalleryHandler.IMAGE_TYPE_ID, request.body.name, imageFile);
            })
            .then((brand) => {
                Logger.debug('Created gallery image', brand);
                context.done(null, brand);
            })
            .catch((err) => {
                Logger.info('Failed to create a gallery image', err);
                context.done(err);
            });
    };
}