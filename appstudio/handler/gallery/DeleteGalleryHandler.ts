import { BaseBrandHandler } from '../brands/BaseBrandHandler';
import { Logger } from 'videa-framework/Logger';

export class DeleteGalleryHandler extends BaseBrandHandler {

    constructor() {
        super();
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);

        this._brandApplicationService.removeGalleryImage(event.path.accountId, event.path.brandId, event.path.galleryImageId)
            .then((brand) => {
                context.done(null, brand);
            })
            .catch((err) => {
                Logger.info('Failed to delete gallery', err);
                context.done(err);
            });
    };
}