import {BaseScreenHandler} from './BaseScreenHandler';

export class EditScreenHandler extends BaseScreenHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);

        const accountId = event.path.accountId;
        const configId = event.path.configId;

        //TODO: force body to be required
        this._screenApplicationService.updateScreen(accountId, configId, event.body).then((screen) => {
            callback(null, screen);
        }).catch((err) => {
            console.log('Fail to create screen', err);
            callback(err);
        });
    };
}