import {BaseScreenHandler} from './BaseScreenHandler';

export class GetScreenHandler extends BaseScreenHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);

        this._screenApplicationService.getScreens(event.path.accountId, event.path.configId).then((screens) => {
            callback(null, {
                data: screens
            });
        }).catch((err) => {
            console.log('Fail to find screens', err);
            callback(err);
        });
    };
}