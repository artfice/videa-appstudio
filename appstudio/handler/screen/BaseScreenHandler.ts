import {AccountBasedHandler} from 'videa-framework/handler';
import {ScreenRepository} from '../../repository/ScreenRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import {ScreenApplicationService} from '../../application/ScreenApplicationService';
import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db';
import {Screen} from '../../dto/app/configuration/uiconfig/Screen';
import {Component} from '../../dto/app/configuration/uiconfig/screen/component/Component';
import {ScreenStyle} from '../../dto/app/configuration/uiconfig/screen/ScreenStyle';
import {ScreenContent} from '../../dto/app/configuration/uiconfig/screen/ScreenContent';
import {BaseStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';

export class BaseScreenHandler extends AccountBasedHandler<Screen<ScreenStyle, Component<BaseStyle>, ScreenContent>> {
    protected _screenApplicationService: ScreenApplicationService;

    constructor() {

        super();

        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'Screen',
            collectionName: 'Screens',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);

        const screenRepository = new ScreenRepository(mongoAdapterConfig);
        const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
        const eventRepository = new EventRepository(awsSnsAdapter);

        this._screenApplicationService = new ScreenApplicationService({
            screenRepository: screenRepository,
            uiConfigRepository: uiConfigRepository,
            eventRepository: eventRepository
        });
    }
}