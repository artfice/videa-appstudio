import {BaseScreenHandler} from './BaseScreenHandler';

export class DeleteScreenHandler extends BaseScreenHandler {
    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);

        this._screenApplicationService.deleteScreen(event.path.accountId, event.path.configId, event.path.screenId).then((undefined) => {
            callback(null, undefined);
        }).catch((err) => {
            console.log('Fail to create screen', err);
            callback(err);
        });
    };
}