import {BaseScreenHandler} from './BaseScreenHandler';

export class CreateScreenHandler extends BaseScreenHandler {

    constructor() {
        super();
    }

    public handler (event, context, callback) {
        super.handler(event, context, callback);

        //TODO: force body to be required
        this._screenApplicationService.createScreen(event.path.accountId, event.path.configId, event.body).then((screen) => {
            callback(null, screen);
        }).catch((err) => {
            console.log('Fail to create screen', err);
            callback(err);
        });
    };
}