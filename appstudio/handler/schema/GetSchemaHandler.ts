import { BaseHandler } from 'videa-framework/handler/BaseHandler';
import { SchemaApplicationService } from 'videa-framework/application/SchemaApplicationService';

export class GetSchemaHandler extends BaseHandler {

    private _schemaApplicationService: SchemaApplicationService;

    public constructor() {
        super();
        this._schemaApplicationService = new SchemaApplicationService(process.env.BASE_SCHEMA_PATH);
    }

    public handler(event, context, callback) {
        super.handler(event, context, callback);
        const schemaId = event.path.schemaId;
        this._schemaApplicationService.getSchema(schemaId).then((schema) => {
            callback(null, schema);
        }).catch((err) => {
            callback(err);
        });
    }
}