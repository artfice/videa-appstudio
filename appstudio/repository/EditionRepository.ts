import { AccountBaseDtoRepository } from 'videa-framework/repository/mongodb';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';

import {Edition} from '../dto/Edition';

export class EditionRepository extends AccountBaseDtoRepository<Edition> {
    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
        this._dtoName = 'Edition';
        this._collectionName = this._dtoName + 's';
    }
}