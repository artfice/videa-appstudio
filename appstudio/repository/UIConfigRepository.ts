import { AccountBaseDtoRepository } from 'videa-framework/repository/mongodb';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';

import {UIConfig as Config} from '../dto/app/configuration/UIConfig';
import {BaseMenu as BaseMenu} from '../dto/common/navigation/BaseMenu';

export class UIConfigRepository extends AccountBaseDtoRepository<Config<BaseMenu>> {

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
        this._dtoName = 'Config';
        this._collectionName = this._dtoName + 's';
    }
}