import shortid = require('shortid');

import { AccountBaseDtoRepository } from 'videa-framework/repository/mongodb';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';

import { BaseDto } from 'videa-framework/domain/BaseDto';

export class ClientConfigRepository extends AccountBaseDtoRepository<BaseDto> {

    protected generateIdForDto(dto: any): string {
        return shortid.generate();
    }

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
        this._dtoName = 'ClientConfig';
        this._collectionName = this._dtoName + 's';
    }
}