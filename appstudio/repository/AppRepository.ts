import { AccountBaseDtoRepository } from 'videa-framework/repository/mongodb';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';
import {App} from '../dto/App';

export class AppRepository extends AccountBaseDtoRepository<App> {
    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
        this._dtoName = 'App';
        this._collectionName = this._dtoName + 's';
    }
}