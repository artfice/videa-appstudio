import { AccountBaseDtoRepository } from 'videa-framework/repository/mongodb';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';

import {GalleryImage} from '../dto/common/GalleryImage';

export class GalleryImageRepository extends AccountBaseDtoRepository<GalleryImage> {
    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
        this._dtoName = 'GalleryImage';
        this._collectionName = this._dtoName + 's';
    }
}