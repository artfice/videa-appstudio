import { AccountBaseDtoRepository } from 'videa-framework/repository/mongodb';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';
import {IBrand as Brand}  from '../dto/brand/IBrand';

export class BrandRepository extends AccountBaseDtoRepository<Brand> {

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
        this._dtoName = 'Brand';
        this._collectionName = this._dtoName + 's';
    }
}