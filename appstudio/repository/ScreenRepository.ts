import { AccountBaseDtoRepository } from 'videa-framework/repository/mongodb';
import { DtoMongoDbAdapterConfig } from 'videa-framework/adapter/db/mongodb';

import {Screen} from '../dto/app/configuration/uiconfig/Screen';
import {Component} from '../dto/app/configuration/uiconfig/screen/component/Component';
import {ScreenStyle} from '../dto/app/configuration/uiconfig/screen/ScreenStyle';
import {ScreenContent} from '../dto/app/configuration/uiconfig/screen/ScreenContent';
import {BaseStyle} from '../dto/app/configuration/uiconfig/screen/component/style/BaseStyle';

export class ScreenRepository extends AccountBaseDtoRepository<Screen<ScreenStyle, Component<BaseStyle>, ScreenContent>> {

    constructor(config: DtoMongoDbAdapterConfig) {
        super(config);
    }
}