import chai = require('chai');
const expect = chai.expect;

import {ScreenRepository} from '../../repository/ScreenRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {AppRepository} from '../../repository/AppRepository';
import {GalleryImageRepository} from '../../repository/GalleryImageRepository';
import {ClientConfigRepository} from '../../repository/ClientConfigRepository';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import {EditionApplicationService} from '../../application/EditionApplicationService';
import {ScreenApplicationService} from '../../application/ScreenApplicationService';
import {EditionStateApplicationService} from '../../application/EditionStateApplicationService';
import {AppPublishingApplicationService} from '../../application/AppPublishingApplicationService';
import {TranslationApplicationService} from '../../application/TranslationApplicationService';
import {TranslatorList} from '../../translation/TranslatorList';
import {Edition} from '../../dto/Edition';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db';

import * as TestSetup from './TestSetup';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

describe('editionApplicationServiceIntegration Test', () => {

        const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
            dtoName: 'Edition',
            collectionName: 'Editions',
            host: process.env.MONGO_HOST,
            database: process.env.DATABASE,
            port: process.env.PORT,
            authSource: process.env.AUTH_SOURCE,
            extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
        };

        const screenRepository = new ScreenRepository(mongoAdapterConfig);
        const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
        const appRepository = new AppRepository(mongoAdapterConfig);
        const clientConfigRepository = new ClientConfigRepository(mongoAdapterConfig);
        const editionRepository = new EditionRepository(mongoAdapterConfig);
        const galleryImageRepository = new GalleryImageRepository(mongoAdapterConfig);
        const eventRepository = new EventRepository({});

        const editionStateApplicationService = new EditionStateApplicationService();

        const screenApplicationService = new ScreenApplicationService({
            screenRepository: screenRepository,
            uiConfigRepository: uiConfigRepository,
            eventRepository: eventRepository
        });

        const translatorList = new TranslatorList(galleryImageRepository,
        screenApplicationService, uiConfigRepository);

        const translationApplicationService = new TranslationApplicationService();
        translationApplicationService.registerTranslators(translatorList.getList());

        const appPublishingApplicationService = new AppPublishingApplicationService({
            appRepository: appRepository,
            editionRepository: editionRepository,
            clientConfigRepository: clientConfigRepository,
            translationService: translationApplicationService
        });

        const editionApplicationService = new EditionApplicationService({
          uiConfigRepository: uiConfigRepository,
          editionRepository: editionRepository,
          editionStateApplicationService: editionStateApplicationService,
          appRepository: appRepository,
          clientConfigRepository: clientConfigRepository,
          screenRepository: screenRepository,
          appPublishingApplicationService:appPublishingApplicationService
        });

    const sampleEdition = AppstudioModelFactory.create('MobileAppEdition', {
        name: 'NewEdition'
    });

    describe('GIVEN: An Invalid Edition Collection', () => {
        describe('WHEN: Retrieving a Edition', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                editionApplicationService.getById(TestSetup.accountId, 'some ID').then(done).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Removing an Edition', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                editionApplicationService.remove(TestSetup.accountId, 'some brand', 'some ID').then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Updating an Edition', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                editionApplicationService.update(TestSetup.accountId, 'some app ID', <Edition>{}).then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Adding an Edition', () => {
            const appId = 'some app';
            it('THEN: An ItemNotFound Error should occur', (done) => {
                editionApplicationService.create(TestSetup.accountId, appId, <Edition>sampleEdition).then(
                    (newEdition) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Duplicating an Edition', () => {
            const appId = 'some app';
            const editionId = 'some edition';
            it('THEN: An ItemNotFound Error should occur', (done) => {
                editionApplicationService.duplicateEdition(TestSetup.accountId, appId, editionId).then(
                    (newEdition) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Retrieving all Edition By App', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                editionApplicationService.getByApp(TestSetup.accountId, 'some APP ID').then(
                    (editions) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
    });
    describe('GIVEN: A Collections of Editions', () => {
        describe('WHEN: Retrieving an Edition', () => {
            it('THEN: The name should be First Edition', (done) => {
                editionApplicationService.getById(TestSetup.accountId, '06d1e690-818d-11e6-b928-f13513bdb337').then(
                    (edition) => {
                        expect(edition.name).to.equal('First Edition');
                        expect(edition.state).to.equal('IN_PROGRESS');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Retrieving all Edition By App', () => {
            it('THEN: The number of editions found should be 4', (done) => {
                editionApplicationService.getByApp(TestSetup.accountId, '543dc5d0-00f9-11e6-b786-132120879927').then(
                    (editions) => {
                        expect(editions.data.length).to.equal(5);
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Creating an Edition', () => {
            it('THEN: The Edition will also create a uiConfig', (done) => {
                editionApplicationService.create(TestSetup.accountId, '543dc5d0-00f9-11e6-b786-132120879927', <any>sampleEdition).then(
                    (edition) => {
                        expect(edition.name).to.equal('NewEdition');
                        expect(edition.uiConfig.length).to.equal(1);
                        return uiConfigRepository.getById(TestSetup.accountId, edition.uiConfig[0]);
                    }).then((config) => {
                        expect(config.name).to.equal('Version');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        // describe('WHEN: Duplicating an Edition', () => {
        //     let duplicatedEdition;
        //     let duplicatedConfig;
        //     it('THEN: A new Edition will be created with new uiConfig and new Screens and new App', (done) => {
        //         editionApplicationService.duplicateEdition(TestSetup.accountId, '543dc5d0-00f9-11e6-b786-132120879927', '06d1e690-818d-11e6-b928-f13513bdb337').then(
        //             (edition) => {
        //                 duplicatedEdition = edition;
        //                 expect(duplicatedEdition.name).to.equal('First Edition Copy');
        //                 expect(duplicatedEdition.state).to.equal('IN_PROGRESS');
        //                 expect(duplicatedEdition.uiConfig.length).to.equal(1);
        //                 return uiConfigRepository.getById(TestSetup.accountId, duplicatedEdition.uiConfig[0]);
        //             }).then((config) => {
        //                 duplicatedConfig = <any>config;
        //                 expect(duplicatedConfig.editionId).to.equal(duplicatedEdition.id);
        //                 return screenRepository.getById(TestSetup.accountId, duplicatedConfig.screen[0]);
        //             }).then((screen: any) => {
        //                 expect(screen.configId).to.equal(duplicatedConfig.id);
        //                 done();
        //             }).catch((err) => {
        //                 done(err);
        //             });
        //     });
        // });
        describe('WHEN: Deleting an Edition', () => {
            it('THEN: The edition should not exist', (done) => {
                editionApplicationService.remove(TestSetup.accountId,
                    '543dc5d0-00f9-11e6-b786-132120879927',
                    '06d1e690-818d-11e6-b928-f13513bdb337').then(
                    () => {
                        return editionRepository.getById(TestSetup.accountId, '06d1e690-818d-11e6-b928-f13513bdb337');
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
            it('THEN: The configs should not exist', (done) => {
                uiConfigRepository.getById(TestSetup.accountId, '06d17160-818d-11e6-b928-f13513bdb337')
                    .then(done).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
            it('THEN: The screen should not exist', (done) => {
                screenRepository.getById(TestSetup.accountId, '00618fa0-6a3a-11e6-b5ef-41279ba710b1')
                    .then(done).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
            it('THEN: The edition should not show up in app', (done) => {
                appRepository.getById(TestSetup.accountId, '543dc5d0-00f9-11e6-b786-132120879927')
                    .then((app) => {
                        expect(app.edition.indexOf('06d1e690-818d-11e6-b928-f13513bdb337')).to.equal(-1);
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
    });
});
