import chai = require('chai');
const expect = chai.expect;
import {ScreenRepository} from '../../repository/ScreenRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {ScreenApplicationService} from '../../application/ScreenApplicationService';
import {MockEventRepository} from 'videa-framework/repository';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db';

import * as TestSetup from './TestSetup';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

describe('screenApplicationService Test', () => {
    const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
        dtoName: 'Screen',
        collectionName: 'Screens',
        host: process.env.MONGO_HOST,
        database: process.env.DATABASE,
        port: process.env.PORT,
        authSource: process.env.AUTH_SOURCE,
        extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
    };

    const screenRepository = new ScreenRepository(mongoAdapterConfig);
    const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
    const eventRepository = new MockEventRepository({});

    const screenApplicationService = new ScreenApplicationService({
        screenRepository: screenRepository,
        uiConfigRepository: uiConfigRepository,
        eventRepository: eventRepository
    });

    const sampleMobileScreen = AppstudioModelFactory.create('MobileScreen', {
        name: 'NewScreen',
        item: []
    });
    const sampleTabletScreen = AppstudioModelFactory.create('TabletScreen', {
        name: 'NewScreen',
        item: []
    });
    const sampleTVScreen = AppstudioModelFactory.create('TVScreen', {
        name: 'NewScreen',
        item: []
    });

    describe('GIVEN: An Invalid Screen Collection', () => {
        describe('WHEN: Retrieving Screens from UIConfig', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                screenApplicationService.getScreens(TestSetup.accountId, 'some ID').then((screens: any) => {
                    expect(screens.data.length).to.equal(0);
                    done();
                }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Removing a Screen', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                screenApplicationService.deleteScreen(TestSetup.accountId, 'some config id', 'some ID').then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Updating a Screen', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                screenApplicationService.updateScreen(TestSetup.accountId, 'some id', <any>{}).then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Adding a Screen', () => {
            const configId = 'some config';
            it('THEN: An ItemNotFound Error should occur', (done) => {
                screenApplicationService.createScreen(TestSetup.accountId, configId, <any>sampleMobileScreen).then(
                    (newScreen) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
    });
    describe('GIVEN: A Collections of Screens', () => {
        describe('WHEN: Retrieving an Screen', () => {
            it('THEN: The name should be Live 3', (done) => {
                screenApplicationService.getScreens(TestSetup.accountId, '470906e0-81da-11e6-8af5-8746d438ccb5').then(
                    (screens: any) => {
                        expect(screens.length).to.equal(1);
                        return screenRepository.getById(TestSetup.accountId, screens[0].id);
                    }).then((screen) => {
                        expect(screen.name).to.equal('Live 4');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Creating an Screen', () => {
            it('THEN: The MobileScreen name will be NewScreen', (done) => {
                screenApplicationService.createScreen(TestSetup.accountId, '6706e400-81da-11e6-8af5-8746d438ccb5', <any>sampleMobileScreen).then(
                    (screen: any) => {
                        expect(screen.name).to.equal('NewScreen');
                        expect(screen.configId).to.equal('6706e400-81da-11e6-8af5-8746d438ccb5');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
            it('THEN: The TabletScreen name will be NewScreen', (done) => {
                screenApplicationService.createScreen(TestSetup.accountId, '6706e400-81da-11e6-8af5-8746d438ccb5', <any>sampleTabletScreen).then(
                    (screen: any) => {
                        expect(screen.name).to.equal('NewScreen');
                        expect(screen.configId).to.equal('6706e400-81da-11e6-8af5-8746d438ccb5');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
            it('THEN: The TVScreen name will be NewScreen', (done) => {
                screenApplicationService.createScreen(TestSetup.accountId, '6706e400-81da-11e6-8af5-8746d438ccb5', <any>sampleTVScreen).then(
                    (screen: any) => {
                        expect(screen.name).to.equal('NewScreen');
                        expect(screen.configId).to.equal('6706e400-81da-11e6-8af5-8746d438ccb5');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Deleting an Screen', () => {
            it('THEN: The UIConfig should not exist', (done) => {
                screenApplicationService.deleteScreen(TestSetup.accountId,
                    '370906e0-81da-11e6-8af5-8746d438ccb5',
                    '170c3b30-81da-11e6-8af5-8746d438ccb7').then(
                    () => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
            it('THEN: The edition should not show up in app', (done) => {
                uiConfigRepository.getById(TestSetup.accountId, '370906e0-81da-11e6-8af5-8746d438ccb5')
                    .then((config: any) => {
                        expect(config.screen.indexOf('170c3b30-81da-11e6-8af5-8746d438ccb7')).to.equal(-1);
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Updating an SCREEN', () => {
            it('THEN: The Screen will have new name', (done) => {
                screenRepository.getById(TestSetup.accountId, '170c3b30-81da-11e6-8af5-8746d438ccb8').then(
                    (screen) => {
                        screen.name = 'UPDATED SCREEN';
                        return screenApplicationService.updateScreen(TestSetup.accountId, '470906e0-81da-11e6-8af5-8746d438ccb5', <any>screen);
                    }).then((updatedResult) => {
                        return screenRepository.getById(TestSetup.accountId, '170c3b30-81da-11e6-8af5-8746d438ccb8');
                    }).then((screen) => {
                        expect(screen.name).to.equal('UPDATED SCREEN');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
    });
});