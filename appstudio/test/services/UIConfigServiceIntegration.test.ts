import chai = require('chai');
const expect = chai.expect;

import {ScreenRepository} from '../../repository/ScreenRepository';
import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {UIConfigApplicationService} from './../../application/UIConfigApplicationService';
import {MockEventRepository} from 'videa-framework/repository/MockEventRepository';
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db';

import * as TestSetup from './TestSetup';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

describe('UIConfigApplicationService Test', () => {
    const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
        dtoName: 'Config',
        collectionName: 'Configs',
        host: process.env.MONGO_HOST,
        database: process.env.DATABASE,
        port: process.env.PORT,
        authSource: process.env.AUTH_SOURCE,
        extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
    };
    const editionRepository = new EditionRepository(mongoAdapterConfig);
    const screenRepository = new ScreenRepository(mongoAdapterConfig);
    const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
    const eventRepository = new MockEventRepository({});

    const uiConfigApplicationService = new UIConfigApplicationService({
        screenRepository: screenRepository,
        uiConfigRepository: uiConfigRepository,
        editionRepository: editionRepository,
        eventRepository: eventRepository
    });

    const sampleMobileUIConfig = AppstudioModelFactory.create('MobileUIConfig', {
        name: 'NewVersion'
    });
    const sampleTabletUIConfig = AppstudioModelFactory.create('TabletUIConfig', {
        name: 'NewVersion'
    });
    const sampleTVUIConfig = AppstudioModelFactory.create('TVUIConfig', {
        name: 'NewVersion'
    });
    const sampleExternalUIConfig = AppstudioModelFactory.create('ExternalUIConfig', {
        name: 'NewVersion',
        url: 'test'
    });

    describe('GIVEN: An Invalid UIConfig Collection', () => {
        describe('WHEN: Retrieving a UIConfig', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                uiConfigApplicationService.getConfigById(TestSetup.accountId, 'some ID').then(done).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Removing an UIConfig', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                uiConfigApplicationService.removeConfig(TestSetup.accountId, 'some app', 'some ID').then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Updating an UIConfig', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                uiConfigApplicationService.updateConfig(TestSetup.accountId, 'some edition id', 'some config ID', <any>{}).then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Adding an UIConfig', () => {
            const appId = 'some app';
            it('THEN: An ItemNotFound Error should occur', (done) => {
                uiConfigApplicationService.addConfig(TestSetup.accountId, appId, <any>sampleMobileUIConfig).then(
                    (newConfig) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
    });
    describe('GIVEN: A Collections of UIConfigs', () => {
        describe('WHEN: Retrieving an UIConfig', () => {
            it('THEN: The name should be Fifth Config', (done) => {
                uiConfigApplicationService.getConfigById(TestSetup.accountId, '6706e400-81da-11e6-8af5-8746d438ccb5').then(
                    (config) => {
                        expect(config.name).to.equal('Fifth Config');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Creating an UIConfig', () => {
            it('THEN: The MobileUIConfig will have a name NewVersion', (done) => {
                uiConfigApplicationService.addConfig(TestSetup.accountId, '1706e400-81da-11e6-8af5-8746d438ccb5', <any>sampleMobileUIConfig).then(
                    (config) => {
                        expect(config.name).to.equal('NewVersion');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
            it('THEN: The TabletUIConfig will have a name NewVersion', (done) => {
                uiConfigApplicationService.addConfig(TestSetup.accountId, '1706e400-81da-11e6-8af5-8746d438ccb5', <any>sampleTabletUIConfig).then(
                    (config) => {
                        expect(config.name).to.equal('NewVersion');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
            it('THEN: The TVUIConfig will have a name NewVersion', (done) => {
                uiConfigApplicationService.addConfig(TestSetup.accountId, '1706e400-81da-11e6-8af5-8746d438ccb5', <any>sampleTVUIConfig).then(
                    (config) => {
                        expect(config.name).to.equal('NewVersion');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
            it('THEN: The External UIConfig will have a name NewVersion and url test', (done) => {
                uiConfigApplicationService.addConfig(TestSetup.accountId, '1706e400-81da-11e6-8af5-8746d438ccb5', <any>sampleExternalUIConfig).then(
                    (config: any) => {
                        expect(config.name).to.equal('NewVersion');
                        expect(config.url).to.equal('test');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Deleting an UIConfig', () => {
            it('THEN: The UIConfig should not exist', (done) => {
                uiConfigApplicationService.removeConfig(TestSetup.accountId,
                    '1706e400-81da-11e6-8af5-8746d438ccb5',
                    '170906e0-81da-11e6-8af5-8746d438ccb5').then(
                    () => {
                        return uiConfigRepository.getById(TestSetup.accountId, '170906e0-81da-11e6-8af5-8746d438ccb5');
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
            it('THEN: The edition should not show up in app', (done) => {
                editionRepository.getById(TestSetup.accountId, '1706e400-81da-11e6-8af5-8746d438ccb5')
                    .then((edition) => {
                        expect(edition.uiConfig.indexOf('170906e0-81da-11e6-8af5-8746d438ccb5')).to.equal(-1);
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Updating an UIConfig', () => {
            it('THEN: The UIConfig will have new name', (done) => {
                uiConfigRepository.getById(TestSetup.accountId, '270906e0-81da-11e6-8af5-8746d438ccb5').then(
                    (config) => {
                        config.name = 'UPDATED UICONFIG';
                        return uiConfigApplicationService.updateConfig(TestSetup.accountId, '2706e400-81da-11e6-8af5-8746d438ccb1', config.id, config);
                    }).then((updatedResult) => {
                        return uiConfigRepository.getById(TestSetup.accountId, '270906e0-81da-11e6-8af5-8746d438ccb5');
                    }).then((config) => {
                        expect(config.name).to.equal('UPDATED UICONFIG');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
    });
});