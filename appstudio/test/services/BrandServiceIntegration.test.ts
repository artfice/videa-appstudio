import chai = require('chai');
const expect = chai.expect;


import {BrandApplicationService} from '../../application/BrandApplicationService';
import {AppApplicationService} from '../../application/AppApplicationService';

import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {ScreenRepository} from '../../repository/ScreenRepository';
import {AppRepository} from '../../repository/AppRepository';
import {BrandRepository} from '../../repository/BrandRepository';
import {ClientConfigRepository} from '../../repository/ClientConfigRepository';
import {MockImageRepository} from 'videa-framework/repository/MockImageRepository';
import {GalleryImageRepository} from '../../repository/GalleryImageRepository';
import {MongoDbImageTypeRepository as ImageTypeRepository} from 'videa-framework/repository/mongodb/MongoDbImageTypeRepository';

import * as TestSetup from './TestSetup';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import {ScreenApplicationService} from "../../application/ScreenApplicationService";
import {TranslatorList} from "../../translation/TranslatorList";
import {TranslationApplicationService} from "../../application/TranslationApplicationService";
import {EditionApplicationService} from "../../application/EditionApplicationService";
import {EditionStateApplicationService} from "../../application/EditionStateApplicationService";
import {AppPublishingApplicationService} from "../../application/AppPublishingApplicationService";
import {DtoMongoDbAdapterConfig} from 'videa-framework/adapter/db';

describe('BrandApplicationService Test', () => {
    const mongoAdapterConfig: DtoMongoDbAdapterConfig = {
        dtoName: 'Brand',
        collectionName: 'Brands',
        host: process.env.MONGO_HOST,
        database: process.env.DATABASE,
        port: process.env.PORT,
        authSource: process.env.AUTH_SOURCE,
        extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
    };

    const imageTypeRepository = new ImageTypeRepository(mongoAdapterConfig);
    const brandRepository = new BrandRepository(mongoAdapterConfig);
    const appRepository = new AppRepository(mongoAdapterConfig);
    const galleryImageRepository = new GalleryImageRepository(mongoAdapterConfig);
    const screenRepository = new ScreenRepository(mongoAdapterConfig);
    const editionRepository = new EditionRepository(mongoAdapterConfig);
    const uiConfigRepository = new UIConfigRepository(mongoAdapterConfig);
    const clientConfigRepository = new ClientConfigRepository(mongoAdapterConfig);
    const mockImageRepository = new MockImageRepository(imageTypeRepository, {});

    const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);
    const eventRepository = new EventRepository(awsSnsAdapter);

    const screenApplicationService = new ScreenApplicationService({
        screenRepository: screenRepository,
        uiConfigRepository: uiConfigRepository,
        eventRepository: eventRepository
    });

    const translatorList = new TranslatorList(galleryImageRepository,
        screenApplicationService, uiConfigRepository);

    const translationApplicationService = new TranslationApplicationService();
    translationApplicationService.registerTranslators(translatorList.getList());

    const editionApplicationService = new EditionApplicationService({
        uiConfigRepository: new UIConfigRepository(mongoAdapterConfig),
        screenRepository: screenRepository,
        editionRepository: editionRepository,
        clientConfigRepository: clientConfigRepository,
        appRepository: appRepository,
        editionStateApplicationService: new EditionStateApplicationService(),
        appPublishingApplicationService: new AppPublishingApplicationService({
            editionRepository: editionRepository,
            clientConfigRepository: clientConfigRepository,
            appRepository: appRepository,
            translationService: translationApplicationService
        }),
    });

    const appApplicationService = new AppApplicationService({
        uiConfigRepository: uiConfigRepository,
        editionRepository: editionRepository,
        appRepository: appRepository,
        brandRepository: brandRepository,
        clientConfigRepository: clientConfigRepository,
        screenRepository: screenRepository,
        editionApplicationService: editionApplicationService
    });

    const brandApplicationService = new BrandApplicationService({
        brandRepository: brandRepository,
        appRepository: appRepository,
        galleryImageRepository: galleryImageRepository,
        imageRepository: mockImageRepository,
        appService: appApplicationService
    });

    const sampleBrand = AppstudioModelFactory.create('Brand', {
        name: 'NewBrand',
        app: [],
        gallery: []
    });

    describe('GIVEN: An Invalid Brand Collection', () => {
        describe('WHEN: Retrieving a Brand', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                brandApplicationService.getBrandById(TestSetup.accountId, 'some ID').then(done).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Removing a Brand', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                brandApplicationService.removeBrand(TestSetup.accountId, 'some brand').then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Updating an Brand', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                brandApplicationService.updateBrand(TestSetup.accountId, 'some brand ID', 'brand name', null).then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Adding an Brand', () => {
            const brandId = 'some brand';
            it('THEN: An ItemNotFound Error should occur', (done) => {
                brandApplicationService.create(TestSetup.accountId, brandId, <any>sampleBrand).then(
                    (newbrand) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
    });
    describe('GIVEN: A Collections of Brands', () => {
        describe('WHEN: Retrieving all Brand', () => {
            it('THEN: There should be 3 brands', (done) => {
                brandApplicationService.search(TestSetup.accountId, <any>{}).then(
                    (brands: any) => {
                        expect(brands.data.length).to.equal(4);
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Retrieving an Brand', () => {
            it('THEN: The name should be brand with no image', (done) => {
                brandApplicationService.getBrandById(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4').then(
                    (app) => {
                        expect(app.name).to.equal('brand with no image');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Creating an Brand', () => {
            it('THEN: The Brand should have name NewBrand', (done) => {
                brandApplicationService.create(TestSetup.accountId, 'NewBrand', null).then(
                    (brand) => {
                        expect(brand.name).to.equal('NewBrand');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Creating an Brand with image', () => {
            it('THEN: The Brand should have a logo', (done) => {
                brandApplicationService.create(TestSetup.accountId, 'NewBrand', {
                    originalname: 'test.png'
                }).then(
                    (result) => {
                        expect(result.name).to.equal('NewBrand');
                        expect(result.id.length > 0).to.be.equal(true);
                        expect(result.app.length == 0).to.be.equal(true);
                        expect(result.image.id.length > 0).to.be.equal(true);
                        expect(result.image.name.length > 0).to.be.equal(true);
                        expect(result.image.fullName.length > 0).to.be.equal(true);
                        expect(result.image.imageType.length > 0).to.be.equal(true);
                        expect(result.image.url.length > 0).to.be.equal(true);
                        expect(result.image.width > 0).to.be.equal(true);
                        expect(result.image.height > 0).to.be.equal(true);
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Deleting an Brand', () => {
            it('THEN: The Brand should not exist', (done) => {
                brandApplicationService.removeBrand(TestSetup.accountId,
                    '14faff10-80fc-11e6-a218-dd3f12ad4da4').then(() => {
                        return brandRepository.getById(TestSetup.accountId, '14faff10-80fc-11e6-a218-dd3f12ad4da4');
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
            it('THEN: The app should not exist', (done) => {
                appRepository.getById(TestSetup.accountId, '843dc5d0-00f9-11e6-b786-132120879927')
                    .then((app) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Updating an Brand', () => {
            it('THEN: The Brand will have new name', (done) => {
                brandRepository.getById(TestSetup.accountId, '44faff10-80fc-11e6-a218-dd3f12ad4da4').then(
                    (brand) => {
                        brand.name = 'UPDATED brand';
                        return brandApplicationService.updateBrand(TestSetup.accountId,
                            '44faff10-80fc-11e6-a218-dd3f12ad4da4',
                            'UPDATED brand', null);
                    }).then((updatedResult) => {
                        return brandRepository.getById(TestSetup.accountId,
                            '44faff10-80fc-11e6-a218-dd3f12ad4da4');
                    }).then((brand) => {
                        expect(brand.name).to.equal('UPDATED brand');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
    });
});