import chai = require('chai');
const expect = chai.expect;

import {AppApplicationService} from '../../application/AppApplicationService';

import {UIConfigRepository} from '../../repository/UIConfigRepository';
import {EditionRepository} from '../../repository/EditionRepository';
import {ScreenRepository} from '../../repository/ScreenRepository';
import {AppRepository} from '../../repository/AppRepository';
import {BrandRepository} from '../../repository/BrandRepository';
import {ClientConfigRepository} from '../../repository/ClientConfigRepository';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import * as TestSetup from './TestSetup';
import {GalleryImageRepository} from "../../repository/GalleryImageRepository";
import {ScreenApplicationService} from "../../application/ScreenApplicationService";
import {TranslationApplicationService} from "../../application/TranslationApplicationService";
import {TranslatorList} from "../../translation/TranslatorList";
import {EditionApplicationService} from "../../application/EditionApplicationService";
import {AppPublishingApplicationService} from "../../application/AppPublishingApplicationService";
import {EditionStateApplicationService} from "../../application/EditionStateApplicationService";
import {AwsSnsAdapter} from 'videa-framework/adapter/aws/AwsSnsAdapter';
import {EventRepository} from 'videa-framework/repository/EventRepository';
import * as _ from 'lodash';


describe('AppApplicationService Test', () => {
    const config: any = {
        host: process.env.MONGO_HOST,
        database: process.env.DATABASE,
        port: process.env.PORT,
        authSource: process.env.AUTH_SOURCE,
        extraOptions: (process.env.EXTRA_OPTIONS != 'test') ? process.env.EXTRA_OPTIONS : false
    };

    const screenRepository = new ScreenRepository(_.extend({
        dtoName: 'Screen',
        collectionName: 'Screens'
    }, config));
    const uiConfigRepository = new UIConfigRepository(_.extend({
        dtoName: 'Config',
        collectionName: 'Configs'
    }, config));
    const appRepository = new AppRepository(_.extend({
        dtoName: 'App',
        collectionName: 'Apps'
    }, config));
    const clientConfigRepository = new ClientConfigRepository(_.extend({
        dtoName: 'ClientConfig',
        collectionName: 'ClientConfigs'
    }, config));
    const editionRepository = new EditionRepository(_.extend({
        dtoName: 'Edition',
        collectionName: 'Editions'
    }, config));
    const brandRepository = new BrandRepository(_.extend({
        dtoName: 'Brand',
        collectionName: 'Brands'
    }, config));

    const galleryImageRepository = new GalleryImageRepository(_.extend({
        dtoName: 'GalleryImage',
        collectionName: 'GalleryImages'
    }, config));

    const awsSnsAdapter = new AwsSnsAdapter(process.env.AWS_REGION, process.env.AWS_ACCOUNTID);
    const eventRepository = new EventRepository(awsSnsAdapter);

    const screenApplicationService = new ScreenApplicationService({
        screenRepository: screenRepository,
        uiConfigRepository: uiConfigRepository,
        eventRepository: eventRepository
    });

    const translatorList = new TranslatorList(galleryImageRepository,
        screenApplicationService, uiConfigRepository);

    const translationApplicationService = new TranslationApplicationService();
    translationApplicationService.registerTranslators(translatorList.getList());

    const editionApplicationService = new EditionApplicationService({
        uiConfigRepository: uiConfigRepository,
        screenRepository: screenRepository,
        editionRepository: editionRepository,
        clientConfigRepository: clientConfigRepository,
        appRepository: appRepository,
        editionStateApplicationService: new EditionStateApplicationService(),
        appPublishingApplicationService: new AppPublishingApplicationService({
            editionRepository: editionRepository,
            clientConfigRepository: clientConfigRepository,
            appRepository: appRepository,
            translationService: translationApplicationService
        }),
    });

    const appApplicationService = new AppApplicationService({
        uiConfigRepository: uiConfigRepository,
        editionRepository: editionRepository,
        appRepository: appRepository,
        brandRepository: brandRepository,
        clientConfigRepository: clientConfigRepository,
        screenRepository: screenRepository,
        editionApplicationService: editionApplicationService
    });

    const sampleMobileApp = AppstudioModelFactory.create('MobileApp', {
        name: 'NewApp'
    });
    const sampleTabletApp = AppstudioModelFactory.create('TabletApp', {
        name: 'NewApp'
    });
    const sampleTVApp = AppstudioModelFactory.create('TVApp', {
        name: 'NewApp'
    });

    describe('GIVEN: An Invalid App Collection', () => {
        describe('WHEN: Retrieving a App', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                appApplicationService.getById(TestSetup.accountId, 'some ID').then(done).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Removing an App', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                appApplicationService.remove(TestSetup.accountId, 'some brand', 'some ID').then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Duplicating an App', () => {
            const appId = 'some app';
            const brandId = 'some brand';
            it('THEN: An ItemNotFound Error should occur', (done) => {
                appApplicationService.duplicate(TestSetup.accountId, brandId, undefined, undefined, appId).then(
                    (newApp) => {
                        done();
                    }).catch((err) => {
                    expect(err.name).to.equal('ItemNotFoundError');
                    done();
                });
            });
        });
        describe('WHEN: Updating an App', () => {
            it('THEN: An ItemNotFound Error should occur', (done) => {
                appApplicationService.update(TestSetup.accountId, 'some brand ID', 'some app id', <any>{}).then(
                    (result) => {
                        done();
                    }).catch(
                    (err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
        describe('WHEN: Adding an App', () => {
            const appId = 'some app';
            it('THEN: An ItemNotFound Error should occur', (done) => {
                appApplicationService.create(TestSetup.accountId, appId, <any>sampleMobileApp).then(
                    (newApp) => {
                        done();
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
        });
    });
    describe('GIVEN: A Collections of Apps', () => {
        describe('WHEN: Retrieving an App', () => {
            it('THEN: The name should be string', (done) => {
                appApplicationService.getById(TestSetup.accountId, '543dc5d0-00f9-11e6-b786-132120879927').then(
                    (app) => {
                        expect(app.name).to.equal('string');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Creating an App', () => {
            it('THEN: The App will also create a Mobile Edition', (done) => {
                appApplicationService.create(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4', <any>sampleMobileApp).then(
                    (app) => {
                        expect(app.name).to.equal('NewApp');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
            it('THEN: The Edition will also create a Tablet App', (done) => {
                appApplicationService.create(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4', <any>sampleTabletApp).then(
                    (app) => {
                        expect(app.name).to.equal('NewApp');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
            it('THEN: The Edition will also create a TV App', (done) => {
                appApplicationService.create(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4', <any>sampleTVApp).then(
                    (app) => {
                        expect(app.name).to.equal('NewApp');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Duplicating an App under the same account and same brand', () => {
            it('THEN: A new App will be copied under the same account and same brand', (done) => {
                appApplicationService.duplicate(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4', undefined, undefined, '543dc5d0-00f9-11e6-b786-132120879927').then(
                    (app) => {
                        expect(app.name).to.equal('string Copy');
                        expect(app.brandId).to.equal('94faff10-80fc-11e6-a218-dd3f12ad4da4');
                        done();
                    }).catch((err) => {
                    done(err);
                });
            });
        });
        describe('WHEN: Duplicating an App under the same account but different brand', () => {
            it('THEN: A new App will be copied under the same account but different brand', (done) => {
                appApplicationService.duplicate(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4', undefined,
                    '44faff10-80fc-11e6-a218-dd3f12ad4da4', '543dc5d0-00f9-11e6-b786-132120879927').then(
                    (app) => {
                        expect(app.name).to.equal('string Copy');
                        expect(app.brandId).to.equal('44faff10-80fc-11e6-a218-dd3f12ad4da4');
                        done();
                    }).catch((err) => {
                    done(err);
                });
            });
        });

        describe('WHEN: Duplicating an App under a different account and a different brand', () => {
            it('THEN: A new App will be copied under the new account and the new brand', (done) => {
                appApplicationService.duplicate(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4', TestSetup.anotherAccountId,
                    '44faff10-80fc-11e6-a218-dd3f12ad4da4', '543dc5d0-00f9-11e6-b786-132120879927').then(
                    (app) => {
                        expect(app.name).to.equal('string Copy');
                        expect(app.brandId).to.equal('44faff10-80fc-11e6-a218-dd3f12ad4da4');

                        return appApplicationService.getById(TestSetup.anotherAccountId, app.id).then((newApp) => {
                            expect(newApp).to.exist;
                            expect(newApp.name).to.equal(app.name);
                            expect(newApp.brandId).to.equal(app.brandId);
                            done();
                        });

                    }).catch((err) => {
                    done(err);
                });
            });
        });
        describe('WHEN: Deleting an App', () => {
            it('THEN: The App should not exist', (done) => {
                appApplicationService.remove(TestSetup.accountId,
                    '94faff10-80fc-11e6-a218-dd3f12ad4da4',
                    '643dc5d0-00f9-11e6-b786-132120879927').then(
                    () => {
                        return appRepository.getById(TestSetup.accountId, '643dc5d0-00f9-11e6-b786-132120879927');
                    }).catch((err) => {
                        expect(err.name).to.equal('ItemNotFoundError');
                        done();
                    });
            });
            it('THEN: The app should not show up in brand', (done) => {
                brandRepository.getById(TestSetup.accountId, '94faff10-80fc-11e6-a218-dd3f12ad4da4')
                    .then((brand) => {
                        expect(brand.app.indexOf('643dc5d0-00f9-11e6-b786-132120879927')).to.equal(-1);
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
        describe('WHEN: Updating an App', () => {
            it('THEN: The App will have new name', (done) => {
                appRepository.getById(TestSetup.accountId, '743dc5d0-00f9-11e6-b786-132120879927').then(
                    (app) => {
                        app.name = 'UPDATED App';
                        return appApplicationService.update(TestSetup.accountId,
                            '94faff10-80fc-11e6-a218-dd3f12ad4da4',
                            '743dc5d0-00f9-11e6-b786-132120879927', app);
                    }).then((updatedResult) => {
                        return appRepository.getById(TestSetup.accountId,
                            '743dc5d0-00f9-11e6-b786-132120879927');
                    }).then((app) => {
                        expect(app.name).to.equal('UPDATED App');
                        done();
                    }).catch((err) => {
                        done(err);
                    });
            });
        });
    });
});