// import mocha for all typings
import 'mocha';

import dotenv = require('dotenv');
dotenv.config();

import fixtures = require('pow-mongodb-fixtures');
const connection1 = fixtures.connect('testaccount00001');
const connection2 = fixtures.connect('testaccount00002');

/* tslint:disable */

before((callback) => {
    connection1.clearAndLoad(__dirname + '/fixtures/', () => {
        connection2.clearAndLoad(__dirname + '/fixtures/', callback);
    });
});

after((callback) => {
    connection1.clear(() => {
        connection2.clear(callback);
    });
});
