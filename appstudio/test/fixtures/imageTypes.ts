/* tslint:disable */

exports.ImageTypes = [
  {
    '_id': 'thumbnail',
    'document_v1': {
      "id": "thumbnail",
      "name": "Thumbnail",
      "width": 200,
      "height": 112
    }
  },
  {
    '_id': 'background',
    'document_v1': {
      "id": "background",
      "name": "Background",
      "width": 1024,
      "height": 768
    }
  },
  {
    '_id': 'poster',
    'document_v1': {
      "id": "poster",
      "name": "Poster",
      "width": 768,
      "height": 1024
    }
  },
  {
    '_id': 'hero',
    'document_v1': {
      "id": "hero",
      "name": "Hero",
      "width": 1200,
      "height": 800
    }
  },
  {
    '_id': 'f599e673-ff65-4b48-bfcb-35a1b74100f2',
    'document_v1': {
      "id": "f599e673-ff65-4b48-bfcb-35a1b74100f2",
      "name": "Brand Logo",
      "width": 100,
      "height": 60
    }
  },
  {
    '_id': '40bebec0-9db1-49cc-baf8-eec62e0709f8',
    'document_v1': {
      "id": "40bebec0-9db1-49cc-baf8-eec62e0709f8",
      "name": "App Type",
      "width": 100,
      "height": 100
    }
  }
];