/* tslint:disable */

exports.Brands = [{
    '_id': '94faff10-80fc-11e6-a218-dd3f12ad4da4',
    'document_v1': {
        'name': 'brand with no image',
        'app': [
            '543dc5d0-00f9-11e6-b786-132120879927',
            '643dc5d0-00f9-11e6-b786-132120879927',
            '743dc5d0-00f9-11e6-b786-132120879927'
        ],
        'gallery': [],
        'id': '94faff10-80fc-11e6-a218-dd3f12ad4da4',
        'createdDate': new Date('2016-09-22T19:41:40.098Z'),
        'modifiedDate': new Date('2016-09-23T12:55:38.604Z')
    }
}, {
    '_id': '14faff10-80fc-11e6-a218-dd3f12ad4da4',
    'document_v1': {
        'name': 'brand to be deleted',
        'app': [
        ],
        'gallery': [],
        'id': '14faff10-80fc-11e6-a218-dd3f12ad4da4',
        'createdDate': new Date('2016-09-22T19:41:40.098Z'),
        'modifiedDate': new Date('2016-09-23T12:55:38.604Z')
    }
}, {
    '_id': '44faff10-80fc-11e6-a218-dd3f12ad4da4',
    'document_v1': {
        'name': 'update this brand name',
        'app': [
        ],
        'gallery': [],
        'id': '44faff10-80fc-11e6-a218-dd3f12ad4da4',
        'createdDate': new Date('2016-09-22T19:41:40.098Z'),
        'modifiedDate': new Date('2016-09-23T12:55:38.604Z')
    }
}];
