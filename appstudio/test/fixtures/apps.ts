/* tslint:disable */

exports.Apps = [{
    '_id': '543dc5d0-00f9-11e6-b786-132120879927',
    'document_v1': {
        'name': 'string',
        '_metadata': 'schema.appstudio.TabletApp',
        'activeEdition': '3706e400-81da-11e6-8af5-8746d438ccb1',
        'clientConfig': '',
        'edition': [
            '06d1e690-818d-11e6-b928-f13513bdb337',
            '1706e400-81da-11e6-8af5-8746d438ccb5',
            '2706e400-81da-11e6-8af5-8746d438ccb1',
            '3706e400-81da-11e6-8af5-8746d438ccb1'
        ],
        'brandId': '94faff10-80fc-11e6-a218-dd3f12ad4da4',
        'id': '543dc5d0-00f9-11e6-b786-132120879927',
        'createdDate': new Date('2016-09-23T12:55:38.599Z'),
        'modifiedDate': new Date('2016-09-27T14:47:17.330Z')
    }
},{
    '_id': '643dc5d0-00f9-11e6-b786-132120879927',
    'document_v1': {
        'name': 'new app',
        '_metadata': 'schema.appstudio.TabletApp',
        'activeEdition': '',
        'clientConfig': '',
        'edition': [
        ],
        'brandId': '94faff10-80fc-11e6-a218-dd3f12ad4da4',
        'id': '643dc5d0-00f9-11e6-b786-132120879927',
        'createdDate': new Date('2016-09-23T12:55:38.599Z'),
        'modifiedDate': new Date('2016-09-27T14:47:17.330Z')
    }
},{
    '_id': '743dc5d0-00f9-11e6-b786-132120879927',
    'document_v1': {
        'name': 'new app',
        '_metadata': 'schema.appstudio.TabletApp',
        'activeEdition': '',
        'clientConfig': '',
        'edition': [
        ],
        'brandId': '94faff10-80fc-11e6-a218-dd3f12ad4da4',
        'id': '743dc5d0-00f9-11e6-b786-132120879927',
        'createdDate': new Date('2016-09-23T12:55:38.599Z'),
        'modifiedDate': new Date('2016-09-27T14:47:17.330Z')
    }
}, {
    '_id': '843dc5d0-00f9-11e6-b786-132120879927',
    'document_v1': {
        'name': 'app to be deleted',
        '_metadata': 'schema.appstudio.TabletApp',
        'activeEdition': '',
        'clientConfig': '',
        'edition': [
        ],
        'brandId': '14faff10-80fc-11e6-a218-dd3f12ad4da4',
        'id': '843dc5d0-00f9-11e6-b786-132120879927',
        'createdDate': new Date('2016-09-23T12:55:38.599Z'),
        'modifiedDate': new Date('2016-09-27T14:47:17.330Z')
    }
}];
