/* tslint:disable */

exports.Screens = [{
    '_id' : '00618fa0-6a3a-11e6-b5ef-41279ba710b1',
    'document_v1' : {
        'controls' : {
            'listeners' : [],
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
            'visible' : 'true'
        },
        'style' : {
            'backgroundColor' : {
                '_metadata' : 'schema.common.Color',
                'value' : '#FFFFFF'
            },
            'backgroundImage' : {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.BackgroundImage',
                'value' : '{model.thumbnails.Default-2208x1242.url}'
            },
            'navigation' : {
                'menu' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.Menu',
                    'visible' : true
                },
                'topBar' : {
                    'style' : {
                        'backgroundColor' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        },
                        '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.topbar.TopBarStyle'
                    },
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.TopBar',
                    'visible' : true
                },
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.NavigationStyle'
            },
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.ScreenStyle'
        },
        'item' : [ 
            {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.Image',
                'controls' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
                    'visible' : 'true',
                    'listeners' : [ 
                        {
                            '_metadata' : 'schema.common.Listener',
                            'event' : {
                                '_metadata' : 'schema.common.listener.event.SelectEvent'
                            },
                            'action' : {
                                '_metadata' : 'schema.common.listener.action.PlaybackAction',
                                'live' : 'true',
                                'title' : '{model.title}',
                                'description' : '{model.description}',
                                'duration' : '',
                                'source' : 'video source',
                                'format' : 'Smooth Streaming',
                                'closedCaptioningFormat' : 'WebVTT'
                            }
                        }
                    ]
                },
                'style' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.ImageStyle',
                    'margin' : '5 5 0 5',
                    'padding' : '5 5 0 5',
                    'height' : 'wrapContent',
                    'width' : 'matchParent',
                    'gravity' : 'center',
                    'scale' : 'Aspect Fit',
                    'aspectRatio' : 'None',
                    'border' : {
                        '_metadata' : 'schema.common.Border',
                        'radius' : 0,
                        'thickness' : 0,
                        'color' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        }
                    }
                },
                'content' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.ImageContent',
                    'value' : '{model.image_large}'
                }
            }
        ],
        '_metadata' : 'schema.appstudio.app.tv.uiconfig.screen.TvScreen',
        'createdDate' : new Date('2016-08-24T20:33:22.842Z'),
        'modifiedDate' : new Date('2016-09-16T17:56:21.974Z'),
        'name' : 'Live',
        'id' : '00618fa0-6a3a-11e6-b5ef-41279ba710b1',
        'configId' : '06d17160-818d-11e6-b928-f13513bdb337'
    }
},
{
    '_id' : '170c3b30-81da-11e6-8af5-8746d438ccb5',
    'document_v1' : {
        'controls' : {
            'listeners' : [],
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
            'visible' : 'true'
        },
        'style' : {
            'backgroundColor' : {
                '_metadata' : 'schema.common.Color',
                'value' : '#FFFFFF'
            },
            'backgroundImage' : {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.BackgroundImage',
                'value' : '{model.thumbnails.Default-2208x1242.url}'
            },
            'navigation' : {
                'menu' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.Menu',
                    'visible' : true
                },
                'topBar' : {
                    'style' : {
                        'backgroundColor' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        },
                        '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.topbar.TopBarStyle'
                    },
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.TopBar',
                    'visible' : true
                },
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.NavigationStyle'
            },
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.ScreenStyle'
        },
        'item' : [ 
            {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.Image',
                'controls' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
                    'visible' : 'true',
                    'listeners' : [ 
                        {
                            '_metadata' : 'schema.common.Listener',
                            'event' : {
                                '_metadata' : 'schema.common.listener.event.SelectEvent'
                            },
                            'action' : {
                                '_metadata' : 'schema.common.listener.action.PlaybackAction',
                                'live' : 'true',
                                'title' : '{model.title}',
                                'description' : '{model.description}',
                                'duration' : '',
                                'source' : 'video source',
                                'format' : 'Smooth Streaming',
                                'closedCaptioningFormat' : 'WebVTT'
                            }
                        }
                    ]
                },
                'style' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.ImageStyle',
                    'margin' : '5 5 0 5',
                    'padding' : '5 5 0 5',
                    'height' : 'wrapContent',
                    'width' : 'matchParent',
                    'gravity' : 'center',
                    'scale' : 'Aspect Fit',
                    'aspectRatio' : 'None',
                    'border' : {
                        '_metadata' : 'schema.common.Border',
                        'radius' : 0,
                        'thickness' : 0,
                        'color' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        }
                    }
                },
                'content' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.ImageContent',
                    'value' : '{model.image_large}'
                }
            }
        ],
        '_metadata' : 'schema.appstudio.app.tv.uiconfig.screen.TvScreen',
        'createdDate' : new Date('2016-09-23T22:07:17.091Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.091Z'),
        'name' : 'Live',
        'configId' : '170906e0-81da-11e6-8af5-8746d438ccb5',
        'id' : '170c3b30-81da-11e6-8af5-8746d438ccb5'
    }
},
{
    '_id' : '170c3b30-81da-11e6-8af5-8746d438ccb6',
    'document_v1' : {
        'controls' : {
            'listeners' : [],
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
            'visible' : 'true'
        },
        'style' : {
            'backgroundColor' : {
                '_metadata' : 'schema.common.Color',
                'value' : '#FFFFFF'
            },
            'backgroundImage' : {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.BackgroundImage',
                'value' : '{model.thumbnails.Default-2208x1242.url}'
            },
            'navigation' : {
                'menu' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.Menu',
                    'visible' : true
                },
                'topBar' : {
                    'style' : {
                        'backgroundColor' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        },
                        '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.topbar.TopBarStyle'
                    },
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.TopBar',
                    'visible' : true
                },
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.NavigationStyle'
            },
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.ScreenStyle'
        },
        'item' : [ 
            {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.Image',
                'controls' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
                    'visible' : 'true',
                    'listeners' : [ 
                        {
                            '_metadata' : 'schema.common.Listener',
                            'event' : {
                                '_metadata' : 'schema.common.listener.event.SelectEvent'
                            },
                            'action' : {
                                '_metadata' : 'schema.common.listener.action.PlaybackAction',
                                'live' : 'true',
                                'title' : '{model.title}',
                                'description' : '{model.description}',
                                'duration' : '',
                                'source' : 'video source',
                                'format' : 'Smooth Streaming',
                                'closedCaptioningFormat' : 'WebVTT'
                            }
                        }
                    ]
                },
                'style' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.ImageStyle',
                    'margin' : '5 5 0 5',
                    'padding' : '5 5 0 5',
                    'height' : 'wrapContent',
                    'width' : 'matchParent',
                    'gravity' : 'center',
                    'scale' : 'Aspect Fit',
                    'aspectRatio' : 'None',
                    'border' : {
                        '_metadata' : 'schema.common.Border',
                        'radius' : 0,
                        'thickness' : 0,
                        'color' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        }
                    }
                },
                'content' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.ImageContent',
                    'value' : '{model.image_large}'
                }
            }
        ],
        '_metadata' : 'schema.appstudio.app.tv.uiconfig.screen.TvScreen',
        'createdDate' : new Date('2016-09-23T22:07:17.091Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.091Z'),
        'name' : 'Live 2',
        'configId' : '270906e0-81da-11e6-8af5-8746d438ccb5',
        'id' : '170c3b30-81da-11e6-8af5-8746d438ccb6'
    }
},
{
    '_id' : '170c3b30-81da-11e6-8af5-8746d438ccb7',
    'document_v1' : {
        'controls' : {
            'listeners' : [],
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
            'visible' : 'true'
        },
        'style' : {
            'backgroundColor' : {
                '_metadata' : 'schema.common.Color',
                'value' : '#FFFFFF'
            },
            'backgroundImage' : {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.BackgroundImage',
                'value' : '{model.thumbnails.Default-2208x1242.url}'
            },
            'navigation' : {
                'menu' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.Menu',
                    'visible' : true
                },
                'topBar' : {
                    'style' : {
                        'backgroundColor' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        },
                        '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.topbar.TopBarStyle'
                    },
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.TopBar',
                    'visible' : true
                },
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.NavigationStyle'
            },
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.ScreenStyle'
        },
        'item' : [ 
            {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.Image',
                'controls' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
                    'visible' : 'true',
                    'listeners' : [ 
                        {
                            '_metadata' : 'schema.common.Listener',
                            'event' : {
                                '_metadata' : 'schema.common.listener.event.SelectEvent'
                            },
                            'action' : {
                                '_metadata' : 'schema.common.listener.action.PlaybackAction',
                                'live' : 'true',
                                'title' : '{model.title}',
                                'description' : '{model.description}',
                                'duration' : '',
                                'source' : 'video source',
                                'format' : 'Smooth Streaming',
                                'closedCaptioningFormat' : 'WebVTT'
                            }
                        }
                    ]
                },
                'style' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.ImageStyle',
                    'margin' : '5 5 0 5',
                    'padding' : '5 5 0 5',
                    'height' : 'wrapContent',
                    'width' : 'matchParent',
                    'gravity' : 'center',
                    'scale' : 'Aspect Fit',
                    'aspectRatio' : 'None',
                    'border' : {
                        '_metadata' : 'schema.common.Border',
                        'radius' : 0,
                        'thickness' : 0,
                        'color' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        }
                    }
                },
                'content' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.ImageContent',
                    'value' : '{model.image_large}'
                }
            }
        ],
        '_metadata' : 'schema.appstudio.app.tv.uiconfig.screen.TvScreen',
        'createdDate' : new Date('2016-09-23T22:07:17.091Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.091Z'),
        'name' : 'Live 3',
        'configId' : '370906e0-81da-11e6-8af5-8746d438ccb5',
        'id' : '170c3b30-81da-11e6-8af5-8746d438ccb7'
    }
},
{
    '_id' : '170c3b30-81da-11e6-8af5-8746d438ccb8',
    'document_v1' : {
        'controls' : {
            'listeners' : [],
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
            'visible' : 'true'
        },
        'style' : {
            'backgroundColor' : {
                '_metadata' : 'schema.common.Color',
                'value' : '#FFFFFF'
            },
            'backgroundImage' : {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.BackgroundImage',
                'value' : '{model.thumbnails.Default-2208x1242.url}'
            },
            'navigation' : {
                'menu' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.Menu',
                    'visible' : true
                },
                'topBar' : {
                    'style' : {
                        'backgroundColor' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        },
                        '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.topbar.TopBarStyle'
                    },
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.navigation.TopBar',
                    'visible' : true
                },
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.NavigationStyle'
            },
            '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.ScreenStyle'
        },
        'item' : [ 
            {
                '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.Image',
                'controls' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.ComponentControls',
                    'visible' : 'true',
                    'listeners' : [ 
                        {
                            '_metadata' : 'schema.common.Listener',
                            'event' : {
                                '_metadata' : 'schema.common.listener.event.SelectEvent'
                            },
                            'action' : {
                                '_metadata' : 'schema.common.listener.action.PlaybackAction',
                                'live' : 'true',
                                'title' : '{model.title}',
                                'description' : '{model.description}',
                                'duration' : '',
                                'source' : 'video source',
                                'format' : 'Smooth Streaming',
                                'closedCaptioningFormat' : 'WebVTT'
                            }
                        }
                    ]
                },
                'style' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.image.ImageStyle',
                    'margin' : '5 5 0 5',
                    'padding' : '5 5 0 5',
                    'height' : 'wrapContent',
                    'width' : 'matchParent',
                    'gravity' : 'center',
                    'scale' : 'Aspect Fit',
                    'aspectRatio' : 'None',
                    'border' : {
                        '_metadata' : 'schema.common.Border',
                        'radius' : 0,
                        'thickness' : 0,
                        'color' : {
                            '_metadata' : 'schema.common.Color',
                            'value' : '#FFFFFF'
                        }
                    }
                },
                'content' : {
                    '_metadata' : 'schema.appstudio.app.configuration.uiconfig.screen.component.field.content.ImageContent',
                    'value' : '{model.image_large}'
                }
            }
        ],
        '_metadata' : 'schema.appstudio.app.tv.uiconfig.screen.TvScreen',
        'createdDate' : new Date('2016-09-23T22:07:17.091Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.091Z'),
        'name' : 'Live 4',
        'configId' : '470906e0-81da-11e6-8af5-8746d438ccb5',
        'id' : '170c3b30-81da-11e6-8af5-8746d438ccb8'
    }
}
];