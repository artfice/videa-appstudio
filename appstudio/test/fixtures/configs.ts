/* tslint:disable */

exports.Configs = [{
    '_id' : '06d17160-818d-11e6-b928-f13513bdb337',
    'document_v1' : {
        'id' : '06d17160-818d-11e6-b928-f13513bdb337',
        '_metadata' : 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig',
        'createdDate' : new Date('2016-09-23T12:55:38.624Z'),
        'modifiedDate' : new Date('2016-09-23T12:55:38.624Z'),
        'name' : 'First Config',
        'screen' : [ 
            '00618fa0-6a3a-11e6-b5ef-41279ba710b1'
        ],
        'theme' : null,
        'navigation' : null,
        'ads' : null,
        'analytics' : [],
        'searchSettings' : null,
        'uiSettings' : null,
        'editionId' : '06d1e690-818d-11e6-b928-f13513bdb337'
    }
}, {
    '_id' : '170906e0-81da-11e6-8af5-8746d438ccb5',
    'document_v1' : {
        '_metadata' : 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig',
        'name' : 'Fifth Config',
        'screen' : [ 
            '170c3b30-81da-11e6-8af5-8746d438ccb5'
        ],
        'analytics' : [],
        'editionId' : '1706e400-81da-11e6-8af5-8746d438ccb5',
        'id' : '170906e0-81da-11e6-8af5-8746d438ccb5',
        'createdDate' : new Date('2016-09-23T22:07:17.070Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.097Z')
    }
}, {
    '_id' : '270906e0-81da-11e6-8af5-8746d438ccb5',
    'document_v1' : {
        '_metadata' : 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig',
        'name' : 'Second Config',
        'screen' : [ 
            '170c3b30-81da-11e6-8af5-8746d438ccb6'
        ],
        'analytics' : [],
        'editionId' : '2706e400-81da-11e6-8af5-8746d438ccb1',
        'id' : '270906e0-81da-11e6-8af5-8746d438ccb5',
        'createdDate' : new Date('2016-09-23T22:07:17.070Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.097Z')
    }
}, {
    '_id' : '370906e0-81da-11e6-8af5-8746d438ccb5',
    'document_v1' : {
        '_metadata' : 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig',
        'name' : 'Third Config',
        'screen' : [ 
            '170c3b30-81da-11e6-8af5-8746d438ccb7'
        ],
        'analytics' : [],
        'editionId' : '3706e400-81da-11e6-8af5-8746d438ccb1',
        'id' : '370906e0-81da-11e6-8af5-8746d438ccb5',
        'createdDate' : new Date('2016-09-23T22:07:17.070Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.097Z')
    }
}, {
    '_id' : '470906e0-81da-11e6-8af5-8746d438ccb5',
    'document_v1' : {
        '_metadata' : 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig',
        'name' : 'Fourth Config',
        'screen' : [ 
            '170c3b30-81da-11e6-8af5-8746d438ccb8'
        ],
        'analytics' : [],
        'editionId' : '3706e400-81da-11e6-8af5-8746d438ccb1',
        'id' : '470906e0-81da-11e6-8af5-8746d438ccb5',
        'createdDate' : new Date('2016-09-23T22:07:17.070Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.097Z')
    }
}, {
    '_id' : '1706e400-81da-11e6-8af5-8746d438ccb5',
    'document_v1' : {
        '_metadata' : 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig',
        'name' : 'Fourth Config',
        'screen' : [ 
        ],
        'analytics' : [],
        'editionId' : '96d1e690-818d-11e6-b928-f13513bdb337',
        'id' : '1706e400-81da-11e6-8af5-8746d438ccb5',
        'createdDate' : new Date('2016-09-23T22:07:17.070Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.097Z')
    }
}, {
    '_id' : '6706e400-81da-11e6-8af5-8746d438ccb5',
    'document_v1' : {
        '_metadata' : 'schema.appstudio.app.tablet.uiconfig.TabletUIConfig',
        'name' : 'Fifth Config',
        'screen' : [],
        'analytics' : [],
        'editionId' : '9706e400-81d1-11e6-8af5-8746d438ccb5',
        'id' : '6706e400-81da-11e6-8af5-8746d438ccb5',
        'createdDate' : new Date('2016-09-23T22:07:17.070Z'),
        'modifiedDate' : new Date('2016-09-23T22:07:17.097Z')
    }
}]