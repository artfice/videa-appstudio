/* tslint:disable */

exports.Editions = [{
    '_id': '06d1e690-818d-11e6-b928-f13513bdb337',
    'document_v1': {
        'id': '06d1e690-818d-11e6-b928-f13513bdb337',
        '_metadata': 'schema.appstudio.app.tablet.TabletAppEdition',
        'createdDate': new Date('2016-09-23T12:55:38.619Z'),
        'modifiedDate': new Date('2016-09-23T12:55:38.626Z'),
        'uiConfig': [
            '06d17160-818d-11e6-b928-f13513bdb337'
        ],
        'cms': [],
        'clientConfig': '',
        'name': 'First Edition',
        'appId': '543dc5d0-00f9-11e6-b786-132120879927',
        'state': 'IN_PROGRESS'
    }
}, {
    '_id': '9706e400-81d1-11e6-8af5-8746d438ccb5',
    'document_v1': {
        'id': '9706e400-81d1-11e6-8af5-8746d438ccb5',
        '_metadata': 'schema.appstudio.app.tablet.TabletAppEdition',
        'createdDate': new Date('2016-09-23T12:55:38.619Z'),
        'modifiedDate': new Date('2016-09-23T12:55:38.626Z'),
        'uiConfig': [
            '6706e400-81da-11e6-8af5-8746d438ccb5'
        ],
        'cms': [],
        'clientConfig': '',
        'name': 'First Edition',
        'appId': '543dc5d0-00f9-11e6-b786-132120879927',
        'state': 'IN_PROGRESS'
    }
},
    {
        '_id': '1706e400-81da-11e6-8af5-8746d438ccb5',
        'document_v1': {
            '_metadata': 'schema.appstudio.app.tablet.TabletAppEdition',
            'uiConfig': [
                '170906e0-81da-11e6-8af5-8746d438ccb5'
            ],
            'customData': {
                'content': '{\"a\":1}'
            },
            'cms': [],
            'clientConfig': '',
            'name': 'Edition Copy',
            'appId': '543dc5d0-00f9-11e6-b786-132120879927',
            'state': 'IN_PROGRESS',
            'id': '1706e400-81da-11e6-8af5-8746d438ccb5',
            'createdDate': new Date('2016-09-23T22:07:17.056Z'),
            'modifiedDate': new Date('2016-09-23T22:07:17.103Z')
        }
    },
    {
        '_id': '2706e400-81da-11e6-8af5-8746d438ccb1',
        'document_v1': {
            '_metadata': 'schema.appstudio.app.tablet.TabletAppEdition',
            'uiConfig': [
                '270906e0-81da-11e6-8af5-8746d438ccb5'
            ],
            'cms': [
                {
                    '_metadata': 'schema.appstudio.app.tv.cms.TvGenericCms',
                    'name': 'Generic',
                    'collectionPath': 's',
                    'uid': '{model.guid}',
                    'delimiter': ''
                }
            ],
            'customData': {
                '_metadata': 'schema.common.CustomData',
                'content': '{\"a\":1}'
            },
            'watchHistory': {
                '_metadata': 'schema.appstudio.app.configuration.WatchHistory',
                'provider': {
                    '_metadata': 'schema.appstudio.app.configuration.uiconfig.watchhistory.LocalProvider',
                    'completionThreshold': 0.94,
                    'frequency': 99,
                    'onResumeEvent': true,
                    'onPauseEvent': true,
                    'onSeekEvent': false,
                    'onStopEvent': false,
                    'onPlayEvent': false
                }
            },
            'chromecast': {
                '_metadata': 'schema.appstudio.app.configuration.Chromecast',
                'enabled': true,
                'appId': 'test'
            },
            'authentication': {
                '_metadata': 'schema.appstudio.app.configuration.Authentication',
                'provider': {
                    '_metadata': 'schema.common.auth.AkamaiProvider',
                    'akamaiIDP': 'test',
                    'platformID': {
                        '_metadata': 'schema.common.auth.akamai.Platform',
                        'iOS': 'IOS',
                        'android': 'android',
                        'web': 'web'
                    },
                    'redirect': {
                        '_metadata': 'schema.common.auth.akamai.Redirect',
                        'iOS': 'IOS',
                        'android': 'android',
                        'web': 'web'
                    },
                    'resourceAccessCodes': [
                    ]
                }
            },
            'clientConfig': '',
            'name': 'Edition Translate this',
            'appId': '543dc5d0-00f9-11e6-b786-132120879927',
            'state': 'IN_PROGRESS',
            'id': '2706e400-81da-11e6-8af5-8746d438ccb1',
            'createdDate': new Date('2016-09-23T22:07:17.056Z'),
            'modifiedDate': new Date('2016-09-27T02:23:13.824Z')
        }
    },
    {
        '_id': '3706e400-81da-11e6-8af5-8746d438ccb1',
        'document_v1': {
            '_metadata': 'schema.appstudio.app.tablet.TabletBundleEdition',
            'uiConfig': [
                '370906e0-81da-11e6-8af5-8746d438ccb5',
                '470906e0-81da-11e6-8af5-8746d438ccb5'
            ],
            'cms': [
                {
                    '_metadata': 'schema.appstudio.app.tv.cms.TvGenericCms',
                    'name': 'Generic',
                    'collectionPath': 'e',
                    'uid': '{model.guid}',
                    'delimiter': ''
                }
            ],
            'customData': {
                '_metadata': 'schema.common.CustomData',
                'content': '{\"a\":1}'
            },
            'watchHistory': {
                '_metadata': 'schema.appstudio.app.configuration.WatchHistory',
                'provider': {
                    '_metadata': 'schema.appstudio.app.configuration.uiconfig.watchhistory.LocalProvider',
                    'completionThreshold': 0.94,
                    'frequency': 99,
                    'onResumeEvent': true,
                    'onPauseEvent': true,
                    'onSeekEvent': false,
                    'onStopEvent': false,
                    'onPlayEvent': false
                }
            },
            'chromecast': {
                '_metadata': 'schema.appstudio.app.configuration.Chromecast',
                'enabled': true,
                'appId': 'test'
            },
            'authentication': {
                '_metadata': 'schema.appstudio.app.configuration.Authentication',
                'provider': {
                    '_metadata': 'schema.common.auth.AkamaiProvider',
                    'akamaiIDP': 'test',
                    'platformID': {
                        '_metadata': 'schema.common.auth.akamai.Platform',
                        'iOS': 'IOS',
                        'android': 'android',
                        'web': 'web'
                    },
                    'redirect': {
                        '_metadata': 'schema.common.auth.akamai.Redirect',
                        'iOS': 'IOS',
                        'android': 'android',
                        'web': 'web'
                    },
                    'resourceAccessCodes': [
                    ]
                }
            },
            'clientConfig': '',
            'name': 'Bundle Edition Translate this',
            'appId': '543dc5d0-00f9-11e6-b786-132120879927',
            'state': 'IN_PROGRESS',
            'id': '3706e400-81da-11e6-8af5-8746d438ccb1',
            'createdDate': new Date('2016-09-23T22:07:17.056Z'),
            'modifiedDate': new Date('2016-09-27T14:47:17.325Z')
        }
    }, {
    '_id': '96d1e690-818d-11e6-b928-f13513bdb337',
    'document_v1': {
        'id': '96d1e690-818d-11e6-b928-f13513bdb337',
        '_metadata': 'schema.appstudio.app.tablet.TabletAppEdition',
        'createdDate': new Date('2016-09-23T12:55:38.619Z'),
        'modifiedDate': new Date('2016-09-23T12:55:38.626Z'),
        'uiConfig': [
            '570906e0-81da-11e6-8af5-8746d438ccb5'
        ],
        'cms': [],
        'clientConfig': '',
        'name': 'Delete Test Edition',
        'appId': '643dc5d0-00f9-11e6-b786-132120879927',
        'state': 'IN_PROGRESS'
    }
}];