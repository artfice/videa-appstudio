import chai = require('chai');
chai.use(require('chai-shallow-deep-equal'));
const expect = chai.expect;
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {SectionTranslator as SectionTranslator} from '../../translation/navigation/SectionTranslator';
import {ListenerTranslator as ListenerTranslator} from '../../translation/ListenerTranslator';
import {LabelSectionTranslator as LabelSectionTranslator} from '../../translation/navigation/section/LabelSectionTranslator';
import {ImageSectionTranslator as ImageSectionTranslator} from '../../translation/navigation/section/ImageSectionTranslator';
import {ImageTranslator as ImageTranslator} from '../../translation/field/ImageTranslator';
import {ColoredComponentStyleTranslator as ColoredComponentStyleTranslator} from '../../translation/component/ColoredComponentStyleTranslator';
import {ImageStyleTranslator as ImageStyleTranslator} from '../../translation/field/style/ImageStyleTranslator';
import {FieldContentTranslator as FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentTranslator as ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {BaseFieldTranslator as BaseFieldTranslator} from '../../translation/field/BaseFieldTranslator';
import {LabelTranslator as LabelTranslator} from '../../translation/field/LabelTranslator';
import {LabelStyleTranslator as LabelStyleTranslator} from '../../translation/field/style/LabelStyleTranslator';
import {LabelContentTranslator as LabelContentTranslator} from '../../translation/field/content/LabelContentTranslator';
import {SizedComponentStyleTranslator as SizedComponentStyleTranslator} from '../../translation/component/SizedComponentStyleTranslator';
import {SectionStyleTranslator as SectionStyleTranslator} from '../../translation/navigation/style/SectionStyleTranslator';
import {NavigationTranslator as NavigationTranslator} from '../../translation/navigation/NavigationTranslator';
import {DrawerMenuTranslator as DrawerMenuTranslator} from '../../translation/navigation/menu/DrawerMenuTranslator';

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('Section Translator tests', () => {
	var translation,
		translator: NavigationTranslator,
		service: TranslationService;

	translator = new NavigationTranslator();
	service = new TranslationService();
	service.registerTranslator(ListenerTranslator);
	service.registerTranslator(ColoredComponentStyleTranslator);
	service.registerTranslator(LabelSectionTranslator);
	service.registerTranslator(ImageSectionTranslator);
	service.registerTranslator(ImageTranslator);
	service.registerTranslator(LabelTranslator);
	service.registerTranslator(ImageStyleTranslator);
	service.registerTranslator(ComponentStyleTranslator);
	service.registerTranslator(ComponentTranslator);
	service.registerTranslator(FieldContentTranslator);
	service.registerTranslator(ComponentControlsTranslator);
	service.registerTranslator(BaseFieldTranslator);
	service.registerTranslator(LabelStyleTranslator);
	service.registerTranslator(LabelTranslator);
	service.registerTranslator(LabelContentTranslator);
	service.registerTranslator(SizedComponentStyleTranslator);
	service.registerTranslator(SectionStyleTranslator);
	service.registerTranslator(SectionTranslator);
	service.registerTranslator(DrawerMenuTranslator);

	describe('GIVEN: An appstudio draw menu with no properties', () => {

		var source = <any>AppstudioModelFactory.create('Navigation', {
            mainMenu: AppstudioModelFactory.create('DrawerMenu')
        });

		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it ('THEN: Translation should be a valid sections container', () => {
                expect(translation.type).to.equal('Navigation');
                expect(translation.mainMenu.type).to.equal('Drawer');
                expect(translation.section.length).to.equal(0);
                expect(translation.secondaryMenu).to.equal(null);
			});
		});

	});
});
