import { expect } from 'chai';

import { BaseFieldTranslator as BaseFieldTranslator } from '../../translation/field/BaseFieldTranslator';
import { TextFieldTranslator as TextFieldTranslator } from '../../translation/field/TextFieldTranslator';
import { TextFieldStyleTranslator as TextFieldStyleTranslator } from '../../translation/field/style/TextFieldStyleTranslator';
import { TextFieldContentTranslator as TextFieldContentTranslator } from '../../translation/field/content/TextFieldContentTranslator';
import { FieldContentTranslator as FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';
import { ComponentControlsTranslator as ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ComponentStyleTranslator as ComponentStyleTranslator } from '../../translation/component/ComponentStyleTranslator';
import { ComponentTranslator as ComponentTranslator } from '../../translation/component/ComponentTranslator';
import { ListenerTranslator as ListenersTranslator } from '../../translation/ListenerTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';

import { AppstudioModelFactory as AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory }from '../../factory/ClientAppModelFactory';

import { TextField as AppstudioTextFieldDto } from '../../dto/app/configuration/uiconfig/screen/component/field/textfield/TextField';

import { ComponentControls as ComponentControlsDto } from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import { BaseEvent as BaseEventDto } from '../../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../../dto/common/listener/action/BaseAction';

import AlignmentSchema = require('../../schema/common/Alignment');

describe('TextField Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(ComponentStyleTranslator);
    translationService.registerTranslator(ComponentTranslator);
    translationService.registerTranslator(FieldContentTranslator);
    translationService.registerTranslator(ComponentControlsTranslator);
    translationService.registerTranslator(BaseFieldTranslator);
    translationService.registerTranslator(ListenersTranslator);
    translationService.registerTranslator(TextFieldStyleTranslator);
    translationService.registerTranslator(TextFieldTranslator);
    translationService.registerTranslator(TextFieldContentTranslator);
    translationService.registerTranslator(OpenSansTranslator);

    const translator = new TextFieldTranslator();

    describe('GIVEN: a TextField', () => {
        const source = <AppstudioTextFieldDto>AppstudioModelFactory.create('TextField', {
            style: AppstudioModelFactory.create('TextFieldStyle'),
            content: AppstudioModelFactory.create('TextFieldContent'),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls')
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });
            it('THEN: the value property should be translated as undefined', () => {
                expect(translation.value).to.equal(undefined);
            });
            it('THEN: the width property should be translated as undefined', () => {
                expect(translation.width).to.equal(undefined);
            });
            it('THEN: the height property should be translated as undefined', () => {
                expect(translation.height).to.equal(undefined);
            });
            it('THEN: the gravity property should be translated as undefined', () => {
                expect(translation.gravity).to.equal(undefined);
            });
            it('THEN: the hint property should be translated as undefined', () => {
                expect(translation.hint).to.equal(undefined);
            });
            it('THEN: the inputType property should be translated as undefined', () => {
                expect(translation.inputType).to.equal(undefined);
            });

            it('THEN: the visible property should be translated as true', () => {
                expect(translation.visible).to.equal('true');
            });
            it('THEN: the listeners property should be translated as []', () => {
                expect(translation.listeners.length).to.equal(0);
            });
        });
    });

    describe('GIVEN: an TextField with style', () => {
        const family = 'OpenSans-Bold';
        const size = 12;
        const alignment = 'Right';
        const weight = 'Bold';
        const color = '#AAAAAA';
        const source = <AppstudioTextFieldDto>AppstudioModelFactory.create('TextField', {
            style: AppstudioModelFactory.create('TextFieldStyle', {
                font: ClientAppModelFactory.create('OpenSans', {
                    size: size,
                    color: AppstudioModelFactory.create('Color', {value: color}),
                    alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                    weight: weight
                })
            }),
            content: AppstudioModelFactory.create('TextFieldContent', {}),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should be translated as empty object', () => {
                expect(translation).to.exist;
            });
            it('THEN: the font family property should be translated as ' + family, () => {
                expect(translation.style.font.family).to.equal(family);
            });
            it('THEN: the font size property should be translated as ' + size, () => {
                expect(translation.style.font.size).to.equal(size);
            });
            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.style.font.alignment).to.equal(alignment);
            });

            it('THEN: the color property should be translated as ' + color, () => {
                expect(translation.style.font.color.value).to.equal(color);
            });
        });
    });

    describe('GIVEN: an TextField with Content', () => {
        const value = 'Roboto';
        const hint = 'Right';
        const inputType = 'Email';
        const componentId = 'id';
        const source = <AppstudioTextFieldDto>AppstudioModelFactory.create('TextField', {
            style: AppstudioModelFactory.create('TextFieldStyle', {}),
            content: AppstudioModelFactory.create('TextFieldContent', {
                hint: hint,
                value: value,
                inputType: inputType,
                componentId: componentId
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should be translated as empty object', () => {
                expect(translation).to.exist;
            });
            it('THEN: the hint property should be translated as ' + hint, () => {
                expect(translation.hint).to.equal(hint);
            });
            it('THEN: the inputType property should be translated as ' + inputType, () => {
                expect(translation.inputType).to.equal(inputType);
            });
            it('THEN: the value property should be translated as ' + value, () => {
                expect(translation.value).to.equal(value);
            });
            it('THEN: the componentId property should be translated as ' + componentId, () => {
                expect(translation.id).to.equal(componentId);
            });
        });
    });
});
