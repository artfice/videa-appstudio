import chaiLib = require('chai');
const expect = chaiLib.expect;
import _ = require('lodash');

import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');

import {ColorTranslator} from '../../translation/color/ColorTranslator';
import {ComplexColorTranslator} from '../../translation/color/ComplexColorTranslator';

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {TopMenuSectionTranslator} from '../../translation/navigation/section/TopMenuSectionTranslator';
import {TopMenuTranslator} from '../../translation/navigation/TopMenuTranslator';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {ComponentStyle as ComponentStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';

import {BaseTranslator} from '../../translation/BaseTranslator';
import {ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ImageContentTranslator} from '../../translation/field/content/ImageContentTranslator';
import {ImageTranslator} from '../../translation/field/ImageTranslator';
import {ImageStyleTranslator} from '../../translation/field/style/ImageStyleTranslator';
import {TopMenuStyleTranslator} from '../../translation/navigation/style/TopMenuStyleTranslator';

import {ListenerTranslator} from '../../translation/ListenerTranslator';
import {StandardAspectRatioTranslator} from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import {CustomAspectRatioTranslator} from '../../translation/aspectRatio/CustomAspectRatioTranslator';

import {TopMenuSection as AppstudioTopMenuSectionDto} from '../../dto/common/navigation/top/TopMenuSection';
import {TopMenu as AppstudioTopMenuDto} from '../../dto/common/navigation/TopMenu';

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('TopMenu Translation Tests', () => {
	let translationService = new TranslationService();

	translationService.registerTranslator(BaseTranslator);
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(ImageStyleTranslator);
	translationService.registerTranslator(ImageContentTranslator);
	translationService.registerTranslator(ImageTranslator);
	translationService.registerTranslator(ComplexColorTranslator);
	translationService.registerTranslator(ColorTranslator);
	translationService.registerTranslator(TopMenuStyleTranslator);
	translationService.registerTranslator(StandardAspectRatioTranslator);
	translationService.registerTranslator(CustomAspectRatioTranslator);
	translationService.registerTranslator(ListenerTranslator);
	translationService.registerTranslator(FieldContentTranslator);
	translationService.registerTranslator(TopMenuSectionTranslator);

	describe('GIVEN: an empty TopMenuSection', () => {
		const source = <AppstudioTopMenuDto>AppstudioModelFactory.create('TopMenu');

		describe('WHEN: translated', () => {
			const translator = new TopMenuTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it('THEN: the translation should exist', () => {
				expect(translation).to.exist;
			});

			it('THEN: the translation should have default configuration translated', () => {
				expect(translation.type).to.equal('Top');
				expect(translation.navigationType.name).to.equal('Top');
				expect(translation.navigationType.value).to.equal('Top');
				expect(translation.item.type).to.equal('Container');
				expect(translation.item.layout.type).to.equal('Relative');
				expect(translation.item.gravity).to.equal('top');
				expect(translation.item.height).to.equal('wrapContent');
				expect(translation.item.width).to.equal('wrapContent');
			});
			it('THEN: the translation should have default style translated', () => {
				expect(translation.item.style.padding).to.equal('0 0 0 0');
				expect(translation.item.style.margin).to.equal('0 0 0 0');
			});
			it('THEN: the translation should have no items', () => {
				expect(translation.item.items.length).to.equal(0);
			});
		});
	});

    describe('GIVEN: a TopMenuSection with Style', () => {
		const backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };

        const source = <AppstudioTopMenuDto>AppstudioModelFactory.create('TopMenu', {
            style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('WrapContent')
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('WrapContent')
				})
            })
        });

        describe('WHEN: translated', () => {
            let translation;
			const translator = new TopMenuTranslator();
            before(() => {
                return translator.translate('videa', translationService, source).then((translated) => {
                    translation = translated;
                });
            });

            it('THEN: the TopMenuSection style should be translated', () => {
                expect(translation.item.style.padding).to.equal('1 1 1 1');
                expect(translation.item.style.margin).to.equal('1 1 1 1');
                expect(translation.item.width).to.equal('wrapContent');
                expect(translation.item.height).to.equal('wrapContent');
				expect(translation.item.style.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.item.style.backgroundColor._metadata).to.equal(backgroundColor._metadata);
            });
        });
    });


	describe('GIVEN: a Left Section', () => {
		const backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };
		const image = AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
		const leftSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
		});

		leftSection.content.subComponent.push(image);

		const source = <AppstudioTopMenuDto>AppstudioModelFactory.create('TopMenu', {
			leftSection: leftSection
		});
		describe('WHEN: translated', () => {
			const translator = new TopMenuTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the value property should be translated', () => {
				expect(translation.item.items[0].gravity).to.equal('top|left');
				expect(translation.item.items[0].width).to.equal(1);
				expect(translation.item.items[0].height).to.equal(1);
				expect(translation.item.items[0].visible).to.equal('not true');
				expect(translation.item.items[0].style.padding).to.equal('1 2 1 1');
				expect(translation.item.items[0].style.margin).to.equal('1 2 1 1');
			});
		});
	});

	describe('GIVEN: a Right Section', () => {
		const backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };
		const image = AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
		const rightSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
		});

		rightSection.content.subComponent.push(image);

		const source = <AppstudioTopMenuDto>AppstudioModelFactory.create('TopMenu', {
			rightSection: rightSection
		});
		describe('WHEN: translated', () => {
			const translator = new TopMenuTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the value property should be translated', () => {
				expect(translation.item.items[0].gravity).to.equal('top|right');
				expect(translation.item.items[0].width).to.equal(1);
				expect(translation.item.items[0].height).to.equal(1);
				expect(translation.item.items[0].visible).to.equal('not true');
				expect(translation.item.items[0].style.padding).to.equal('1 2 1 1');
				expect(translation.item.items[0].style.margin).to.equal('1 2 1 1');
			});
		});
	});

	describe('GIVEN: a Middle Section', () => {
		const backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };
		const image = AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
		const middleSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
		});

		middleSection.content.subComponent.push(image);

		const source = <AppstudioTopMenuDto>AppstudioModelFactory.create('TopMenu', {
			middleSection: middleSection
		});
		describe('WHEN: translated', () => {
			const translator = new TopMenuTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the value property should be translated', () => {
				expect(translation.item.items[0].gravity).to.equal('top|center');
				expect(translation.item.items[0].width).to.equal(1);
				expect(translation.item.items[0].height).to.equal(1);
				expect(translation.item.items[0].visible).to.equal('not true');
				expect(translation.item.items[0].style.padding).to.equal('1 2 1 1');
				expect(translation.item.items[0].style.margin).to.equal('1 2 1 1');
			});
		});
	});

	describe('GIVEN: a Left and Right Section', () => {
		const backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };
		const image = AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
		const leftSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
		});

		leftSection.content.subComponent.push(image);

		const rightSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 2,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 2,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 2
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 2
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'qwe'
            })
		});

		rightSection.content.subComponent.push(image);

		const source = <AppstudioTopMenuDto>AppstudioModelFactory.create('TopMenu', {
			leftSection: leftSection,
			rightSection: rightSection
		});
		describe('WHEN: translated', () => {
			const translator = new TopMenuTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the value property should be translated', () => {
				expect(translation.item.items[0].gravity).to.equal('top|left');
				expect(translation.item.items[0].width).to.equal(1);
				expect(translation.item.items[0].height).to.equal(1);
				expect(translation.item.items[0].visible).to.equal('not true');
				expect(translation.item.items[0].style.padding).to.equal('1 2 1 1');
				expect(translation.item.items[0].style.margin).to.equal('1 2 1 1');

				expect(translation.item.items[1].gravity).to.equal('top|right');
				expect(translation.item.items[1].width).to.equal(2);
				expect(translation.item.items[1].height).to.equal(2);
				expect(translation.item.items[1].visible).to.equal('qwe');
				expect(translation.item.items[1].style.padding).to.equal('1 2 2 1');
				expect(translation.item.items[1].style.margin).to.equal('1 2 2 1');
			});
		});
	});

	describe('GIVEN: a Left, Middle, and Right Section', () => {
		const backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };
		const image = AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
		const leftSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
		});

		leftSection.content.subComponent.push(image);

		const rightSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 2,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 2,
                    bottom: 2,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 2
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 2
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'qwe'
            })
		});

		rightSection.content.subComponent.push(image);

		const middleSection = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			},
			style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 3,
                    bottom: 3,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 3,
                    bottom: 3,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				},
				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 3
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 3
					})
				})
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'wer'
            })
		});

		middleSection.content.subComponent.push(image);

		const source = <AppstudioTopMenuDto>AppstudioModelFactory.create('TopMenu', {
			leftSection: leftSection,
			rightSection: rightSection,
			middleSection: middleSection
		});
		describe('WHEN: translated', () => {
			const translator = new TopMenuTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the value property should be translated', () => {
				expect(translation.item.items[0].gravity).to.equal('top|left');
				expect(translation.item.items[0].width).to.equal(1);
				expect(translation.item.items[0].height).to.equal(1);
				expect(translation.item.items[0].visible).to.equal('not true');
				expect(translation.item.items[0].style.padding).to.equal('1 2 1 1');
				expect(translation.item.items[0].style.margin).to.equal('1 2 1 1');

				expect(translation.item.items[1].gravity).to.equal('top|center');
				expect(translation.item.items[1].width).to.equal(3);
				expect(translation.item.items[1].height).to.equal(3);
				expect(translation.item.items[1].visible).to.equal('wer');
				expect(translation.item.items[1].style.padding).to.equal('1 3 3 1');
				expect(translation.item.items[1].style.margin).to.equal('1 3 3 1');

				expect(translation.item.items[2].gravity).to.equal('top|right');
				expect(translation.item.items[2].width).to.equal(2);
				expect(translation.item.items[2].height).to.equal(2);
				expect(translation.item.items[2].visible).to.equal('qwe');
				expect(translation.item.items[2].style.padding).to.equal('1 2 2 1');
				expect(translation.item.items[2].style.margin).to.equal('1 2 2 1');
			});
		});
	});
});