import chai = require('chai');
const expect = chai.expect;

import {GemiusAnalyticsTranslator as GemiusTranslator} from '../../translation/uiConfig/analytic/GemiusAnalyticsTranslator';
import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {GemiusAnalytics as GemiusAnalyticsDto} from '../../dto/app/configuration/uiconfig/analytics/gemius/GemiusAnalytics';

describe('Gemius Translation Tests', () => {

    let translationService = new TranslationService();


    describe('GIVEN: An empty Gemius analytic', () => {

        let gemius = <GemiusAnalyticsDto>AppstudioModelFactory.create('GemiusAnalytics');

        describe('WHEN: translated', () => {

            let translator = new GemiusTranslator();
            let translation;
            let host;
            let name;
            let type = 'Gemius';
            let iOS;
            let window8;
            let windowsPhone;
            let android;
            let testing;

            before((done) => {

                translator.translate('videa', translationService, gemius).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation field property host should be ' + host, () => {
                expect(translation.host).to.equal(host);
            });
            it('THEN: the translation field property name should be ' + name, () => {
                expect(translation.name).to.equal(name);
            });
            it('THEN: the translation field property type should be ' + type, () => {
                expect(translation.type).to.equal(type);
            });
            it('THEN: the translation field property iOS should be ' + iOS, () => {
                expect(translation.accountID.iOS).to.equal(iOS);
            });
            it('THEN: the translation field property window8 should be ' + window8, () => {
                expect(translation.accountID.window8).to.equal(window8);
            });
            it('THEN: the translation field property windowPhone should be ' + windowsPhone, () => {
                expect(translation.accountID.windowsPhone).to.equal(windowsPhone);
            });
            it('THEN: the translation field property android should be ' + android, () => {
                expect(translation.accountID.android).to.equal(android);
            });
            it('THEN: the translation field property testing should be ' + testing, () => {
                expect(translation.accountID.testing).to.equal(testing);
            });
        });
    });
    describe('GIVEN: A Filled Gemius analytic', () => {

        describe('WHEN: translated', () => {

            let translator = new GemiusTranslator();
            let translation;
            let host = 'a';
            let name = 'Gemius';
            let type = 'Gemius';
            let iOS = 'a';
            let window8 = 'a';
            let windowsPhone = 'a';
            let android = 'a';
            let testing = 'a';
            let gemius = <GemiusAnalyticsDto>AppstudioModelFactory.create('GemiusAnalytics', {
                host: host,
                name: 'Gemius',
                iOS: iOS,
                window8: window8,
                windowsPhone: windowsPhone,
                android: android,
                testing: testing
            });
            before((done) => {

                translator.translate('videa', translationService, gemius).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation field property host should be ' + host, () => {
                expect(translation.host).to.equal(host);
            });
            it('THEN: the translation field property name should be ' + name, () => {
                expect(translation.name).to.equal(name);
            });
            it('THEN: the translation field property type should be ' + type, () => {
                expect(translation.type).to.equal(type);
            });
            it('THEN: the translation field property iOS should be ' + iOS, () => {
                expect(translation.accountID.iOS).to.equal(iOS);
            });
            it('THEN: the translation field property window8 should be ' + window8, () => {
                expect(translation.accountID.window8).to.equal(window8);
            });
            it('THEN: the translation field property windowPhone should be ' + windowsPhone, () => {
                expect(translation.accountID.windowsPhone).to.equal(windowsPhone);
            });
            it('THEN: the translation field property android should be ' + android, () => {
                expect(translation.accountID.android).to.equal(android);
            });
            it('THEN: the translation field property testing should be ' + testing, () => {
                expect(translation.accountID.testing).to.equal(testing);
            });
        });
    });
});
