/**
 * Created by bardiakhosravi on 2016-03-09.
 */
///<reference path="../../../typings/index.d.ts"/>

import chai = require('chai');
chai.use(require('chai-shallow-deep-equal'));
const expect = chai.expect;
// import _ = require('underscore');

import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { ClientAppModelFactory as ClientAppModelFactory } from '../../factory/ClientAppModelFactory';
import { AppstudioModelFactory as AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import { Section as SectionDto } from '../../dto/app/configuration/uiconfig/navigation/Section';
import { Image as AppstudioImageDto } from '../../dto/app/configuration/uiconfig/screen/component/field/image/Image';
import { Label as AppstudioLabelDto } from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';
import { ComponentControls as ComponentControlsDto } from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import { BaseEvent as BaseEventDto } from '../../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../../dto/common/listener/action/BaseAction';
import { Listener as ListenerDto } from '../../dto/common/Listener';

import { SectionTranslator } from '../../translation/navigation/SectionTranslator';
import { ListenerTranslator } from '../../translation/ListenerTranslator';
import { LabelSectionTranslator } from '../../translation/navigation/section/LabelSectionTranslator';
import { ImageSectionTranslator } from '../../translation/navigation/section/ImageSectionTranslator';
import { ImageTranslator } from '../../translation/field/ImageTranslator';
import { ColoredComponentStyleTranslator } from '../../translation/component/ColoredComponentStyleTranslator';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';
import { ImageStyleTranslator } from '../../translation/field/style/ImageStyleTranslator';
import { FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';
import { ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ComponentStyleTranslator } from '../../translation/component/ComponentStyleTranslator';
import { ComponentTranslator } from '../../translation/component/ComponentTranslator';
import { BaseFieldTranslator } from '../../translation/field/BaseFieldTranslator';
import { LabelTranslator } from '../../translation/field/LabelTranslator';
import { LabelStyleTranslator } from '../../translation/field/style/LabelStyleTranslator';
import { LabelContentTranslator } from '../../translation/field/content/LabelContentTranslator';
import { SizedComponentStyleTranslator } from '../../translation/component/SizedComponentStyleTranslator';
import { SectionStyleTranslator } from '../../translation/navigation/style/SectionStyleTranslator';
import { StandardAspectRatioTranslator } from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import { CustomAspectRatioTranslator } from '../../translation/aspectRatio/CustomAspectRatioTranslator';
import { ColorTranslator } from '../../translation/color/ColorTranslator';

import ContainerSchema = require('../../schema/clientapp/app/uiconfig/screen/component/container/Container');
import ClientImageSchema = require('../../schema/clientapp/app/uiconfig/screen/field/Image');
import SelectEventSchema = require('../../schema/common/listener/event/SelectEvent');
import SignoutActionSchema = require('../../schema/common/listener/action/SignoutAction');
import AlignmentSchema = require('../../schema/common/Alignment');

describe('Section Translator tests', () => {
    let translation;

    let translator: SectionTranslator;

    const service: TranslationService = new TranslationService();
    service.registerTranslator(StandardAspectRatioTranslator);
    service.registerTranslator(CustomAspectRatioTranslator);
    service.registerTranslator(ListenerTranslator);
    service.registerTranslator(ColoredComponentStyleTranslator);
    service.registerTranslator(LabelSectionTranslator);
    service.registerTranslator(ImageSectionTranslator);
    service.registerTranslator(ImageTranslator);
    service.registerTranslator(LabelTranslator);
    service.registerTranslator(ImageStyleTranslator);
    service.registerTranslator(ComponentStyleTranslator);
    service.registerTranslator(ComponentTranslator);
    service.registerTranslator(FieldContentTranslator);
    service.registerTranslator(ComponentControlsTranslator);
    service.registerTranslator(BaseFieldTranslator);
    service.registerTranslator(LabelStyleTranslator);
    service.registerTranslator(LabelTranslator);
    service.registerTranslator(LabelContentTranslator);
    service.registerTranslator(SizedComponentStyleTranslator);
    service.registerTranslator(SectionStyleTranslator);
    service.registerTranslator(OpenSansTranslator);
    service.registerTranslator(ColorTranslator);

    function validateSectionContainer(sectionContainer) {
        expect(sectionContainer).to.exist;
        expect(sectionContainer._metadata).to.equal(ContainerSchema.ID);
        expect(sectionContainer.width).to.equal('matchParent');
        expect(sectionContainer.height).to.equal('wrapContent');
        expect(sectionContainer.style.padding).to.equal('0 0 0 0');
        expect(sectionContainer.style.margin).to.equal('0 0 0 0');
        expect(sectionContainer.style.backgroundColor.value).to.equal('#00000000');
        expect(sectionContainer.items).to.exist;
        validateSectionImageContainer(sectionContainer.items[0]);
        validateSectionLabelContainer(sectionContainer.items[1]);
    }

    function validateSectionLabelContainer(labelContainer) {
        expect(labelContainer._metadata).to.equal(ContainerSchema.ID);
        expect(labelContainer.type).to.equal('Container');
        expect(labelContainer.width).to.equal('fillParent');
        expect(labelContainer.height).to.equal('wrapContent');
        expect(labelContainer.style.padding).to.equal('0 0 0 0');
        expect(labelContainer.style.margin).to.equal('0 0 0 0');
        expect(labelContainer.layout.type).to.equal('Relative');
        expect(labelContainer.items.length).to.equal(0);
    }

    function validateSectionImageContainer(imageContainer) {
        expect(imageContainer._metadata).to.equal(ContainerSchema.ID);
        expect(imageContainer.type).to.equal('Container');
        expect(imageContainer.width).to.equal('wrapContent');
        expect(imageContainer.height).to.equal('wrapContent');
        expect(imageContainer.style.padding).to.equal('0 0 0 0');
        expect(imageContainer.style.margin).to.equal('0 0 0 0');
        expect(imageContainer.layout.type).to.equal('Relative');
        expect(imageContainer.items.length).to.equal(0);
    }

    describe('GIVEN: An appstudio section with no properties', () => {
        const section = <SectionDto>AppstudioModelFactory.create('Section');

        describe('WHEN:  translated', () => {

            before((done) => {
                translator = new SectionTranslator({});

                translator.translate('videa', service, section).then(
                    (t) => {
                        translation = t;
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: Translation should be a valid sections container', () => {
                validateSectionContainer(translation);
            });
        });

    });

    describe('GIVEN: An appstudio section with an icon', () => {
        const backgroundColor = '#AAAAAA';
        const padding = '1 1 1 1';
        const margin = '1 1 1 1';
        const aspectRatio = '1:1';
        const width = 1;
        const height = 1;
        const image = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                    value: aspectRatio
                }),
                scale: 'aspectFit',
                gravity: 'Left',
                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                }),
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                })
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
        const settings = <SectionDto>AppstudioModelFactory.create('ImageSettingsSection', {
            style: AppstudioModelFactory.create('ColoredComponentStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
                    value: backgroundColor
                }
            })
        });
        const section = <SectionDto>AppstudioModelFactory.create('Section', {
            image: AppstudioModelFactory.create('ImageSection', {
                settings: settings,
                image: [
                    image
                ]
            })
        });

        let translatedImageContainer;

        describe('WHEN:  translated', () => {

            before((done) => {
                translator.translate('videa', service, section).then(
                    (t) => {
                        translation = t;
                        translatedImageContainer = translation.items[0];
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: sections container is not empty', () => {
                expect(translation.items).to.not.be.empty;
            });

            it('THEN: the image container has a valid image component', () => {
                expect(translatedImageContainer).to.exist;
                expect(translatedImageContainer.items[0]._metadata).to.equal(ClientImageSchema.ID);
                expect(translatedImageContainer.items[0].type).to.equal('Image');
                expect(translatedImageContainer.items[0].value).to.equal('test');
                expect(translatedImageContainer.items[0].scale).to.equal('aspectFit');
                expect(translatedImageContainer.items[0].aspectRatio).to.equal(1);
                expect(translatedImageContainer.items[0].height).to.equal(height);
                expect(translatedImageContainer.items[0].width).to.equal(width);
                expect(translatedImageContainer.items[0].gravity).to.equal('Left');
                expect(translatedImageContainer.items[0].style.padding).to.equal(padding);
                expect(translatedImageContainer.items[0].style.margin).to.equal(margin);
            });
        });
    });

    describe('GIVEN: An appstudio section with a title', () => {
        const backgroundColor = '#AAAAAA';
        const padding = '1 1 1 1';
        const margin = '1 1 1 1';
        const size = 10;
        const alignment = 'Left';
        const width = 1;
        const height = 1;
        const label = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
            style: AppstudioModelFactory.create('LabelStyle', {
                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                }),
                gravity: 'Left',
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                font: ClientAppModelFactory.create('OpenSans', {
                    weight: 'Regular',
                    color: {
                        value: backgroundColor
                    },
                    size: size,
                    alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment})
                })
            }),
            content: AppstudioModelFactory.create('LabelContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
        const settings = <SectionDto>AppstudioModelFactory.create('ImageSettingsSection', {
            style: AppstudioModelFactory.create('ColoredComponentStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
                    value: backgroundColor
                }
            })
        });
        const section = <SectionDto>AppstudioModelFactory.create('Section', {
            label: AppstudioModelFactory.create('LabelSection', {
                settings: settings,
                text: [
                    label
                ]
            })
        });

        let translatedLabelContainer;

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, section).then(
                    (t) => {
                        translation = t;
                        translatedLabelContainer = translation.items[1];
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: the label Container has a valid label component', () => {
                expect(translatedLabelContainer).to.exist;
                expect(translatedLabelContainer.items[0].type).to.equal('Label');
                expect(translatedLabelContainer.items[0].value).to.equal('test');
                expect(translatedLabelContainer.items[0].height).to.equal(1);
                expect(translatedLabelContainer.items[0].width).to.equal(1);
                expect(translatedLabelContainer.items[0].gravity).to.equal('Left');
                expect(translatedLabelContainer.items[0].style.padding).to.equal(padding);
                expect(translatedLabelContainer.items[0].style.margin).to.equal(margin);
                expect(translatedLabelContainer.items[0].style.font.size).to.equal(size);
                expect(translatedLabelContainer.items[0].style.font.alignment).to.equal(alignment);
            });
        });
    });

    describe('GIVEN: An appstudio section with setting style', () => {
        const backgroundColor = '#AAAAAA';
        const padding = '1 1 1 1';
        const margin = '1 1 1 1';
        const section = <SectionDto>AppstudioModelFactory.create('Section', {
            settings: AppstudioModelFactory.create('SectionSettings', {
                style: AppstudioModelFactory.create('SectionStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                    backgroundColor: ClientAppModelFactory.create('Color', {
                        value: backgroundColor
                    })
                })
            })
        });

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, section).then(
                    (t) => {
                        translation = t;
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: Translation should be a valid sections style', () => {
                expect(translation._metadata).to.equal(ContainerSchema.ID);
                expect(translation.type).to.equal('Container');
                expect(translation.width).to.equal('matchParent');
                expect(translation.height).to.equal('wrapContent');
                expect(translation.style.padding).to.equal(padding);
                expect(translation.style.margin).to.equal(margin);
                expect(translation.style.backgroundColor.value).to.equal(backgroundColor);
                expect(translation.layout.type).to.equal('Linear');
                expect(translation.items.length).to.equal(2);
            });
        });
    });

    describe('GIVEN: An appstudio section with setting controls', () => {
        const visible = 'not true';
        const section = <SectionDto>AppstudioModelFactory.create('Section', {
            settings: AppstudioModelFactory.create('SectionSettings', {

                controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                    visible: visible
                })
            })
        });
        const selectEvent = ClientAppModelFactory.create(SelectEventSchema.ID);
        const select = 'Select';
        const signoutAction = ClientAppModelFactory.create(SignoutActionSchema.ID);
        const signout = 'Signout';
        const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener', {
            event: selectEvent,
            action: signoutAction
        });

        section.settings.controls.listeners.push(listener);

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, section).then(
                    (t) => {
                        translation = t;
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: visible property should be translated to ' + visible, () => {
                expect(translation.visible).to.equal(visible);
            });
            it('THEN: the listener event type property should be translated as ' + select, () => {
                expect(translation.listeners[0].event.type).to.equal(select);
            });
            it('THEN: the listener action type property should be translated as ' + signout, () => {
                expect(translation.listeners[0].action.type).to.equal(signout);
            });
        });

    });
});
