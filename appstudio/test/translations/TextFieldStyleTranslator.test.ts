///<reference path="../../../typings/index.d.ts"/>

import { expect } from 'chai';

import { TextFieldStyleTranslator } from '../../translation/field/style/TextFieldStyleTranslator';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import { TextFieldStyle as AppstudioTextFieldStyleDto } from '../../dto/app/configuration/uiconfig/screen/component/field/textfield/TextFieldStyle';

import AlignmentSchema = require('../../schema/common/Alignment');

describe('TextFieldStyle Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(OpenSansTranslator);

    describe('GIVEN: an empty TextField Style', () => {
        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle');

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the font property should be undefined ', () => {
                expect(translation.font).to.equal(undefined);
            });
        });
    });

    describe('GIVEN: a TextFieldStyle with all font parameters', () => {
        const family = 'OpenSans-Bold';
        const size = 12;
        const alignment = 'Right';
        const weight = 'Bold';
        const color = '#AAAAAA';

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                color: AppstudioModelFactory.create('Color', {value: color}),
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font family property should be translated as ' + family, () => {
                expect(translation.font.family).to.equal(family);
            });
            it('THEN: the font size property should be translated as ' + size, () => {
                expect(translation.font.size).to.equal(size);
            });
            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
            it('THEN: the color property should be translated as ' + color, () => {
                expect(translation.font.color.value).to.equal(color);
            });
        });
    });

    describe('GIVEN: a TextFieldStyle set font size to Extra Small', () => {
        const size = 10;
        const sizeResult = 10;
        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a TextFieldStyle set font size to Small', () => {
        const size = 13;
        const sizeResult = 13;

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });
    describe('GIVEN: a TextFieldStyle set font size to Medium', () => {
        const size = 15;
        const sizeResult = 15;

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a TextFieldStyle set font size to Large', () => {
        const size = 18;
        const sizeResult = 18;

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a TextFieldStyle set font size to Extra Large', () => {
        const size = 22;
        const sizeResult = 22;

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a TextFieldStyle set font alignment to Left', () => {
        const alignment = 'Left';

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
        });
    });
    describe('GIVEN: a TextFieldStyle set font alignment to Right', () => {
        const alignment = 'Right';

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
        });
    });

    describe('GIVEN: a TextFieldStyle set font alignment to Center', () => {
        const alignment = 'Center';

        const source = <AppstudioTextFieldStyleDto>AppstudioModelFactory.create('TextFieldStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new TextFieldStyleTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
        });
    });
});
