import chai = require('chai');
const expect = chai.expect;

import { Favorites as AppstudioFavoritesDto } from '../../dto/app/configuration/Favorites';
import { BaseProvider as BaseProvider } from '../../dto/app/configuration/favorites/BaseProvider';
import { Favorites as FavoritesDto } from '../../dto/clientapp/app/Favorites';

import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { FavoritesTranslator } from '../../translation/favorites/FavoritesTranslator';
import { LocalProviderTranslator } from '../../translation/favorites/LocalProviderTranslator';
import { CustomDataTranslator } from '../../translation/CustomDataTranslator';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import AppstudioFavoritesSchema = require('../../schema/appstudio/app/configuration/Favorites');
import FavoritesSchema = require('../../schema/clientapp/app/Favorites');
import LocalProviderSchema = require('../../schema/appstudio/app/configuration/favorites/LocalProvider');
import NoDataSchema = require('../../schema/common/NoData');
import CustomDataSchema = require('../../schema/common/CustomData');

describe('Favorites Translator Tests', () => {
    const accountId: string = 'videa';
    const translationService = new TranslationService();
	translationService.registerTranslator(FavoritesTranslator);
	translationService.registerTranslator(LocalProviderTranslator);
	translationService.registerTranslator(CustomDataTranslator);
    const translator = new FavoritesTranslator();

    describe('GIVEN: Favorites without information', () => {
        const appstudioFavorites = <AppstudioFavoritesDto<BaseProvider>>AppstudioModelFactory.create(AppstudioFavoritesSchema.ID);

        describe('WHEN: translated', () => {
            let translation: FavoritesDto;
            before((done) => {
                new FavoritesTranslator().translate(accountId, translationService, appstudioFavorites).then(
                    (favoritesDto: FavoritesDto) => {
                        translation = favoritesDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation is NULL', () => {
                expect(translation).to.equal(null);
            });
        });
    });

    describe('GIVEN: Favorites with none provider', () => {
        let appstudioFavorites = <AppstudioFavoritesDto<BaseProvider>>AppstudioModelFactory.create(AppstudioFavoritesSchema.ID);
        appstudioFavorites.provider = <any>AppstudioModelFactory.create(NoDataSchema.ID);

        describe('WHEN: translated', () => {
            let translation: FavoritesDto;
            before((done) => {
                new FavoritesTranslator().translate(accountId, translationService, appstudioFavorites).then(
                    (favoritesDto: FavoritesDto) => {
                        translation = favoritesDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation is NULL', () => {
                expect(translation).to.equal(null);
            });
        });
    });

    describe('GIVEN: Favorites with Local provider', () => {
        const appstudioFavorites = <AppstudioFavoritesDto<BaseProvider>>AppstudioModelFactory.create(AppstudioFavoritesSchema.ID);
        appstudioFavorites.provider = <any>AppstudioModelFactory.create(LocalProviderSchema.ID, {
            frequency: 1000,
            uid: 'myUID',
            link: 'www.any_given_link.com',
            collectionPath: 'anyGivenCollectionPath',
            undesiredProperty: 'undesiredValue'
        });

        describe('WHEN: translated', () => {
            let translation: FavoritesDto;
            before((done) => {
                new FavoritesTranslator().translate(accountId, translationService, appstudioFavorites).then(
                    (favoritesDto: FavoritesDto) => {
                        translation = favoritesDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation has Favorites metadata AND Local provider properties', () => {
                expect(translation).to.have.property('_metadata', FavoritesSchema.ID);
                expect(translation).to.have.property('type', 'Local');
                expect(translation).to.have.property('provider', 'MPX');
                expect(translation).to.have.property('frequency', 1000);
                expect(translation).to.have.property('uid', 'myUID');
                expect(translation).to.have.property('link', 'www.any_given_link.com');
                expect(translation).to.have.property('collectionPath', 'anyGivenCollectionPath');
                expect(translation).to.not.have.property('undesiredProperty');
            });
        });
    });

    describe('GIVEN: Favorites with Custom provider', () => {
        const provider = <any>AppstudioModelFactory.create(CustomDataSchema.ID);
        provider.content = '{ "myProvider": "myCustomProvider", "favoritesProp1": "prop1Value", "favoritesNumber": 7 }';

        const appstudioFavorites = <AppstudioFavoritesDto<BaseProvider>>AppstudioModelFactory.create(AppstudioFavoritesSchema.ID);
        appstudioFavorites.provider = provider;
        describe('WHEN: translated', () => {
            let translation: FavoritesDto;
            before((done) => {
                translator.translate(accountId, translationService, appstudioFavorites).then(
                    (favoritesDto: FavoritesDto) => {
                        translation = favoritesDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation has Favorites metadata AND custom provider properties', () => {
                expect(translation).to.have.property('_metadata', FavoritesSchema.ID);
                expect(translation).to.not.have.property('provider');
                expect(translation).to.have.property('myProvider', 'myCustomProvider');
                expect(translation).to.have.property('favoritesProp1', 'prop1Value');
                expect(translation).to.have.property('favoritesNumber', 7);
            });
        });
    });
});