import { expect } from 'chai';

import { CollectionPickerStyleTranslator } from '../../translation/dataview/style/CollectionPickerStyleTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import { PickerStyle as AppstudioCollectionPickerStyleDto } from '../../dto/app/configuration/uiconfig/screen/collectionview/picker/PickerStyle';

import AlignmentSchema = require('../../schema/common/Alignment');

describe('CollectionPickerStyle Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(OpenSansTranslator);

    const translator = new CollectionPickerStyleTranslator();

    describe('GIVEN: an empty CollectionPickerStyle', () => {
        const source = <AppstudioCollectionPickerStyleDto>AppstudioModelFactory.create('CollectionPickerStyle');

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });
            it('THEN: the font should be undefined ', () => {
                expect(translation.font).to.equal(undefined);
            });
        });
    });

    describe('GIVEN: a CollectionPickerStyle with font family, size, alignment and weight', () => {
        const family = 'OpenSans-Bold';
        const size = 11;
        const color = '#FFFFFE';
        const alignment = 'right';
        const weight = 'Bold';
        const source = <AppstudioCollectionPickerStyleDto>AppstudioModelFactory.create('CollectionPickerStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                color: AppstudioModelFactory.create('Color', {
                    value: color
                }),
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font family property should be translated as ' + family, () => {
                expect(translation.font.family).to.equal(family);
            });
            it('THEN: the font size property should be translated as ' + size, () => {
                expect(translation.font.size).to.equal(size);
            });
            it('THEN: the font color property should be translated as ' + color, () => {
                expect(translation.font.color.value).to.equal(color);
            });
            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
        });
    });
});
