///<reference path="../../../typings/index.d.ts"/>

import { expect } from 'chai';

import { LabelStyleTranslator } from '../../translation/field/style/LabelStyleTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';
import { RobotoTranslator } from '../../translation/font/RobotoTranslator';
import { CustomFontTranslator } from '../../translation/font/CustomFontTranslator';
import { SanFranciscoTranslator } from '../../translation/font/SanFranciscoTranslator';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import { LabelStyle as AppstudioLabelStyleDto } from '../../dto/app/configuration/uiconfig/screen/component/field/label/LabelStyle';

import AlignmentSchema = require('../../schema/common/Alignment');

describe('LabelStyle Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(OpenSansTranslator);
    translationService.registerTranslator(RobotoTranslator);
    translationService.registerTranslator(CustomFontTranslator);
    translationService.registerTranslator(SanFranciscoTranslator);

    describe('GIVEN: a Label Style', () => {
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle');

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the font should be undefined ', () => {
                expect(translation.font).to.equal(undefined);
            });
        });
    });

    describe('GIVEN: a LabelStyle with all font parameters', () => {
        const family = 'OpenSans-Bold';
        const size = 12;
        const alignment = 'Right';
        const weight = 'Bold';
        const color = '#AAAAAA';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                color: AppstudioModelFactory.create('Color', {value: color}),
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font family property should be translated as ' + family, () => {
                expect(translation.font.family).to.equal(family);
            });
            it('THEN: the font size property should be translated as ' + size, () => {
                expect(translation.font.size).to.equal(size);
            });
            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });

            it('THEN: the color property should be translated as ' + color, () => {
                expect(translation.font.color.value).to.equal(color);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font size to 10', () => {
        const size = 10;
        const sizeResult = 10;
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font size to 13', () => {
        const size = 13;
        const sizeResult = 13;
        const weight = 'Regular';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });
    describe('GIVEN: a LabelStyle set font size to 15', () => {
        const size = 15;
        const sizeResult = 15;
        const weight = 'Regular';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font size to 18', () => {
        var size = 18,
            sizeResult = 18,
            weight = 'Regular',
            source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
                font: ClientAppModelFactory.create('OpenSans', {
                    size: size,
                    weight: weight
                })
            });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font size to 22', () => {
        const size = 22;
        const sizeResult = 22;
        const weight = 'Regular';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                size: size,
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + sizeResult, () => {
                expect(translation.font.size).to.equal(sizeResult);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font alignment to Left', () => {
        const alignment = 'Left';
        const weight = 'Regular';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font size property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font alignment to Right', () => {
        const alignment = 'Right';
        const weight = 'Regular';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font alignment to Center', () => {
        const alignment = 'Center';
        const weight = 'Regular';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('OpenSans', {
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: weight
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font alignment property should be translated as ' + alignment, () => {
                expect(translation.font.alignment).to.equal(alignment);
            });
        });
    });


    describe('GIVEN: a LabelStyle set font family to Roboto', () => {
        const family = 'Roboto-Regular';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('Roboto', {
                weight: 'Regular'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font weight property should be translated as ' + family, () => {
                expect(translation.font.family).to.equal(family);
            });
        });
    });

    describe('GIVEN: a LabelStyle set font family to .SFUIText-Bold', () => {
        const family = '.SFUIText-Bold';
        const source = <AppstudioLabelStyleDto>AppstudioModelFactory.create('LabelStyle', {
            font: ClientAppModelFactory.create('SanFrancisco', {
                weight: 'Bold'
            })
        });

        describe('WHEN:  translated', () => {
            const translator = new LabelStyleTranslator();
            let translation = null;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the font family property should be ' + family, () => {
                expect(translation.font.family).to.equal(family);
            });
        });
    });
});
