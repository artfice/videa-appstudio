import chai = require('chai');
const expect = chai.expect;

import sinon = require('sinon');

import _ = require('lodash');
import Promise = require('bluebird');

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {UIConfigTranslator} from '../../translation/uiConfig/UIConfigTranslator';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {UIConfig as AppStudioUIConfigDto} from '../../dto/app/configuration/UIConfig';
import AppStudioUIConfigSchema = require('../../schema/appstudio/app/configuration/UIConfig');
import {Theme as AppStudioTheme} from '../../dto/app/configuration/uiconfig/Theme';
import {Assets as AppStudioAsset} from '../../dto/app/configuration/uiconfig/theme/Assets';
import {BaseMenu as BaseMenu} from '../../dto/common/navigation/BaseMenu';

import {UIConfig as ClientUIConfigDto} from '../../dto/clientapp/app/UIConfig';
import {Analytics} from '../../dto/app/configuration/uiconfig/Analytics';
import {Colors as AppStudioColors} from '../../dto/app/configuration/uiconfig/theme/Colors';

import {GalleryImageRepository as GalleryImageServiceMongoDB} from '../../repository/GalleryImageRepository';
import {ScreenApplicationService as ScreenServiceIntegration} from '../../application/ScreenApplicationService';

describe('UIConfig Translator Tests', () => {
    
    const galleryImageService = new GalleryImageServiceMongoDB(<any>{});
    sinon.stub(galleryImageService, 'getById').returns(
        Promise.resolve(<any> {
            rawUrl: 'imageUrl'
        })
    );

    const screenService = new ScreenServiceIntegration(<any>{});
    sinon.stub(screenService, 'getScreens').returns(
        Promise.resolve([])
    );

    const translationService = new TranslationService();
    translationService.registerTranslator(UIConfigTranslator, null);

    
    describe('GIVEN: an Appstudio with an empty Theme', () => {
        const appStudioUIConfig = <AppStudioUIConfigDto<BaseMenu>> AppstudioModelFactory.create(AppStudioUIConfigSchema.ID);
        appStudioUIConfig.theme = <AppStudioTheme> {};

        describe('WHEN: translated', () => {
            let translation: ClientUIConfigDto<Analytics>;

            before((done) => {
                const uiConfigTranslator = new UIConfigTranslator({
                    galleryService: galleryImageService,
                    screenService: screenService
                });

                uiConfigTranslator.translate(null, translationService, appStudioUIConfig).then(
                    (clientUIConfigDto: ClientUIConfigDto<Analytics>) => {
                        translation = clientUIConfigDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation has a Theme with Assets AND Colors items as NULL', () => {
                expect(translation).to.exist;
                expect(translation.theme).to.exist;

                expect(translation.theme.asset).to.exist;
                expect(translation.theme.asset.logo).to.be.null;
                expect(translation.theme.asset.splashScreen).to.be.null;
                expect(translation.theme.asset.background).to.be.null;
                expect(translation.theme.asset.navBackground).to.be.null;
                expect(translation.theme.asset.appIcon).to.be.null;
                expect(translation.theme.asset.topBar).to.be.null;

                expect(translation.theme.colors).to.be.exist;
                expect(translation.theme.colors.primarySpot).to.be.null;
                expect(translation.theme.colors.secondarySpot).to.be.null;
                expect(translation.theme.colors.background).to.be.null;
                expect(translation.theme.colors.foreground).to.be.null;
                expect(translation.theme.colors.accent).to.be.null;
            });
        });
    });

    describe('GIVEN: an Appstudio with Theme containing all Assets and Color properties', () => {
        const themeAssets: AppStudioAsset = <AppStudioAsset> {
            logo: {
                galleryImageId: 'logoId'
            },
            splashScreen: {
                galleryImageId: 'splashScreenId'
            },
            background: {
                galleryImageId: 'backgroundId',
                color: {
                    value: 'backgroundColor'
                }
            },
            navBackground: {
                galleryImageId: 'navBackgroundId',
                color: {
                    value: 'navBackgroundColor'
                }
            },
            topBar: {
                galleryImageId: '',     //The image value after translation must be NULL, for there is no imageId.
                color: {
                    value: 'topBarColor'
                }
            },
            appIcon: {
                galleryImageId: 'appIconId',
            }
        };

        const themeColors: AppStudioColors = <AppStudioColors> {
            primarySpot: {
                value: 'primarySpotColorValue'
            },
            secondarySpot: {
                value: ''
            },
            background: {
                value: 'backgroundColorValue'
            },
            foreground: {
                value: 'foregroundColorValue'
            },
            accent: {
                value: 'accentColorValue'
            }
        };

        const appStudioUIConfig = <AppStudioUIConfigDto<BaseMenu>> AppstudioModelFactory.create(AppStudioUIConfigSchema.ID);
        appStudioUIConfig.theme = <AppStudioTheme> {
            assets: themeAssets,
            colors: themeColors
        };

        describe('WHEN: translated', () => {
            let translation: ClientUIConfigDto<Analytics>;

            before((done) => {
                const uiConfigTranslator = new UIConfigTranslator({
                    galleryService: galleryImageService,
                    screenService: screenService
                });

                uiConfigTranslator.translate(null, translationService, appStudioUIConfig).then(
                    (clientUIConfigDto: ClientUIConfigDto<Analytics>) => {
                        translation = clientUIConfigDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation has only the expected Assets and Colors', () => {
                expect(translation).to.exist;
                expect(translation.theme).to.exist;
                
                expect(translation.theme.asset).to.exist;
                expect(translation.theme.asset).to.have.deep.property('logo.value', 'imageUrl');
                expect(translation.theme.asset.splashScreen).to.be.null;
                expect(translation.theme.asset.background).to.be.null;
                expect(translation.theme.asset.navBackground).to.be.null;
                expect(translation.theme.asset.appIcon).to.be.null;
                
                expect(translation.theme.asset.topBar).to.exist;
                expect(translation.theme.asset.topBar).to.not.have.property('value');
                expect(translation.theme.asset.topBar).to.have.deep.property('color.value', 'topBarColor');

                expect(translation.theme.colors).to.exist;
                expect(translation.theme.colors).to.have.deep.property('primarySpot.value', 'primarySpotColorValue');
                expect(translation.theme.colors).to.have.deep.property('secondarySpot.value', '');
                expect(translation.theme.colors.background).to.be.null;
                expect(translation.theme.colors.foreground).to.be.null;
                expect(translation.theme.colors).to.have.deep.property('accent.value', 'accentColorValue');
            });
        });
    });

});