import chaiLib = require('chai');
const expect = chaiLib.expect;

import {ComponentTranslator as ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {Component as ComponentDto} from '../../dto/app/configuration/uiconfig/screen/component/Component';
import {ComponentStyle as ComponentStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';

import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {BaseTranslator as BaseTranslator} from '../../translation/BaseTranslator';

describe('Component Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty component', () => {
		var source = <ComponentDto<ComponentStyle>>AppstudioModelFactory.create('Component');
		describe('WHEN: translated', () => {
			var translator = new ComponentTranslator(),
				translation;

			translationService.registerTranslator(BaseTranslator);
			translationService.registerTranslator(ComponentStyleTranslator);
			translationService.registerTranslator(ComponentControlsTranslator);

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the id property should be translated as ' + source.id, () => {
				expect(translation.id).to.equal(source.id);
			});
		});
	});
});
