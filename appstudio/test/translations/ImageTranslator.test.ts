import chaiLib = require('chai');
const expect = chaiLib.expect;

import { BaseFieldTranslator as BaseFieldTranslator } from '../../translation/field/BaseFieldTranslator';
import { ImageTranslator as ImageTranslator } from '../../translation/field/ImageTranslator';
import { ImageStyleTranslator as ImageStyleTranslator } from '../../translation/field/style/ImageStyleTranslator';
import { FieldContentTranslator as FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';
import { ComponentControlsTranslator as ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ComponentStyleTranslator as ComponentStyleTranslator } from '../../translation/component/ComponentStyleTranslator';
import { ComponentTranslator as ComponentTranslator } from '../../translation/component/ComponentTranslator';
import { ListenerTranslator as ListenersTranslator } from '../../translation/ListenerTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { AppstudioModelFactory as AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import { Image as AppstudioImageDto } from '../../dto/app/configuration/uiconfig/screen/component/field/image/Image';

import { ComponentControls as ComponentControlsDto } from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import { BaseEvent as BaseEventDto } from '../../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../../dto/common/listener/action/BaseAction';

import { StandardAspectRatioTranslator } from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import { CustomAspectRatioTranslator } from '../../translation/aspectRatio/CustomAspectRatioTranslator';

describe('Image Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(ImageStyleTranslator);
	translationService.registerTranslator(FieldContentTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(BaseFieldTranslator);
	translationService.registerTranslator(ListenersTranslator);
	translationService.registerTranslator(ImageTranslator);
	translationService.registerTranslator(StandardAspectRatioTranslator);
	translationService.registerTranslator(CustomAspectRatioTranslator);

	describe('GIVEN: an Image', () => {
		var source = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
			style: AppstudioModelFactory.create('ImageStyle'),
			content: AppstudioModelFactory.create('FieldContent'),
			controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls')
		});

		describe('WHEN: translated', () => {
			var translator = new ImageTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it('THEN: the value property should be translated as undefined', () => {
				expect(translation.value).to.equal(undefined);
			});
			it('THEN: the width property should be translated as undefined', () => {
				expect(translation.width).to.equal(undefined);
			});
			it('THEN: the height property should be translated as undefined', () => {
				expect(translation.height).to.equal(undefined);
			});
			it('THEN: the gravity property should be translated as undefined', () => {
				expect(translation.gravity).to.equal(undefined);
			});
			it('THEN: the visible property should be translated as true', () => {
				expect(translation.visible).to.equal('true');
			});
			it('THEN: the listeners property should be translated as []', () => {
				expect(translation.listeners.length).to.equal(0);
			});
		});
	});

	describe('GIVEN: an Image with base style and value', () => {
		var source = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
			aspectRatio: '1:1',
			style: AppstudioModelFactory.create('ImageStyle', {
				aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
				scale: 'aspectFit',
				gravity: 'Left',
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

		describe('WHEN: translated', () => {
			var translator = new ImageTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as test', () => {
				expect(translation.value).to.equal('test');
			});			
			it ('THEN: the width property should be translated as 1', () => {
				expect(translation.width).to.equal(1);
			});			
			it ('THEN: the height property should be translated as 1', () => {
				expect(translation.height).to.equal(1);
			});			
			it ('THEN: the scale property should be translated as aspectFit', () => {
				expect(translation.scale).to.equal('aspectFit');
			});			
			it ('THEN: the aspectRatio property should be translated as 1:1', () => {
				expect(translation.aspectRatio).to.equal(1);
			});			
			it ('THEN: the gravity property should be translated as Left', () => {
				expect(translation.gravity).to.equal('Left');
			});			
			it ('THEN: the visible property should be translated as not true', () => {
				expect(translation.visible).to.equal('not true');
			});			
		});
	});
});