import chaiLib = require('chai');
const expect = chaiLib.expect;

import {FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {FieldContent as AppstudioFieldContentDto} from '../../dto/app/configuration/uiconfig/screen/component/field/FieldContent';

describe('FieldContent Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty Field Content', () => {
		var source = <AppstudioFieldContentDto<string>>AppstudioModelFactory.create('FieldContent'),
			value = undefined;

		describe('WHEN:  translated', () => {
			var translator = new FieldContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as ' + value, () => {
				expect(translation.value).to.equal(value);
			});
		});
	});

	describe('GIVEN: a Field Content with a value', () => {
		var value = '1 1 1 1',
			source = <AppstudioFieldContentDto<string>>AppstudioModelFactory.create('FieldContent', {
            value: value
		});

		describe('WHEN:  translated', () => {
			var translator = new FieldContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as ' + value, () => {
				expect(translation.value).to.equal(value);
			});
		});
	});
});
