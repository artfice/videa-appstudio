import chaiLib = require('chai');
const expect = chaiLib.expect;

import {SizedComponentStyleTranslator as SizedComponentStyleTranslator} from '../../translation/component/SizedComponentStyleTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {SizedComponentStyle as AppstudioSizedComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/SizedComponentStyle';

describe('SizedComponentStyle Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: a SizedComponentStyle', () => {
		var source = <AppstudioSizedComponentStyleDto>AppstudioModelFactory.create('SizedComponentStyle'),
			width = undefined,
			height = undefined;

		describe('WHEN:  translated', () => {
			var translator = new SizedComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the width property should be translated as ' + width, () => {
				expect(translation.width).to.equal(width);
			});
			it ('THEN: the height property should be translated as ' + height, () => {
				expect(translation.height).to.equal(height);
			});
		});
	});

	describe('GIVEN: a SizedComponentStyle with width and height', () => {
		var width = 'wrapContent',
			height = 'wrapContent',
			source = <AppstudioSizedComponentStyleDto>AppstudioModelFactory.create('SizedComponentStyle', {
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('WrapContent')
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('WrapContent')
				})
		});

		describe('WHEN:  translated', () => {
			var translator = new SizedComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});
			it ('THEN: the width property should be translated as ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should be translated as ' + height, () => {
				expect(translation.height).to.equal(height);
			});				
		});
	});	

describe('GIVEN: a SizedComponentStyle with width and height', () => {
		var width = 'matchParent',
			height = 'matchParent',
			source = <AppstudioSizedComponentStyleDto>AppstudioModelFactory.create('SizedComponentStyle', {
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('MatchParent')
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('MatchParent')
				})
		});

		describe('WHEN:  translated', () => {
			var translator = new SizedComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});
			it ('THEN: the width property should be translated as ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should be translated as ' + height, () => {
				expect(translation.height).to.equal(height);
			});				
		});
	});	

describe('GIVEN: a SizedComponentStyle with width and height', () => {
		var width = 1,
			height = 1,
			source = <AppstudioSizedComponentStyleDto>AppstudioModelFactory.create('SizedComponentStyle', {
            
			width: AppstudioModelFactory.create('Width', {
				value: AppstudioModelFactory.create('CustomWidth', {
					value: width
				})
			}),
			height: AppstudioModelFactory.create('Height', {
				value: AppstudioModelFactory.create('CustomHeight', {
					value: height
				})
			})
		});

		describe('WHEN:  translated', () => {
			var translator = new SizedComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});
			it ('THEN: the width property should be translated as ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should be translated as ' + height, () => {
				expect(translation.height).to.equal(height);
			});				
		});
	});			
});