///<reference path="../../../typings/index.d.ts"/>

import {expect} from 'chai';

import {ImageStyleTranslator} from '../../translation/field/style/ImageStyleTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import ImageStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/field/image/ImageStyle');
import {ImageStyle as AppstudioImageStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/field/image/ImageStyle';

import {StandardAspectRatioTranslator} from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import {CustomAspectRatioTranslator} from '../../translation/aspectRatio/CustomAspectRatioTranslator';

import ScaleSchema = require('../../schema/common/Scale');

describe('ImageStyle Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(StandardAspectRatioTranslator);
    translationService.registerTranslator(CustomAspectRatioTranslator);

    describe('GIVEN: an empty Image Style', () => {
        const source = <AppstudioImageStyleDto> AppstudioModelFactory.create(ImageStyleSchema.ID);
        const expectedScale: string = undefined;
        const aspectRatio = undefined;

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new ImageStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the scale property should be translated as ' + expectedScale, () => {
                expect(translation.scale).to.equal(expectedScale);
            });

            it('THEN: the aspectRatio property should be translated as ' + aspectRatio, () => {
                expect(translation.aspectRatio).to.equal(aspectRatio);
            });
        });
    });

    describe('GIVEN: an ImageStyle with aspectRatio as 1:1 AND scale as "Aspect Fit"', () => {
        const aspectRatio = '1:1';
        const aspectRatioResult = 1;

        const scale: string = 'Aspect Fit';
        const expectedScale: string = 'aspectFit';

        const source = <AppstudioImageStyleDto> AppstudioModelFactory.create(ImageStyleSchema.ID, {
            aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                value: aspectRatio
            }),
            scale: AppstudioModelFactory.create(ScaleSchema.ID, {
                value: scale
            })
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new ImageStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the aspectRatio property should be translated as ' + aspectRatioResult, () => {
                expect(translation.aspectRatio).to.equal(aspectRatioResult);
            });

            it('THEN: the scale property should be translated as ' + expectedScale, () => {
                expect(translation.scale).to.equal(expectedScale);
            });
        });
    });

    describe('GIVEN: an ImageStyle with aspectRatio as 4:3 AND scale as "Aspect Fill"', () => {
        const aspectRatio = '4:3';
        const aspectRatioResult = 4 / 3;

        const scale: string = 'Aspect Fill';
        const expectedScale: string = 'aspectFill';

        const source = <AppstudioImageStyleDto>AppstudioModelFactory.create(ImageStyleSchema.ID, {
            aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                value: aspectRatio
            }),
            scale: AppstudioModelFactory.create(ScaleSchema.ID, {
                value: scale
            })
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new ImageStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the aspectRatio property should be translated as ' + aspectRatioResult, () => {
                expect(translation.aspectRatio).to.equal(aspectRatioResult);
            });

            it('THEN: the scale property should be translated as ' + expectedScale, () => {
                expect(translation.scale).to.equal(expectedScale);
            });
        });
    });

    describe('GIVEN: an ImageStyle with aspectRatio as 16:9 AND scale as "Fill Parent"', () => {
        const aspectRatio = '16:9';
        const aspectRatioResult = 16 / 9;

        const scale: string = 'Fill Parent';
        const expectedScale: string = 'fillParent';

        const source = <AppstudioImageStyleDto>AppstudioModelFactory.create(ImageStyleSchema.ID, {
            aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                value: aspectRatio
            }),
            scale: AppstudioModelFactory.create(ScaleSchema.ID, {
                value: scale
            })
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new ImageStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the aspectRatio property should be translated as ' + aspectRatioResult, () => {
                expect(translation.aspectRatio).to.equal(aspectRatioResult);
            });

            it('THEN: the scale property should be translated as ' + expectedScale, () => {
                expect(translation.scale).to.equal(expectedScale);
            });
        });
    });

    describe('GIVEN: an ImageStyle with aspectRatio', () => {
        const aspectRatio = '2:3';
        const aspectRatioResult = 2 / 3;

        const source = <AppstudioImageStyleDto>AppstudioModelFactory.create(ImageStyleSchema.ID, {
            aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                value: aspectRatio
            })
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new ImageStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the aspectRatio property should be translated as ' + aspectRatioResult, () => {
                expect(translation.aspectRatio).to.equal(aspectRatioResult);
            });
        });
    });

    describe('GIVEN: an ImageStyle with aspectRatio as 10:3', () => {
        const aspectRatio = '10:3';
        const aspectRatioResult = 10 / 3;

        const source = <AppstudioImageStyleDto>AppstudioModelFactory.create(ImageStyleSchema.ID, {
            aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                value: aspectRatio
            })
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new ImageStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the aspectRatio property should be translated as ' + aspectRatioResult, () => {
                expect(translation.aspectRatio).to.equal(aspectRatioResult);
            });
        });
    });

});
