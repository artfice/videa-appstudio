import chaiLib = require('chai');
const expect = chaiLib.expect;

import {BaseCollectionTranslator} from '../../translation/dataview/collection/BaseCollectionTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import JsonCollectionProviderSchema = require('../../schema/common/collection/provider/JsonProvider');
import MpxCollectionProviderSchema = require('../../schema/common/collection/provider/MpxProvider');
import VideaCollectionProviderSchema = require('../../schema/common/collection/provider/VideaProvider');
import CustomCollectionProviderSchema = require('../../schema/common/collection/provider/CustomProvider');

import {BaseCollection as AppstudioCollectionDto} from '../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';

describe('BaseCollection Translation Tests', () => {
  var translationService = new TranslationService();

  describe('GIVEN: an empty Base Collection', () => {
    var source = <AppstudioCollectionDto>AppstudioModelFactory.create('BaseCollection');

    describe('WHEN: translated', () => {
      var translator = new BaseCollectionTranslator(source),
      translation;

      before((done) => {
        translator.translate('videa', translationService, source).then(
        (t) => {
          translation = t;
          done();
        },
        (err) => {
          done();
        });
      });

      it ('THEN: the translation should not be an empty object', () => {
        expect(translation).to.exist;
      });
    });
  });

  describe('GIVEN: a BaseCollection with Json provider', () => {
    var provider = 'JSON',
    collectionPath = 'test',
    source = <AppstudioCollectionDto>AppstudioModelFactory.create('BaseCollection', {
      provider: AppstudioModelFactory.create(JsonCollectionProviderSchema.ID,{
      collectionPath: collectionPath
      })
    });

    describe('WHEN: translated', () => {
      var translator = new BaseCollectionTranslator(source),
      translation;

      before((done) => {
        translator.translate('videa', translationService, source).then(
        (t) => {
          translation = t;
          done();
        },
        (err) => {
          done();
        });
      });

      it ('THEN: the translation should have field provider and translates to ' + provider, () => {
        expect(translation.provider).to.equal(provider);
      });
      it ('THEN: the translation should have field collectionPath and translates to ' + collectionPath, () => {
        expect(translation.collectionPath).to.equal(collectionPath);
      });
    });
  });

  describe('GIVEN: a BaseCollection with Mpx provider', () => {
    var provider = 'MPX',
    collectionPath = 'test',
    source = <AppstudioCollectionDto>AppstudioModelFactory.create('BaseCollection', {
      provider: AppstudioModelFactory.create(MpxCollectionProviderSchema.ID,{
        collectionPath: collectionPath
      })
    });

    describe('WHEN: translated', () => {
      var translator = new BaseCollectionTranslator(source),
      translation;

      before((done) => {
        translator.translate('videa', translationService, source).then(
        (t) => {
          translation = t;
          done();
        },
        (err) => {
          done();
        });
      });

      it ('THEN: the translation should have field provider and translates to ' + provider, () => {
        expect(translation.provider).to.equal(provider);
      });
    });
  });

  describe('GIVEN: a BaseCollection with videa provider', () => {
    var provider = 'Videa',
    collectionPath = 'test',
    source = <AppstudioCollectionDto>AppstudioModelFactory.create('BaseCollection', {
      provider: AppstudioModelFactory.create(VideaCollectionProviderSchema.ID,{
        collectionPath: collectionPath
      })
    });

    describe('WHEN: translated', () => {
      var translator = new BaseCollectionTranslator(source),
      translation;

      before((done) => {
        translator.translate('videa', translationService, source).then(
        (t) => {
          translation = t;
          done();
        },
        (err) => {
          done();
        });
      });

      it ('THEN: the translation should have field provider and translates to ' + provider, () => {
        expect(translation.provider).to.equal(provider);
      });
    });
  });

describe('GIVEN: a BaseCollection with Custom provider', () => {
     var provider = 'hahaha',
     collectionPath = 'test',
     source = <AppstudioCollectionDto>AppstudioModelFactory.create('BaseCollection', {
       provider: AppstudioModelFactory.create(CustomCollectionProviderSchema.ID,{
         collectionPath: collectionPath,
         customProvider: provider
       })
     });
 
     describe('WHEN: translated', () => {
       var translator = new BaseCollectionTranslator(source),
       translation;
 
       before((done) => {
       translator.translate('videa', translationService, source).then(
       (t) => {
         translation = t;
         done();
       },
       (err) => {
         done();
       });
     });
 
       it ('THEN: the translation should have field provider and translates to ' + provider, () => {
         expect(translation.provider).to.equal(provider);
       });
     });
   });	
});
