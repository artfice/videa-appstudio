import chaiLib = require('chai');
const expect = chaiLib.expect;

import {ColoredComponentStyleTranslator} from '../../translation/component/ColoredComponentStyleTranslator';
import {ComplexColorTranslator} from '../../translation/color/ComplexColorTranslator';
import {ColorTranslator} from '../../translation/color/ColorTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {ColoredComponentStyle as AppstudioColoredComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/ColoredComponentStyle';
import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');

describe('ColoredComponentStyle Translation Tests', () => {
	var translationService = new TranslationService();

	translationService.registerTranslator(ComplexColorTranslator);
	translationService.registerTranslator(ColorTranslator);

	describe('GIVEN: a ColoredComponentStyle', () => {
		var source = <AppstudioColoredComponentStyleDto>AppstudioModelFactory.create('ColoredComponentStyle'),
			backgroundColor = undefined;

		describe('WHEN:  translated', () => {
			var translator = new ColoredComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the backgroundColor property should be translated as ' + backgroundColor, () => {
				expect(translation.backgroundColor).to.equal(backgroundColor);
			});
		});
	});

	describe('GIVEN: a ColoredComponentStyle with backgroundColor', () => {
		let backgroundColor = {
			_metadata: ColorSchema.ID,
			value: '#AAAAAAAA'
		};

		var source = <AppstudioColoredComponentStyleDto>AppstudioModelFactory.create('ColoredComponentStyle', {
            backgroundColor: {
            	_metadata: ComplexColorSchema.ID,
                value: backgroundColor
            }
		});

		describe('WHEN:  translated', () => {
			var translator = new ColoredComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});
			it ('THEN: the backgroundColor property should be translated as ' + JSON.stringify(backgroundColor), () => {
				expect(translation.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.backgroundColor._metadata).to.equal(backgroundColor._metadata);
			});
		});
	});
});
