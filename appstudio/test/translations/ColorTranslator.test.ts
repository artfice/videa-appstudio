import chaiLib = require('chai');
const expect = chaiLib.expect;

import { ColorTranslator } from '../../translation/color/ColorTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import ColorSchema = require('../../schema/common/Color');
import { Color as ColorDto } from '../../dto/common/Color';

describe('Color Translation Tests', () => {
    let translationService = new TranslationService();
    translationService.registerTranslator(ColorTranslator);

    describe('GIVEN: a Color with a value', () => {
        let value = '0xFF00FF00';

        let source = <ColorDto>ClientAppModelFactory.create(ColorSchema.ID, {
            value: value
        });

        describe('WHEN: translated', () => {
            let translator = new ColorTranslator();

            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the value property should translate to ' + value, () => {
                expect(translation.value).to.equal(value);
            });
        });
    });
});