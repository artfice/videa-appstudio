import chaiLib = require('chai');
const expect = chaiLib.expect;
import _ = require('lodash');

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {WelcomeScreen as WelcomeScreenDto} from '../../dto/app/configuration/uiconfig/settings/WelcomeScreen';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {WelcomeScreenTranslator} from '../../translation/WelcomeScreenTranslator';

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('Welcome Translation Tests', () => {
	const translationService = new TranslationService();

	describe('GIVEN: an empty welcome screen', () => {
		const source = <WelcomeScreenDto>AppstudioModelFactory.create('WelcomeScreen');

		describe('WHEN:  translated', () => {
			const translator = new WelcomeScreenTranslator();
			let translation;

			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the translation should undefined', () => {
				expect(translation).to.equal(undefined);
			});
		});
	});

	describe('GIVEN: a chromecast with enabled unchecked', () => {
		const source = <WelcomeScreenDto>AppstudioModelFactory.create('WelcomeScreen', {
			enabled: false
		});

		describe('WHEN:  translated', () => {
			const translator = new WelcomeScreenTranslator();
			let translation;

			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the translation should be undefined', () => {
				expect(translation).to.equal(undefined);
			});
		});
	});	

	describe('GIVEN: a chromecast with enabled checked', () => {
		const refId = 'a';
		const persistKey = 'persistKey';
		const source = <WelcomeScreenDto>AppstudioModelFactory.create('WelcomeScreen', {
			enabled: true,
			screen : AppstudioModelFactory.create('BaseReference', {
				value: 'Value',
				refId: refId
			}),
			persistKey: persistKey
		});

		describe('WHEN:  translated', () => {
			const translator = new WelcomeScreenTranslator();
			let translation;

			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the the persistKey property should be translated', () => {
				expect(translation.persistKey).to.equal(persistKey);
			});

			it ('THEN: the the welcomeScreenId property should be translated', () => {
				expect(translation.screenId).to.equal(refId);
			});
		});
	});	       
});