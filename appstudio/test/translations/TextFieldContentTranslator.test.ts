import chaiLib = require('chai');
var expect = chaiLib.expect;

import {TextFieldContentTranslator as TextFieldContentTranslator} from '../../translation/field/content/TextFieldContentTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {TextFieldContent as AppstudioTextFieldContentDto} from '../../dto/app/configuration/uiconfig/screen/component/field/content/TextFieldContent';

describe('TextFieldContent Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty TextField Content', () => {
		var source = <AppstudioTextFieldContentDto>AppstudioModelFactory.create('TextFieldContent'),
            value = undefined,
            numberOfLines = 1,
            hint = undefined,
            inputType = undefined,
            componentId = undefined;

		describe('WHEN:  translated', () => {
			var translator = new TextFieldContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as ' + value, () => {
				expect(translation.value).to.equal(value);
			});
			it ('THEN: the numberOfLines property should be translated as ' + numberOfLines, () => {
				expect(translation.numberOfLines).to.equal(numberOfLines);
			});
			it ('THEN: the hint property should be translated as ' + hint, () => {
				expect(translation.hint).to.equal(hint);
			});
			it ('THEN: the inputType property should be translated as ' + inputType, () => {
				expect(translation.inputType).to.equal(inputType);
			});
			it ('THEN: the componentId property should be translated as ' + componentId, () => {
				expect(translation.componentId).to.equal(componentId);
			});
		});
	});

	describe('GIVEN: a TextField Content with a value', () => {
		var numberOfLines = 1,
            hint = undefined,
            inputType = undefined,
            componentId = undefined,
            source = <AppstudioTextFieldContentDto>AppstudioModelFactory.create('TextFieldContent', {
            value: '1 1 1 1'
		});

		describe('WHEN:  translated', () => {
			var translator = new TextFieldContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the value property should be translated as 1 1 1 1', () => {
				expect(translation.value).to.equal('1 1 1 1');
			});
			it ('THEN: the numberOfLines property should be translated as ' + numberOfLines, () => {
				expect(translation.numberOfLines).to.equal(numberOfLines);
			});
            it ('THEN: the hint property should be translated as ' + hint, () => {
				expect(translation.hint).to.equal(hint);
			});
			it ('THEN: the inputType property should be translated as ' + inputType, () => {
				expect(translation.inputType).to.equal(inputType);
			});
			it ('THEN: the componentId property should be translated as ' + componentId, () => {
				expect(translation.componentId).to.equal(componentId);
			});
		});
	});

	describe('GIVEN: a TextField Content with an inputType', () => {
		var numberOfLines = 1,
            hint = undefined,
            inputType = 'Email',
            componentId = undefined,
            value = undefined,
            source = <AppstudioTextFieldContentDto>AppstudioModelFactory.create('TextFieldContent', {
            inputType: inputType
		});

		describe('WHEN:  translated', () => {
			var translator = new TextFieldContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the value property should be translated as ' + value, () => {
				expect(translation.value).to.equal(value);
			});
			it ('THEN: the numberOfLines property should be translated as ' + numberOfLines, () => {
				expect(translation.numberOfLines).to.equal(numberOfLines);
			});
            it ('THEN: the hint property should be translated as ' + hint, () => {
				expect(translation.hint).to.equal(hint);
			});
			it ('THEN: the inputType property should be translated as ' + inputType, () => {
				expect(translation.inputType).to.equal(inputType);
			});
			it ('THEN: the componentId property should be translated as ' + componentId, () => {
				expect(translation.componentId).to.equal(componentId);
			});
		});
	});

	describe('GIVEN: a TextField Content with an hint', () => {
		var numberOfLines = 1,
            hint = 'hint',
            inputType = undefined,
            componentId = undefined,
            value = undefined,
            source = <AppstudioTextFieldContentDto>AppstudioModelFactory.create('TextFieldContent', {
            hint: hint
		});

		describe('WHEN:  translated', () => {
			var translator = new TextFieldContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the value property should be translated as ' + value, () => {
				expect(translation.value).to.equal(value);
			});
			it ('THEN: the numberOfLines property should be translated as ' + numberOfLines, () => {
				expect(translation.numberOfLines).to.equal(numberOfLines);
			});
            it ('THEN: the hint property should be translated as ' + hint, () => {
				expect(translation.hint).to.equal(hint);
			});
			it ('THEN: the inputType property should be translated as ' + inputType, () => {
				expect(translation.inputType).to.equal(inputType);
			});
			it ('THEN: the componentId property should be translated as ' + componentId, () => {
				expect(translation.componentId).to.equal(componentId);
			});
		});
	});

	describe('GIVEN: a TextField Content with an componentId', () => {
		var numberOfLines = 1,
            hint = undefined,
            inputType = undefined,
            componentId = 'id',
            value = undefined,
            source = <AppstudioTextFieldContentDto>AppstudioModelFactory.create('TextFieldContent', {
            componentId: componentId
		});

		describe('WHEN:  translated', () => {
			var translator = new TextFieldContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the value property should be translated as ' + value, () => {
				expect(translation.value).to.equal(value);
			});
			it ('THEN: the numberOfLines property should be translated as ' + numberOfLines, () => {
				expect(translation.numberOfLines).to.equal(numberOfLines);
			});
            it ('THEN: the hint property should be translated as ' + hint, () => {
				expect(translation.hint).to.equal(hint);
			});
			it ('THEN: the inputType property should be translated as ' + inputType, () => {
				expect(translation.inputType).to.equal(inputType);
			});
			it ('THEN: the componentId property should be translated as ' + componentId, () => {
				expect(translation.componentId).to.equal(componentId);
			});
		});
	});
});
