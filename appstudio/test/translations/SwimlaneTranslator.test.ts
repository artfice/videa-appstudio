import chaiLib = require('chai');
const expect = chaiLib.expect;

import {BaseCollection as AppstudioBaseCollectionDto} from '../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';
import {
	ComponentControls as AppstudioComponentControlsDto,
	ComponentControls
} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as AppstudioBaseEventDto, BaseEvent} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as AppstudioBaseActionDto, BaseAction} from '../../dto/common/listener/action/BaseAction';

import {SwimlaneTranslator as SwimlaneTranslator} from '../../translation/dataview/tileview/SwimlaneTranslator';
import {GridViewTranslator as GridViewTranslator} from '../../translation/dataview/tileview/GridViewTranslator';
import {TileViewTranslator as TileViewTranslator} from '../../translation/dataview/TileViewTranslator';
import {BaseCollectionTranslator as BaseCollectionTranslator} from '../../translation/dataview/collection/BaseCollectionTranslator';
import {BaseFieldStyleTranslator as BaseFieldStyleTranslator} from '../../translation/field/style/BaseFieldStyleTranslator';
import {FieldContentTranslator as FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentTranslator as ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {TileViewStyleTranslator as TileViewStyleTranslator} from '../../translation/dataview/style/TileViewStyleTranslator';
import {SizedComponentStyleTranslator as SizedComponentStyleTranslator} from '../../translation/component/SizedComponentStyleTranslator';
import {DataViewHeadingTranslator as DataViewHeadingTranslator} from '../../translation/dataview/heading/DataViewHeadingTranslator';
import {DataViewHeadingStyleTranslator as DataViewHeadingStyleTranslator} from '../../translation/dataview/style/DataViewHeadingStyleTranslator';
import {LabelTranslator as LabelTranslator} from '../../translation/field/LabelTranslator';
import {LabelStyleTranslator as LabelStyleTranslator} from '../../translation/field/style/LabelStyleTranslator';
import {LabelContentTranslator as LabelContentTranslator} from '../../translation/field/content/LabelContentTranslator';
import {DataViewTileTranslator as DataViewTileTranslator} from '../../translation/dataview/tile/DataViewTileTranslator';
import {ImageTranslator as ImageTranslator} from '../../translation/field/ImageTranslator';
import {ImageStyleTranslator as ImageStyleTranslator} from '../../translation/field/style/ImageStyleTranslator';
import {GridViewStyleTranslator as GridViewStyleTranslator} from '../../translation/dataview/style/GridViewStyleTranslator';
import {ListenerTranslator as ListenerTranslator} from '../../translation/ListenerTranslator';
import {ImageContentTranslator as ImageContentTranslator} from '../../translation/field/content/ImageContentTranslator';

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {ClientAppModelFactory as ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {GridView as AppstudioGridViewDto} from '../../dto/app/configuration/uiconfig/screen/dataview/gridview/GridView';
import {GridViewStyle as AppstudioGridViewStyleDto} from '../../dto/app/configuration/uiconfig/screen/dataview/gridview/GridViewStyle';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';

import {Label as AppstudioLabelDto} from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';

import {Image as AppstudioImageDto} from '../../dto/app/configuration/uiconfig/screen/component/field/image/Image';
import {DataViewHeading} from "../../dto/app/configuration/uiconfig/screen/dataview/DataViewHeading";
import {Listener} from "../../dto/common/Listener";
import {DataViewTileSettings} from "../../dto/app/configuration/uiconfig/screen/dataview/tile/DataViewTileSettings";
import {DataViewTile} from "../../dto/app/configuration/uiconfig/screen/dataview/DataViewTile";
import {DataViewTileStyle} from "../../dto/app/configuration/uiconfig/screen/dataview/tile/DataViewTileStyle";

describe('Swimlane Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(BaseFieldStyleTranslator);
	translationService.registerTranslator(FieldContentTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(BaseCollectionTranslator);
	translationService.registerTranslator(TileViewStyleTranslator);
	translationService.registerTranslator(SizedComponentStyleTranslator);
	translationService.registerTranslator(DataViewHeadingTranslator);
	translationService.registerTranslator(DataViewHeadingStyleTranslator);
    translationService.registerTranslator(LabelStyleTranslator);
	translationService.registerTranslator(LabelTranslator);
	translationService.registerTranslator(LabelContentTranslator);
	translationService.registerTranslator(TileViewTranslator);
	translationService.registerTranslator(DataViewTileTranslator);
	translationService.registerTranslator(ImageTranslator);
	translationService.registerTranslator(ImageStyleTranslator);
	translationService.registerTranslator(GridViewStyleTranslator);
	translationService.registerTranslator(ListenerTranslator);
	translationService.registerTranslator(ImageContentTranslator);
	translationService.registerTranslator(GridViewTranslator);

	describe('GIVEN: an empty Swimlane', () => {
		var source = <any>AppstudioModelFactory.create('Swimlane');

		describe('WHEN: translated', () => {
			var translator = new SwimlaneTranslator({}),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
                        console.log(err);
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});

            it ('THEN: the style border property should be translated as undefined', () => {
				expect(translation.style.border).to.equal(undefined);
			});

            it ('THEN: the width property should be translated as matchParent', () => {
				expect(translation.width).to.equal('matchParent');
			});

            it ('THEN: the height property should be translated as wrapContent', () => {
				expect(translation.height).to.equal('wrapContent');
			});

            it ('THEN: the visible property should be translated as true', () => {
				expect(translation.visible).to.equal('true');
			});

            it ('THEN: the items property should have no items', () => {
				expect(translation.items.length).to.equal(2);
			});

            it ('THEN: the layout type property should be translated as Linear', () => {
				expect(translation.layout.type).to.equal('Linear');
			});

            it ('THEN: the layout orientation property should be translated as vertical', () => {
				expect(translation.layout.orientation).to.equal('vertical');
			});

            it ('THEN: the layout rows property should be translated as 1', () => {
				expect(translation.items[1].layout.rows).to.equal(1);
			});

            it ('THEN: the layout columns property should be translated as undefined', () => {
				expect(translation.items[1].layout.columns).to.equal(undefined);
			});

			it ('THEN: the value property should be translated as undefined', () => {
				expect(translation.value).to.equal(undefined);
			});

            it ('THEN: Swimlane width property should be translated as matchParent', () => {
				expect(translation.items[1].width).to.equal('matchParent');
			});

            it ('THEN: Swimlane height property should be translated as wrapContent', () => {
				expect(translation.items[1].height).to.equal('wrapContent');
			});

            it ('THEN: Swimlane dataview collection property should be translated as undefined', () => {
				expect(translation.items[1].collection).to.equal(undefined);
			});

            it ('THEN: Swimlane dataview itemRenderer style property should be translated as null', () => {
				expect(translation.items[1].itemRenderer.style).to.equal(null);
			});

            it ('THEN: Swimlane dataview itemRenderer  visible property should be translated as null', () => {
				expect(translation.items[1].itemRenderer.visible).to.equal(null);
			});

            it ('THEN: Swimlane dataview itemRenderer listener property should be translated as empty list', () => {
				expect(translation.items[1].itemRenderer.listeners.length).to.equal(0);
			});

            it ('THEN: Swimlane type property should be translated as DataView', () => {
				expect(translation.items[1].type).to.equal('DataView');
			});

            it ('THEN: the scroll property should be translated as undefined', () => {
				expect(translation.items[1].scroll).to.equal('horizontal');
			});

            it ('THEN: Swimlane layout type property should be translated as Grid', () => {
				expect(translation.items[1].layout.type).to.equal('Grid');
			});
            it ('THEN: Swimlane layout orientation property should be translated as vertical', () => {
				expect(translation.items[1].layout.orientation).to.equal('vertical');
			});
            it ('THEN: Swimlane itemRenderer type property should be translated as Container', () => {
				expect(translation.items[1].itemRenderer.type).to.equal('Container');
			});
            it ('THEN: Swimlane itemRenderer width property should be translated as matchParent', () => {
				expect(translation.items[1].itemRenderer.width).to.equal('matchParent');
			});
            it ('THEN: Swimlane itemRenderer height property should be translated as wrapContent', () => {
				expect(translation.items[1].itemRenderer.height).to.equal('wrapContent');
			});
            it ('THEN: Swimlane itemRenderer style type property should be translated as null', () => {
				expect(translation.items[1].itemRenderer.style).to.equal(null);
			});
            it ('THEN: Swimlane itemRenderer layout type property should be translated as Linear', () => {
				expect(translation.items[1].itemRenderer.layout.type).to.equal('Linear');
			});
            it ('THEN: Swimlane itemRenderer layout orientation property should be translated as vertical', () => {
				expect(translation.items[1].itemRenderer.layout.orientation).to.equal('vertical');
			});

            it ('THEN: Swimlane itemRenderer  labelContainer layout type property should be translated as Linear since no tileContainer for image set', () => {
				expect(translation.items[1].itemRenderer.items[0].layout.type).to.equal('Linear');
			});

			it ('THEN: Swimlane itemRenderer labelContainer type property should be translated as Container', () => {
				expect(translation.items[1].itemRenderer.items[0].type).to.equal('Container');
			});
            it ('THEN: Swimlane itemRenderer  labelContainer width property should be translated as matchParent', () => {
				expect(translation.items[1].itemRenderer.items[0].width).to.equal('matchParent');
			});
            it ('THEN: Swimlane itemRenderer  labelContainer height property should be translated as wrapContent', () => {
				expect(translation.items[1].itemRenderer.items[0].height).to.equal('wrapContent');
			});
            it ('THEN: Swimlane itemRenderer  labelContainer style type property should be translated as ContainerStyle', () => {
				expect(translation.items[1].itemRenderer.items[0].style.type).to.equal('Style');
			});


            it ('THEN: Swimlane itemRenderer  labelContainer layout orientation property should be translated as vertical', () => {
				expect(translation.items[1].itemRenderer.items[0].layout.orientation).to.equal('vertical');
			});

            it ('THEN: headerContainer type property should be translated as Container', () => {
				expect(translation.items[0].type).to.equal('Container');
			});

            it ('THEN: headerContainer Style type property should be translated as ContainerStyle', () => {
				expect(translation.items[0].style.type).to.equal('ContainerStyle');
			});

			it ('THEN: headerContainer layout type property should be translated as Relative', () => {
				expect(translation.items[0].layout.type).to.equal('Relative');
			});
			it ('THEN: headerContainer visible property should be translated as undefined', () => {
				expect(translation.items[0].visible).to.equal(undefined);
			});
		});
	});

    describe('GIVEN: a Swimlane with style', () => {
		var source = <any> AppstudioModelFactory.create('Swimlane'),
			row = 1,
			column = undefined,
			type = 'Grid',
			orientation = 'vertical',
			style = <AppstudioGridViewStyleDto>AppstudioModelFactory.create('SizedComponentStyle');
			source.style = style;

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

            it ('THEN: Swimlane layout type property should be translated as ' + type, () => {
				expect(translation.items[1].layout.type).to.equal(type);
			});

            it ('THEN: Swimlane layout rows property should be translated as ' + row, () => {
				expect(translation.items[1].layout.rows).to.equal(row);
			});

            it ('THEN: Swimlane layout columns property should be translated as ' + column, () => {
				expect(translation.items[1].layout.columns).to.equal(column);
			});
            it ('THEN: Swimlane layout orientation property should be translated as ' + orientation, () => {
				expect(translation.items[1].layout.orientation).to.equal(orientation);
			});
		});
	});

describe('GIVEN: a Swimlane with controls', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			visible = '1',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = <any>ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}),
			controls = <any>AppstudioModelFactory.create('ComponentControls', {
				visible: visible
			});

			controls.listeners.push(listener);
			source.controls = controls;

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});

			it ('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.listeners[0].action.type).to.equal(signout);
			});
		});
	});

	describe('GIVEN: a Swimlane with heading style', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			margin = '1 1 1 1',
			padding = '2 2 2 2',
			radius = 1,
			thickness = 1,
			color = '#AAAAAA',
			width = 10,
			height = 20,
			backgroundColor = '#AAAAAA',
			style = AppstudioModelFactory.create('DataViewHeadingStyle', {
				margin: AppstudioModelFactory.create('BoxParams', {
					top: 1,
					right: 1,
					bottom: 1,
					left: 1
				}),
				padding: AppstudioModelFactory.create('BoxParams', {
					top: 2,
					right: 2,
					bottom: 2,
					left: 2
				}),
                border: {
                    radius: radius,
                    thickness: thickness,
                    color: {
                        value: color
                    }
                },
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				}),
				backgroundColor: {
					value: backgroundColor
				}
			}),
			settings = AppstudioModelFactory.create('DataViewHeadingSettings', {
				style: style
			}),
			heading = AppstudioModelFactory.create('DataViewHeading', {
				settings: settings
			});

			source.heading = heading;

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

		it ('THEN: the header width property should be translated as ' + width, () => {
				expect(translation.items[0].width).to.equal(width);
			});

            it ('THEN: the header height property should be translated as ' + height, () => {
				expect(translation.items[0].height).to.equal(height);
			});

			it ('THEN: the header style border radius property should be translated as ' + radius, () => {
				expect(translation.items[0].style.border.radius).to.equal(radius);
			});
			it ('THEN: the header style border thickness property should be translated as ' + thickness, () => {
				expect(translation.items[0].style.border.thickness).to.equal(thickness);
			});
			it ('THEN: the header style border color property should be translated as ' + color, () => {
				expect(translation.items[0].style.border.color.value).to.equal(color);
			});

			it ('THEN: the header style margin property should be translated as ' + margin, () => {
				expect(translation.items[0].style.margin).to.equal(margin);
			});

			it ('THEN: the header style padding property should be translated as ' + padding, () => {
				expect(translation.items[0].style.padding).to.equal(padding);
			});
		});
	});

describe('GIVEN: a Swimlane with heading controls', () => {
		var source = <AppstudioGridViewDto <AppstudioBaseCollectionDto,
                                      AppstudioGridViewStyleDto,
                                      AppstudioComponentControlsDto<AppstudioBaseEventDto, AppstudioBaseActionDto>>>
                                            AppstudioModelFactory.create('Swimlane'),
			visible = '1',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}) as Listener<BaseEvent, BaseAction>,
			controls = AppstudioModelFactory.create('ComponentControls', {
				visible: visible
			}),
			settings = AppstudioModelFactory.create('DataViewHeadingSettings', {
				controls: controls
			}),
			heading = AppstudioModelFactory.create('DataViewHeading', {
				settings: settings
			}) as DataViewHeading;

			heading.settings.controls.listeners.push(listener);
			source.heading = heading;

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.items[0].visible).to.equal(visible);
			});

			it ('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.items[0].listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.items[0].listeners[0].action.type).to.equal(signout);
			});
		});
	});

describe('GIVEN: a Swimlane with heading label', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			width = 1,
			height = 1,
			gravity = 'Left',
			value = 'test',
			visible = 'not true',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}) as Listener<BaseEvent, BaseAction>,
			labelSource = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
				style: AppstudioModelFactory.create('LabelStyle', {
					width: AppstudioModelFactory.create('Width', {
						value: AppstudioModelFactory.create('CustomWidth', {
							value: width
						})
					}),
					height: AppstudioModelFactory.create('Height', {
						value: AppstudioModelFactory.create('CustomHeight', {
							value: height
						})
					}),
					gravity: gravity
				}),
				content: AppstudioModelFactory.create('LabelContent', {
					value: value
				}),
				controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
					visible: 'not true'
				})
        	}),
			settings = AppstudioModelFactory.create('DataViewHeadingSettings'),
			heading = AppstudioModelFactory.create('DataViewHeading', {
				settings: settings
			});

			labelSource.controls.listeners.push(listener);
			source.heading = heading;
			source.heading.text.push(labelSource);

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

			it ('THEN: the header label\'s value property should be translated as ' + value, () => {
				expect(translation.items[0].items[0].value).to.equal(value);
			});
			it ('THEN: the header label\'s width property should be translated as ' + width, () => {
				expect(translation.items[0].items[0].width).to.equal(width);
			});
			it ('THEN: the header label\'s height property should be translated as ' + height, () => {
				expect(translation.items[0].items[0].height).to.equal(height);
			});
			it ('THEN: the header label\'s gravity property should be translated as ' + gravity, () => {
				expect(translation.items[0].items[0].gravity).to.equal(gravity);
			});
			it ('THEN: the header label\'s visible property should be translated as ' + visible, () => {
				expect(translation.items[0].items[0].visible).to.equal(visible);
			});

			it ('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.items[0].items[0].listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.items[0].items[0].listeners[0].action.type).to.equal(signout);
			});
		});
	});


	describe('GIVEN: a Swimlane with tile style', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			margin = '1 1 1 1',
			padding = '2 2 2 2',
			radius = 1,
			thickness = 1,
			color = '#AAAAAA',
			width = 10,
			height = 20,
			backgroundColor = '#AAAAAA',
			style = AppstudioModelFactory.create('TileStyle', {
				margin: AppstudioModelFactory.create('BoxParams', {
					top: 1,
					right: 1,
					bottom: 1,
					left: 1
				}),
				padding: AppstudioModelFactory.create('BoxParams', {
					top: 2,
					right: 2,
					bottom: 2,
					left: 2
				}),
                border: {
                    radius: radius,
                    thickness: thickness,
                    color: {
                        value: color
                    }
                },
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				}),
				backgroundColor: {
					value: backgroundColor
				}
			}) as DataViewTileStyle,
			settings = AppstudioModelFactory.create('DataViewTileSettings') as DataViewTileSettings,
			tile = AppstudioModelFactory.create('DataViewTile', {
				settings: settings
			}) as DataViewTile;

			tile.settings.style = style;
			source.tile = tile;

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

		it ('THEN: the tile width property should be translated as ' + width, () => {
				expect(translation.items[1].itemRenderer.width).to.equal(width);
			});

            it ('THEN: the tile height property should be translated as ' + height, () => {
				expect(translation.items[1].itemRenderer.height).to.equal(height);
			});

			it ('THEN: the tile style border radius property should be translated as ' + radius, () => {
				expect(translation.items[1].itemRenderer.style.border.radius).to.equal(radius);
			});
			it ('THEN: the tile style border thickness property should be translated as ' + thickness, () => {
				expect(translation.items[1].itemRenderer.style.border.thickness).to.equal(thickness);
			});
			it ('THEN: the tile style border color property should be translated as ' + color, () => {
				expect(translation.items[1].itemRenderer.style.border.color.value).to.equal(color);
			});

			it ('THEN: the tile style margin property should be translated as ' + margin, () => {
				expect(translation.items[1].itemRenderer.style.margin).to.equal(margin);
			});

			it ('THEN: the tile style padding property should be translated as ' + padding, () => {
				expect(translation.items[1].itemRenderer.style.padding).to.equal(padding);
			});
		});
	});

describe('GIVEN: a Swimlane with tile controls', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			visible = '1',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}) as Listener<BaseEvent, BaseAction>,
			controls = AppstudioModelFactory.create('ComponentControls', {
				visible: visible
			}) as ComponentControls<BaseEvent, BaseAction>,
			settings = AppstudioModelFactory.create('DataViewTileSettings') as DataViewTileSettings,
			tile = AppstudioModelFactory.create('DataViewTile', {
				settings: settings
			}) as DataViewTile;

			tile.settings.controls = controls;
			tile.settings.controls.listeners.push(listener);
			source.tile = tile;

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.items[1].itemRenderer.visible).to.equal(visible);
			});

			it ('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.items[1].itemRenderer.listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.items[1].itemRenderer.listeners[0].action.type).to.equal(signout);
			});
		});
	});

describe('GIVEN: a Swimlane with tile label', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			width = 1,
			height = 1,
			gravity = 'Left',
			value = 'test',
			visible = 'not true',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}) as Listener<BaseEvent, BaseAction>,
			labelSource = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
				style: AppstudioModelFactory.create('LabelStyle', {
					width: AppstudioModelFactory.create('Width', {
						value: AppstudioModelFactory.create('CustomWidth', {
							value: width
						})
					}),
					height: AppstudioModelFactory.create('Height', {
						value: AppstudioModelFactory.create('CustomHeight', {
							value: height
						})
					}),
					gravity: gravity
				}),
				content: AppstudioModelFactory.create('LabelContent', {
					value: value
				}),
				controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
					visible: 'not true'
				})
        	}),
			settings = AppstudioModelFactory.create('DataViewTileSettings'),
			tile = AppstudioModelFactory.create('DataViewTile', {
				settings: settings
			});

			labelSource.controls.listeners.push(listener);
			source.tile = tile;
			source.tile.text.push(labelSource);

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

			it ('THEN: the tile label\'s value property should be translated as ' + value, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].value).to.equal(value);
			});
			it ('THEN: the tile label\'s width property should be translated as ' + width, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].width).to.equal(width);
			});
			it ('THEN: the tile label\'s height property should be translated as ' + height, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].height).to.equal(height);
			});
			it ('THEN: the tile label\'s gravity property should be translated as ' + gravity, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].gravity).to.equal(gravity);
			});
			it ('THEN: the tile label\'s visible property should be translated as ' + visible, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].visible).to.equal(visible);
			});

			it ('THEN: the tile label\'slistener event type property should be translated as ' + longSelect, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the tile label\'s listener action type property should be translated as ' + signout, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].listeners[0].action.type).to.equal(signout);
			});
		});
	});

describe('GIVEN: a Swimlane with tile image', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			aspectRatio = '1:1',
			scale = 'aspectFit',
			gravity = 'Left',
			width = 1,
			height = 1,
			value = 'test',
			visible = 'not true',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}) as Listener<BaseEvent, BaseAction>,
			imageSource = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
				style: AppstudioModelFactory.create('ImageStyle', {
					aspectRatio: aspectRatio,
					scale: scale,
					gravity: gravity,
					width: AppstudioModelFactory.create('Width', {
						value: AppstudioModelFactory.create('CustomWidth', {
							value: width
						})
					}),
					height: AppstudioModelFactory.create('Height', {
						value: AppstudioModelFactory.create('CustomHeight', {
							value: height
						})
					})
				}),
				content: AppstudioModelFactory.create('ImageContent', {
					value: value
				}),
				controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
					visible: visible
				})
			}),
			settings = AppstudioModelFactory.create('DataViewTileSettings'),
			tile = AppstudioModelFactory.create('DataViewTile', {
				settings: settings
			});

			imageSource.controls.listeners.push(listener);
			source.tile = tile;
			source.tile.subComponent.push(imageSource);

		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

			it ('THEN: the tile image\'s value property should be translated as ' + value, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].value).to.equal(value);
			});
			it ('THEN: the tile image\'s width property should be translated as ' + width, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].width).to.equal(width);
			});
			it ('THEN: the tile image\'s height property should be translated as ' + height, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].height).to.equal(height);
			});
			it ('THEN: the tile image\'s gravity property should be translated as ' + gravity, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].gravity).to.equal(gravity);
			});
			it ('THEN: the tile image\'s visible property should be translated as ' + visible, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].visible).to.equal(visible);
			});

			it ('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].listeners[0].action.type).to.equal(signout);
			});
		});
	});
describe('GIVEN: a Swimlane with tile image and label', () => {
		var source = <any>AppstudioModelFactory.create('GridView'),
			aspectRatio = '1:1',
			scale = 'aspectFit',
			gravity = 'Left',
			width = 1,
			height = 1,
			value = 'test',
			visible = 'not true',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}) as Listener<BaseEvent, BaseAction>,
			imageSource = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
				style: AppstudioModelFactory.create('ImageStyle', {
					aspectRatio: aspectRatio,
					scale: scale,
					gravity: gravity,
					width: AppstudioModelFactory.create('Width', {
						value: AppstudioModelFactory.create('CustomWidth', {
							value: width
						})
					}),
					height: AppstudioModelFactory.create('Height', {
						value: AppstudioModelFactory.create('CustomHeight', {
							value: height
						})
					})
				}),
				content: AppstudioModelFactory.create('ImageContent', {
					value: value
				}),
				controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
					visible: visible
				})
			}),
			labelSource = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
				style: AppstudioModelFactory.create('LabelStyle', {
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				}),
					gravity: gravity
				}),
				content: AppstudioModelFactory.create('LabelContent', {
					value: value
				}),
				controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
					visible: 'not true'
				})
        	}),
			settings = AppstudioModelFactory.create('DataViewTileSettings'),
			tile = AppstudioModelFactory.create('DataViewTile', {
				settings: settings
			});

			imageSource.controls.listeners.push(listener);
			labelSource.controls.listeners.push(listener);
			source.tile = tile;
			source.tile.subComponent.push(imageSource);
			source.tile.text.push(labelSource);
		describe('WHEN: translated', () => {
				var translator = new SwimlaneTranslator({}),
					translation;

				before((done) => {
					translator.translate('videa', translationService, source).then(
						(t) => {
							translation = t;
							done();
						},
						(err) => {
							console.log(err);
							done();
						}
					);
				});

			it ('THEN: the tile image\'s value property should be translated as ' + value, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].value).to.equal(value);
			});
			it ('THEN: the tile image\'s width property should be translated as ' + width, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].width).to.equal(width);
			});
			it ('THEN: the tile image\'s height property should be translated as ' + height, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].height).to.equal(height);
			});
			it ('THEN: the tile image\'s gravity property should be translated as ' + gravity, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].gravity).to.equal(gravity);
			});
			it ('THEN: the tile image\'s visible property should be translated as ' + visible, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].visible).to.equal(visible);
			});

			it ('THEN: the tile image listener event type property should be translated as ' + longSelect, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the tile image listener action type property should be translated as ' + signout, () => {
				expect(translation.items[1].itemRenderer.items[0].items[0].listeners[0].action.type).to.equal(signout);
			});
			it ('THEN: the tile label\'s value property should be translated as ' + value, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].value).to.equal(value);
			});
			it ('THEN: the tile label\'s width property should be translated as ' + width, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].width).to.equal(width);
			});
			it ('THEN: the tile label\'s height property should be translated as ' + height, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].height).to.equal(height);
			});
			it ('THEN: the tile label\'s gravity property should be translated as ' + gravity, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].gravity).to.equal(gravity);
			});
			it ('THEN: the tile label\'s visible property should be translated as ' + visible, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].visible).to.equal(visible);
			});

			it ('THEN: the tile label\'slistener event type property should be translated as ' + longSelect, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the tile label\'s listener action type property should be translated as ' + signout, () => {
				expect(translation.items[1].itemRenderer.items[1].items[0].listeners[0].action.type).to.equal(signout);
			});
		});
	});
});