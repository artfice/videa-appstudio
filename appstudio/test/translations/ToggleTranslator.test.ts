import chaiLib = require('chai');
const expect = chaiLib.expect;

import {BaseFieldTranslator as BaseFieldTranslator} from '../../translation/field/BaseFieldTranslator';
import {ToggleTranslator as ToggleTranslator} from '../../translation/field/ToggleTranslator';
import {ToggleStyleTranslator as ToggleStyleTranslator} from '../../translation/field/style/ToggleStyleTranslator';
import {FieldContentTranslator as FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentTranslator as ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {ListenerTranslator as ListenersTranslator} from '../../translation/ListenerTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {Toggle as AppstudioToggleDto} from '../../dto/app/configuration/uiconfig/screen/component/field/toggle/Toggle';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';

describe('Toggle Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(ToggleStyleTranslator);
	translationService.registerTranslator(FieldContentTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(BaseFieldTranslator);
	translationService.registerTranslator(ListenersTranslator);
	translationService.registerTranslator(ToggleTranslator);

	describe('GIVEN: an Toggle', () => {
		var source = <AppstudioToggleDto>AppstudioModelFactory.create('Toggle', {
            style: AppstudioModelFactory.create('ToggleStyle'),
            content: AppstudioModelFactory.create('FieldContent'),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls')
        });

		describe('WHEN: translated', () => {
			var translator = new ToggleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as undefined', () => {
				expect(translation.value).to.equal(undefined);
			});
			it ('THEN: the width property should be translated as undefined', () => {
				expect(translation.width).to.equal(undefined);
			});
			it ('THEN: the height property should be translated as undefined', () => {
				expect(translation.height).to.equal(undefined);
			});
			it ('THEN: the gravity property should be translated as undefined', () => {
				expect(translation.gravity).to.equal(undefined);
			});
			it ('THEN: the visible property should be translated as true', () => {
				expect(translation.visible).to.equal('true');
			});
			it ('THEN: the listeners property should be translated as []', () => {
				expect(translation.listeners.length).to.equal(0);
			});
		});
	});

	describe('GIVEN: an Toggle with style', () => {
		var activatedColor = '#EEEEEE',
            deactivatedColor = '#AAAAAA',
            source = <AppstudioToggleDto>AppstudioModelFactory.create('Toggle', {
            style: AppstudioModelFactory.create('ToggleStyle', {
                activatedColor: AppstudioModelFactory.create('Color', {value: activatedColor}),
                deactivatedColor: AppstudioModelFactory.create('Color', {value: deactivatedColor})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

		describe('WHEN: translated', () => {
			var translator = new ToggleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as test', () => {
				expect(translation.value).to.equal('test');
			});
			it ('THEN: the visible property should be translated as not true', () => {
				expect(translation.visible).to.equal('not true');
			});
            it ('THEN: the activatedColor property should be translated as ' + activatedColor, () => {
				expect(translation.style.activatedColor.value).to.equal(activatedColor);
			});
			it ('THEN: the deactivatedColor property should be translated as ' + deactivatedColor, () => {
				expect(translation.style.deactivatedColor.value).to.equal(deactivatedColor);
			});
		});
	});
});
