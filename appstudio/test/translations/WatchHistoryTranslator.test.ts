import chaiLib = require('chai');
const expect = chaiLib.expect;

import {WatchHistoryTranslator as WatchHistoryTranslator} from '../../translation/watchhistory/WatchHistoryTranslator';
import {LocalProviderTranslator as LocalProviderTranslator} from '../../translation/watchhistory/LocalProviderTranslator';
import {MpxProviderTranslator as MpxProviderTranslator} from '../../translation/watchhistory/MpxProviderTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {BaseProvider as BaseProvider} from '../../dto/app/configuration/watchhistory/BaseProvider';
import {WatchHistory as AppstudioWatchHistoryDto} from '../../dto/app/configuration/WatchHistory';

describe('WatchHistory Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty WatchHistory', () => {
		var source = <AppstudioWatchHistoryDto<BaseProvider>>AppstudioModelFactory.create('WatchHistory');

		describe('WHEN: translated', () => {
			var translator = new WatchHistoryTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should null', () => {
				expect(translation).to.equal(null);
			});
		});
	});

	describe('GIVEN: a WatchHistory with a none provider', () => {
		var source = <AppstudioWatchHistoryDto<BaseProvider>>AppstudioModelFactory.create('WatchHistory'),
            noWatchHistoryProvider = AppstudioModelFactory.create('NoWatchHistoryProvider');

        source.provider = <any>noWatchHistoryProvider;

		describe('WHEN: translated', () => {
			var translator = new WatchHistoryTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should null', () => {
				expect(translation).to.equal(null);
			});
		});
	});

	describe('GIVEN: a WatchHistory with a local provider', () => {
		var source = <AppstudioWatchHistoryDto<BaseProvider>>AppstudioModelFactory.create('WatchHistory'),
            completionThreshold = 0.95,
            frequency = 10000,
            localWatchHistoryProvider = AppstudioModelFactory.create('LocalWatchHistoryProvider',{
                completionThreshold : completionThreshold,
                frequency : frequency,
                onPauseEvent : false,
                onResumeEvent : false,
                onSeekEvent : false,
                onStopEvent : false,
                onPlayEvent : false
            });

        source.provider = <any>localWatchHistoryProvider;

		describe('WHEN: translated', () => {
			var translator = new WatchHistoryTranslator(),
				translation;
            translationService.registerTranslator(LocalProviderTranslator);

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the completionThreshold property should translate to ' + completionThreshold, () => {
				expect(translation.completionThreshold).to.equal(completionThreshold);
			});
			it ('THEN: the frequency property should translate to ' + frequency, () => {
				expect(translation.frequency).to.equal(frequency);
			});
			it ('THEN: the events property should have no events', () => {
				expect(translation.events.length).to.equal(0);
			});
		});
	});

	describe('GIVEN: a WatchHistory with localprovider with a events', () => {
		var source = <AppstudioWatchHistoryDto<BaseProvider>>AppstudioModelFactory.create('WatchHistory'),
            completionThreshold = 0.95,
            frequency = 10000,
            localWatchHistoryProvider = AppstudioModelFactory.create('LocalWatchHistoryProvider',{
                completionThreshold : completionThreshold,
                frequency : frequency,
                onPauseEvent : true,
                onResumeEvent : true,
                onSeekEvent : true,
                onStopEvent : true,
                onPlayEvent : true
            });

        source.provider = <any>localWatchHistoryProvider;

		describe('WHEN: translated', () => {
			var translator = new WatchHistoryTranslator(),
				translation;
            translationService.registerTranslator(LocalProviderTranslator);

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the completionThreshold property should translate to ' + completionThreshold, () => {
				expect(translation.completionThreshold).to.equal(completionThreshold);
			});
			it ('THEN: the frequency property should translate to ' + frequency, () => {
				expect(translation.frequency).to.equal(frequency);
			});
			it ('THEN: the events property should have 5 events', () => {
				expect(translation.events.length).to.equal(5);
			});
			it ('THEN: the events property should have onPause', () => {
				expect(translation.events.indexOf('onPause') > -1).to.equal(true);
			});
			it ('THEN: the events property should have onPlay', () => {
				expect(translation.events.indexOf('onPlay') > -1).to.equal(true);
			});
			it ('THEN: the events property should have onResume', () => {
				expect(translation.events.indexOf('onResume') > -1).to.equal(true);
			});
			it ('THEN: the events property should have onStop', () => {
				expect(translation.events.indexOf('onStop') > -1).to.equal(true);
			});
		});
	});

	describe('GIVEN: a WatchHistory with a mpx provider', () => {
		var source = <AppstudioWatchHistoryDto<BaseProvider>>AppstudioModelFactory.create('WatchHistory'),
            completionThreshold = 0.95,
            frequency = 10000,
            refreshInterval = 30000,
            host = 'a',
            localWatchHistoryProvider = AppstudioModelFactory.create('MpxWatchHistoryProvider',{
                completionThreshold : completionThreshold,
                frequency : frequency,
                host: host,
                refreshInterval: refreshInterval,
                onPauseEvent : false,
                onResumeEvent : false,
                onSeekEvent : false,
                onStopEvent : false,
                onPlayEvent : false
            });

        source.provider = <any>localWatchHistoryProvider;

		describe('WHEN: translated', () => {
			var translator = new WatchHistoryTranslator(),
				translation;
            translationService.registerTranslator(LocalProviderTranslator);
            translationService.registerTranslator(MpxProviderTranslator);

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the completionThreshold property should translate to ' + completionThreshold, () => {
				expect(translation.completionThreshold).to.equal(completionThreshold);
			});
			it ('THEN: the frequency property should translate to ' + frequency, () => {
				expect(translation.frequency).to.equal(frequency);
			});
			it ('THEN: the host property should translate to ' + host, () => {
				expect(translation.host).to.equal(host);
			});
			it ('THEN: the refreshInterval property should translate to ' + refreshInterval, () => {
				expect(translation.refreshInterval).to.equal(refreshInterval);
			});
			it ('THEN: the events property should have no events', () => {
				expect(translation.events.length).to.equal(0);
			});
		});
	});

	describe('GIVEN: a WatchHistory with a events', () => {
		var source = <AppstudioWatchHistoryDto<BaseProvider>>AppstudioModelFactory.create('WatchHistory'),
            completionThreshold = 0.95,
            frequency = 10000,
            refreshInterval = 30000,
            host = 'a',
            localWatchHistoryProvider = AppstudioModelFactory.create('MpxWatchHistoryProvider',{
                completionThreshold : completionThreshold,
                host: host,
                refreshInterval: refreshInterval,
                frequency : frequency,
                onPauseEvent : true,
                onResumeEvent : true,
                onSeekEvent : true,
                onStopEvent : true,
                onPlayEvent : true
            });

        source.provider = <any>localWatchHistoryProvider;

		describe('WHEN: translated', () => {
			var translator = new WatchHistoryTranslator(),
				translation;
            translationService.registerTranslator(LocalProviderTranslator);
            translationService.registerTranslator(MpxProviderTranslator);

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the completionThreshold property should translate to ' + completionThreshold, () => {
				expect(translation.completionThreshold).to.equal(completionThreshold);
			});
			it ('THEN: the frequency property should translate to ' + frequency, () => {
				expect(translation.frequency).to.equal(frequency);
			});

			it ('THEN: the host property should translate to ' + host, () => {
				expect(translation.host).to.equal(host);
			});
			it ('THEN: the refreshInterval property should translate to ' + refreshInterval, () => {
				expect(translation.refreshInterval).to.equal(refreshInterval);
			});
			it ('THEN: the events property should have 5 events', () => {
				expect(translation.events.length).to.equal(5);
			});
			it ('THEN: the events property should have onPause', () => {
				expect(translation.events.indexOf('onPause') > -1).to.equal(true);
			});
			it ('THEN: the events property should have onPlay', () => {
				expect(translation.events.indexOf('onPlay') > -1).to.equal(true);
			});
			it ('THEN: the events property should have onResume', () => {
				expect(translation.events.indexOf('onResume') > -1).to.equal(true);
			});
			it ('THEN: the events property should have onStop', () => {
				expect(translation.events.indexOf('onStop') > -1).to.equal(true);
			});
		});
	});
});
