import chaiLib = require('chai');
const expect = chaiLib.expect;

import {BaseFieldStyleTranslator} from '../../translation/field/style/BaseFieldStyleTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {BaseFieldStyle as AppstudioBaseFieldStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/field/BaseFieldStyle';

describe('BaseFieldStyle Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty BaseField Style', () => {
		var source = <AppstudioBaseFieldStyleDto>AppstudioModelFactory.create('BaseFieldStyle');

		describe('WHEN: translated', () => {
			var translator = new BaseFieldStyleTranslator(),
				translation,
				gravity = undefined,
				width = undefined,
				height = undefined;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be an empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the gravity property should be ' + gravity, () => {
				expect(translation.gravity).to.equal(gravity);
			});
			it ('THEN: the width property should be ' + width, () => {
				expect(translation.width).to.equal(width);
			});
			it ('THEN: the height property should be ' + height, () => {
				expect(translation.height).to.equal(height);
			});
		});
	});

	describe('GIVEN: a Field Style with gravity, width, height', () => {
		var gravity = 'Left',
			width = 12,
			height = 23,
			source = <AppstudioBaseFieldStyleDto>AppstudioModelFactory.create('BaseFieldStyle', {
            gravity: gravity,
            
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				})
		});

		describe('WHEN: translated', () => {
			var translator = new BaseFieldStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the gravity property should translate to ' + gravity, () => {
				expect(translation.gravity).to.equal(gravity);
			});			
			
			it ('THEN: the width property should translate to ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			
			it ('THEN: the height property should translate to ' + height, () => {
				expect(translation.height).to.equal(height);
			});				
		});
	});	
});