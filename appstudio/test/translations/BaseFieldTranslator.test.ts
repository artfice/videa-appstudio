import chaiLib = require('chai');
const expect = chaiLib.expect;

import {BaseFieldTranslator} from '../../translation/field/BaseFieldTranslator';
import {BaseFieldStyleTranslator} from '../../translation/field/style/BaseFieldStyleTranslator';
import {FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {BaseField as AppstudioBaseFieldDto} from '../../dto/app/configuration/uiconfig/screen/component/field/BaseField';
import {BaseFieldStyle as AppstudioBaseFieldStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/field/BaseFieldStyle';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';

describe('BaseField Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(BaseFieldStyleTranslator);
	translationService.registerTranslator(FieldContentTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);

	describe('GIVEN: an empty BaseField ', () => {
		var value = undefined,
			width = undefined,
			height = undefined,
			gravity = undefined,
			border = undefined,
			source = <AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>>AppstudioModelFactory.create('BaseField', {
            style: AppstudioModelFactory.create('BaseFieldStyle'),
            content: AppstudioModelFactory.create('FieldContent'),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls')
        });

		describe('WHEN: translated', () => {
			var translator = new BaseFieldTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should translate to ' + value, () => {
				expect(translation.value).to.equal(value);
			});			
			it ('THEN: the width property should translate to ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should translate to ' + height, () => {
				expect(translation.height).to.equal(height);
			});			
			it ('THEN: the gravity property should translate to ' + gravity, () => {
				expect(translation.gravity).to.equal(gravity);
			});			
			it ('THEN: the visible property should be true', () => {
				expect(translation.visible).to.equal('true');
			});			
			it ('THEN: the style property should have type Style', () => {
				expect(translation.style.type).to.equal('Style');
			});			
			it ('THEN: the style border property should translate to ' + border, () => {
				expect(translation.style.border).to.equal(border);
			});						
			it ('THEN: the listeners property should be []', () => {
				expect(translation.listeners.length).to.equal(0);
			});					
		});
	});

	describe('GIVEN: a BaseField with a style', () => {
		var width = 1,
		    height = 1,
			gravity = 'Left',
			value = 'test',
			visible = 'not true',
			source = <AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>>AppstudioModelFactory.create('BaseField', {
            style: AppstudioModelFactory.create('BaseFieldStyle', {
                
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				}),
                gravity: gravity
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: value
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });

		describe('WHEN: translated', () => {
			var translator = new BaseFieldTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the width property should translate to ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should translate to ' + height, () => {
				expect(translation.height).to.equal(height);
			});			
			it ('THEN: the gravity property should translate to ' + gravity, () => {
				expect(translation.gravity).to.equal(gravity);
			});			
		});
	});
	
	describe('GIVEN: a BaseField with a content', () => {
		var value = 'test',
			source = <AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>>AppstudioModelFactory.create('BaseField', {
            content: AppstudioModelFactory.create('FieldContent', {
                value: value
            })
        });

		describe('WHEN: translated', () => {
			var translator = new BaseFieldTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the value property should translate to ' + value , () => {
				expect(translation.value).to.equal(value);
			});			
		});
	});	

	describe('GIVEN: a BaseField with a controls', () => {
		var visible = 'not true',
			source = <AppstudioBaseFieldDto<AppstudioBaseFieldStyleDto>>AppstudioModelFactory.create('BaseField', {
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });

		describe('WHEN: translated', () => {
			var translator = new BaseFieldTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the visible property should translate to ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});			
		});
	});	
});