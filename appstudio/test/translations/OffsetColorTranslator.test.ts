import chaiLib = require('chai');
const expect = chaiLib.expect;

import { OffsetColorTranslator as OffsetColorTranslator } from '../../translation/color/OffsetColorTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import OffsetColorSchema = require('../../schema/common/OffsetColor');
import { OffsetColor as OffsetColorDto } from '../../dto/common/OffsetColor';

describe('OffsetColor Translation Tests', () => {
    let translationService = new TranslationService();
    translationService.registerTranslator(OffsetColorTranslator);

    describe('GIVEN: a OffsetColor with a value and offset', () => {
        let value = '0xFF00FF00';
        let offset = '0.1';

        let source = <OffsetColorDto>ClientAppModelFactory.create(OffsetColorSchema.ID, {
            value: value,
            offset: offset
        });

        describe('WHEN: translated', () => {
            let translator = new OffsetColorTranslator();

            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the value property should translate to ' + value, () => {
                expect(translation.value).to.equal(value);
            });
            it('THEN: the offset property should translate to ' + offset, () => {
                expect(translation.offset).to.equal(offset);
            });
        });
    });
});