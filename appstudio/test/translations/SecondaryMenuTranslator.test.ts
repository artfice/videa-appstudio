/**
 * Created by bardiakhosravi on 2016-03-09.
 */
///<reference path="../../../typings/index.d.ts"/>

import * as chai from 'chai';
chai.use(require('chai-shallow-deep-equal'));
const expect = chai.expect;

import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';
import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { SecondaryMenuTranslator } from '../../translation/navigation/SecondaryMenuTranslator';
import { ListenerTranslator } from '../../translation/ListenerTranslator';
import { ImageTranslator } from '../../translation/field/ImageTranslator';
import { ColoredComponentStyleTranslator } from '../../translation/component/ColoredComponentStyleTranslator';
import { ImageStyleTranslator } from '../../translation/field/style/ImageStyleTranslator';
import { FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';
import { ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ComponentStyleTranslator } from '../../translation/component/ComponentStyleTranslator';
import { ComponentTranslator } from '../../translation/component/ComponentTranslator';
import { BaseFieldTranslator } from '../../translation/field/BaseFieldTranslator';
import { LabelTranslator } from '../../translation/field/LabelTranslator';
import { LabelStyleTranslator } from '../../translation/field/style/LabelStyleTranslator';
import { LabelContentTranslator } from '../../translation/field/content/LabelContentTranslator';
import { SizedComponentStyleTranslator } from '../../translation/component/SizedComponentStyleTranslator';
import { SecondaryMenuStyleTranslator } from '../../translation/navigation/style/SecondaryMenuStyleTranslator';
import { SecondaryMenuSectionTranslator } from '../../translation/navigation/section/SecondaryMenuSectionTranslator';
import { StandardAspectRatioTranslator } from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import { CustomAspectRatioTranslator } from '../../translation/aspectRatio/CustomAspectRatioTranslator';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';
import { ComplexColorTranslator } from '../../translation/color/ComplexColorTranslator';
import { ColorTranslator } from '../../translation/color/ColorTranslator';

import { Listener as ListenerDto } from '../../dto/common/Listener';
import { Image as AppstudioImageDto } from '../../dto/app/configuration/uiconfig/screen/component/field/image/Image';
import { Label as AppstudioLabelDto } from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';
import { ComponentControls as ComponentControlsDto } from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import { BaseEvent as BaseEventDto } from '../../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../../dto/common/listener/action/BaseAction';
import { SecondaryMenu as SecondaryMenuDto } from '../../dto/app/configuration/uiconfig/navigation/SecondaryMenu';

import SignoutActionSchema = require('../../schema/common/listener/action/SignoutAction');
import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');
import SelectEventSchema = require('../../schema/common/listener/event/SelectEvent');
import ContainerSchema = require('../../schema/clientapp/app/uiconfig/screen/component/container/Container');
import ClientImageSchema = require('../../schema/clientapp/app/uiconfig/screen/field/Image');
import AlignmentSchema = require('../../schema/common/Alignment');

describe('SecondaryMenu Translator tests', () => {
    let translation;

    const translator: SecondaryMenuTranslator = new SecondaryMenuTranslator();

    const service: TranslationService = new TranslationService();
    service.registerTranslator(StandardAspectRatioTranslator);
    service.registerTranslator(CustomAspectRatioTranslator);
    service.registerTranslator(SecondaryMenuSectionTranslator);
    service.registerTranslator(SecondaryMenuStyleTranslator);
    service.registerTranslator(ListenerTranslator);
    service.registerTranslator(ColoredComponentStyleTranslator);
    service.registerTranslator(ImageTranslator);
    service.registerTranslator(LabelTranslator);
    service.registerTranslator(ImageStyleTranslator);
    service.registerTranslator(ComponentStyleTranslator);
    service.registerTranslator(ComponentTranslator);
    service.registerTranslator(FieldContentTranslator);
    service.registerTranslator(ComponentControlsTranslator);
    service.registerTranslator(BaseFieldTranslator);
    service.registerTranslator(LabelStyleTranslator);
    service.registerTranslator(LabelTranslator);
    service.registerTranslator(LabelContentTranslator);
    service.registerTranslator(SizedComponentStyleTranslator);
    service.registerTranslator(OpenSansTranslator);
    service.registerTranslator(ComplexColorTranslator);
    service.registerTranslator(ColorTranslator);

    function validateSectionContainer(sectionContainer) {
        expect(sectionContainer).to.exist;
        expect(sectionContainer._metadata).to.equal(ContainerSchema.ID);
        expect(sectionContainer.width).to.equal('matchParent');
        expect(sectionContainer.height).to.equal('wrapContent');
        expect(sectionContainer.style.padding).to.equal('0 0 0 0');
        expect(sectionContainer.style.margin).to.equal('0 0 0 0');
        expect(sectionContainer.style.backgroundColor.value).to.equal('#00000000');
        expect(sectionContainer.items).to.exist;
    }

    describe('GIVEN: An appstudio secondaryMenu with no properties', () => {
        const source = <SecondaryMenuDto>AppstudioModelFactory.create('SecondaryMenu');

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, source).then(
                    (t) => {
                        translation = t;
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: Translation should be a valid secondaryMenu container', () => {
                validateSectionContainer(translation);
            });
        });

    });

    describe('GIVEN: An appstudio secondarymenu with a section', () => {
        let backgroundColor = {
            _metadata: ColorSchema.ID,
            value: '#AAAAAA'
        };

        const padding = '1 1 1 1';
        const margin = '1 1 1 1';
        const width = 1;
        const height = 1;
        const image = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                    value: '1:1'
                }),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                }),
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                })
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

        const style = AppstudioModelFactory.create('ColoredComponentStyle', {
            margin: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            padding: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            backgroundColor: {
                _metadata: ComplexColorSchema.ID,
                value: backgroundColor
            }
        });

        const section = <any>AppstudioModelFactory.create('SecondaryMenuSection', {
            settings: AppstudioModelFactory.create('SecondaryMenuSectionSettings', {
                style: style
            })

        });

        const source = <SecondaryMenuDto>AppstudioModelFactory.create('SecondaryMenu', {
            sections: []
        });
        section.image = [image];
        source.sections.push(section);

        let translatedSection;

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, source).then(
                    (t) => {
                        translation = t;
                        translatedSection = translation.items[0];
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: the image container has a valid image component', () => {
                expect(translatedSection._metadata).to.equal(ContainerSchema.ID);
                expect(translatedSection.width).to.equal('fillParent');
                expect(translatedSection.height).to.equal('wrapContent');
                expect(translatedSection.style.padding).to.equal(padding);
                expect(translatedSection.style.margin).to.equal(margin);
                expect(translatedSection.style.backgroundColor.value).to.equal(backgroundColor.value);
                expect(translatedSection.style.backgroundColor._metadata).to.equal(backgroundColor._metadata);
                expect(translatedSection.items).to.exist;
                expect(translatedSection).to.exist;
                expect(translatedSection.items[0]._metadata).to.equal(ClientImageSchema.ID);
                expect(translatedSection.items[0].type).to.equal('Image');
                expect(translatedSection.items[0].value).to.equal('test');
                expect(translatedSection.items[0].scale).to.equal('aspectFit');
                expect(translatedSection.items[0].aspectRatio).to.equal(1);
                expect(translatedSection.items[0].height).to.equal(height);
                expect(translatedSection.items[0].width).to.equal(width);
                expect(translatedSection.items[0].gravity).to.equal('Left');
                expect(translatedSection.items[0].style.padding).to.equal(padding);
                expect(translatedSection.items[0].style.margin).to.equal(margin);
            });
        });

    });

    describe('GIVEN: An appstudio secondarymenu with a text', () => {
        const backgroundColor = '#AAAAAA';
        const padding = '1 1 1 1';
        const margin = '1 1 1 1';
        const size = 10;
        const alignment = 'Left';
        const width = 1;
        const height = 1;
        const label = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
            style: AppstudioModelFactory.create('LabelStyle', {
                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                }),
                gravity: 'Left',
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                font: ClientAppModelFactory.create('OpenSans', {
                    weight: 'Regular',
                    color: {
                        value: backgroundColor
                    },
                    size: size,
                    alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment})
                })
            }),
            content: AppstudioModelFactory.create('LabelContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

        const source = <SecondaryMenuDto>AppstudioModelFactory.create('SecondaryMenu', {
            text: [label]
        });

        let translatedLabel;

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, source).then(
                    (t) => {
                        translation = t;
                        translatedLabel = translation.items[0];
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: the label Container has a valid label component', () => {
                expect(translatedLabel).to.exist;
                expect(translatedLabel.type).to.equal('Label');
                expect(translatedLabel.value).to.equal('test');
                expect(translatedLabel.height).to.equal(width);
                expect(translatedLabel.width).to.equal(height);
                expect(translatedLabel.gravity).to.equal('Left');
                expect(translatedLabel.style.padding).to.equal(padding);
                expect(translatedLabel.style.margin).to.equal(margin);
                expect(translatedLabel.style.font.size).to.equal(size);
                expect(translatedLabel.style.font.alignment).to.equal(alignment);
            });
        });
    });

    describe('GIVEN: An appstudio secondarymenu with setting style', () => {
        const backgroundColor = '#AAAAAA';
        const padding = '1 1 1 1';
        const margin = '1 1 1 1';
        const width = 1;
        const height = 1;
        const source = <SecondaryMenuDto>AppstudioModelFactory.create('SecondaryMenu', {
            settings: AppstudioModelFactory.create('SecondaryMenuSettings', {
                style: AppstudioModelFactory.create('SecondaryMenuStyle', {
                    margin: AppstudioModelFactory.create('BoxParams', {
                        top: 1,
                        right: 1,
                        bottom: 1,
                        left: 1
                    }),
                    padding: AppstudioModelFactory.create('BoxParams', {
                        top: 1,
                        right: 1,
                        bottom: 1,
                        left: 1
                    }),
                    width: AppstudioModelFactory.create('Width', {
                        value: AppstudioModelFactory.create('CustomWidth', {
                            value: width
                        })
                    }),
                    height: AppstudioModelFactory.create('Height', {
                        value: AppstudioModelFactory.create('CustomHeight', {
                            value: height
                        })
                    }),
                    backgroundColor: ClientAppModelFactory.create('Color', {
                        value: backgroundColor
                    })
                })
            })
        });

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, source).then(
                    (t) => {
                        translation = t;
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: Translation should be a valid sections style', () => {
                expect(translation._metadata).to.equal(ContainerSchema.ID);
                expect(translation.type).to.equal('Container');
                expect(translation.width).to.equal(width);
                expect(translation.height).to.equal(height);
                expect(translation.style.padding).to.equal(padding);
                expect(translation.style.margin).to.equal(margin);
                expect(translation.style.backgroundColor.value).to.equal(backgroundColor);
                expect(translation.layout.type).to.equal('Linear');
                expect(translation.items.length).to.equal(0);
            });
        });

    });

    describe('GIVEN: An appstudio secondarymenu with setting controls', () => {
        const visible = 'not true';
        const source = <SecondaryMenuDto>AppstudioModelFactory.create('SecondaryMenu', {
            settings: AppstudioModelFactory.create('SecondaryMenuSettings', {

                controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                    visible: visible
                })
            })
        });
        const selectEvent = ClientAppModelFactory.create(SelectEventSchema.ID);
        const select = 'Select';
        const signoutAction = ClientAppModelFactory.create(SignoutActionSchema.ID);
        const signout = 'Signout';
        const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener', {
            event: selectEvent,
            action: signoutAction
        });

        source.settings.controls.listeners.push(listener);

        describe('WHEN:  translated', () => {
            before((done) => {
                translator.translate('videa', service, source).then(
                    (t) => {
                        translation = t;
                        done();
                    }
                ).catch((err) => {
                    done(err);
                });
            });

            it('THEN: visible property should be translated to ' + visible, () => {
                expect(translation.visible).to.equal(visible);
            });
            it('THEN: the listener event type property should be translated as ' + select, () => {
                expect(translation.listeners[0].event.type).to.equal(select);
            });
            it('THEN: the listener action type property should be translated as ' + signout, () => {
                expect(translation.listeners[0].action.type).to.equal(signout);
            });
        });

    });

});
