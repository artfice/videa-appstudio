import chaiLib = require('chai');
const expect = chaiLib.expect;

import {LabelContentTranslator as LabelContentTranslator} from '../../translation/field/content/LabelContentTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {LabelContent as AppstudioLabelContentDto} from '../../dto/app/configuration/uiconfig/screen/component/field/content/LabelContent';

describe('LabelContent Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty Label Content', () => {
		var source = <AppstudioLabelContentDto>AppstudioModelFactory.create('LabelContent');

		describe('WHEN:  translated', () => {
			var translator = new LabelContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as undefined', () => {
				expect(translation.value).to.equal(undefined);
			});
			it ('THEN: the numberOfLines property should be translated as undefined', () => {
				expect(translation.numberOfLines).to.equal(undefined);
			});
		});
	});

	describe('GIVEN: a Label Content with value', () => {
		var source = <AppstudioLabelContentDto>AppstudioModelFactory.create('LabelContent', {
            value: '1 1 1 1',
            numberOfLines: 2
		});

		describe('WHEN:  translated', () => {
			var translator = new LabelContentTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the value property should be translated as 1 1 1 1', () => {
				expect(translation.value).to.equal('1 1 1 1');
			});
			it ('THEN: the numberOfLines property should be translated as 2', () => {
				expect(translation.numberOfLines).to.equal(2);
			});
		});
	});
});
