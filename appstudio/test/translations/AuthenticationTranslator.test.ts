import chai = require('chai');
const expect = chai.expect;

import {AuthenticationTranslator as AuthenticationTranslator} from '../../translation/authentication/AuthenticationTranslator';
import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {ClientAppModelFactory as ClientAppModelFactory} from '../../factory/ClientAppModelFactory';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import AkamaiProvider = require('../../schema/common/auth/AkamaiProvider');
import NoneProvider = require('../../schema/common/auth/NoneProvider');
import {Authentication as AuthenticationDto} from '../../dto/app/configuration/Authentication';
import {Provider as BaseProviderDto} from '../../dto/common/auth/BaseProvider';

describe('Authentication Translation Tests', () => {

    let translationService = new TranslationService();


    describe('GIVEN: Authentication with Akamai Provider', () => {

    const akamaiIDP = '1';
    const iOS = 'iOS';
    const android = 'android';
    const web = 'web';
    const akamai = <AuthenticationDto<CustomDataDto>>AppstudioModelFactory.create('Authentication', {
        provider: ClientAppModelFactory.create(AkamaiProvider.ID, {
          akamaiIDP: akamaiIDP,
          platformID: {
            iOS: iOS,
            android: android,
            web: web
          },
          redirect: {
            iOS: iOS,
            android: android,
            web: web
          },
          resourceAccessCodes: AppstudioModelFactory.create('CustomData')
        })
      });

    akamai.provider.resourceAccessCodes.content = '{"a":1}';

    describe('WHEN: translated', () => {
      const translator = new AuthenticationTranslator();
      let translation;

      before((done) => {

        translator.translate('videa', translationService, akamai).then(
          (t) => {
            translation = t;
            done();
          },
          (err) => {
            done();
          }
        );
      });

      it('THEN: the translation field akamaiIDP should be translated', () => {
        expect(translation.akamaiIDP).to.equal('1');
      });
      it('THEN: the translation field platform iOS should be translated', () => {
        expect(translation.platformID.iOS).to.equal('iOS');
      });
      it('THEN: the translation field platform android should be translated', () => {
        expect(translation.platformID.android).to.equal('android');
      });
      it('THEN: the translation field platform web should be translated', () => {
        expect(translation.platformID.web).to.equal('web');
      });
      it('THEN: the translation field redirect iOS should be translated', () => {
        expect(translation.redirect.iOS).to.equal('iOS');
      });
      it('THEN: the translation field redirect android should be translated', () => {
        expect(translation.redirect.android).to.equal('android');
      });
      it('THEN: the translation field redirect web should be translated', () => {
        expect(translation.redirect.web).to.equal('web');
      });
      it('THEN: the translation field resourceAccessCodes should be translated', () => {
        expect(translation.resourceAccessCodes.a).to.equal(1);
      });
    });
  });

  describe('GIVEN: Authentication with Custom Provider', () => {

    let custom = <AuthenticationDto<BaseProviderDto>>AppstudioModelFactory.create('Authentication', {
      provider: <CustomDataDto>AppstudioModelFactory.create('CustomData')
    });

    custom.provider.content = '{"a":1}';

    describe('WHEN: translated', () => {
      let translator = new AuthenticationTranslator(),
        translation;

      before((done) => {

        translator.translate('videa', translationService, custom).then(
          (t) => {
            translation = t;
            done();
          },
          (err) => {
            done();
          }
        );
      });

      it('THEN: the translation should be customData content', () => {
        expect(translation.a).to.equal(1);
      });
    });
  });

  describe('GIVEN: Authentication with BasicAuthentication Provider', () => {

    const loginScreenId = '1';
    const loginConfigId = '1';
    const defaultScreenId = '1';
    const defaultConfigId = '1';
    const basicAuthentication = <AuthenticationDto<BaseProviderDto>>AppstudioModelFactory.create('Authentication', {
        provider: ClientAppModelFactory.create('BasicAuthentication', {
          loginMethod : 'POST',
          loginUrl : 'http://api.com',
          logoutMethod : 'PUT',
          logoutUrl : 'http://api.com',
          loginScreenId: loginScreenId,
          loginConfigId: loginConfigId,
          defaultScreenId: defaultScreenId,
          defaultConfigId: defaultConfigId
        })
      });

    describe('WHEN: translated', () => {
      const translator = new AuthenticationTranslator();
      let translation;

      before(() => {
        return translator.translate('videa', translationService, basicAuthentication).then(
          (t) => {
            translation = t;
          });
      });
 
      it('THEN: the translation field loginScreen loginConfigId should be translated', () => {
        expect(translation.loginScreen.configId).to.equal('1');
      });
      it('THEN: the translation field loginScreen loginScreenId should be translated', () => {
        expect(translation.loginScreen.screenId).to.equal('1');
      });
      it('THEN: the translation field defaultScreen defaultConfigId should be translated', () => {
        expect(translation.defaultScreen.configId).to.equal('1');
      });
      it('THEN: the translation field defaultScreen defaultScreenId should be translated', () => {
        expect(translation.defaultScreen.screenId).to.equal('1');
      });
      it('THEN: the translation field login method should be translated', () => {
        expect(translation.login.method).to.equal('POST');
      });
      it('THEN: the translation field login url should be translated', () => {
        expect(translation.login.url).to.equal('http://api.com');
      });
      it('THEN: the translation field logout method should be translated', () => {
        expect(translation.logout.method).to.equal('PUT');
      });
      it('THEN: the translation field logout url should be translated', () => {
        expect(translation.logout.url).to.equal('http://api.com');
      });
    });
  });

  describe('GIVEN: Authentication with None Provider', () => {

    let none = <AuthenticationDto<BaseProviderDto>>AppstudioModelFactory.create('Authentication', {
      provider: ClientAppModelFactory.create(NoneProvider.ID)
    });

    describe('WHEN: translated', () => {
      const translator = new AuthenticationTranslator();
      let translation;

      before((done) => {

        translator.translate('videa', translationService, none).then(
          (t) => {
            translation = t;
            done();
          },
          (err) => {
            done();
          }
        );
      });

      it('THEN: the translation should be empty', () => {
        expect(translation).to.equal(null);
      });
    });
  });
});