import chaiLib = require('chai');
const expect = chaiLib.expect;
import _ = require('lodash');

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {ColorTranslator} from '../../translation/color/ColorTranslator';
import {ComplexColorTranslator} from '../../translation/color/ComplexColorTranslator';
import {TopMenuStyleTranslator} from '../../translation/navigation/style/TopMenuStyleTranslator';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import TopMenuStyleSchema = require('../../schema/appstudio/app/configuration/uiconfig/navigation/style/TopMenuStyle');
import {TopMenuStyle as AppstudioTopMenuStyleDto} from '../../dto/common/navigation/top/TopMenuStyle';

import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('TopMenuStyle Translation Tests', () => {
	const translationService = new TranslationService();

	translationService.registerTranslator(ComplexColorTranslator);
	translationService.registerTranslator(ColorTranslator);

	describe('GIVEN: a TopMenuStyle', () => {
		const source = <AppstudioTopMenuStyleDto>AppstudioModelFactory.create('TopMenuStyle');
		const backgroundColor = undefined;

		describe('WHEN:  translated', () => {
			const translator = new TopMenuStyleTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the backgroundColor property should not translate', () => {
				expect(translation.backgroundColor).to.equal(backgroundColor);
			});
		});
	});

	describe('GIVEN: a TopMenuStyle with backgroundColor', () => {
		let backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };


		let	source = <AppstudioTopMenuStyleDto>AppstudioModelFactory.create('TopMenuStyle', {
            backgroundColor: {
                _metadata: ComplexColorSchema.ID,
                value: backgroundColor
            }
		});

		describe('WHEN:  translated', () => {
			const translator = new TopMenuStyleTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});
			it ('THEN: the backgroundColor property should translated', () => {
				expect(translation.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.backgroundColor._metadata).to.equal(backgroundColor._metadata);
			});
		});
	});
});