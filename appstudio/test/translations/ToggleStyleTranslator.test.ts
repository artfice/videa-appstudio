import chaiLib = require('chai');
var expect = chaiLib.expect;

import {ToggleStyleTranslator as ToggleStyleTranslator} from '../../translation/field/style/ToggleStyleTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {ToggleStyle as AppstudioToggleStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/field/toggle/ToggleStyle';

describe('ToggleStyle Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty Toggle Style', () => {
		var source = <AppstudioToggleStyleDto>AppstudioModelFactory.create('ToggleStyle'),
			activatedColor = undefined,
            deactivatedColor = undefined;

		describe('WHEN:  translated', () => {
			var translator = new ToggleStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should be empty object', () => {
				expect(translation).to.exist;
			});

			it ('THEN: the activatedColor property should be translated as ' + activatedColor, () => {
				expect(translation.activatedColor).to.equal(activatedColor);
			});
			it ('THEN: the deactivatedColor property should be translated as ' + deactivatedColor, () => {
				expect(translation.deactivatedColor).to.equal(deactivatedColor);
			});
		});
	});

	describe('GIVEN: a ToggleStyle with activatedColor and deactivatedColor', () => {
		var activatedColor = '#EEEEEE',
            deactivatedColor = '#AAAAAA',
			source = <AppstudioToggleStyleDto>AppstudioModelFactory.create('ToggleStyle', {
                activatedColor: AppstudioModelFactory.create('Color', {value: activatedColor}),
                deactivatedColor: AppstudioModelFactory.create('Color', {value: deactivatedColor})
		});

		describe('WHEN:  translated', () => {
			var translator = new ToggleStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

            it ('THEN: the activatedColor property should be translated as ' + activatedColor, () => {
				expect(translation.activatedColor.value).to.equal(activatedColor);
			});
			it ('THEN: the deactivatedColor property should be translated as ' + deactivatedColor, () => {
				expect(translation.deactivatedColor.value).to.equal(deactivatedColor);
			});
		});
	});
});
