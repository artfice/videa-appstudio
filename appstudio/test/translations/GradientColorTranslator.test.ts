import chaiLib = require('chai');
const expect = chaiLib.expect;

import Directions = require('../../schema/common/enum/components/Directions');
import { GradientColorTranslator as GradientColorTranslator } from '../../translation/color/GradientColorTranslator';
import { OffsetColorTranslator as OffsetColorTranslator } from '../../translation/color/OffsetColorTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import ClientGradientColorSchema = require('../../schema/clientapp/GradientColor');
import { GradientColor as GradientColorDto } from '../../dto/common/GradientColor';

describe('GradientColor Translation Tests', () => {
    let translationService = new TranslationService();
    translationService.registerTranslator(GradientColorTranslator);
    translationService.registerTranslator(OffsetColorTranslator);

    describe('GIVEN: a Gradient Color with vertical direction', () => {
        let translation;

        let direction = Directions[0];

        let source = <GradientColorDto>AppstudioModelFactory.create('GradientColor', {
            direction: direction
        });

        describe('WHEN: translated', () => {
            let translator = new GradientColorTranslator();


            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done(err);
                    }
                );
            });

            it('THEN: the ' + Directions[0] + ' direction property should translate to ' + 'start: [0.5, 0.0], end: [0.5,1.0]', () => {
                expect(translation.start).to.deep.equal([0.5, 0.0]);
                expect(translation.end).to.deep.equal([0.5, 1.0]);
            });

            it('THEN: the _metadata property should translate to ' + ClientGradientColorSchema.ID, () => {
                expect(translation._metadata).to.equal(ClientGradientColorSchema.ID);
            });
        });
    });

    describe('GIVEN: a Gradient Color with horizontal direction', () => {
        let translation;

        let direction = Directions[1];

        let source = <GradientColorDto>AppstudioModelFactory.create('GradientColor', {
            direction: direction
        });

        describe('WHEN: translated', () => {
            let translator = new GradientColorTranslator();


            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done(err);
                    }
                );
            });

            it('THEN: the ' + Directions[1] + ' direction property should translate to ' + 'start: [0.0, 0.5], end: [1.0, 0.5]', () => {
                expect(translation.start).to.deep.equal([0.0, 0.5]);
                expect(translation.end).to.deep.equal([1.0, 0.5]);
            });

            it('THEN: the _metadata property should translate to ' + ClientGradientColorSchema.ID, () => {
                expect(translation._metadata).to.equal(ClientGradientColorSchema.ID);
            });
        });
    });

    describe('GIVEN: a Gradient Color with two stops', () => {
        let translation;

        let direction = Directions[1];
        let stops = [];

        let offsetColor1 = {
            value: '#FFFF0000',
            offset: '0.1'
        };

        let offsetColor2 = {
            value: '#FF00FF00',
            offset: '0.9'
        };

        stops.push(<GradientColorDto>AppstudioModelFactory.create('OffsetColor', {
            value: offsetColor1.value,
            offset: offsetColor1.offset
        }));

        stops.push(<GradientColorDto>AppstudioModelFactory.create('OffsetColor', {
            value: offsetColor2.value,
            offset: offsetColor2.offset
        }));

        let source = <GradientColorDto>AppstudioModelFactory.create('GradientColor', {
            direction: direction,
            stops: stops
        });

        describe('WHEN: translated', () => {
            let translator = new GradientColorTranslator();


            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done(err);
                    }
                );
            });

            it('THEN: the _metadata property should translate to ' + ClientGradientColorSchema.ID, () => {
                expect(translation._metadata).to.equal(ClientGradientColorSchema.ID);
            });

            it('THEN: the stop1 property should translate to ' + '{value: ' + offsetColor1.value + ', offset: ' + offsetColor1.offset + '}', () => {
                expect(translation.stops[0].value).to.equal(offsetColor1.value);
                expect(translation.stops[0].offset).to.equal(offsetColor1.offset);
            });

            it('THEN: the stop2 property should translate to ' + '{value: ' + offsetColor2.value + ', offset: ' + offsetColor2.offset + '}', () => {
                expect(translation.stops[1].value).to.equal(offsetColor2.value);
                expect(translation.stops[1].offset).to.equal(offsetColor2.offset);
            });
        });
    });
});