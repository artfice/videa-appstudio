import chaiLib = require('chai');
const expect = chaiLib.expect;

import {DataViewHeadingStyleTranslator as DataViewHeadingStyleTranslator} from '../../translation/dataview/style/DataViewHeadingStyleTranslator';
import {ComplexColorTranslator} from '../../translation/color/ComplexColorTranslator';
import {ColorTranslator} from '../../translation/color/ColorTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {DataViewHeadingStyle as AppstudioDataViewHeadingStyleDto} from '../../dto/app/configuration/uiconfig/screen/dataview/heading/DataViewHeadingStyle';

import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');

describe('TileStyle Translation Tests', () => {
	var translationService = new TranslationService();

	translationService.registerTranslator(ComplexColorTranslator);
	translationService.registerTranslator(ColorTranslator);

	describe('GIVEN: an empty DataViewHeadingStyle', () => {
		var source = <AppstudioDataViewHeadingStyleDto>AppstudioModelFactory.create('DataViewHeadingStyle');

		describe('WHEN:  translated', () => {
			var translator = new DataViewHeadingStyleTranslator(),
				translation,
				backgroundColor = undefined;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the backgroundColor value property should be translated as ' + backgroundColor, () => {
				expect(translation.backgroundColor).to.equal(backgroundColor);
			});
		});
	});

	describe('GIVEN: a DataViewHeadingStyle with values', () => {
		let backgroundColor = {
			_metadata: ColorSchema.ID,
			value: '#BBBBBB'
		};
		var source = <AppstudioDataViewHeadingStyleDto>AppstudioModelFactory.create('DataViewHeadingStyle', {
				backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				}
		});

		describe('WHEN:  translated', () => {
			var translator = new DataViewHeadingStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the backgroundColor property should be translated as ' + JSON.stringify(backgroundColor), () => {
				expect(translation.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.backgroundColor._metadata).to.equal(backgroundColor._metadata);
			});
		});
	});
});
