import chaiLib = require('chai');
const expect = chaiLib.expect;

import { BaseFieldStyleTranslator } from '../../translation/field/style/BaseFieldStyleTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { ProgressBarTranslator } from '../../translation/field/ProgressBarTranslator';
import { ProgressBarStyleTranslator } from '../../translation/field/style/ProgressBarStyleTranslator';
import { FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

describe('ProgressBar Translation Tests', () => {
    var translationService = new TranslationService();
    translationService.registerTranslator(ProgressBarTranslator);
    translationService.registerTranslator(ProgressBarStyleTranslator);
    translationService.registerTranslator(BaseFieldStyleTranslator);
    translationService.registerTranslator(FieldContentTranslator);

    describe('GIVEN: a ProgressBar', () => {
        var source = <any>AppstudioModelFactory.create('ProgressBar', {
            style: AppstudioModelFactory.create('ProgressBarStyle'),
            content: AppstudioModelFactory.create('FieldContent')
        });

        describe('WHEN: translated', () => {
            var translator = new ProgressBarTranslator(),
                translation;
            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });
            it('THEN: the value property should be translated as undefined', () => {
                expect(translation.value).to.equal(undefined);
            });
            it('THEN: the width property should be translated as undefined', () => {
                expect(translation.width).to.equal(undefined);
            });
            it('THEN: the height property should be translated as undefined', () => {
                expect(translation.height).to.equal(undefined);
            });
            it('THEN: the gravity property should be translated as undefined', () => {
                expect(translation.gravity).to.equal(undefined);
            });
        });
    });

    describe('GIVEN: a ProgressBar with style and value', () => {
		var width = 1,
			height = 1,
			source = <any>AppstudioModelFactory.create('ProgressBar', {
            style: AppstudioModelFactory.create('ProgressBarStyle', {
                backgroundColor: AppstudioModelFactory.create('Color', {
					value: '#111111'
				}),
                foregroundColor: AppstudioModelFactory.create('Color', {
					value: '#111111'
				}),
                gravity: 'Left',
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            })
        });

		describe('WHEN: translated', () => {
			var translator = new ProgressBarTranslator(),
				translation;
			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated as test', () => {
				expect(translation.value).to.equal('test');
			});			
			it ('THEN: the width property should be translated as 1', () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should be translated as 1', () => {
				expect(translation.height).to.equal(height);
			});			
			it ('THEN: the foregroundColor property should be translated as #111111', () => {
				expect(translation.style.foregroundColor.value).to.equal('#111111');
			});			
			it ('THEN: the backgroundColor property should be translated as #111111', () => {
				expect(translation.style.backgroundColor.value).to.equal('#111111');
			});			
			it ('THEN: the gravity property should be translated as Left', () => {
				expect(translation.gravity).to.equal('Left');
			});			
		});
	});
});