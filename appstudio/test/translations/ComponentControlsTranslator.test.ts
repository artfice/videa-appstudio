import chaiLib = require('chai');
const expect = chaiLib.expect;

import { ComponentControlsTranslator as ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ListenerTranslator as ListenerTranslator } from '../../translation/ListenerTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { AppstudioModelFactory as AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory as ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import { ComponentControls as ComponentControlsDto } from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import { BaseEvent as BaseEventDto } from '../../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../../dto/common/listener/action/BaseAction';

import SelectEventSchema = require('../../schema/common/listener/event/SelectEvent');
import LongSelectEventSchema = require('../../schema/common/listener/event/LongSelectEvent');

import SignoutActionSchema = require('../../schema/common/listener/action/SignoutAction');
import NavigationActionSchema = require('../../schema/common/listener/action/NavigationAction');
import SigninActionSchema = require('../../schema/common/listener/action/SigninAction');
import PlayBackActionSchema = require('../../schema/common/listener/action/PlaybackAction');
import SaveOfflineActionSchema = require('../../schema/common/listener/action/SaveOfflineAction');
import RemoveOfflineActionSchema = require('../../schema/common/listener/action/RemoveOfflineAction');
import SaveUIDActionSchema = require('../../schema/common/listener/action/SaveUIDAction');
import RemoveUIDActionSchema = require('../../schema/common/listener/action/RemoveUIDAction');
import LinkActionSchema = require('../../schema/common/listener/action/LinkAction');

import { Listener as ListenerDto } from '../../dto/common/Listener';
import {SignoutAction} from '../../dto/common/listener/action/SignoutAction';
import {SigninAction} from '../../dto/common/listener/action/SigninAction';
import {SaveUIDAction} from '../../dto/common/listener/action/SaveUIDAction';
import {RemoveUIDAction} from '../../dto/common/listener/action/RemoveUIDAction';
import {SaveOfflineAction} from '../../dto/common/listener/action/SaveOfflineAction';
import {RemoveOfflineAction} from '../../dto/common/listener/action/RemoveOfflineAction';
import {PlaybackAction} from '../../dto/common/listener/action/PlaybackAction';
import {NavigationAction} from '../../dto/common/listener/action/NavigationAction';

describe('ComponentControls Translation Tests', () => {
	const translationService = new TranslationService();
	translationService.registerTranslator(ListenerTranslator);

	describe('GIVEN: an empty ComponentControls', () => {
		const visible = 'true';
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the translation should not be an empty object', () => {
				expect(translation).to.exist;
			});
			it('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});
		});
	});

	describe('GIVEN: a ComponentControls with visible', () => {
		const visible = '1';
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
				visible: visible
			});

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});
		});
	});

	describe('GIVEN: a ComponentControls with a listener with event select and action signout', () => {
		const selectEvent = ClientAppModelFactory.create(SelectEventSchema.ID)
		const select = 'Select';
		const signoutAction = ClientAppModelFactory.create(SignoutActionSchema.ID) as SignoutAction;
		const signout = 'Signout';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = selectEvent;
		listener.action = signoutAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + select, () => {
				expect(translation.listeners[0].event.type).to.equal(select);
			});
			it('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.listeners[0].action.type).to.equal(signout);
			});
		});
	});

	describe('GIVEN: a ComponentControls with a listener with event long select and action signout', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const signoutAction = ClientAppModelFactory.create(SignoutActionSchema.ID) as SignoutAction;
		const signout = 'Signout';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = signoutAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.listeners[0].action.type).to.equal(signout);
			});
		});
	});

	describe('GIVEN: a ComponentControls with a listener with event long select and action signin no username and password', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const username = undefined;
		const password = undefined;
		const signinAction = ClientAppModelFactory.create(SigninActionSchema.ID, {
		}) as SigninAction;
		const signin = 'Signin';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = signinAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + signin, () => {
				expect(translation.listeners[0].action.type).to.equal(signin);
			});
			it('THEN: the listener action username property should be translated as ' + username, () => {
				expect(translation.listeners[0].action.data).to.equal(username);
			});
			it('THEN: the listener action password property should be translated as ' + password, () => {
				expect(translation.listeners[0].action.data).to.equal(password);
			});
		});
	});

	describe('GIVEN: a ComponentControls with a listener with event long select and action signin and has username and password', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const username = 'user';
		const password = 'password';
		const signinAction = ClientAppModelFactory.create(SigninActionSchema.ID, {
				data: {
					username: username,
					password: password
				}
			}) as SigninAction;
		const signin = 'Signin';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = signinAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + signin, () => {
				expect(translation.listeners[0].action.type).to.equal(signin);
			});
			it('THEN: the listener action username property should be translated as ' + username, () => {
				expect(translation.listeners[0].action.data.username).to.equal(username);
			});
			it('THEN: the listener action password property should be translated as ' + password, () => {
				expect(translation.listeners[0].action.data.password).to.equal(password);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action SaveUIDAction and has path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = 'path';
		const saveUIDAction = ClientAppModelFactory.create(SaveUIDActionSchema.ID, {
				path: path
			}) as SaveUIDAction;
		const saveUID = 'SaveUID';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = saveUIDAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + saveUID, () => {
				expect(translation.listeners[0].action.type).to.equal(saveUID);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action SaveUIDAction and has no path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = undefined;
		const saveUIDAction = ClientAppModelFactory.create(SaveUIDActionSchema.ID) as SaveUIDAction;
		const saveUID = 'SaveUID';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = saveUIDAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + saveUID, () => {
				expect(translation.listeners[0].action.type).to.equal(saveUID);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action RemoveUIDAction and has path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = 'path';
		const removeUIDAction = ClientAppModelFactory.create(RemoveUIDActionSchema.ID, {
				path: path
		}) as RemoveUIDAction;
		const removeUID = 'RemoveUID';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = removeUIDAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + removeUID, () => {
				expect(translation.listeners[0].action.type).to.equal(removeUID);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action RemoveUIDAction and has no path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = undefined;
		const removeUIDAction = ClientAppModelFactory.create(RemoveUIDActionSchema.ID) as RemoveUIDAction;
		const removeUID = 'RemoveUID';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = removeUIDAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			var translator = new ComponentControlsTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + removeUID, () => {
				expect(translation.listeners[0].action.type).to.equal(removeUID);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action SaveOfflineAction and has path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = 'path';
		const saveOfflineAction = ClientAppModelFactory.create(SaveOfflineActionSchema.ID, {
				path: path
			}) as SaveOfflineAction;
		const saveOffline = 'SaveOffline';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = saveOfflineAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + saveOffline, () => {
				expect(translation.listeners[0].action.type).to.equal(saveOffline);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action SaveOfflineAction and has no path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = undefined;
		const saveOfflineAction = ClientAppModelFactory.create(SaveOfflineActionSchema.ID) as SaveOfflineAction;
		const saveOffline = 'SaveOffline';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = saveOfflineAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + saveOffline, () => {
				expect(translation.listeners[0].action.type).to.equal(saveOffline);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action RemoveOfflineAction and has path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = 'path';
		const removeOfflineAction = ClientAppModelFactory.create(RemoveOfflineActionSchema.ID, {
				path: path
			}) as RemoveOfflineAction;
		const removeOffline = 'RemoveOffline';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = removeOfflineAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + removeOffline, () => {
				expect(translation.listeners[0].action.type).to.equal(removeOffline);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action RemoveOfflineAction and has no path', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const path = undefined;
		const removeOfflineAction = ClientAppModelFactory.create(RemoveOfflineActionSchema.ID) as RemoveOfflineAction;
		const removeOffline = 'RemoveOffline';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = removeOfflineAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + removeOffline, () => {
				expect(translation.listeners[0].action.type).to.equal(removeOffline);
			});
			it('THEN: the listener action path property should be translated as ' + path, () => {
				expect(translation.listeners[0].action.path).to.equal(path);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action PlaybackAction and has no fields', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const live = undefined;
		const title = undefined;
		const description = undefined;
		const duration = undefined;
		const sourceField = undefined;
		const format = undefined;
		const player = undefined;
		const closedCaptioningFormat = undefined;
		const playbackAction = ClientAppModelFactory.create(PlayBackActionSchema.ID) as PlaybackAction;
		const playback = 'Playback';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');
		listener.event = longSelectEvent;
		listener.action = playbackAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + playback, () => {
				expect(translation.listeners[0].action.type).to.equal(playback);
			});
			it('THEN: the listener action live property should be translated as ' + live, () => {
				expect(translation.listeners[0].action.live).to.equal(live);
			});
			it('THEN: the listener action title property should be translated as ' + title, () => {
				expect(translation.listeners[0].action.title).to.equal(title);
			});
			it ('THEN: the listener action player property should be translated as ' + player, () => {
 				expect(translation.listeners[0].action.player).to.equal(player);
 			});			
			it('THEN: the listener action description property should be translated as ' + description, () => {
				expect(translation.listeners[0].action.description).to.equal(description);
			});
			it('THEN: the listener action duration property should be translated as ' + duration, () => {
				expect(translation.listeners[0].action.duration).to.equal(duration);
			});
			it('THEN: the listener action source property should be translated as ' + sourceField, () => {
				expect(translation.listeners[0].action.source).to.equal(sourceField);
			});
			it('THEN: the listener action format property should be translated as ' + format, () => {
				expect(translation.listeners[0].action.format).to.equal(format);
			});
			it('THEN: the listener action closedCaptioningFormat property should be translated as ' + closedCaptioningFormat, () => {
				expect(translation.listeners[0].action.closedCaptioningFormat).to.equal(closedCaptioningFormat);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action PlaybackAction and has live, title, description, duration, closedCaptioningFormat, format', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const live = 'live';
		const title = 'title';
		const description = 'description';
		const duration = 'duration';
		const sourceField = 'source';
		const format = 'format';
		const player = 'player';
		const closedCaptioningFormat = 'closedCaptioningFormat';
		const playbackAction = ClientAppModelFactory.create(PlayBackActionSchema.ID, {
				live: 'live',
				title: 'title',
				description: 'description',
				duration: 'duration',
				source: 'source',
				format: 'format',
				player : 'player',
				closedCaptioningFormat: 'closedCaptioningFormat'
			});
		const playback = 'Playback';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');
		
		listener.event = longSelectEvent;
		listener.action = playbackAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			var translator = new ComponentControlsTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + playback, () => {
				expect(translation.listeners[0].action.type).to.equal(playback);
			});
			it('THEN: the listener action live property should be translated as ' + live, () => {
				expect(translation.listeners[0].action.live).to.equal(live);
			});
			it('THEN: the listener action title property should be translated as ' + title, () => {
				expect(translation.listeners[0].action.title).to.equal(title);
			});
			it('THEN: the listener action description property should be translated as ' + description, () => {
				expect(translation.listeners[0].action.description).to.equal(description);
			});
			it('THEN: the listener action duration property should be translated as ' + duration, () => {
				expect(translation.listeners[0].action.duration).to.equal(duration);
			});
			it('THEN: the listener action source property should be translated as ' + sourceField, () => {
				expect(translation.listeners[0].action.source).to.equal(sourceField);
			});
			it('THEN: the listener action format property should be translated as ' + format, () => {
				expect(translation.listeners[0].action.format).to.equal(format);
			});
			it('THEN: the listener action closedCaptioningFormat property should be translated as ' + closedCaptioningFormat, () => {
				expect(translation.listeners[0].action.closedCaptioningFormat).to.equal(closedCaptioningFormat);
			});
			it ('THEN: the listener action player property should be translated as ' + player, () => {
 				expect(translation.listeners[0].action.player).to.equal(player);
 			});			
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action NavigationAction and has no fields', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const configId = undefined;
		const screenId = undefined;
		const navigationAction = ClientAppModelFactory.create(NavigationActionSchema.ID);
		const navigation = 'Navigation';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = navigationAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			var translator = new ComponentControlsTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + navigation, () => {
				expect(translation.listeners[0].action.type).to.equal(navigation);
			});
			it('THEN: the listener action configId property should be translated as ' + configId, () => {
				expect(translation.listeners[0].action.configId).to.equal(configId);
			});
			it('THEN: the listener action screenId property should be translated as ' + screenId, () => {
				expect(translation.listeners[0].action.screenId).to.equal(screenId);
			});
		});
	});
	describe('GIVEN: a ComponentControls with a listener with event long select and action NavigationAction and has live, title, description, duration, closedCaptioningFormat, format', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const configId = 'configId';
		const screenId = 'screenId';
		const navigationAction = ClientAppModelFactory.create(NavigationActionSchema.ID, {
				configId: 'configId',
				screenId: 'screenId'
			});
		const navigation = 'Navigation';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.event = longSelectEvent;
		listener.action = navigationAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			var translator = new ComponentControlsTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + navigation, () => {
				expect(translation.listeners[0].action.type).to.equal(navigation);
			});
			it('THEN: the listener action configId property should be translated as ' + configId, () => {
				expect(translation.listeners[0].action.configId).to.equal(configId);
			});
			it('THEN: the listener action screenId property should be translated as ' + screenId, () => {
				expect(translation.listeners[0].action.screenId).to.equal(screenId);
			});
		});
	});

	describe('GIVEN: a ComponentControls with active', () => {
		const active = 'active state';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');

		listener.active = active;
		source.listeners = [listener];
		
		describe('WHEN: translated', () => {
			var translator = new ComponentControlsTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the active property should be translated', () => {
				expect(translation.listeners[0].active).to.equal(active);
			});
		});
	});	
		
	describe('GIVEN: a ComponentControls with a listener with event long select and action AnalyticAction and has BaseReference', () => {
		let longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		let longSelect = 'LongSelect';
		let analyticValue = 'Analytic Event';
		let analyticAction = ClientAppModelFactory.create('AnalyticAction', {
			event: AppstudioModelFactory.create('BaseReference', {
				value: analyticValue,
				refId: analyticValue
			})
		});
		let analytic = 'Analytics';
		let listener = <any>ClientAppModelFactory.create('Listener');
		let source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');
		listener.event = longSelectEvent;
		listener.action = analyticAction;

		source.listeners.push(listener);

		describe('WHEN: translated', () => {
			var translator = new ComponentControlsTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it('THEN: the listener action type property should be translated as ' + analytic, () => {
				expect(translation.listeners[0].action.type).to.equal(analytic);
			});
		});
	});

	describe('GIVEN: a ComponentControls with a listener with event long select and action LinkAction and has link', () => {
		const longSelectEvent = ClientAppModelFactory.create(LongSelectEventSchema.ID);
		const longSelect = 'LongSelect';
		const link = 'url';
		const linkAction = ClientAppModelFactory.create(LinkActionSchema.ID, {
			link: link
		});
		const type = 'Link';
		const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
		const source = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls');
			
		listener.event = longSelectEvent;
		listener.action = linkAction;
		
		source.listeners.push(listener);
		
		describe('WHEN: translated', () => {
			const translator = new ComponentControlsTranslator();
			let translation;
				
			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});
			
			it ('THEN: the listener event type property should be translated', () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});
			it ('THEN: the listener action type property should be translated', () => {
				expect(translation.listeners[0].action.type).to.equal(type);
			});
			it ('THEN: the listener action path property should be translated', () => {
				expect(translation.listeners[0].action.link).to.equal(link);
			});						
		});
	});	
});
