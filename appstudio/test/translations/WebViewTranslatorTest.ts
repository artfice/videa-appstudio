import chaiLib = require('chai');
const expect = chaiLib.expect;
import _ = require('lodash');

import {WebViewTranslator} from '../../translation/webview/WebViewTranslator';
import {WebViewStyleTranslator} from '../../translation/webview/WebViewStyleTranslator';
import {WebViewContentTranslator} from '../../translation/webview/WebViewContentTranslator';
import {ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import WebViewSchema = require('../../schema/appstudio/app/configuration/uiconfig/screen/component/webview/WebView');
import {WebView as AppstudioWebViewDto} from '../../dto/app/configuration/uiconfig/screen/webview/WebView';
import {WebViewStyle as AppstudioWebViewStyleDto} from '../../dto/app/configuration/uiconfig/screen/webview/WebViewStyle';
import {WebViewContent as WebViewContentDto} from '../../dto/app/configuration/uiconfig/screen/webview/WebViewContent';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('WebView Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(WebViewStyleTranslator);
	translationService.registerTranslator(WebViewContentTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
    
	describe('GIVEN: an empty WebView ', () => {
		var width = undefined,
			height = undefined,
			uri = '',
			name = undefined,
			verticalScrollEnabled = undefined,
			horizontalScrollEnabled = undefined,
			fallbackMargin = undefined,
			visible = 'true',
			border = undefined,
			source = <any>AppstudioModelFactory.create('WebView', {
            style: AppstudioModelFactory.create('WebViewStyle'),
            content: AppstudioModelFactory.create('WebViewContent'),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls')
        });

		describe('WHEN: translated', () => {
			var translator = new WebViewTranslator(),
				translation;

			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
                });
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the uri property should translate to ' + uri, () => {
				expect(translation.uri).to.equal(uri);
			});			
			it ('THEN: the width property should translate to ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should translate to ' + height, () => {
				expect(translation.height).to.equal(height);
			});			
			it ('THEN: the name property should translate to ' + name, () => {
				expect(translation.name).to.equal(name);
			});			
			it ('THEN: the fallbackMargin property should translate to ' + fallbackMargin, () => {
				expect(translation.fallbackMargin).to.equal(fallbackMargin);
			});			
			it ('THEN: the verticalScrollEnabled property should translate to ' + verticalScrollEnabled, () => {
				expect(translation.verticalScrollEnabled).to.equal(verticalScrollEnabled);
			});			
			it ('THEN: the horizontalScrollEnabled property should translate to ' + horizontalScrollEnabled, () => {
				expect(translation.horizontalScrollEnabled).to.equal(horizontalScrollEnabled);
			});			
			it ('THEN: the visible property should be true', () => {
				expect(translation.visible).to.equal('true');
			});			
			it ('THEN: the style property should have type Style', () => {
				expect(translation.style.type).to.equal('Style');
			});			
			it ('THEN: the style border property should translate to ' + border, () => {
				expect(translation.style.border).to.equal(border);
			});						
			it ('THEN: the listeners property should be []', () => {
				expect(translation.listeners.length).to.equal(0);
			});					
		});
	});

	describe('GIVEN: a WebView with a style', () => {
		var width = 1,
		    height = 1,
			verticalScrollEnabled = true,
			horizontalScrollEnabled = true,
			fallbackMargin = '0 0 0 0',
			uri = 'a',
			visible = 'not true',
			source = <any>AppstudioModelFactory.create('WebView', {
            style: AppstudioModelFactory.create('WebViewStyle', {
                
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				}),
                verticalScrollEnabled : verticalScrollEnabled,
				horizontalScrollEnabled: horizontalScrollEnabled,
				fallbackMargin: fallbackMargin
            }),
            content: AppstudioModelFactory.create('WebViewContent', {
                uri: uri
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });

		describe('WHEN: translated', () => {
			var translator = new WebViewTranslator(),
				translation;

			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					}
				);
			});

			it ('THEN: the width property should translate to ' + width, () => {
				expect(translation.width).to.equal(width);
			});			
			it ('THEN: the height property should translate to ' + height, () => {
				expect(translation.height).to.equal(height);
			});			
			it ('THEN: the verticalScrollEnabled property should translate to ' + verticalScrollEnabled, () => {
				expect(translation.verticalScrollEnabled).to.equal(verticalScrollEnabled);
			});			
			it ('THEN: the horizontalScrollEnabled property should translate to ' + horizontalScrollEnabled, () => {
				expect(translation.horizontalScrollEnabled).to.equal(horizontalScrollEnabled);
			});			
			it ('THEN: the fallbackMargin property should translate to ' + fallbackMargin, () => {
				expect(translation.fallbackMargin).to.equal(fallbackMargin);
			});			
		});
	});
	
	describe('GIVEN: a WebView with a content', () => {
		var uri = 'test',
			source = <any>AppstudioModelFactory.create('WebView', {
            content: AppstudioModelFactory.create('WebViewContent', {
                uri: uri
            })
        });

		describe('WHEN: translated', () => {
			var translator = new WebViewTranslator(),
				translation;

			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					}
				);
			});

			it ('THEN: the uri property should translate to ' + uri , () => {
				expect(translation.uri).to.equal(uri);
			});			
		});
	});	

	describe('GIVEN: a WebView with a controls', () => {
		var visible = 'not true',
			source = <any>AppstudioModelFactory.create('WebView', {
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });

		describe('WHEN: translated', () => {
			var translator = new WebViewTranslator(),
				translation;

			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					}
				);
			});

			it ('THEN: the visible property should translate to ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});			
		});
	});	
});