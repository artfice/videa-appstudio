import chaiLib = require('chai');
const expect = chaiLib.expect;
import _ = require('lodash');

import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');

import {ColorTranslator}  from '../../translation/color/ColorTranslator';
import {ComplexColorTranslator}  from '../../translation/color/ComplexColorTranslator';

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {TopMenuSectionTranslator} from '../../translation/navigation/section/TopMenuSectionTranslator';

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {ComponentStyle as ComponentStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';

import {BaseTranslator} from '../../translation/BaseTranslator';
import {ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ImageContentTranslator} from '../../translation/field/content/ImageContentTranslator';
import {ImageTranslator} from '../../translation/field/ImageTranslator';
import {ImageStyleTranslator} from '../../translation/field/style/ImageStyleTranslator';
import {TopMenuStyleTranslator} from '../../translation/navigation/style/TopMenuStyleTranslator';


import {ListenerTranslator} from '../../translation/ListenerTranslator';
import {CustomAspectRatioTranslator} from '../../translation/aspectRatio/CustomAspectRatioTranslator';
import {StandardAspectRatioTranslator} from '../../translation/aspectRatio/StandardAspectRatioTranslator';

import {TopMenuSection as AppstudioTopMenuSectionDto} from '../../dto/common/navigation/top/TopMenuSection';

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('TopMenuSection Component Translation Tests', () => {
	let translationService = new TranslationService();

	translationService.registerTranslator(BaseTranslator);
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(ImageStyleTranslator);
	translationService.registerTranslator(ImageContentTranslator);
	translationService.registerTranslator(ImageTranslator);
	translationService.registerTranslator(ComplexColorTranslator);
	translationService.registerTranslator(ColorTranslator);
	translationService.registerTranslator(TopMenuStyleTranslator);
	translationService.registerTranslator(StandardAspectRatioTranslator);
	translationService.registerTranslator(CustomAspectRatioTranslator);
	translationService.registerTranslator(ListenerTranslator);
	translationService.registerTranslator(FieldContentTranslator);

	describe('GIVEN: an empty TopMenuSection', () => {
		const source = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection');

		describe('WHEN: translated', () => {
			const translator = new TopMenuSectionTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it('THEN: the translation should exist', () => {
				expect(translation).to.exist;
				expect(translation.content).to.exist;
				expect(translation.content.subComponent.length).to.equal(0);
				expect(translation.style).to.equal(null);
				expect(translation.controls).to.equal(null);
			});
		});
	});

    describe('GIVEN: a TopMenuSection with Style', () => {
		const backgroundColor = {
		    _metadata: ColorSchema.ID,
		    value: '#AAAAAA'
        };

        const source = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
            style: AppstudioModelFactory.create('TopMenuStyle', {
                margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
                backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				}
            })
        });

        describe('WHEN: translated', () => {
            let translation;
			const translator = new TopMenuSectionTranslator();
            before(() => {
                return translator.translate('videa', translationService, source).then((translated) => {
                    translation = translated;
                });
            });

            it('THEN: the TopMenuSection style should be translated', () => {
                expect(translation.style.padding).to.equal('1 1 1 1');
                expect(translation.style.margin).to.equal('1 1 1 1');
				expect(translation.style.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.style.backgroundColor._metadata).to.equal(backgroundColor._metadata);
            });
        });
    });

    describe('GIVEN: a TopMenuSection with Controls', () => {
        const source = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
             controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

        describe('WHEN: translated', () => {
            let translation;
			const translator = new TopMenuSectionTranslator();
            before(() => {
                return translator.translate('videa', translationService, source).then((translated) => {
                    translation = translated;
                });
            });

			it ('THEN: the TopMenuSection controls property should be translated', () => {
				expect(translation.controls.visible).to.equal('not true');
			});
        });
    });

	describe('GIVEN: a Layer with items', () => {
		const image = AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });
		const source = <AppstudioTopMenuSectionDto>AppstudioModelFactory.create('TopMenuSection', {
			content: {
				subComponent: []
			}
		});

		source.content.subComponent.push(image);

		describe('WHEN: translated', () => {
			const translator = new TopMenuSectionTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it ('THEN: the value property should be translated', () => {
				expect(translation.content.subComponent[0].value).to.equal('test');
			});
			it ('THEN: the width property should be translated', () => {
				expect(translation.content.subComponent[0].width).to.equal(1);
			});
			it ('THEN: the height property should be translated', () => {
				expect(translation.content.subComponent[0].height).to.equal(1);
			});
			it ('THEN: the scale property should be translated', () => {
				expect(translation.content.subComponent[0].scale).to.equal('aspectFit');
			});
			it ('THEN: the aspectRatio property should be translated', () => {
				expect(translation.content.subComponent[0].aspectRatio).to.equal(1);
			});
			it ('THEN: the gravity property should be translated', () => {
				expect(translation.content.subComponent[0].gravity).to.equal('Left');
			});
			it ('THEN: the visible property should be translated', () => {
				expect(translation.content.subComponent[0].visible).to.equal('not true');
			});
		});
	});
});