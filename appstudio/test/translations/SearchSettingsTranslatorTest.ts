import chaiLib = require('chai');
const expect = chaiLib.expect;

import { BaseFieldStyleTranslator } from '../../translation/field/style/BaseFieldStyleTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { SearchSettingsTranslator } from '../../translation/uiConfig/search/SearchSettingsTranslator';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import AppstudioSearchSettingsSchema = require('../../schema/appstudio/app/configuration/uiconfig/SearchSettings');
import SearchSettingsSchema = require('../../schema/clientapp/app/uiconfig/Search');

import {SearchSettings as AppstudioSearchSettingsDto} from '../../dto/app/configuration/uiconfig/SearchSettings';
import {SearchSettings as SearchSettingsDto} from '../../dto/clientapp/app/uiconfig/SearchSettings';

describe('SearchSettings Translation Tests', () => {
    const accountId: string = 'videa';
    var translationService = new TranslationService();
    translationService.registerTranslator(SearchSettingsTranslator);

    describe('GIVEN: an empty Search', () => {
            const searchSettings = <AppstudioSearchSettingsDto> AppstudioModelFactory.create(AppstudioSearchSettingsSchema.ID);

            describe('WHEN: translated', () => {
                let translation: SearchSettingsDto;
                before((done) => {
                    new SearchSettingsTranslator().translate(accountId, translationService, searchSettings).then(
                        (searchSettingsDto: SearchSettingsDto) => {
                            translation = searchSettingsDto;
                            done();
                        }, (error) => {
                            done();
                        }
                    );
                });

                it('THEN: the translation is NULL', () => {
                    expect(translation).to.equal(null);
                });
            });
        });
describe('GIVEN: an enabled Search containing all the parameters', () => {
        const searchSettings = <AppstudioSearchSettingsDto> AppstudioModelFactory.create(AppstudioSearchSettingsSchema.ID);
        searchSettings.enabled = true;
        searchSettings.name = 'NAME';
        searchSettings.screenId = 'SEARCH_RESULT_SCREEN_ID';

        describe('WHEN: translated', () => {
            let translation: SearchSettingsDto;
            before((done) => {
                new SearchSettingsTranslator().translate(accountId, translationService, searchSettings).then(
                    (searchSettingsDto: SearchSettingsDto) => {
                        translation = searchSettingsDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation produces the Client metadata AND all the parameters, except "enabled"', () => {
                expect(translation).to.have.property('_metadata', SearchSettingsSchema.ID);
                expect(translation).to.not.have.property('enabled');
                expect(translation).to.have.property('name', 'NAME');
                expect(translation).to.have.property('screenId', 'SEARCH_RESULT_SCREEN_ID');
            });
        });
    });

    describe('GIVEN: an disabled Search containing all the parameters', () => {
        const searchSettings = <AppstudioSearchSettingsDto> AppstudioModelFactory.create(AppstudioSearchSettingsSchema.ID);
        searchSettings.enabled = false;
        searchSettings.name = 'NAME';
        searchSettings.screenId = 'SEARCH_RESULT_SCREEN_ID';

        describe('WHEN: translated', () => {
            let translation: SearchSettingsDto;
            before((done) => {
                new SearchSettingsTranslator().translate(accountId, translationService, searchSettings).then(
                    (searchSettingsDto: SearchSettingsDto) => {
                        translation = searchSettingsDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation is NULL', () => {
                expect(translation).to.equal(null);
            });
        });
    });

    describe('GIVEN: an enabled Search without Search Result Screen', () => {
        const searchSettings = <AppstudioSearchSettingsDto> AppstudioModelFactory.create(AppstudioSearchSettingsSchema.ID);
        searchSettings.enabled = true;
        searchSettings.name = 'NAME';
        searchSettings.screenId = null;

        describe('WHEN: translated', () => {
            let translation: SearchSettingsDto;
            before((done) => {
                new SearchSettingsTranslator().translate(accountId, translationService, searchSettings).then(
                    (searchSettingsDto: SearchSettingsDto) => {
                        translation = searchSettingsDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation is NULL', () => {
                expect(translation).to.equal(null);
            });
        });
    });        

});