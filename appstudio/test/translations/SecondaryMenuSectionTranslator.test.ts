/**
 * Created by bardiakhosravi on 2016-03-09.
 */

import chai = require('chai');
chai.use(require('chai-shallow-deep-equal'));
const expect = chai.expect;
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { ClientAppModelFactory as ClientAppModelFactory } from '../../factory/ClientAppModelFactory';
import { AppstudioModelFactory as AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import ContainerSchema = require('../../schema/clientapp/app/uiconfig/screen/component/container/Container');

import { SecondaryMenuSection as SecondaryMenuSectionDto } from '../../dto/app/configuration/uiconfig/navigation/secondarymenu/SecondaryMenuSection';
import { ListenerTranslator as ListenerTranslator } from '../../translation/ListenerTranslator';
import { ImageTranslator as ImageTranslator } from '../../translation/field/ImageTranslator';
import { ColoredComponentStyleTranslator as ColoredComponentStyleTranslator } from '../../translation/component/ColoredComponentStyleTranslator';
import { Image as AppstudioImageDto } from '../../dto/app/configuration/uiconfig/screen/component/field/image/Image';

import { ComponentControls as ComponentControlsDto } from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import { BaseEvent as BaseEventDto } from '../../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../../dto/common/listener/action/BaseAction';
import { ImageStyleTranslator as ImageStyleTranslator } from '../../translation/field/style/ImageStyleTranslator';
import { FieldContentTranslator as FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';
import { ComponentControlsTranslator as ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ComponentStyleTranslator as ComponentStyleTranslator } from '../../translation/component/ComponentStyleTranslator';
import { ComponentTranslator as ComponentTranslator } from '../../translation/component/ComponentTranslator';
import { BaseFieldTranslator as BaseFieldTranslator } from '../../translation/field/BaseFieldTranslator';
import { LabelTranslator as LabelTranslator } from '../../translation/field/LabelTranslator';
import { LabelStyleTranslator as LabelStyleTranslator } from '../../translation/field/style/LabelStyleTranslator';
import { LabelContentTranslator as LabelContentTranslator } from '../../translation/field/content/LabelContentTranslator';
import { SizedComponentStyleTranslator as SizedComponentStyleTranslator } from '../../translation/component/SizedComponentStyleTranslator';
import { SecondaryMenuStyleTranslator as SecondaryMenuStyleTranslator } from '../../translation/navigation/style/SecondaryMenuStyleTranslator';
import { SecondaryMenuSectionTranslator as SecondaryMenuSectionTranslator } from '../../translation/navigation/section/SecondaryMenuSectionTranslator';
import { StandardAspectRatioTranslator } from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import { CustomAspectRatioTranslator } from '../../translation/aspectRatio/CustomAspectRatioTranslator';
import {ComplexColorTranslator} from '../../translation/color/ComplexColorTranslator';
import {ColorTranslator} from '../../translation/color/ColorTranslator';

import SelectEventSchema = require('../../schema/common/listener/event/SelectEvent');

import SignoutActionSchema = require('../../schema/common/listener/action/SignoutAction');
import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');
import { Listener as ListenerDto } from '../../dto/common/Listener';

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('SecondaryMenuSection Translator tests', () => {
	var translation,
		translator: SecondaryMenuSectionTranslator,
		service: TranslationService;

	translator = new SecondaryMenuSectionTranslator({});
	service = new TranslationService();
	service.registerTranslator(SecondaryMenuSectionTranslator);
	service.registerTranslator(SecondaryMenuStyleTranslator);
	service.registerTranslator(ListenerTranslator);
	service.registerTranslator(ColoredComponentStyleTranslator);
	service.registerTranslator(ImageTranslator);
	service.registerTranslator(LabelTranslator);
	service.registerTranslator(ImageStyleTranslator);
	service.registerTranslator(ComponentStyleTranslator);
	service.registerTranslator(ComponentTranslator);
	service.registerTranslator(FieldContentTranslator);
	service.registerTranslator(ComponentControlsTranslator);
	service.registerTranslator(BaseFieldTranslator);
	service.registerTranslator(LabelStyleTranslator);
	service.registerTranslator(LabelTranslator);
	service.registerTranslator(LabelContentTranslator);
	service.registerTranslator(SizedComponentStyleTranslator);
	service.registerTranslator(StandardAspectRatioTranslator);
	service.registerTranslator(CustomAspectRatioTranslator);
	service.registerTranslator(ComplexColorTranslator);
	service.registerTranslator(ColorTranslator);

	describe('GIVEN: An appstudio SecondaryMenuSection with no properties', () => {

		var source = <SecondaryMenuSectionDto>AppstudioModelFactory.create('SecondaryMenuSection');

		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it('THEN: Translation should be a valid secondaryMenu container', () => {
				expect(translation).to.exist;
				expect(translation._metadata).to.equal(ContainerSchema.ID);
				expect(translation.layout.type).to.equal('Relative');
				expect(translation.width).to.equal('fillParent');
				expect(translation.height).to.equal('wrapContent');
				expect(translation.visible).to.equal('true');
				expect(translation.style.padding).to.equal('0 0 0 0');
				expect(translation.style.margin).to.equal('0 0 0 0');
				expect(translation.style.backgroundColor.value).to.equal('#00000000');
				expect(translation.items).to.exist;
				expect(translation.listeners).to.exist;
			});
		});

	});

	describe('GIVEN: An appstudio SecondaryMenuSection with style', () => {
		let backgroundColor = {
			_metadata: ColorSchema.ID,
			value: '#BBBBBBBB'
		};

		var padding = '1 1 1 1',
			margin = '1 1 1 1',
			style = AppstudioModelFactory.create('ColoredComponentStyle', {
				margin: AppstudioModelFactory.create('BoxParams', {
					top: 1,
					right: 1,
					bottom: 1,
					left: 1
				}),
				padding: AppstudioModelFactory.create('BoxParams', {
					top: 1,
					right: 1,
					bottom: 1,
					left: 1
				}),
				backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				}
			}),
			source = <any>AppstudioModelFactory.create('SecondaryMenuSection', {
				settings: AppstudioModelFactory.create('SecondaryMenuSectionSettings', {
					style: style
				})

			});

		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it('THEN: the width property should be translated to fillParent', () => {
				expect(translation.width).to.equal('fillParent');
			});
			it('THEN: the width property should be translated to wrapContent', () => {
				expect(translation.height).to.equal('wrapContent');
			});
			it('THEN: the padding property should be translated to ' + padding, () => {
				expect(translation.style.padding).to.equal(padding);
			});
			it('THEN: the margin property should be translated to ' + margin, () => {
				expect(translation.style.margin).to.equal(margin);
			});
			it('THEN: the backgroundColor property should be translated to ' + JSON.stringify(backgroundColor), () => {
				expect(translation.style.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.style.backgroundColor._metadata).to.equal(backgroundColor._metadata);
			});
		});

	});

	describe('GIVEN: An appstudio SecondaryMenuSection with an image', () => {
		let backgroundColor = {
			_metadata: ColorSchema.ID,
			value: '#BBBBBBBB'
		};
		var padding = '1 1 1 1',
			margin = '1 1 1 1',
			type = 'Image',
			value = 'test',
			scale = 'aspectFit',
			aspectRatio = 1,
			width = 1,
			height = 1,
			gravity = 'Left',
			image = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
				style: AppstudioModelFactory.create('ImageStyle', {
					aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
						value: '1:1'
					}),
					scale: 'aspectFit',
					gravity: 'Left',
					width: AppstudioModelFactory.create('Width', {
						value: AppstudioModelFactory.create('CustomWidth', {
							value: width
						})
					}),
					height: AppstudioModelFactory.create('Height', {
						value: AppstudioModelFactory.create('CustomHeight', {
							value: height
						})
					}),
					margin: AppstudioModelFactory.create('BoxParams', {
						top: 1,
						right: 1,
						bottom: 1,
						left: 1
					}),
					padding: AppstudioModelFactory.create('BoxParams', {
						top: 1,
						right: 1,
						bottom: 1,
						left: 1
					})
				}),
				content: AppstudioModelFactory.create('FieldContent', {
					value: 'test'
				}),
				controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
					visible: 'not true'
				})
			}),
			style = AppstudioModelFactory.create('ColoredComponentStyle', {
					margin: AppstudioModelFactory.create('BoxParams', {
						top: 1,
						right: 1,
						bottom: 1,
						left: 1
					}),
					padding: AppstudioModelFactory.create('BoxParams', {
						top: 1,
						right: 1,
						bottom: 1,
						left: 1
					}),
					backgroundColor: {
						_metadata: ComplexColorSchema.ID,
						value: backgroundColor
					}
			}),
			source = <any>AppstudioModelFactory.create('SecondaryMenuSection', {
				setting: AppstudioModelFactory.create('SecondaryMenuSectionSettings',{
					style: style
				})

			}),
			translatedSection;
			source.image = [image];

		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						translatedSection = translation.items[0];
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it ('THEN: the width property should be translated to fillParent', () => {
				expect(translation.width).to.equal('fillParent');
			});
			it ('THEN: the width property should be translated to wrapContent', () => {
				expect(translation.height).to.equal('wrapContent');
			});
			it ('THEN: the padding property should be translated to ' + padding, () => {
				expect(translation.style.padding).to.equal(padding);
			});
			it ('THEN: the margin property should be translated to ' + margin, () => {
				expect(translation.style.margin).to.equal(margin);
			});
			it ('THEN: the backgroundColor property should be translated to ' + JSON.stringify(backgroundColor), () => {
				expect(translation.style.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.style.backgroundColor._metadata).to.equal(backgroundColor._metadata);
			});
			it ('THEN: the image type property should be translated to ' + type, () => {
				expect(translatedSection.type).to.equal(type);
			});
			it ('THEN: the image value property should be translated to ' + value, () => {
				expect(translatedSection.value).to.equal(value);
			});
			it ('THEN: the image scale property should be translated to ' + scale, () => {
				expect(translatedSection.scale).to.equal(scale);
			});
			it ('THEN: the image aspectRatio property should be translated to ' + aspectRatio, () => {
				expect(translatedSection.aspectRatio).to.equal(aspectRatio);
			});
			it ('THEN: the image width property should be translated to ' + width, () => {
				expect(translatedSection.width).to.equal(width);
			});
			it ('THEN: the image height property should be translated to ' + height, () => {
				expect(translatedSection.height).to.equal(height);
			});
			it ('THEN: the image gravity property should be translated to ' + gravity, () => {
				expect(translatedSection.gravity).to.equal(gravity);
			});
			it ('THEN: the image style padding property should be translated to ' + padding, () => {
				expect(translatedSection.style.padding).to.equal(padding);
			});
			it ('THEN: the image style margin property should be translated to ' + margin, () => {
				expect(translatedSection.style.margin).to.equal(margin);
			});
		});

	});

	describe('GIVEN: An appstudio SecondaryMenuSection with controls', () => {
		var visible = 'not true',
			source = <SecondaryMenuSectionDto>AppstudioModelFactory.create('SecondaryMenuSection', {
				settings: AppstudioModelFactory.create('SecondaryMenuSectionSettings', {

					controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
						visible: visible
					})
				})
			}),
			selectEvent = ClientAppModelFactory.create(SelectEventSchema.ID),
			select = 'Select',
			signoutAction = ClientAppModelFactory.create(SignoutActionSchema.ID),
			signout = 'Signout',
			listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener',{
				event: selectEvent,
				action: signoutAction
			});

			source.settings.controls.listeners.push(listener);
		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it ('THEN: visible property should be translated to ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});
			it ('THEN: the listener event type property should be translated as ' + select, () => {
				expect(translation.listeners[0].event.type).to.equal(select);
			});
			it ('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.listeners[0].action.type).to.equal(signout);
			});
		});

	});
});