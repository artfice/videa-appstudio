import chaiLib = require('chai');
const expect = chaiLib.expect;

import {BaseFieldTranslator as BaseFieldTranslator} from '../../translation/field/BaseFieldTranslator';
import {LabelTranslator as LabelTranslator} from '../../translation/field/LabelTranslator';
import {LabelStyleTranslator as LabelStyleTranslator} from '../../translation/field/style/LabelStyleTranslator';
import {LabelContentTranslator as LabelContentTranslator} from '../../translation/field/content/LabelContentTranslator';
import {FieldContentTranslator as FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentTranslator as ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {ListenerTranslator as ListenersTranslator} from '../../translation/ListenerTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {Label as AppstudioLabelDto} from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';

describe('Label Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(FieldContentTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(BaseFieldTranslator);
	translationService.registerTranslator(ListenersTranslator);
	translationService.registerTranslator(LabelStyleTranslator);
	translationService.registerTranslator(LabelTranslator);
	translationService.registerTranslator(LabelContentTranslator);

	describe('GIVEN: a Label', () => {
		var source = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
            style: AppstudioModelFactory.create('LabelStyle'),
            content: AppstudioModelFactory.create('LabelContent'),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls')
        });

		describe('WHEN: translated', () => {
			var translator = new LabelTranslator(),
				translation;
			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should not be translated', () => {
				expect(translation.value).to.equal(undefined);
			});
			it ('THEN: the width property should not be translated', () => {
				expect(translation.width).to.equal(undefined);
			});
			it ('THEN: the height property should not be translated', () => {
				expect(translation.height).to.equal(undefined);
			});
			it ('THEN: the gravity property should not be translated', () => {
				expect(translation.gravity).to.equal(undefined);
			});
			it ('THEN: the visible property should be translated as true', () => {
				expect(translation.visible).to.equal('true');
			});
			it ('THEN: the listeners property should be translated as []', () => {
				expect(translation.listeners.length).to.equal(0);
			});
		});
	});

	describe('GIVEN: an Label with base style and value', () => {
		var source = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
            style: AppstudioModelFactory.create('LabelStyle', {
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				}),
                gravity: 'Left'
            }),
            content: AppstudioModelFactory.create('LabelContent', {
                value: 'test',
				maxLines: 2,
				numberOfLines: 1
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        });

		describe('WHEN: translated', () => {
			var translator = new LabelTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should be translated', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the value property should be translated', () => {
				expect(translation.value).to.equal('test');
			});
			it ('THEN: the numberOfLines property should be translated', () => {
				expect(translation.numberOfLines).to.equal(1);
			});
			it ('THEN: the maxLines property should be translated', () => {
				expect(translation.maxLines).to.equal(2);
			});
			it ('THEN: the width property should be translated', () => {
				expect(translation.width).to.equal(1);
			});			
			it ('THEN: the height property should be translated', () => {
				expect(translation.height).to.equal(1);
			});			
			it ('THEN: the gravity property should be translated', () => {
				expect(translation.gravity).to.equal('Left');
			});			
			it ('THEN: the visible property should be translated', () => {
				expect(translation.visible).to.equal('not true');
			});			
		});
	});
});