import chai = require('chai');
const expect = chai.expect;
import dotenv = require('dotenv');
dotenv.config();

import {AdvanceComponentTranslator} from '../../translation/component/AdvanceComponentTranslator';
import {AdvanceComponent as AdvanceComponentDto} from '../../dto/app/configuration/uiconfig/screen/component/AdvanceComponent';

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

describe('Advance Component Translation Tests', () => {

	var translationService = new TranslationService();


	describe('GIVEN: Advance Component with no content', () => {

		var advanceComponent = <AdvanceComponentDto>AppstudioModelFactory.create('AdvanceComponent');

		describe('WHEN: translated', () => {
			var translator = new AdvanceComponentTranslator(),
				translation;

			before((done) => {

				translator.translate('videa', translationService, advanceComponent).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be undefined', () => {
				expect(translation).to.not.exist;
			});
		});


	});

	describe('GIVEN: Advance Component with invalid json content', () => {

		var advanceComponent = <AdvanceComponentDto>AppstudioModelFactory.create('AdvanceComponent', {
			content: 'sdfkjhsdfkjh'
		});

		describe('WHEN: translated', () => {
			var translator = new AdvanceComponentTranslator(),
				translation;

			before((done) => {

				translator.translate('videa', translationService, advanceComponent).then(
					(t) => {
						translation = t;
						done('translation should not have translated invalid json content');
					}
				).catch(
					(err) => {
						translation = err;
						done();
					}
				);
			});

			it ('THEN: the translation should result in an error', () => {
				expect(translation).to.be.an.instanceOf(Error);
			});

		});


	});
	describe('GIVEN: Advance Component with valid json content', () => {

		var advanceComponent = <AdvanceComponentDto> AppstudioModelFactory.create('AdvanceComponent', {
            customData: {
                content: '{"a": 1, "b": 2}'
            }
		});

		describe('WHEN: translated', () => {
			var translator = new AdvanceComponentTranslator(),
				translation;

			before((done) => {

				translator.translate('videa', translationService, advanceComponent).then(
					(t) => {
						translation = t;
						done();
					}
				).catch(
					(err) => {
						done(err);
					}
				);
			});

			it ('THEN: the translation should be a valid json object', () => {
				expect(translation).to.exist;
				expect(translation.a).to.exist;
				expect(translation.a).to.equal(1);
				expect(translation.b).to.exist;
				expect(translation.b).to.equal(2);
			});

		});


	});

});
