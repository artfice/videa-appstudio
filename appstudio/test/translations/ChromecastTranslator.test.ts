///<reference path="../../../typings/index.d.ts"/>

import { expect } from 'chai';

import { ChromecastTranslator } from '../../translation/chromecast/ChromecastTranslator';
import { ChromecastThemeTranslator } from '../../translation/chromecast/ChromecastThemeTranslator';
import { ChromecastTitleTranslator } from '../../translation/chromecast/ChromecastTitleTranslator';
import { ChromecastReceiverTranslator } from '../../translation/chromecast/ChromecastReceiverTranslator';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { Chromecast as ChromecastDto } from '../../dto/app/configuration/Chromecast';
import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import { Title as ChromecastTitleDto } from '../../dto/app/configuration/chromecast/Title';
import { Theme as ChromecastThemeDto } from '../../dto/app/configuration/chromecast/Theme';
import { Receiver as ChromecastReceiverDto } from '../../dto/app/configuration/chromecast/Receiver';

import AlignmentSchema = require('../../schema/common/Alignment');

describe('Chromecast Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(ChromecastThemeTranslator);
    translationService.registerTranslator(ChromecastTitleTranslator);
    translationService.registerTranslator(ChromecastReceiverTranslator);
    translationService.registerTranslator(OpenSansTranslator);

    const translator = new ChromecastTranslator();

    describe('GIVEN: an empty chromecast', () => {
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });
        });
    });

    describe('GIVEN: a chromecast with appId', () => {
        const appId = 'appId';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast', {
            appId: appId
        });

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the appId property should be translated to ' + appId, () => {
                expect(translation.appId).to.equal(appId);
            });
        });
    });

    describe('GIVEN: a chromecast with theme and accentColor', () => {
        const accentColor = '#E2E2E2';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const chromecastTheme = <ChromecastThemeDto>AppstudioModelFactory.create('ChromecastTheme', {
            accentColor: AppstudioModelFactory.create('Color', {
                value: accentColor
            })
        });

        source.theme = chromecastTheme;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the theme\'s accentColor property should be translated to ' + accentColor, () => {
                expect(translation.theme.accentColor.value).to.equal(accentColor);
            });
        });
    });
    describe('GIVEN: a chromecast with theme and secondaryColor', () => {
        const secondaryColor = '#E2E2E2';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const chromecastTheme = <ChromecastThemeDto>AppstudioModelFactory.create('ChromecastTheme', {
            secondaryColor: AppstudioModelFactory.create('Color', {
                value: secondaryColor
            })
        });

        source.theme = chromecastTheme;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the theme\'s secondaryColor property should be translated to ' + secondaryColor, () => {
                expect(translation.theme.secondaryColor.value).to.equal(secondaryColor);
            });
        });
    });
    describe('GIVEN: a chromecast with theme and backgroundColor', () => {
        const backgroundColor = '#E2E2E2';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const chromecastTheme = <ChromecastThemeDto>AppstudioModelFactory.create('ChromecastTheme', {
            backgroundColor: AppstudioModelFactory.create('Color', {
                value: backgroundColor
            })
        });

        source.theme = chromecastTheme;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the theme\'s backgroundColor property should be translated to ' + backgroundColor, () => {
                expect(translation.theme.backgroundColor.value).to.equal(backgroundColor);
            });
        });
    });
    describe('GIVEN: a chromecast with theme and foregroundColor', () => {
        const foregroundColor = '#E2E2E2';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const chromecastTheme = <ChromecastThemeDto>AppstudioModelFactory.create('ChromecastTheme', {
            foregroundColor: AppstudioModelFactory.create('Color', {
                value: foregroundColor
            })
        });

        source.theme = chromecastTheme;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the theme\'s foregroundColor property should be translated to ' + foregroundColor, () => {
                expect(translation.theme.foregroundColor.value).to.equal(foregroundColor);
            });
        });
    });

    describe('GIVEN: a chromecast with title and font family', () => {
        const weight = 'Regular';
        const family = 'OpenSans-' + weight;
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            weight: weight
        });
        const chromecastTitle = <ChromecastTitleDto>AppstudioModelFactory.create('ChromecastTitle', {
            font: font
        });

        source.title = chromecastTitle;
        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the title font family property should be translated to ' + family, () => {
                expect(translation.title.font.family).to.equal(family);
            });
        });
    });

    describe('GIVEN: a chromecast with title and font color', () => {
        const color = '#E2E2E2';
        const colorObject = ClientAppModelFactory.create('Color', {
            value: color
        });
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            color: colorObject
        });
        const chromecastTitle = <ChromecastTitleDto>AppstudioModelFactory.create('ChromecastTitle', {
            font: font
        });

        source.title = chromecastTitle;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the title font color property should be translated to ' + color, () => {
                expect(translation.title.font.color.value).to.equal(color);
            });
        });
    });

    describe('GIVEN: a chromecast with title and font size', () => {
        const size = 14;
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            size: size
        });
        const chromecastTitle = <ChromecastTitleDto>AppstudioModelFactory.create('ChromecastTitle', {
            font: font
        });

        source.title = chromecastTitle;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the title font size property should be translated to ' + size, () => {
                expect(translation.title.font.size).to.equal(size);
            });
        });
    });

    describe('GIVEN: a chromecast with title and font alignment', () => {
        const alignment = 'Center';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment})
        });
        const chromecastTitle = <ChromecastTitleDto>AppstudioModelFactory.create('ChromecastTitle', {
            font: font
        });

        source.title = chromecastTitle;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the title font alignment property should be translated to ' + alignment, () => {
                expect(translation.title.font.alignment).to.equal(alignment);
            });
        });
    });

    describe('GIVEN: a chromecast with receiver and font family', () => {
        const weight = 'Regular';
        const family = 'OpenSans-' + weight;
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            weight: weight
        });
        const chromecastReceiver = <ChromecastReceiverDto>AppstudioModelFactory.create('ChromecastReceiver', {
            font: font
        });

        source.receiver = chromecastReceiver;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the receiver font family property should be translated to ' + family, () => {
                expect(translation.receiver.font.family).to.equal(family);
            });
        });
    });

    describe('GIVEN: a chromecast with receiver and font weight', () => {
        const weight = 'Regular';
        const family = 'OpenSans-' + weight;
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            weight: weight
        })
        const chromecastReceiver = <ChromecastReceiverDto>AppstudioModelFactory.create('ChromecastReceiver', {
            font: font
        });

        source.receiver = chromecastReceiver;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the receiver font family property should be translated to ' + family, () => {
                expect(translation.receiver.font.family).to.equal(family);
            });
        });
    });

    describe('GIVEN: a chromecast with receiver and font color', () => {
        const color = '#E2E2E2';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            color: {
                value: color
            }
        });
        const chromecastReceiver = <ChromecastReceiverDto>AppstudioModelFactory.create('ChromecastReceiver', {
            font: font
        });

        source.receiver = chromecastReceiver;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the receiver font color property should be translated to ' + color, () => {
                expect(translation.receiver.font.color.value).to.equal(color);
            });
        });
    });

    describe('GIVEN: a chromecast with receiver and font size', () => {
        const size = 14;
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            size: size
        });
        const chromecastReceiver = <ChromecastReceiverDto>AppstudioModelFactory.create('ChromecastReceiver', {
            font: font
        });

        source.receiver = chromecastReceiver;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the receiver font size property should be translated to ' + size, () => {
                expect(translation.receiver.font.size).to.equal(size);
            });
        });
    });

    describe('GIVEN: a chromecast with receiver and font alignment', () => {
        const alignment = 'Center';
        const source = <ChromecastDto>AppstudioModelFactory.create('Chromecast');
        const font = ClientAppModelFactory.create('OpenSans', {
            alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment})
        });
        const chromecastReceiver = <ChromecastReceiverDto>AppstudioModelFactory.create('ChromecastReceiver', {
            font: font
        });

        source.receiver = chromecastReceiver;

        describe('WHEN:  translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the receiver font alignment property should be translated to ' + alignment, () => {
                expect(translation.receiver.font.alignment).to.equal(alignment);
            });
        });
    });
});
