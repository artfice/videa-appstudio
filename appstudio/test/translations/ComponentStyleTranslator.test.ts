import chaiLib = require('chai');
const expect = chaiLib.expect;

import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {ComponentStyle as ComponentStyleDto} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';

describe('ComponentStyle Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty Component Style', () => {
		var padding = undefined,
			margin = undefined,
			border = undefined,
			source = <ComponentStyleDto>AppstudioModelFactory.create('ComponentStyle');

		describe('WHEN:  translated', () => {
			var translator = new ComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the margin property should be translated as ' + padding, () => {
				expect(translation.padding).to.equal(padding);
			});
			it ('THEN: the padding property should be translated as ' + margin, () => {
				expect(translation.margin).to.equal(margin);
			});
			it ('THEN: the border property should be translated as ' + border, () => {
				expect(translation.border).to.equal(border);
			});
		});
	});

	describe('GIVEN: a Component Style with margin, padding, border', () => {
		var margin = '1 1 1 1',
            padding = '1 1 1 1',
			radius = 1,
			thickness = 1,
			value = '#AAAAAA',
			source = <ComponentStyleDto>AppstudioModelFactory.create('ComponentStyle', {
            margin: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            padding: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            border: {
                radius: radius,
                thickness: thickness,
                color: {
                    value: value
                }
            }
		});

		describe('WHEN:  translated', () => {
			var translator = new ComponentStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the margin property should be translated as ' + padding, () => {
				expect(translation.padding).to.equal(padding);
			});
			it ('THEN: the padding property should be translated as ' + margin, () => {
				expect(translation.margin).to.equal(margin);
			});
			it ('THEN: the border radius property should be translated as ' + radius, () => {
				expect(translation.border.radius).to.equal(radius);
			});
			it ('THEN: the border thickness property should be translated as ' + thickness, () => {
				expect(translation.border.thickness).to.equal(thickness);
			});
			it ('THEN: the border color property should be translated as ' + value, () => {
				expect(translation.border.color.value).to.equal(value);
			});
		});
	});
});
