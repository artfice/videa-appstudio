import chaiLib = require('chai');
const expect = chaiLib.expect;

import {ComponentControls} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction} from '../../dto/common/listener/action/BaseAction';

import {DataViewHeading as AppStudioDataViewHeadingDto} from '../../dto/app/configuration/uiconfig/screen/dataview/DataViewHeading';

import {DataViewHeadingTranslator as DataViewHeadingTranslator} from '../../translation/dataview/heading/DataViewHeadingTranslator';
import {BaseCollectionTranslator as BaseCollectionTranslator} from '../../translation/dataview/collection/BaseCollectionTranslator';
import {BaseFieldStyleTranslator as BaseFieldStyleTranslator} from '../../translation/field/style/BaseFieldStyleTranslator';
import {FieldContentTranslator as FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentTranslator as ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {TileViewStyleTranslator as TileViewStyleTranslator} from '../../translation/dataview/style/TileViewStyleTranslator';
import {SizedComponentStyleTranslator as SizedComponentStyleTranslator} from '../../translation/component/SizedComponentStyleTranslator';
import {DataViewHeadingStyleTranslator as DataViewHeadingStyleTranslator} from '../../translation/dataview/style/DataViewHeadingStyleTranslator';
import {LabelTranslator as LabelTranslator} from '../../translation/field/LabelTranslator';
import {LabelStyleTranslator as LabelStyleTranslator} from '../../translation/field/style/LabelStyleTranslator';
import {LabelContentTranslator as LabelContentTranslator} from '../../translation/field/content/LabelContentTranslator';
import {ListenerTranslator as ListenerTranslator} from '../../translation/ListenerTranslator';


import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {ClientAppModelFactory as ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';
import {Label as AppstudioLabelDto} from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';

import {Listener} from "../../dto/common/Listener";
import {DataViewHeadingSettings} from "../../dto/app/configuration/uiconfig/screen/dataview/heading/DataViewHeadingSettings";

describe('DataViewHeading Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(BaseFieldStyleTranslator);
	translationService.registerTranslator(FieldContentTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(BaseCollectionTranslator);
	translationService.registerTranslator(TileViewStyleTranslator);
	translationService.registerTranslator(SizedComponentStyleTranslator);
	translationService.registerTranslator(DataViewHeadingStyleTranslator);
    translationService.registerTranslator(LabelStyleTranslator);
	translationService.registerTranslator(LabelTranslator);
	translationService.registerTranslator(LabelContentTranslator);
	translationService.registerTranslator(ListenerTranslator);

	describe('GIVEN: an empty DataViewHeading', () => {
		var source = <AppStudioDataViewHeadingDto>AppstudioModelFactory.create('DataViewHeading', {});

		describe('WHEN: translated', () => {
			var translator = new DataViewHeadingTranslator(),
				translation,
				width = undefined,
				height = undefined,
				items = [],
				layout = 'Relative',
				visible = 'true',
				listener = [],
				containerType = 'Container';

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
                        console.log(err);
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});

            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});

			it ('THEN: the width property should be translated as ' + width, () => {
				expect(translation.width).to.equal(width);
			});

            it ('THEN: the height property should be translated as ' + height, () => {
				expect(translation.height).to.equal(height);
			});
            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});

            it ('THEN: the items property should have no items', () => {
				expect(translation.items.length).to.equal(items.length);
			});

			it ('THEN: the listeners property should have no listeners', () => {
				expect(translation.listeners.length).to.equal(listener.length);
			});

            it ('THEN: the layout type property should be ' + layout, () => {
				expect(translation.layout.type).to.equal(layout);
			});

            it ('THEN: the Component type property should be ' + containerType, () => {
				expect(translation.type).to.equal(containerType);
			});

            // it ('THEN: the Component stype type property should be ' + styleType, () => {
			// 	expect(translation.style.type).to.equal(styleType);
			// });
            it ('THEN: the Component style property should be ' + undefined, () => {
				expect(translation.style).to.equal(undefined);
			});
		});
	});

	describe('GIVEN: a DataViewHeading with style', () => {
		var source = <any>AppstudioModelFactory.create('DataViewHeading', {}),
			margin = '1 1 1 1',
			padding = '2 2 2 2',
			radius = 1,
			thickness = 1,
			color = '#AAAAAA',
			width = 10,
			height = 20,
			layout = 'Relative',
			visible = 'true',
			listener = [],
			items = [],
			containerType = 'Container',
			styleType = 'ContainerStyle',
			style = AppstudioModelFactory.create('DataViewHeadingStyle', {
				margin: AppstudioModelFactory.create('BoxParams', {
					top: 1,
					right: 1,
					bottom: 1,
					left: 1
				}),
				padding: AppstudioModelFactory.create('BoxParams', {
					top: 2,
					right: 2,
					bottom: 2,
					left: 2
				}),
                border: {
                    radius: radius,
                    thickness: thickness,
                    color: {
                        value: color
                    }
                },
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				})
			}),
			controls = AppstudioModelFactory.create('ComponentControls'),
			settings = AppstudioModelFactory.create('DataViewHeadingSettings', {
				style : style,
				controls : controls
			});

			source.settings = settings;

		describe('WHEN: translated', () => {
			var translator = new DataViewHeadingTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
                        console.log(err);
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});

            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});

			it ('THEN: the width property should be translated as ' + width, () => {
				expect(translation.width).to.equal(width);
			});

            it ('THEN: the height property should be translated as ' + height, () => {
				expect(translation.height).to.equal(height);
			});
            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});

            it ('THEN: the items property should have no items', () => {
				expect(translation.items.length).to.equal(items.length);
			});

			it ('THEN: the listeners property should have no listeners', () => {
				expect(translation.listeners.length).to.equal(listener.length);
			});

            it ('THEN: the layout type property should be ' + layout, () => {
				expect(translation.layout.type).to.equal(layout);
			});

            it ('THEN: the Component type property should be ' + containerType, () => {
				expect(translation.type).to.equal(containerType);
			});

            it ('THEN: the Component style type property should be ' + styleType, () => {
				expect(translation.style.type).to.equal(styleType);
			});

			it ('THEN: the style border radius property should be translated as ' + radius, () => {
				expect(translation.style.border.radius).to.equal(radius);
			});
			it ('THEN: the style border thickness property should be translated as ' + thickness, () => {
				expect(translation.style.border.thickness).to.equal(thickness);
			});
			it ('THEN: the style border color property should be translated as ' + color, () => {
				expect(translation.style.border.color.value).to.equal(color);
			});

			it ('THEN: the style margin property should be translated as ' + margin, () => {
				expect(translation.style.margin).to.equal(margin);
			});

			it ('THEN: the style padding property should be translated as ' + padding, () => {
				expect(translation.style.padding).to.equal(padding);
			});
		});
	});

describe('GIVEN: a DataViewHeading with controls', () => {
		var source = <AppStudioDataViewHeadingDto>AppstudioModelFactory.create('DataViewHeading', {}),
			visible = '1',
			longSelect = 'LongSelect',
			signout = 'Signout',
			listener = ClientAppModelFactory.create('Listener', {
				event : ClientAppModelFactory.create('LongSelectEvent'),
				action: ClientAppModelFactory.create('Signout')
			}) as Listener<BaseEvent, BaseAction>,
			controls = AppstudioModelFactory.create('ComponentControls', {
				visible: visible
			}) as ComponentControls<BaseEvent, BaseAction>,
			settings = AppstudioModelFactory.create('DataViewHeadingSettings') as DataViewHeadingSettings;

			controls.listeners.push(listener);
			settings.controls = controls;
			source.settings = settings;

		describe('WHEN: translated', () => {
			var translator = new DataViewHeadingTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
                        console.log(err);
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});

            it ('THEN: the visible property should be translated as ' + visible, () => {
				expect(translation.visible).to.equal(visible);
			});

			it ('THEN: the listener event type property should be translated as ' + longSelect, () => {
				expect(translation.listeners[0].event.type).to.equal(longSelect);
			});

			it ('THEN: the listener action type property should be translated as ' + signout, () => {
				expect(translation.listeners[0].action.type).to.equal(signout);
			});
		});
	});

	describe('GIVEN: a DataViewHeading with label', () => {
		var source = <AppStudioDataViewHeadingDto>AppstudioModelFactory.create('DataViewHeading', {}),
			width = 1,
			height = 1,
			gravity = 'Left',
			value = 'test',
			visible = 'not true',
			labelSource = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
				style: AppstudioModelFactory.create('LabelStyle', {
					width: AppstudioModelFactory.create('Width', {
						value: AppstudioModelFactory.create('CustomWidth', {
							value: width
						})
					}),
					height: AppstudioModelFactory.create('Height', {
						value: AppstudioModelFactory.create('CustomHeight', {
							value: height
						})
					}),
					gravity: gravity
				}),
				content: AppstudioModelFactory.create('LabelContent', {
					value: value
				}),
				controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
					visible: visible
				})
			});

			source.text.push(labelSource);

		describe('WHEN: translated', () => {
			var translator = new DataViewHeadingTranslator(),
				translation,
				label;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						label = translation.items[0];
						done();
					},
                    (err) => {
                        console.log(err);
						done();
					}
				);
			});

			it ('THEN: the label\'s value property should be translated as ' + value, () => {
				expect(label.value).to.equal(value);
			});
			it ('THEN: the label\'s width property should be translated as ' + width, () => {
				expect(label.width).to.equal(width);
			});
			it ('THEN: the label\'s height property should be translated as ' + height, () => {
				expect(label.height).to.equal(height);
			});
			it ('THEN: the label\'s gravity property should be translated as ' + gravity, () => {
				expect(label.gravity).to.equal(gravity);
			});
			it ('THEN: the label\'s visible property should be translated as  ' + visible, () => {
				expect(label.visible).to.equal(visible);
			});
		});
	});
});