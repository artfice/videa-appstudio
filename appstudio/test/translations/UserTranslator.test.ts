import chai = require('chai');
const expect = chai.expect;

import { User as AppstudioUserDto } from '../../dto/app/configuration/User';
import { BaseProvider as BaseProvider } from '../../dto/app/configuration/user/BaseProvider';
import { User as UserDto } from '../../dto/clientapp/app/User';

import { ITranslationService } from '../../interface/ITranslationService';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { UserTranslator } from '../../translation/user/UserTranslator';
import { MpxProviderTranslator } from '../../translation/user/MpxProviderTranslator';
import { VideaProviderTranslator } from '../../translation/user/VideaProviderTranslator';
import { CustomDataTranslator } from '../../translation/CustomDataTranslator';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import AppstudioUserSchema = require('../../schema/appstudio/app/configuration/User');
import UserSchema = require('../../schema/clientapp/app/User');
import NoDataSchema = require('../../schema/common/NoData');
import MpxProviderSchema = require('../../schema/appstudio/app/configuration/user/MpxProvider');
import VideaProviderSchema = require('../../schema/appstudio/app/configuration/user/VideaProvider');
import CustomDataSchema = require('../../schema/common/CustomData');


describe('User Translator Tests', () => {
    const accountId: string = 'videa';

    const translationService: ITranslationService = new TranslationService();
    translationService.registerTranslator(UserTranslator, null);
    translationService.registerTranslator(MpxProviderTranslator, null);
    translationService.registerTranslator(VideaProviderTranslator, null);
    translationService.registerTranslator(CustomDataTranslator, null);

    describe('GIVEN: an empty User', () => {
        const appstudioUser = <AppstudioUserDto<BaseProvider>>AppstudioModelFactory.create(AppstudioUserSchema.ID);

        describe('WHEN: translated', () => {
            let translation: UserDto;
            before((done) => {
                new UserTranslator().translate(accountId, translationService, appstudioUser).then(
                    (userDto: UserDto) => {
                        translation = userDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation is NULL', () => {
                expect(translation).to.equal(null);
            });
        });
    });

    describe('GIVEN: a User with none provider', () => {
        const appstudioUser = <AppstudioUserDto<BaseProvider>>AppstudioModelFactory.create(AppstudioUserSchema.ID);
        appstudioUser.provider = <any>AppstudioModelFactory.create(NoDataSchema.ID);

        describe('WHEN: translated', () => {
            let translation: UserDto;
            before((done) => {
                new UserTranslator().translate(accountId, translationService, appstudioUser).then(
                    (userDto: UserDto) => {
                        translation = userDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation is NULL', () => {
                expect(translation).to.equal(null);
            });
        });
    });

    describe('GIVEN: a User with MPX provider', () => {
        const appstudioUser = <AppstudioUserDto<BaseProvider>>AppstudioModelFactory.create(AppstudioUserSchema.ID);
        appstudioUser.provider = <any>AppstudioModelFactory.create(MpxProviderSchema.ID, {
            accountId: 'myAccountId',
            pid: 'myPid'
        });

        describe('WHEN: translated', () => {
            let translation: UserDto;
            before((done) => {
                new UserTranslator().translate(accountId, translationService, appstudioUser).then(
                    (userDto: UserDto) => {
                        translation = userDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation has User metadata AND Mpx provider properties', () => {
                expect(translation).to.have.property('_metadata', UserSchema.ID);
                expect(translation).to.have.property('provider', 'MPX');
                expect(translation).to.have.property('accountId', 'myAccountId');
                expect(translation).to.have.property('pid', 'myPid');
            });
        });
    });

    describe('GIVEN: a User with Videa provider', () => {
        const appstudioUser = <AppstudioUserDto<BaseProvider>>AppstudioModelFactory.create(AppstudioUserSchema.ID);
        appstudioUser.provider = <any>AppstudioModelFactory.create(VideaProviderSchema.ID, {
            accountId: 'anyGivenAccountId',
            pid: 'anyGivenPid'
        });

        describe('WHEN: translated', () => {
            let translation: UserDto;
            before((done) => {
                new UserTranslator().translate(accountId, translationService, appstudioUser).then(
                    (userDto: UserDto) => {
                        translation = userDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation has User metadata AND Videa provider properties', () => {
                expect(translation).to.have.property('_metadata', UserSchema.ID);
                expect(translation).to.have.property('provider', 'Videa');
                expect(translation).to.not.have.property('accountId');
                expect(translation).to.not.have.property('pid');
            });
        });
    });

    describe('GIVEN: a User with Custom provider', () => {
        const provider = <any>AppstudioModelFactory.create(CustomDataSchema.ID);
        provider.content = '{ "myProvider": "myCustomProvider", "oneProperty": "oneValue", "number": 7 }';

        const appstudioUser = <AppstudioUserDto<BaseProvider>>AppstudioModelFactory.create(AppstudioUserSchema.ID);
        appstudioUser.provider = provider;

        describe('WHEN: translated', () => {
            let translation: UserDto;
            before((done) => {
                new UserTranslator().translate(accountId, translationService, appstudioUser).then(
                    (userDto: UserDto) => {
                        translation = userDto;
                        done();
                    }, (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation has User metadata AND custom provider properties', () => {
                expect(translation).to.have.property('_metadata', UserSchema.ID);
                expect(translation).to.not.have.property('provider');
                expect(translation).to.have.property('myProvider', 'myCustomProvider');
                expect(translation).to.have.property('oneProperty', 'oneValue');
                expect(translation).to.have.property('number', 7);
            });
        });
    });
});