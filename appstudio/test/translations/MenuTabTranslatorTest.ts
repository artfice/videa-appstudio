import chai = require('chai');
chai.use(require('chai-shallow-deep-equal'));
const expect = chai.expect;
import _ = require('lodash');
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';
import TabSchema = require('../../schema/appstudio/app/configuration/uiconfig/navigation/Tab');
import {Tabs as TabsDto} from '../../dto/common/navigation/tabs/Tabs';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {LabelTranslator} from '../../translation/field/LabelTranslator';
import {MenuTabTranslator} from '../../translation/navigation/menu/tab/MenuTabTranslator';
import {ImageTranslator} from '../../translation/field/ImageTranslator';
import {ImageStyleTranslator} from '../../translation/field/style/ImageStyleTranslator';
import {LabelStyleTranslator} from '../../translation/field/style/LabelStyleTranslator';
import {FieldContentTranslator} from '../../translation/field/content/FieldContentTranslator';
import {LabelContentTranslator} from '../../translation/field/content/LabelContentTranslator';
import {BaseFieldTranslator} from '../../translation/field/BaseFieldTranslator';
import {ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {SizedComponentStyleTranslator} from '../../translation/component/SizedComponentStyleTranslator';
import {ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {StandardAspectRatioTranslator} from '../../translation/aspectRatio/StandardAspectRatioTranslator';

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('MenuTab Translator tests', () => {
	let translation = null;
	const translator = new MenuTabTranslator();
	const service = new TranslationService();
	service.registerTranslator(ComponentControlsTranslator);
	service.registerTranslator(StandardAspectRatioTranslator);
	service.registerTranslator(ComponentStyleTranslator);
	service.registerTranslator(ComponentTranslator);
	service.registerTranslator(ImageTranslator);
	service.registerTranslator(LabelTranslator);
	service.registerTranslator(ImageStyleTranslator);
	service.registerTranslator(LabelStyleTranslator);
	service.registerTranslator(ComponentTranslator);
	service.registerTranslator(FieldContentTranslator);
	service.registerTranslator(BaseFieldTranslator);
	service.registerTranslator(LabelContentTranslator);
	service.registerTranslator(SizedComponentStyleTranslator);

	describe('GIVEN: An appstudio MenuTab with no properties', () => {

		var source = <any>AppstudioModelFactory.create('MenuTab', {});

		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it ('THEN: Translation should be a valid sections container', () => {
                expect(translation.type).to.equal('Tab');
                expect(translation.width).to.equal('fillParent');
                expect(translation.height).to.equal('wrapContent');
                expect(translation.visible).to.equal('true');
                expect(translation.label).to.equal(null);
                expect(translation.icon).to.equal(null);
                expect(translation.style).to.equal(null);
                expect(translation.listeners.length).to.equal(0);
			});
		});

	});

	describe('GIVEN: An appstudio MenuTab with label', () => {

		var label = <any>AppstudioModelFactory.create('Label', {
            style: AppstudioModelFactory.create('LabelStyle', {
                
                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				}),
                gravity: 'Left'
            }),
            content: AppstudioModelFactory.create('LabelContent', {
                value: 'test'
            }),
            controls: <any>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        }),
		source = <any>AppstudioModelFactory.create('MenuTab', {
			label: label
		});

		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it ('THEN: Translation should be a valid sections container', () => {
                expect(translation.label.value).to.equal('test');
                expect(translation.label.width).to.equal(1);
                expect(translation.label.height).to.equal(1);
                expect(translation.label.gravity).to.equal('Left');
                expect(translation.label.visible).to.equal('not true');
			});
		});

	});

	describe('GIVEN: An appstudio MenuTab with image', () => {

		var image = <any>AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: '1:1'
				}),
                scale: 'aspectFit',
                gravity: 'Left',

                width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: 1
					})
				}),
                height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: 1
					})
				})
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: 'test'
            }),
            controls: <any>AppstudioModelFactory.create('ComponentControls', {
                visible: 'not true'
            })
        }),
		source = <any>AppstudioModelFactory.create('MenuTab', {
			icon: image
		});

		describe('WHEN:  translated', () => {

			before((done) => {
				translator.translate('videa', service, source).then(
					(t) => {
						translation = t;
						done();
					}
				).catch((err) => {
					print(err);
					done(err);
				});
			});

			it ('THEN: Translation should be a valid image', () => {
                expect(translation.icon.value).to.equal('test');
                expect(translation.icon.width).to.equal(1);
                expect(translation.icon.height).to.equal(1);
                expect(translation.icon.gravity).to.equal('Left');
                expect(translation.icon.scale).to.equal('aspectFit');
                expect(translation.icon.aspectRatio).to.equal(1);
                expect(translation.icon.visible).to.equal('not true');
			});
		});

	});		
});