import chai = require('chai');
const expect = chai.expect;

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';
import {GoogleAnalyticTranslator} from '../../translation/uiConfig/analytic/GoogleAnalyticTranslator';
import {SystemEventTranslator} from '../../translation/uiConfig/analytic/systemEvents/SystemEventTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {Event as EventDto} from '../../dto/app/configuration/uiconfig/analytics/google/systemEvent/Event';
import {SystemEvents as SystemEventsDto} from '../../dto/app/configuration/uiconfig/analytics/google/SystemEvents';
import {GoogleAnalytics as GoogleAnalyticsDto} from '../../dto/app/configuration/uiconfig/analytics/google/GoogleAnalytics';
import GoogleAnalyticSchema = require('../../schema/appstudio/app/configuration/uiconfig/analytics/google/GoogleAnalytics');
import SystemEventSchema = require('../../schema/appstudio/app/configuration/uiconfig/analytics/google/SystemEvents');
import MobileGoogleAnalytics = require('../../schema/appstudio/app/mobile/uiconfig/analytics/MobileGoogleAnalytics');
import PlayEventSchema = require('../../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/PlayEvent');
import PercentageEventSchema = require('../../schema/appstudio/app/configuration/uiconfig/analytics/google/systemEvents/PercentageEvent');

describe('Google Analytic Translation Tests', () => {

    let translationService = new TranslationService();
    translationService.registerTranslator(SystemEventTranslator);

    describe('GIVEN: An empty Google analytic', () => {

        let googleAnalytic = <GoogleAnalyticsDto>AppstudioModelFactory.create(MobileGoogleAnalytics.ID);

        describe('WHEN: translated', () => {

            let translator = new GoogleAnalyticTranslator();
            let translation;

            before(() => {
                return translator.translate('videa', translationService, googleAnalytic).then(
                    (t) => {
                        translation = t;
                    });
            });

            it('THEN: the type property has translated', () => {
                expect(translation.type).to.equal('GoogleAnalytics');
            });
            it('THEN: the events property has translated', () => {
                expect(translation.events.length).to.equal(0);
            });
            it('THEN: the systemEvents property has translated', () => {
                expect(translation.systemEvents).to.equal(null);
            });
        });
    });

    describe('GIVEN: A Google analytic with screens', () => {

        let screenId = '7ca4d080-e178-11e6-8688-f9a7ebc801b4',
            googleAnalytic = <GoogleAnalyticsDto>AppstudioModelFactory.create(MobileGoogleAnalytics.ID, {
                screens: [
                    AppstudioModelFactory.create('GooglePage', {
                        screenId: screenId,
                        screenName: 's1'
                    })
                ]
            });

        describe('WHEN: translated', () => {

            let translator = new GoogleAnalyticTranslator();
            let translation;

            before(() => {
                return translator.translate('videa', translationService, googleAnalytic).then(
                    (t) => {
                        translation = t;
                    });
            });

            it('THEN: the screens property has translated', () => {
                expect(translation.screens.length).to.equal(1);
                expect(translation.screens[0].screenName).to.equal('s1');
            });
        });
    });

    describe('GIVEN: A Google analytic with events', () => {

        let result = 'a',
            googleAnalytic = <GoogleAnalyticsDto>AppstudioModelFactory.create(MobileGoogleAnalytics.ID, {
                events: [
                    AppstudioModelFactory.create('GoogleEvent', {
                        videaEventTag: 'a',
                        category: 'a',
                        action: 'a',
                        labels: '{model.series_name} - {model.title} | EP {model.episode}',
                        value: 'a',
                        custom: AppstudioModelFactory.create('CustomData', {
                        })
                    })
                ]
            });
        googleAnalytic.events[0].custom.content = '{\n  \"a\": 1\n}';

        describe('WHEN: translated', () => {

            let translator = new GoogleAnalyticTranslator();
            let translation;

            before(() => {
                return translator.translate('videa', translationService, googleAnalytic).then(
                    (t) => {
                        translation = t;
                    });
            });

            it('THEN: the events property has translated', () => {
                expect(translation.events.length).to.equal(1);
                expect(translation.events[0].id).to.equal(result);
                expect(translation.events[0].category).to.equal(result);
                expect(translation.events[0].action).to.equal(result);
                expect(translation.events[0].value).to.equal(result);
                expect(translation.events[0].labels).to.equal('{model.series_name} - {model.title} | EP {model.episode}');
                expect(translation.events[0].custom.a).to.equal(1);
            });
        });
    });

    describe('GIVEN: A Google analytic with systemEvents enabled', () => {

        let result = 'a',
            label = '{model.series_name} - {model.title} | EP {model.episode}',
            googleAnalytic = <GoogleAnalyticsDto>AppstudioModelFactory.create(MobileGoogleAnalytics.ID, {
                systemEvents: AppstudioModelFactory.create('SystemEvent', {
                    videoPlayback: []
                })
            });
        googleAnalytic.systemEvents.signInSuccess = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: true,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.signInFailure = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: true,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.search = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: true,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.chromecastConnect = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: true,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.chromecastDisconnect = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: true,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });

        const custom = AppstudioModelFactory.create('CustomData');
        custom.content = '{\n  \"a\": 1\n}';
        googleAnalytic.systemEvents.signInSuccess.custom.push(custom);

        describe('WHEN: translated', () => {

            let translator = new GoogleAnalyticTranslator();
            let translation;

            before(() => {
                return translator.translate('videa', translationService, googleAnalytic).then(
                    (t) => {
                        translation = t;
                    });
            });

            it('THEN: the signInSuccess property has translated', () => {
                expect(translation.systemEvents.signInSuccess.category).to.equal(result);
                expect(translation.systemEvents.signInSuccess.action).to.equal(result);
                expect(translation.systemEvents.signInSuccess.value).to.equal(result);
                expect(translation.systemEvents.signInSuccess.labels).to.equal(label);
                expect(translation.systemEvents.signInSuccess.custom[0].a).to.equal(1);
            });
            it('THEN: the signInFailure property has translated', () => {
                expect(translation.systemEvents.signInFailure.category).to.equal(result);
                expect(translation.systemEvents.signInFailure.action).to.equal(result);
                expect(translation.systemEvents.signInFailure.value).to.equal(result);
                expect(translation.systemEvents.signInFailure.labels).to.equal(label);
            });
            it('THEN: the search property has translated', () => {
                expect(translation.systemEvents.search.category).to.equal(result);
                expect(translation.systemEvents.search.action).to.equal(result);
                expect(translation.systemEvents.search.value).to.equal(result);
                expect(translation.systemEvents.search.labels).to.equal(label);
            });
            it('THEN: the chromecastConnect property has translated', () => {
                expect(translation.systemEvents.chromecastConnect.category).to.equal(result);
                expect(translation.systemEvents.chromecastConnect.action).to.equal(result);
                expect(translation.systemEvents.chromecastConnect.value).to.equal(result);
                expect(translation.systemEvents.chromecastConnect.labels).to.equal(label);
            });
            it('THEN: the chromecastDisconnect property has translated', () => {
                expect(translation.systemEvents.chromecastDisconnect.category).to.equal(result);
                expect(translation.systemEvents.chromecastDisconnect.action).to.equal(result);
                expect(translation.systemEvents.chromecastDisconnect.value).to.equal(result);
                expect(translation.systemEvents.chromecastDisconnect.labels).to.equal(label);
            });
        });
    });

    describe('GIVEN: A Google analytic with systemEvents disabled', () => {

        let result = 'a',
            label = '{model.series_name} - {model.title} | EP {model.episode}',
            googleAnalytic = <GoogleAnalyticsDto>AppstudioModelFactory.create(MobileGoogleAnalytics.ID, {
                systemEvents: AppstudioModelFactory.create('SystemEvent', {
                    videoPlayback: []
                })
            });
        googleAnalytic.systemEvents.signInSuccess = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: false,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.signInFailure = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: false,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.search = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: false,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.chromecastConnect = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: false,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });
        googleAnalytic.systemEvents.chromecastDisconnect = AppstudioModelFactory.create('GoogleSystemEvent', {
            enabled: false,
            category: 'a',
            action: 'a',
            labels: '{model.series_name} - {model.title} | EP {model.episode}',
            value: 'a',
            custom: []
        });

        const custom = AppstudioModelFactory.create('CustomData');
        custom.content = '{\n  \"a\": 1\n}';
        googleAnalytic.systemEvents.signInSuccess.custom.push(custom);

        describe('WHEN: translated', () => {

            let translator = new GoogleAnalyticTranslator();
            let translation;

            before(() => {
                return translator.translate('videa', translationService, googleAnalytic).then(
                    (t) => {
                        translation = t;
                    });
            });

            it('THEN: the signInSuccess property has translated', () => {
                expect(translation.systemEvents.signInSuccess).to.equal(undefined);
            });
            it('THEN: the signInFailure property has translated', () => {
                expect(translation.systemEvents.signInFailure).to.equal(undefined);
            });
            it('THEN: the search property has translated', () => {
                expect(translation.systemEvents.search).to.equal(undefined);
            });
            it('THEN: the chromecastConnect property has translated', () => {
                expect(translation.systemEvents.chromecastConnect).to.equal(undefined);
            });
            it('THEN: the chromecastDisconnect property has translated', () => {
                expect(translation.systemEvents.chromecastDisconnect).to.equal(undefined);
            });
        });
    });

    describe('GIVEN: A Google analytic with systemEvents videoPlayback PlayEvent', () => {

        let result = 'a',
            label = '{model.series_name} - {model.title} | EP {model.episode}',
            googleAnalytic = <GoogleAnalyticsDto>AppstudioModelFactory.create(MobileGoogleAnalytics.ID, {
                systemEvents: AppstudioModelFactory.create('SystemEvent', {
                    videoPlayback: []
                })
            });
        const videoPlayback = AppstudioModelFactory.create('VideoPlaybackEvent', {
            event: ClientAppModelFactory.create(PlayEventSchema.ID, {
                category: 'a',
                action: 'a',
                labels: '{model.series_name} - {model.title} | EP {model.episode}',
                value: 'a',
                custom: []
            })
        });

        const custom = AppstudioModelFactory.create('CustomData');
        custom.content = '{\n  \"a\": 1\n}';
        videoPlayback.event.custom.push(custom);

        googleAnalytic.systemEvents.videoPlayback.push(videoPlayback);

        describe('WHEN: translated', () => {

            let translator = new GoogleAnalyticTranslator();
            let translation;

            before(() => {
                return translator.translate('videa', translationService, googleAnalytic).then(
                    (t) => {
                        translation = t;
                    });
            });

            it('THEN: the videoPlayback PlayEvent has translated', () => {
                expect(translation.systemEvents.videoPlayback[0].type).to.equal('Play');
                expect(translation.systemEvents.videoPlayback[0].category).to.equal(result);
                expect(translation.systemEvents.videoPlayback[0].action).to.equal(result);
                expect(translation.systemEvents.videoPlayback[0].value).to.equal(result);
                expect(translation.systemEvents.videoPlayback[0].labels).to.equal(label);
                expect(translation.systemEvents.videoPlayback[0].custom[0].a).to.equal(1);
            });
        });
    });

    describe('GIVEN: A Google analytic with systemEvents videoPlayback PercentageEvent', () => {

        let result = 'a',
            label = '{model.series_name} - {model.title} | EP {model.episode}',
            googleAnalytic = <GoogleAnalyticsDto>AppstudioModelFactory.create(MobileGoogleAnalytics.ID, {
                systemEvents: AppstudioModelFactory.create('SystemEvent', {
                    videoPlayback: []
                })
            });
        const videoPlayback = AppstudioModelFactory.create('VideoPlaybackEvent', {
            event: ClientAppModelFactory.create(PercentageEventSchema.ID, {
                category: 'a',
                action: 'a',
                percentage: 0.25,
                labels: '{model.series_name} - {model.title} | EP {model.episode}',
                value: 'a',
                custom: []
            })
        });

        const custom = AppstudioModelFactory.create('CustomData');
        custom.content = '{\n  \"a\": 1\n}';
        videoPlayback.event.custom.push(custom);

        googleAnalytic.systemEvents.videoPlayback.push(videoPlayback);

        describe('WHEN: translated', () => {

            let translator = new GoogleAnalyticTranslator();
            let translation;

            before(() => {
                return translator.translate('videa', translationService, googleAnalytic).then(
                    (t) => {
                        translation = t;
                    });
            });

            it('THEN: the videoPlayback PercentageEvent has translated', () => {
                expect(translation.systemEvents.videoPlayback[0].type).to.equal('Percentage');
                expect(translation.systemEvents.videoPlayback[0].category).to.equal(result);
                expect(translation.systemEvents.videoPlayback[0].action).to.equal(result);
                expect(translation.systemEvents.videoPlayback[0].percentage).to.equal(0.25);
                expect(translation.systemEvents.videoPlayback[0].value).to.equal(result);
                expect(translation.systemEvents.videoPlayback[0].labels).to.equal(label);
                expect(translation.systemEvents.videoPlayback[0].custom[0].a).to.equal(1);
            });
        });
    });    
});