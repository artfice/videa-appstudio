import chai = require('chai');
const expect = chai.expect;

import dotenv = require('dotenv');
dotenv.config();

import {AdvanceScreenTranslator} from '../../translation/screen/AdvanceScreenTranslator';
import {AdvanceScreen as AdvanceScreenDto} from '../../dto/app/configuration/uiconfig/AdvanceScreen';

import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

describe('Advance Screen Translation Tests', () => {

	var translationService = new TranslationService();


	describe('GIVEN: Advance screen with no content', () => {

		var advanceScreen = <AdvanceScreenDto> {};

		describe('WHEN: translated', () => {
			var translator = new AdvanceScreenTranslator(),
				translation;

			before((done) => {

				translator.translate('videa', translationService, advanceScreen).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be undefined', () => {
				expect(translation).to.not.exist;
			});
		});


	});

	describe('GIVEN: Advance screen with invalid json content', () => {

		var advanceScreen = <any> {
			content: 'sdfkjhsdfkjh'
		};

		describe('WHEN: translated', () => {
			var translator = new AdvanceScreenTranslator(),
				translation;

			before((done) => {

				translator.translate('videa', translationService, advanceScreen).then(
					(t) => {
						translation = t;
						done('translation should not have translated invalid json content');
					}
				).catch(
					(err) => {
						translation = err;
						done();
					}
				);
			});

			it ('THEN: the translation should result in an error', () => {
				expect(translation).to.be.an.instanceOf(Error);
			});

		});


	});
	describe('GIVEN: Advance screen with valid json content', () => {

		var advanceScreen = <AdvanceScreenDto> {
            customData: {
                content: '{"a": 1, "b": 2}'
            }
		};

		describe('WHEN: translated', () => {
			var translator = new AdvanceScreenTranslator(),
				translation;

			before((done) => {

				translator.translate('videa', translationService, advanceScreen).then(
					(t) => {
						translation = t;
						done();
					}
				).catch(
					(err) => {
						done(err);
					}
				);
			});

			it ('THEN: the translation should be a valid json object', () => {
				expect(translation).to.exist;
				expect(translation.a).to.exist;
				expect(translation.a).to.equal(1);
				expect(translation.b).to.exist;
				expect(translation.b).to.equal(2);
			});

		});


	});

});
