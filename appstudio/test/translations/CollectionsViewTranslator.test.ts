import { expect } from 'chai';

import { CollectionsViewTranslator } from '../../translation/dataview/CollectionsViewTranslator';
import { CollectionPickerContentTranslator } from '../../translation/dataview/picker/CollectionPickerContentTranslator';
import { GridViewTranslator } from '../../translation/dataview/tileview/GridViewTranslator';
import { TileViewTranslator } from '../../translation/dataview/TileViewTranslator';
import { BaseCollectionTranslator } from '../../translation/dataview/collection/BaseCollectionTranslator';
import { BaseFieldStyleTranslator } from '../../translation/field/style/BaseFieldStyleTranslator';
import { FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';
import { ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ComponentStyleTranslator } from '../../translation/component/ComponentStyleTranslator';
import { ComponentTranslator } from '../../translation/component/ComponentTranslator';
import { TileViewStyleTranslator } from '../../translation/dataview/style/TileViewStyleTranslator';
import { SizedComponentStyleTranslator } from '../../translation/component/SizedComponentStyleTranslator';
import { DataViewHeadingTranslator } from '../../translation/dataview/heading/DataViewHeadingTranslator';
import { DataViewHeadingStyleTranslator } from '../../translation/dataview/style/DataViewHeadingStyleTranslator';
import { LabelTranslator } from '../../translation/field/LabelTranslator';
import { LabelStyleTranslator } from '../../translation/field/style/LabelStyleTranslator';
import { LabelContentTranslator } from '../../translation/field/content/LabelContentTranslator';
import { DataViewTileTranslator } from '../../translation/dataview/tile/DataViewTileTranslator';
import { ImageTranslator } from '../../translation/field/ImageTranslator';
import { ImageStyleTranslator } from '../../translation/field/style/ImageStyleTranslator';
import { GridViewStyleTranslator } from '../../translation/dataview/style/GridViewStyleTranslator';
import { CollectionPickerStyleTranslator } from '../../translation/dataview/style/CollectionPickerStyleTranslator';
import { OpenSansTranslator } from '../../translation/font/OpenSansTranslator';
import { ComplexColorTranslator } from '../../translation/color/ComplexColorTranslator';
import { ColorTranslator } from '../../translation/color/ColorTranslator';

import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { AppstudioModelFactory as AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory as ClientappModelFactory } from '../../factory/ClientAppModelFactory';

import { CollectionPickerSettings as CollectionPickerDto } from '../../dto/app/configuration/uiconfig/screen/collectionview/picker/CollectionPickerSettings';
import { PickerContent as CollectionPickerContentDto } from '../../dto/app/configuration/uiconfig/screen/collectionview/picker/PickerContent';
import { PickerStyle as CollectionPickerStyleDto } from '../../dto/app/configuration/uiconfig/screen/collectionview/picker/PickerStyle';
import { CollectionView as AppstudioCollectionViewDto } from '../../dto/app/configuration/uiconfig/screen/collectionview/CollectionView';
import { SizedComponentStyle as AppstudioSizedComponentStyleDto } from '../../dto/app/configuration/uiconfig/screen/component/style/SizedComponentStyle';
import { BaseCollection as AppstudioBaseCollectionDto } from '../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';

import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');
import AlignmentSchema = require('../../schema/common/Alignment');

describe('CollectionView Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(ComponentStyleTranslator);
    translationService.registerTranslator(ComponentTranslator);
    translationService.registerTranslator(BaseFieldStyleTranslator);
    translationService.registerTranslator(FieldContentTranslator);
    translationService.registerTranslator(ComponentControlsTranslator);
    translationService.registerTranslator(BaseCollectionTranslator);
    translationService.registerTranslator(TileViewStyleTranslator);
    translationService.registerTranslator(SizedComponentStyleTranslator);
    translationService.registerTranslator(DataViewHeadingTranslator);
    translationService.registerTranslator(DataViewHeadingStyleTranslator);
    translationService.registerTranslator(LabelStyleTranslator);
    translationService.registerTranslator(LabelTranslator);
    translationService.registerTranslator(LabelContentTranslator);
    translationService.registerTranslator(TileViewTranslator);
    translationService.registerTranslator(DataViewTileTranslator);
    translationService.registerTranslator(ImageTranslator);
    translationService.registerTranslator(ImageStyleTranslator);
    translationService.registerTranslator(GridViewStyleTranslator);
    translationService.registerTranslator(GridViewTranslator);
    translationService.registerTranslator(CollectionPickerContentTranslator);
    translationService.registerTranslator(CollectionPickerStyleTranslator);
    translationService.registerTranslator(OpenSansTranslator);
    translationService.registerTranslator(ComplexColorTranslator);
    translationService.registerTranslator(ColorTranslator);

    const translator = new CollectionsViewTranslator();

    describe('GIVEN: an empty CollectionView', () => {
        const source = <AppstudioCollectionViewDto>AppstudioModelFactory.create('CollectionView', {});

        describe('WHEN: translated', () => {
            let translation;

            const container = 'Container';
            const layout = 'Grid';
            const columns = 1;
            const orientation = 'vertical';

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the style border property should be translated as undefined', () => {
                expect(translation.style.border).to.equal(undefined);
            });

            it('THEN: the width property should be translated as matchParent', () => {
                expect(translation.width).to.equal('matchParent');
            });

            it('THEN: the height property should be translated as wrapContent', () => {
                expect(translation.height).to.equal('wrapContent');
            });

            it('THEN: the visible property should be translated as true', () => {
                expect(translation.visible).to.equal('true');
            });

            it('THEN: the gridview container type property should be translated as ' + container, () => {
                expect(translation.itemRenderer.type).to.equal(container);
            });

            it('THEN: the gridview layout type property should be translated as ' + layout, () => {
                expect(translation.itemRenderer.layout.type).to.equal(layout);
            });
            it('THEN: the gridview layout orientation property should be translated as ' + orientation, () => {
                expect(translation.itemRenderer.layout.orientation).to.equal(orientation);
            });
            it('THEN: the gridview layout columns property should be translated as ' + columns, () => {
                expect(translation.itemRenderer.layout.columns).to.equal(columns);
            });
        });
    });

    describe('GIVEN: a CollectionView with a style', () => {
        const source = <AppstudioCollectionViewDto>AppstudioModelFactory.create('CollectionView', {});
        const margin = '1 1 1 1';
        const padding = '2 2 2 2';
        const radius = 1;
        const thickness = 1;
        const color = '#AAAAAA';
        const width = 10;
        const height = 20;
        const style = <AppstudioSizedComponentStyleDto>AppstudioModelFactory.create('SizedComponentStyle', {
            margin: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            padding: AppstudioModelFactory.create('BoxParams', {
                top: 2,
                right: 2,
                bottom: 2,
                left: 2
            }),
            border: {
                radius: radius,
                thickness: thickness,
                color: {
                    value: color
                }
            },
            width: AppstudioModelFactory.create('Width', {
                value: AppstudioModelFactory.create('CustomWidth', {
                    value: width
                })
            }),
            height: AppstudioModelFactory.create('Height', {
                value: AppstudioModelFactory.create('CustomHeight', {
                    value: height
                })
            })
        });

        source.style = style;

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the style border radius property should be translated as ' + radius, () => {
                expect(translation.style.border.radius).to.equal(radius);
            });

            it('THEN: the style border thickness property should be translated as ' + thickness, () => {
                expect(translation.style.border.thickness).to.equal(thickness);
            });

            it('THEN: the style border color property should be translated as ' + color, () => {
                expect(translation.style.border.color.value).to.equal(color);
            });

            it('THEN: the width property should be translated as ' + width, () => {
                expect(translation.style.width).to.equal(width);
            });

            it('THEN: the height property should be translated as ' + height, () => {
                expect(translation.style.height).to.equal(height);
            });

            it('THEN: the margin property should be translated as ' + margin, () => {
                expect(translation.style.margin).to.equal(margin);
            });

            it('THEN: the padding property should be translated as ' + padding, () => {
                expect(translation.style.padding).to.equal(padding);
            });

            it('THEN: the visible property should be translated as true', () => {
                expect(translation.visible).to.equal('true');
            });
        });
    });

    describe('GIVEN: a CollectionView with a picker content', () => {
        const source = <AppstudioCollectionViewDto>AppstudioModelFactory.create('CollectionView', {});
        const provider = 'True Logic';
        const filterFunction = 'someFunction';
        const defineFilterFunction = 'defineFunction';
        const sortFunctions = 'sort';
        const type = 'DropdownNew';
        const displayName = 'displayName';
        const picker = <CollectionPickerDto>AppstudioModelFactory.create('CollectionPicker');
        const content = <CollectionPickerContentDto<AppstudioBaseCollectionDto>>AppstudioModelFactory.create('CollectionPickerContent', {
            collection: ClientappModelFactory.create('Collection', {
                provider: provider,
                filterFunction: filterFunction,
                defineFilterFunction: defineFilterFunction,
                sortFunctions: sortFunctions
            }),
            type: type,
            displayName: displayName
        });

        picker.content = content;
        source.picker = picker;

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the content type property should be translated as ' + type, () => {
                expect(translation.pickerRenderer.type).to.equal(type);
            });

            it('THEN: the content displayName property should be translated as ' + displayName, () => {
                expect(translation.pickerRenderer.displayName).to.equal(displayName);
            });

            it('THEN: the content collection provider property should be translated as ' + provider, () => {
                expect(translation.collection.provider).to.equal(provider);
            });

            it('THEN: the content collection filterFunction property should be translated as ' + filterFunction, () => {
                expect(translation.collection.filterFunction).to.equal(filterFunction);
            });

            it('THEN: the content collection defineFilterFunction property should be translated as ' + defineFilterFunction, () => {
                expect(translation.collection.defineFilterFunction).to.equal(defineFilterFunction);
            });

            it('THEN: the content collection sortFunctions property should be translated as ' + sortFunctions, () => {
                expect(translation.collection.sortFunctions).to.equal(sortFunctions);
            });
        });
    });

    describe('GIVEN: a CollectionView with a picker style', () => {
        let backgroundColor = {
            _metadata: ColorSchema.ID,
            value: '#BBBBBBBB'
        };

        const source = <AppstudioCollectionViewDto>AppstudioModelFactory.create('CollectionView', {});
        const margin = '1 1 1 1';
        const padding = '2 2 2 2';
        const radius = 1;
        const thickness = 1;
        const color = '#AAAAAA';
        const width = 10;
        const height = 20;
        const alignment = 'Right';
        const weight = 'Bold';
        const size = 11;

        const style = <CollectionPickerStyleDto>AppstudioModelFactory.create('CollectionPickerStyle', {
            margin: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            padding: AppstudioModelFactory.create('BoxParams', {
                top: 2,
                right: 2,
                bottom: 2,
                left: 2
            }),
            border: {
                radius: radius,
                thickness: thickness,
                color: {
                    value: color
                }
            },
            backgroundColor: {
                _metadata: ComplexColorSchema.ID,
                value: backgroundColor
            },
            font: ClientappModelFactory.create('OpenSans', {
                size: size,
                color: AppstudioModelFactory.create('Color', {
                    value: color
                }),
                alignment: alignment,
                weight: weight
            }),
            width: AppstudioModelFactory.create('Width', {
                value: AppstudioModelFactory.create('CustomWidth', {
                    value: width
                })
            }),
            height: AppstudioModelFactory.create('Height', {
                value: AppstudioModelFactory.create('CustomHeight', {
                    value: height
                })
            })
        });

        const picker = <CollectionPickerDto>AppstudioModelFactory.create('CollectionPicker');
        picker.style = style;

        source.picker = picker;

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the picker style padding property should be translated as ' + padding, () => {
                expect(translation.pickerRenderer.style.padding).to.equal(padding);
            });
            it('THEN: the picker style margin property should be translated as ' + margin, () => {
                expect(translation.pickerRenderer.style.margin).to.equal(margin);
            });

            it('THEN: the picker style padding property should be translated as ' + padding, () => {
                expect(translation.pickerRenderer.style.padding).to.equal(padding);
            });

            it('THEN: the style border radius property should be translated as ' + radius, () => {
                expect(translation.pickerRenderer.style.border.radius).to.equal(radius);
            });

            it('THEN: the style border thickness property should be translated as ' + thickness, () => {
                expect(translation.pickerRenderer.style.border.thickness).to.equal(thickness);
            });

            it('THEN: the style border color property should be translated as ' + color, () => {
                expect(translation.pickerRenderer.style.border.color.value).to.equal(color);
            });

        });
    });

    describe('GIVEN: a CollectionView with a picker content', () => {
        let backgroundColor = {
            _metadata: ColorSchema.ID,
            value: '#BBBBBBBB'
        };

        const source = <AppstudioCollectionViewDto>AppstudioModelFactory.create('CollectionView', {});
        const margin = '1 1 1 1';
        const padding = '2 2 2 2';
        const radius = 1;
        const thickness = 1;
        const color = '#AAAAAA';
        const width = 10;
        const height = 20;
        const alignment = 'Right';
        const weight = 'Bold';
        const family = 'OpenSans-Bold';
        const size = 11;
        const pickerHeight = '50';
        const style = <CollectionPickerStyleDto>AppstudioModelFactory.create('CollectionPickerStyle', {
            margin: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            padding: AppstudioModelFactory.create('BoxParams', {
                top: 2,
                right: 2,
                bottom: 2,
                left: 2
            }),
            border: {
                radius: radius,
                thickness: thickness,
                color: {
                    value: color
                }
            },
            backgroundColor: {
                _metadata: ComplexColorSchema.ID,
                value: backgroundColor
            },
            font: ClientappModelFactory.create('OpenSans', {
                size: size,
                color: AppstudioModelFactory.create('Color', {
                    value: color
                }),
                alignment: AppstudioModelFactory.create(AlignmentSchema.ID, {value: alignment}),
                weight: weight
            }),
            width: AppstudioModelFactory.create('Width', {
                value: AppstudioModelFactory.create('CustomWidth', {
                    value: width
                })
            }),
            height: AppstudioModelFactory.create('Height', {
                value: AppstudioModelFactory.create('CustomHeight', {
                    value: height
                })
            })
        });
        const picker = <CollectionPickerDto>AppstudioModelFactory.create('CollectionPicker');
        picker.style = style;

        source.picker = picker;

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the picker style padding property should be translated as ' + padding, () => {
                expect(translation.pickerRenderer.style.padding).to.equal(padding);
            });
            it('THEN: the picker style margin property should be translated as ' + margin, () => {
                expect(translation.pickerRenderer.style.margin).to.equal(margin);
            });

            it('THEN: the picker style padding property should be translated as ' + padding, () => {
                expect(translation.pickerRenderer.style.padding).to.equal(padding);
            });

            it('THEN: the style border radius property should be translated as ' + radius, () => {
                expect(translation.pickerRenderer.style.border.radius).to.equal(radius);
            });

            it('THEN: the style border thickness property should be translated as ' + thickness, () => {
                expect(translation.pickerRenderer.style.border.thickness).to.equal(thickness);
            });

            it('THEN: the style border color property should be translated as ' + color, () => {
                expect(translation.pickerRenderer.style.border.color.value).to.equal(color);
            });
            it('THEN: the style backgroundColor property should be translated as ' + JSON.stringify(backgroundColor), () => {
                expect(translation.pickerRenderer.style.backgroundColor.value).to.equal(backgroundColor.value);
                expect(translation.pickerRenderer.style.backgroundColor._metadata).to.equal(backgroundColor._metadata);
            });
            it('THEN: the height property should be translated as ' + pickerHeight, () => {
                expect(translation.pickerRenderer.height).to.equal(pickerHeight);
            });

            it('THEN: the style font color property should be translated as ' + color, () => {
                expect(translation.pickerRenderer.style.font.color.value).to.equal(color);
            });

            it('THEN: the style font family property should be translated as ' + family, () => {
                expect(translation.pickerRenderer.style.font.family).to.equal(family);
            });

            it('THEN: the style font size property should be translated as ' + size, () => {
                expect(translation.pickerRenderer.style.font.size).to.equal(size);
            });

            it('THEN: the style font alignment property should be translated as ' + alignment, () => {
                expect(translation.pickerRenderer.style.font.alignment).to.equal(alignment);
            });

        });
    });
});
