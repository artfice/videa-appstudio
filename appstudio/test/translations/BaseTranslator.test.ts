import chaiLib = require('chai');
const expect = chaiLib.expect;

import {BaseTranslator} from '../../translation/BaseTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';
import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

describe('Base Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty source', () => {
		var source = AppstudioModelFactory.create('MetadataOwner');

		describe('WHEN:  translated', () => {
			var translator = new BaseTranslator(source),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
		});
	});

	describe('GIVEN: a source with field a', () => {
		var a = a,
			source = AppstudioModelFactory.create('MetadataOwner',{
            	a: a
        });

		describe('WHEN:  translated', () => {
			var translator = new BaseTranslator(source),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should have field a', () => {
				expect(translation.a).to.equal(a);
			});
		});
	});
});
