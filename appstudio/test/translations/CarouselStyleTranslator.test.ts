///<reference path="../../../typings/index.d.ts"/>

import { expect } from 'chai';

import { CarouselStyleTranslator } from '../../translation/dataview/style/CarouselStyleTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import { CarouselStyle as AppstudioCarouselStyleDto } from '../../dto/app/configuration/uiconfig/screen/dataview/carousel/CarouselStyle';

import PositionSchema = require('../../schema/common/Position');
import { Position as PositionDto } from '../../dto/common/Position';

describe('CarouselStyle Translation Tests', () => {
    const translationService = new TranslationService();

    describe('GIVEN: an empty CarouselStyle', () => {
        const tileScale = undefined;
        const paginationDots = undefined;
        const paginationDotColors = undefined;
        const paginationDotPosition = 'center';
        const paginationLocation = undefined;
        const paginationMargin = undefined;
        const autoScrollDuration = undefined;

        const source = <AppstudioCarouselStyleDto>AppstudioModelFactory.create('CarouselStyle');

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new CarouselStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });
            it('THEN: the itemScale property should be translated as ' + tileScale, () => {
                expect(translation.itemScale).to.equal(tileScale);
            });
            it('THEN: the paginationDots property should be translated as ' + paginationDots, () => {
                expect(translation.pagination.visible).to.equal(paginationDots);
            });
            it('THEN: the paginationDotPosition property should be translated as ' + paginationDotPosition, () => {
                expect(translation.pagination.alignment).to.equal(paginationDotPosition);
            });
            it('THEN: the location property should be translated as ' + paginationLocation, () => {
                expect(translation.pagination.location).to.equal(paginationLocation);
            });
            it('THEN: the margin property should be translated as ' + paginationMargin, () => {
                expect(translation.pagination.margin).to.equal(paginationMargin);
            });
            it('THEN: the style selectedColor should be translated as ' + paginationDotColors, () => {
                expect(translation.pagination.selectedColor).to.equal(paginationDotColors);
            });
            it('THEN: the style unselectedColor should be translated as ' + paginationDotColors, () => {
                expect(translation.pagination.unselectedColor).to.equal(paginationDotColors);
            });
            it('THEN: the style autoScrollDuration should be translated as ' + autoScrollDuration, () => {
                expect(translation.autoScrollDuration).to.equal(autoScrollDuration);
            });
        });
    });

    describe('GIVEN: a CarouselStyle with tileScale, paginationDotColors, paginationDotPosition, paginationDots', () => {
        const tileScale = 1;
        const paginationDots = false;
        const paginationDotColors = '#FFFFFE';
        const paginationDotPosition = 'test';
        const paginationLocation = 'outside';
        const paginationMargin = '1 1 1 1';
        const autoScrollDuration = 10;

        const source = <AppstudioCarouselStyleDto>AppstudioModelFactory.create('CarouselStyle', {
            tileScale: tileScale,
            paginationDots: paginationDots,
            paginationSelectedDotColors: AppstudioModelFactory.create('Color', {
                value: paginationDotColors
            }),
            paginationUnselectedDotColors: AppstudioModelFactory.create('Color', {
                value: paginationDotColors
            }),
            paginationMargin: paginationMargin,
            paginationLocation: paginationLocation,
            paginationDotPosition: <PositionDto> AppstudioModelFactory.create(PositionSchema.ID, {
                value: paginationDotPosition
            }),
            autoScrollDuration: autoScrollDuration
        });

        describe('WHEN: translated', () => {
            let translation;

            before((done) => {
                new CarouselStyleTranslator().translate('videa', translationService, source).then(
                    (translationResult) => {
                        translation = translationResult;
                        done();
                    },
                    (error) => {
                        done();
                    }
                );
            });

            it('THEN: the itemScale property should translate to ' + tileScale, () => {
                expect(translation.itemScale).to.equal(tileScale);
            });
            it('THEN: the paginationDots property should translate to ' + paginationDots, () => {
                expect(translation.pagination.visible).to.equal(paginationDots);
            });
            it('THEN: the alignment property should translate to ' + paginationDotPosition, () => {
                expect(translation.pagination.alignment).to.equal(paginationDotPosition);
            });
            it('THEN: the style selectedColor should be translated as ' + paginationDotColors, () => {
                expect(translation.pagination.selectedColor.value).to.equal(paginationDotColors);
            });
            it('THEN: the style unselectedColor should be translated as ' + paginationDotColors, () => {
                expect(translation.pagination.unselectedColor.value).to.equal(paginationDotColors);
            });
            it('THEN: the location property should be translated as ' + paginationLocation, () => {
                expect(translation.pagination.location).to.equal(paginationLocation);
            });
            it('THEN: the paginationMargin property should be translated as ' + paginationMargin, () => {
                expect(translation.pagination.margin).to.equal(paginationMargin);
            });
            it('THEN: the autoScrollDuration property should be translated as ' + autoScrollDuration, () => {
                expect(translation.autoScrollDuration).to.equal(autoScrollDuration);
            });
        });
    });

});
