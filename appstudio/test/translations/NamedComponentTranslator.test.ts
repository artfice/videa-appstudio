import chaiLib = require('chai');
const expect = chaiLib.expect;

import {ComponentTranslator as ComponentTranslator} from '../../translation/component/ComponentTranslator';
import {NamedComponentTranslator as NamedComponentTranslator} from '../../translation/component/NamedComponentTranslator';
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService';

import {AppstudioModelFactory as AppstudioModelFactory} from '../../factory/AppstudioModelFactory';

import {NamedComponent as NamedComponent} from '../../dto/app/configuration/uiconfig/screen/component/NamedComponent';
import {ComponentStyle as ComponentStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';

import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {BaseTranslator as BaseTranslator} from '../../translation/BaseTranslator';


describe('Component Translation Tests', () => {
	var translationService = new TranslationService();

	describe('GIVEN: an empty NamedComponent', () => {
		var source = <NamedComponent<ComponentStyle>>AppstudioModelFactory.create('NamedComponent'),
			name = '';

		describe('WHEN: translated', () => {
			var translator = new NamedComponentTranslator(),
				translation;

			translationService.registerTranslator(BaseTranslator);
			translationService.registerTranslator(ComponentStyleTranslator);
			translationService.registerTranslator(ComponentControlsTranslator);
			translationService.registerTranslator(ComponentTranslator);

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the name property should be empty string', () => {
				expect(translation.name).to.equal(name);
			});
		});
	});

    describe('GIVEN: an NamedCompoent with a name', () => {
		var name = 'hello',
			source = <NamedComponent<ComponentStyle>>AppstudioModelFactory.create('NamedComponent', {
            name: name
        });

		describe('WHEN: translated', () => {
			var translator = new NamedComponentTranslator(),
				translation;

			translationService.registerTranslator(BaseTranslator);
			translationService.registerTranslator(ComponentStyleTranslator);
			translationService.registerTranslator(ComponentControlsTranslator);
			translationService.registerTranslator(ComponentTranslator);

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
                    (err) => {
						done();
					}
				);
			});

			it ('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it ('THEN: the name property should be translated as ' + name, () => {
				expect(translation.name).to.equal( name);
			});
		});
	});
});
