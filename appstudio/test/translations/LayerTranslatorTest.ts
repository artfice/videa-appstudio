import chaiLib = require('chai');
const expect = chaiLib.expect;
import _ = require('lodash');

import {ComponentTranslator} from '../../translation/component/ComponentTranslator');
import {LayerTranslator} from '../../translation/component/LayerTranslator');
import {TranslationApplicationService as TranslationService} from '../../application/TranslationApplicationService');

import {AppstudioModelFactory} from '../../factory/AppstudioModelFactory';
import {ClientAppModelFactory} from '../../factory/ClientAppModelFactory';

import {ComponentControls as ComponentControlsDto} from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import {Layer as Layer} from '../../dto/app/configuration/uiconfig/screen/component/Layer';
import {ComponentStyle as ComponentStyle} from '../../dto/app/configuration/uiconfig/screen/component/style/ComponentStyle';
import {BaseAction as BaseActionDto} from '../../dto/common/listener/action/BaseAction';
import {BaseEvent as BaseEventDto} from '../../dto/common/listener/event/BaseEvent';

import {BaseTranslator as BaseTranslator} from '../../translation/BaseTranslator';
import {ComponentControlsTranslator as ComponentControlsTranslator} from '../../translation/component/ComponentControlsTranslator';
import {ComponentStyleTranslator as ComponentStyleTranslator} from '../../translation/component/ComponentStyleTranslator';
import {WebViewContentTranslator as WebViewContentTranslator} from '../../translation/webview/WebViewContentTranslator';
import {WebViewStyleTranslator as WebViewStyleTranslator} from '../../translation/webview/WebViewStyleTranslator';
import {WebViewTranslator as WebViewTranslator} from '../../translation/webview/WebViewTranslator';
import {LayerControlsTranslator} from '../../translation/component/LayerControlsTranslator';

import VerticalLayout = require('../../schema/common/layout/VerticalLayout');
import RelativeLayout = require('../../schema/common/layout/RelativeLayout');
import HorizontalLayout = require('../../schema/common/layout/HorizontalLayout');

function print(o) {
	console.log(JSON.stringify(o, null, 4));
}

describe('Layer Component Translation Tests', () => {
	let translationService = new TranslationService();

	translationService.registerTranslator(BaseTranslator);
	translationService.registerTranslator(ComponentStyleTranslator);
	translationService.registerTranslator(ComponentControlsTranslator);
	translationService.registerTranslator(ComponentTranslator);
	translationService.registerTranslator(WebViewStyleTranslator);
	translationService.registerTranslator(WebViewContentTranslator);
	translationService.registerTranslator(WebViewTranslator);
	translationService.registerTranslator(LayerControlsTranslator);

	describe('GIVEN: an empty Layer', () => {
		const source = <Layer<ComponentStyle>>AppstudioModelFactory.create('Layer');

		describe('WHEN: translated', () => {
			const translator = new LayerTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it('THEN: the translation has Space type AND no items', () => {
				expect(translation.type).to.equal('Space');
                expect(translation.items).to.equal(undefined);
			});
		});
	});

	describe('GIVEN: a Layer with no items', () => {
		const margin = '1 1 1 1';
		const padding = '1 1 1 1';
		const radius = 1;
		const value = '#AAAAAA';
		const source = <Layer<ComponentStyle>>AppstudioModelFactory.create('Layer', {
			style: AppstudioModelFactory.create('ComponentStyle', {
				margin: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
				padding: AppstudioModelFactory.create('BoxParams', {
                    top: 1,
                    right: 1,
                    bottom: 1,
                    left: 1
                }),
				border: {
					radius: radius,
					color: {
						value: value
					}
				}
			})
		});

		describe('WHEN: translated', () => {
			const translator = new LayerTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it('THEN: the type property should be translated', () => {
				expect(translation.type).to.equal('Space');
			});
			it('THEN: the items property should not exist', () => {
				expect(translation.items).to.equal(undefined);
			});

			it('THEN: the margin property should be translated', () => {
				expect(translation.style.padding).to.equal('1 1 1 1');
			});
			it('THEN: the padding property should be translated', () => {
				expect(translation.style.margin).to.equal('1 1 1 1');
			});
			it('THEN: the border radius property should be translated', () => {
				expect(translation.style.border.radius).to.equal(1);
			});

			it('THEN: the border color property should be translated', () => {
				expect(translation.style.border.color.value).to.equal('#AAAAAA');
			});
		});
	});

	describe('GIVEN: a Layer with items', () => {
		const source = <Layer<ComponentStyle>>AppstudioModelFactory.create('Layer', {
			subComponent: []
		});

		const width = 1;
		const height = 1;
		const verticalScrollEnabled = true;
		const horizontalScrollEnabled = true;
		const fallbackMargin = '0 0 0 0';
		const uri = 'a';
		const visible = 'not true';
		const webview = <any>AppstudioModelFactory.create('WebView', {
			style: AppstudioModelFactory.create('WebViewStyle', {

				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
				height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				}),
				verticalScrollEnabled: verticalScrollEnabled,
				horizontalScrollEnabled: horizontalScrollEnabled,
				fallbackMargin: fallbackMargin
			}),
			content: AppstudioModelFactory.create('WebViewContent', {
				uri: uri
			}),
			controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
				visible: visible
			})
		});

		source.subComponent.push(webview);

		describe('WHEN: translated', () => {
			const translator = new LayerTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						console.log(translation);
					});
			});

			it('THEN: the type property should be translated', () => {
				expect(translation.type).to.equal('Container');
			});
			it('THEN: the translation has items', () => {
				expect(translation.items.length).to.equal(1);
			});
			it('THEN: the width property should translated', () => {
				expect(translation.items[0].width).to.equal(1);
			});
			it('THEN: the height property should translated', () => {
				expect(translation.items[0].height).to.equal(1);
			});
			it('THEN: the verticalScrollEnabled property should translated', () => {
				expect(translation.items[0].verticalScrollEnabled).to.equal(true);
			});
			it('THEN: the horizontalScrollEnabled property should translated', () => {
				expect(translation.items[0].horizontalScrollEnabled).to.equal(true);
			});
			it('THEN: the fallbackMargin property should translated', () => {
				expect(translation.items[0].fallbackMargin).to.equal('0 0 0 0');
			});
		});
	});

	describe('GIVEN: a Layer with layout set', () => {
		let controls = AppstudioModelFactory.create('LayerControls', {
			listeners: [],
			visible: 'true',
			layout: ClientAppModelFactory.create('VerticalLayout')
		});

		const source = <Layer<ComponentStyle>>AppstudioModelFactory.create('Layer', {
			controls : controls,
			subComponent: []
		});

		const width = 1;
		const height = 1;
		const verticalScrollEnabled = true;
		const horizontalScrollEnabled = true;
		const fallbackMargin = '0 0 0 0';
		const uri = 'a';
		const visible = 'not true';
		const webview = <any>AppstudioModelFactory.create('WebView', {
			style: AppstudioModelFactory.create('WebViewStyle', {

				width: AppstudioModelFactory.create('Width', {
					value: AppstudioModelFactory.create('CustomWidth', {
						value: width
					})
				}),
				height: AppstudioModelFactory.create('Height', {
					value: AppstudioModelFactory.create('CustomHeight', {
						value: height
					})
				}),
				verticalScrollEnabled: verticalScrollEnabled,
				horizontalScrollEnabled: horizontalScrollEnabled,
				fallbackMargin: fallbackMargin
			}),
			content: AppstudioModelFactory.create('WebViewContent', {
				uri: uri
			}),
			controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
				visible: visible
			})
		});

		source.subComponent.push(webview);

		describe('WHEN: translated', () => {
			const translator = new LayerTranslator();
			let translation;


			before(() => {
				return translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
					});
			});

			it('THEN: the layout property should be translated', () => {
				expect(translation.layout.orientation).to.equal('Vertical');
			});
		});
	});	
});