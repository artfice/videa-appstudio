import { expect } from 'chai';

import { CarouselTranslator } from '../../translation/dataview/CarouselTranslator';
import { ListenerTranslator } from '../../translation/ListenerTranslator';
import { TileViewTranslator } from '../../translation/dataview/TileViewTranslator';
import { BaseCollectionTranslator } from '../../translation/dataview/collection/BaseCollectionTranslator';
import { BaseFieldStyleTranslator } from '../../translation/field/style/BaseFieldStyleTranslator';
import { FieldContentTranslator } from '../../translation/field/content/FieldContentTranslator';
import { ComponentControlsTranslator } from '../../translation/component/ComponentControlsTranslator';
import { ComponentStyleTranslator } from '../../translation/component/ComponentStyleTranslator';
import { ComponentTranslator } from '../../translation/component/ComponentTranslator';
import { TileViewStyleTranslator } from '../../translation/dataview/style/TileViewStyleTranslator';
import { SizedComponentStyleTranslator } from '../../translation/component/SizedComponentStyleTranslator';
import { DataViewHeadingTranslator } from '../../translation/dataview/heading/DataViewHeadingTranslator';
import { DataViewHeadingStyleTranslator } from '../../translation/dataview/style/DataViewHeadingStyleTranslator';
import { LabelTranslator } from '../../translation/field/LabelTranslator';
import { LabelStyleTranslator } from '../../translation/field/style/LabelStyleTranslator';
import { LabelContentTranslator } from '../../translation/field/content/LabelContentTranslator';
import { DataViewTileTranslator } from '../../translation/dataview/tile/DataViewTileTranslator';
import { ImageTranslator } from '../../translation/field/ImageTranslator';
import { ImageStyleTranslator } from '../../translation/field/style/ImageStyleTranslator';
import { CarouselStyleTranslator } from '../../translation/dataview/style/CarouselStyleTranslator';
import { CarouselTileTranslator } from '../../translation/dataview/tile/CarouselTileTranslator';
import { CarouselControlsTranslator } from '../../translation/dataview/CarouselControlsTranslator';
import { RemoteCollectionTranslator } from '../../translation/dataview/collection/RemoteCollectionTranslator';
import { StandardAspectRatioTranslator } from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import { CustomAspectRatioTranslator } from '../../translation/aspectRatio/CustomAspectRatioTranslator';

import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';

import { AppstudioModelFactory } from '../../factory/AppstudioModelFactory';
import { ClientAppModelFactory } from '../../factory/ClientAppModelFactory';

import { BaseCollection as AppstudioBaseCollectionDto } from '../../dto/app/configuration/uiconfig/screen/dataview/collectiontypes/BaseCollection';
import { DataViewContent as AppstudioDataViewContentDto } from '../../dto/app/configuration/uiconfig/screen/dataview/DataViewContent';
import { Carousel as AppstudioCarouselDto } from '../../dto/app/configuration/uiconfig/screen/dataview/carousel/Carousel';
import { CarouselControls as AppstudioCarouselControlsDto } from '../../dto/app/configuration/uiconfig/screen/dataview/carousel/CarouselControls';
import { ComponentControls as ComponentControlsDto } from '../../dto/app/configuration/uiconfig/screen/component/ComponentControls';
import { BaseEvent as BaseEventDto } from '../../dto/common/listener/event/BaseEvent';
import { BaseAction as BaseActionDto } from '../../dto/common/listener/action/BaseAction';
import { Label as AppstudioLabelDto } from '../../dto/app/configuration/uiconfig/screen/component/field/label/Label';
import { Image as AppstudioImageDto } from '../../dto/app/configuration/uiconfig/screen/component/field/image/Image';
import { CarouselStyle as AppstudioCarouselStyleDto } from '../../dto/app/configuration/uiconfig/screen/dataview/carousel/CarouselStyle';
import { DataViewTile as AppstudioDataViewTileDto } from '../../dto/app/configuration/uiconfig/screen/dataview/DataViewTile';
import { DataViewTileSettings as AppstudioDataViewTileSettingsDto } from '../../dto/app/configuration/uiconfig/screen/dataview/tile/DataViewTileSettings';
import { DataViewTileStyle as AppstudioDataViewTileStyleDto } from '../../dto/app/configuration/uiconfig/screen/dataview/tile/DataViewTileStyle';
import { Listener as ListenerDto } from '../../dto/common/Listener';

import PositionSchema = require('../../schema/common/Position');
import { Position as PositionDto } from '../../dto/common/Position';

describe('Carousel Translation Tests', () => {
    const translationService = new TranslationService();
    translationService.registerTranslator(StandardAspectRatioTranslator);
    translationService.registerTranslator(CustomAspectRatioTranslator);
    translationService.registerTranslator(ComponentStyleTranslator);
    translationService.registerTranslator(ComponentTranslator);
    translationService.registerTranslator(BaseFieldStyleTranslator);
    translationService.registerTranslator(FieldContentTranslator);
    translationService.registerTranslator(ComponentControlsTranslator);
    translationService.registerTranslator(BaseCollectionTranslator);
    translationService.registerTranslator(TileViewStyleTranslator);
    translationService.registerTranslator(SizedComponentStyleTranslator);
    translationService.registerTranslator(DataViewHeadingTranslator);
    translationService.registerTranslator(DataViewHeadingStyleTranslator);
    translationService.registerTranslator(LabelStyleTranslator);
    translationService.registerTranslator(LabelTranslator);
    translationService.registerTranslator(LabelContentTranslator);
    translationService.registerTranslator(TileViewTranslator);
    translationService.registerTranslator(DataViewTileTranslator);
    translationService.registerTranslator(ImageTranslator);
    translationService.registerTranslator(ImageStyleTranslator);
    translationService.registerTranslator(CarouselStyleTranslator);
    translationService.registerTranslator(CarouselTileTranslator);
    translationService.registerTranslator(ListenerTranslator);
    translationService.registerTranslator(CarouselControlsTranslator);
    translationService.registerTranslator(RemoteCollectionTranslator);

    describe('GIVEN: an empty Carousel', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel', {});
        const border = undefined;
        const itemScale = undefined;
        const width = 'matchParent';
        const height = 'wrapContent';
        const itemRenderer = undefined;
        const scroll = undefined;
        const type = 'Carousel';
        const visible = 'true';

        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the style border property should be translated as ' + border, () => {
                expect(translation.style.border).to.equal(border);
            });

            it('THEN: the style itemScale property should be translated as ' + itemScale, () => {
                expect(translation.style.itemScale).to.equal(itemScale);
            });

            it('THEN: the width property should be translated as ' + width, () => {
                expect(translation.width).to.equal(width);
            });

            it('THEN: the height property should be translated as ' + height, () => {
                expect(translation.height).to.equal(height);
            });

            it('THEN: the visible property should be translated as ' + visible, () => {
                expect(translation.visible).to.equal(visible);
            });

            it('THEN: Carousel type property should be translated as ' + type, () => {
                expect(translation.type).to.equal(type);
            });

            it('THEN: the scroll property should be translated as ' + scroll, () => {
                expect(translation.scroll).to.equal(scroll);
            });

            it('THEN: Carousel itemRenderer property should be ' + itemRenderer, () => {
                expect(translation.itemRenderer).to.equal(itemRenderer);
            });

        });
    });

    describe('GIVEN: an empty Carousel with control, style, content set', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel');
        const style = <AppstudioCarouselStyleDto>AppstudioModelFactory.create('CarouselStyle');
        const controls = <AppstudioCarouselControlsDto>AppstudioModelFactory.create('CarouselControls');
        const content = <AppstudioDataViewContentDto<AppstudioBaseCollectionDto>>AppstudioModelFactory.create('DataViewContent');
        const border = undefined;
        const itemScale = undefined;
        const width = 'matchParent';
        const height = 'wrapContent';
        const itemRenderer = undefined;
        const scroll = undefined;
        const type = 'Carousel';
        const visible = 'true';

        source.style = style;
        source.controls = controls;
        source.content = content;

        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the style border property should be translated as ' + border, () => {
                expect(translation.style.border).to.equal(border);
            });

            it('THEN: the style itemScale property should be translated as ' + itemScale, () => {
                expect(translation.style.itemScale).to.equal(itemScale);
            });

            it('THEN: the width property should be translated as ' + width, () => {
                expect(translation.width).to.equal(width);
            });

            it('THEN: the height property should be translated as ' + height, () => {
                expect(translation.height).to.equal(height);
            });

            it('THEN: the visible property should be translated as ' + visible, () => {
                expect(translation.visible).to.equal(visible);
            });

            it('THEN: Carousel type property should be translated as ' + type, () => {
                expect(translation.type).to.equal(type);
            });

            it('THEN: the scroll property should be translated as ' + scroll, () => {
                expect(translation.scroll).to.equal(scroll);
            });

            it('THEN: Carousel itemRenderer property should be translated as ' + itemRenderer, () => {
                expect(translation.itemRenderer).to.equal(itemRenderer);
            });

        });
    });

    describe('GIVEN: a Carousel with style values', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel', {});
        const tileScale = 50;
        const paginationDots = true;
        const paginationDotColors = '#EWEWEW';
        const paginationDotPosition = 'center';
        const margin = '1 1 1 1';
        const paginationMargin = '1 1 1 1';
        const padding = '2 2 2 2';
        const paginationLocation = 'outside';
        const style = <AppstudioCarouselStyleDto>AppstudioModelFactory.create('CarouselStyle', {
            tileScale: tileScale,
            paginationDots: paginationDots,
            paginationSelectedDotColors: {
                value: paginationDotColors
            },
            paginationUnselectedDotColors: {
                value: paginationDotColors
            },
            paginationDotPosition: <PositionDto> AppstudioModelFactory.create(PositionSchema.ID, {
                value: paginationDotPosition
            }),
            paginationLocation: paginationLocation,
            paginationMargin: paginationMargin,
            margin: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            padding: AppstudioModelFactory.create('BoxParams', {
                top: 2,
                right: 2,
                bottom: 2,
                left: 2
            })
        });

        source.style = style;

        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        done();
                    }
                );
            });

            it('THEN: the translation should not be empty object', () => {
                expect(translation).to.exist;
            });

            it('THEN: the style itemScale should be translated as ' + tileScale, () => {
                expect(translation.style.itemScale).to.equal(tileScale);
            });
            it('THEN: the style visible should be translated as ' + paginationDots, () => {
                expect(translation.style.pagination.visible).to.equal(paginationDots);
            });
            it('THEN: the style selectedColor should be translated as ' + paginationDotColors, () => {
                expect(translation.style.pagination.selectedColor.value).to.equal(paginationDotColors);
            });
            it('THEN: the style unselectedColor should be translated as ' + paginationDotColors, () => {
                expect(translation.style.pagination.unselectedColor.value).to.equal(paginationDotColors);
            });
            it('THEN: the style alignment should be translated as ' + paginationDotPosition, () => {
                expect(translation.style.pagination.alignment).to.equal(paginationDotPosition);
            });
            it('THEN: the style location should be translated as ' + paginationLocation, () => {
                expect(translation.style.pagination.location).to.equal(paginationLocation);
            });
            it('THEN: the style paginationMargin should be translated as ' + paginationMargin, () => {
                expect(translation.style.pagination.margin).to.equal(paginationMargin);
            });
            it('THEN: the style margin should be translated as ' + margin, () => {
                expect(translation.style.margin).to.equal(margin);
            });
            it('THEN: the style padding should be translated as ' + padding, () => {
                expect(translation.style.padding).to.equal(padding);
            });
        });
    });

    describe('GIVEN: a Carousel with controls', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel');
        const visible = '1';
        const longSelect = 'LongSelect';
        const signout = 'Signout';
        const listener = <any>ClientAppModelFactory.create('Listener', {
            event: ClientAppModelFactory.create('LongSelectEvent'),
            action: ClientAppModelFactory.create('Signout')
        });
        const controls = <AppstudioCarouselControlsDto>AppstudioModelFactory.create('CarouselControls', {
            visible: visible
        });

        controls.listeners.push(listener);
        source.controls = controls;

        describe('WHEN: it is translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the visible property should be translated as ' + visible, () => {
                expect(translation.visible).to.equal(visible);
            });

            it('THEN: the listener event type property should be translated as ' + longSelect, () => {
                expect(translation.listeners[0].event.type).to.equal(longSelect);
            });

            it('THEN: the listener action type property should be translated as ' + signout, () => {
                expect(translation.listeners[0].action.type).to.equal(signout);
            });
        });
    });

    describe('GIVEN: a Carousel with a content', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel');
        const content = <any>AppstudioModelFactory.create('DataViewContent');
        const provider = 'TrueLogic';
        const filterFunction = 'a';
        const defineFilterFunction = 'a';
        const sortFunctions = 'A-Z';
        const link = 'Some link';
        const beginIndex = 1;
        const endIndex = 22;
        const remoteCollection = ClientAppModelFactory.create('Remote', {
            provider: provider,
            filterFunction: filterFunction,
            defineFilterFunction: defineFilterFunction,
            sortFunctions: sortFunctions,
            link: link,
            range: {
                beginIndex: beginIndex,
                endIndex: endIndex
            }
        });

        content.collection = remoteCollection;
        source.content = content;

        describe('WHEN: it is translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the provider property should be translated as ' + provider, () => {
                expect(translation.collection.provider).to.equal(provider);
            });
            it('THEN: the filterFunction property should be translated as ' + filterFunction, () => {
                expect(translation.collection.filterFunction).to.equal(filterFunction);
            });
            it('THEN: the defineFilterFunction property should be translated as ' + defineFilterFunction, () => {
                expect(translation.collection.defineFilterFunction).to.equal(defineFilterFunction);
            });
            it('THEN: the sortFunctions property should be translated as ' + sortFunctions, () => {
                expect(translation.collection.sortFunctions).to.equal(sortFunctions);
            });
            it('THEN: the link property should be translated as ' + link, () => {
                expect(translation.collection.link).to.equal(link);
            });
            it('THEN: the beginIndex property should be translated as ' + beginIndex, () => {
                expect(translation.collection.range.beginIndex).to.equal(beginIndex);
            });
            it('THEN: the endIndex property should be translated as ' + endIndex, () => {
                expect(translation.collection.range.endIndex).to.equal(endIndex);
            });
        });
    });
    describe('GIVEN: a Carousel with tile style values', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel', {});
        const radius = 1;
        const thickness = 1;
        const width = 10;
        const height = 20;
        const color = '#EWEWEW';
        const aspectRatio = '1:1';
        const expectedAspectRatio = 1;
        const margin = '1 1 1 1';
        const padding = '2 2 2 2';
        const style = <AppstudioDataViewTileStyleDto>AppstudioModelFactory.create('TileStyle', {
            margin: AppstudioModelFactory.create('BoxParams', {
                top: 1,
                right: 1,
                bottom: 1,
                left: 1
            }),
            padding: AppstudioModelFactory.create('BoxParams', {
                top: 2,
                right: 2,
                bottom: 2,
                left: 2
            }),
            border: {
                radius: radius,
                thickness: thickness,
                color: {
                    value: color
                }
            },
            width: AppstudioModelFactory.create('Width', {
                value: AppstudioModelFactory.create('CustomWidth', {
                    value: width
                })
            }),
            height: AppstudioModelFactory.create('Height', {
                value: AppstudioModelFactory.create('CustomHeight', {
                    value: height
                })
            }),
            aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                value: aspectRatio
            })
        });
        const settings = <AppstudioDataViewTileSettingsDto>AppstudioModelFactory.create('DataViewTileSettings');
        const tile = <AppstudioDataViewTileDto>AppstudioModelFactory.create('CarouselTile', {
            text: [],
            subComponent: []
        });

        settings.style = style;
        tile.settings = settings;

        source.tile = tile;

        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the style margin should be translated as ' + margin, () => {
                expect(translation.itemRenderer.style.margin).to.equal(margin);
            });
            it('THEN: the style padding should be translated as ' + padding, () => {
                expect(translation.itemRenderer.style.padding).to.equal(padding);
            });
            it('THEN: the style width should be translated as ' + width, () => {
                expect(translation.itemRenderer.style.width).to.equal(width);
            });
            it('THEN: the style height should be translated as ' + height, () => {
                expect(translation.itemRenderer.style.height).to.equal(height);
            });
            it('THEN: the aspectRatio should be translated as ' + expectedAspectRatio, () => {
                expect(translation.itemRenderer.aspectRatio).to.equal(expectedAspectRatio);
            });
            it('THEN: the border radius should be translated as ' + radius, () => {
                expect(translation.itemRenderer.style.border.radius).to.equal(radius);
            });
            it('THEN: the border thickness should be translated as ' + thickness, () => {
                expect(translation.itemRenderer.style.border.thickness).to.equal(thickness);
            });
            it('THEN: the border color should be translated as ' + color, () => {
                expect(translation.itemRenderer.style.border.color.value).to.equal(color);
            });
        });
    });

    describe('GIVEN: a Carousel with tile control values', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel', {});
        const visible = '1 + 2';
        const settings = <AppstudioDataViewTileSettingsDto>AppstudioModelFactory.create('DataViewTileSettings');
        const tile = <AppstudioDataViewTileDto>AppstudioModelFactory.create('CarouselTile', {
            text: [],
            subComponent: []
        });
        const longSelectEvent = <any>ClientAppModelFactory.create('LongSelectEvent');
        const longSelect = 'LongSelect';
        const signoutAction = <any>ClientAppModelFactory.create('Signout');
        const signout = 'Signout';
        const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
        const controls = <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
            visible: visible
        });

        listener.event = longSelectEvent;
        listener.action = signoutAction;
        controls.listeners.push(listener);

        settings.controls = controls;
        tile.settings = settings;

        source.tile = tile;

        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;

                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the control visible should be translated as' + visible, () => {
                expect(translation.itemRenderer.visible).to.equal(visible);
            });
            it('THEN: the listener event type property should be translated as ' + longSelect, () => {
                expect(translation.itemRenderer.listeners[0].event.type).to.equal(longSelect);
            });
            it('THEN: the listener action type property should be translated as ' + signout, () => {
                expect(translation.itemRenderer.listeners[0].action.type).to.equal(signout);
            });
        });
    });

    describe('GIVEN: a Carousel with tile label', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel', {});
        const settings = <AppstudioDataViewTileSettingsDto>AppstudioModelFactory.create('DataViewTileSettings');
        const tile = <AppstudioDataViewTileDto>AppstudioModelFactory.create('CarouselTile', {
            text: [],
            subComponent: []
        });
        const width = 1;
        const height = 1;
        const gravity = 'Left';
        const value = 'test';
        const visible = 'not true';
        const longSelectEvent = <any>ClientAppModelFactory.create('LongSelectEvent');
        const longSelect = 'LongSelect';
        const signoutAction = <any>ClientAppModelFactory.create('Signout');
        const signout = 'Signout';
        const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
        const labelSource = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
            style: AppstudioModelFactory.create('LabelStyle', {

                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                }),
                gravity: gravity
            }),
            content: AppstudioModelFactory.create('LabelContent', {
                value: value
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });

        listener.event = longSelectEvent;
        listener.action = signoutAction;
        labelSource.controls.listeners.push(listener);
        tile.settings = settings;
        tile.text.push(labelSource);
        source.tile = tile;

        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;

                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the tile label\'s value property should be translated as' + value, () => {
                expect(translation.itemRenderer.items[0].items[0].value).to.equal(value);
            });
            it('THEN: the tile label\'s width property should be translated as' + width, () => {
                expect(translation.itemRenderer.items[0].items[0].width).to.equal(width);
            });
            it('THEN: the tile label\'s height property should be translated as' + height, () => {
                expect(translation.itemRenderer.items[0].items[0].height).to.equal(height);
            });
            it('THEN: the tile label\'s gravity property should be translated as' + gravity, () => {
                expect(translation.itemRenderer.items[0].items[0].gravity).to.equal(gravity);
            });
            it('THEN: the tile label\'s visible property should be translated as' + visible, () => {
                expect(translation.itemRenderer.items[0].items[0].visible).to.equal(visible);
            });
            it('THEN: the listener event type property should be translated as ' + longSelect, () => {
                expect(translation.itemRenderer.items[0].items[0].listeners[0].event.type).to.equal(longSelect);
            });
            it('THEN: the listener action type property should be translated as ' + signout, () => {
                expect(translation.itemRenderer.items[0].items[0].listeners[0].action.type).to.equal(signout);
            });
        });
    });

    describe('GIVEN: a Carousel with tile image', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel', {});
        const settings = <AppstudioDataViewTileSettingsDto>AppstudioModelFactory.create('DataViewTileSettings');
        const tile = <AppstudioDataViewTileDto>AppstudioModelFactory.create('CarouselTile', {
            text: [],
            subComponent: []
        });
        const aspectRatio = '1:1';
        const aspectRatioResult = 1;
        const scale = 'aspectFit';
        const width = 1;
        const height = 1;
        const gravity = 'Left';
        const value = 'test';
        const visible = 'not true';
        const longSelectEvent = <any>ClientAppModelFactory.create('LongSelectEvent');
        const longSelect = 'LongSelect';
        const signoutAction = <any>ClientAppModelFactory.create('Signout');
        const signout = 'Signout';
        const containerType = 'Container';
        const listener = <ListenerDto<BaseEventDto, BaseActionDto>>ClientAppModelFactory.create('Listener');
        const imageSource = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                    value: aspectRatio
                }),
                scale: scale,
                gravity: gravity,

                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                })
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: value
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });
        listener.event = longSelectEvent;
        listener.action = signoutAction;
        imageSource.controls.listeners.push(listener);
        tile.settings = settings;
        tile.subComponent.push(imageSource);
        source.tile = tile;

        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the tile image value property should be translated as ' + value, () => {
                expect(translation.itemRenderer.items[0].value).to.equal(value);
            });
            it('THEN: the tile image width property should be translated as ' + width, () => {
                expect(translation.itemRenderer.items[0].width).to.equal(width);
            });
            it('THEN: the tile image height property should be translated as ' + height, () => {
                expect(translation.itemRenderer.items[0].height).to.equal(height);
            });
            it('THEN: the tile image gravity property should be translated as ' + gravity, () => {
                expect(translation.itemRenderer.items[0].gravity).to.equal(gravity);
            });
            it('THEN: the tile image visible property should be translated as ' + visible, () => {
                expect(translation.itemRenderer.items[0].visible).to.equal(visible);
            });
            it('THEN: the tile image aspectRatio property should be translated as ' + aspectRatioResult, () => {
                expect(translation.itemRenderer.items[0].aspectRatio).to.equal(aspectRatioResult);
            });
            it('THEN: the container after the images has a container type ' + containerType, () => {
                expect(translation.itemRenderer.items[1].type).to.equal(containerType);
            });
            it('THEN: the container after the images has linear layout', () => {
                expect(translation.itemRenderer.items[1].layout.type).to.equal('Linear');
            });
            it('THEN: the container after the images has linear layout and orientation', () => {
                expect(translation.itemRenderer.items[1].layout.orientation).to.equal('vertical');
            });
            it('THEN: the listener event type property should be translated as ' + longSelect, () => {
                expect(translation.itemRenderer.items[0].listeners[0].event.type).to.equal(longSelect);
            });
            it('THEN: the listener action type property should be translated as ' + signout, () => {
                expect(translation.itemRenderer.items[0].listeners[0].action.type).to.equal(signout);
            });
        });
    });

    describe('GIVEN: a Carousel with tile image and label', () => {
        const source = <AppstudioCarouselDto>AppstudioModelFactory.create('Carousel', {});
        const settings = <AppstudioDataViewTileSettingsDto>AppstudioModelFactory.create('DataViewTileSettings');
        const tile = <AppstudioDataViewTileDto>AppstudioModelFactory.create('CarouselTile', {
            text: [],
            subComponent: []
        });
        const aspectRatio = '1:1';
        const scale = 'aspectFit';
        const width = 1;
        const height = 1;
        const gravity = 'Left';
        const value = 'test';
        const visible = 'not true';
        const containerType = 'Container';
        const imageSource = <AppstudioImageDto>AppstudioModelFactory.create('Image', {
            style: AppstudioModelFactory.create('ImageStyle', {
                aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
                    value: aspectRatio
                }),
                scale: scale,
                gravity: gravity,

                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                })
            }),
            content: AppstudioModelFactory.create('FieldContent', {
                value: value
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });
        const labelSource = <AppstudioLabelDto>AppstudioModelFactory.create('Label', {
            style: AppstudioModelFactory.create('LabelStyle', {

                width: AppstudioModelFactory.create('Width', {
                    value: AppstudioModelFactory.create('CustomWidth', {
                        value: width
                    })
                }),
                height: AppstudioModelFactory.create('Height', {
                    value: AppstudioModelFactory.create('CustomHeight', {
                        value: height
                    })
                }),
                gravity: gravity
            }),
            content: AppstudioModelFactory.create('LabelContent', {
                value: value
            }),
            controls: <ComponentControlsDto<BaseEventDto, BaseActionDto>>AppstudioModelFactory.create('ComponentControls', {
                visible: visible
            })
        });

        tile.settings = settings;
        tile.text.push(labelSource);
        tile.subComponent.push(imageSource);
        source.tile = tile;
        describe('WHEN: translated', () => {
            const translator = new CarouselTranslator();
            let translation;

            before((done) => {
                translator.translate('videa', translationService, source).then(
                    (t) => {
                        translation = t;
                        done();
                    },
                    (err) => {
                        console.log(err);
                        done();
                    }
                );
            });

            it('THEN: the tile image value property should be translated as ' + value, () => {
                expect(translation.itemRenderer.items[0].value).to.equal(value);
            });
            it('THEN: the tile image width property should be translated as ' + width, () => {
                expect(translation.itemRenderer.items[0].width).to.equal(width);
            });
            it('THEN: the tile image height property should be translated as ' + height, () => {
                expect(translation.itemRenderer.items[0].height).to.equal(height);
            });
            it('THEN: the tile image gravity property should be translated as ' + gravity, () => {
                expect(translation.itemRenderer.items[0].gravity).to.equal(gravity);
            });
            it('THEN: the tile image visible property should be translated as ' + visible, () => {
                expect(translation.itemRenderer.items[0].visible).to.equal(visible);
            });
            it('THEN: the tile image aspectRatio property should be translated as 1:1', () => {
                expect(translation.itemRenderer.items[0].aspectRatio).to.equal(1);
            });
            it('THEN: the container after the images has a container type ' + containerType, () => {
                expect(translation.itemRenderer.items[1].type).to.equal(containerType);
            });
            it('THEN: the container after the images has linear layout', () => {
                expect(translation.itemRenderer.items[1].layout.type).to.equal('Linear');
            });
            it('THEN: the container after the images has linear layout and orientation', () => {
                expect(translation.itemRenderer.items[1].layout.orientation).to.equal('vertical');
            });
            it('THEN: the tile label\'s value property should be translated as ' + value, () => {
                expect(translation.itemRenderer.items[1].items[0].value).to.equal(value);
            });
            it('THEN: the tile label\'s width property should be translated as ' + width, () => {
                expect(translation.itemRenderer.items[1].items[0].width).to.equal(width);
            });
            it('THEN: the tile label\'s height property should be translated as ' + height, () => {
                expect(translation.itemRenderer.items[1].items[0].height).to.equal(height);
            });
            it('THEN: the tile label\'s gravity property should be translated as ' + gravity, () => {
                expect(translation.itemRenderer.items[1].items[0].gravity).to.equal(gravity);
            });
            it('THEN: the tile label\'s visible property should be translated as ' + visible, () => {
                expect(translation.itemRenderer.items[1].items[0].visible).to.equal(visible);
            });
        });
    });
});
