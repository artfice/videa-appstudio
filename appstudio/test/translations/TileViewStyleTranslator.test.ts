import chaiLib = require('chai');
const expect = chaiLib.expect;

import { TileViewStyleTranslator as TileStyleTranslator } from '../../translation/dataview/style/TileViewStyleTranslator';
import { TranslationApplicationService as TranslationService } from '../../application/TranslationApplicationService';
import { StandardAspectRatioTranslator } from '../../translation/aspectRatio/StandardAspectRatioTranslator';
import { CustomAspectRatioTranslator } from '../../translation/aspectRatio/CustomAspectRatioTranslator';
import { ComplexColorTranslator } from '../../translation/color/ComplexColorTranslator';
import { ColorTranslator } from '../../translation/color/ColorTranslator';

import { AppstudioModelFactory as AppstudioModelFactory } from '../../factory/AppstudioModelFactory';

import { DataViewTileStyle as AppstudioDataViewTileStyleDto } from '../../dto/app/configuration/uiconfig/screen/dataview/tile/DataViewTileStyle';
import ColorSchema = require('../../schema/common/Color');
import ComplexColorSchema = require('../../schema/appstudio/common/ComplexColor');

describe('TileStyle Translation Tests', () => {
	var translationService = new TranslationService();
	translationService.registerTranslator(StandardAspectRatioTranslator);
	translationService.registerTranslator(CustomAspectRatioTranslator);
	translationService.registerTranslator(ColorTranslator);
	translationService.registerTranslator(ComplexColorTranslator);

	describe('GIVEN: a TileStyle', () => {
		var source = <AppstudioDataViewTileStyleDto>AppstudioModelFactory.create('TileStyle'),
			aspectRatio = undefined;

		describe('WHEN: translated', () => {
			var translator = new TileStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the translation should not be empty object', () => {
				expect(translation).to.exist;
			});
			it('THEN: the aspectRatio property should be translated as ' + aspectRatio, () => {
				expect(translation.aspectRatio).to.equal(aspectRatio);
			});
		});
	});

	describe('GIVEN: a TileStyle with aspectRatio', () => {
		var aspectRatio = '1:1',
			aspectRatioResult = 1,
			source = <AppstudioDataViewTileStyleDto>AppstudioModelFactory.create('TileStyle', {
				aspectRatio: AppstudioModelFactory.create('StandardAspectRatio', {
					value: aspectRatio
				})
			});

		describe('WHEN: translated', () => {
			var translator = new TileStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the aspectRatio property should be translated as ' + aspectRatioResult, () => {
				expect(translation.aspectRatio).to.equal(aspectRatioResult);
			});
		});
	});
	describe('GIVEN: a TileStyle with backgroundColor', () => {
		const backgroundColor = {
			_metadata: ColorSchema.ID,
			value: '#AAAAAA'
		};

		const source = <AppstudioDataViewTileStyleDto>AppstudioModelFactory.create('TileStyle', {
				backgroundColor: {
					_metadata: ComplexColorSchema.ID,
					value: backgroundColor
				}
			});

		describe('WHEN: translated', () => {
			var translator = new TileStyleTranslator(),
				translation;

			before((done) => {
				translator.translate('videa', translationService, source).then(
					(t) => {
						translation = t;
						done();
					},
					(err) => {
						done();
					}
				);
			});

			it('THEN: the backgroundColor property should be translated as ' + JSON.stringify(backgroundColor), () => {
				expect(translation.backgroundColor.value).to.equal(backgroundColor.value);
				expect(translation.backgroundColor._metadata).to.equal(backgroundColor._metadata);
			});
		});
	});
});
